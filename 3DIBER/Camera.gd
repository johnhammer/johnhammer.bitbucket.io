extends Camera

var actionMargin = 50
var speed = 0.2
var speedX = 0
var speedY = 0
var moving = false
var ray_length = 500

var main 

func _ready():
	main = get_node("..")

func _input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		var from = project_ray_origin(event.position)
		var to = from + project_ray_normal(event.position) * ray_length
		var space_state = get_world().get_direct_space_state()
		var results =  space_state.intersect_ray(from, to)
		if results.size() > 0:
			main.Click(results.collider)
	
	if event is InputEventMouseMotion:
		var x = event.position[0]
		var y = event.position[1]
		var viewport = get_viewport().size
		
		if(x<actionMargin):
			speedX=-speed
		elif(x>viewport.x-actionMargin):
			speedX=speed
		else:
			speedX=0
		if(y<actionMargin):
			speedY=-speed
		elif(y>viewport.y-actionMargin):
			speedY=speed
		else:
			speedY=0

func _process(delta):
	global_translate(Vector3(speedX,0,speedY))
