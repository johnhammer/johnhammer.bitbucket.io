extends StaticBody

var h = 0
var x = 0
var y = 0
var r = 0

var hc7 = 0
var hc9 = 0
var hc1 = 0
var hc3 = 0

func ShowCube():
	$cube.visible = true

func Raise():
	h += 1
	Ramp(h,h,h,h,true)
	
func Lower():
	h -= 1
	Ramp(h,h,h,h,false)

func Ramp(c7,c9,c1,c3,isHigh):
	if isHigh:
		if(hc7<c7): hc7=c7
		if(hc9<c9): hc9=c9
		if(hc1<c1): hc1=c1
		if(hc3<c3): hc3=c3
	else:
		if(hc7>c7): hc7=c7
		if(hc9>c9): hc9=c9
		if(hc1>c1): hc1=c1
		if(hc3>c3): hc3=c3
		
	$MeshInstance.get_active_material(0).set_shader_param("v7", hc7)
	$MeshInstance.get_active_material(0).set_shader_param("v9", hc9)
	$MeshInstance.get_active_material(0).set_shader_param("v1", hc1)
	$MeshInstance.get_active_material(0).set_shader_param("v3", hc3)
