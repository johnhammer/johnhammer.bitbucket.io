extends CanvasLayer

var main 

func _ready():
	main = get_node("..")

func _on_Raise_pressed():
	main.action = Global.Actions.Raise

func _on_Lower_pressed():
	main.action = Global.Actions.Lower

func _on_Cube_pressed():
	main.action = Global.Actions.Cube
