extends Node

const CellResource = preload("res://Cell.tscn")

var matrix = []
var action

var width = 5;
var height = 5;
var vertices = PoolVector3Array();
var tmpMesh = Mesh.new()
var mat = SpatialMaterial.new()
var color = Color(0.9, 0.1, 0.1)

var grid = 4
var halfgrid = grid/2

func EditVertex(v,h):
	if v>=vertices.size() || v<0:return
	vertices[v] = Vector3(vertices[v].x,h,vertices[v].z)

func DrawChunk(x,y):
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	st.set_material(mat)

	for row in range(height):
		for col in range(width+1):
			vertices.push_back(Vector3(row*grid-halfgrid,0,col*grid-halfgrid));
			vertices.push_back(Vector3((row+1)*grid-halfgrid,0,col*grid-halfgrid));
	
	for row in range(height):
		for col in range(width):
			var cell = matrix[row][col]
			if !cell.r:
				EditVertex(((row*width+row) + col-1)*2 +2,cell.h)
				EditVertex(((row*width+row) + col-1)*2 + 4,cell.h)
				EditVertex(((row*width+row) + col-1)*2 + 3,cell.h)
				EditVertex(((row*width+row) + col-1)*2 + 5,cell.h)
				EditVertex((((row+1)*width+row) + col-1)*2 +4,cell.h)
				EditVertex((((row+1)*width+row) + col-1)*2 +6,cell.h)
				EditVertex((((row-1)*width+row) + col-1)*2 +1,cell.h)
				EditVertex((((row-1)*width+row) + col-1)*2 +3,cell.h)
	
	for v in vertices.size(): 
		st.add_color(color)
		st.add_vertex(vertices[v])
		
	st.commit(tmpMesh)
	$MeshInstance.mesh = tmpMesh

func _ready():
	for row in range(height):
		matrix.push_back([])
		for col in range(width):
			var cell = CellResource.instance()
			$Terrain.add_child(cell)
			cell.translate(Vector3(grid*row,0,grid*col))
			cell.x = row
			cell.y = col
			cell.h = 0
			cell.r = true #isramp
			matrix[row].push_back(cell)
	matrix[1][1].h = 1
	matrix[1][1].r = false
	matrix[0][0].h = 1
	matrix[0][0].r = false
	DrawChunk(0,0)
	

func Click(cell):
	match action:
		Global.Actions.Raise:
			cell.Raise()
			#Height(cell,true)
		Global.Actions.Lower: 
			cell.Lower()
			#Height(cell,false)
		Global.Actions.Cube: 
			cell.ShowCube()

