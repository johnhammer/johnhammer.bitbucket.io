
var Shogi_Const = {
	CellsX:8,
	CellsY:8,
	CellW:50,
	CellH:50,
	OffsetX:10,
	OffsetY:10,
}

var Shogi_Draw = {
	AltColor(color){
		if(color=="#FF00FF"){
			return "#FFFF00";
		}
		else{
			return "#FF00FF";
		}
	},
	Cell(ctx,x,y,color){
		ctx.fillStyle=color;
		ctx.fillRect(x*Shogi_Const.CellW, y*Shogi_Const.CellH, Shogi_Const.CellW, Shogi_Const.CellH);
	},
	Piece(ctx,x,y,player){
		if(player==1){
			ctx.fillStyle="#FFFFFF";
		}
		else{
			ctx.fillStyle="#000000";
		}		
		var margin = .2;
		ctx.fillRect(
			x*Shogi_Const.CellW + margin*Shogi_Const.CellW, 
			y*Shogi_Const.CellH + margin*Shogi_Const.CellH, 
			Shogi_Const.CellW - margin*2*Shogi_Const.CellW, 
			Shogi_Const.CellH - margin*2*Shogi_Const.CellH);
	},
	Board(ctx,board){
		var color = "#FF00FF";
		for (var y = 0; y < Shogi_Const.CellsY; y++) {
			for (var x = 0; x < Shogi_Const.CellsX; x++) {				
				Shogi_Draw.Cell(ctx,x,y,color);
				if(board[x][y]){
					Shogi_Draw.Piece(ctx,x,y,board[x][y]);
				}
				color = Shogi_Draw.AltColor(color);		
			}
			color = Shogi_Draw.AltColor(color);	
		}
	},
	PShogi_AintPossibleMove(ctx,x,y){
		ctx.fillStyle="#0000FF";
		var margin = .3;
		ctx.fillRect(
			x*Shogi_Const.CellW + margin*Shogi_Const.CellW, 
			y*Shogi_Const.CellH + margin*Shogi_Const.CellH, 
			Shogi_Const.CellW - margin*2*Shogi_Const.CellW, 
			Shogi_Const.CellH - margin*2*Shogi_Const.CellH);
	},
	PShogi_AintPossibleMoves(ctx,moves){
		for (var i = 0; i < moves.length; i++) {
			Shogi_Draw.PShogi_AintPossibleMove(ctx,moves[i].x,moves[i].y);
		}
	}
}

var Shogi_Interaction = {
	canvas:null,
	ctx:null,
	States : {
		Player1:0,
		Player1Selected:1
	},
	CurrentState : null,
	CurrentSelection : null,
	CurrentMoves : [],
	Init(canvas,ctx){
		Shogi_Interaction.canvas=canvas;
		Shogi_Interaction.ctx=ctx;
		Shogi_Interaction.CurrentState = Shogi_Interaction.States.Player1
	},
	GetMousePos(evt) {
		var rect = Shogi_Interaction.canvas.getBoundingClientRect();
		return {
		  x: evt.clientX - rect.left,
		  y: evt.clientY - rect.top
		};
	},
	GetNearestSquare(position) {
		var x = position.x;
		var y = position.y;
		if (x < 0 || y < 0) return null;
		x = (Math.floor((x- Shogi_Const.OffsetX) / Shogi_Const.CellW));
		y = (Math.floor((y- Shogi_Const.OffsetY) / Shogi_Const.CellH));
		return {x: x, y: y};
	},
	SelectPiece(pos){
		Shogi_Interaction.CurrentSelection=pos;
		Shogi_Interaction.CurrentMoves=Shogi_Game.GetPossibleMoves(pos);
		Shogi_Draw.PShogi_AintPossibleMoves(Shogi_Interaction.ctx,Shogi_Interaction.CurrentMoves);
	},
	MovePiece(pos){
		Shogi_Game.MovePiece(Shogi_Game.GetPieceAtPos(Shogi_Interaction.CurrentSelection),pos);
		Shogi_Interaction.CurrentSelection=null;
		Shogi_Interaction.CurrentMoves=[];
		Shogi_Game.Move++;
		if(Shogi_Game.Move>1)Shogi_Ai.Depth=2;
		Shogi_Game.Refresh();
	},
	IsValidMove(pos){
		for (var i = 0; i < Shogi_Interaction.CurrentMoves.length; i++) {
			if(Shogi_Interaction.CurrentMoves[i].x==pos.x && Shogi_Interaction.CurrentMoves[i].y==pos.y){
				return true;
			}
		}
		return false;
	},
	State(newState){
		Shogi_Interaction.CurrentState = newState;
	},
	Click(evt) {
		var pos = Shogi_Interaction.GetNearestSquare(Shogi_Interaction.GetMousePos(evt));
		if (pos != null) {
			switch(Shogi_Interaction.CurrentState){
				case Shogi_Interaction.States.Player1:
					if(Shogi_Game.IsPlayerCell(1,pos)){
						Shogi_Interaction.SelectPiece(pos);
						Shogi_Interaction.State(Shogi_Interaction.States.Player1Selected);
					}
					break;
				case Shogi_Interaction.States.Player1Selected:
					if(Shogi_Interaction.IsValidMove(pos)){
						Shogi_Interaction.MovePiece(pos);
						
						var before = new Date();
						Shogi_Ai.Play();
						var after = new Date();
						console.log(after - before);
						
						Shogi_Game.Refresh();
						Shogi_Interaction.State(Shogi_Interaction.States.Player1);
					}
					else if(Shogi_Game.IsPlayerCell(1,pos)){
						Shogi_Game.Refresh();
						Shogi_Interaction.SelectPiece(pos);
						Shogi_Interaction.State(Shogi_Interaction.States.Player1Selected);
					}
					else{
						Shogi_Game.Refresh();
						Shogi_Interaction.State(Shogi_Interaction.States.Player1);
					}
					break;
			}
		}
    }
}

var Shogi_Game = {
	Move:0,
	Board : {a:[],b:{},c:{}},//array, player pieces, Shogi_Ai pieces
	ctx:null,
	Init(c,ctx,board=Shogi_Game.Board){
		for (var y = 0; y < Shogi_Const.CellsY; y++) {
			board.a[y] = [];
			for (var x = 0; x < Shogi_Const.CellsX; x++) {
				board.a[y].push(0);
			}
		}
		for (var x = 0; x < Shogi_Const.CellsX; x++) {
			board.a[x][0] = 2;
		}
		for (var x = 0; x < Shogi_Const.CellsX; x++) {
			board.a[x][Shogi_Const.CellsY-1] = 1;
		}
		
		for (var x = 0; x < Shogi_Const.CellsX; x++) {
			Shogi_Game.Board.c[x]={x:x,y:0,i:x}; 				//Shogi_Ai
			Shogi_Game.Board.b[x]={x:x,y:Shogi_Const.CellsY-1,i:x}; //player
		}
		
		Shogi_Game.ctx = ctx;
		Shogi_Interaction.Init(c,ctx);
	},
	Refresh(board=Shogi_Game.Board){
		Shogi_Draw.Board(Shogi_Game.ctx,board.a);
	},
	IsPlayerCell(player,pos,board=Shogi_Game.Board){
		return board.a[pos.x][pos.y]==player;
	},
	GetPossibleMoves(pos,board=Shogi_Game.Board){
		var moves = [];
		var i = pos.x;		
		while(i>0){
			i--;
			if(board.a[i][pos.y]){
				break;
			}
			moves.push({x:i,y:pos.y});
		}
		i = pos.x;	
		while(i<Shogi_Const.CellsX-1){
			i++;
			if(board.a[i][pos.y]){
				break;
			}
			moves.push({x:i,y:pos.y});
		}
		i = pos.y;	
		while(i>0){
			i--;
			if(board.a[pos.x][i]){
				break;
			}
			moves.push({x:pos.x,y:i});			
		}
		i = pos.y;
		while(i<Shogi_Const.CellsY-1){
			i++;
			if(board.a[pos.x][i]){
				break;
			}
			moves.push({x:pos.x,y:i});
		}
		return moves;
	},
	
	GetPieceAtPos(pos,board=Shogi_Game.Board,pieces=Shogi_Game.Board.b){
		for(var i in pieces){
			if(pieces[i].x==pos.x&&pieces[i].y==pos.y){
				return pieces[i]
			}
		}
	},
	
	MovePiece(piece,pos,board=Shogi_Game.Board,pieces=board.b){	
		pieces[piece.i] = {x:pos.x,y:pos.y,i:piece.i};		
		Shogi_Game.ValidateMove(piece,pos,board);
		board.a[pos.x][pos.y] = board.a[piece.x][piece.y];
		board.a[piece.x][piece.y] = 0;		
	},
	ValidateMove(piece,pos,board=Shogi_Game.Board){
		var player = board.a[piece.x][piece.y];	
		var feind = player==1?2:1;
		var removed = [];	
		var i,remove;	
		//left
		remove = [];
		if(pos.x>0 && board.a[pos.x-1][pos.y]==feind){
			if(pos.x==1 && pos.y==0){
				if(board.a[0][1]==player){				
					removed.push({x:pos.x-1,y:pos.y});
				}
			}
			else if(pos.x==1 && pos.y==Shogi_Const.CellsY-1){
				if(board.a[0][Shogi_Const.CellsY-2]==player){				
					removed.push({x:pos.x-1,y:pos.y});
				}
			}
			else{
				i = pos.x;		
				while(i>0){
					i--;
					if(board.a[i][pos.y]!=feind){
						break;
					}
					remove.push({x:i,y:pos.y});
				}
				if(board.a[i][pos.y]==player){				
					removed = removed.concat(remove);
				}
			}
		}
		//right
		remove = [];
		if(pos.x<Shogi_Const.CellsX-1 && board.a[pos.x+1][pos.y]==feind){
			if(pos.x==Shogi_Const.CellsX-2 && pos.y==0){
				if(board.a[Shogi_Const.CellsX-1][1]==player){				
					removed.push({x:pos.x+1,y:pos.y});
				}
			}
			else if(pos.x==Shogi_Const.CellsX-2 && pos.y==Shogi_Const.CellsY-1){
				if(board.a[Shogi_Const.CellsX-1][Shogi_Const.CellsY-2]==player){				
					removed.push({x:pos.x+1,y:pos.y});
				}
			}
			else{
				i = pos.x;		
				while(i<Shogi_Const.CellsX-1){
					i++;
					if(board.a[i][pos.y]!=feind){
						break;
					}
					remove.push({x:i,y:pos.y});
				}
				if(board.a[i][pos.y]==player){				
					removed = removed.concat(remove);
				}
			}
		}
		//top
		remove = [];
		if(pos.y>0 && board.a[pos.x][pos.y-1]==feind){
		//corner
			i = pos.y;		
			while(i>0){
				i--;
				if(board.a[pos.x][i]!=feind){
					break;
				}
				remove.push({x:pos.x,y:i});
			}
			if(board.a[pos.x][i]==player){				
				removed = removed.concat(remove);
			}
		}
		//bottom
		remove = [];
		if(pos.y<Shogi_Const.CellsY-1 && board.a[pos.x][pos.y+1]==feind){
		///corner
			i = pos.y;		
			while(i<Shogi_Const.CellsY-1){
				i++;
				if(board.a[pos.x][i]!=feind){
					break;
				}
				remove.push({x:pos.x,y:i});
			}
			if(board.a[pos.x][i]==player){				
				removed = removed.concat(remove);
			}
		}
		
		for (var x = 0; x < removed.length; x++) {
			Shogi_Game.KillPiece(removed[x],board);
		}
	},
	KillPiece(piece,board=Shogi_Game.Board){
		var player = board.a[piece.x][piece.y];
		var pieces = board.b;
		if(player==2){
			 pieces = board.c;
		}
		for (var i in pieces) {
			if(pieces[i].x==piece.x&&pieces[i].y==piece.y){
				pieces[i] = null;
				delete pieces[i];
			}
		}
		board.a[piece.x][piece.y] = 0;
	}
}

var Shogi_Ai = {
	rnd : null,
	Pieces : {},
	Depth : 1,
	Init(rnd){		
		Shogi_Ai.rnd = rnd;
		for (var x = 0; x < Shogi_Const.CellsX; x++) {
			Shogi_Game.Board.c[x]={x:x,y:0,i:x}; //Shogi_Ai
			Shogi_Game.Board.b[x]={x:x,y:Shogi_Const.CellsY-1,i:x}; //player
		}
	},
	EvaluateBoard(board,isShogi_Ai){
		if(!isShogi_Ai){
			var value = Object.keys(board.c).length*2-Object.keys(board.b).length;
			return value;
		}
		else{
			var value = Object.keys(board.b).length*2-Object.keys(board.c).length;
			return value;
		}
	},
	iEvaluateMoveShogi_Ai(piece,move,fakeBoard,level,isShogi_Ai){
		var score = 0;
		if(level>Shogi_Ai.Depth){
			return Shogi_Ai.EvaluateBoard(fakeBoard,isShogi_Ai);	
		}
		else{
			var pieces = isShogi_Ai?fakeBoard.c:fakeBoard.b;
			Shogi_Ai.MovePiece(pieces[piece],move,fakeBoard,pieces);
			pieces = !isShogi_Ai?fakeBoard.c:fakeBoard.b;
			return Shogi_Ai.Play(level+1,fakeBoard,pieces,!isShogi_Ai);			
		}
	},
	FastClone(obj) {
		var board = {a:[],b:{},c:{}};
		for (var y = 0; y < Shogi_Const.CellsY; y++) {
			board.a[y] = [];
			for (var x = 0; x < Shogi_Const.CellsX; x++) {
				board.a[y].push(obj.a[y][x]);
			}
		}
		for (var k in obj.b) {
			board.b[k] = {x:obj.b[k].x,y:obj.b[k].y,i:k};
		}
		for (var k in obj.c) {
			board.c[k] = {x:obj.c[k].x,y:obj.c[k].y,i:k};
		}
		return board;
	},
	EvaluateMove(piece,move,level,isShogi_Ai,board){
		var fakeBoard = Shogi_Ai.FastClone(board);		
		var score = Shogi_Ai.iEvaluateMoveShogi_Ai(piece,move,fakeBoard,level,isShogi_Ai);
		return {s:score,m:move,p:piece}
	},
	MovePiece(piece,move,board,pieces){
		pieces[piece.i] = {x:move.x,y:move.y,i:piece.i};
		Shogi_Game.MovePiece(piece,move,board,false);
	},
	ArrayMax(arr) {
		return arr.reduce(function (p, v) {
			return ( p.s >= v.s ? p : v );
		});
	},
	Max(arr) {
		var max = -Infinity;
		var maxIndices = [];
		for (var i = 0; i < arr.length; i++) {
			if (arr[i].s === max) {
			  maxIndices.push(arr[i]);
			} else if (arr[i].s > max) {
				maxIndices = [arr[i]];
				max = arr[i].s;
			}
		}
		return maxIndices;
	 },
	Play(level=1,board=Shogi_Game.Board,pieces=Shogi_Game.Board.c,isShogi_Ai=true){	
		var moves = []
		for(var piece in pieces){
			moves = moves.concat(Shogi_Game.GetPossibleMoves(pieces[piece],board).map(x=>Shogi_Ai.EvaluateMove(piece,x,level,isShogi_Ai,board)));			
		}
		var move = Random.Choice(Shogi_Ai.rnd,Shogi_Ai.Max(moves));		
		Shogi_Ai.MovePiece(pieces[move.p],move.m,board,pieces);	
		return Shogi_Ai.EvaluateBoard(board,isShogi_Ai);
	}
}
