var Brain = {
	Relation(me,you,t1,t2){
		me.Relations[you.Id] = {
			Opinion : t1,
			Knowledge:0	
		};
		you.Relations[me.Id] = {
			Opinion : t2,
			Knowledge:0	
		};
	},
	RandomRelative(rnd,brain){
		var id = brain.Id;
		return Random.Choice(rnd,Object.keys(brain.Relations).filter(x=>x!='God'&&x!=id));
	},
	Grow(person,world,age){		
		var spokenByOthers = 	2;//20-person.Age;
		var spokenBySelf = 		1;//20-person.Age;
		
		for(var i=0;i<spokenByOthers;i++){
			var speaker = world.People[Brain.RandomRelative(person.Rnd,person.Brain)];	
			if(speaker){
				Speak.SpeakRandom(world,person.Rnd,speaker.Brain,person.Brain);
			}			
		}
		for(var i=0;i<spokenBySelf;i++){
			Speak.SpeakRandom(world,person.Rnd,person.Brain,person.Brain);
		}
	},
	God(person,world,age){
		var spokenByGod = 1;//20-person.Age;
		
		for(var i=0;i<spokenByGod;i++){
			Speak.SpeakRandom(world,person.Rnd,world.God,person.Brain);
		}
	},
	Birth(rnd,person,god){
		console.log('birth of '+person.Name);
		person.Rnd = Random.Create(Random.Int(rnd));
		var brain = God.TabulaRasa(person.Id,person.Name);
		Brain.Relation(brain,god,10,10);
		Brain.Relation(brain,brain,person.Personality.Ego,person.Personality.Ego);		
		person.Brain = brain;
	},
}