var ConversationFactory = {
	Agreement(grade){
		var result;
		if (grade == -1){
			result = "I don't have anything to say to that"
		}		
		if(grade>8){
			result = "I totally agree with you"
		}
		else if(grade>6){
			result = "I agree"
		}
		else if(grade>4){
			result = "I get your point"
		}
		else if(grade>2){
			result = "I disagree"
		}
		else{
			result = "You are very wrong with this"
		}
		return result;
	},
	Opinion(grade){
		var result;
		if (grade == -1){
			result = "I don't know anything about it"
		}		
		if(grade>8){
			result = "I love it"
		}
		else if(grade>6){
			result = "I like it"
		}
		else if(grade>4){
			result = "It's OK"
		}
		else if(grade>2){
			result = "I don't like it"
		}
		else{
			result = "I hate it"
		}
		return result;
	},
	PersonOpinion(grade){
		var result;
		if (grade == -1){
			result = "I don't know this person"
		}		
		if(grade>8){
			result = "One of my best friends"
		}
		else if(grade>6){
			result = "A good friend"
		}
		else if(grade>4){
			result = "We get along just fine"
		}
		else if(grade>2){
			result = "Not my best friend"
		}
		else{
			result = "I really don't like this person"
		}
		return result;
	}
}	

var StateMachine = {
	Stack : [],
	StateStack : [],
	Actions : {},
	Conditions : {},
	Transitions:null,
	State:null,
	Run(t){		
		if(t=="Back"){
			StateMachine.Back();
			return;
		}
		StateMachine.Stack.push(t);
		StateMachine.StateStack.push(StateMachine.State);
		Conversation._Clear();
		if(StateMachine.Actions[t]) {
			StateMachine.Actions[t]()
		}		
		StateMachine.State=StateMachine.Transitions[StateMachine.State][t];
		for(var i in StateMachine.Transitions[StateMachine.State]){
			if(!StateMachine.Conditions[i] || StateMachine.Conditions[i]()){
				Conversation._Option(i);
			}			
		}
		if(StateMachine.Stack.length>1){
			Conversation._Option("Back");
		}		
	},
	Back(lim=1){		
		for(var i=0;i<lim;i++){
			StateMachine.Stack.pop();
			StateMachine.StateStack.pop();
		}		
		var result = StateMachine.Stack.pop();
		var state = StateMachine.StateStack.pop();
		StateMachine.State = state;
		StateMachine.Run(result);
	},
	Create(s,t){
		StateMachine.Transitions = t;
		StateMachine.State = s;
		StateMachine.Stack = [];
		StateMachine.StateStack = [];
		StateMachine.Actions = {};
		StateMachine.Conditions = {};
		StateMachine.Run(s);
	}
}


var Conversation = {
	State : 0,
	Container : null,
	Person : null,  //you
	Person2 : null, //he/she
	
	_Option(content){
		Conversation.Container.innerHTML += `<button onclick="StateMachine.Run('${content}')">${content}</button>`
	},
	_Answer(content){
		console.log(content);
		Conversation.Container.innerHTML += `<p>${content}</p>`
	},
	_Clear(){
		Conversation.Container.innerHTML = `
			<ul class="breadcrumb">
				${StateMachine.Stack.map((x,i)=>`<a onclick="StateMachine.Back('${StateMachine.Stack.length-i-1}')"><li>${x}</li></a>`).join('')}
			</ul>
		`;
	},
	React(score){
		if(score==-1){
			Conversation._Answer(ConversationFactory.Agreement(null));
			return;
		}		
		var content = 10-Math.abs(Conversation.Person.Relations[Conversation.Person2.Id].General-score);		
		Conversation._Answer(ConversationFactory.Agreement(content));
	},
	Start(id){
		Conversation.Container = document.getElementById("conversation_container_"+id);
		Conversation._Clear();		
		Conversation.Person = universe.People[id];		
		
		StateMachine.Create("Conversation",{
			"Conversation" : {
				"Conversation":"INITIAL"
			},
			"INITIAL" : {
				"Tell me about":"Topics",
				"What do you think about":"Topics",
				"Let me tell you that":"Topics",
				"I think that":"Topics",
				"Do you want to":"Commands"
			},
			"Commands" : {
				"Play shogi" : "Back",
				"Tell me anything" : "Back",
				"Ask me anything" : "Back",
			},
			"Topics" : {
				"People":"People",
				"Politics":"Politics",
				"History":"History",
				"Philosophy":"Philosophy",
				"Skills":"Skills",
				"Weather":"Weather",
			},
			"People" : {
				"Me":"Me",
				"You":"You"
			},
			"Relation":{
				"Basic data":"Basic data",
				"Interests":"Interests",
				
				"In general?":"Topic",
				"Personality?":"Personality?",
			},	
			"Topic":{
				"I don#t know anything about it": "Back",
				"I love it": "Back",
				"I like it": "Back",
				"It#s OK": "Back",
				"I don#t like it": "Back",
				"I hate it": "Back",
			},
			"Politics":{},
			"History":{},
		});		
		
		StateMachine.Actions = {
			"Play shogi":()=>Game.Shogi(Conversation.Person),
			
			"In general?":()=>{
				var content = Conversation.Person.Relations[Conversation.Person2.Id].General;
				Conversation._Answer(ConversationFactory.PersonOpinion(content));
			},
			
			"I don#t know anything about it":()=>Conversation.React(-1),
			"I love it":()=>Conversation.React(8),
			"I like it":()=>Conversation.React(6),
			"It#s OK":()=>Conversation.React(4),
			"I don#t like it":()=>Conversation.React(2),
			"I hate it":()=>Conversation.React(0),
		};	
		
		StateMachine.Conditions = {
			"Basic data":()=>StateMachine.Stack[1]=="Tell me about",
			
			"In general?":()=>StateMachine.Stack[1]=="What do you think about" || "I think that",
			"Personality?":()=>StateMachine.Stack[1]=="What do you think about",
			
			"I don#t know anything about it":()=>StateMachine.Stack[1]=="I think that",
			"I love it":()=>StateMachine.Stack[1]=="I think that",
			"I like it":()=>StateMachine.Stack[1]=="I think that",
			"It#s OK":()=>StateMachine.Stack[1]=="I think that",
			"I don#t like it":()=>StateMachine.Stack[1]=="I think that",
			"I hate it":()=>StateMachine.Stack[1]=="I think that",
		};	
		
		//dynamic
		
		
		for(var i in Conversation.Person.Relations){
			const other = universe.People[i]; //Conversation.Person.Relations[i]
			var name = other.Name+" "+other.Surname;
			StateMachine.Transitions["People"][name] = "Relation";
			StateMachine.Actions[name] = ()=>Conversation.Person2=other;
		}		
		for(var i in universe.Countries){
			var name = universe.Countries[i].Name;
			StateMachine.Transitions["Politics"][name] = "Topic";
		}
		
		for (var i=0; i < universe.History.length; i++) {
			var name = HistoryTextMaker.EventText(universe.History[i]);
			StateMachine.Transitions["History"][name] = "Topic";
		}
	},	
}