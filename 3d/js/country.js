var Country = {
	CountryPrefix : [
		'meil','enn','gho','leght','sein','kat','gwel','tem','dhen','nooi','meen','sent','olw','wer','lok','ghron'
	],	
	CountrySuffix : [
		'rey','cia','en','ex','os','as','en','ex','bria','lia','gas','sey','wara','way','an','ent'
	],
	BigCountryTitle : [
		'Empire of ',
		'Imperial ',
		'Union of ',
		'Great ',
		'Federation of ',
		'Kingdoms of ',
		'Commonwealth of ',
		'Alliance of ',
	],
	MidCountryTitle : [
		'Crown of ',
		'United ',
		'Confederation of ',
		'Dominion of ',
		'Republic of ',
		'Free ',
		'States of ',
		'Provinces of ',
	],
	CountryName(rnd,power){
		var name = Random.Choice(rnd,Country.CountryPrefix) + Random.Choice(rnd,Country.CountrySuffix);
		name = LIB.Capitalize(name);
		if(power>70){
			return Random.Choice(rnd,Country.BigCountryTitle) + name;
		}
		else if(power>20){
			return Random.Choice(rnd,Country.MidCountryTitle) + name;
		}
		else{
			return name;
		}	
	},
	GvtPrefix2 : [
		'mon','demo','co','an','des','feu','auto','capit','min','neo','anarco','soci'
	],
	GvtPrefix3 : [
		'arch','crac','fasc','mun','pot','dal','al'
	],	
	GovernmentForm(rnd){
		var name = "";
		if(Random.Chance(rnd,1)){
			name += Random.Choice(rnd,Country.GvtPrefix2);
		}
		name += Random.Choice(rnd,Country.GvtPrefix3) + 'ism'
		name = LIB.Capitalize(name);
		return name;
	},	
	Country : function(rnd,power,year){
		return {
			Name:Country.CountryName(rnd,power),
			Power:power,
			Allies:{},
			Enemies:{},
			Government:Country.GovernmentForm(rnd),
			Birth:year
		};
	},
	Remove : function(country,Countries,year){
		country.Death = year;
		for(i in country.Allies){
			delete country.Allies[i].Allies[country.Name];
		}	
		for(i in country.Enemies){
			delete country.Enemies[i].Enemies[country.Name];
		}
		delete Countries[country.Name];
	},
	Pick : function(rnd,list){
		return list[Random.Choice(rnd,Object.keys(list))];			
	},
	Write(country){
		return `${country.Name} - ${country.Government}. ${country.Birth?country.Birth:''} - ${country.Death?country.Death:''}`;
	},
}
