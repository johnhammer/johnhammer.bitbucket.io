var timeUniform = {
	iGlobalTime: {
		type: 'f',
		value: 0.1
	},
	iResolution: {
		type: 'v2',
		value: new THREE.Vector2()
	}
};		
timeUniform.iResolution.value.x = window.innerWidth;
timeUniform.iResolution.value.y = window.innerHeight;
var clock = new THREE.Clock();


var raycaster;

var Engine = {
	PhisicsObjects : [],
	InteractionObjects : [],
	ActiveObject : null,
	Objects : [],
	ChunkSize : 16,
	ChunkRender : 1,
	MaxChunks : 256,
	DynamicRender : true,
	Chunks : {},
	
		
	AddPerson(id){
		var ox=0,oy=0;
		
		var material = new THREE.MeshBasicMaterial( { color: 0xFF0000 } );
		var halfExtents = new CANNON.Vec3(1,1,1);
		var boxShape = new CANNON.Box(halfExtents);
		var boxGeometry = new THREE.BoxGeometry(halfExtents.x*2,halfExtents.y*2,halfExtents.z*2);

		//var heightmap = ProceduralGenerator.GetHeightMap(ox,oy,Engine.ChunkSize,ProceduralGenerator.ForestTerrain);
		var x = (Math.random())*(Engine.ChunkSize-1);
		var z = (Math.random())*-(Engine.ChunkSize-1);
		var y = 10//Engine.GetTerrainHeightAt(x*Engine.ChunkSize,-z*Engine.ChunkSize,heightmap,Engine.ChunkSize);		
		var newx = x+(Engine.ChunkSize-1)*ox
		var newz = z+(Engine.ChunkSize-1)*oy

		var boxBody = new CANNON.Body({ mass: 0 });
		boxBody.addShape(boxShape);
		var boxMesh = new THREE.Mesh( boxGeometry, material );
		world.add(boxBody);
		scene.add(boxMesh);
		boxBody.position.set(newx,y,newz);
		boxMesh.position.set(newx,y,newz);
		boxBody.mesh = boxMesh;
		
		Engine.PhisicsObjects.push({m:boxMesh,b:boxBody})
		Engine.Objects.push({x:newx+halfExtents.x,y:newz+halfExtents.z,id:id})
		
		return boxBody;
	},
	Animate() {		

		raycaster.setFromCamera( { x : 0, y : 0 }, camera );
		var intersects = raycaster.intersectObjects( Engine.InteractionObjects );
		for ( var i = 0; i < intersects.length; i++ ) {
			if(intersects[ i ].distance<10){
				if(Engine.ActiveObject){
					Engine.ActiveObject.material = Engine.ActiveObject.PrevMat;
				}
				Engine.ActiveObject = intersects[ i ].object;
				Engine.ActiveObject.PrevMat = Engine.ActiveObject.material;
				Engine.ActiveObject.material = new THREE.MeshBasicMaterial();			
			}
		}
	
		timeUniform.iGlobalTime.value += clock.getDelta();
		requestAnimationFrame( Engine.Animate );
		if(controls.enabled){
			world.step(dt);
			
			for(var i=0; i<Engine.PhisicsObjects.length; i++){
				Engine.PhisicsObjects[i].m.position.copy(Engine.PhisicsObjects[i].b.position);
				Engine.PhisicsObjects[i].m.quaternion.copy(Engine.PhisicsObjects[i].b.quaternion);
			}
		}
		controls.update( Date.now() - time );
		renderer.render( scene, camera );
		time = Date.now();
	},	
	
	RemoveChunk(key){ //
		for (var i = 0; i < Engine.Chunks[key].o.length; i++) {
			var body = Engine.Chunks[key].o[i];			
			scene.remove(body.mesh);
			world.removeBody(body);
		}		
		var hbody = Engine.Chunks[key].b;
		scene.remove(hbody.mesh);
		world.removeBody(hbody);
		delete Engine.Chunks[key];
	},
	TerrainCleanup(){ //
		if(Object.keys(Engine.Chunks).length>Engine.MaxChunks){
			var lim = Math.pow(Engine.ChunkRender*2+1,1);		
			var remove = Object.keys(Engine.Chunks);
			remove = remove.sort(function(a, b){
				var x = a.split("-");
				var y = b.split("-");
				return LIB.PointDist(x[0],x[1],y[0],y[1]);
			})
			remove = remove.slice(0,lim);
			for (var i = 0; i < remove.length - 1; i++) {
				Engine.RemoveChunk(remove[i]);				
			}
		}
	},
	TerrainChunk(x,y){ //
		if(Engine.Chunks[x+"-"+y]){
			Engine.Chunks[x+"-"+y].t++;
			return;
		}
		var chunk = TerrainFactory.Chunk(x,y);
		Engine.Chunks[x+"-"+y] = chunk;
	},
	Terrain(x,y){ //
		for (var i = x-Engine.ChunkRender; i <= x+Engine.ChunkRender; i++) {
			for (var j = y-Engine.ChunkRender; j <= y+Engine.ChunkRender; j++) {
				Engine.TerrainChunk(i,j);				
			}
		}
		Engine.TerrainCleanup(x,y);
	},	
	PlayerHasMoved(pos){ //
		var x = Math.floor(pos.x/(Engine.ChunkSize-1));
		var y = Math.floor(pos.z/(Engine.ChunkSize-1))+1;
		if(Engine.DynamicRender){
			Engine.Terrain(x,y);
		}
		var objnearby = false;
		for(var i=0;i<Engine.Objects.length;i++){
			if(LIB.PointDist(Engine.Objects[i].x,Engine.Objects[i].y,pos.x,pos.z)<4){
				Game.ObjectNearby(Engine.Objects[i].id);
				objnearby = true;
				break;
			}
		}
		if(!objnearby){
			Game.NoObjectNearby();
		}
	},
	Load(){
		//MaterialsFactory.Load(callback);
	},
	Start(callback,rnd){
		//var promises = [];
		//Engine.Load(promises);
		//Promise.all(promises).then(results => {
			var rnd2 = Random.Create();
			var firstChunkX = Random.Range(rnd2,0,1000);
			var firstChunkY = Random.Range(rnd2,0,1000);
		
			Engine.InitControls();
			Physics.Init();			
			Physics.Player(firstChunkX*(Engine.ChunkSize-1)+(Engine.ChunkSize/2),firstChunkY*(Engine.ChunkSize-1)-(Engine.ChunkSize/2),1020);
			
			Graphics.Init(rnd);
			
			Engine.Terrain(firstChunkX,firstChunkY);
			
			callback();
			
			raycaster = new THREE.Raycaster();
			
			Engine.Animate();		







    






			
		//});	
	},	
	InitControls(){
		var blocker = document.getElementById( 'blocker' );
		var instructions = document.getElementById( 'instructions' );

		var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

		if ( havePointerLock ) {

			var element = document.body;

			var pointerlockchange = function ( event ) {

				if ( document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element ) {

					controls.enabled = true;

					blocker.style.display = 'none';

				} else {

					controls.enabled = false;

					blocker.style.display = '-webkit-box';
					blocker.style.display = '-moz-box';
					blocker.style.display = 'box';

					instructions.style.display = '';

				}

			}

			var pointerlockerror = function ( event ) {
				instructions.style.display = '';
			}

			// Hook pointer lock state change events
			document.addEventListener( 'pointerlockchange', pointerlockchange, false );
			document.addEventListener( 'mozpointerlockchange', pointerlockchange, false );
			document.addEventListener( 'webkitpointerlockchange', pointerlockchange, false );

			document.addEventListener( 'pointerlockerror', pointerlockerror, false );
			document.addEventListener( 'mozpointerlockerror', pointerlockerror, false );
			document.addEventListener( 'webkitpointerlockerror', pointerlockerror, false );

			instructions.addEventListener( 'click', function ( event ) {
				instructions.style.display = 'none';

				// Ask the browser to lock the pointer
				element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;

				if ( /Firefox/i.test( navigator.userAgent ) ) {

					var fullscreenchange = function ( event ) {

						if ( document.fullscreenElement === element || document.mozFullscreenElement === element || document.mozFullScreenElement === element ) {

							document.removeEventListener( 'fullscreenchange', fullscreenchange );
							document.removeEventListener( 'mozfullscreenchange', fullscreenchange );

							element.requestPointerLock();
						}

					}

					document.addEventListener( 'fullscreenchange', fullscreenchange, false );
					document.addEventListener( 'mozfullscreenchange', fullscreenchange, false );

					element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;

					element.requestFullscreen();

				} else {

					element.requestPointerLock();

				}

			}, false );

		} else {

			instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';

		}
	}
}
