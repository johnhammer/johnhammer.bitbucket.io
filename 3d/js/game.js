var Frontend = {
	Modal : function(content){
		messagebox.innerHTML = `
			<div id="current_modal" class="modal">
				<div class="modal-content">
					${content}
				</div>
			</div>            
		`;		
		window.onclick = function(event) {
			if (event.target == current_modal) {
				Frontend.CloseModal();
			}
		};
	},
	CloseModal : function(){
		current_modal.outerHTML = '';
		window.onclick = null;
	},
	InitCollapsibles(){
		var coll = document.getElementsByClassName("collapsible");
		var i;
		for (i = 0; i < coll.length; i++) {
			const content = coll[i].nextElementSibling;
			content.style.display = "none";
			coll[i].addEventListener("click", function() {
				this.classList.toggle("active");		
				if (content.style.display === "block") {
					content.style.display = "none";
				}
				else {
					content.style.display = "block";
				}		
			});				
		}		
	},	
	Collapsible(button,content){		
		return `
			<button type="button" class="collapsible">${button}</button>
			<div class="content">
				${content}
			</div>
		`;
	}
}


var Game = {
	Object : null,
	Interact(){
		if(Game.Object){
			document.exitPointerLock();
			
			var content = Person.Write(Game.Object,universe.People);
			Frontend.Modal(content);	
			Frontend.InitCollapsibles();			
		}		
	},
	ObjectNearby(id){
		Game.Object = universe.People[id];
		messagebox.innerHTML = "Press E to speak to " + Game.Object.Name;
	},
	NoObjectNearby(id){
		Game.Object = null;
		messagebox.innerHTML = "";
	},
	Shogi(person){		
		Frontend.Modal(`<canvas id="board_canvas" width=1000 height=1000 oncontextmenu="return false;" onclick="Shogi_Interaction.Click(event)"></canvas>`);
		
		var rnd = Random.Create();
		var c = document.getElementById("board_canvas");
		var ctx = c.getContext("2d");

		Shogi_Game.Init(c,ctx);
		Shogi_Game.Refresh();
		Shogi_Ai.Init(rnd);
	}
}