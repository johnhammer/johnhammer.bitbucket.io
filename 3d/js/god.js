var God = {
	CreatePeople(rnd,amount){
		var people = {};
		for(var i=0;i<amount;i++){
			var id = LIB.Guid();
			people[id] = Person.Create(rnd,id);
		}
		return people;
	},
	GrowPeople(rnd,world){	
		var people = world.People;
		var maxAge = LIB.MaxProp(people,x=>x.Age);
		for(var i=maxAge;i>0;i--){
			var subset = LIB.Filter(people,x=>x.Age<=i);
			for(var id in subset){
				if(!people[id].Brain){
					Brain.Birth(rnd,people[id],world.God);
				}
				Brain.God(people[id],world,people[id].Age-i);
			}
			for(var id in subset){
				Brain.Grow(people[id],world,people[id].Age-i);
			}
		}
	},
	Value(){
		return {
			Value:10,
			Knowledge:100
		};
	},
	TabulaRasa(id,name){
		return {
			Id : id,
			Name : name,
			//relations
			Relations : {},
			//history
			HistoryCountries:{},
			HistoryFacts:{},
			//politics
			Countries:{},
			Movements:{},	
			//Nature
			Trees:{},	
			Bushes:{},	
			Herbs:{},
			Rocks:{},	
			
		}
	},
	Create(world){
		var god = God.TabulaRasa("God","GOD");
		
		for(var country in world.Countries){
			var name = world.Countries[country].Name;
			god.Countries[name] = God.Value();
		}
		for(var country in world.HistoryCountries){
			var name = world.HistoryCountries[country].Name;
			god.HistoryCountries[name] = God.Value();
		}
		for(var fact in world.History){
			var name = world.History[fact];
			god.HistoryFacts[name] = God.Value();
		}
		return god;
	},
}