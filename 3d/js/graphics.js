var Graphics = {	
	_AssignUVs(geometry) {
		geometry.faceVertexUvs[0] = [];
		geometry.faces.forEach(function(face) {
			var components = ['x', 'y', 'z'].sort(function(a, b) {
				return Math.abs(face.normal[a]) > Math.abs(face.normal[b]);
			});

			var v1 = geometry.vertices[face.a];
			var v2 = geometry.vertices[face.b];
			var v3 = geometry.vertices[face.c];

			geometry.faceVertexUvs[0].push([
				new THREE.Vector2(v1[components[0]], v1[components[1]]),
				new THREE.Vector2(v2[components[0]], v2[components[1]]),
				new THREE.Vector2(v3[components[0]], v3[components[1]])
			]);

		});
		geometry.uvsNeedUpdate = true;
	},
	TerrainGeometry(hfBody,x,y,material){
		var geometry = new THREE.Geometry();
		let shape = hfBody.shapes[0];
		var v0 = new CANNON.Vec3();
		var v1 = new CANNON.Vec3();
		var v2 = new CANNON.Vec3();
		for (var xi = 0; xi < shape.data.length -2; xi++) {
		  for (var yi = 0; yi < shape.data[xi].length - 2; yi++) {
			  for (var k = 0; k < 2; k++) {
				  shape.getConvexTrianglePillar(xi, yi, k===0);
				  v0.copy(shape.pillarConvex.vertices[0]);
				  v1.copy(shape.pillarConvex.vertices[1]);
				  v2.copy(shape.pillarConvex.vertices[2]);
				  v0.vadd(shape.pillarOffset, v0);
				  v1.vadd(shape.pillarOffset, v1);
				  v2.vadd(shape.pillarOffset, v2);
				  geometry.vertices.push(
					  new THREE.Vector3(v0.x, v0.y, v0.z),
					  new THREE.Vector3(v1.x, v1.y, v1.z),
					  new THREE.Vector3(v2.x, v2.y, v2.z)
				  );
				  var i = geometry.vertices.length - 3;
				  geometry.faces.push(new THREE.Face3(i, i+1, i+2));
			  }
		  }
		}
		geometry.computeBoundingSphere();
		geometry.computeFaceNormals();
		Graphics._AssignUVs(geometry);
		return geometry;
	},
	Terrain(hfBody,x,y,material,geo){
		var geometry = geo;
		mesh = new THREE.Mesh(geometry, material);
		mesh.position.copy(hfBody.position);
		mesh.quaternion.copy(hfBody.quaternion);
		scene.add( mesh );
		hfBody.mesh = mesh;
	},
	Init(rnd){
		MaterialsFactory.Init(rnd);
		
		camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

		scene = new THREE.Scene();
		scene.fog = new THREE.Fog( 0x88AAFF, 0, 100 );

		var ambient = new THREE.AmbientLight( 0xAAAAAA );
		scene.add( ambient );
		
		light = new THREE.SpotLight( 0xffffff );
		light.position.set( 10, 30, 20 );
		light.target.position.set( 0, 0, 0 );
		scene.add( light );
		
		controls = new PointerLockControls( camera , sphereBody );
		scene.add( controls.getObject() );
		
		renderer = new THREE.WebGLRenderer();
		renderer.setSize( window.innerWidth, window.innerHeight );
		renderer.setClearColor( scene.fog.color, 1 );
		
		document.body.appendChild( renderer.domElement );

		window.addEventListener( 'resize', Graphics.OnWindowResize, false );		
	},
	OnWindowResize() {
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
	}	
}
