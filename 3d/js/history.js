var MaxYear = 1000;

var HistoryTextMaker = {
	Disasters : [
		'A volcanic eruption',
		'A earthquake',
		'A meteorite strike',
		'A nerarby supernova',
		'A great flood',
		'A great drought',
		'A plague',
		'A climate change',
	],
	Technologies1:[
		'agriculture',
		'craftsmanship',
		'metallurgy',
		'weaponry'
	],
	Technologies2:[
		'architecture',
		'navigation',
		'education',
		'medicine'
	],
	Technologies3:[
		'industry',
		'transportation',
		'chemistry',
		'phisics'
	],
	Technologies4:[
		'computers',
		'electronics',
		'comunications',
		'politics'
	],	
	GetTechnology : function(year,MaxYear){
		var tech = HistoryTextMaker.Technologies1;
		if(year>=MaxYear/4){
			tech = tech.concat(HistoryTextMaker.Technologies2);
		}
		if(year>=MaxYear/2){
			tech = tech.concat(HistoryTextMaker.Technologies3);
		}
		if(year>=MaxYear/4*3){
			tech = tech.concat(HistoryTextMaker.Technologies4);
		}
		return tech;
	},	
	ReadLog : function(log){
		var result = "";
		for (var i=0; i < log.length; i++) {
			result += "<p>"+HistoryTextMaker.EventText(log[i]) + "</p>";
		}
		return result;
	},	
	ReadCountries : function(countries){
		var result = "";
		for (var i in countries) {
			result += "<p>"+Country.Write(countries[i]) + "</p>";
		}
		return result;
	},
	EventText : function(event){
		var result = event.year + ": ";
		switch(event.type){			
			case HistoryEvents.Type.CivilWar : 
				if(event.newlands.length==0){
					result+= `A rebellion is suffocated in ${event.country.Name}`;
				}
				else if (event.newlands.length==1){
					result+= `A rebellion succeeds in ${event.country.Name}, now to be called ${event.newlands[0].Name}`;
				}
				else{
					result+= `A civil war breaks ${event.country.Name} into smaller nations: ${event.newlands.map(x=>x.Name).join(', ')}`;
				}		
				break;
			case HistoryEvents.Type.War : 
				result+= `${event.country.Name} invades ${event.other.Name} `;
				if(event.win){
					if(event.minor){
						result+= `earning a minor victory over it.`;
					}
					else{
						result+= `and conquers it.`;
					}
				}
				else{
					if(event.minor){
						result+= `but the invasion fails.`;
					}
					else{
						result+= `but the invasion fails and ${event.other.Name} conquers ${event.country.Name} in retaliation.`;
					}
				}
				break;
			case HistoryEvents.Type.PowerDown : 
				result+= `${event.country.Name} suffers a famine.`;
				break;
			case HistoryEvents.Type.PowerUp : 
				result+= `${event.country.Name} enjoys a golden age.`;
				break;
			case HistoryEvents.Type.Aliance : 
				if(event.country == event.other){
					result+= `A population minority is assimilated in ${event.country.Name}.`;
				}
				else if(event.former){
					result+= `${event.country.Name} and ${event.other.Name} sign a peace treaty.`;
				}
				else{
					result+= `${event.country.Name} and ${event.other.Name} sign an alliance.`;
				}
				break;
			case HistoryEvents.Type.Treason : 
				if(event.country == event.other){
					result+= `Genocide of a population minority in ${event.country.Name}.`;
				}
				else if(event.former){
					result+= `${event.country.Name} breaks its alliance with ${event.other.Name}.`;
				}
				else{
					result+= `${event.country.Name} threatens to destroy ${event.other.Name}.`;
				}
				break;
			case HistoryEvents.Type.Merge : 
				if(event.self){
					result+= `${event.country.Name} establishes a colony.`;
				}
				else{
					result+= `${event.merged.Name} is peacefully assimilated into ${event.country.Name}.`;
				}
				break;
			case HistoryEvents.Type.Disaster : 
				result+= `${event.detail} causes considerable devastation to all countries.`;
				break;
			case HistoryEvents.Type.Technology : 
				result+= `All countries benefit from an improvement in ${event.detail}.`;
				break;
		}		
		if(event.intervention){
			result+=HistoryTextMaker.Intervention(event);
		}		
		return result;
	},
	Intervention : function(events){
		return " INTERVENTION";
	}
}


var HistoryEvents = {
	
	_Log : [],
	_Countries : {},
	_HistoryCountries : {},
	
	Type : {
		CivilWar : 0,
		War : 1,
		PowerDown : 2,
		PowerUp : 3,
		Aliance : 4,
		Treason : 5,
		Merge : 6,
		Disaster : 7,
		Technology : 8,
	},
	Log : function(log){
		this._Log[this._Log.length-1] = {...this._Log[this._Log.length-1], ...log};
	},
	ForeignIntervention(rnd,c1,c2){
		if(Random.Chance(rnd,2)) return 0;
		
		var val = 0;
		if(Object.keys(c1.Allies).length>0){
			var ally = Country.Pick(rnd,c1.Allies);
			HistoryEvents.Log({intervention:true,foreignAllies : ally});
			val += ally.Power;
		}
		if(Object.keys(c1.Enemies).length>0){
			var enemy = Country.Pick(rnd,c1.Enemies);
			HistoryEvents.Log({intervention:true,foreignEnemies : enemy});
			val -= enemy.Power;
		}		
		return val;
	},	
	CivilWar : function(rnd,country){
		var year = this._Log[this._Log.length-1].year;
		var log = {newlands:[]};		
		HistoryEvents.PowerDown(rnd,country);
		var intervened = HistoryEvents.ForeignIntervention(rnd,country,country);
		if(intervened>0 || Random.Chance(rnd,2)){
			var parts = Random.Range(rnd,1,4);
			var powers = Random.NumberParts(rnd,country.Power,parts,.5);
			for (var i=0; i < parts; i++) {
				var newcountry = HistoryEvents.AddCountry(rnd,powers[i],year);
				log.newlands.push(newcountry);
			}
			Country.Remove(country,this._Countries,year);			
		}
		HistoryEvents.Log(log);
	},
	War : function(rnd,country){	
		var c2 = Country.Pick(rnd,this._Countries);
		if(country.Allies[c2.Name]){
			this._Log[this._Log.length-1].type = HistoryEvents.Type.Treason; //needed?
			HistoryEvents.Treason(rnd,country,c2);
			return;
		}
		if(c2==country){
			this._Log[this._Log.length-1].type = HistoryEvents.Type.CivilWar;
			HistoryEvents.CivilWar(rnd,country);
			return;
		}
		
		var intervened = HistoryEvents.ForeignIntervention(rnd,country,c2);
		var log = {other:c2,minor:false,win:false};		
		if(country.Power+intervened>c2.Power*2){
			log.win = true;
			HistoryEvents.Merge(rnd,country,c2);
		}
		else if((country.Power+intervened)*2<=c2.Power){
			HistoryEvents.Merge(rnd,c2,country);
		}
		else if(country.Power>c2.Power){
			log.minor = true;
			country.Enemies[c2.Name] = c2;
			c2.Enemies[country.Name] = country;
			HistoryEvents.PowerDown(rnd,c2);
		}
		else if(country.Power<=c2.Power){
			log.minor = true;
			log.win = true;
			country.Enemies[c2.Name] = c2;
			c2.Enemies[country.Name] = country;
			HistoryEvents.PowerDown(rnd,country);
		}
		HistoryEvents.Log(log);
		//foreign intervention, allies, allies of allies, enemies
		
	},
	PowerDown : function(rnd,country){
		country.Power *= 0.5;
	},
	PowerUp : function(rnd,country){
		country.Power *= 10;
	},
	Aliance : function(rnd,country,c2=null){
		c2 = c2==null? Country.Pick(rnd,this._Countries) : c2;	
		var log = {other:c2};
		if(c2==country){
			HistoryEvents.PowerUp(rnd,country);
			HistoryEvents.Log(log);
			return;
		}		
		if(country.Enemies[c2.Name]){
			log.former = true;
			delete country.Enemies[c2.Name];
			delete c2.Enemies[country.Name];
		}
		country.Allies[c2.Name] = c2;
		c2.Allies[country.Name] = country;
		HistoryEvents.Log(log);
	},
	Treason : function(rnd,country,c2=null){
		c2 = c2==null? Country.Pick(rnd,this._Countries) : c2;	
		var log = {other:c2};
		if(c2==country){
			HistoryEvents.PowerDown(rnd,country);
			HistoryEvents.Log(log);
			return;
		}
		if(country.Allies[c2.Name]){
			log.former = true;
			delete country.Allies[c2.Name];
			delete c2.Allies[country.Name];
		}
		country.Enemies[c2.Name] = c2;
		c2.Enemies[country.Name] = country;
		HistoryEvents.Log(log);
	},
	Merge : function(rnd,country,c2=null){
		c2 = c2==null? Country.Pick(rnd,this._Countries) : c2;	
		if(c2==country){
			HistoryEvents.PowerUp(rnd,country);
			HistoryEvents.Log({self:true});
			return;
		}		
		country.Power += c2.Power;
		country.Allies = {...country.Allies, ...c2.Allies };
		country.Enemies = {...country.Enemies, ...c2.Enemies };
		Country.Remove(c2,this._Countries,this._Log[this._Log.length-1].year);
		HistoryEvents.Log({merged:c2});
	},
	Disaster : function(rnd,country){
		for(var i=0; i < this._Countries.length; i++){
			HistoryEvents.PowerDown(rnd,counties[i]);
		}
		HistoryEvents.Log({detail:Random.Choice(rnd,HistoryTextMaker.Disasters)});
	},
	Technology : function(rnd,country,MaxYear){
		for(var i=0; i < this._Countries.length; i++){
			HistoryEvents.PowerUp(rnd,counties[i]);
		}
		HistoryEvents.Log({detail:Random.Choice(rnd,HistoryTextMaker.GetTechnology(this._Log[this._Log.length-1].year,MaxYear))});
	},
	Random : function(rnd,year,MaxYear){		
		var country = Country.Pick(rnd,this._Countries);	
		var action = Random.WChoice(rnd,[
			{w:15,v:HistoryEvents.Type.CivilWar},
			{w:10,v:HistoryEvents.Type.PowerDown},
			{w:5,v:HistoryEvents.Type.PowerUp},
			{w:3,v:HistoryEvents.Type.Disaster},
			{w:18,v:HistoryEvents.Type.Aliance},
			{w:5,v:HistoryEvents.Type.Treason},
			{w:20,v:HistoryEvents.Type.War},
			{w:20,v:HistoryEvents.Type.Technology},
			{w:4,v:HistoryEvents.Type.Merge},
		]);		
		
		HistoryEvents._Log.push({type:action,year:year,country:country});
		
		switch(action){			
			case HistoryEvents.Type.CivilWar : 
				HistoryEvents.CivilWar(rnd,country);
				break;
			case HistoryEvents.Type.War : 
				HistoryEvents.War(rnd,country);
				break;
			case HistoryEvents.Type.PowerDown : 
				HistoryEvents.PowerDown(rnd,country);
				break;
			case HistoryEvents.Type.PowerUp : 
				HistoryEvents.PowerUp(rnd,country);
				break;
			case HistoryEvents.Type.Aliance : 
				HistoryEvents.Aliance(rnd,country);
				break;
			case HistoryEvents.Type.Treason : 
				HistoryEvents.Treason(rnd,country);
				break;
			case HistoryEvents.Type.Merge : 
				HistoryEvents.Merge(rnd,country);
				break;
			case HistoryEvents.Type.Disaster : 
				HistoryEvents.Disaster(rnd,country);
				break;
			case HistoryEvents.Type.Technology : 
				HistoryEvents.Technology(rnd,country,MaxYear);
				break;
		}		
	},	
	AddCountry(newcountry,power,year){
		var newcountry = Country.Country(rnd,100,year);
		this._Countries[newcountry.Name] = newcountry;
		this._HistoryCountries[newcountry.Name] = newcountry;
		return newcountry;
	},
	Tick : function(rnd,MaxYear){		
		var year = 0;
		HistoryEvents.AddCountry(rnd,100,year);
		do{			
			year += Random.Range(rnd,0,100);
			HistoryEvents.Random(rnd,year,MaxYear);			
		} while(year<MaxYear);
		
		return {
			Log:this._Log,
			Countries:this._Countries,	
			HistoryCountries:this._HistoryCountries			
		}
	}
}

