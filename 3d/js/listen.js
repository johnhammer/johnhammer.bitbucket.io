var Listen = {
	Listen(world,content,from,to,log=true){
		if(log){
			Listen.WriteLine(world,from,to,content);
		}
		if(content.IsQuestion){
			content.IsQuestion = false;
			Listen.Listen(world,content,to,from,false);
			return;
		}	

		var neuron = to[content.Topic] ? to[content.Topic][content.Object] : null;
		var value;		
		if(neuron){
			if(content.IsFact){	
				Listen.KnownFact(content,world,to);	
			}
			else{
				Listen.KnownOpinion(content,world,to);
			}
		}
		else{			
			if(content.IsFact){
				Listen.UnknownFact(content,world,to);			
			}
			else{
				Listen.UnknownOpinion(content,world,to);
			}			
		}
	},
	KnownOpinion(content,world,to){
		
	},
	KnownFact(content,world,to){
		to[content.Topic][content.Object].Knowledge++;
	},
	UnknownOpinion(content,world,to){
		var opinion = content.Value;	
		if(content.Topic == "Relations"){
			Brain.Relation(to,world.People[content.Object].Brain,opinion,opinion);
		}
		to[content.Topic][content.Object] = {
			Opinion:opinion,
			Knowledge:0			
		};
	},
	UnknownFact(content,world,to){
		var opinion = 5;	
		if(content.Topic == "Relations"){
			Brain.Relation(to,world.People[content.Object].Brain,opinion,opinion);
		}
		to[content.Topic][content.Object] = {
			Opinion:opinion,
			Knowledge:1
		};
	},
	
	WriteLine(world,from,to,line){
		var verb = "tells";
		if(line.IsQuestion){
			verb = "asks";
		}
		if(!line.IsFact){
			verb += " opinion";
		}
		
		var subject = "";
		if(from.Id == "God"){
			subject = "GOD";
			return;
		}
		else if(from.Id == to.Id){
			subject = to.Name+" monologue";
			return;
		}
		else{
			subject = `${from.Name} ${verb} to ${to.Name}`
		}
		
		var value = line.Topic=="Relations" ? world.People[line.Object].Name : line.Object
		return console.log(`${line.Topic} ${subject} ${value}`);
	},
}