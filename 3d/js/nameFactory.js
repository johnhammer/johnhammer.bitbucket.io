var NameFactory = {
	//person
	PersonNamePrefix : [
		'ka','jo','al','to','an','as','ro','ai'
	],
	PersonNameSuffix : [
		'var','nas','ber','fred','tor','que','nio','ton'
	],
	PersonPatronymSuffix : [
		'ovich','son','ez','sen','ov','ian','fi','atos'
	],
	PersonGenderSuffix : [
		'na','noa','la','ta','sandra','ria','riana','ana'
	],	
	PersonName(rnd,isMale){
		var name = Random.Choice(rnd,NameFactory.PersonNamePrefix) + Random.Choice(rnd,NameFactory.PersonNameSuffix);
		if(!isMale){
			name = Random.Choice(rnd,NameFactory.PersonNamePrefix) + Random.Choice(rnd,NameFactory.PersonGenderSuffix);
		}		
		name = LIB.Capitalize(name);
		return name;
	},
	PersonSurname(rnd){
		var name = NameFactory.PersonName(rnd,true) + Random.Choice(rnd,NameFactory.PersonPatronymSuffix);
		return name;
	},
	//nature
	PlantName(rnd){
		var isMale = Random.Chance(rnd,2);
		var name = NameFactory.PersonName(rnd,isMale);
		if(isMale){
			return name+"ii";
		}
		else{
			return name+"e";
		}
	},
	TreeName(rnd){
		return "T. "+NameFactory.PlantName(rnd);
	},
	BushName(rnd){
		return "B. "+NameFactory.PlantName(rnd);
	},
	HerbName(rnd){
		return "H. "+NameFactory.PlantName(rnd);
	},
	RockName(rnd){
		var isMale = Random.Chance(rnd,2);
		var name = NameFactory.PersonName(rnd,isMale);
		if(isMale){
			return name+"site";
		}
		else{
			return name+"ite";
		}
	},
	
}