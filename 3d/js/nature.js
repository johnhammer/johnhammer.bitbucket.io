var Nature = {
	CreateTree(rnd){
		return {
			Name : NameFactory.TreeName(rnd),
			r:Random.FloatRange(rnd,.5,1),
            l:Random.FloatRange(rnd,1,5),
            rf:Random.FloatRange(rnd,.4,.6),
            lf:Random.FloatRange(rnd,.5,.7),
			ilim:Random.Range(rnd,3,4),
			af:Random.Range(rnd,35,55),
			bf:Random.Range(rnd,2,5),
			colors:{
				b1:ColorFactory.Birch(rnd),
				b2:ColorFactory.Birch(rnd),
				l1:ColorFactory.Leaf(rnd),
				l2:ColorFactory.Leaf(rnd),
				f1:Random.FloatRange(rnd,1.32,1.32),  //1.32
				f2:Random.FloatRange(rnd,3.5,4.0),  //4.0
				f3:Random.FloatRange(rnd,0,1.5),  //1.5
			}
		};		
	},
	CreateBush(rnd){
		return {
			Name : NameFactory.BushName(rnd),
			r:Random.FloatRange(rnd,.1,.2),
            l:2.2,
            rf:Random.FloatRange(rnd,.4,.6),
            lf:Random.FloatRange(rnd,.5,.7),
			ilim:0,
			af:Random.Range(rnd,35,55),
			bf:Random.Range(rnd,5,15),
			colors:{
				b1:ColorFactory.Birch(rnd),
				b2:ColorFactory.Birch(rnd),
				l1:ColorFactory.Leaf(rnd),
				l2:ColorFactory.Leaf(rnd),
				f1:Random.FloatRange(rnd,1.32,1.32),  //1.32
				f2:Random.FloatRange(rnd,3.5,4.0),  //4.0
				f3:Random.FloatRange(rnd,0,1.5),  //1.5
			}
		};
	},
	CreateHerb(rnd){
		return {
			Name : NameFactory.HerbName(rnd),
			LeafColor : ColorFactory.Leaf(rnd),
		};
	},
	CreateRock(rnd){
		return {
			Name : NameFactory.RockName(rnd),
			Color : ColorFactory.Concrete(rnd),
		};
	},
	Items(rnd,amtTrees,f){
		var treesDist = Random.NumberParts(rnd,100,amtTrees,1);		
		var trees = [];
		for(var i=0;i<treesDist.length;i++){
			var tree = f(rnd);
			tree.Distribution = treesDist[i];
			trees.push({w:treesDist[i],v:tree});
		}
		return trees;
	},
	Create(rnd){
		var amtTrees = Random.Range(rnd,2,5);
		var amtBushes = Random.Range(rnd,2,5);
		var amtHerbs = Random.Range(rnd,2,5);
		var amtRocks = Random.Range(rnd,2,5);
		
		return {
			Trees : Nature.Items(rnd,amtTrees,Nature.CreateTree),
			Bushes : Nature.Items(rnd,amtBushes,Nature.CreateBush),
			Herbs : Nature.Items(rnd,amtHerbs,Nature.CreateHerb),
			Rocks : Nature.Items(rnd,amtRocks,Nature.CreateRock),
		}
	},
}