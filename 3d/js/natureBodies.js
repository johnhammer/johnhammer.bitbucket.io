var NatureBodies = {
	Rock(ox,oy,heightmap){
		var halfExtents = new CANNON.Vec3(.5,.5,.5);
		var boxShape = new CANNON.Box(halfExtents);
		var boxGeometry = new THREE.BoxGeometry(halfExtents.x*2,halfExtents.y*2,halfExtents.z*2);
		
		var sz = halfExtents.x;
		var geo = new THREE.DodecahedronGeometry(sz, 1);
		var size = 1;
		geo.vertices.forEach(function(v){
			v.x += (0-Math.random()*(size/4));
			v.y += (0-Math.random()*(size/4));
			v.z += (0-Math.random()*(size/4));
		});
		for (var i = 0; i < geo.faces.length; i++) {
			face = geo.faces[i];
			face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
			face.faceid = i;
		} 
		geo.computeFaceNormals();
		
		var material =new THREE.MeshPhongMaterial({}) 
		var boxMesh = new THREE.Mesh( geo, material );
		
		return NatureBodies.Block(ox,oy,heightmap,boxShape,boxMesh);
	},
	Tree(ox,oy,heightmap,rnd){
		var halfExtents = new CANNON.Vec3(0.1,0.1,0.1);
		var boxShape = new CANNON.Box(halfExtents);
		
		var rnd = Random.Create();
		var type = Random.WChoice(rnd,universe.Nature.Trees);
		
		var options = type;
				
		var groupMesh= TreeFactory.Tree(rnd,options);
		
		return NatureBodies.Block(ox,oy,heightmap,boxShape,groupMesh);
	},
	Bush(ox,oy,heightmap,rnd){
		var halfExtents = new CANNON.Vec3(0.1,0.1,0.1);
		var boxShape = new CANNON.Box(halfExtents);
		
		var rnd = Random.Create();
		var type = Random.WChoice(rnd,universe.Nature.Bushes);
		
		var options = type;
				
		var groupMesh= TreeFactory.Tree(rnd,options);
		
		return NatureBodies.Block(ox,oy,heightmap,boxShape,groupMesh);
	},
	Leaf(ox,oy,heightmap,rnd){
		var halfExtents = new CANNON.Vec3(0.1,0.1,0.1);
		var boxShape = new CANNON.Box(halfExtents);
		
		var boxGeometry = new THREE.PlaneGeometry(1,1);
		
		var rnd = Random.Create();
		var options = Random.WChoice(rnd,universe.Nature.Bushes);
		
		var material = MaterialsFactory.TreeBranch(
			options.colors.l1,
			options.colors.l2,
			options.colors.f1,
			options.colors.f2,
			options.colors.f3);
		
		var groupMesh = new THREE.Mesh( boxGeometry, material ); 
		groupMesh.rotateX(Math.PI/2);		
		
		var x = (Math.random())*(Engine.ChunkSize-1);
		var z = (Math.random())*-(Engine.ChunkSize-1);
		var y = TerrainFactory.GetTerrainHeightAt(x*Engine.ChunkSize,-z*Engine.ChunkSize,heightmap,Engine.ChunkSize) + .1;		
		var newx = x+(Engine.ChunkSize-1)*ox
		var newz = z+(Engine.ChunkSize-1)*oy
		
		scene.add(groupMesh);
		groupMesh.position.set(newx,y,newz);
		
		return groupMesh;
	},
	BoxMesh(x,y,z,fx,fy,fz,material){
		var boxGeometry = new THREE.BoxGeometry(fx,fy,fz);
		var boxMesh = new THREE.Mesh( boxGeometry, material );
		boxMesh.position.set(x+fx/2,y+fy/2,z-fz/2);
		
		scene.add(boxMesh);
		return boxMesh;
	},	
	Block(ox,oy,heightmap,boxShape,boxMesh){				
		var x = (Math.random())*(Engine.ChunkSize-1);
		var z = (Math.random())*-(Engine.ChunkSize-1);
		var y = TerrainFactory.GetTerrainHeightAt(x*Engine.ChunkSize,-z*Engine.ChunkSize,heightmap,Engine.ChunkSize);		
		var newx = x+(Engine.ChunkSize-1)*ox
		var newz = z+(Engine.ChunkSize-1)*oy
		
		var boxBody = new CANNON.Body({ mass: 0 });
		boxBody.addShape(boxShape);
		world.add(boxBody);
		scene.add(boxMesh);
		boxBody.position.set(newx,y,newz);
		boxMesh.position.set(newx,y,newz);
		boxBody.mesh = boxMesh;
		
		return boxBody;
	},	
	
}