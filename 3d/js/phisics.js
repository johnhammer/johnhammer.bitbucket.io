var Physics = {
	Terrain(matrix,cx,cy){
		var sizeX = matrix.length-2,
			sizeY = matrix[0].length-2;
		var hfShape = new CANNON.Heightfield(matrix, {
			elementSize: 1
		});
		hfBody = new CANNON.Body({ mass: 0 });
		hfBody.addShape(hfShape);
		hfBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), THREE.Math.degToRad(-90));
		hfBody.position.set(cx*sizeX, 0, cy*sizeY);
		world.add(hfBody);
		
		return hfBody;
	},
	Player(x,y,h){
		var mass = 100, radius = 1;
		var sphereShape = new CANNON.Sphere(radius);
		sphereBody = new CANNON.Body({ mass: mass });
		sphereBody.addShape(sphereShape);
		sphereBody.position.set(x,h,y);
		//sphereBody.linearDamping = 0.9;
		world.add(sphereBody);
	},
	Init(){
		world = new CANNON.World();
		world.gravity.set(0,-20,0);		
	}
}
