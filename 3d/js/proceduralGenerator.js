var Worley = {
	Seed :42,
    xorshift(value) {
        let x = value ^ (value >> 12);
        x = x ^ (x << 25);
        x = x ^ (x >> 27);
        return x * 2;
    },
    hash(i, j, k) {
        return (((((2166136261 ^ i) * 16777619) ^ j) * 16777619) ^ k) * 16777619 & 0xffffffff;
    },
    d(p1, p2) {
        return [p1.x - p2.x, p1.y - p2.y, p1.z - p2.z];
    },
	EuclideanDistance(p1, p2) {
        return Worley.d(p1, p2).reduce((sum, x) => sum + (x * x), 0);
    },
    ManhattanDistance(p1, p2) {
        return Worley.d(p1, p2).reduce((sum, x) => sum + Math.abs(x), 0)
    },
    probLookup(value) {
        value = value & 0xffffffff;
        if (value < 393325350) return 1;
        if (value < 1022645910) return 2;
        if (value < 1861739990) return 3;
        if (value < 2700834071) return 4;
        if (value < 3372109335) return 5;
        if (value < 3819626178) return 6;
        if (value < 4075350088) return 7;
        if (value < 4203212043) return 8;
        return 9;
    },

    insert(arr, value) {
        let temp;
        for (let i = arr.length - 1; i >= 0; i--) {
            if (value > arr[i]) break;
            temp = arr[i];
            arr[i] = value;
            if (i + 1 < arr.length) arr[i + 1] = temp;
        }
    },

    noise(input, distanceFunc) {
        let lastRandom,
            numberFeaturePoints,
            randomDiff = { x: 0, y: 0, z: 0 },
            featurePoint = { x: 0, y: 0, z: 0 };
        let cubeX, cubeY, cubeZ;
        let distanceArray = [9999999, 9999999, 9999999];

        for (let i = -1; i < 2; ++i)
            for (let j = -1; j < 2; ++j)
                for (let k = -1; k < 2; ++k) {
                    cubeX = Math.floor(input.x) + i;
                    cubeY = Math.floor(input.y) + j;
                    cubeZ = Math.floor(input.z) + k;
                    lastRandom = Worley.xorshift(
                        Worley.hash(
                            (cubeX + Worley.Seed) & 0xffffffff,
                            (cubeY) & 0xffffffff,
                            (cubeZ) & 0xffffffff
                        )
                    );
                    numberFeaturePoints = Worley.probLookup(lastRandom);
                    for (let l = 0; l < numberFeaturePoints; ++l) {
                        lastRandom = Worley.xorshift(lastRandom);
                        randomDiff.X = lastRandom / 0x100000000;

                        lastRandom = Worley.xorshift(lastRandom);
                        randomDiff.Y = lastRandom / 0x100000000;

                        lastRandom = Worley.xorshift(lastRandom);
                        randomDiff.Z = lastRandom / 0x100000000;

                        featurePoint = {
                            x: randomDiff.X + cubeX,
                            y: randomDiff.Y + cubeY,
                            z: randomDiff.Z + cubeZ
                        };
                        Worley.insert(distanceArray, distanceFunc(input, featurePoint));
                    }
                }

        var n = distanceArray.map(x => x < 0 ? 0 : x > 1 ? 1 : x );
		return Math.floor(255 * (n[1] - n[0]));
    },

    Euclidean(x, y, z) {
        return this.noise({ x:x, y:y, z:z }, Worley.EuclideanDistance);
    },
	Manhattan(x, y, z) {
        return this.noise({ x:x, y:y, z:z }, Worley.ManhattanDistance);
    }
}




var ProceduralGenerator = {
	GenerateTexture(w,h,ox,oy,scale,f){	
		var canvas = document.createElement('canvas'),
		ctx = canvas.getContext('2d');
		canvas.width = w;
		canvas.height = h;
		var image = ctx.createImageData(w, h);
		var data = image.data;
		for (var i = 0; i < h; i++) {
			for (var j = 0; j < w; j++) {
				var x = (i+ox)*scale;
				var y = (j+oy)*scale;				
				var cell = (i + j * w) * 4;				
				var value = f(x,y);							
				data[cell] = value[0];
				data[cell + 1] = value[1];
				data[cell + 2] = value[2];
				data[cell + 3] = value[3]; 
			}
		}
		ctx.putImageData(image, 0, 0); 
		var texture = new THREE.Texture(canvas);
		texture.needsUpdate = true;
		return texture;
	},	
	_Perlin(x,y,grain,h){
		return Math.abs(noise.perlin2(x/grain,y/grain))* h;
	},
	_PerlinTerrain(x,y,data){
		var val = 0;
		for (var i = 0; i < data.length; i++) {
			val+=ProceduralGenerator._Perlin(x,y,data[i].g,data[i].h);
		}
		return val;
	},	
	SineTexture(x,y){		
		//xPeriod and yPeriod together define the angle of the lines
		//xPeriod and yPeriod both 0 ==> it becomes a normal clouds or turbulence pattern
		var xPeriod = 5.0; //defines repetition of marble lines in x direction
		var yPeriod = 10.0; //defines repetition of marble lines in y direction
		//turbPower = 0 ==> it becomes a normal sine pattern
		var turbPower = 80.0; //makes twists
		var turbSize = 1.0; //initial size of the turbulence

		var xyValue = x * xPeriod / 256 + y * yPeriod / 256 + turbPower * turbulence(x, y, turbSize) / 256.0;
		var sineValue = Math.abs(Math.sin(xyValue * 3.14159));
		return sineValue;
	},
	GrayscaleTexture(x,y){
		var t = [{g:5,h:1}];
	
		var v = ProceduralGenerator._PerlinTerrain(x,y,t)*500

		return [
			v,
			v,
			v,
			255
		];
	},
	
	FractalPointMandel(x0,y0){ 
		/*
		//X scale (-2.5, 1)) Y scale (-1, 1))
		var value = ProceduralGenerator.FractalPoint(x*0.0002-1.5,(y)*0.0002)*8
		return [value,value,value,255];
		*/
		var x = 0.0;
		var y = 0.0;
		var iteration = 0;
		var max_iteration = 1000;
		
		var rsquare = 0
		var isquare = 0
		var zsquare = 0
		while (rsquare + isquare <= 4  &&  iteration < max_iteration) {
		  x = rsquare - isquare + x0
		  y = zsquare - rsquare - isquare + y0
		  rsquare = x*x
		  isquare = y*y
		  zsquare = (x + y)*(x + y)
		  iteration = iteration + 1
		}	
		
		return zsquare;
		return {r:zsquare,g:rsquare,b:isquare};
	},
	MandelBox(x0,y0,fixZ=1){ //X scale (-2.5, 1)) Y scale (-1, 1))
		/*
		var value = ProceduralGenerator.MandelBox(x/100,y/100,0)
		return [
			value[0]*255,
			value[1]*.09,
			value[2]*.5,
			255
		];	
		*/
		var fixedradius=1.5;   
		var minradius=0.05;    // lowwering this makes the core more detailed
		var scale=-1.5;        // zooms in or out (doesn't scale  the quality settings like depth/escape/detail)
		var escape=0.55;       // how deep should we search the complex
		var depth=1000;          //how many itteratins should we run before givving up
		var RADIUSRATIO = Math.sqrt(fixedradius) / Math.sqrt(minradius);
		function iterate(vector){	
		 
			if(vector.x > 1){ vector.x = 2 - vector.x; } else if(vector.x < -1){ vector.x = -2 - vector.x; }
			if(vector.y > 1){ vector.y = 2 - vector.y; } else if(vector.y < -1){ vector.y = -2 - vector.y; }
			if(vector.z > 1){ vector.z = 2 - vector.z; } else if(vector.z < -1){ vector.z = -2 - vector.z; }


			var len = vector.length();
			if(len < minradius){
				vector.multiplyScalar(RADIUSRATIO);
			}
			else if(len < fixedradius){
				vector.addScalar(Math.sqrt(fixedradius) / Math.sqrt(len));
			}
			vector.multiplyScalar(scale);
		}				
		var point = new THREE.Vector3(x0,y0,fixZ);
		var px = x0;
		var py = y0;
		var pz = point.z;
		for(var x = depth; x > 0;  x--){
			iterate(point);

			point.x = point.x + px;
			point.y = point.y + py;
			point.z = point.z + pz;

			var l = point.length();
			if(escape > point.length()){
			  return [l,x,x*l];
			}
		}
		return [1*255,1*255,1*255,255];
	},
	FractalTexture(x,y){
		var value = ProceduralGenerator.MandelBox(x/1000,y/10000,0)
		return [
			value[0]*500,
			value[1]*.09,
			value[2]*.5,
			255
		];	
	},
}

function turbulence(x, y, size)
{
	
  var value = 0.0, initialSize = size;

  while(size >= 1)
  {
    value += ProceduralGenerator._Perlin(x / size, y / size,5,5) * size;
    size--;
  }

  return(value / initialSize);
}