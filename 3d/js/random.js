var _Random = {
  GetSeed : function(seed,a,b){
    return a*123 + seed*456 + b*789;
  },
  RunN : function(range,callback){
    var result = [];
    for(var i=0;i<range;i++){
      result.push(callback());
    }
    return result;
  }
}

var Random = {
  Create : function(seed) {
    obj = {}
    // LCG using GCC's constants
    obj.m = 0x80000000; // 2**31;
    obj.a = 1103515245;
    obj.c = 12345;

    obj.state = seed ? seed : Math.floor(Math.random() * (obj.m - 1));
    return obj;
  },
  MultiSeed : function(seed,a,b){
    return Random.Create(_Random.GetSeed(seed,a,b));
  },
  MultiSeed2 : function(seed,a,b,c,d){    
    return Random.MultiSeed(seed,_Random.GetSeed(seed,a,c),_Random.GetSeed(seed,b,d));
  },
  Int : function(obj) {
    obj.state = (obj.a * obj.state + obj.c) % obj.m;
    return obj.state;
  },
  Bool : function(obj){
    return Random.Range(obj,0,1);
  },
  Float : function(obj) {
    return Random.Int(obj) / (obj.m - 1);
  },
  Range : function(obj,start,end) {
    var rangeSize = end+1 - start;
    var randomUnder1 = Random.Int(obj) / obj.m;
    return start + Math.floor(randomUnder1 * rangeSize);
  },
  FloatRange : function(obj,start,end) {
    return Random.Float(obj) * (end - start) + start;
  },
  Chance : function(obj,chance){
    var val = Random.Range(obj,1,chance);
    return val == chance;
  },
  ObjChoice : function(obj,object) {
    return object[Random.Choice(obj,Object.keys(object))];
  },
  Choice : function(obj,array) {
    return array[Random.Range(obj,0, array.length-1)];
  },
  WChoice : function(obj,array) {    
    var val = Random.Range(obj,0,100);
    var sum = 0;
    for(var i=0;i<array.length;i++){
      sum += array[i].w;
      if(val<=sum){
        return array[i].v;
      }
    }
  },
  Multichoice : function(obj,array,min,max){
    return _Random.RunN(Random.Range(obj,min,max),function(){return Random.Choice(obj,array)})
  },
  WMultichoice : function(obj,array,min,max){
    return _Random.RunN(Random.Range(obj,min,max),function(){return Random.WChoice(obj,array)})
  },
	NumberParts(obj,number,parts,min) {
		var randombit = number - min * parts;
		var out = [];
		for (var i=0; i < parts; i++) {
			out.push(Random.Float(obj));
		}
		var mult = randombit / out.reduce(function (a,b) {return a+b;});
		return out.map(function (el) { return el * mult + min; });
	},
	Distribute(rnd,length, amount) {
	  var arr = [];
	  for (var i = 0; i < length; i++) arr.push(0);
	  var i = 0;
	  var maxval = Math.floor(amount / length);
	  while (amount > 0) {
		if (Random.Chance(rnd,2)) {
		  var val = Random.Range(rnd,1, maxval);
		  arr[i] += val;
		  amount -= val;
		}
		i++;
		if (i >= length) i = 0;
	  }
	  return arr;
	}
}