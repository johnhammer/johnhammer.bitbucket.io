var Speak = {	
	GetRandomObject(rnd,brain,to,topic){
		var object;
		if(topic == "Relations"){
			object = Brain.RandomRelative(rnd,brain); //exclude self & god
			if(object == to.Id){
				object = null;
			}
		}
		else{
			object = Random.Choice(rnd,Object.keys(brain[topic]));	
		}	
		return object;
	},
	GetRandomOpinion(rnd){
		return Random.Range(rnd,0,10);
	},
	GetRandomLine(rnd,brain,to){	
		var topic = Random.Choice(rnd,Object.keys(brain).filter(x=>x!="Id"&&x!="Name"));		
		var object = Speak.GetRandomObject(rnd,brain,to,topic);	
		var isQuestion = Random.Chance(rnd,2);
		var isFact = Random.Chance(rnd,2);		
		if(object){
			var value = brain.Id=="God"?
				Speak.GetRandomOpinion(rnd):
				brain[topic][object].Opinion;
			
			if(isQuestion || isFact || value){
				return {
					IsQuestion : isQuestion,
					IsFact : isFact,
					Topic : topic,
					Object : object,
					Value : value
				};
			}
		}		
	},
	SpeakRandom(world,rnd,from,to){
		var content = Speak.GetRandomLine(rnd,from,to);
		if(content){
			Listen.Listen(world,content,from,to);
		}		
	}
}