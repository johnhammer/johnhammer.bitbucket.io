var TerrainFactory = {	
	GetTerrainHeightAt(xPos, yPos, heightmap, tilesize){
		
		var x = Math.floor(xPos / tilesize),
		y = Math.floor(yPos / tilesize),
				
		xPlusOne = x + 1,
		yPlusOne = y + 1,
				
		triY0 = heightmap[x][y],
		triY1 = heightmap[xPlusOne][y],
		triY2 = heightmap[x][yPlusOne],
		triY3 = heightmap[xPlusOne][yPlusOne],
				
		height = 0.0,
		sqX = (xPos / tilesize) - x,
		sqY = (yPos / tilesize) - y;
				
		if ((sqX + sqY) < 1) {
			height = triY0;
			height += (triY1 - triY0) * sqX;
			height += (triY2 - triY0) * sqY;
		} else {
			height = triY3;
			height += (triY1 - triY3) * (1.0 - sqY);
			height += (triY2 - triY3) * (1.0 - sqX);
		}
		return height;
	},	
	GetHeightMap(oox,ooy,ChunkSize,f){
		var ox=oox*(ChunkSize-1);
		var oy=-ooy*(ChunkSize-1)
		var matrix = [];
		var sizeX = ChunkSize+1, sizeY = ChunkSize+1; //extra row anti clipping
		for (var i = 0; i < sizeX; i++) {
			matrix.push([]);
			for (var j = 0; j < sizeY; j++) {
				var val = f(i+ox,j+oy);
				matrix[i].push(val+1000); //0.035*ox+0.025*oy
			}
		}		
		return matrix;
	},
	GenericChunk(x,y,terrainFunction,material,objects = []){
		//terrain
		var heightmap = TerrainFactory.GetHeightMap(x,y,Engine.ChunkSize,terrainFunction);			
		var hfBody = Physics.Terrain(heightmap,x,y);		
		var geometry = Graphics.TerrainGeometry(hfBody,x,y);
		//graphics
		Graphics.Terrain(hfBody,x,y,material,geometry);			
		//return
		return {b:hfBody,t:1,o:objects};	
	},
	
	GetTownColor(x,y){
		var noised = Math.abs(noise.perlin2(x/2,y/2));	
		if(noised>=0 && noised<=0.05){
			return TerrainTypes.Town2;
		}
		else{
			return TerrainTypes.Town;
		}
		
		var nx = Math.floor(x/4)/2;
		var ny = Math.floor(y/4)/2;
		var noised = Math.abs(noise.perlin2(x/2,y/2));	
		var noisedArea = Math.abs(noise.perlin2(nx,ny));
		
		if(noisedArea>=0 && noisedArea<=0.3){
			if(noised>=0 && noised<=0.05){
				return TerrainTypes.Town;	
			}
			else if(noised>0.05 && noised<=0.2){
				return TerrainTypes.Town3;	
			}
			else{
				return TerrainTypes.Town4;
			}
		}
		else{
			if(noised>=0 && noised<=0.2){
				return TerrainTypes.Town2;
			}
			else{
				return TerrainTypes.Town;
			}
		}
	},
	GetColor(x,y){
		var shore = 0;
		var beachLimit = 50;
		var downtown = 300;
		var forestLimit = 600;
		
		if(x<shore){
			return TerrainTypes.Sea;
		}
		if(x>=shore && x<beachLimit){
			if(y%10==0){
				return TerrainTypes.BeachSubriver;
			}
			else{
				return TerrainTypes.Beach;
			}		
		}
		if(x==beachLimit){
			return TerrainTypes.BeachTransition;
		}
		if(x>beachLimit && x<downtown){
			if(y%10==0){
				return TerrainTypes.TownSubriver;
			}
			else{
				return TerrainFactory.GetTownColor(x,y);
			}
		}
		if(x==downtown){
			return TerrainTypes.TownSubriver;	
		}
		if(x>downtown && x<forestLimit){
			return TerrainFactory.GetTownColor(x,y);
		}
		if(x==forestLimit){
			return TerrainTypes.ForestLimit;	
		}
		if(x>forestLimit){
			//return TerrainTypes.Forest;	
			return TerrainTypes.Fractalia;
		}
	},	
	Chunk(x,y){
		return TerrainFactory.GetColor(x,y)(x,y);
	}
	
}