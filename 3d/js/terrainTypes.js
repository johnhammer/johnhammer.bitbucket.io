function MandelBox(x0,y0,fixZ=1){ //X scale (-2.5, 1)) Y scale (-1, 1))
	/*
	var value = ProceduralGenerator.MandelBox(x/100,y/100,0)
	return [
		value[0]*255,
		value[1]*.09,
		value[2]*.5,
		255
	];	
	*/
	var fixedradius=1.5;   
	var minradius=0.05;    // lowwering this makes the core more detailed
	var scale=-1.5;        // zooms in or out (doesn't scale  the quality settings like depth/escape/detail)
	var escape=0.55;       // how deep should we search the complex
	var depth=1000;          //how many itteratins should we run before givving up
	var RADIUSRATIO = Math.sqrt(fixedradius) / Math.sqrt(minradius);
	function iterate(vector){	
	 
		if(vector.x > 1){ vector.x = 2 - vector.x; } else if(vector.x < -1){ vector.x = -2 - vector.x; }
		if(vector.y > 1){ vector.y = 2 - vector.y; } else if(vector.y < -1){ vector.y = -2 - vector.y; }
		if(vector.z > 1){ vector.z = 2 - vector.z; } else if(vector.z < -1){ vector.z = -2 - vector.z; }


		var len = vector.length();
		if(len < minradius){
			vector.multiplyScalar(RADIUSRATIO);
		}
		else if(len < fixedradius){
			vector.addScalar(Math.sqrt(fixedradius) / Math.sqrt(len));
		}
		vector.multiplyScalar(scale);
	}				
	var point = new THREE.Vector3(x0,y0,fixZ);
	var px = x0;
	var py = y0;
	var pz = point.z;
	for(var x = depth; x > 0;  x--){
		iterate(point);

		point.x = point.x + px;
		point.y = point.y + py;
		point.z = point.z + pz;

		var l = point.length();
		if(escape > point.length()){
		  return 1;
		}
	}
	return 0;
}




var TerrainTypes = {
	Fractalia:function(x,y){
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		
		var material = MaterialsFactory.ForestGround();
		var heightmap = TerrainFactory.GetHeightMap(x,y,Engine.ChunkSize,terrainFunction);			
		
		var objects = [];
		for (var i = 0; i < 10; i++) {
			var obj = NatureBodies.Rock(x,y,heightmap);
			Engine.InteractionObjects.push(obj.mesh);
			objects.push(obj)
		}				
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material,objects);
	},	
	
	
	
	Forest:function(x,y){
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		
		var material = MaterialsFactory.ForestGround();
		var heightmap = TerrainFactory.GetHeightMap(x,y,Engine.ChunkSize,terrainFunction);			
		
		var objects = [];
		for (var i = 0; i < 1; i++) {
			objects.push(NatureBodies.Tree(x,y,heightmap))
		}	
		for (var i = 0; i < 10; i++) {
			objects.push(NatureBodies.Bush(x,y,heightmap))
		}	
		for (var i = 0; i < 100; i++) {
			objects.push(NatureBodies.Leaf(x,y,heightmap))
		}
		for (var i = 0; i < 10; i++) {
			var obj = NatureBodies.Rock(x,y,heightmap);
			Engine.InteractionObjects.push(obj.mesh);
			objects.push(obj)
		}				
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material,objects);
	},		
	Sea:function(x,y){
		var terrainFunction = (x,y)=>0;
		
		var material = MaterialsFactory.SeaWater();
		
		var heightmap = TerrainFactory.GetHeightMap(x,y,Engine.ChunkSize,terrainFunction);			
		var hfBody = Physics.Terrain(heightmap,x,y);		
		var geometry = Graphics.TerrainGeometry(hfBody,x,y);
		Graphics.Terrain(hfBody,x,y,material,geometry);	
		world.remove(hfBody)
		
		//bottom
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t)+x/4;
		
		var material = MaterialsFactory.BeachSand();
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},
	Beach:function(x,y){			
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		var material = MaterialsFactory.BeachSand();			
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},
	BeachSubriver:function(x,y){
		var t = [{g:5,h:1}];
		var material = MaterialsFactory.BeachSand();
		var terrainFunction = function(xx,yy){
			var depth = 4;
			var perl = ProceduralGenerator._PerlinTerrain(xx,yy,t);
			perl+=Math.abs(Engine.ChunkSize/2-(yy+0.5))/depth - (Engine.ChunkSize-1)/2/depth;
			return perl;
		}			
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},
	BeachTransition:function(x,y){			
		var terrainFunction = (x,y)=>2;
		
		var material = MaterialsFactory.BrickWall();
		
		var objects = [];
		objects.push(NatureBodies.BoxMesh(
			x*(Engine.ChunkSize-1),
			1000,
			y*(Engine.ChunkSize-1),
			1,
			2,
			Engine.ChunkSize-1,
			material
		));
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material,objects);
	},
	Town:function(x,y){
		var terrainFunction = (x,y)=>2;
		
		var material = MaterialsFactory.BrickWall();
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},	
	
	Town2:function(x,y){
		var terrainFunction = (x,y)=>2;
		
		var material = MaterialsFactory.BrickWall();
		
		var objects = [];
		objects.push(NatureBodies.BoxMesh(
			x*(Engine.ChunkSize-1) +2,
			1000,
			y*(Engine.ChunkSize-1) -2,
			Engine.ChunkSize-1 -4,
			4,
			Engine.ChunkSize-1 -4,
			material
		));
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},		
	Town3:function(x,y){
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		
		var material = MaterialsFactory.BrickWall();
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},		
	Town4:function(x,y){
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		
		var material = MaterialsFactory.BrickWall();
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},		
	TownSubriver:function(x,y){
		var t = [{g:5,h:1}];
		var material = MaterialsFactory.BrickWall();
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		var terrainFunction = function(xx,yy){
			var x = (xx/(Engine.ChunkSize-1));
			var y = (yy/(Engine.ChunkSize-1))-1;
			
			var depth = 4;
			var perl = ProceduralGenerator._PerlinTerrain(xx,yy,t);
			perl+=Math.abs(Engine.ChunkSize/2-(yy+0.5))/depth - (Engine.ChunkSize-1)/2/depth;
			return perl;
		}	
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},		
		
	ForestLimit:function(x,y){
		var t = [{g:5,h:1}];
		var terrainFunction = (x,y)=>ProceduralGenerator._PerlinTerrain(x,y,t);
		
		var material = MaterialsFactory.BeachSand();
		
		return TerrainFactory.GenericChunk(x,y,terrainFunction,material);
	},
	
}