

function degToRad(degrees)
{
	var pi = Math.PI;
	return degrees * (pi/180);
}
function perp(a,b,c) 
{
	return  c<a  ? new THREE.Vector3(b,-a,0) : new THREE.Vector3(0,-c,b) 
}

var TreeFactory = {
	Rotate(mesh,vector){
        mesh.lookAt( vector );
    },
    Move(mesh,point){
        mesh.position.x = point.x;
        mesh.position.y = point.y;
        mesh.position.z = point.z;
    },
    SegmentMesh : function(options,lastOptions,material){
		
		var r1 = options.r;
		var r2 = options.r*options.rf;
		var r3 = lastOptions.r;
		
		var adj = Math.tan(options.a)*(r3)/2;
				
		options.p.add(lastOptions.v.clone().multiplyScalar(-adj));
		
        var nextPoint = options.v.clone().multiplyScalar(options.l).add(options.p);
		
        var geo = new THREE.CylinderGeometry( r2, r1, options.l, 6,1,true );
        geo.applyMatrix4( new THREE.Matrix4().makeTranslation( 0, options.l / 2, 0 ) );
        geo.applyMatrix4( new THREE.Matrix4().makeRotationX( Math.PI / 2 ) );

        var mesh = new THREE.Mesh(geo, material);
        TreeFactory.Rotate(mesh,options.v);
        TreeFactory.Move(mesh,options.p);
        
        return {
            next:nextPoint,
            mesh:mesh
        };
    },
	Leaf : function(p,v,r,l,material){
		var geometry = new THREE.PlaneGeometry(1,1);
		//var material = MaterialsFactory.TreeBranch();
		var mesh = new THREE.Mesh( geometry, material );

		var axis = perp(v.x,v.y,v.z);
		var angle = degToRad(-45);
		axis.applyAxisAngle( v, angle );
		//v.applyAxisAngle( axis, angle );
				
		TreeFactory.Rotate(mesh,axis);
		var nextPoint = v.clone().multiplyScalar(0).add(p);
				
		TreeFactory.Move(mesh,nextPoint);
	    return mesh;
    },
	
    TweakVector : function(vector,rnd,af){
        
        var newVec = vector.clone();

        var axis = perp(vector.x,vector.y,vector.z);
        var angle = degToRad(Random.Range(rnd,0,af));
        newVec.applyAxisAngle( axis, angle );

        var angle2 = degToRad(Random.Range(rnd,0,360));
        newVec.applyAxisAngle( vector, angle2 );

        return {v:newVec,a:angle};
    },
    Segment : function(group,options,rnd,lastOptions,material,leafmaterial){
        if(options.i>options.ilim){
			var result = TreeFactory.Leaf(options.p,options.v,options.r,options.l,leafmaterial);
			group.add(result);
            return;
        }

        var result = TreeFactory.SegmentMesh(options,lastOptions,material);
        group.add(result.mesh);

        var branches = Random.Range(rnd,1,options.bf);
        for(var i=0;i<branches;i++){
            var newOptions = TreeFactory.NewSegmentOptions(options,result.next,rnd);        
            TreeFactory.Segment(group,newOptions,rnd,options,material,leafmaterial);
        }
    },
	NewSegmentOptions(options,next,rnd){
		var vec = TreeFactory.TweakVector(options.v,rnd,options.af);		
        return newOptions = {
            p:next,
            v:vec.v,
			a:vec.a,
			i:options.i+1,
			
			r:options.r*options.rf,
            l:options.l*options.lf,
            rf:options.rf,
            lf:options.lf,
            ilim:options.ilim,
			af:options.af,
			bf:options.bf,
        };
    },
    Tree: function(worldRnd,initOptions){
		/*
			p: point
			v: vector
			r: radius
			l: length
			rf: radius increment
			lf: length increment
			i: iteration
			ilim: last iteration
			a: angle to previous segment
			af: angle variance
			bf: branch variance
		*/		
		var options = {
            p:new THREE.Vector3(0,-2,0),
            v:new THREE.Vector3(0,1,0),
			a:0,
            i:0
        };
		options.r = initOptions.r;
		options.l = initOptions.l;
		options.rf = initOptions.rf;
		options.lf = initOptions.lf;
		options.ilim = initOptions.ilim;
		options.af = initOptions.af;
		options.bf = initOptions.bf;
		
		var material = MaterialsFactory.TreeBirch(initOptions.colors.b1,initOptions.colors.b2);
		var leafmaterial = MaterialsFactory.TreeBranch(
			initOptions.colors.l1,
			initOptions.colors.l2,
			initOptions.colors.f1,
			initOptions.colors.f2,
			initOptions.colors.f3);
		
        var group = new THREE.Mesh();
        var seed = Random.Int(worldRnd);
        var rnd = Random.Create(seed);
        TreeFactory.Segment(group,options,rnd,options,material,leafmaterial);
        return group;
    }
}

