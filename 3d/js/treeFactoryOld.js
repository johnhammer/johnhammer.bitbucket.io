var MeshBuilder = {
    BranchMaterial : function(){
        //return new THREE.MeshBasicMaterial();// MaterialsFactory.BeachSand();
    },
    LeafMaterial : function(){
        return MaterialsFactory.BeachSand();
    },
    Rotate(mesh,vector){
        mesh.lookAt( vector );
    },
    Move(mesh,point){
        mesh.position.x = point.x;
        mesh.position.y = point.y;
        mesh.position.z = point.z;
    },
    Segment : function(p,v,r1,r2,l,a,lastV){
        var material = MeshBuilder.BranchMaterial();
		
		var side = r1/2;
		var adj = Math.tan(a)*(r1)/2;
		
		//Math.sqrt(side*side + side*side - 2*side*side*Math.cos(a))
		
		var adjustedPoint = p//lastV.clone().multiplyScalar(adj).add(p);
		
        var nextPoint = v.clone().multiplyScalar(l).add(p);
		

        var geo = new THREE.CylinderGeometry( r2, r1, l, 8 );
        geo.applyMatrix( new THREE.Matrix4().makeTranslation( 0, l / 2, 0 ) );
        geo.applyMatrix( new THREE.Matrix4().makeRotationX( Math.PI / 2 ) );

        var mesh = new THREE.Mesh(geo, material);
        MeshBuilder.Rotate(mesh,v);
        MeshBuilder.Move(mesh,adjustedPoint);
        
        return {
            next:nextPoint,
            mesh:mesh
        };
    },
	Leaf : function(p,v,r,l){
        var m = MeshBuilder.LeafMaterial();

		/*
        var geometry = new THREE.CylinderGeometry( r*2, r*4, l*2, 3 );
        geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, l / 2, 0 ) );
        geometry.applyMatrix( new THREE.Matrix4().makeRotationX( Math.PI / 2 ) );

        var mesh = new THREE.Mesh(geometry, material);
        MeshBuilder.Rotate(mesh,v);
        MeshBuilder.Move(mesh,p);
        */
		

		var geometry = new THREE.Geometry();
/*

var s = 2
var geom = new THREE.Geometry();
var v1 = new THREE.Vector3(0,0,0);
var v2 = new THREE.Vector3(s,0,s);
var v3 = new THREE.Vector3(s,s,s);

geom.vertices.push(v1);
geom.vertices.push(v2);
geom.vertices.push(v3);

geom.faces.push( new THREE.Face3( 0, 1, 2 ) );
geom.computeFaceNormals();

var mesh= new THREE.Mesh( geom, new THREE.MeshNormalMaterial() );
MeshBuilder.Rotate(mesh,v);
MeshBuilder.Move(mesh,p);
return mesh;
		

		var s = 2;
for (var i = 0; i < 100; i++) {
    var vertex = new THREE.Vector3();
    vertex.x = Math.random() * s - s/2;
    vertex.y = Math.random() * s - s/2;
    vertex.z = Math.random() * s - s/2;
    geometry.vertices.push(vertex);
}
material = new THREE.PointCloudMaterial({
    size: 1,
    sizeAttenuation: true,
    map: m.map,
	//color: 0x888888,
    transparent: true,
    opacity: 0.5
});
mesh = new THREE.PointCloud(geometry, material);
 MeshBuilder.Move(mesh,p);
 */
        return mesh;
    }
}

 function degToRad(degrees)
        {
            var pi = Math.PI;
            return degrees * (pi/180);
        }

var TreeBuilder = {
    TweakVector : function(vector,rnd){
        function perp(a,b,c) 
        {
            return  c<a  ? new THREE.Vector3(b,-a,0) : new THREE.Vector3(0,-c,b) 
        }
       

        var newVec = vector.clone();

        var axis = perp(vector.x,vector.y,vector.z);
        var angle = degToRad(Random.Range(rnd,0,45));
        newVec.applyAxisAngle( axis, angle );

        var angle2 = degToRad(Random.Range(rnd,0,360));
        newVec.applyAxisAngle( vector, angle2 );

        return {v:newVec,a:angle};
    },
    NewSegmentOptions(options,next,rnd){
		var vec = TreeBuilder.TweakVector(options.v,rnd);
		
        return newOptions = {
            p:next,
            v:vec.v,
			a:vec.a,
            r:options.r*options.rf,
            l:options.l*options.lf,
            rf:options.rf,
            lf:options.lf,
            rlim:options.rlim,
        };
    },
    Segment : function(group,options,rnd,lastV){
        if(options.r<options.rlim){
			var result = MeshBuilder.Leaf(options.p,options.v,options.r,options.l);
			//group.add(result);
            return;
        }
		if(!lastV){
			lastV = options.v;
		}

        var result = MeshBuilder.Segment(options.p,options.v,options.r,options.r*options.rf,options.l,options.a,lastV);
        group.add(result.mesh);

        var branches = Random.Range(rnd,1,4);
        for(var i=0;i<branches;i++){
            var newOptions = TreeBuilder.NewSegmentOptions(options,result.next,rnd);        
            TreeBuilder.Segment(group,newOptions,rnd,options.v);
        }
    },
    Tree: function(worldRnd){
		var options = {
            p:new THREE.Vector3(0,-2,0),
            v:new THREE.Vector3(0,1,0),
            r:.7,
            l:2,
            rf:.7,
            lf:.9,
            rlim:.1,
			a:45
        };
        var group = new THREE.Mesh();
        var seed = Random.Int(worldRnd);
        var rnd = Random.Create(seed);
        TreeBuilder.Segment(group,options,rnd);
        return group;
    }
}

