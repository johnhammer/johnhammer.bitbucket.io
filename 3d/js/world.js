var World = {
	Create(rnd){
		var terrainSeed = Random.Int(rnd);
		var amountOfPeople = Random.Range(rnd,2,3);
		var people = God.CreatePeople(rnd,amountOfPeople);
		var history = HistoryEvents.Tick(rnd,2000);
		var nature = Nature.Create(rnd);
		
		var world = {
			People:people,
			History:HistoryEvents._Log,
			Countries:HistoryEvents._Countries,
			HistoryCountries:HistoryEvents._HistoryCountries,
			Nature : nature
		};
		
		var god = God.Create(world);
		world.God = god;
		God.GrowPeople(rnd,world);
		
		return world;
	}
}