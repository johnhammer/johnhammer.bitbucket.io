var TerrainFactory = {
	_Perlin(x,y,grain,h){
		return Math.abs(noise.perlin2(x/grain,y/grain))* h;
	},
	_PerlinTerrain(x,y,data){
		var val = 0;
		for (var i = 0; i < data.length; i++) {
			val+=TerrainFactory._Perlin(x,y,data[i].g,data[i].h);
		}
		return val;
	},	
	ForestTerrain(x,y){
		/*
			[{g:5,h:5},{g:100,h:100}] high mountains
			[{g:5,h:1}]	calmland
		*/
		var t = [{g:5,h:1}];
		return TerrainFactory._PerlinTerrain(x,y,t)
	},
	GetHeightMap(oox,ooy,ChunkSize,f){
		var ox=oox*(ChunkSize-1);
		var oy=-ooy*(ChunkSize-1)
		var matrix = [];
		var sizeX = ChunkSize+1, sizeY = ChunkSize+1; //extra row anti clipping
		for (var i = 0; i < sizeX; i++) {
			matrix.push([]);
			for (var j = 0; j < sizeY; j++) {
				var val = f(i+ox,j+oy);
				matrix[i].push(val); //0.035*ox+0.025*oy
			}
		}		
		return matrix;
	},
	GrayscaleTexture(x,y){
		var t = [{g:5,h:1}];
		var value = TerrainFactory._PerlinTerrain(x,y,t)
		value *= 255;	
		return [
			value,
			value,
			value,
			255
		];
	},
	
	FractalPoint(x0,y0){ //X scale (-2.5, 1)) Y scale (-1, 1))
		var x = 0.0;
		var y = 0.0;
		var iteration = 0;
		var max_iteration = 1000;
		
		var rsquare = 0
		var isquare = 0
		var zsquare = 0
		while (rsquare + isquare <= 4  &&  iteration < max_iteration) {
		  x = rsquare - isquare + x0
		  y = zsquare - rsquare - isquare + y0
		  rsquare = x*x
		  isquare = y*y
		  zsquare = (x + y)*(x + y)
		  iteration = iteration + 1
		}	
		
		return zsquare;
		return {r:zsquare,g:rsquare,b:isquare};
	},
	FractalTexture(x,y){
		var value = TerrainFactory.FractalPoint((x+100)*0.002,(y+100)*0.002)
		return [
			value,
			value,
			value,
			255
		];
	},
}