var LIB = {
	Capitalize : function(string){		
		return string.charAt(0).toUpperCase() + string.slice(1);
	},
	Replace : function(str, find, replace){
		return str.replace(new RegExp(find, 'g'), replace);
	},
	Normalize(number1,number2){
		return Math.floor((number1 / number2) * 100)
	},
	Guid : function() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	},
	NextChar(c) {
		return String.fromCharCode(c.charCodeAt(0) + 1);
	},
	RemoveIndex(array,index) {
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	}
}




var ConversationParser = {
	_CountTabs : function(line){
		var i = 0;
		while(line[i]=="\t"){
			i++;
		}
		return i;
	},
	_ParseLine : function(lines,index,lastlevel){
		if(index>=lines.length) return;
		
		var line = lines[index];
		var tabs = ConversationParser._CountTabs(line);			
		
		if(tabs<=lastlevel) return;				
		
		var answers = [];
		var stop = false;		
		
		while(!stop){
			var nextline = ConversationParser._ParseLine(lines,index+1,tabs);
			if(!nextline) {
				stop = true;
			}
			else{
				answers.push({p:nextline.p,a:nextline.a});		
				index = nextline.i;
			}
		}
		
		line = line.substr(tabs);
		return {p:ConversationParser._ParseLineContent(line),a:answers,i:index};
	},
	_Trim : function(str) {
		str = str.replace(/^\s+/, '');
		for (var i = str.length - 1; i >= 0; i--) {
			if (/\S/.test(str.charAt(i))) {
				str = str.substring(0, i + 1);
				break;
			}
		}
		return str;
	},
	_CountChar : function(string,character){
		return (string.split(character).length - 1);
	},
	_ProcessPie : function(text){ 
		var words = text.split(' ');
		var newwords = words.map(x=>PIE[x]?PIE[x]:x);
		text = newwords.join(' ');		
		return text;
	},
	_ParseLineContent : function(text){			
		var rxp = /\(([^\)]+)\)?/g; //no support multiple words
		var curMatch;			
		while(curMatch = rxp.exec(text)) {
			var rawPie = curMatch[1];
			var cookedPie = ConversationParser._ProcessPie(rawPie);		
			text = text.replace(curMatch[0],cookedPie);
		}
		return text;
	},
	ParseConversation : function(raw){
		var lines = ConversationParser._Trim(raw).split('\n');
		var index = 0;
		
		var obj = ConversationParser._ParseLine(lines,index,-1);
		obj = {a:obj.a,p:obj.p};
		
		return obj;
	}
}

var MaterialsFactory = {
	Textures : {
		Wall:{url:"0outwall"},
		Floor:{url:"0floor"},
		Dirt:{url:"0moss"},
	},
	Get : function(tex){
		return new THREE.MeshPhongMaterial({
		  map: tex.val
		});
	},
	Load : function(promises){
		var path = 'https://johnhammer.bitbucket.io/3d/assets/textures/';

		var textureLoader = new THREE.TextureLoader();
		for (var key in MaterialsFactory.Textures) {
			promises.push(new Promise((resolve, reject) => {
				var entry = MaterialsFactory.Textures[key]
				var url = path + entry.url + '.jpg'
				textureLoader.load(url,
					texture => {
						texture.repeat.set(10, 10);
						texture.wrapS = THREE.RepeatWrapping;
						texture.wrapT = THREE.RepeatWrapping;
						entry.val = texture;
						resolve(entry);
					},
					xhr => {
						console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
					},
					xhr => {}
				)	  
			}));
		}
	}
}


var Geometry = {
	IntersectRect : function(r1, r2) {
		return !(r2.x > r1.x+r1.w || 
				r2.x+r2.w < r1.x || 
				r2.y > r1.y+r1.h ||
				r2.y+r2.h < r1.y);
	},
	WithinRect : function(R2,R1){
		return (R2.x+R2.w) < (R1.x+R1.w)
			&& (R2.x) > (R1.x)
			&& (R2.y) > (R1.y)
			&& (R2.y+R2.h) < (R1.y+R1.h)
	},
	PointInCircle(x, y, cx, cy, radius) {
		var distancesquared = (x - cx) * (x - cx) + (y - cy) * (y - cy);
		return distancesquared <= radius * radius;
	}
}

var Draw2d = {
	PaintRectangle : function (ctx,rectangle){
		ctx.fillStyle = rectangle.color;
		ctx.fillRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
	}
}

var camera, scene, renderer, controls;

var objects = [];

var raycaster;

var moveForward = false;
var moveBackward = false;
var moveLeft = false;
var moveRight = false;
var canJump = false;

var prevTime = performance.now();
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();
var vertex = new THREE.Vector3();
var color = new THREE.Color();

var MeshFactory = {
	BuildRoom : function(W,H,X,Y,B,T){ //Width Height X Y Broad Tall
	
		var wallMaterial = MaterialsFactory.Get(MaterialsFactory.Textures.Wall);
		var floorMaterial = MaterialsFactory.Get(MaterialsFactory.Textures.Floor);
	
		MeshFactory.BuildBox(B,		H,		X+B/2,			Y+H/2,		T,wallMaterial);
		MeshFactory.BuildBox(W-B,	B,		X+B+(W-B)/2,	Y+B/2,		T,wallMaterial);
		MeshFactory.BuildBox(W-B,	B,		X+B+(W-B)/2,	Y+H-B/2,	T,wallMaterial);
		MeshFactory.BuildBox(B,		H-2*B,	X+W-B/2,		Y+H/2,		T,wallMaterial);
		
		MeshFactory.BuildBox(W,H,X+W/2,Y+H/2,2,floorMaterial);
	},	
	BuildBox : function(w,h,x,y,T,material){
		var geometry = new THREE.BoxGeometry(w, T, h);
		var cube = new THREE.Mesh( geometry, material );
		cube.position.z = y;
		cube.position.x = x;
		cube.position.y = T/2;
		scene.add( cube );
	}
}

var Draw3d = {
	PaintRectangle : function (rectangle){
		MeshFactory.BuildRoom(rectangle.w,rectangle.h,rectangle.x,rectangle.y,2,20);
	},
	PaintDoor : function(rectangle){
		MeshFactory.BuildDoor(rectangle.w,rectangle.h,rectangle.x,rectangle.y,2,20);
	},
	init:function() {

				camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 );
				camera.position.y = 10;

				scene = new THREE.Scene();
				scene.background = new THREE.Color( 0xffffff );
				scene.fog = new THREE.Fog( 0xffffff, 0, 750 );
				
				var light = new THREE.AmbientLight( 0xCCCCCC); // soft white light
				scene.add( light );

				//var pointLight = new THREE.PointLight( 0xffFFFF, 1.1, 100 );
				//pointLight.position.set( 10, 20, 10 );
				//scene.add( pointLight );

				controls = new THREE.PointerLockControls( camera );


				scene.add( controls.getObject() );

				var onKeyDown = function ( event ) {

					switch ( event.keyCode ) {

						case 38: // up
						case 87: // w
							moveForward = true;
							break;

						case 37: // left
						case 65: // a
							moveLeft = true;
							break;

						case 40: // down
						case 83: // s
							moveBackward = true;
							break;

						case 39: // right
						case 68: // d
							moveRight = true;
							break;

						case 32: // space
							if ( canJump === true ) velocity.y += 350;
							canJump = false;
							break;

					}

				};

				var onKeyUp = function ( event ) {
					switch ( event.keyCode ) {
						case 38: // up
						case 87: // w
							moveForward = false;
							break;
						case 37: // left
						case 65: // a
							moveLeft = false;
							break;
						case 40: // down
						case 83: // s
							moveBackward = false;
							break;
						case 39: // right
						case 68: // d
							moveRight = false;
							break;
					}
				};

				document.addEventListener( 'keydown', onKeyDown, false );
				document.addEventListener( 'keyup', onKeyUp, false );

				raycaster = new THREE.Raycaster( new THREE.Vector3(), new THREE.Vector3( 0, - 1, 0 ), 0, 10 );

				renderer = new THREE.WebGLRenderer( { antialias: true } );
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				document.body.appendChild( renderer.domElement );

				window.addEventListener( 'resize', Draw3d.onWindowResize, false );

				document.onclick = function () {
					document.body.requestPointerLock();
				}
				

			},

			onWindowResize: function () {

				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );

			},

			animate : function() {

				requestAnimationFrame( Draw3d.animate );

	
					raycaster.ray.origin.copy( controls.getObject().position );
					raycaster.ray.origin.y -= 10;

					var intersections = raycaster.intersectObjects( objects );

					var onObject = intersections.length > 0;

					var time = performance.now();
					var delta = ( time - prevTime ) / 1000;

					velocity.x -= velocity.x * 10.0 * delta;
					velocity.z -= velocity.z * 10.0 * delta;

					velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

					direction.z = Number( moveForward ) - Number( moveBackward );
					direction.x = Number( moveLeft ) - Number( moveRight );
					direction.normalize(); // this ensures consistent movements in all directions

					if ( moveForward || moveBackward ) velocity.z -= direction.z * 400.0 * delta;
					if ( moveLeft || moveRight ) velocity.x -= direction.x * 400.0 * delta;

					if ( onObject === true ) {

						velocity.y = Math.max( 0, velocity.y );
						canJump = true;

					}
					controls.getObject().translateX( velocity.x * delta );
					controls.getObject().position.y += ( velocity.y * delta ); // new behavior
					controls.getObject().translateZ( velocity.z * delta );

					if ( controls.getObject().position.y < 10 ) {

						velocity.y = 0;
						controls.getObject().position.y = 10;

						canJump = true;

					}

					prevTime = time;

				

				renderer.render( scene, camera );

			}
			
}


var CONST = {
	DoorWidth : 20,
	Separation : 1,
}

function Range(min,max){
	return Math.ceil(Math.random() * (max - min) + min);
}

function AddRectangle(rectangle,x,y){
	result.push(rectangle);
}

function AddDoorH(x,y){
	var rectangle = {
		x:x,
		y:y,
		w:CONST.DoorWidth,
		h:CONST.Separation
	};
	
	doors.push(rectangle);
}
function AddDoorV(x,y){
	var rectangle = {
		x:x,
		y:y,
		w:CONST.Separation,
		h:CONST.DoorWidth
	};
	
	doors.push(rectangle);
}

function PaintRectangle(rectangle){
	ctx.fillStyle = rectangle.color;
	ctx.fillRect(rectangle.x, rectangle.y, rectangle.w, rectangle.h);
}

function PaintResult(){
	for ( var i = 0; i < result.length; i ++ ) {
		//Draw2d.PaintRectangle(result[i]);
		Draw3d.PaintRectangle(result[i]);
	}
}

function TryPlaceRectangle(rectangle,last){
	var x;
	var y;
	var contact = 2;
	var separation = CONST.Separation;
	
	
	if(!last){
		x = Range(0,width-rectangle.w);
	    y = Range(0,height-rectangle.h);
	}
	else{
		switch (Range(0,3)){
			case 0: //right
				x = last.x+last.w + separation;
				y = Range(last.y-rectangle.h + contact, last.y+last.h - contact);				
				rectangle.doorv = true;		
				rectangle.doorx = x - separation;				
				break;
			case 1: //bottom
				x = Range(last.x-rectangle.w + contact, last.x+last.w - contact);
				y = last.y+last.h + separation; 
				rectangle.doorh = true;
				rectangle.doory = y - separation;
				break;
			case 2: //left
				x = last.x-rectangle.w - separation;
				y = Range(last.y-rectangle.h + contact, last.y+last.h - contact);
				rectangle.doorv = true;
				rectangle.doorx = last.x;
				break;
			case 3: //top
				x = Range(last.x-rectangle.w + contact, last.x+last.w - contact);
				y = last.y-rectangle.h - separation; 
				rectangle.doorh = true;
				rectangle.doory = last.y;
				break;
		}		
		var p0,pf;
		if(rectangle.doorh){		
			p0 = x>last.x ? x : last.x;
			pf = x+rectangle.w<last.x+last.w ? x+rectangle.w : last.x+last.w;				
			rectangle.doorx = Range(pf + CONST.DoorWidth/2, p0- CONST.DoorWidth/2);
		}
		else{			
			p0 = y>last.y ? y : last.y;
			pf = y+rectangle.h<last.y+last.h ? y+rectangle.h : last.y+last.h;
			rectangle.doory = Range(pf+ CONST.DoorWidth/2, p0- CONST.DoorWidth/2);
		}	
	}
	
	rectangle.x = x;
	rectangle.y = y;
	return rectangle;
}

function IntersectRect(r1, r2) {
	return !(r2.x > r1.x+r1.w || 
			r2.x+r2.w < r1.x || 
			r2.y > r1.y+r1.h ||
			r2.y+r2.h < r1.y);
}

function IsRectangleValid(rectangle){
	if(rectangle.x<0 || 
		rectangle.y<0 || 
		rectangle.x+rectangle.w>=width || 
		rectangle.y+rectangle.h>=height 
	){
		return false;
	}

	for ( var i = 0; i < result.length; i ++ ) {
		if(IntersectRect(rectangle,result[i])){
			return false;
		}
	}
	return true;
}

function Init(){
	result = [];
	//var last=null;
	for ( var i = 0; i < rectangles.length; i ++ ) {
	
		var last = rectangles[i].parent ? result[rectangles[i].parent-1] : null;
	
		var rectangle = TryPlaceRectangle(rectangles[i],last);
		if(IsRectangleValid(rectangle)){
			AddRectangle(rectangle);
			if(rectangle.doorh){
				AddDoorH(rectangle.doorx,rectangle.doory);
			}
			else if(rectangle.doorv){
				AddDoorV(rectangle.doorx,rectangle.doory);
			}
		}
		else{
			Init();
			return;
		}		
	}
	PaintResult();
}	