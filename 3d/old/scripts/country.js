var Country = {
	CountryPrefix : [
		'meil','enn','gho','leght','sein','kat','gwel','tem','dhen','nooi','meen','sent','olw','wer','lok','ghron'
	],	
	CountrySuffix : [
		'rey','cia','en','ex','os','as','en','ex','bria','lia','gas','sey','wara','way','an','ent'
	],
	BigCountryTitle : [
		'Empire of ',
		'Imperial ',
		'Union of ',
		'Great ',
		'Federation of ',
		'Kingdoms of ',
		'Commonwealth of ',
		'Alliance of ',
	],
	MidCountryTitle : [
		'Crown of ',
		'United ',
		'Confederation of ',
		'Dominion of ',
		'Republic of ',
		'Free ',
		'States of ',
		'Provinces of ',
	],
	CountryName(rnd,power){
		var name = Random.Choice(rnd,Country.CountryPrefix) + Random.Choice(rnd,Country.CountrySuffix);
		name = LIB.Capitalize(name);
		if(power>70){
			return Random.Choice(rnd,Country.BigCountryTitle) + name;
		}
		else if(power>20){
			return Random.Choice(rnd,Country.MidCountryTitle) + name;
		}
		else{
			return name;
		}	
	},
	Country : function(rnd,power){
		return {
			Name:Country.CountryName(rnd,power),
			Power:power,
			Allies:{},
			Enemies:{}
		};
	},
	Add : function(country,Countries){
		Countries[country.Name] = country;
	},
	Remove : function(country,Countries){
		for(i in country.Allies){
			delete country.Allies[i].Allies[country.Name];
		}	
		for(i in country.Enemies){
			delete country.Enemies[i].Enemies[country.Name];
		}
		delete Countries[country.Name];
	},
	Pick : function(rnd,list){
		return list[Random.Choice(rnd,Object.keys(list))];			
	}
}
