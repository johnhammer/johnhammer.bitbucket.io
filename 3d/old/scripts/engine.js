
var prevTime = performance.now();
var velocity = new THREE.Vector3();
var direction = new THREE.Vector3();

//engine
var Engine = {
	Map:{},
	Current:{
		Object:null
	},
	scene:null,
	camera:null,
	renderer:null,
	
	Pole(id,x,y){
		var geometry = new THREE.CylinderGeometry(8,8,8);		
		var material = MaterialsFactory.Get(MaterialsFactory.Textures.Dirt);		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.x = x;
		cube.position.y = 0;
		cube.position.z = y;
		Engine.Map[id] = {x:x,y:y,Id:id};
		Engine.scene.add( cube );		
	},	
	CheckMap(x,y){
		for(var obj in Engine.Map){
			if(Geometry.PointInCircle(Engine.Map[obj].x,Engine.Map[obj].y,x,y,8)){
				return Engine.Map[obj];
			}
		}
		return null;
	},
	Init(){
		Engine.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 );		
		Engine.scene = new THREE.Scene();		
		Engine.renderer = new THREE.WebGLRenderer( { antialias: true } );
		Engine.renderer.setPixelRatio( window.devicePixelRatio );
		Engine.renderer.setSize( window.innerWidth, window.innerHeight );
		
		document.body.appendChild( Engine.renderer.domElement );		
		Engine.InitControls();
		
		var aLight = new THREE.AmbientLight( 0x151c0f );
	Engine.scene.add(aLight);

	var pLight = new THREE.PointLight( 0xe3fbdc, 0.9 );
	pLight.position.set(1000,600,0);
	Engine.scene.add(pLight);


		//extra
		
		var geometry = new THREE.BoxGeometry(100,10,100);		
		var material = MaterialsFactory.Get(MaterialsFactory.Textures.Dirt);		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.y = -10;
		Engine.scene.add( cube );	
		
		Engine.Pole("ea",20,20);

		var planes = ForestGraphics.GetGrass();
		Engine.scene.add(planes);
		
	},
	OnTouch(e) {
		var t = e.touches[0];
		this.onTouchEnd(e);
		if (t.pageY < window.innerHeight * 0.5) this.onKey(true, { keyCode: 38 });
		else if (t.pageX < window.innerWidth * 0.5) this.onKey(true, { keyCode: 37 });
		else if (t.pageY > window.innerWidth * 0.5) this.onKey(true, { keyCode: 39 });
	},
	OnTouchEnd(e) {
		this.states = { 'left': false, 'right': false, 'forward': false, 'backward': false };
		e.preventDefault();
		e.stopPropagation();
	},
	OnKey(val, e) {
		var state = this.codes[e.keyCode];
		if (typeof state === 'undefined') return;
		this.states[state] = val;
		e.preventDefault && e.preventDefault();
		e.stopPropagation && e.stopPropagation();
	},
	InitControls(){
		Engine.controls = new THREE.PointerLockControls( Engine.camera );
		Engine.scene.add( Engine.controls.getObject() )		
		document.onclick = function () {document.body.requestPointerLock();}
		
		this.codes  = { 37: 'left', 39: 'right', 38: 'forward', 40: 'backward' };
        this.states = { 'left': false, 'right': false, 'forward': false, 'backward': false };
        document.addEventListener('keydown', this.OnKey.bind(this, true), false);
        document.addEventListener('keyup', this.OnKey.bind(this, false), false);
        document.addEventListener('touchstart', this.OnTouch.bind(this), false);
        document.addEventListener('touchmove', this.OnTouch.bind(this), false);
        document.addEventListener('touchend', this.OnTouchEnd.bind(this), false);
	},
	UpdateControls(){
		var time = performance.now();
		
		var delta = ( time - prevTime ) / 1000;

		velocity.x -= velocity.x * 10.0 * delta;
		velocity.z -= velocity.z * 10.0 * delta;

		//velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass
		
		direction.z = Number( this.states.forward ) - Number( this.states.backward );
		direction.x = Number( this.states.left ) - Number( this.states.right );
		direction.normalize();		

		if ( this.states.forward || this.states.backward ) velocity.z -= direction.z * 400.0 * delta;
		if ( this.states.left || this.states.right ) velocity.x -= direction.x * 400.0 * delta;
		
		Engine.controls.getObject().translateX( velocity.x * delta );
		Engine.controls.getObject().position.y += ( velocity.y * delta ); // new behavior
		Engine.controls.getObject().translateZ( velocity.z * delta );
		
		var mapObj = Engine.CheckMap(Engine.controls.getObject().position.x,Engine.controls.getObject().position.z);
		if(!Engine.Current.Object && mapObj){
			Engine.Current.Object = mapObj;
			console.log(mapObj.Id);
		}
		else if(Engine.Current.Object && !mapObj){
			Engine.Current.Object = null;
		}
		
		prevTime = time;
	},
	Start(){
		var promises = [];
		MaterialsFactory.Load(promises);
		Promise.all(promises).then(results => {
			Engine.Init();
			Engine.Animate();
		});		
	},
	Animate : function() {
		requestAnimationFrame( Engine.Animate );
		
		Engine.UpdateControls();
		
		Engine.renderer.render( Engine.scene, Engine.camera );
	}
}