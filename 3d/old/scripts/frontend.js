//frontend

var Frontend = {
	InitCollapsibles(){
		var coll = document.getElementsByClassName("collapsible");
		var i;
		for (i = 0; i < coll.length; i++) {
			const content = coll[i].nextElementSibling;
			content.style.display = "none";
			coll[i].addEventListener("click", function() {
				this.classList.toggle("active");		
				if (content.style.display === "block") {
					content.style.display = "none";
				}
				else {
					content.style.display = "block";
				}		
			});				
		}		
	},
	Init(){
		Frontend.InitCollapsibles();
		
	},
	Collapsible(button,content){		
		return `
			<button type="button" class="collapsible">${button}</button>
			<div class="content">
				${content}
			</div>
		`;
	}
	
}