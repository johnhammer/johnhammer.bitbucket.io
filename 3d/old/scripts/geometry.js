var Geometry = {
	PointInCircle(x, y, cx, cy, radius) {
		var distancesquared = (x - cx) * (x - cx) + (y - cy) * (y - cy);
		return distancesquared <= radius * radius;
	}
}