var HistoryEvents = {
	
	_Log : [],
	_Countries : {},
	
	Type : {
		CivilWar : 0,
		War : 1,
		PowerDown : 2,
		PowerUp : 3,
		Aliance : 4,
		Treason : 5,
		Merge : 6,
		Disaster : 7,
		Technology : 8,
	},
	Log : function(log){
		this._Log[this._Log.length-1] = {...this._Log[this._Log.length-1], ...log};
	},
	ForeignIntervention(rnd,c1,c2){
		if(Random.Chance(rnd,2)) return 0;
		
		var val = 0;
		if(Object.keys(c1.Allies).length>0){
			var ally = Country.Pick(rnd,c1.Allies);
			HistoryEvents.Log({intervention:true,foreignAllies : ally});
			val += ally.Power;
		}
		if(Object.keys(c1.Enemies).length>0){
			var enemy = Country.Pick(rnd,c1.Enemies);
			HistoryEvents.Log({intervention:true,foreignEnemies : enemy});
			val -= enemy.Power;
		}		
		return val;
	},	
	CivilWar : function(rnd,country){
		var log = {newlands:[]};		
		HistoryEvents.PowerDown(rnd,country);
		var intervened = HistoryEvents.ForeignIntervention(rnd,country,country);
		if(intervened>0 || Random.Chance(rnd,2)){
			var parts = Random.Range(rnd,1,4);
			var powers = Random.NumberParts(rnd,country.Power,parts,.5);
			for (var i=0; i < parts; i++) {
				var newcountry = Country.Country(rnd,powers[i]);
				Country.Add(newcountry,this._Countries);
				log.newlands.push(newcountry);
			}
			Country.Remove(country,this._Countries);			
		}
		HistoryEvents.Log(log);
	},
	War : function(rnd,country){	
		var c2 = Country.Pick(rnd,this._Countries);
		if(country.Allies[c2.Name]){
			this._Log[this._Log.length-1].type = HistoryEvents.Type.Treason; //needed?
			HistoryEvents.Treason(rnd,country,c2);
			return;
		}
		if(c2==country){
			this._Log[this._Log.length-1].type = HistoryEvents.Type.CivilWar;
			HistoryEvents.CivilWar(rnd,country);
			return;
		}
		
		var intervened = HistoryEvents.ForeignIntervention(rnd,country,c2);
		var log = {other:c2,minor:false,win:false};		
		if(country.Power+intervened>c2.Power*2){
			log.win = true;
			HistoryEvents.Merge(rnd,country,c2);
		}
		else if((country.Power+intervened)*2<=c2.Power){
			HistoryEvents.Merge(rnd,c2,country);
		}
		else if(country.Power>c2.Power){
			log.minor = true;
			country.Enemies[c2.Name] = c2;
			c2.Enemies[country.Name] = country;
			HistoryEvents.PowerDown(rnd,c2);
		}
		else if(country.Power<=c2.Power){
			log.minor = true;
			log.win = true;
			country.Enemies[c2.Name] = c2;
			c2.Enemies[country.Name] = country;
			HistoryEvents.PowerDown(rnd,country);
		}
		HistoryEvents.Log(log);
		//foreign intervention, allies, allies of allies, enemies
		
	},
	PowerDown : function(rnd,country){
		country.Power *= 0.5;
	},
	PowerUp : function(rnd,country){
		country.Power *= 10;
	},
	Aliance : function(rnd,country,c2=null){
		c2 = c2==null? Country.Pick(rnd,this._Countries) : c2;	
		var log = {other:c2};
		if(c2==country){
			HistoryEvents.PowerUp(rnd,country);
			HistoryEvents.Log(log);
			return;
		}		
		if(country.Enemies[c2.Name]){
			log.former = true;
			delete country.Enemies[c2.Name];
			delete c2.Enemies[country.Name];
		}
		country.Allies[c2.Name] = c2;
		c2.Allies[country.Name] = country;
		HistoryEvents.Log(log);
	},
	Treason : function(rnd,country,c2=null){
		c2 = c2==null? Country.Pick(rnd,this._Countries) : c2;	
		var log = {other:c2};
		if(c2==country){
			HistoryEvents.PowerDown(rnd,country);
			HistoryEvents.Log(log);
			return;
		}
		if(country.Allies[c2.Name]){
			log.former = true;
			delete country.Allies[c2.Name];
			delete c2.Allies[country.Name];
		}
		country.Enemies[c2.Name] = c2;
		c2.Enemies[country.Name] = country;
		HistoryEvents.Log(log);
	},
	Merge : function(rnd,country,c2=null){
		c2 = c2==null? Country.Pick(rnd,this._Countries) : c2;	
		if(c2==country){
			HistoryEvents.PowerUp(rnd,country);
			HistoryEvents.Log({self:true});
			return;
		}		
		country.Power += c2.Power;
		country.Allies = {...country.Allies, ...c2.Allies };
		country.Enemies = {...country.Enemies, ...c2.Enemies };
		Country.Remove(c2,this._Countries);
		HistoryEvents.Log({merged:c2});
	},
	Disaster : function(rnd,country){
		for(var i=0; i < this._Countries.length; i++){
			HistoryEvents.PowerDown(rnd,counties[i]);
		}
		HistoryEvents.Log({detail:Random.Choice(rnd,HistoryTextMaker.Disasters)});
	},
	Technology : function(rnd,country,MaxYear){
		for(var i=0; i < this._Countries.length; i++){
			HistoryEvents.PowerUp(rnd,counties[i]);
		}
		HistoryEvents.Log({detail:Random.Choice(rnd,HistoryTextMaker.GetTechnology(this._Log[this._Log.length-1].year,MaxYear))});
	},
	Random : function(rnd,year,MaxYear){		
		var country = Country.Pick(rnd,this._Countries);	
		var action = Random.WChoice(rnd,[
			{w:15,v:HistoryEvents.Type.CivilWar},
			{w:10,v:HistoryEvents.Type.PowerDown},
			{w:5,v:HistoryEvents.Type.PowerUp},
			{w:3,v:HistoryEvents.Type.Disaster},
			{w:18,v:HistoryEvents.Type.Aliance},
			{w:5,v:HistoryEvents.Type.Treason},
			{w:20,v:HistoryEvents.Type.War},
			{w:20,v:HistoryEvents.Type.Technology},
			{w:4,v:HistoryEvents.Type.Merge},
		]);		
		
		Log.push({type:action,year:year,country:country});
		
		switch(action){			
			case HistoryEvents.Type.CivilWar : 
				HistoryEvents.CivilWar(rnd,country);
				break;
			case HistoryEvents.Type.War : 
				HistoryEvents.War(rnd,country);
				break;
			case HistoryEvents.Type.PowerDown : 
				HistoryEvents.PowerDown(rnd,country);
				break;
			case HistoryEvents.Type.PowerUp : 
				HistoryEvents.PowerUp(rnd,country);
				break;
			case HistoryEvents.Type.Aliance : 
				HistoryEvents.Aliance(rnd,country);
				break;
			case HistoryEvents.Type.Treason : 
				HistoryEvents.Treason(rnd,country);
				break;
			case HistoryEvents.Type.Merge : 
				HistoryEvents.Merge(rnd,country);
				break;
			case HistoryEvents.Type.Disaster : 
				HistoryEvents.Disaster(rnd,country);
				break;
			case HistoryEvents.Type.Technology : 
				HistoryEvents.Technology(rnd,country,MaxYear);
				break;
		}		
	},	
	Tick : function(rnd,MaxYear){		
		var empire = Country.Country(rnd,100);
		Country.Add(empire,this._Countries);
		
		var year = 0;
		do{			
			year += Random.Range(rnd,0,100);
			HistoryEvents.Random(rnd,year,MaxYear);			
		} while(year<MaxYear);
		
		return {
			Log:this._Log,
			Countries:this._Countries			
		}
	}
}

