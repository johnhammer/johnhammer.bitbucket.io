var MaxYear = 1000;
	var Countries = {};
	var Log = [];


var HistoryTextMaker = {
	Disasters : [
		'A volcanic eruption',
		'A earthquake',
		'A meteorite strike',
		'A nerarby supernova',
		'A great flood',
		'A great drought',
		'A plague',
		'A climate change',
	],
	Technologies1:[
		'agriculture',
		'craftsmanship',
		'metallurgy',
		'weaponry'
	],
	Technologies2:[
		'architecture',
		'navigation',
		'education',
		'medicine'
	],
	Technologies3:[
		'industry',
		'transportation',
		'chemistry',
		'phisics'
	],
	Technologies4:[
		'computers',
		'electronics',
		'comunications',
		'politics'
	],	
	GetTechnology : function(year,MaxYear){
		var tech = HistoryTextMaker.Technologies1;
		if(year>=MaxYear/4){
			tech = tech.concat(HistoryTextMaker.Technologies2);
		}
		if(year>=MaxYear/2){
			tech = tech.concat(HistoryTextMaker.Technologies3);
		}
		if(year>=MaxYear/4*3){
			tech = tech.concat(HistoryTextMaker.Technologies4);
		}
		return tech;
	},	
	ReadLog : function(log){
		var result = "";
		for (var i=0; i < log.length; i++) {
			result += "<p>"+HistoryTextMaker.EventText(log[i]) + "</p>";
		}
		return result;
	},
	EventText : function(event){
		var result = event.year + ": ";
		switch(event.type){			
			case HistoryEvents.Type.CivilWar : 
				if(event.newlands.length==0){
					result+= `A rebellion is suffocated in ${event.country.Name}`;
				}
				else if (event.newlands.length==1){
					result+= `A rebellion succeeds in ${event.country.Name}, now to be called ${event.newlands[0].Name}`;
				}
				else{
					result+= `A civil war breaks ${event.country.Name} into smaller nations: ${event.newlands.map(x=>x.Name).join(', ')}`;
				}		
				break;
			case HistoryEvents.Type.War : 
				result+= `${event.country.Name} invades ${event.other.Name} `;
				if(event.win){
					if(event.minor){
						result+= `earning a minor victory over it.`;
					}
					else{
						result+= `and conquers it.`;
					}
				}
				else{
					if(event.minor){
						result+= `but the invasion fails.`;
					}
					else{
						result+= `but the invasion fails and ${event.other.Name} conquers ${event.country.Name} in retaliation.`;
					}
				}
				break;
			case HistoryEvents.Type.PowerDown : 
				result+= `${event.country.Name} suffers a famine.`;
				break;
			case HistoryEvents.Type.PowerUp : 
				result+= `${event.country.Name} enjoys a golden age.`;
				break;
			case HistoryEvents.Type.Aliance : 
				if(event.country == event.other){
					result+= `A population minority is assimilated in ${event.country.Name}.`;
				}
				else if(event.former){
					result+= `${event.country.Name} and ${event.other.Name} sign a peace treaty.`;
				}
				else{
					result+= `${event.country.Name} and ${event.other.Name} sign an alliance.`;
				}
				break;
			case HistoryEvents.Type.Treason : 
				if(event.country == event.other){
					result+= `Genocide of a population minority in ${event.country.Name}.`;
				}
				else if(event.former){
					result+= `${event.country.Name} breaks its alliance with ${event.other.Name}.`;
				}
				else{
					result+= `${event.country.Name} threatens to destroy ${event.other.Name}.`;
				}
				break;
			case HistoryEvents.Type.Merge : 
				if(event.self){
					result+= `${event.country.Name} establishes a colony.`;
				}
				else{
					result+= `${event.merged.Name} is peacefully assimilated into ${event.country.Name}.`;
				}
				break;
			case HistoryEvents.Type.Disaster : 
				result+= `${event.detail} causes considerable devastation to all countries.`;
				break;
			case HistoryEvents.Type.Technology : 
				result+= `All countries benefit from an improvement in ${event.detail}.`;
				break;
		}		
		if(event.intervention){
			result+=HistoryTextMaker.Intervention(event);
		}		
		return result;
	},
	Intervention : function(events){
		return " INTERVENTION";
	}
}
