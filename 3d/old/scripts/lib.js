//lib
var LIB = {
	Capitalize : function(string){		
		return string.charAt(0).toUpperCase() + string.slice(1);
	},
	Replace : function(str, find, replace){
		return str.replace(new RegExp(find, 'g'), replace);
	},
	Normalize(number1,number2){
		return Math.floor((number1 / number2) * 100)
	},
	Guid : function() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	},
	NextChar(c) {
		return String.fromCharCode(c.charCodeAt(0) + 1);
	},
	RemoveIndex(array,index) {
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	},
	
	Clone : function(obj){
		return JSON.parse(JSON.stringify(obj));
	},
	FastClone(aObject) { //no recursivity
		if (!aObject) {
			return aObject;
		}
		let v;
		let bObject = Array.isArray(aObject) ? [] : {};
		for (const k in aObject) {
			v = aObject[k];
			bObject[k] = (typeof v === "object") ? LIB.FastClone(v) : v;
		}
		return bObject;
	},
	PointDist(x1,y1,x2,y2){ 
	  if(!x2) x2=0; 
	  if(!y2) y2=0;
	  return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); 
	}
}