var MaterialsFactory = {
	Textures : {
		Wall:{url:"0outwall"},
		Floor:{url:"0floor"},
		Dirt:{url:"0moss"},
		Bush:{url:"0bush2"},
		Tree:{url:"tree"},
		Leaves:{url:"leaves",ext:"png"},
	},
	Get : function(tex,opacity=1){
		
		
		return new THREE.MeshPhongMaterial({
		  map: tex.val,
		  side: THREE.DoubleSide,
		  opacity:opacity,
		  transparent:opacity<1
		  
		});
	},
	Load : function(promises){
		var path = 'assets/textures/';

		var textureLoader = new THREE.TextureLoader();
		for (var key in MaterialsFactory.Textures) {
			promises.push(new Promise((resolve, reject) => {
				var entry = MaterialsFactory.Textures[key]
				var url = path + entry.url + '.' +(entry.ext?entry.ext:'jpg');
				textureLoader.load(url,
					texture => {
						texture.repeat.set(1, 1);
						texture.wrapS = THREE.RepeatWrapping;
						texture.wrapT = THREE.RepeatWrapping;
						entry.val = texture;
						resolve(entry);
					},
					xhr => {
						console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
					},
					xhr => {}
				)	  
			}));
		}
	}
}