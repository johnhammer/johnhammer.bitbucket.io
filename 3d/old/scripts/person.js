
var ProcConv = {
	Create(person){
		
	},
	Write(conv){
		
	}
}

var Person = {
	NamePrefix : [
		'ka','jo','al','to','an','as','ro','ai'
	],
	NameSuffix : [
		'var','nas','ber','fred','tor','que','nio','ton'
	],
	PatronymSuffix : [
		'ovich','son','ez','sen','ov','ian','fi','atos'
	],
	GenderSuffix : [
		'na','noa','la','ta','sandra','ria','riana','ana'
	],	
	Name(rnd,isMale){
		var name = Random.Choice(rnd,Person.NamePrefix) + Random.Choice(rnd,Person.NameSuffix);
		if(!isMale){
			name = Random.Choice(rnd,Person.NamePrefix) + Random.Choice(rnd,Person.GenderSuffix);
		}		
		name = LIB.Capitalize(name);
		return name;
	},
	Surname(rnd){
		var name = Person.Name(rnd,true) + Random.Choice(rnd,Person.PatronymSuffix);
		return name;
	},
	Personality(rnd){
		return{
			Ego:			Random.Range(rnd,0,10),//value of self
			Sheep:			Random.Range(rnd,0,10),//factor aquitances opinions
			Active:			Random.Range(rnd,0,10),//willimg to do things
			Materialist:	Random.Range(rnd,0,10),//gains vs moral			
			Cheerful:		Random.Range(rnd,0,10),//mood up
			Emotional:		Random.Range(rnd,0,10),//easy to anger & mood change
			Extroverted:	Random.Range(rnd,0,10),//willing to meet new people
			Erratic:		Random.Range(rnd,0,10) //random
		}
	},
	Stats(rnd,age){
		var statsPoints = age * 2;
		var remainingPoints = statsPoints;		
		var points = Random.Distribute(rnd,12,statsPoints);		
		return{
			Logic:			points[0],
			Social:			points[1],
			Learning:		points[2],
			Creative:		points[3],			
			Reaction:		points[4],
			Precision:		points[5],
			Perception:		points[6],
			Coordination:	points[7],			
			Strenght:		points[8],
			Stamina:		points[9],
			Speed:			points[10],
			Resistance:		points[11]
		}
	},
	Skills(rnd,age,stats){
		
	},
	Relation(rnd,me,you){
		me.Relations[you.Id] = {
					
		};
		you.Relations[me.Id] = {
					
		};
	},
	Relations(rnd,person,people){
		var amount = Random.Range(rnd,person.Age,person.Age+8) 
			- 7 
			- Object.keys(person.Relations).length;
		
		if(amount<1) return;
		
		var aquitances = Random.Multichoice(rnd,Object.keys(people),amount,amount);
		
		for(var i=0;i<aquitances.length;i++){
			Person.Relation(rnd,person,people[aquitances[i]]);
		}
	},
	Brain(rnd,person){
		return {
			
		}		
	},
	Create(rnd,id){
		var age = Random.Range(rnd,8,12);
		var isMale = Random.Bool(rnd);
		var name = Person.Name(rnd,isMale);
		var surname = Person.Surname(rnd);
		var personality = Person.Personality(rnd);
		var stats = Person.Stats(rnd,age);
		var skills = Person.Skills(rnd,age,stats);
		return {
			Id:id,
			Name:name,
			Surname:surname,
			IsMale:isMale,
			Age:age,
			Personality:personality,
			Stats:stats,
			Skills:skills,
			Brain:null,
			Relations:{},
		}
	},
	Write(p,people){
		var result = "";		
		var content = "";				
		
		content = "";
		if(p.IsMale){
			content += `<p>Male</p>`;
		}
		else{
			content += `<p>Female</p>`;
		}
		content += `<p>Age: ${p.Age}</p>`;
		result += Frontend.Collapsible("Info:","<ul>"+content+"</ul>");
		
		
		content = "";
		for(var i in p.Personality){	
			content += `<li>${i}:${p.Personality[i]}</li>`;
		}
		result += Frontend.Collapsible("Personality:","<ul>"+content+"</ul>");
		
		content = "";
		for(var i in p.Stats){	
			content += `<li>${i}:${p.Stats[i]}</li>`;
		}
		result += Frontend.Collapsible("Stats:","<ul>"+content+"</ul>");	
		
		content = "";
		for(var i in p.Relations){	
			content += `<li>${people[i].Name} ${people[i].Surname}</li>`;
		}
		result += Frontend.Collapsible("Relations:","<ul>"+content+"</ul>");		
		
		result += "<hr>"
		
		result += `
			<button type="button" onclick="Conversation.Start(p.Id)">Conversation</button>
			<div id="conversation_container" class="content"></div>
		`;
		
		content = `
			<div class="card">
				<h3>${p.Name} ${p.Surname}</p></h3>
				${result}
			</div>
		`;
		
		return content;
	}
}