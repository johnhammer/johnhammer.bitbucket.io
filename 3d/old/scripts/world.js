var World = {
	CreatePeople(rnd,amount){
		var people = {};
		for(var i=0;i<amount;i++){
			var id = LIB.Guid();
			people[id] = Person.Create(rnd,id);
		}
		for(var id in people){
			Person.Relations(rnd,people[id],people);
			people[id].Brain = Person.Brain(rnd,people[id]);
		}
		return people;
	},
	Create(rnd){
		var terrainSeed = Random.Int(rnd);
		var amountOfPeople = Random.Range(rnd,2,4);
		var people = World.CreatePeople(rnd,amountOfPeople);
						
		//var history = HistoryEvents.Tick(rnd,2000);
		
		return {
			People:people,
			//History:Log
		};
	},
	Write(w){
		var people = "";
		for (var i in w.People) {
			people += Person.Write(w.People[i],w.People);
		}
		
		var result = `
			<div class="row">
				${people}
			</div>		
		`;
		return result;
	}
}