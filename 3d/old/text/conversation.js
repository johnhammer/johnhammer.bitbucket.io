var CONVERSATION = {
	
DREAM_ENERGY_EXPLANATION:`
Everyone living in this world is a source of Energy. 
It gives you the ability to do almost anything you want, you can manipulate matter, space and even time with it.
	You mean, I can do all that too?
		Yes. You will learn to use it when the time comes.		
	So then, everyone is like a god?
		Amost everyone has a limited amount of energy. If you run out of Energy, you disappear from the world.
			You die?
				Death and the end of life are different concepts in this world.
					I don't get it.
						You will, in due time.
			Almost everyone?
				There is a person in this world, that draws their enery from another world, thus making it virtually infinite.
					Who is that?
						Maybe you will get to know him, would you choose to stay.
`,
	
DREAM_EXPLANATION:`
There are three concentric circles that form this world: Lucidia, Fractalia and the Shell.
	Tell me about Lucidia.
		Lucidia, the world of dreams, is where the Dreamers live. 
		It is an ideal world with no fears and ruled by creativity.
			How does it look like?
				The center of the circle is the City of Dreams, surounded by farms and small towns that slowly leave way for green pastures and a forest, that grows denser the further you stray to the center.
					Tell me about the City of Dreams.
						Most of the Dreamers live there, albeit some of them prefer the quieter life in the suroundings.
						The City of Dreams is a metropolis where, the closer you get to the center, the taller the buildings get. In the very center, the skyline merges itself with the very sky.							
					What happens if you reach the edge of the forest?
						Then you fall down to Fractalia.
						And you stop being a Dreamer to become a Worker. You can 'never' again go back to the world of dreams, now you face the fear of reality.
							What fear?
								The Nightmare.
									What is the Nightmare?
										Are you sure you want to know that?
											Yes.
											Wait... Maybe not yet.
							You say never in a weird way.
								A Worker can't go back to being a Dreamer, but he could become a Soldier.
									Who are the Soldiers?
										The Soldiers use their Energy to fight. They risk their lives to protect the Shell from the outside.
											What is there outside the Shell?
												#The Nightmare.
											What is this Energy you keep talking about?
												##DREAM_ENERGY_EXPLANATION
							Who are the Workers?
								The Workers use their Energy in the machines of Fractalia. 
								They live in constant fear that the Shell might break, so they work as hard as they can to keep the world powered.
									#What is this Energy you keep talking about?
			Who are the Dreamers?
				The Dreamers are the very best amongst us, and as such they get to live without fear or responsability in Lucidia.
				They use their Energy to create and imagine things, like gods and artists.
				But their life is a lie. The sky they see is painted in the borders of their world, of which they know nothing about.
				That is the price they pay for a perfect life. Some of them seek out the truth, and fall off the edge.	
					#What happens if you reach the edge of the forest?
					#What is this Energy you keep talking about?
	What is there in Fractalia?
		Fractalia holds together the structure of the world and powers Lucidia.
		Workers live there, in fear of going further out into the Shell. 
			How does it look like?
				A superstructure of machines covers the sky and the ground below you.
				Fractal shapes and eternally moving parts give this region its name.
					How is all of this powered? 
						Below the world there is the world's Core, that keeps it afloat and alive.
							How is that possible?
								The Core uses the power of the Dreamers and the machines of Fractalia to give energy to the world.	
			#Who are the Workers?
	What is the Shell?
		The Shell is run by the Soldiers. 
		It's made out of various layers that can self regenerate.
			What is it protecting us from?
				#The Nightmare.
			#What is there outside the Shell?
			#Who are the Soldiers?
	Where are we right now?
		We are in a separate place above Lucidia, called the Palace.
		There is a staircase that goes down to the center main circle, and below.
			#Tell me about Lucidia.
			#How is all of this powered?#Below?	
`,
FIGHTING_BOOK:`
Basic personal defense
	Introduction
		If you get involved in a fight, you have to attack and defend yourself from your oponent.
	How to attack
		When you see the red bar is full on the screen, press 1 to attack.
	How to defend
		When your opponent attacks you, you can block his attack by pressing 2.
	How to counterattack
		You can use a special attack, if you press 3 right after you blocked an enemy strike.
	On techniques
		You can change your moves and improve your fighting if you learn martial arts.
`,
CARDS_BOOK:`
How to play 7 and a half
	Introduction
		The objective of the game is get as close as possible to 7 and a half, without suprassing it.
		Every play a card is drawed, up to 3 cards. Cards above 9 have a value of a half.
	On bets
		The game is played with bets. Once a card is drawed, you might double your bet.
		If your oponent has a bet you don't want to accept, you can retire for the round, loosing your current bet.
		In the third round you have the option to bet all in.
	How to win
		You can win the round if all your oponents retire, or if at the third round your cards are the closest to 7 and a half.
	Players
		The game can be played by 2 up to 8 people.
`,
HASAMI_BOOK:`
My first Hasami book
	Introduction
		Hasami is a board game that is played in a 8x8 board.
		Every player has 8 pieces, and plays by turns.
	How to move
		Pieces can move any amount of pieces vertically or horizontally, without jumping over other pieces, like a rook in chess.
	How to capture
		You can capture one or more enemy pieces by surounding them with yours either vertically or horizontally.
		A piece in the corner can be captured by being cornered.
	How to win
		The first player with an advantage of 3 pieces wins the game.
`,
CIRCUIT_BOOK:`
Digital electronics
	Introduction
		Most modern machines use digital electronics.
		You can easily build or alter this circuits yourself using gates and cable.
	Circuits
		A circuit has a series of inputs and outputs.
		The circuit will activate if all the given combinations are fullfiled.
		You can solve this by connecting parts of the circuit and if needed, adding gates.
		There are many ways to solve the same circuit.
	Gates
		You can find digital gates in supermarkets and shops.
		They weight very little to carry around and are fairly cheap.
	NOT gate
		The NOT gate activates with an inactive signal, and deactivates with an active signal.
	AND gate
		The AND gate activates with two active signals, and deactivates in all other cases
	OR gate
		The OR gate activates with atleast one out of the two signals is active.
`,


//on outland
OUTLAND1:`
#hi!
	#hi
		Oh, you can speak in #word#
		#word speak ?
			#speak forest word
				END
			#grandmother word friend speak#
				END
			#word little speak
				END
			I'm sorry, I don't understand.
				END
			Just kidding, I have no idea what you are saying.
				END
	What was that?
		END
	Speak to me in my tongue!
		END
`
/*
You place the verb at the end of the phrase. For example, you say ## 
*/
}


/*
Answer("you my friend be",true);
Line("i don't understand you",false);
Line("no to_you understand",true);
Line("no word big speak",true);
Line("money to_me give",true);
Line("hi , my name johan be , your name what be ?",true);

*/