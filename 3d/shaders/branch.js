// Author @patriciogv - 2015
// http://patriciogonzalezvivo.com


#ifdef GL_ES
precision mediump float;
#endif
uniform vec2 u_resolution;


float f(float x){
    if(x<1.0){
        //return 1.0;
    }
    return -pow(x,1.45)+5.0*x-sin(x*3.0);
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    
    float offsetY = 50.0;
    float offsetX = 80.0;
    float scaleY = 220.0;
    float scaleX = 150.0;
    float repetition = scaleY*0.35;
    
    
    float fx = f(st.x*scaleX-offsetX);
    float y = st.y*scaleY;
    
    if(st.x==offsetX/scaleX){
        fx = 1.0;       
    }
    if(st.x<offsetX/scaleX){
       fx = f(-st.x*scaleX+offsetX);
        y+=0.55*scaleY;
    }
    
   
    
    bool v = mod(y,repetition)<fx+offsetY && mod(y,repetition)>-fx+offsetY; 
    
    vec3 color = vec3(0.0);
    if(v){
        color = vec3(1.0);
    }
	gl_FragColor = vec4(color,1.0);
}
