/*
dirt
180,165,162
171,160,154
168,153,148

*/
var ColorFactory = {
	Sand(rnd){
		var r = Random.Range(rnd,245,255);
		var g = Random.Range(rnd,215,255);
		var b = Random.Range(rnd,150,170);
		return {r:r,g:g,b:b}
	},
	Brick(rnd){
		var r = Random.Range(rnd,128,255);
		var g = Random.Range(rnd,100,128);
		var b = Random.Range(rnd,0,0);
		return {r:r,g:g,b:b}
	},
	Concrete(rnd){
		var g = Random.Range(rnd,64,212);
		return {r:g,g:g,b:g}
	},
	Forest(rnd){
		var r = Random.Range(rnd,0,0);
		var g = Random.Range(rnd,0,255);
		var b = Random.Range(rnd,0,0);
		return {r:r,g:g,b:b}
	},
	Leaf(rnd){
		var r = Random.Range(rnd,0,20);
		var g = Random.Range(rnd,20,235);
		var b = Random.Range(rnd,0,20);
		return {r:r,g:g,b:b}
	},
	Birch(rnd){
		var r = Random.Range(rnd,65,236);
		var g = Random.Range(rnd,39,206);
		var b = Random.Range(rnd,16,179);
		return {r:b+60,g:b+30,b:b}
	},
}