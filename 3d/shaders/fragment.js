var Shader_Fragment = {
	ProcBranch2:()=>`
uniform vec2 u_resolution;
varying vec4 localPosition;
varying vec4 worldPosition;
varying vec4 viewPosition;
uniform vec3 color1;
uniform vec3 color2;
uniform float f1;
uniform float f2;
uniform float f3;

float f(float x){
    return -pow(x,f1)+f2*x-sin(x*f3);
}

void main(){
    vec2 st = localPosition.yx/0.1+vec2(.5);
    
    float offsetY = 1.0;
    float offsetX = 1.0;
    float scaleY = 10.0;
    float scaleX = 10.0;
    
    float fx = f(st.x*scaleX-offsetX + st.y*scaleY-offsetY);
	
    float y = st.y*scaleY - st.x*scaleX;
    
    bool v = y<fx && y>-fx; 
	if(!v){
		discard;
	}
    vec3 color = mix(color1,color2,(st.x + st.y)/10.0);
	gl_FragColor = vec4(color,v);
}
	`,
	ProcBranch:()=>`
uniform vec2 u_resolution;
varying vec4 localPosition;
varying vec4 worldPosition;
varying vec4 viewPosition;


float f(float x){
    return -pow(x,1.45)+5.0*x-sin(x*3.0);
}

void main(){
    vec2 st = localPosition.yx/0.1+vec2(.5);
    float offsetY = 25.0;
    float offsetX = 0.0;
    float scaleY = 20.0;
    float scaleX = 10.0;
    float repetition = scaleY*5.0;
    
    
    float fx = f(st.x*scaleX-offsetX);
    float y = st.y*scaleY;
    
    if(st.x==offsetX/scaleX){
        fx = 1.0;       
    }
    if(st.x<offsetX/scaleX){
		fx = f(-st.x*scaleX+offsetX);
        y+=2.0*scaleY;
    }
    
    bool v = mod(y,repetition)<fx+offsetY && mod(y,repetition)>-fx+offsetY; 
    
    vec3 color = vec3(0.0);
    if(v){
        color = vec3(0.0,fx/100.0,0.0);
    }
	gl_FragColor = vec4(color,v);
}
	`,
	
	
	
	Branch:()=>`
uniform vec2 u_resolution;
varying vec3 objectPosition;
void main(){
	
    vec2 st = objectPosition.xy;

    vec2 pos = vec2(0.0)-st;

    float r = length(pos)*2.0;
    float a = atan(pos.y,pos.x);

    float f = cos(a*3.);
    // f = abs(cos(a*3.));
    // f = abs(cos(a*2.5))*.5+.3;
    // f = abs(cos(a*12.)*sin(a*3.))*.8+.1;
    // f = smoothstep(-.5,1., cos(a*10.))*0.2+0.5;

	vec3 color = vec3(0.0,1.0,0.0);
    gl_FragColor = vec4(color,1.-smoothstep(f,f+0.02,r));
}	
	`,
	Dirt:()=>`
uniform vec3 color1;
uniform vec3 color2;
varying vec3 worldPosition;
uniform float scale;

${Shader_LIB.Noise()}

void main() {
  float n = cnoise(worldPosition * scale);
  vec3 color = mix(color1,color2,n);
  gl_FragColor = vec4(color, 1.0);
}
	`,
	Brick:()=>`
uniform vec2 u_resolution;
varying vec3 objectPosition;
uniform vec3 color1;
uniform vec3 color2;
uniform vec3 color3;
uniform vec3 color4;
varying vec3 worldPosition;

${Shader_LIB.Noise()}

vec2 brickTile(vec2 _st, float _zoom){
    _st *= _zoom;
    _st.x += step(1., mod(_st.y,2.0)) * 0.5;
    return fract(_st);
}
float box(vec2 _st, vec2 _size){
    _size = vec2(0.5)-_size*0.5;
    vec2 uv = smoothstep(_size,_size+vec2(1e-4),_st);
    uv *= smoothstep(_size,_size+vec2(1e-4),vec2(1.0)-_st);
    return uv.x*uv.y;
}
void main(){
    vec2 st = vec2(objectPosition.x+objectPosition.z,objectPosition.y)/u_resolution.xy;
    st /= vec2(2.15,0.65)/1.5;
    st = brickTile(st,5.0);
	float n = cnoise(worldPosition * 50.0);
	vec3 brickColor = mix(color1,color2,n);
	vec3 concreteColor = mix(color3,color4,n);
	vec3 color = mix(concreteColor,brickColor,box(st,vec2(0.9)));
	gl_FragColor = vec4(color, 1.0);
}
	`,
	Birch:()=>`
uniform vec2 u_resolution;
varying vec3 objectPosition;
uniform vec3 color1;
uniform vec3 color2;
varying vec3 worldPosition;

${Shader_LIB.Voronoi()}

void main() {
	vec3 net = voronoi(worldPosition,vec2(0.01,0.6));
	vec3 result = mix(color1,color2,net);
	gl_FragColor = vec4(result, 1.0);
}
	
	`,
	
	Grass:()=>`
varying vec3 worldPosition;	
varying vec3 objectPosition;	
	
${Shader_LIB.Noise()}
${Shader_LIB.Voronoi()}

vec3 color(float r,float g,float b){
	return vec3(r/255.0,g/255.0,b/255.0);
}

vec3 colorNoise(float scale, vec3 c1, vec3 c2){
	float n = cnoise(worldPosition * scale);
	return mix(c1,c2,n);
}



void main() {
	vec3 dirt = colorNoise(50.0,
		color(69.0,59.0,25.0),
		color(138.0,125.0,135.0)
	);	
	vec3 moss = colorNoise(50.0,
		color(71.0,102.0,43.0),
		color(137.0,164.0,97.0)
	);	
	vec3 net = voronoi();
	vec3 result = colorNoise(10.0,dirt,net);
	/*
	float branch = cnoise(worldPosition * 10.0);	
	float branch2 = cnoise(worldPosition * 1.5);	
	if(branch<0.2 && branch>0.05 && branch2<0.3 && branch2>0.1){
		result = color(215.0,200.0,215.0);
		result.x *= branch2;
		result.y *= branch2;
		result.z *= branch2;
	}
	float branch3 = cnoise(worldPosition * 6.0);	
	float branch4 = cnoise(worldPosition * 5.0);	
	if(branch3<0.1 && branch3>0.05 && branch4<0.3 && branch4>0.1){
		result = color(255.0,220.0,255.0);
		result.x *= branch2;
		result.y *= branch2;
		result.z *= branch2;
	}
	*/
	gl_FragColor = vec4(result, 1.0);
}
	`,	
	
	
	Water:()=>`
varying vec3 worldPosition;
  uniform float iGlobalTime;
  uniform vec2 iResolution;

  const int NUM_STEPS = 8;
  const float PI	 	= 3.1415;
  const float EPSILON	= 1e-3;
  float EPSILON_NRM	= 0.1 / iResolution.x;

  // sea variables
  const int ITER_GEOMETRY = 3;
  const int ITER_FRAGMENT = 5;
  const float SEA_HEIGHT = 0.6;
  const float SEA_CHOPPY = 1.0;
  const float SEA_SPEED = 1.0;
  const float SEA_FREQ = 0.16;
  const vec3 SEA_BASE = vec3(0.1,0.19,0.22);
  const vec3 SEA_WATER_COLOR = vec3(0.8,0.9,0.6);
  float SEA_TIME = iGlobalTime * SEA_SPEED;
  mat2 octave_m = mat2(1.6,1.2,-1.2,1.6);

  mat3 fromEuler(vec3 ang) {
    vec2 a1 = vec2(sin(ang.x),cos(ang.x));
    vec2 a2 = vec2(sin(ang.y),cos(ang.y));
    vec2 a3 = vec2(sin(ang.z),cos(ang.z));
    mat3 m;
    m[0] = vec3(
    	a1.y*a3.y+a1.x*a2.x*a3.x,
    	a1.y*a2.x*a3.x+a3.y*a1.x,
    	-a2.y*a3.x
    );
    m[1] = vec3(-a2.y*a1.x,a1.y*a2.y,a2.x);
    m[2] = vec3(
    	a3.y*a1.x*a2.x+a1.y*a3.x,
      a1.x*a3.x-a1.y*a3.y*a2.x,
      a2.y*a3.y
    );
    return m;
  }

  float hash( vec2 p ) {
    float h = dot(p,vec2(127.1,311.7));	
    return fract(sin(h)*43758.5453123);
  }

  float noise( in vec2 p ) {
    vec2 i = floor(p);
    vec2 f = fract(p);	
    vec2 u = f * f * (3.0 - 2.0 * f);
    return -1.0 + 2.0 * mix(
    	mix(
      	hash(i + vec2(0.0,0.0)
      ), 
    	hash(i + vec2(1.0,0.0)), u.x),
    	mix(hash(i + vec2(0.0,1.0) ), 
    	hash(i + vec2(1.0,1.0) ), u.x), 
      u.y
    );
  }

  float diffuse(vec3 n,vec3 l,float p) {
    return pow(dot(n,l) * 0.4 + 0.6,p);
  }

  float specular(vec3 n,vec3 l,vec3 e,float s) {    
    float nrm = (s + 8.0) / (3.1415 * 8.0);
    return pow(max(dot(reflect(e,n),l),0.0),s) * nrm;
  }

  vec3 getSkyColor(vec3 e) {
    e.y = max(e.y, 0.0);
    vec3 ret;
    ret.x = pow(1.0 - e.y, 2.0);
    ret.y = 1.0 - e.y;
    ret.z = 0.6+(1.0 - e.y) * 0.4;
    return ret;
  }


  float sea_octave(vec2 uv, float choppy) {
    uv += noise(uv);         
    vec2 wv = 1.0 - abs(sin(uv));
    vec2 swv = abs(cos(uv));    
    wv = mix(wv, swv, wv);
    return pow(1.0 - pow(wv.x * wv.y, 0.65), choppy);
  }

  float map(vec3 p) {
    float freq = SEA_FREQ;
    float amp = SEA_HEIGHT;
    float choppy = SEA_CHOPPY;
    vec2 uv = p.xz; 
    uv.x *= 0.75;

    float d, h = 0.0;    
    for(int i = 0; i < ITER_GEOMETRY; i++) {        
      d = sea_octave((uv + SEA_TIME) * freq, choppy);
      d += sea_octave((uv - SEA_TIME) * freq, choppy);
      h += d * amp;        
      uv *= octave_m;
      freq *= 1.9; 
      amp *= 0.22;
      choppy = mix(choppy, 1.0, 0.2);
    }
    return p.y - h;
  }

  float map_detailed(vec3 p) {
      float freq = SEA_FREQ;
      float amp = SEA_HEIGHT;
      float choppy = SEA_CHOPPY;
      vec2 uv = p.xz;
      uv.x *= 0.75;

      float d, h = 0.0;    
      for(int i = 0; i < ITER_FRAGMENT; i++) {        
        d = sea_octave((uv+SEA_TIME) * freq, choppy);
        d += sea_octave((uv-SEA_TIME) * freq, choppy);
        h += d * amp;        
        uv *= octave_m;
        freq *= 1.9; 
        amp *= 0.22;
        choppy = mix(choppy,1.0,0.2);
      }
      return p.y - h;
  }

  vec3 getSeaColor(
  	vec3 p,
    vec3 n, 
    vec3 l, 
    vec3 eye, 
    vec3 dist
  ) {  
    float fresnel = 1.0 - max(dot(n,-eye),0.0);
    fresnel = pow(fresnel,3.0) * 0.65;

    vec3 reflected = getSkyColor(reflect(eye,n));    
    vec3 refracted = SEA_BASE + diffuse(n,l,80.0) * SEA_WATER_COLOR * 0.12; 

    vec3 color = mix(refracted,reflected,fresnel);

    float atten = max(1.0 - dot(dist,dist) * 0.001, 0.0);
    color += SEA_WATER_COLOR * (p.y - SEA_HEIGHT) * 0.18 * atten;

    color += vec3(specular(n,l,eye,60.0));

    return color;
  }

  // tracing
  vec3 getNormal(vec3 p, float eps) {
    vec3 n;
    n.y = map_detailed(p);    
    n.x = map_detailed(vec3(p.x+eps,p.y,p.z)) - n.y;
    n.z = map_detailed(vec3(p.x,p.y,p.z+eps)) - n.y;
    n.y = eps;
    return normalize(n);
  }

  float heightMapTracing(vec3 ori, vec3 dir, out vec3 p) {  
    float tm = 0.0;
    float tx = 1000.0;    
    float hx = map(ori + dir * tx);

    if(hx > 0.0) {
      return tx;   
    }

    float hm = map(ori + dir * tm);    
    float tmid = 0.0;
    for(int i = 0; i < NUM_STEPS; i++) {
      tmid = mix(tm,tx, hm/(hm-hx));                   
      p = ori + dir * tmid;                   
      float hmid = map(p);
      if(hmid < 0.0) {
        tx = tmid;
        hx = hmid;
      } else {
        tm = tmid;
        hm = hmid;
       }
    }
    return tmid;
  }

  void main() {
    vec2 uv;
	
	uv = (worldPosition).zx / vec2(10.0,10.0);
	uv = uv * 2.0 - 1.0;
    //uv.x *= iResolution.x / iResolution.y; 
	
    float time = iGlobalTime * 0.003;

    // ray
    vec3 ang = vec3(
      sin(time*3.0)*0.1,sin(time)*0.2+0.3,time
    );    
    vec3 ori = vec3(0.0,3.5,time*5.0);
    vec3 dir = normalize(
      vec3(uv.xy,-2.0)
    );
    dir.z += length(uv) * 0.15;
    dir = normalize(dir);

    // tracing
    vec3 p;
    heightMapTracing(ori,dir,p);
    vec3 dist = p - ori;
    vec3 n = getNormal(
      p,
      dot(dist,dist) * EPSILON_NRM
    );
    vec3 light = normalize(vec3(0.0,1.0,0.8)); 
	
    // color
    vec3 color = getSeaColor(p,n,light,dir,dist);

    // post
    gl_FragColor = vec4(pow(color,vec3(0.75)), 1.0);
  }
`
}