// Author @patriciogv - 2015
// http://patriciogonzalezvivo.com


uniform vec2 u_resolution;

float f(float x){
    return -pow(x,1.45)+5.0*x-sin(x*3.0);
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    
    float offsetY = 50.0;
    float offsetX = 8.0;
    float scaleY = 120.0;
    float scaleX = 80.0;
    
    float fx = f(st.x*scaleX-offsetX);
    if(st.x<0.1){
        fx= 1.0;
    }
    float y = st.y*scaleY;
    
    bool v = y<fx+offsetY && y>-fx+offsetY;   
    vec3 color = vec3(0.0);
    if(v){
        color = vec3(fx/255.0,fx/50.0,fx/255.0);
        if(st.x<0.1){
            color=vec3(0.2,-st.x*5.0+0.6,0.2);
        }
    }
	gl_FragColor = vec4(color,1.0);
}

/*
//diagonal
#ifdef GL_ES
precision mediump float;
#endif
// Author @patriciogv - 2015
// http://patriciogonzalezvivo.com


uniform vec2 u_resolution;

float f(float x){
    return -pow(x,1.32)+4.0*x-sin(x*1.5);
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    
    float offsetY = 10.0;
    float offsetX = 10.0;
    float scaleY = 50.0;
    float scaleX = 50.0;
    
    float fx = f(st.x*scaleX-offsetX + st.y*scaleY-offsetY);
	if((st.x+st.y)<0.4){
        fx= 1.0;
    }
    float y = st.y*scaleY - st.x*scaleX;
    
    bool v = y<fx && y>-fx; 
    vec3 color = vec3(0.0);
    if(v){
        color = vec3(fx/255.0,v,fx/255.0);
    }
	gl_FragColor = vec4(color,1.0);
}
*/