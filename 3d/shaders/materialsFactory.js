var STORED_MATERIAL;
var MaterialsFactory = {
	ShaderMaterial(vertex,fragment,uniforms){
		var material = new THREE.ShaderMaterial(
		{
		  uniforms : uniforms,
		  vertexShader : vertex,
		  fragmentShader : fragment,
		  dithering:true
		});
		return material;
	},
	ColorUniform(c){
		return { type: "v3", value: new THREE.Vector3(c.r/255,c.g/255,c.b/255)};
	},
	ScalarUniform(v){
		return { type: "f", value: v };
	},	
	
	Colors : {},
	Init(rnd){
		MaterialsFactory.Colors.Sand1 = ColorFactory.Sand(rnd);
		MaterialsFactory.Colors.Sand2 = ColorFactory.Sand(rnd);
		MaterialsFactory.Colors.Brick1 = ColorFactory.Brick(rnd);
		MaterialsFactory.Colors.Brick2 = ColorFactory.Brick(rnd);
		MaterialsFactory.Colors.Concrete1 = ColorFactory.Concrete(rnd);
		MaterialsFactory.Colors.Concrete2 = ColorFactory.Concrete(rnd);		
		MaterialsFactory.Colors.Forest1 = ColorFactory.Forest(rnd);		
		MaterialsFactory.Colors.Forest2 = ColorFactory.Forest(rnd);		
		MaterialsFactory.Colors.Birch1 = ColorFactory.Birch(rnd);		
		MaterialsFactory.Colors.Birch2 = ColorFactory.Birch(rnd);		
	},
	
	TreeBirch(c1,c2){
		var mat = MaterialsFactory.ShaderMaterial(
			Shader_Vertex.Position(true,true),
			Shader_Fragment.Birch(),
		{
			scale: MaterialsFactory.ScalarUniform(10000.0),
			color1 : MaterialsFactory.ColorUniform(c1),
			color2 : MaterialsFactory.ColorUniform(c2)
		});		
		return mat;	
	},
	TreeBranch(c1,c2,f1,f2,f3){
		var material = MaterialsFactory.ShaderMaterial(
			Shader_Vertex.GL(),
			Shader_Fragment.ProcBranch2(),
		{			
			color1 : MaterialsFactory.ColorUniform(c1),
			color2 : MaterialsFactory.ColorUniform(c2),
			f1: MaterialsFactory.ScalarUniform(f1),
			f2: MaterialsFactory.ScalarUniform(f2),
			f3: MaterialsFactory.ScalarUniform(f3),
		});
		material.side= THREE.DoubleSide;
		//material.transparent=true;
		return material;
	},
	BeachSand(){
		return MaterialsFactory.ShaderMaterial(
			Shader_Vertex.Position(true,true),
			Shader_Fragment.Dirt(),
		{
			scale: MaterialsFactory.ScalarUniform(50.0),
			color1 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Sand1),
			color2 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Sand2)
		});		
	},
	ForestGround(){
		return MaterialsFactory.ShaderMaterial(
			Shader_Vertex.Position(true,true),
			Shader_Fragment.Dirt(),
		{
			scale: MaterialsFactory.ScalarUniform(50.0),
			color1 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Forest1),
			color2 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Forest2)
		});		
	},
	SeaWater(){
		return MaterialsFactory.ShaderMaterial(
			Shader_Vertex.Position(true,false),
			Shader_Fragment.Water(),
		timeUniform);	
	},
	BrickWall(){
		return MaterialsFactory.ShaderMaterial(
			Shader_Vertex.Position(true,true),
			Shader_Fragment.Brick(),
		{ 
			u_resolution : {type: "v2", value: new THREE.Vector2(1.0,1.0)},			
			color1 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Brick1),
			color2 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Brick2),			
			color3 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Concrete1),
			color4 : MaterialsFactory.ColorUniform(MaterialsFactory.Colors.Concrete2),
		});	
	},	
	
	Get : function(tex,opacity=1){		
		return new THREE.MeshPhongMaterial({
		  map: null,
		  side: THREE.DoubleSide,
		  opacity:opacity,
		  transparent:opacity<1		  
		});
	},
}