var Shader_Vertex = {
	Position:(world,relative) => `
${world?'varying vec3 worldPosition;':''}	
${relative?'varying vec3 objectPosition;':''}	
void main() {
	${world?'worldPosition = (modelMatrix * vec4(position, 1.0)).xyz;':''}	
	${relative?'objectPosition = normalize(position);':''}	
	gl_Position = projectionMatrix * modelViewMatrix * vec4(position,1.0);
	
}
	`,
	Grass:()=>`

	`,
	GL:()=>`
varying vec4 localPosition;
varying vec4 worldPosition;
varying vec4 viewPosition;
void main(){
   localPosition = vec4( position, 1.);
   worldPosition = modelMatrix * localPosition;
   viewPosition = viewMatrix * worldPosition;
   vec4 projectedPosition = projectionMatrix * viewPosition; 

   gl_Position = projectedPosition;
}
	`,
	
}