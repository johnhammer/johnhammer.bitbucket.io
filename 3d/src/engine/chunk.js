class Chunk {
	constructor(x,y,seed){
		this.ChunkSize = 16;
		this.meshes = [];
		this.bodies = [];
		this.rnd = Random.Create(seed);
		this.x = x;
		this.y = y;
		
		this.createTerrain(x,y);
	}	
	
	createTerrain(x,y){
		var heightmap = this._getHeightMap(x,y,this.ChunkSize,()=>1);			
		var hfBody = Physics.Terrain(heightmap,x,y);		
		var geometry = Graphics.TerrainGeometry(hfBody,x,y);
		Graphics.Terrain(hfBody,x,y,material,geometry);
	}
	
	_getHeightMap(oox,ooy,ChunkSize,f){
		var ox=oox*(ChunkSize-1);
		var oy=-ooy*(ChunkSize-1)
		var matrix = [];
		var sizeX = ChunkSize+1, sizeY = ChunkSize+1; //extra row anti clipping
		for (var i = 0; i < sizeX; i++) {
			matrix.push([]);
			for (var j = 0; j < sizeY; j++) {
				var val = f(i+ox,j+oy);
				matrix[i].push(val+1000); //0.035*ox+0.025*oy
			}
		}		
		return matrix;
	}
	
	static getCoords(x,y){
		var x = Math.floor(pos.x/(Chunk.ChunkSize-1));
		var y = Math.floor(pos.z/(Chunk.ChunkSize-1))+1;
		return { x:x, y:y };
	}
	static pointDist(x1,y1,x2,y2){ 
		if(!x2) x2=0; 
		if(!y2) y2=0;
		return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); 
	}
}