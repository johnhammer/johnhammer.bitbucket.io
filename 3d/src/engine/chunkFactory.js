class ChunkFactory {
	constructor(rnd){
		this.seed = Random.Int(rnd);
	}
	init(){
		
	}
	_cantorPair(x,y) {
		return (0.5 * (x + y) * (x + y + 1)) + y;
	}
	create(x,y,seed){
		var pairedSeed = this._cantorPair(this._cantorPair(x,seed),y);		
		var chunk = new ForestChunk(x,y,pairedSeed);
		return chunk;
		/*
		var shore = 0;
		var beachLimit = 50;
		var downtown = 300;
		var forestLimit = 600;
		
		if(x<shore){
			return TerrainTypes.Sea;
		}
		if(x>=shore && x<beachLimit){
			if(y%10==0){
				return TerrainTypes.BeachSubriver;
			}
			else{
				return TerrainTypes.Beach;
			}		
		}
		if(x==beachLimit){
			return TerrainTypes.BeachTransition;
		}
		if(x>beachLimit && x<downtown){
			if(y%10==0){
				return TerrainTypes.TownSubriver;
			}
			else{
				return TerrainFactory.GetTownColor(x,y);
			}
		}
		if(x==downtown){
			return TerrainTypes.TownSubriver;	
		}
		if(x>downtown && x<forestLimit){
			return TerrainFactory.GetTownColor(x,y);
		}
		if(x==forestLimit){
			return TerrainTypes.ForestLimit;	
		}
		if(x>forestLimit){
			//return TerrainTypes.Forest;	
			return TerrainTypes.Fractalia;
		}
		*/
	}
}