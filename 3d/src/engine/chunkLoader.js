class ChunkLoader {
	constructor(engine,rnd){
		this.chunks = {};		
		this.maxChunks = 256;
		this.dynamicRender = true;
		this.chunkRender = 1;
		this.engine = engine;
		this.chunkFactory = new ChunkFactory(rnd);
	}
	init(){
	}
	loadChunks(x,y){
		for (var i = x-this.chunkRender; i <= x+this.chunkRender; i++) {
			for (var j = y-this.chunkRender; j <= y+this.chunkRender; j++) {
				this._loadChunk(i,j);				
			}
		}
		this._cleanup(x,y);
	}
	
	_loadChunk(x,y){
		if(this.chunks[x+"-"+y]){
			this.chunks[x+"-"+y].t++;
			return;
		}
		var chunk = this.chunkFactory.create(x,y);
		this.engine.addChunk(chunk);	
		this.chunks[x+"-"+y] = chunk;
	}
	_cleanup(x,y){
		if(Object.keys(this.chunks).length>this.maxChunks){
			var lim = Math.pow(this.chunkRender*2+1,1);		
			var remove = Object.keys(this.chunks);
			remove = remove.sort(function(a, b){
				var x = a.split("-");
				var y = b.split("-");
				return Chunk.pointDist(x[0],x[1],y[0],y[1]);
			})
			remove = remove.slice(0,lim);
			for (var i = 0; i < remove.length - 1; i++) {
				var key = remove[i];
				this.engine.removeChunk(this.chunks[key]);					
				delete this.chunks[key];
			}
		}
	}
}