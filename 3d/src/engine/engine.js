class Engine {
	constructor(seed){		
		this.seed = seed;
		this.rnd = Random.Create(seed);
		this.time = Date.now();
		
		//object in player selection
		this.activeObject = null;
		//objects that can be selected
		this.interactionObjects = [];		
		
		this.graphics = new GraphicsEngine();
		this.physics = new PhysicsEngine();
		this.chunkLoader = new ChunkLoader(this,this.rnd);		
		this.player = new Player(this);		
	}
	init(){
		this.graphics.init();
		this.physics.init();
		this.chunkLoader.init();
		
		//start game
		var rnd2 = Random.Create();
		var firstChunkX = Random.Range(rnd2,0,1000);
		var firstChunkY = Random.Range(rnd2,0,1000);
		
		this.chunkLoader.loadChunks(firstChunkX,firstChunkY);		
		this.animate();
	}
	animate(){
		//selection
		var foundObject = this.graphics.checkRaycaster(this.interactionObjects);
		if(foundObject){
			if(this.activeObject){
				this.graphics.deselectObject(this.activeObject);				
			}
			this.activeObject = foundObject;
			this.graphics.selectObject(this.activeObject);
		}
		
		
		requestAnimationFrame(()=>this.animate());	
				
		//animation
		this.graphics.animate();		
		this.physics.animate();		
		this.player.animate(this.time);	

		this.time = Date.now();
	}
	onMove(pos){
		if(this.dynamicRender){
			var coords = Chunk.getCoords(pos.x,pos.z);
			this.chunkLoader.loadChunks(coords.x,coords.y);
		}
	}
	addChunk(chunk){
		
	}
	removeChunk(chunk){
		for (var i = 0; i < chunk.meshes.length; i++) {						
			this.graphics.remove(chunk.meshes[i]);
		}
		for (var i = 0; i < chunk.bodies.length; i++) {
			this.physics.remove(chunk.bodies[i]);
		}
	}
}