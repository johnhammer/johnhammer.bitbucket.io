class GraphicsEngine {
	constructor(engine){
		this.interactionDistance = 10;
		this.camera = null;
		this.scene = null;
		this.renderer = null;
		this.raycaster = null;
		this.lights = [];
	}
	initLights(){
		var ambient = new THREE.AmbientLight( 0xBBAAAA );
			
		var light = new THREE.SpotLight( 0xffffff );
		light.position.set( 10, 30, 20 );
		light.target.position.set( 0, 0, 0 );
		
		this.scene.add( ambient );		
		this.scene.add( light );
		
		this.lights.push(ambient);
		this.lights.push(light);
		
		this.scene.fog = new THREE.Fog( 0xFF00FF, 0, 100 );
		this.renderer.setClearColor( this.scene.fog.color, 1 );
		
	}
	init(){
		//MaterialsFactory.Init(rnd);
		
		//threejs		
		this.camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

		this.scene = new THREE.Scene();
		
		
		this.renderer = new THREE.WebGLRenderer();
		this.renderer.setSize( window.innerWidth, window.innerHeight );
		
		
		this.raycaster = new THREE.Raycaster();

		document.body.appendChild( this.renderer.domElement );
		
		//light
		this.initLights()
		
		/*
		this.controls = new PointerLockControls( this.camera , sphereBody );
		this.scene.add( controls.getObject() );
		*/
	
		//events
		window.addEventListener( 'resize', this.onWindowResize, false );	
	}
	checkRaycaster(objects){
		this.raycaster.setFromCamera({x:0,y:0}, this.camera);
		var intersects = this.raycaster.intersectObjects(objects);
		for (var i = 0; i < intersects.length; i++ ) {
			if(intersects[i].distance<this.interactionDistance){
				return intersects[i];
			}
		}
		return null;
	}	
	deselectObject(object){
		object.material = object.prevMat;
	}
	selectObject(object){
		object.prevMat = object.material;
		object.material = new THREE.MeshBasicMaterial();
	}
	animate(){
		//timeUniform.iGlobalTime.value += clock.getDelta();
		this.renderer.render(this.scene,this.camera);
	}
	onWindowResize() {
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();
		this.renderer.setSize( window.innerWidth, window.innerHeight );
	}
	remove(mesh){
		this.scene.remove(mesh);
	}
}