class PhysicsEngine {
	constructor(engine){
		this.objects = [];
		this.world = new CANNON.World();
	}
	init(){		
		this.world.gravity.set(0,-5,0);		
	}
	animate(){
		var dt = 0.01
		this.world.step(dt);			
		for(var i=0; i<this.objects.length; i++){
			this.objects[i].m.position.copy(this.objects[i].b.position);
			this.objects[i].m.quaternion.copy(this.objects[i].b.quaternion);
		}
	}
	remove(body){
		this.world.removeBody(body);
	}
	terrain(matrix,cx,cy){
		var sizeX = matrix.length-2,
			sizeY = matrix[0].length-2;
			
		var hfShape = new CANNON.Heightfield(matrix, {
			elementSize: 1
		});
		var hfBody = new CANNON.Body({ mass: 0 });
		hfBody.addShape(hfShape);
		hfBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), THREE.Math.degToRad(-90));
		hfBody.position.set(cx*sizeX, 0, cy*sizeY);
		
		this.world.add(hfBody);
		
		return hfBody;
	}
}