class Player {
	constructor(engine){
		this.engine = engine;
		this.controls = new Controls();
	}
	init(){
		this.controls.init();
	}
	onMove(){
		this.engine.onMove(this.pos);
	}
	animate(time){
		//this.controls.update(Date.now()-time);
	}
}