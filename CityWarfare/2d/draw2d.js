var Draw2d = {
	ctx : null,
	Start : function(){
		this.ctx = CANVAS.getContext("2d");
	},
	_Rectangle : function (x,y,color) {		
		this.ctx.beginPath();
		this.ctx.rect(x*CONST.gridXsize,y*CONST.gridYsize, CONST.gridXsize,CONST.gridYsize);
		this.ctx.fillStyle = color;
		this.ctx.fill();
	},
	RectangleSmall : function (x,y,color) {
		this.ctx.beginPath();
		this.ctx.rect(x*CONST.gridXsize+CONST.gridXsize/4,y*CONST.gridYsize+CONST.gridYsize/4, CONST.gridXsize/2,CONST.gridYsize/2);
		this.ctx.fillStyle = color;
		this.ctx.fill();
	},
	_Mask : function (x,y,color) {
		this.ctx.beginPath();
		this.ctx.rect(x*CONST.gridXsize,y*CONST.gridYsize, CONST.gridXsize,CONST.gridYsize);
		this.ctx.strokeStyle = color;
		this.ctx.stroke();
	},
	Text : function(x,y,text){
		this.ctx.beginPath();
		this.ctx.fillStyle = "black";
		this.ctx.font = "20px Arial";
		this.ctx.fillText(text, x*CONST.gridXsize,y*CONST.gridYsize+CONST.gridYsize);
	},
	SmallImage : function (x,y,image){
		var img = document.getElementById(image);
  		this.ctx.drawImage(img, x*CONST.gridXsize+CONST.gridXsize/4,y*CONST.gridYsize+CONST.gridXsize/4);
	},
	BigImage : function(x,y,image){
		var img = document.getElementById(image);
  		this.ctx.drawImage(img, x*CONST.gridXsize,y*CONST.gridYsize);
	},	
	Building : function(x,y,building){
		Draw2d.BigImage(x,y,"bigimage"+building);
	},
}