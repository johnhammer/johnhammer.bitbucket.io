var Interaction2d = {
	Start : function(){
		document.addEventListener( 'mousedown', Interaction2d.OnMouseDown, false );
	},
	OnMouseDown : function(){
		var rect = CANVAS.getBoundingClientRect();
	    var x = Math.floor((event.clientX - rect.left)/CONST.gridXsize);
	    var y = Math.floor((event.clientY - rect.top)/CONST.gridYsize);

		Board.Click(x,y);
	},
}