var Globals = {
	map : null
}

var Const3d = {
	relsize : 50,
}

var Globals3d = {
	camera : null,
	renderer : null,
	scene : null,
	controls : null,
	raycaster : null,
	mouse : null,
	rayobjects : [],
}

var Draw3d = {
	UpdateCanvasSize : function(){
		Globals3d.camera.aspect = window.innerWidth / window.innerHeight;
		Globals3d.camera.updateProjectionMatrix();
		Globals3d.renderer.setSize( window.innerWidth, window.innerHeight );
	},
	_setupCamera : function(){
		Globals3d.camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 10000 );
		Globals3d.camera.position.set( 500, 800, 1300 );
		Globals3d.camera.lookAt( 0, 0, 0 );
		Globals3d.camera.position.z = 10;
	},
	_setupScene : function(){
		Globals3d.scene = new THREE.Scene();
		Globals3d.scene.background = new THREE.Color( 0xf0f0f0 );
	},
	_setupRenderer : function(){
		Globals3d.renderer = new THREE.WebGLRenderer({
			canvas:CANVAS,
			antialias: true
		});
		Globals3d.renderer.setPixelRatio( window.devicePixelRatio );
	},
	_setupControls : function(){
		Globals3d.controls = new THREE.OrbitControls( Globals3d.camera );
		Globals3d.controls.enableRotate = false;
	},
	_setup : function(){
		this._setupCamera();
		this._setupScene();
		this._setupRenderer();
		this._setupControls();
		this.UpdateCanvasSize();
	},
	Render : function() {
	    requestAnimationFrame(Draw3d.Render);
		Globals3d.controls.update();
	    Globals3d.renderer.render(Globals3d.scene, Globals3d.camera);
	},
	Start : function(){
		this._setup();
		Terrain3d.Start();

		var rollOverGeo = new THREE.BoxBufferGeometry( 50, 50, 50 );
			rollOverMaterial = new THREE.MeshBasicMaterial( { color: 0xffffff, opacity: 0.5, transparent: true } );
			rollOverMesh = new THREE.Mesh( rollOverGeo, rollOverMaterial );
			Globals3d.scene.add( rollOverMesh );

		this.Render();
	},
	DrawMapItem : function(mesh,x,y){		
		var offset = Const3d.relsize/4-Const3d.relsize/10;
		
		mesh.position.set(
			(x-offset)*Const3d.relsize,
			Globals.map.heightmap[x][y]+25,
			(y-offset)*Const3d.relsize
		);
		
		Globals3d.scene.add( mesh );
		return mesh;
	},
}