var Interaction3d = {
	Start : function(){
		Globals3d.raycaster = new THREE.Raycaster();
		Globals3d.mouse = new THREE.Vector2();

		document.addEventListener( 'mousemove', Interaction3d.OnMouseMove, false );
		document.addEventListener( 'mousedown', Interaction3d.OnMouseDown, false );
	},
	OnMouseMove : function(){
		event.preventDefault();
		
		let canvasBounds = Globals3d.renderer.context.canvas.getBoundingClientRect();
		Globals3d.mouse.set(( ( event.clientX - canvasBounds.left ) / ( canvasBounds.right - canvasBounds.left ) ) * 2 - 1, 
		- ( ( event.clientY - canvasBounds.top ) / ( canvasBounds.bottom - canvasBounds.top) ) * 2 + 1);

		Globals3d.raycaster.setFromCamera( Globals3d.mouse, Globals3d.camera );

		var intersects = Globals3d.raycaster.intersectObjects( Globals3d.rayobjects );
		if ( intersects.length > 0 ) {
			var intersect = intersects[ 0 ];
			
			rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );

			var offset = CONST.mapSize/2-0.5;
			var x = Math.floor((rollOverMesh.position.x)/50)+8;
			var y = Math.floor((rollOverMesh.position.z)/50)+8;

			var height = Globals.map.heightmap[x][y] + 25;

			rollOverMesh.position.set((x-7.5)*50,height,(y-7.5)*50);
		}
	},
	OnMouseDown : function(){
		event.preventDefault();

		if(event.button == 2){
			return;
		}
		
		let canvasBounds = Globals3d.renderer.context.canvas.getBoundingClientRect();
		Globals3d.mouse.set(( ( event.clientX - canvasBounds.left ) / ( canvasBounds.right - canvasBounds.left ) ) * 2 - 1, 
		- ( ( event.clientY - canvasBounds.top ) / ( canvasBounds.bottom - canvasBounds.top) ) * 2 + 1);


		Globals3d.raycaster.setFromCamera( Globals3d.mouse, Globals3d.camera );

		var intersects = Globals3d.raycaster.intersectObjects( Globals3d.rayobjects );
		if ( intersects.length > 0 ) {
			var intersect = intersects[ 0 ];
			
			rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );
			
			var offset = CONST.mapSize/2-0.5;
			var x = Math.floor((rollOverMesh.position.x)/50)+8;
			var y = Math.floor((rollOverMesh.position.z)/50)+8;

			var height = Globals.map.heightmap[x][y] + 25;

			rollOverMesh.position.set((x-7.5)*50,height,(y-7.5)*50);

			Board.Click(x,y);
		}
	}
}