var MaterialsFactory = {
	Textures : {
		Wall:{url:"stonetex3.jpg"},
		SoftWall:{url:"towerwall2.jpg"},
		Field:{url:"scorch2.jpg"},

		Grass:{url:"g1.jpg"},
		Transition:{url:"transition.jpg"},
		Dirt:{url:"d1.jpg"},
		Wheat:{url:"wheat.png"},
	},
	Get : function(tex){
		return new THREE.MeshPhongMaterial({
		  map: tex.val
		});
	},
	Load : function(promises){
		var path = 'https://johnhammer.bitbucket.io/CityWarfare/assets/textures/';

		var textureLoader = new THREE.TextureLoader();
		for (var key in MaterialsFactory.Textures) {
			promises.push(new Promise((resolve, reject) => {
				var entry = MaterialsFactory.Textures[key]
				var url = path + entry.url// + '.jpg'
				textureLoader.load(url,
					texture => {
						texture.repeat.set(10, 10);
						texture.wrapS = THREE.RepeatWrapping;
						texture.wrapT = THREE.RepeatWrapping;
						entry.val = texture;
						resolve(entry);
					},
					xhr => {
						console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
					},
					xhr => {}
				)	  
			}));
		}
	},
	MakeTransition : function(t1,t2){
		var uniforms = {
			oceanTexture: { type: "t", value: MaterialsFactory.Textures.Grass.val },
			sandyTexture: { type: "t", value: MaterialsFactory.Textures.Dirt.val },
			grassTexture: { type: "t", value: MaterialsFactory.Textures.Grass.val },
			rockyTexture: { type: "t", value: MaterialsFactory.Textures.Dirt.val },
			snowyTexture: { type: "t", value: MaterialsFactory.Textures.Grass.val },
			uFloatArray3 : { type: "fv",  value: [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6 ] },
		};

		var material_shh = new THREE.ShaderMaterial({
			uniforms: uniforms,
			vertexShader: Shaders.VertexMerge,
			fragmentShader: Shaders.FragmentMerge
		});

		return material_shh;
	},
	Basic : function(color = 0xbbbbbb){
		return new THREE.MeshLambertMaterial({color: color});
	},
	Transparent : function(color){
		return new THREE.MeshLambertMaterial({color: color, opacity: 0.5, transparent: true});
	},
	Wall : function(){
		return new THREE.MeshPhongMaterial({
		  map: MaterialsFactory.Textures.Wall.val
		});
	},
	Wheat : function(){

		MaterialsFactory.Textures.Wheat.val.wrapS = THREE.RepeatWrapping;
		MaterialsFactory.Textures.Wheat.val.wrapT = THREE.RepeatWrapping;
		MaterialsFactory.Textures.Wheat.val.repeat.set(1,1);

		return new THREE.MeshPhongMaterial({
		  map: MaterialsFactory.Textures.Wheat.val,
		  transparent: true,
    		side: THREE.DoubleSide,
		});
	},
	Field : function(){
		return new THREE.MeshPhongMaterial({
		  map: MaterialsFactory.Textures.Field.val
		});
	}
}