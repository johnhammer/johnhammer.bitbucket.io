


PrismGeometry = function ( vertices, height ) {

    var Shape = new THREE.Shape();

    ( function f( ctx ) {

        ctx.moveTo( vertices[0].x, vertices[0].y );
        for (var i=1; i < vertices.length; i++) {
            ctx.lineTo( vertices[i].x, vertices[i].y );
        }
        ctx.lineTo( vertices[0].x, vertices[0].y );

    } )( Shape );

    var settings = { };
    settings.amount = height;
    settings.bevelEnabled = false;
    THREE.ExtrudeGeometry.call( this, Shape, settings );

};

/*
var A = new THREE.Vector2( 0, 0 );
var B = new THREE.Vector2( 30, 10 );
var C = new THREE.Vector2( 20, 50 );

var height = 12;                   
var geometry = new PrismGeometry( [ A, B, C ], height ); 

var material = new THREE.MeshPhongMaterial( { color: 0x00b2fc, specular: 0x00ffff, shininess: 20 } );

var prism1 = new THREE.Mesh( geometry, material );
prism1.rotation.x = -Math.PI  /  2;

scene.add( prism1 );
*/

var lib = {
	part : function(geo,h){
		return {m:new THREE.Mesh(geo),s:h};
	},
	flip : function(part){
		part.m.rotation.z = Math.PI/2;		
		return part;
	},
	altcyl : function(r1,r2,h) {
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r1,r2,h, 16, 1, false)),s:h};
	},
	cyl : function(r,h){
		return lib.altcyl(r,r,h);
	},
	prism : function(r,p,h){
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r,r,h, p, 1, false)),s:h};
	},
	sph : function(h) {
		return {m:new THREE.Mesh(new THREE.SphereGeometry(h, 8, 8)),s:h};
	},
	tor : function(h1,h2){
		var tor = new THREE.Mesh(new THREE.TorusGeometry(h1,h2,8,16));
		tor.rotation.x=Math.PI/2;
		return {m:tor,s:1};		
	},
	littleWing : function(size){
		var shape = new THREE.Shape();
		shape.moveTo( size,size );
		shape.lineTo( size,-size/2 );
		shape.lineTo( -size/2,-size/2 );
		var mesh = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, {steps: 1,depth: .2,bevelEnabled: false} ));
		return lib.skew({m:mesh,s:1},3);
	},
	skew : function(part,amt){
		part.m.rotation.z = Math.PI / amt;
		return part;
	},
	merge : function(b,a){
		a.updateMatrix();
		b.merge(a.geometry, a.matrix);
	},	
	assemble :function(parts){
		var geo = new THREE.Geometry();	
		var orig = parts[0] ? -parts[0].s/2 : -parts[1].s/2;
		for ( var i = 0; i < parts.length; i++ ) {	
			if(parts[i]){
				parts[i].m.position.y = orig+parts[i].s/2 + (parts[i].shift||0);
				lib.merge(geo,parts[i].m);
				if(!parts[i].skip) orig += parts[i].s;
			}
		}
		geo.translate(0,-orig/2,0);
		return geo;
	},
	multi: function(object,amount,radius,rotate=false){
		var geo = new THREE.Geometry();
		
		var x0 = 0;
		var y0 = 0;		
		var angle = 0;
		for(var i = 0; i < amount; i++) {
			var x = x0 + radius * Math.cos(2 * Math.PI * i / amount);
			var y = y0 + radius * Math.sin(2 * Math.PI * i / amount);   
			var cyl = object.m.clone();					
			cyl.position.x = x;
			cyl.position.z = y;			
			if(rotate){
				cyl.rotation.y = angle;			
				angle -= 2*Math.PI/amount;	
			}
			lib.merge(geo,cyl);
		}
		
		return {m:new THREE.Mesh(geo),s:object.s};
	},
	multiRot: function(object,amount,radius){
		return lib.multi(object,amount,radius,true)
	},	
	skip(piece){
		piece.skip = true;
		return piece;		
	},
	iif(part,num,lim=1){
		return num<lim ? null : part;
	},
	nif(part,num,lim=1){
		return !num<lim ? null : part;
	},
	f(num,factor){
		return 1+num*factor;
	},
	move(piece,amount){
		piece.shift = amount;
		return piece;
	}
}

GLOBAL = {
	jsonModels : {
		horse 		: {url: 'horse.js'		},
	},
	world:{
		lastTime :null
	},
	animations:[]
}

function LoadObjModels(promises){
	var modelLoader = new THREE.LegacyJSONLoader();
	modelLoader.setCrossOrigin('');
	for (var key in GLOBAL.jsonModels) {
		promises.push(new Promise((resolve, reject) => {
			var entry = GLOBAL.jsonModels[key]
			var url = 'https://johnhammer.bitbucket.io/zeus/assets/' + entry.url
			modelLoader.load(url,
				texture => {
					entry.val = texture;
					resolve(entry);
				},
				xhr => {
					console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
				},
				xhr => {}
			)	  
		}));
	}
}


var ModelsFactory = {

	Horse : function(){

		LoadModel = function(geometry){
			var mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( {
				vertexColors: THREE.FaceColors,
				morphTargets: true
			} ) );
			mesh.scale.set( 0.1,0.1, 0.1 );
			//mesh.rotation.x = Math.PI/2;


			var mixer = new THREE.AnimationMixer( mesh );

			var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'gallop', mesh.geometry.morphTargets, 30 );
			mixer.clipAction( clip ).setDuration( 1 ).play();
			
			GLOBAL.animations.push(mixer);	

			return mesh;
		}
		//return LoadModel(GLOBAL.jsonModels.horse.val);
		return this.Cube(MaterialsFactory.Wall())
	},


	Cube : function(material){
		var size = Const3d.relsize/8;

		var cubeGeo = new THREE.BoxBufferGeometry( size, size, size );
		var cubeMat = material;
		var voxel = new THREE.Mesh( cubeGeo, cubeMat );
		return voxel;
	},
	CubeInterface : function(){
		return ModelsFactory.Cube(MaterialsFactory.Transparent(0x00ffff));
	},
	House : function(){
		var result = new THREE.Mesh();

		var size = Const3d.relsize/2;

		var geo = lib.assemble([
			lib.prism(size/2,4,size/3),
			lib.prism(size/3,4,size/2),
		]);

		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		result.add(mesh);

		return result;
	},
	
	Tower : function(){
		var result = new THREE.Mesh();

		var size = Const3d.relsize/2;
		
		var geo = lib.assemble([
			lib.cyl(size/2,size/2),
			lib.skip(lib.multiRot(lib.flip(lib.prism(size/14,6,size/16)),16,size/2-size/24)),
			lib.cyl(size/3,size/3),
			lib.skip(lib.multiRot(lib.flip(lib.prism(size/14,6,size/16)),8,size/3-size/24))
		]);

		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		result.add(mesh);

		return result;
	},
	Field : function(){
		var result = new THREE.Mesh();

		var size = Const3d.relsize;

		var geo = new THREE.BoxBufferGeometry(size,size/8,size );
		var mesh = new THREE.Mesh(geo,MaterialsFactory.Field());
		//mesh.position.setY(size+size/16);
		result.add(mesh);

		return mesh;
	},




	City : function(){
		var result = new THREE.Mesh();

		var geo = new THREE.BoxBufferGeometry(Const3d.CellSize,Const3d.CellSize/8,Const3d.CellSize );
		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		mesh.position.setY(-Const3d.CellSize/2+Const3d.CellSize/16);
		result.add(mesh);

		return result;
	},
	Cistern : function(){
		var result = new THREE.Mesh();

		var geo = new THREE.BoxBufferGeometry(Const3d.CellSize,Const3d.CellSize/8,Const3d.CellSize );
		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		mesh.position.setY(-Const3d.CellSize/2+Const3d.CellSize/16);
		result.add(mesh);

		return result;
	},
	Granary : function(){
		var result = new THREE.Mesh();

		var geo = lib.assemble([
			lib.multi(lib.cyl(Const3d.CellSize/6,Const3d.CellSize/8),4,Const3d.CellSize/3),
		]);

		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		result.add(mesh);

		return result;
	},
	
	Market : function(){
		var result = new THREE.Mesh();

		var geo = new THREE.BoxBufferGeometry(size,size/8,size );
		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		mesh.position.setY(-size/2+size/16);
		result.add(mesh);

		return result;
	},	
	Workshop : function(){
		var result = new THREE.Mesh();

		var geo = new THREE.BoxBufferGeometry(Const3d.CellSize,Const3d.CellSize/8,Const3d.CellSize );
		var mesh = new THREE.Mesh(geo,MaterialsFactory.Wall());
		mesh.position.setY(-Const3d.CellSize/2+Const3d.CellSize/16);
		result.add(mesh);

		return result;
	},
}