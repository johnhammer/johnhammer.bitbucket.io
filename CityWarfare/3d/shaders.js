var Shaders = {
	
	VertexMerge : `
varying vec2 vUv;
varying vec4 worldCoord;

void main()
{
	vUv = uv;
	vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
	gl_Position = projectionMatrix * mvPosition;

	worldCoord = modelMatrix * vec4( position, 1.0 );

}

	`,
	FragmentMerge : `
		 
uniform sampler2D oceanTexture;
uniform sampler2D sandyTexture;
uniform sampler2D grassTexture;
uniform sampler2D rockyTexture;
uniform sampler2D snowyTexture;

varying vec2 vUv;

float vAmount;
varying vec4 worldCoord;

void main() 
{
	float g = 0.3;
    float b = 0.3;
    g = abs(worldCoord.y / 100.0)+0.1;

    vAmount =g;

    

	vec4 water = (smoothstep(0.0, 0.10, vAmount) - smoothstep(0.20, 0.40, vAmount)) * texture2D( oceanTexture, vUv * 10.0 );
	vec4 sandy = (smoothstep(0.25, 0.35, vAmount) - smoothstep(0.55, 1.0, vAmount)) * texture2D( sandyTexture, vUv * 10.0 );
	gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0) + water + sandy; //, 1.0);
}  
	`,
}