var Terrain3d = {
	Refresh : function(){
		Globals3d.scene.remove(Globals3d.map);
		this._addTerrain();
	},
	_addGrid : function(){
		var gridHelper = new THREE.GridHelper( CONST.mapSize*50, CONST.mapSize*2-1);
		Globals3d.scene.add( gridHelper );
	},
	_addTerrain : function(){
		function UpdateVertex(vertices,value,yy,xx){
			var i = (yy+xx*CONST.mapSize*2);			
			vertices[i].y = value; //vertices[i*3+1] = value;
		}

		function UpdateHeight(x,y,vertices){
			var value = Globals.map.heightmap[x][y];
			
			UpdateVertex(vertices,value,x*2+0,y*2+0);
			UpdateVertex(vertices,value,x*2+1,y*2+0);
			UpdateVertex(vertices,value,x*2+0,y*2+1);
			UpdateVertex(vertices,value,x*2+1,y*2+1);
		}

		function SetMaterial(faces,i,mat){
			faces[i].materialIndex = mat; 
			faces[i+1].materialIndex = mat; 
		}

		function MergeTerrain(x1,y1,x2,y2,faces){
			var v1 = Globals.map.watermap[y1][x1];
			var v2 = Globals.map.watermap[y2][x2];
			
			var value = 1; //merge texture
			if(v1==v2){
				value=v1;
			}

			var xx = x1+(x2-x1)/2;
			var yy = y1+(y2-y1)/2;
			i = yy*4 + xx*(CONST.mapSize*8-4);			
			SetMaterial(faces,i,value);
		}

		function UpdateTerrain(x,y,faces){
			var value = Globals.map.watermap[y][x];

			var i = y*4 + x*(CONST.mapSize*8-4);			
			SetMaterial(faces,i,value);

			if(x>0) MergeTerrain(x,y,x-1,y,faces);
			if(y>0) MergeTerrain(x,y,x,y-1,faces);
			if(x>0&&y>0) MergeTerrain(x,y,x-1,y-1,faces);
		}

		var geometry = new THREE.PlaneGeometry( CONST.mapSize*50, CONST.mapSize*50, CONST.mapSize*2-1, CONST.mapSize*2-1 );
		geometry.rotateX( - Math.PI / 2 );

		var vertices = geometry.vertices;//geometry.attributes.position.array;
		var faces = geometry.faces;

		for(var x = 0;x<CONST.mapSize;x++){
			for(var y = 0;y<CONST.mapSize;y++){
				UpdateHeight(x,y,vertices);
				UpdateTerrain(x,y,faces);
			}
		}

		geometry.computeVertexNormals();		

		var plane = new THREE.Mesh( geometry,  [		
			MaterialsFactory.MakeTransition(MaterialsFactory.Textures.Dirt,MaterialsFactory.Textures.Grass),		
			MaterialsFactory.MakeTransition(MaterialsFactory.Textures.Dirt,MaterialsFactory.Textures.Grass), 					
			MaterialsFactory.MakeTransition(MaterialsFactory.Textures.Dirt,MaterialsFactory.Textures.Grass),
			
			
		]);

		Globals3d.map = plane;
		Globals3d.scene.add( plane );
		Globals3d.rayobjects.push(plane);
	},
	_addSea : function(){
		var waterGeometry = new THREE.PlaneBufferGeometry( CONST.mapSize*50, CONST.mapSize*50);
		var params = {
			color : 0xFFFFFF,
			scale : 1,
			flowX : 0,
			flowY : 0
		}
		water = new THREE.Water( waterGeometry, {
			color: params.color,
			scale: params.scale,
			flowDirection: new THREE.Vector2( params.flowX, params.flowY ),
			textureWidth: 1024,
			textureHeight: 1024
		} );
		water.position.y = 10;
		water.rotation.x = Math.PI * - 0.5;
		Globals3d.scene.add( water );
	},
	_addLight : function(){
		var ambientLight = new THREE.AmbientLight( 0x606060 );
		Globals3d.scene.add( ambientLight );

		var directionalLight = new THREE.DirectionalLight( 0xffffff );
		directionalLight.position.set( 1, 0.75, 0.5 ).normalize();
		Globals3d.scene.add( directionalLight );
	},
	Start : function(){
		this._addLight();
		//this._addGrid();
		this._addTerrain();
		//this._addSea();
	}
}