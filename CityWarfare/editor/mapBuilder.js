var MapBuilder = {
	_Line : function(heightmap,x,y,level){
		if(x<0||y<0||x>=CONST.mapSize||y>=CONST.mapSize||heightmap[x][y]!=0 || LIB.Rnd(0,2)==0){
			return;
		}
		else{
			heightmap[x][y]=level;
			var cell = LIB.Rnd(0,3);
			switch(cell){
				case 0: MapBuilder._Line(heightmap,x+1,y,level);
				case 1: MapBuilder._Line(heightmap,x-1,y,level);
				case 2: MapBuilder._Line(heightmap,x,y+1,level);
				case 3: MapBuilder._Line(heightmap,x,y-1,level);
			}				
		}		
	},
	_DrawLine : function(heightmap,level){
		var startx = LIB.Rnd(0,CONST.mapSize-1);
		var starty = LIB.Rnd(0,CONST.mapSize-1);

		MapBuilder._Line(heightmap,startx,starty,level);
	},
	_FillLevel : function(heightmap,l1,l2){
		MapBuilder._FillExtLevel(heightmap,l1,l2,false)
	},
	_FillExtLevel : function(heightmap,l1,l2,ext=true){
		function CheckNeighbor(x,y){
			if(x<0||y<0||x>=CONST.mapSize||y>=CONST.mapSize) return false;
			return heightmap[x][y]>=l1;
		}

		function CheckNeighbors(x,y){
			return  CheckNeighbor(x+1,y) ||
					CheckNeighbor(x-1,y) ||
					CheckNeighbor(x,y+1) ||
					CheckNeighbor(x,y-1);
		}

		for(var x=0;x<CONST.mapSize;x++){
			for(var y=0;y<CONST.mapSize;y++){
				if(heightmap[x][y]==0 && CheckNeighbors(x,y)){
					if(ext){
						MapBuilder._Line(heightmap,x,y,l2);
					}
					else{
						heightmap[x][y]=l2;
					}
				}
			}
		}
	},
	_Reverse : function(heightmap){
		for(var j=0;j<CONST.mapSize;j++){
			for(var i=0;i<CONST.mapSize;i++){
				heightmap[j][i] =heightmap[j][i] *-1 + 60; 
			}
		}
	},
	BuildMap : function(){		
		var heightmap = [];
		var watermap = [];

		for(var j=0;j<CONST.mapSize;j++){
			heightmap.push([]);
			watermap.push([]);

			for(var i=0;i<CONST.mapSize;i++){
				heightmap[j].push(0);
				watermap[j].push(0);
			}
		}

		for (var i = 0; i < LIB.Rnd(1,4); i++) {
			MapBuilder._DrawLine(heightmap,60);
		}
		MapBuilder._FillExtLevel(heightmap,60,30);
		MapBuilder._FillLevel(heightmap,30,10);
		//MapBuilder._Reverse(heightmap);


		MapBuilder._DrawLine(watermap,2);
		MapBuilder._DrawLine(watermap,2);

		var map = {
			heightmap : heightmap,
			watermap : watermap
		};

		return map;
	},
}

var EditorInterface = {
	_callback :null,
	Action : function(key){
		this._callback = EditorInterface.Actions[key];
	},
	Edit : function(x,y){
		if(this._callback) this._callback(x,y);
	},
	Actions : {
		Grass : function(x,y){
			Globals.map.watermap[x][y] = 2;			
			Terrain3d.Refresh();
		},
		Dirt : function(x,y){
			Globals.map.watermap[x][y] = 0;			
			Terrain3d.Refresh();
		},
		Raise : function(x,y){
			Globals.map.heightmap[x][y] += 30;
			Terrain3d.Refresh();
		},
		Lower : function(x,y){
			Globals.map.heightmap[x][y] -= 30;
			Terrain3d.Refresh();
		},
		Tower : function(x,y){						
			Draw3d.DrawMapItem(ModelsFactory.Tower(),x,y);
		},
		House : function(x,y){						
			Draw3d.DrawMapItem(ModelsFactory.House(),x,y);
		},
		Field : function(x,y){						
			Globals.map.watermap[x][y] = 1;			
			Terrain3d.Refresh();
		},
		City : function(x,y){						
			Draw3d.DrawMapItem(ModelsFactory.City(),x,y);
		},
		Workshop : function(x,y){						
			Draw3d.DrawMapItem(ModelsFactory.Workshop(),x,y);
		},
		Horse : function(x,y){						
			Draw3d.DrawMapItem(ModelsFactory.Horse(),x,y);
		},
	}
}