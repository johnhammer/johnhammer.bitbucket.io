var ISONLINEGAME = true;
var PLAYERS = [];
var CURRENT_PLAYER = 0;
var PLAYER_ID = null;
var OBJECTS = [];

var CONST = {
	mapSize : 16,
	gridXsize : 64,
	gridYsize : 64,
	type : {
		unit : 0,
		city : 1,
		interface : 2,
		interfaceAttack : 3,
		building : 4,
	}	
}