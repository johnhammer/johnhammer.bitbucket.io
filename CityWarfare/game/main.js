var Main = {
	Is3d : null,
	StartGame : function(){
		Main.Is3d=true;

		Globals.map = MapBuilder.BuildMap(); //load map?

		if(Main.Is3d){
			var promises = [];

			MaterialsFactory.Load(promises);
			LoadObjModels(promises);

			Promise.all(promises).then(results => {
				Main._Start();
			});	
		}
		else{
			Main._Start();
		}		
	},
	_Start : function(){
		Draw.Start();
		Interaction.Start();

		LIB.Toggle(room);
		LIB.Toggle(game);

		Board.Init();
		City.Create(10,10, 0);

		Unit.Create(11,11, 0,Unit.Type.Peon);
		//City.Create(5,5, 1);
 		
		//Building.Create(1,1, 1, "Cistern");
		//Building.Create(2,1, 1, "Granary");
		//Building.Create(3,1, 1, "Field");
		//Building.Create(4,1, 1, "House");
		//Building.Create(5,1, 1, "Market");
		//Building.Create(1,2, 1, "Tower");
		//Building.Create(2,2, 1, "Workshop");
	},
	Refresh : function(){
		Interface.Refresh();
		Board.Refresh();
	}
}

