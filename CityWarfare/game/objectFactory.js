var ObjectFactory = {
	_Object : function(x,y,player,id){		
		var object = {
			id:id,
			x:x,
			y:y,
			player:player
		};
				
		OBJECTS[id] = object;
		
		return object;
	},
	Interface : function(x,y,object){
		Draw.Move(x,y);
		var id = LIB.Guid();
		var obj = this._Object(x,y,0,id); //!
		obj.object = object;
		obj.type = CONST.type.interface;
		return obj;
	},
	InterfaceAttack : function(x,y,object){
		Draw.Attack(x,y);
		var id = LIB.Guid();
		var obj = this._Object(x,y,0,id); //!
		obj.object = object;
		obj.type = CONST.type.interfaceAttack;
		return obj;
	},

}
