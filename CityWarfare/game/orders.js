var Orders = {
	_Emit : function(emit,params){

	},
	_Refresh : function(){
		Interface.Refresh();
		Board.Refresh();
	},
	NextTurn : function(emit){
		if(ISONLINEGAME&&emit){
			GameProtocol.NextTurn();
		}
		TurnManager.Next();
        this._Refresh();
	},
	SelectInterface : function(object,emit){	
		if(ISONLINEGAME&&emit){
			GameProtocol.MoveUnit(object.x,object.y,object.object);
		}
		Unit.Move(object.x,object.y,object.object);
		Unit.Spend(object.object);
		this._Refresh();
	},
	SelectInterfaceAttack : function(object,emit){	
		if(ISONLINEGAME&&emit){
			GameProtocol.MoveUnit(object.x,object.y,object.object);
		}
		var objective = Board.FindTypeInPosition(object.x,object.y,CONST.type.unit);
		Battle.Fight(object.object,objective);
		Unit.Spend(object.object);
		this._Refresh();
	},	
	SetCityProduction : function(id,productionKey,emit){
		if(ISONLINEGAME&&emit){
			GameProtocol.CityProd(id,productionKey);
		}
		City.SetProduction(OBJECTS[id],City.Production[productionKey]);
		this._Refresh();
	},
	BuildBuilding : function(id,buildingKey,emit){
		if(ISONLINEGAME&&emit){
			
		}
		Unit.Spend(OBJECTS[id]);
		Building.Create(OBJECTS[id].x,OBJECTS[id].y,OBJECTS[id].player,buildingKey);
		this._Refresh();
	},
	UnitOrder : function(id,orderKey,emit){
		if(ISONLINEGAME&&emit){
			
		}
		Unit.Order(OBJECTS[id],orderKey);
		this._Refresh();
	}
}