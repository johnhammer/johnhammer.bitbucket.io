var TurnManager = {
	Counter : 0,
	EndTurn : function(player){
		
	},	
	StartTurn : function(){
		for(var i in OBJECTS){
			switch(OBJECTS[i].type){
		    	case CONST.type.unit: Unit.StartTurn(OBJECTS[i]); break;
		    	case CONST.type.city: City.StartTurn(OBJECTS[i]); break;
		    	case CONST.type.building: Building.StartTurn(OBJECTS[i]); break;
		    }
		}
	},
	Next : function(){
		this.EndTurn(CURRENT_PLAYER);
		CURRENT_PLAYER++;		
		if(CURRENT_PLAYER>=PLAYERS.length){
			this.Counter++;
			CURRENT_PLAYER=0;				
			this.StartTurn();
		}
		TURNINTERFACE.innerHTML = CURRENT_PLAYER;			
	}
}