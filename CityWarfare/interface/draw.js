var Draw = {
	_Interface : [],

	Start : function(){
		if(Main.Is3d){
			Draw3d.Start();
		}
		else{
			Draw2d.Start();
		}
	},
	Terrain(){
		if(Main.Is3d){
			for(var i=0;i<Draw._Interface.length;i++){
				Globals3d.scene.remove(Draw._Interface[i]);
			}
			Draw._Interface = [];
		}
		else{
			for(var j=0;j<CONST.mapSize;j++){
				for(var i=0;i<CONST.mapSize;i++){
					Draw2d.BigImage(i,j,"bigimage1");
				}
			}
		}			
	},
	City : function (x,y){
		if(Main.Is3d){

			Draw3d.DrawMapItem(ModelsFactory.Tower(),x,y);
		}
		else{
			Draw2d.Building(x,y,2);
		}		
	},
	Building : function (x,y,id,obj){
		if(Main.Is3d){
			//if(obj.mesh) Globals3d.scene.remove(obj.mesh);
			var mesh = ModelsFactory.House();

			switch(id){
				case "House" : mesh = ModelsFactory.House();break;
				case "Tower" : mesh = ModelsFactory.Tower();break;
				case "Field" : mesh = ModelsFactory.Field();break;
				case "Workshop" : mesh = ModelsFactory.Workshop();break;
				case "City" : mesh = ModelsFactory.City();break;
			}

			obj.mesh = mesh;
			Draw3d.DrawMapItem(mesh,x,y);
		}
		else{
			Draw2d.Building(x,y,Building.Type[id].i);
		}		
	},
	Unit : function(object){
		if(Main.Is3d){
			if(object.mesh) Globals3d.scene.remove(object.mesh);
			object.mesh = Draw3d.DrawMapItem(ModelsFactory.Horse(),object.x,object.y);
		}
		else{
			Draw2d.SmallImage(object.x,object.y,"smallimage"+object.img);
			//Draw2d.SmallRectangle(object.x,object.y,PLAYERS[object.player].Info.Color);
			var text = "h"+object.cur.h+" t"+object.cur.turns;
			Draw2d.Text(object.x,object.y,text);
		}			
	},	
	Select : function (x,y){
 		if(Main.Is3d){
			Draw._Interface.push(Draw3d.DrawMapItem(ModelsFactory.CubeInterface(),x,y));
		}
		else{
			Draw2d._Mask(x,y,"#00ffff");
		}		
	},
	Move : function (x,y){
		if(Main.Is3d){
			Draw._Interface.push(Draw3d.DrawMapItem(ModelsFactory.CubeInterface(),x,y));
		}
		else{
			Draw2d._Mask(x,y,"#ffffff");
		}		
	},
	Attack : function (x,y){
		if(Main.Is3d){
			Draw._Interface.push(Draw3d.DrawMapItem(ModelsFactory.CubeInterface(),x,y));
		}
		else{
			Draw2d._Mask(x,y,"#ff0000");
		}		
	},
}