var Interaction = {
	Start : function(){
		document.addEventListener( 'keyup', 	Interaction.OnKeyUp, false );
		if(Main.Is3d){
			Interaction3d.Start();
		}
		else{
			Interaction2d.Start();
		}
	},
	OnKeyUp : function(){
		if (event.key === "Enter") {
	    	if(PLAYER_ID == CURRENT_PLAYER) {
	    		Orders.NextTurn(true);
	    	}
	    }
	},
	_current : null,
	SelectUnit : function(object){
		var isMine = object.player == PLAYER_ID;
		var isMyTurn = PLAYER_ID == CURRENT_PLAYER;

		Interface.ShowUnitInterface(object);

		if(isMine && isMyTurn){
			_current = CONST.type.unit;
			if(object.cur.turns>0){
				Board.ApplyAround(object.x,object.y,object.max.r,function(x,y){
					Unit.CreateUnitInterface(x,y,object);
				});
			}   
		}				
	},
	SelectCity : function(object){
		Interface.ShowCityInterface(object);		
	},
	SelectInterface : function(object){	
		Orders.SelectInterface(object,true);
	},
	SelectInterfaceAttack : function(object,emit){	
		Orders.SelectInterfaceAttack(object,true);
	},	
	
}