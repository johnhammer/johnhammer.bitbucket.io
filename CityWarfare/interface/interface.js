var Interface = {
	_action : null,
	_object : null,

	_canPlay : function(){
		var isMine = this._object.player == PLAYER_ID;
		var isMyTurn = PLAYER_ID == CURRENT_PLAYER;
		
		if(this._object.type == CONST.type.unit){
			return this._object.cur.turns>0 && isMine&&isMyTurn;
		}
		return isMine&&isMyTurn;
	},
	Refresh : function(){
		if(this._action) INTERFACE.innerHTML = this._action(this._canPlay());
	},
	ShowUnitInterface : function(unit){
		this._object = unit;
		this._action = function(canPlay){return Templating.UnitInterface(unit,canPlay,Unit.GetBuildings(unit))};
		this.Refresh();
	},
	ShowCityInterface : function(city){
		this._object = city;
		this._action = function(canPlay){return Templating.CityInterface(city,canPlay)}
		this.Refresh();
	},
	SetCityProduction : function(id,productionKey){
		Orders.SetCityProduction(id,productionKey,true);
	},
	BuildBuilding : function(id,buildingKey){
		Orders.BuildBuilding(id,buildingKey,true);
	},
	UnitOrder : function(id,orderKey){
		Orders.UnitOrder(id,orderKey,true);
	}
}