var Templating = {
	Player : function(id,human){
		return `
			<div class="player js_player">
				<input onchange="Room.ChangePlayerName(${id},this.value)" value="Jugador"></input>		
				${human?'':'<input id="player1isia" type="checkbox" checked disabled>IA</input>'}

				<input type="color" id="myColor">

			</div>
		`;		
	},
	ChatMessage : function(nick,message,me){
		return `
			<div ${me?'class="chat_message_me"':''}>${nick}: ${message}</div>
		`;		
	},
	CityInterfaceItem : function(id,key){
		return `<button onclick=Interface.SetCityProduction(${id},"${key}")>${City.Production[key].n}</button>`
	},
	CityInterface : function(city,canPlay){
		return `
			<h2>${city.name}</h2>
			<p>Turnos para generar ${city.production.n}: ${city.max.turns-city.cur.turns}</p>
			${
				canPlay?				
				Object.keys(City.Production).map(function (key) {
					return Templating.CityInterfaceItem(city.id,key);
				}).join(''):
				''
			}
		`;		
	},
	UnitInterface : function(unit,canPlay,buildings){
		return `
			<h2>${unit.unitType}</h2>
			${
				canPlay&&buildings?				
				buildings.map(function (key) {
					return Templating.UnitBuildItem(unit.id,key);
				}).join(''):
				''
			}
			<h3>Actions</h3>
			<button onclick=Interface.UnitOrder(${id},"farm")>Cultivar</button>
		`;
	},
	UnitBuildItem : function(id,key){
		return `<button onclick=Interface.BuildBuilding(${id},"${key}")>${Building.Type[key].n}</button>`
	},
}