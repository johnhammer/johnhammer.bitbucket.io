var LIB = {	
	Toggle : function(node){
		if(node.style.display == "none"){
			node.style.display = "block";
		}
		else{
			node.style.display = "none";
		}
	},
	Crypt : function(me, key){
		key = Number(String(Number(key))) === key ? Number(key) : 13;
		me = me.split('').map(function(c){return c.charCodeAt(0);}).map(function(i){return i ^ key;});
		me = String.fromCharCode.apply(undefined, me);
		return me;
	},
	Encrypt : function(me, key="entropy"){
		return LIB.Crypt(window.btoa(me),key);
	},
	Decrypt : function(me, key="entropy"){
		return window.atob(LIB.Crypt(me,key));
	},
	Rnd : function(min,max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	PickRnd : function(arr){
		return arr[Math.floor(Math.random() * arr.length)];
	}, 
	Clone : function(obj){
		return JSON.parse(JSON.stringify(obj));
	},
	_counter : 0,
	Guid : function() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	},
	Counter : function() {
		this._counter++;
	  return this._counter;
	}
}