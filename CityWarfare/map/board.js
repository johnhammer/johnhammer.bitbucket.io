var Board ={
	Map : [],
	Init : function(){

		for(var j=0;j<CONST.mapSize;j++){
			this.Map.push([]);
			for(var i=0;i<CONST.mapSize;i++){
				this.Map[j][i]={
					t:0,		//terrain grass
					c:false,	//is city
				}; 
			}
		}

		this.Refresh();
	},
	Refresh : function(){
		Draw.Terrain();
		for(var i in OBJECTS){
			switch(OBJECTS[i].type){
		    	case CONST.type.unit: Unit.Draw(OBJECTS[i]); break;
		    	case CONST.type.building: Building.Draw(OBJECTS[i]); break;
		    	case CONST.type.city: Draw.City(OBJECTS[i].x,OBJECTS[i].y); break;
		    	case CONST.type.interface: 
		    	case CONST.type.interfaceAttack: 
		    		delete OBJECTS[i]; break;
		    }
		}		
	},
	SetCityCell : function(x,y){
		Board.Map[x][y].c=true;
	},	
	IsCityCell : function(x,y){
		return Board.Map[x][y].c;
	},	
	CheckPos : function(x,y){
		return x>=0 && y>=0 && x<CONST.mapSize && y<CONST.mapSize;
	},
	ApplyAround : function(x,y,size,func){
		if(size == 1){
			if(this.CheckPos(x-1,y-1)) 	func(x-1,y-1);
			if(this.CheckPos(x-1,y)) 	func(x-1,y);
			if(this.CheckPos(x-1,y+1)) 	func(x-1,y+1);
			if(this.CheckPos(x,y-1)) 	func(x,y-1);
			if(this.CheckPos(x,y+1)) 	func(x,y+1);
			if(this.CheckPos(x+1,y-1)) 	func(x+1,y-1);
			if(this.CheckPos(x+1,y)) 	func(x+1,y);
			if(this.CheckPos(x+1,y+1)) 	func(x+1,y+1);
		}
	},
	Click : function(x,y){
	    var object = this.FindTypeInPosition(x,y,CONST.type.interface);
	    if(object) {
			Interaction.SelectInterface(object);
			return;
	    }

	    object = this.FindTypeInPosition(x,y,CONST.type.interfaceAttack);
	    if(object) {
			Interaction.SelectInterfaceAttack(object);
			return;
	    }

	    this.Refresh(); //clear click

	    object = this.FindTypeInPosition(x,y,CONST.type.unit);
	    if(object) {
			Interaction.SelectUnit(object);
			return;
	    }

	    object = this.FindTypeInPosition(x,y,CONST.type.city);
	    if(object) {
			Interaction.SelectCity(object);
			return;
	    }
	},
	FindTypeInPosition : function(x,y,type){		
		for(var i in OBJECTS){
			if(OBJECTS[i].x == x && OBJECTS[i].y == y && OBJECTS[i].type==type){
				return OBJECTS[i];
			}
		}
		return null;
	},
	FindInPosition : function(x,y){		
		for(var i in OBJECTS){
			if(OBJECTS[i].x == x && OBJECTS[i].y == y){
				return OBJECTS[i];
			}
		}
		return null;
	}
}