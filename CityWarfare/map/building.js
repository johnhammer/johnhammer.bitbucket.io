var Building = {
	Type : {
		//Cistern 	: {t:3,i:3 		,n:"cisterna"},
		//Granary 	: {t:3,i:5 		,n:"granero"},
		Field 		: {t:3,i:4 		,n:"field"},
		House 		: {t:3,i:6 		,n:"casa"},
		//Market 		: {t:3,i:7 		,n:"mercado"},
		Tower 		: {t:3,i:8 		,n:"torre"},
		Workshop 	: {t:3,i:9 		,n:"taller"},
	},
	Create : function(x,y,player,type){	

		var id = LIB.Counter();		
		var obj = ObjectFactory._Object(x,y,player,id);

		obj.type = CONST.type.building;
		obj.buildingType = type;

		if(type=='Tower'){
			Walls.AddTower(obj);
		}	
		else if(type=='Field'){	
			obj.fieldTurn = 0;
		}

		//Draw.Building(x,y,type,obj);

		return obj;
	},
	Draw : function(object){
		Draw.Building(object.x,object.y,object.buildingType,object);
	},
	StartTurn : function(object){
		if(object.buildingType=='Field'){
			Field.NextTurn(object);
		}
	},
	
}

var Field = {
	NextTurn : function(obj){
		obj.fieldTurn++;
		if(obj.fieldTurn==5){
			Field.Grow(obj);
		}
		else if(obj.fieldTurn==10){
			Field.Ungrow(obj);
			obj.fieldTurn=0;
		}
	},
	Grow : function(obj){
		
		obj.mesh.position.setY(100);
		obj.mesh.material.map = MaterialsFactory.Textures.Wall.val;
		obj.mesh.material.needsUpdate = true
	},
	Ungrow : function(obj){
		
		obj.mesh.material.map = MaterialsFactory.Textures.Wall.val;
		obj.mesh.material.needsUpdate = true
	},
}