var City = {
	Production : {
		Spear 		: {t:3,u:Unit.Type.Spear 		,n:"lancero"},
		Sword 		: {t:4,u:Unit.Type.Sword 		,n:"espadachin"},
		Falcata 	: {t:4,u:Unit.Type.Falcata 		,n:"guerrero"},
		Gladius 	: {t:4,u:Unit.Type.Gladius 		,n:"soldado"},
		Bow 		: {t:4,u:Unit.Type.Bow 			,n:"arquero"},
		Javelin 	: {t:4,u:Unit.Type.Javelin 		,n:"lanzador"},
		Stone 		: {t:3,u:Unit.Type.Stone 		,n:"hondero"},
		Soliferrum 	: {t:5,u:Unit.Type.Soliferrum 	,n:"lanzador pesado"},

		Peon 		: {t:2,u:Unit.Type.Peon 		,n:"peon"},
		Caravan 	: {t:2,u:Unit.Type.Caravan 		,n:"caravana"},
	},
	Create : function(x,y,player){
		Draw.City(x,y);
		var id = LIB.Counter();
		
		var obj = ObjectFactory._Object(x,y,player,id);

		obj.name = "CITYNAME";
		obj.production = City.Production.Spear;
		obj.type = CONST.type.city;
		obj.max = {
			turns : 3
		};
		obj.cur = {
			turns : 0
		};

		Board.ApplyAround(x,y,1,Board.SetCityCell)

		return obj;
	},
	StartTurn : function(object){
		object.cur.turns++;
		if(object.cur.turns == object.max.turns){
			var x = object.x;
			var y = object.y;

			Unit.Create(x,y,object.player,object.production.u);

			object.cur.turns = 0;
		}
	},
	SetProduction : function(object,production){
		object.max.turns = production.t;
		object.production = production;
	}

}