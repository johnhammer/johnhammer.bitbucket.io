var Unit = {
	Type:{ //Speedhit,Attack,Defence,Reach,TurnsToBuild
		Spear 		: {s:3,a:1,d:1,r:1,t:3,		img:1,	key:"Spear",n:"lancero"},
		Sword 		: {s:1,a:2,d:2,r:1,t:4,		img:2,	key:"Sword",n:"espadachin"},
		Falcata 	: {s:2,a:2,d:1,r:1,t:4,		img:3,	key:"Falcata",n:"guerrero"},
		Gladius 	: {s:2,a:1,d:2,r:1,t:4,		img:4,	key:"Gladius",n:"soldado"},
		Bow 		: {s:2,a:1,d:1,r:2,t:4,		img:5,	key:"Bow",n:"arquero"},
		Javelin 	: {s:1,a:2,d:1,r:2,t:4,		img:6,	key:"Javelin",n:"lanzador"},
		Stone 		: {s:2,a:2,d:1,r:1,t:3,		img:7,	key:"Stone",n:"hondero"},
		Soliferrum 	: {s:1,a:3,d:1,r:1,t:5,		img:8,	key:"Soliferrum",n:"lanzador pesado"},
		Peon		: {s:1,a:0,d:0,r:1,t:2,		img:9,	key:"Peon",n:"obrero"},
		Caravan     : {s:1,a:0,d:0,r:1,t:2,		img:10,	key:"Caravan",n:"caravana"},
	},
	Shield:{
		Light		: {a:1,d:1,w:1},
		Round		: {a:2,d:1,w:2},
		Long		: {a:1,d:2,w:2},
		Heavy		: {a:2,d:2,w:3}
	},
	Create(x,y,player,type){
		var id = LIB.Counter();
		var obj = ObjectFactory._Object(x,y,player,id);
		obj.type = CONST.type.unit;		
		obj.max = {
			turns : 12,
			a:type.a,
			d:type.d,
			r:type.r,
			h:3
		};
		obj.img = type.img;
		obj.cur = LIB.Clone(obj.max);
		obj.unitType = type.key;	

		UnitAi.Start(obj);

		this.Draw(obj);
		return obj;		
	},
	Draw : function(object){
		Draw.Unit(object);
	},
	Move : function(x,y,object){
		object.x = x;
		object.y = y;		
	},
	Spend : function(object,amt=1){
		object.cur.turns-=amt;
	},
	StartTurn : function(object){
		object.cur.turns = object.max.turns;
		UnitAi.Turn(object);
	},
	CreateUnitInterface : function(x,y,object){
		var target = Board.FindInPosition(x,y);
	
		if(target){
			ObjectFactory.InterfaceAttack(x,y,object);
		}
		else{
			ObjectFactory.Interface(x,y,object);
		}		
	},
	GetBuildings :function(unit){
		if(unit.unitType=="Peon"){
			if(Board.IsCityCell(unit.x,unit.y)){
				return [
					'House',
					'Workshop'
				]
			}
			else{
				return [
					'Field',
					'Tower'
				]
			}
		}
		return null;
	},
	Order : function(unit,orderKey){
		if(orderKey=="farm"){
			Peon.Farm(unit);
		}
	}
}

var Peon = {
	Farm : function(unit){
		var nearestField = Peon.GetNearestField(unit.x,unit.y);
		UnitAi.SetObjective(unit,nearestField);
	},
	GetNearestField :function(x,y){
		return {x:0,y:0}
	}
}