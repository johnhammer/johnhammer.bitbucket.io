var Walls = {
	Walls : [],
	Oppidums : [],
	GetNeighbor(x,y){
		var building = Board.FindTypeInPosition(x,y,CONST.type.building);
		return building;
	},
	CreateWall : function(obj){
		var wall = [obj];
		Walls.Walls.push(wall);
		obj.wall = wall;
	},
	AddToWall : function(obj,wall){
		wall.push(obj);
		obj.wall = wall;
	},
	CloseWall : function(obj,wall){
		Walls.AddToWall(obj,wall);
		Walls.CreateOppidum(wall);
	},
	CreateOppidum : function(wall){
		var oppidum = {
			wall : wall,
			cells : []
		}
		for(var i=0;i<wall.length;i++){
			wall[i].oppidumWall = oppidum;
		}
		var firstCell = Walls.FindOppidumFirstCell(wall);

		Walls.FloodOppidum(firstCell,wall,oppidum.cells);

		Walls.Oppidums.push(oppidum);
	},
	FindOppidumFirstCell : function(unsortedWall){
		var wall = unsortedWall.sort(function(a, b){return b.x - a.x}).sort(function(a, b){return b.y - a.y}).reverse();

		for (var i = 1; i < wall.length-1; i++) {
			if(wall[i-1].y == wall[i+1].y){
				return {
					x:wall[i].x,
					y:wall[i].y+1
				};
			}
		}
	},
	FloodOppidum : function(cell,wall,oppidum){
		var building = Board.FindTypeInPosition(cell.x,cell.y,CONST.type.building);
		if(Walls.OppidumHasCell(wall,cell)) return;
		if(Walls.OppidumHasCell(oppidum,cell)) return;
		Walls.AddCellToOppidum(oppidum,cell);

		Walls.FloodOppidum({x:cell.x-1,y:cell.y},wall,oppidum);
		Walls.FloodOppidum({x:cell.x+1,y:cell.y},wall,oppidum);
		Walls.FloodOppidum({x:cell.x,y:cell.y-1},wall,oppidum);
		Walls.FloodOppidum({x:cell.x,y:cell.y+1},wall,oppidum);
	},
	AddCellToOppidum : function(oppidum,cell){
		Board.SetCityCell(cell.x,cell.y);
		oppidum.push(cell);
	},
	OppidumHasCell : function(oppidum,cell){
		for (var i = 1; i < oppidum.length-1; i++) {
			if(oppidum[i].x==cell.x && oppidum[i].y==cell.y){
				return true;
			}
		}
		return false;
	},
	MergeWall : function(w1,w2){
		for(var i=0;i<w2.length;i++){
			w2[i].wall = w1;
		}
		w1.push.apply(w1, w2);
		w2.length = 0;
	},
	AddTower : function(obj){
		var neighbors = [
			Walls.GetNeighbor(obj.x+1,obj.y),
			Walls.GetNeighbor(obj.x-1,obj.y),
			Walls.GetNeighbor(obj.x,obj.y+1),
			Walls.GetNeighbor(obj.x,obj.y-1)
		];		
		var walls = neighbors.filter(x => x && x.wall);
		var oppidums = walls.filter(x => x.oppidumWall);
		if(oppidums.length>0){
			return false;
		}
		switch(walls.length){
			case 0: Walls.CreateWall(obj); return true;
			case 1: Walls.AddToWall(obj,walls[0].wall); return true;
			case 2: 
				if(walls[0].wall==walls[1].wall){
					Walls.CloseWall(obj,walls[0].wall); 
				}
				else{
					Walls.MergeWall(walls[0].wall,walls[1].wall);
					Walls.AddToWall(obj,walls[0].wall); 
				}				
				return true;
			default: return false;
		}
	}
}