
var Listener = {
	Listen : function(message){		
		switch(message.t){
			case Protocol.Types.Chat:
				Chat.Show(message.n,message.m);
				break;
			case Protocol.Types.Enter:
				Room.AddHuman();
				Protocol.Welcome();
				break;
			case Protocol.Types.Welcome:
				Room.SetPlayers(message.m);
				break;
			case Protocol.Types.Start:
				Main.StartGame();
				break;
			case Protocol.Types.Game:
				this.GameListen(message.m);
				break;
			default:
				console.log(message);
		}		
	},
	GameListen : function(message){
		switch(message.t){
			case GameProtocol.Types.MoveUnit:
				if(Board.FindTypeInPosition(message.x,message.y,CONST.type.unit)){
					var objective = Board.FindTypeInPosition(message.x,message.y,CONST.type.unit);
					Battle.Fight(OBJECTS[message.id],objective);
				}
				else{
					Unit.Move(message.x,message.y,OBJECTS[message.id]);
				}
				Board.Refresh();
				break;
			case GameProtocol.Types.CityProd:
				City.SetProduction(OBJECTS[message.id],City.Production[message.prod]);
				break;
			case GameProtocol.Types.NextTurn:
				Interaction.NextTurn(false);
				break;
			default:
				console.log(message);
		}	


		
	}	
}
