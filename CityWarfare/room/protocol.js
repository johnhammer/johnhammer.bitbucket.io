var GameProtocol = {
	Types:{
		MoveUnit:0,	
		NextTurn:1,
		CityProd:2
	},	
	MoveUnit : function(x,y,unit){
		Protocol.Game({
			t:GameProtocol.Types.MoveUnit,
			id:unit.id,
			x:x,
			y:y
		});
	},
	NextTurn : function(){
		Protocol.Game({
			t:GameProtocol.Types.NextTurn
		});
	},
	CityProd : function(id,prod){
		Protocol.Game({
			t:GameProtocol.Types.CityProd,
			id:id,
			prod:prod
		});
	}
}

var Protocol = {
	Types:{
		Chat:0,
		Enter:1,
		Welcome:2,
		Start:3,
		Game:4,		
	},	
	Enter : function(){ //player to host, on enter room
		var message = {
			t:Protocol.Types.Enter
		}		
		Online.Raw.Send(message);
	},
	Welcome : function(){ //host to players, new player entered room
		var message = {
			t:Protocol.Types.Welcome,
			m:Room.GetPlayers()
		}		
		Online.Raw.Send(message);
	},	
	Chat : function(raw){
		var message = {
			t:Protocol.Types.Chat,
			m:raw,
			n:PLAYERS[PLAYER_ID].Info.Name
		}		
		Online.Raw.Send(message);
	},
	StartGame : function(){
		var message = {
			t:Protocol.Types.Start
		}		
		Online.Raw.Send(message);
	},
	Game : function(raw){
		var message = {
			t:Protocol.Types.Game,
			m:raw		
		}		
		Online.Raw.Send(message);
	}
}
