
var Room = {
	Single : function(){
		ISONLINEGAME = false;
		
		this.AddHuman();
		PLAYER_ID = 0;
	
		LIB.Toggle(room_single);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_start);
	},	
	Create : function(){
		this.AddHuman();	
		PLAYER_ID = 0;
	
		Online.Init();	
		Online.useServer = false;
		Online.CreateManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_single);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_allow);
		LIB.Toggle(room_start);
		LIB.Toggle(chat);
	},
	Join : function(){
		Online.Init();	
		Online.JoinManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_single);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);	
		LIB.Toggle(chat);		
	},
	Allow : function(){
		Online.Allow();
	},
	StartGame : function(){		
		if(ISONLINEGAME){
			Protocol.StartGame();
		}
		Main.StartGame();
	},
	AddPlayer : function(human){		
		this.AddGlobalPlayer(human);		
		players.insertAdjacentHTML('beforebegin',Templating.Player(PLAYERS.length-1,human));		
	},	
	AddHuman : function(){
		this.AddPlayer(true);
	},
	AddAI : function(){		
		this.AddPlayer(false);
		if(ISONLINEGAME){
			Protocol.Welcome();
		}
	},
	GetPlayers : function(){
		var playerNodes = document.getElementsByClassName('js_player');
		var playersData = [];		
		for(var i=0;i<playerNodes.length;i++){
			playersData.push('kurwa');			
		}		
		return playersData;
	},
	SetPlayers : function(playersData){
		players.innerHTML = '';
		var i;
		for(i=0;i<playersData.length;i++){
			this.AddHuman();
		}
		if(!PLAYER_ID){
			PLAYER_ID = i-1;
		}
	},	
	AddGlobalPlayer : function(human){
		PLAYERS.push({
			Info:{
				Name : 'Jugador',
				Civ : 0,
				Human : human,
				Color : "#000000"
			},
		});
	},
	ChangePlayerName : function(id,name){
		PLAYERS[id].Info.Name = name;
	},
	ChangePlayerFaction : function(id,civ){
		PLAYERS[id].Info.Civ = Number(civ);
	},
	ShowInfo : function(info){
		room_code.value = LIB.Encrypt(info);
	}
}
