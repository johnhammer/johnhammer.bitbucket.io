Bronze Age
2nd millennium BCE
ca. 1800 BCE – The El Argar civilization appears in Almería, south-east of Spain, replacing the earlier civilization of Los Millares. The adoption of bronze metallurgy allows gradual dominance and influence in the region.[1]
ca. 1500 BCE
A culture of smaller fortified villages known as Bronze of Levante appears in the modern-day region of Valencia, particularly in the southern half, being close culturally to El Argar. This people starts to install the first settlements in the semi-desertic La Mancha called Motillas (fortifications in the top of man-made hills).[1]
For the first time the cattle-herding tribes of the central plateau get organize into a single culture, known as Cogotas I, practising transhumance herding.[1]
The presence of strategic tin resources in the North Western Iberia is probably the cause of some development in this region. The Montelavar group is characterized especially by its bronze axes.
ca. 1300 BCE
El Argar disappears abruptly, giving way to a less homogeneous post-Argarian culture.[1]
The Motillas are abandoned, perhaps due to the disappearance of the Argarian state and its military needs.
The Urnfield culture is the first wave of Indo-European migrations to enter in the Peninsula. Although they stayed in Catalonia, they triggered the Atlantic Bronze Age in the Northwest of the peninsula (modern Galicia and northern Portugal), that maintained commercial relations with Brittany and the British Isles.[2]
In Western Andalusia appears an internally burnished pottery culture.
The Northwest is defined by their typical axes, divided into two types: Galician and Astur-Cantabrian.
1st millennium BCE
First wave of Indo-European migrations into Iberia, of the Urnfield culture (Proto-Celts).
Bronze culture (Indo-European) in the Northwest of Iberia (modern Galicia and northern Portugal), maintaining commercial relations with Brittany and the British Isles. Emergence of the Castro Village culture in this Iberian area.
Bronze culture in the Portuguese Estremadura (not Indo-European).
Bronze culture of Portuguese Beira Alta (not Indo-European but influenced by).
Emergence of Tartessian society in the territory of modern Andalusia.
Iron Age
11th century BCE
First contacts between Phoenicians and Iberia (along the Mediterranean coast).
In 1104 BCE Phoenicians found the first walled city Gadir (modern Cádiz) as a trading post.[3]
10th century BCE
ca. 1000 BCE – Development of Tartessos, the first Iberian State mentioned in writing sources. Tartessos was a centralized Monarchy brought about under Phoenician influence and maintained commercial relations with the area of modern Algarve, inhabited by the Cynetes or Cunetes, and Portuguese Estremadura.[4]
Emergence of towns and cities in the southern littoral areas of western Iberia.
9th century BCE
Foundation of the Phoenician (from the city-state of Tyre) colony of Carthage (in North Africa).
Foundation of the Phoenician colony of Gadir (modern Cádiz) near Tartessos. Contrary to myth, there is no record of Phoenician colonies west of the Strait of Gibraltar[citation needed], even though there might have been some voyages of discovery. Phoenician influence in what is now Portuguese territory was done through cultural and commercial exchange with Tartessos.
Phoenicians introduce in Iberia the use of Iron, of the Potter's wheel, the production of olive oil and wine. They were also responsible for the first forms of Iberian writing, had a big religious influence and accelerated urban development.
There are organized settlements in Olissipona (modern Lisbon, in Portuguese Estremadura) with clear Mediterranean influences. The myth of a Phoenician foundation of the city as far back as 1300 BCE, under the name Alis Ubbo ("Safe Harbour") is not true.[citation needed]
ca. 900 BCE – The Castro Village culture appears in the northwestern part of the peninsula (roughly present-day northern Portugal, Galicia and Asturias). This culture is characterized by their walled villages and hill forts. It expanded from south to north and from the coast to the interior of the peninsula during the next centuries.[2]
8th century BCE – Strong Phoenician influence in the city of Balsa (modern Tavira in the Algarve).
ca. 800 BCE – The Celtic Hallstatt culture reaches the local Urnfields Celts of the Northeast, bringing the iron working technology to Iberia. This culture starts to expand to the nearby areas, embracing the northern region of the Levante and the upper Ebro valley.
The Phoenicians establish a colony in Almunecar called Sexi.
7th century BCE
Strong Tartessian influence in the area of modern Algarve.
Second wave of Indo-European (Celts of the Hallstatt culture?) migration into Portuguese territory.
ca. 700 BCE – The cattle herding culture of Cogotas I is transformed into Cogotas II, mixing the Celtic culture with the Iberian culture (Celtiberians).[5]
654 BCE – Phoenician settlers found a port in the Balearic Islands as Ibossim (Ibiza).[6]
6th century BCE
Decadence of Phoenician colonization of the Mediterranean coast of Iberia. Many of the colonies are deserted.
Fall of Tartessos.
Beginning of Greek settlement in the Iberian peninsula, namely in the eastern Mediterranean shore (modern Catalonia). There are no Greek colonies west of the Strait of Gibraltar, only voyages of discovery. The myth of an ancient Greek founding of Olissipo (modern Lisbon) by Ulysses is not true.
Rise of the colonial might of Carthage, which slowly replaces the Phoenician in its former areas of dominion.
Phoenician influenced Tavira is destroyed by violence.
Cultural shift in southern Portuguese territory after the fall of Tartessos, with a strong Mediterranean character that prolongs and modifies Tartessian culture. This occurs mainly in Low Alentejo and the Algarve, but has littoral extensions up to the Tagus mouth (namely the important city of Bevipo, modern Alcácer do Sal).
First form of writing in western Iberia (south of Portugal), the Southwest script (still to be translated), denotes strong Tartessian influence in its use of a modified Phoenician alphabet. In these writings the word Conii (similar to Cunetes or Cynetes, the people of the Algarve) appears frequently.
The poem Ora Maritima, written by Avienus in the 4th century and based on the Massaliote Periplus of the 6th century BCE, states that all of western Iberia was once called for the name of its people, the Oestriminis, which were replaced by an invasion of the Saephe or Ophis (meaning Serpent). From then on western Iberia would have been known as Ophiussa (Land of the Serpents).The poem also describes the various ethnic groups present at that time:
The Saephe or Ophis, today seen as probably Hallstatt culture Celts, in all of western Iberia (modern Portugal) between the Douro and the Sado rivers.
The Cempsi, probably Hallstatt culture Celts, in the Tagus mouth and the south up to the Algarve.
The Cynetes or Cunetes in the extreme south and some cities along the Atlantic coast (such as Olissipo, modern Lisbon), probably not Indo-European, but autochthonous Iberian (even if strongly or totally celticized over the next centuries).
The Dragani, Celt or Proto-Celt of the first Indo-European wave, in the mountainous areas of Galicia, northern Portugal, Asturias and Cantabria.
The Lusis, probably a first reference to the Lusitanians, similar to the Dragani (Celt or Proto-Celt of the first Indo-European wave).
ca. 600 BCE – Celts penetrate in the Northwest of the Peninsula, although it has been debated whether all tribes of this area are actually Celtic, Celtizied or just native with Celtic influences.
Penetration of Celtic culture into the northern mountainous strip is minimal and most likely the tribes of this region remain fully pre-Indo European.
5th century BCE
Further development of strong Central European (Celtic) influences and migrations in western Iberia north of the Tagus river.
Development of a second Castro Village culture in Galicia and northern Portugal.
The Celtic Calaicians or Gallaeci inhabit all the region above the Douro river (modern Galicia and northern Portugal).
First mint of coins and use of money in the Iberian peninsula.
Discovery voyages to the Atlantic by the Carthaginians.
The Greek historian Herodotus of Halicarnassus cites the word Iberia to designate what is now the Iberian peninsula, according to ancient Greek costume.
Urban bloom of Tartessian influenced Tavira.
575 BCE
Foundation of Emporion (Ampurias), in the Catalan coast, by Greek colonists from Phocaea.[7]
Soon afterwards the Northwest is rapidly re-Iberized from the south. This process cut the Celts of Iberia off from their continental counterparts, preventing the late Celtic culture of La Tène from affecting the peninsular Celts.
ca. 500 BCE
Decadence of Phoenician colonization of the Mediterranean coast of Iberia. Many of the colonies are deserted. Carthago slowly replaces the Phoenician in its former areas of dominion.
Tartessos disappears suddenly, probably destroyed by the Carthaginians as revenge of the Tartessian alliance with the Greeks during the battle of Alalia, in the coast of Corsica. The Turdetanians become their successors, although with a strong Carthaginian influence.[7]
4th century BCE
The Celtici, a new wave of Celtic migration, enter deeply into Portuguese territory and settle in the Alentejo also penetrating in the Algarve.
The Turduli and Turdetani, probably descendants of the Tartessians, are established in the area of the Guadiana river, in the south of modern Portugal, but celtized.
A series of cities in the Algarve, such as Balsa (Tavira), Baesuris (Castro Marim), Ossonoba(Faro) and Cilpes (Silves), are inhabited by the Cynetes or Cunetes progressively mingled with Celtic populations.
The Lusitanians (most probably proto-Celt) inhabit the area between the Douro and the Tagus rivers (and progressively penetrate the High Alentejo). They are neighbored to the east by the Vettones (also probably proto-Celt).
Rome begins to rise as a Mediterranean power rival to Carthage.

Iberia before Carthaginian conquests
ca. 400 BCE
The Greek historian Herodotus of Halicarnassus cites the word Iberia to designate what is now the Iberian peninsula, according to ancient Greek costume.[4]
Further development of strong Central European (Celtic) influences and migrations in western Iberia north of the Tagus river.
The Autrigones, along with the Turmodigi and Belli, migrate from Gaul, overrunning the entire area of the modern provinces of Cantabria and Burgos, where they establish their first capital Autraca or Austraca, located at the banks of the river Autra (Odra). They also gain an outlet to the sea by seizing from the Caristii further east the coastal highland region between the rivers Asón and Nervión, in the modern Vizcaya and Álava Basque provinces.[8]
The Belli settle along the lower Jalón river valley alongside their neighbours and clients, the Titii.[9]
3rd century BCE
The First Punic War (264-241 BCE) between Rome and Carthage. Roman victory.
ca. 300 BCE
The Celtic Calaicians or Gallaeci inhabit all the region above the Douro river (modern Galicia and northern Portugal).[10]
The Autrigones are driven out from southern Autrigonia (the western Burgos region) by the Turmodigi and the Vaccaei, who seize the Autrigones’ early capital Autraca.
The Belli join the Celtiberian confederacy alongside the Arevaci, Lusones and Titii, with whom they develop close political and military ties.
The Turboletae settle in the modern Teruel province.[11]
See also