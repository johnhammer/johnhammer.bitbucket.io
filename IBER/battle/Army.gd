extends Panel

var obj
var parent
var isMe

func UpdateNumbers(amount,maxamount):
	$Numbers.max_value = maxamount
	$Numbers.value = amount
	$Numbers/Value.text = String(amount)+"/"+String(maxamount)

func Init(data,_isMe,_parent):
	parent=_parent
	isMe=_isMe
	if isMe:
		$Retreat.visible = true
		
	obj = data
	
	$Name.text = obj.name
	obj.maxamount = 0
	var i = 1
	obj.unitsamt = 0
	for unit in obj.units:
		unit.maxamount = unit.amount
		obj.maxamount += unit.amount
		unit.node = get_node("Unit"+String(i))
		unit.node.Init(unit,isMe,self)
		i+=1
		obj.unitsamt+=1
	obj.amount = obj.maxamount
	UpdateNumbers(obj.amount,obj.maxamount)
	
	obj.morale = 100
	$Moral.value = obj.morale

func Damage(amount):
	var numbers = 0
	var dmg = amount/obj.unitsamt
	for unit in obj.units:
		var health = unit.node.Damage(dmg)
		numbers+=health
	if numbers>0:
		UpdateNumbers(numbers,obj.maxamount)
	else:
		numbers=0
		print('lost')

func Morale(amount):
	pass

func UnitSkill(data,name):
	var atk = data.unit.atk
	var hea = data.unit.hea
	var rng = data.unit.rng
	var numbers = data.amount
	
	match name:		
		"intimidate":
			parent.Morale(isMe,numbers)
		"rally":
			parent.Morale(!isMe,-numbers)
		"volley":
			parent.Attack(isMe,rng*numbers)
		"charge":
			parent.Attack(isMe,atk/2*numbers)
		"attack":
			parent.Attack(isMe,atk*numbers)
			#parent.Counter(isMe,self)
		"scavenge":
			parent.Attack(isMe,rng*numbers)
		"fireatwill":
			parent.Attack(isMe,rng*numbers)
		"ceasefire":
			parent.Attack(isMe,rng*numbers)
		"shieldwall":
			parent.Attack(isMe,rng*numbers)
		"snipe":
			parent.Attack(isMe,rng*numbers)
		"reload":
			parent.Attack(isMe,rng*numbers)
		"aim":
			parent.Attack(isMe,rng*numbers)
		
