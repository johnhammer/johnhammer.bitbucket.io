extends Button

var obj
var isMe
var parent
var speed = 10

func Init(data,_time,_isMe,_parent):
	icon = Resources.GetBattle(data)
	obj=data
	parent = _parent
	$progress.max_value = _time*speed
	isMe = _isMe
	if isMe:
		$progress.visible = true

func _process(delta):
	if !isMe: return
	if($progress.value<$progress.max_value):
		$progress.value += 1
	else:
		disabled = false

func _pressed():
	parent.Skill(obj)
	$progress.value = 0
	disabled = true

