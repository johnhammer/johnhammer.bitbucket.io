extends Panel

var obj
var parent

var UnitTypes={
	"slinger":{
		"atk":1,
		"hea":4,
		"rng":2,
		"skills":["volley","intimidate","scavenge"]
	},
	"scutati":{
		"atk":2,
		"hea":4,
		"rng":1,
		"skills":["attack","shieldwall","volley"]
	},
	"caestrati":{
		"atk":3,
		"hea":4,
		"rng":0,
		"skills":["attack","rally","snipe"]
	},
	"jinete":{
		"atk":4,
		"hea":4,
		"rng":0,
		"skills":["attack","charge","snipe"]
	},
	"archer":{
		"atk":1,
		"hea":4,
		"rng":2,
		"skills":["volley","fireatwill","ceasefire"]
	},
	"elephant":{
		"atk":6,
		"hea":8,
		"rng":1,
		"skills":["volley","charge","intimidate"]
	},
	"cavalry":{
		"atk":3,
		"hea":4,
		"rng":1,
		"skills":["attack","charge","volley"]
	},
	"skirmisher":{
		"atk":2,
		"hea":4,
		"rng":1,
		"skills":["attack","scavenge","volley"]
	},
	"swordsman":{
		"atk":3,
		"hea":4,
		"rng":0,
		"skills":["attack","intimidate","rally"]
	},
	"ballista":{
		"atk":0,
		"hea":8,
		"rng":0,
		"skills":["snipe","reload","aim"]
	}
}

var SkillTimes={
	"volley":5,
	"intimidate":20,
	"scavenge":15,
	"fireatwill":2,
	"ceasefire":0,
	"shieldwall":10,
	"snipe":0,
	"reload":5,
	"aim":5,
	"rally":20,
	"charge":20,
	"attack":5
}

func UpdateNumbers(amount,maxamount):
	obj.amount = amount
	$Numbers.max_value = maxamount
	$Numbers.value = amount
	$Numbers/Value.text = String(amount)+"/"+String(maxamount)

func Init(data,isMe,_parent):
	parent=_parent
	visible = true
	obj = data
	UpdateNumbers(data.amount,data.maxamount)
	$Sprite.texture = Resources.GetBattle(data.type)
	obj.unit = UnitTypes[data.type]
	var i = 1
	for skill in obj.unit.skills:
		get_node("h"+String(i)).Init(skill,SkillTimes[skill],isMe,self)
		i+=1

func Skill(name):
	parent.UnitSkill(obj,name)

func Damage(amount):
	var health = obj.amount-amount/obj.unit.hea
	if health>0:
		UpdateNumbers(health,obj.maxamount)
	else:
		health=0
		visible = false
	return health
