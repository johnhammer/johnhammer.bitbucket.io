extends Node2D

func _ready():
	Init()

func Init():
	var me = {
		"name":"Iberian army, Numantia",
		"units":[
			{
				"type":"slinger",
				"amount":20
			},
			{
				"type":"scutati",
				"amount":10
			}
		]
	}	
	var you = {
		"name":"Iberian army, Baskunes",
		"units":[
			{
				"type":"slinger",
				"amount":30
			},
			{
				"type":"scutati",
				"amount":5
			}
		]
	}
	$Me.Init(me,true,self)
	$You.Init(you,false,self)

func Attack(isMe,amount):
	if isMe:
		$You.Damage(amount)
	else:
		$Me.Damage(amount)

func Morale(isMe,amount):
	if isMe:
		$You.Morale(amount)
	else:
		$Me.Morale(amount)
