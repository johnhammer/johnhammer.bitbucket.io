extends Sprite

var tile_pos
var disabled
var tileset
var collided = false
var parent
var pivot = null

func _ready():
	parent = get_node("../..")
	disable()

func _process(delta):
	tileset = get_node("../Navigation2D/collision")
	var mousepos = get_global_mouse_position()
	tile_pos = tileset.world_to_map(mousepos)
	var pos = tile_pos
	var canBuild = true
	
	if pivot!=null:
		var ox = pivot.x
		var oy = pivot.y
		var dx = tile_pos.x
		var dy = tile_pos.y
		if(ox>dx): 
			ox = tile_pos.x
			dx = pivot.x
		if(oy>dy): 
			oy = tile_pos.y
			dy = pivot.y
		pos = Vector2(ox,oy)
		scale.x = dx - ox + 1
		scale.y = dy - oy + 1
		var size = scale.x*scale.y
		canBuild = parent.CanBuildPivoted(size)
	
	position = tileset.map_to_world(pos)	
	if(check_collision(pos) || !canBuild):
		collided = true
		modulate = Color(1, 0, 0)
	else:
		collided = false
		modulate = Color(1, 1, 1)
		
	

func disable():
	scale.x = 1
	scale.y = 1
	visible = false
	disabled = true
	
func enable(x,y):
	scale.x = x
	scale.y = y
	visible = true
	disabled = false

func check_collision(pos):
	for y in range(scale.y):
		for x in range(scale.x):
			if(tileset.get_cell(pos.x+x,pos.y+y)==-1):
				return true
	return false

func PivotedBuild():
	var ox = pivot.x
	var oy = pivot.y
	var dx = tile_pos.x
	var dy = tile_pos.y
	if(ox>dx): 
		ox = tile_pos.x
		dx = pivot.x
	if(oy>dy): 
		oy = tile_pos.y
		dy = pivot.y
	for y in range(dy - oy + 1):
		for x in range(dx - ox + 1):
			parent.CreateCurrentBuilding(Vector2(ox+x,oy+y))

func _input(event):
	if disabled || collided: return
	if event is InputEventMouseButton && event.button_index == BUTTON_LEFT:
		var pos = event.position
		if event.pressed && pos.x<700:
			LeftClick()
	if event is InputEventMouseButton && event.button_index == BUTTON_RIGHT:
		pivot = null
		disable()	
		
func LeftClick():
	if parent.IsCurrentBuildingPivoted():
		if pivot == null:
			pivot = tile_pos
			return
		else:
			PivotedBuild()
	else:			
		parent.CreateCurrentBuilding(tile_pos)
	pivot = null
	disable()
