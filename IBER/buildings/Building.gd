extends StaticBody2D

const ManResource = preload("res://man/Man.tscn")
const WomanResource = preload("res://man/Woman.tscn")

var timer = 0
var wait_time = 1
signal timeout
var tile_pos
var family = null
var object
var details = []
var tileset
var dir
var sellerFamily = null
var sellerPerson = null

func GetDoor():
	var pos
	if object.w == 1 && object.h == 1:
		pos = tile_pos
	else:
		if dir==2:
			pos = Vector2(tile_pos.x+(object.w/2),tile_pos.y+object.h)
		elif dir==4:
			pos = Vector2(tile_pos.x-1,tile_pos.y+(object.h+1)/2)
		elif dir==6:
			pos = Vector2(tile_pos.x+object.w,tile_pos.y+(object.h+1)/2-1)
		elif dir==8:
			pos = Vector2(tile_pos.x+(object.w/2)-1,tile_pos.y-1)
	
	var real = tileset.map_to_world(pos)
	real.x+=32
	real.y+=32
	return real

func GetImage(building):
	var f
	if "id" in building:
		f = building.id
	elif "v" in building:
		if dir==2 || dir==8:
			f = building.h
		else:
			f = building.v
	elif "b" in building:
		if dir==2:
			f = building.b
		elif dir==4:
			f = building.l
		elif dir==6:
			f = building.r
		elif dir==8:
			f = building.t
	else:
		f = 0
	return f

func Create(currentBuilding,pos,_dir,_season):
	dir = _dir
	var f = GetImage(currentBuilding)
	object = {
		"f":f,						#image
		"w":currentBuilding.size,	#width
		"h":currentBuilding.y,		#height
		"n":currentBuilding.action,	#name
	}
	if "home" in currentBuilding:
		object.home = 1
	if "work" in currentBuilding:
		object.work = 1
	if "state" in currentBuilding:
		object.s = 0
		object.ms = currentBuilding.state
	
	if "ground" in currentBuilding:
		tileset = get_node("../groundbuildings")
	else:
		tileset = get_node("../YSort/buildings")
	
	tile_pos = Vector2(pos.x,pos.y)
	position = tileset.map_to_world(tile_pos)
	tileset.set_cell(tile_pos.x,tile_pos.y,object.f)
	
	if !("ground" in currentBuilding):
		var collision = get_node("../Navigation2D/collision")
		for i in range(object.w):
			for j in range(object.h):
				collision.set_cell(tile_pos.x+i,tile_pos.y+j,-1)

	return #DOOR
	var door = GetDoor()
	var spr = Sprite.new()
	spr.texture = load("res://art/items/base/art.png")
	add_child(spr)
	spr.z_index = 1000
	spr.global_position.x = door.x
	spr.global_position.y = door.y
	
func LeaveTask():
	sellerFamily = null
	sellerPerson = null

func CanWork():
	return sellerPerson==null

func ClearSprites():
	for sprite in details:
		sprite.queue_free()
	details = []

func ReserveBuilding(family,person):
	sellerFamily = family
	sellerPerson = person

func CheckState(resource):
	object.s += 1
	if(object.s>=object.ms):
		object.s = 0
		if resource!=null: ClearSprites()
		return true
	if resource!=null: 
		var spr = Sprite.new()
		spr.texture = Resources.GetResource(resource)
		spr.position.y += 72
		spr.position.x += 16 + 16 * details.size()
		spr.z_index = 10
		add_child(spr)
		details.push_back(spr)
	return false
	
func spawn_human(man,building):
	var human
	if man.male:
		human = ManResource.instance()
	else:
		human = WomanResource.instance()
	get_parent().add_child(human)
	var door = GetDoor()
	var dest = building.GetDoor()
	human.Init(door.x,door.y,man,dest)
