extends "res://buildings/Building.gd"

var season
var planting
var winter = 0
var spring = 0
var summer = 0
var autumn = 0
var noseason = 0
var farmed = 0 #0 not 1 yes 2 wild

func Create(currentBuilding,pos,_dir,_season):
	.Create(currentBuilding,pos,_dir,_season)
	planting = currentBuilding.plant
	winter = currentBuilding.h
	spring = currentBuilding.p
	summer = currentBuilding.e
	autumn = currentBuilding.t
	season = _season
	if currentBuilding.n==-1:
		farmed=2
		SeasonEvent(_season)
	else:
		noseason = currentBuilding.n
		SeasonEvent(4)
	

func CheckState(resource):
	if(object.s+1>=object.ms):
		if(Farm()):
			return true
	.CheckState(resource)

func CanWork():
	var previouslyFarmed = IsPreviouslyFarmed(season)
	return farmed != 1 && !previouslyFarmed && !planting[season] == 1

func Farm():
	var result = false
	if planting==null:return false
	farmed = 1
	if planting[season] == 1:
		match season:
			0: object.f = winter
			1: object.f = spring
			2: object.f = summer
			3: object.f = autumn
			4: object.f = noseason
	elif planting[season] == 2:
		object.f = noseason
		result=true
	tileset.set_cell(tile_pos.x,tile_pos.y,object.f)
	return result

func IsPreviouslyFarmed(season):
	match season:
		0: return object.f == winter
		1: return object.f == spring
		2: return object.f == summer
		3: return object.f == autumn
	return false

func SeasonEvent(_season):
	var previouslyFarmed = IsPreviouslyFarmed(season)
	season = _season
	if (previouslyFarmed||farmed==2) && (planting[season] == 0 || planting[season] == 2): #nothing or reap
		match season:
			0: object.f = winter
			1: object.f = spring
			2: object.f = summer
			3: object.f = autumn
			4: object.f = noseason
	elif planting[season] == 1: #needs farming
		if farmed==0:
			object.f = noseason
	if farmed!=2:
		farmed = 0
	tileset.set_cell(tile_pos.x,tile_pos.y,object.f)
