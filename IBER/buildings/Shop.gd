extends "res://buildings/Building.gd"

var selling = null

func Transaction(family):
	var sprite = details.pop_back()
	if sprite==null:return
	sprite.queue_free()
	Families.AddGood(family,selling,1)
	Families.AddGood(sellerFamily,selling,-1)

func LeaveTask():
	selling = null
	ClearSprites()
	.LeaveTask()

func SetShop(item):
	selling = item
	for i in range(6):
		var spr = Sprite.new()
		spr.texture = Resources.GetResource(selling)
		spr.position.y += 72
		spr.position.x += 16 + 16 * details.size()
		spr.z_index = 10
		add_child(spr)
		details.push_back(spr)
	
