extends VBoxContainer

const ManInMenuResource = preload("res://man/ManInMenu.tscn")

func ShowInfo(family):
	if family == null:
		return
	var info = Families.FamilyInfo(family)
	var i = 1
	var j = 1
	
	for list in info:
		for item in list:
			var node = "Info/Panel"+String(i)+"/Button"+String(j)+""
			var button = get_node(node)
			if item==Families.NeedStates.NeededHaveExcedent || item==Families.NeedStates.NotNeededHave:
				button.modulate = Color(0,1,0)
			elif item==Families.NeedStates.NeededHave:
				button.modulate = Color(1,0,0)
			else:
				button.modulate = Color(0,0,1)
			j+=1
		j=1
		i+=1
			

	var index = 0
	for person in family.people:
		var panel = ManInMenuResource.instance()
		rect_min_size.y += 64
		$Members.rect_min_size.y += 64
		$Members.add_child(panel)
		panel.Init(index,person,family)
		index+=1
