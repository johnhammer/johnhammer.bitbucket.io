extends Node

var actions
var parent
var rnd 

var PersonTypes={
	"boy":0,
	"girl":1,
	"man":2,
	"woman":3,
	"s_boy":4,
	"s_girl":5,
	"s_man":6,
	"s_woman":7
}

var PersonStates={
	"home":0,
	"go_to_task":1,
	"task":2,
	"go_home":3,
	"homeless":4,
}

func Init(_parent,_actions):
	actions = _actions
	parent = _parent
	rnd = Random.Create(Random.GlobalSeed)

func Create(name,isMale,isAdult,isFree,colors,good,bad):	
	return {
		"n": name, #name
		"male": isMale, 
		"adult": isAdult, 
		"free": isFree,
		"colors":colors,
		"s":PersonStates.homeless,
		"good":good,
		"bad":bad,
		"action":null,
		"location":null
	}

### tasks

func CanPersonDoAction(person,action):
	return (action[0]==0&&person.male||action[0]==1||action[0]==2&&!person.male)&& \
		(action[1] == 0 && person.adult||action[1]==1||action[1]==2&&!person.adult) && \
		(action[2] == 0 && person.free||action[2]==1||action[2]==2&&!person.free)

func IsActionValid(family,person,action):
	if "need" in action && family.goods[action.need]<1:
		return false
	if !CanPersonDoAction(person,action.person):
		return false
	return true

func GetShopToBuy(family,action):
	var validBuildings = []
	for need in family.needs:
		if family.needs[need] == Families.NeedStates.NeededDontHave:
			var shop = parent.GetShop(need)
			if shop!=null:
				validBuildings.push_back({"building":shop,"resource":need})
	if(validBuildings.size()>0):
		var result = Random.Choice(rnd,validBuildings)
		action.resource = result.resource
		return result.building

func GetShopToSell(family,action):
	var validBuildings = []
	for need in family.needs:
		if family.needs[need] == Families.NeedStates.NeededHaveExcedent || family.needs[need] == Families.NeedStates.NotNeededHave:
			var shop = parent.GetEmptyShop()
			if shop!=null:
				validBuildings.push_back({"building":shop,"resource":need})
	if(validBuildings.size()>0):
		var result = Random.Choice(rnd,validBuildings)
		action.resource = result.resource
		return result.building

func GetBuilding(family,name,list):
	var validBuildings = []
	for building in list:
		if name == building.object.n:
			validBuildings.push_back(building)
	if(validBuildings.size()>0):
		return Random.Choice(rnd,validBuildings)

func GetTask(family,person,actions):
	var validActions = []
	for action in actions:
		if IsActionValid(family,person,action):
			var building
			if "building" in action:
				building = GetBuilding(family,action.building,family.works)
			if "public" in action:
				building = GetBuilding(family,action.public,parent.PublicBuildings)
			#buy and sell
			if action.name == "buy":
				building = GetShopToBuy(family,action)
			if action.name == "sell":
				building = GetShopToSell(family,action)
			if building != null && building.CanWork():
				validActions.push_back({"action":action,"building":building})
	if(validActions.size()>0):
		var result = Random.Choice(rnd,validActions)
		#if result.action.name == "sell":
		result.building.ReserveBuilding(family,self)
		return result

func DoAction(family,person,prioWork):
	var task = null
	if(prioWork):
		task = GetTask(family,person,actions.work)
	if(task==null):
		task = GetTask(family,person,actions.task)
	
	if(person.location != null && task != null):
		person.s = PersonStates.go_to_task
		person.action = task.action	
		person.location.spawn_human(person,task.building)
		person.location = task.building

func GoHome(family,person):
	var home = Families.GetHome(family)
	person.s = PersonStates.go_home
	person.action = null
	person.location.spawn_human(person,home)
	person.location = home

func LeaveTask(person):
	person.location.LeaveTask()

func HourAtTask(family,person):
	if person.action==null: return
		
	#buy and leave
	if person.action.name == "buy":
		person.location.Transaction(family)
		return true
	
	#produce or work
	var produce = null
	if person.action.name == "get water":
		Families.GetWater(family)
	elif person.action.name == "cook":
		Families.UseFurnace(family)
	
	if "produce" in person.action:
		produce = person.action.produce
	if person.location.CheckState(produce):
		#production
		if "produce" in person.action:
			Families.AddGood(family,person.action.produce,1)
		if "need" in person.action:
			Families.AddGood(family,person.action.need,-1)
	return person.location.CanWork()

func ClockEventPerson(hour,family,person):
	match person.s:
		PersonStates.homeless:
			var home = Families.GetHome(family)
			if home!=null:
				person.s = PersonStates.home
				person.location = home
				ClockEventPerson(hour,family,person)
		PersonStates.home:
			if hour >=0 && hour<=2: DoAction(family,person,true) #go to work
			elif hour >=5 && hour<=7: DoAction(family,person,false) #go to task
		PersonStates.task:
			var changeWork = !HourAtTask(family,person)
			if hour >=3 && hour<=4: 
				LeaveTask(person)
				GoHome(family,person)
			elif hour >=8 && hour<=10: 
				LeaveTask(person)
				GoHome(family,person)
			elif changeWork:
				LeaveTask(person)
				person.s = PersonStates.home
				ClockEventPerson(hour,family,person)

func ObjectiveReached(person):
	match person.s:
		PersonStates.go_home:
			person.s = PersonStates.home
		PersonStates.go_to_task:
			if person.action.name == "sell":
				person.location.SetShop(person.action.resource)
			person.s = PersonStates.task
