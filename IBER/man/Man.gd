extends RigidBody2D

var speed = 100
var eps = 5.5
var points = []

var target_position
var object
var cardinal_direction

func play(anim):	
	$skin.play(anim)
	$short.play(anim)
	$tunic.play(anim)
	$boots.play(anim)
	$hair.play(anim)
	$belt.play(anim)
	#$shield.play(anim)
	$capa.play(anim)
	$skin_carry.play(anim)
	$short_carry.play(anim)
	$tunic_carry.play(anim)
	$boots_carry.play(anim)
	$hair_carry.play(anim)
	$belt_carry.play(anim)
	
func Colorize(colors):
	$skin.modulate = colors.skin
	$short.modulate = colors.short
	$tunic.modulate = colors.tunic
	$boots.modulate = colors.boots
	$hair.modulate = colors.hair
	$belt.modulate = colors.belt
	#$shield.modulate = colors.shield
	$capa.modulate = colors.capa
	$skin_carry.modulate = colors.skin
	$short_carry.modulate = colors.short
	$tunic_carry.modulate = colors.tunic
	$boots_carry.modulate = colors.boots
	$hair_carry.modulate = colors.hair
	$belt_carry.modulate = colors.belt

func SetScale(scalex,scaley):
	position.y+=scaley*64
	$skin.scale = Vector2(scalex,scaley)
	$short.scale = Vector2(scalex,scaley)
	$tunic.scale = Vector2(scalex,scaley)
	$boots.scale = Vector2(scalex,scaley)
	$hair.scale = Vector2(scalex,scaley)
	$belt.scale = Vector2(scalex,scaley)
	$shield.scale = Vector2(scalex,scaley)
	$capa.scale = Vector2(scalex,scaley)

func Init(x,y,man,objective):
	object = man
	position.x = x+32
	position.y = y+32
	target_position = objective
	Colorize(man.colors)
	if!man.adult:
		SetScale(.35,.35)

	#move
	var o = position
	var d = target_position
	points = GetPathPoints(o,d)
	set_physics_process(true)

func objective_reached():
	Human.ObjectiveReached(object)
	queue_free()

func GetPathPoints(o,d):
	var raw = Array(get_node("../Navigation2D").get_simple_path(o, d, false))
	var new = []
	for i in range(raw.size()-1):
		new.push_back(raw[i])
		if(raw[i].x!=raw[i+1].x && raw[i].y!=raw[i+1].y):
			var modx = int(raw[i].x)%64
			if(modx==0 || modx==63):
				new.push_back(Vector2(raw[i+1].x,raw[i].y))
			else:
				new.push_back(Vector2(raw[i].x,raw[i+1].y))
	new.push_back(d)
	return new

func _physics_process(delta):
	if points.size() > 1:
		var distance = points[1] - position
		var l = distance.length()
		var direction = distance.normalized() # direction of movement
		if l > eps:
			set_linear_velocity(direction*speed)
			set_dir(direction)
		else:
			points.pop_front()
		update()
	else:
		set_linear_velocity(Vector2(0, 0))
		set_dir(null)
		objective_reached()

func _draw():
	return
	if points.size() > 1:
		for p in points:
			draw_circle(p - get_global_position(), 8, Color(1, 0, 0)) # we draw a circle (convert to global position first)


### movement ###

func set_dir(dir):	
	var type = "idle"
	if(dir!=null):
		type = "run"
		cardinal_direction = int(4.0 * (dir.rotated(PI / 4.0).angle() + PI) / TAU)
	match cardinal_direction:
		0:
			play("4"+type)
		1:
			play("8"+type)
		2:
			play("6"+type)
		3:
			play("2"+type)
	
