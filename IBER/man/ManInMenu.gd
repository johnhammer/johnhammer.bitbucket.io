extends Panel

var PersonTypes={
	"boy":0,
	"girl":1,
	"man":2,
	"woman":3,
	"s_boy":4,
	"s_girl":5,
	"s_man":6,
	"s_woman":7
}

var SocialStatus = {
	"guest":0,
	"worker":1,
	"citizen":2,
	"noble":3,
	"rider":4,
}

func Colorize(colors,status):	
	$Man/skin.modulate = colors.skin
	$Man/short.modulate = colors.short
	$Man/hair.modulate = colors.hair
	$Man/belt.modulate = colors.belt
	$Man/tunic.modulate = colors.tunic
	$Man/boots.modulate = colors.boots
	$Man/shield.modulate = colors.shield
	$Man/capa.modulate = colors.capa
	if status>=SocialStatus.worker:
		$Man/belt.visible = true
	if status>=SocialStatus.citizen:
		$Man/tunic.visible = true
	if status>=SocialStatus.noble:
		$Man/boots.visible = true
		$Man/shield.visible = true
	if status>=SocialStatus.rider:
		$Man/capa.visible = true

	$Woman/skin.modulate = colors.skin
	$Woman/short.modulate = colors.short
	$Woman/tunic.modulate = colors.tunic
	$Woman/belt.modulate = colors.belt
	$Woman/chal.modulate = colors.chal
	$Woman/skirt.modulate = colors.skirt
	$Woman/headwear.modulate = colors.headwear
	$Woman/boots.modulate = colors.boots
	$Woman/earring.modulate = colors.earring
	$Woman/capa.modulate = colors.capa
	$Woman/hair.modulate = colors.hair
	if status>=SocialStatus.worker:
		$Woman/belt.visible = true
	if status>=SocialStatus.citizen:
		$Woman/chal.visible = true
		$Woman/skirt.visible = true
	if status>=SocialStatus.noble:
		$Woman/headwear.visible = true
		$Woman/boots.visible = true
	if status>=SocialStatus.rider:
		$Woman/earring.visible = true
		$Woman/capa.visible = true
	if status<SocialStatus.noble:
		$Woman/hair.visible = true
	
func SetScale(scalex,scaley):
	$Man/skin.scale = Vector2(scalex,scaley)
	$Man/short.scale = Vector2(scalex,scaley)
	$Man/tunic.scale = Vector2(scalex,scaley)
	$Man/boots.scale = Vector2(scalex,scaley)
	$Man/hair.scale = Vector2(scalex,scaley)
	$Man/belt.scale = Vector2(scalex,scaley)
	$Man/shield.scale = Vector2(scalex,scaley)
	$Man/capa.scale = Vector2(scalex,scaley)
	
	$Woman/skin.scale = Vector2(scalex,scaley)
	$Woman/short.scale = Vector2(scalex,scaley)
	$Woman/skirt.scale = Vector2(scalex,scaley)
	$Woman/tunic.scale = Vector2(scalex,scaley)
	$Woman/hair.scale = Vector2(scalex,scaley)
	$Woman/headwear.scale = Vector2(scalex,scaley)
	$Woman/earring.scale = Vector2(scalex,scaley)
	$Woman/boots.scale = Vector2(scalex,scaley)
	$Woman/belt.scale = Vector2(scalex,scaley)
	$Woman/chal.scale = Vector2(scalex,scaley)
	$Woman/capa.scale = Vector2(scalex,scaley)

func Init(index,pers,family):
	rect_position.x = 16
	rect_position.y = (index)*72
	Colorize(pers.colors,family.status)
	$Text.text = pers.n + "\n" + String(pers.s)
	$Man.visible = pers.male
	$Woman.visible = !pers.male
	$Good.frame = pers.good
	$Bad.frame = pers.bad
	if!pers.adult:
		SetScale(.35,.35)
