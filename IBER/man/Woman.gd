extends "res://man/Man.gd"

func play(anim):	
	$skin.play(anim)
	$short.play(anim)
	$skirt.play(anim)
	$tunic.play(anim)
	$hair.play(anim)
	$headwear.play(anim)
	$earring.play(anim)
	$boots.play(anim)
	$belt.play(anim)
	$chal.play(anim)
	$capa.play(anim)

func SetScale(scalex,scaley):
	$skin.scale = Vector2(scalex,scaley)
	$short.scale = Vector2(scalex,scaley)
	$skirt.scale = Vector2(scalex,scaley)
	$tunic.scale = Vector2(scalex,scaley)
	$hair.scale = Vector2(scalex,scaley)
	$headwear.scale = Vector2(scalex,scaley)
	$earring.scale = Vector2(scalex,scaley)
	$boots.scale = Vector2(scalex,scaley)
	$belt.scale = Vector2(scalex,scaley)
	$chal.scale = Vector2(scalex,scaley)
	$capa.scale = Vector2(scalex,scaley)

func Colorize(colors):
	$skin.modulate = colors.skin
	$short.modulate = colors.short
	$skirt.modulate = colors.skirt
	$tunic.modulate = colors.tunic
	$hair.modulate = colors.hair
	$headwear.modulate = colors.headwear
	$earring.modulate = colors.earring
	$boots.modulate = colors.boots
	$belt.modulate = colors.belt
	$chal.modulate = colors.chal
	$capa.modulate = colors.capa
