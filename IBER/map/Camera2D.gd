extends Camera2D

var fixed_toggle_point = Vector2(0,0)

var currently_moving_map = false
var zoomfactor = 1.0

func _unhandled_input (event):
	if event is InputEventMouseButton and event.pressed:
		var old_pos = position
		position = get_global_mouse_position()
		set_offset(  old_pos - get_global_mouse_position()  + get_offset() )
		
		if event.button_index == BUTTON_WHEEL_UP :
			if(zoomfactor>1.0): zoomfactor *= 0.5
		if event.button_index == BUTTON_WHEEL_DOWN:
			if(zoomfactor<16.0): zoomfactor *= 2.0
		set_zoom(Vector2(zoomfactor,zoomfactor))

func _process(delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) or Input.is_mouse_button_pressed(BUTTON_MIDDLE):
		# This happens once 'move_map' is pressed
		if( !currently_moving_map ):
			var ref = get_viewport().get_mouse_position()
			fixed_toggle_point = ref
			currently_moving_map = true
		# This happens while 'move_map' is pressed
		move_map_around()
	else:
		currently_moving_map = false

# this stays the same
func move_map_around():
	var ref = get_viewport().get_mouse_position()
	self.global_position.x -= (ref.x - fixed_toggle_point.x)*zoomfactor
	self.global_position.y -= (ref.y - fixed_toggle_point.y)*zoomfactor
	fixed_toggle_point = ref
