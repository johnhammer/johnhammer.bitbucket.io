extends Node

var parent
var time = 0
var time_mult = 1.0
var paused = false

var hour = 0
var day = 0
var season = 0
var year = 0
var generation = 0

var timeToHour = 2
var hoursToDay = 12
var daysToSeason = 2
var yearsToGeneration = 2

var sun

func Init(parentNode):
	parent = parentNode
	set_process(true)
	parent.modulate = GetHourColor(hour,season)

func GetHourColor(hour,season):
	match hour:
		0: return Color(1,1,0.85)
		1: return Color(1,1,0.9)
		2: return Color(1,1,0.95)
		3: return Color(1,1,1)
		4: return Color(1,0.95,0.95)
		5: return Color(1,0.9,0.9)
		6: return Color(1,0.85,0.85)
		7: return Color(0.95,0.85,0.85)
		8: return Color(0.75,0.85,0.85)
		9: return Color(0.75,0.85,0.85)
		10: return Color(0.75,0.85,0.85)
		11: return Color(0.95,0.85,0.85)

func _process(delta):
	if parent==null: return #for non main rooms
	
	if not paused: time += delta * time_mult
	if time>=timeToHour:
		hour+=1
		if hour>=hoursToDay: 
			day+=1
			hour=0
			if day>=daysToSeason:
				season+=1
				day=0
				if season>=4:
					year+=1
					season=0
					if year>=yearsToGeneration:
						generation+=1
						year=0
						Families.SwapGeneration()
				parent.SeasonEvent(season)
		parent.modulate = GetHourColor(hour,season)
		time=0
		#print(hour)
		parent.ClockEvent(hour)
		
	
	
	
	
	
	
	#print(String(hour) + " " + String(day) + " " + String(season) + " " + String(year))
