extends Node2D

var parent
var menu_dictionary
var menu_current
var familyGold
var publicGold

func Init(parentNode,menu):
	parent = parentNode
	menu_dictionary = menu

#### FAMILIES ####
func AddFamily(name):
	var popup = $Control/Info/MenuButton.get_popup()
	popup.add_item(name)
	popup.connect("id_pressed", self, "SelectFamily")

func SelectFamily(id):
	var popup = $Control/Info/MenuButton.get_popup()
	parent.SelectFamily(id)

func Refresh(_publicGold,family=null):
	familyGold = 0
	publicGold = _publicGold
	if(family!=null):
		familyGold = family.gold
		$Control/Info/FamilyName.text = family.name
		$Control/Info/PrivateGold.text = String(familyGold)	
	$Control/Info/PublicGold.text = String(publicGold)
	load_menu("menu0")
	
func FamilyPanel():
	var family = parent.currentFamily
	$Control/FamilyPanel.visible = true
	$Control/FamilyPanel/scroll/box.ShowInfo(family)	

#### END FAMILIES ####

#### WORLD ####

func WorldPanel():
	get_node("../../World").visible = true
	get_node("../../Nodes").visible = false
	
	$Control/Info.visible = false
	$Control/WorldInfo.visible = true

func CityInfo(name,text):
	$Control/WorldInfo/CityName.text = name
	$Control/WorldInfo/CityInfo.text = text

#### END WORLD ####

func load_menu(menu):
	menu_current = menu
	var node = menu_dictionary[menu_current]
	setup_button(1,node[0])
	setup_button(2,node[1])
	setup_button(3,node[2])
	setup_button(4,node[3])
	setup_button(5,node[4])
	setup_button(6,node[5])
	setup_button(7,node[6])
	setup_button(8,node[7])
	setup_button(9,node[8])

func get_button(id):
	var buttons = get_node("Control/Panel")
	return buttons.get_node("Button"+str(id))

func setup_button(id,item):
	var button = get_button(id)	
	#gold
	if "cg" in item:
		button.disabled = int(item.cg)>publicGold
	elif "fg" in item:
		button.disabled = int(item.fg)>familyGold
	else:
		button.disabled = false
	#name
	if "n" in item:
		button.text = item.n
	elif "a" in item:
		button.text = item.a
	else:
		button.text = ""

func RefreshMenu():
	$Control/FamilyPanel.visible = false
	get_node("../../World").visible = false
	get_node("../../Nodes").visible = true
	$Control/Info.visible = true
	$Control/WorldInfo.visible = false

func button_toggled(target):	
	var node = menu_dictionary[menu_current][target-1]
	if !"d" in node:
		RefreshMenu()
	
	if "s" in node:
		if node.s == "families":
			FamilyPanel()
		elif node.s == "map":
			WorldPanel()
	if "m" in node:
		load_menu(node.m)
	elif "a" in node:	
		if "cg" in node:
			parent.PublicGoldCost = int(node.cg)
			parent.PrivateGoldCost = 0
		elif "fg" in node:
			parent.PublicGoldCost = 0
			parent.PrivateGoldCost = int(node.fg)
		parent.OnActionPicked(node.a)

func _on_Button1_pressed(): button_toggled(1)
func _on_Button2_pressed(): button_toggled(2)
func _on_Button3_pressed(): button_toggled(3)
func _on_Button4_pressed(): button_toggled(4)
func _on_Button5_pressed(): button_toggled(5)
func _on_Button6_pressed(): button_toggled(6)
func _on_Button7_pressed(): button_toggled(7)
func _on_Button8_pressed(): button_toggled(8)
func _on_Button9_pressed(): button_toggled(9)

func _on_up_pressed(): 
	parent.SetDir(8)
	$Control/Info/up.pressed = true
	$Control/Info/left.pressed = false
	$Control/Info/right.pressed = false
	$Control/Info/down.pressed = false
func _on_left_pressed(): 
	parent.SetDir(4)
	$Control/Info/up.pressed = false
	$Control/Info/left.pressed = true
	$Control/Info/right.pressed = false
	$Control/Info/down.pressed = false
func _on_right_pressed(): 
	parent.SetDir(6)
	$Control/Info/up.pressed = false
	$Control/Info/left.pressed = false
	$Control/Info/right.pressed = true
	$Control/Info/down.pressed = false
func _on_down_pressed(): 
	parent.SetDir(2)
	$Control/Info/up.pressed = false
	$Control/Info/left.pressed = false
	$Control/Info/right.pressed = false
	$Control/Info/down.pressed = true
	
