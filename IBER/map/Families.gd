extends Node

var rnd
var parent
var families = []

var NeedStates={
	"NotNeededDontHave":0,
	"NotNeededHave":1,
	"NeededDontHave":2,
	"NeededHave":3,
	"NeededHaveExcedent":4
}

var SocialStatus = {
	"guest":0,
	"worker":1,
	"citizen":2,
	"noble":3,
	"rider":4
}

var names = [
	"abaŕ","aibe","aile","ain","aitu","aiun","aker","albe","aloŕ","an","anaŕ","aŕbi","aŕki","aŕs","asai","aster","ata","atin","atun","aunin","auŕ","austin","baiser","balaŕ","balke","bartaś","baś","bastok","bekon","belauŕ","beleś","bels","bene","beŕ","beri","beŕon","betan","betin","bikir","bilos","bin","bir","bitu","biuŕ","bolai","boŕ","boś","boton","ekes","ekaŕ","eler","ena","esto","eten","eter","iar","iaun","ibeś","ibeis","ike","ikoŕ","iltiŕ","iltur","inte","iskeŕ","istan","iunstir","iur","kaisur","kakeŕ","kaltuŕ","kani","kaŕes","kaŕko","katu","keŕe","kibaś","kine","kitaŕ","kon","koŕo","koŕś","kuleś","kurtar","lako","lauŕ","leis","lor","lusban","nalbe","neitin","neŕse","nes","niś","nios","oŕtin","sakaŕ","sakin","saltu","śani","śar","seken","selki","sike","sili","sine","sir","situ","soket","sor","sosin","suise","taker","talsku","tan","tanek","taneś","taŕ","tarban","taŕtin","taś","tautin","teita","tekeŕ","tibaś","tikeŕ","tikirs","tikis","tileis","tolor","tuitui","tumar","tuŕś","turkir","tortin","ulti","unin","uŕke","ustain","ḿbaŕ","nḿkei"
]

func Init(parentNode):
	parent = parentNode
	rnd = Random.Create(Random.GlobalSeed)
	
	AddFamily(Family(rnd))
	AddFamily(Family(rnd))
	#AddFamily(Family(rnd))

func Name(rnd):
	return Random.Choice(rnd,names)+Random.Choice(rnd,names)

func Person(rnd,male,adult,free):
	var hairR = Random.FloatRange(rnd,0.0,0.91)
	var hairG = Random.FloatRange(rnd,0.0,hairR)
	var hairB = Random.FloatRange(rnd,0.0,hairG)
	var colors = {
		"skin" : Color(
			Random.FloatRange(rnd,0.9,1.0), 
			Random.FloatRange(rnd,0.74,0.9), 
			Random.FloatRange(rnd,0.38,0.74)),
		"short" : Color(0.87, 0.87, 0.87),
		"tunic" : Color(0.87, 0.87, 0.87),
		"boots" : Color(0.39, 0.29, 0.2),
		"hair" : Color(hairR,hairG,hairB),
		"belt" : Color(0.37, 0.26, 0.13),
		"shield" : Color(0.49, 0.15, 0.18),
		"capa" : RandomColor(rnd),
		"skirt" : RandomColor(rnd),
		"headwear" : RandomColor(rnd),
		"earring" : RandomColor(rnd),
		"chal" : RandomColor(rnd)
	}
	var good = Random.Range(rnd,0,9)
	var bad = Random.Range(rnd,0,9)
	return Human.Create(Name(rnd),male,adult,free,colors,good,bad)

func RandomColor(rnd):
	return Color(Random.FloatRange(rnd,0.0,1.0),
		Random.FloatRange(rnd,0.0,1.0),
		Random.FloatRange(rnd,0.0,1.0))

func Family(rnd):
	var people = [
		Person(rnd,true,true,true),
		Person(rnd,false,true,true)
	]
	
	var family = {
		"id":families.size(),
		"gold" : Random.Range(rnd,5000,10000),
		"people" : people,
		"slaves" : [],
		"goods" : {
			"urns":0,
			"beer":0,
			"fish":0,
			"meat":0,
			"iron":0,
			"boots":0,
			"clothes":0,
			"leather":0,
			"garum":0,
			"olives":0,
			"figs":0,
			"oil":0,
			"linen":0,
			"milk":0,
			"grain":0,
			"pots":0,
			"tools":0,
			"cows":0,
			"horses":0
		},
		"needs":{
			"urns":NeedStates.NotNeededDontHave,
			"beer":NeedStates.NotNeededDontHave,
			"fish":NeedStates.NotNeededDontHave,
			"meat":NeedStates.NotNeededDontHave,
			"iron":NeedStates.NotNeededDontHave,
			"boots":NeedStates.NotNeededDontHave,
			"clothes":NeedStates.NotNeededDontHave,
			"leather":NeedStates.NotNeededDontHave,
			"garum":NeedStates.NotNeededDontHave,
			"olives":NeedStates.NotNeededDontHave,
			"figs":NeedStates.NotNeededDontHave,
			"oil":NeedStates.NotNeededDontHave,
			"linen":NeedStates.NotNeededDontHave,
			"milk":NeedStates.NotNeededDontHave,
			"grain":NeedStates.NotNeededDontHave,
			"pots":NeedStates.NotNeededDontHave,
			"tools":NeedStates.NotNeededDontHave,
			"cows":NeedStates.NotNeededDontHave,
			"horses":NeedStates.NotNeededDontHave
		},
		"visitedWater":0,
		"visitedFurnace":0,
		"status": SocialStatus.guest,
		"works" : [],
		"private" : []	
	}
	family.name = GetFamilyName(family)
	CheckStatus(family)
	return family

func GetFamilyName(family):
	return family.people[0].n + " " + family.people[1].n

func AddFamily(family):
	parent.AddFamily(family)
	families.push_back(family)
	
func SelectFamily(id):
	return families[id]

func AddBuilding(family,building,cost):
	if "work" in building.object:
		family.works.push_back(building)
	else:
		family.private.push_back(building)
	family.gold -= cost
	building.family = family

func AddGood(family,good,value):
	family.goods[good] += value
	UpdateGood(family,good)
	CheckStatus(family)

func ClockEvent(hour):
	for family in families:
		for person in family.people:
			Human.ClockEventPerson(hour,family,person)
		if hour==0:
			Tick(family)

#next generation

func Child(rnd,father,mother):
	var colors = {
		"skin" : Random.Choice(rnd,[father.colors.skin,mother.colors.skin]),
		"short" : Color(0.87, 0.87, 0.87),
		"tunic" : Color(0.87, 0.87, 0.87),
		"boots" : Color(0.39, 0.29, 0.2),
		"hair" : Random.Choice(rnd,[father.colors.hair,mother.colors.hair]),
		"belt" : Color(0.37, 0.26, 0.13),
		"shield" : Color(0.49, 0.15, 0.18),
		"capa" : RandomColor(rnd),
		"skirt" : RandomColor(rnd),
		"headwear" : RandomColor(rnd),
		"earring" : RandomColor(rnd),
		"chal" : RandomColor(rnd)
	}
	var good = Random.Choice(rnd,[father.good,mother.good,father.bad,mother.bad])
	var bad = Random.Choice(rnd,[father.good,mother.good,father.bad,mother.bad])
	return Human.Create(Name(rnd),Random.Bool(rnd),false,mother.free,colors,good,bad)

func CreateChildren(family):
	var children = []
	var amount = Random.Range(rnd,0,6)
	for i in range(amount):
		children.push_back(Child(rnd,family.people[0],family.people[1]))
	return children

func SwapGeneration():
	pass
#	var females = []
#	for family in families:
#		var adults = []
#		var children = CreateChildren(family)
#		for person in family.people:
#			if !person.adult:
				

# buildings needs

func Tick(family):
	if(family.visitedWater>0):
		family.visitedWater -= 1
	if(family.visitedFurnace>0):
		family.visitedFurnace -= 1

func GetWater(family):
	family.visitedWater=5

func UseFurnace(family):
	family.visitedFurnace=5

func GetBuildingAccess(property):
	if property==0:
		return NeedStates.NeededDontHave
	elif property<3:
		return NeedStates.NeededHave
	else:
		return NeedStates.NeededHaveExcedent

func GetBuilding(family,name):
	for building in family.private:
		if building.object.n == name:
			return building
	return null

func GetBuildingNeed(family,name):
	var building = GetBuilding(family,name)
	if building == null:
		return NeedStates.NeededDontHave
	else:
		return NeedStates.NeededHaveExcedent

func GetHome(family):
	var home = GetBuilding(family,"mansion")
	if home==null:
		home = GetBuilding(family,"house")
	return home

# needs

func UpdateGood(family,good):
	var excedentValue = 2
	match family.needs[good]:
		NeedStates.NotNeededDontHave:
			if family.goods[good]>0:
				family.needs[good] = NeedStates.NotNeededHave
		NeedStates.NotNeededHave:
			if family.goods[good]<1:
				family.needs[good] = NeedStates.NotNeededDontHave
		NeedStates.NeededDontHave:
			if family.goods[good]>=excedentValue:
				family.needs[good] = NeedStates.NeededHaveExcedent
			elif family.goods[good]>0:
				family.needs[good] = NeedStates.NeededHave
		NeedStates.NeededHave:
			if family.goods[good]>=excedentValue:
				family.needs[good] = NeedStates.NeededHaveExcedent
			elif family.goods[good]<1:
				family.needs[good] = NeedStates.NeededDontHave
		NeedStates.NeededHaveExcedent:
			if family.goods[good]<1:
				family.needs[good] = NeedStates.NeededDontHave
			elif family.goods[good]<excedentValue:
				family.needs[good] = NeedStates.NeededHave

func CheckStatus(family):
	UpdateNeedsByStatus(family,family.status)
	while StatusNeedsFilled(family,family.status):
		family.status+=1

func StatusNeedsFilled(family,status):
	var needs = GetStatusNeeds(family,status)
	for need in needs:
		if need == NeedStates.NeededDontHave:
			return false
	return true

func ActivateNeed(family,need):
	family.needs[need] = NeedStates.NeededDontHave
	UpdateGood(family,need)

func IsNeedHave(need):
	return need == NeedStates.NeededHave ||\
		need == NeedStates.NotNeededHave ||\
		need == NeedStates.NeededHaveExcedent

func ActivateFoodNeeds(family):
	ActivateNeed(family,"beer")
	ActivateNeed(family,"fish")
	ActivateNeed(family,"meat")
	ActivateNeed(family,"grain")
	ActivateNeed(family,"figs")
	ActivateNeed(family,"milk")
	
func ActivateLuxNeeds(family):
	ActivateNeed(family,"urns")
	ActivateNeed(family,"garum")
	ActivateNeed(family,"oil")

func GetGoodAmountPair(family,good):
	return {"n":good,"amt":family.goods[good]}

func SortGoodAmountPairsFunc(a,b):
	return a.amt > b.amt

func SortGoodAmountPairs(list):
	list.sort_custom(self,"SortGoodAmountPairsFunc")
	return list

func GetFoodAmountBySort(family,sort):
	var foods = [
		GetGoodAmountPair(family,"beer"),
		GetGoodAmountPair(family,"fish"),
		GetGoodAmountPair(family,"meat"),
		GetGoodAmountPair(family,"grain"),
		GetGoodAmountPair(family,"figs"),
		GetGoodAmountPair(family,"milk")
	]
	var list = SortGoodAmountPairs(foods)
	var need = list[sort].n
	return family.needs[need]
	
func GetLuxAmountBySort(family,sort):
	var luxes = [
		GetGoodAmountPair(family,"urns"),
		GetGoodAmountPair(family,"garum"),
		GetGoodAmountPair(family,"oil")
	]
	var list = SortGoodAmountPairs(luxes)
	var need = list[sort].n
	return family.needs[need]

func UpdateNeedsByStatus(family,status):
	var ft1 = GetFoodAmountBySort(family,0) 
	var ft2 = GetFoodAmountBySort(family,1) 
	var ft3 = GetFoodAmountBySort(family,2) 
	var ft4 = GetFoodAmountBySort(family,3)
	var lx1 = GetLuxAmountBySort(family,0)
	var lx2 = GetLuxAmountBySort(family,1)
	match status:
		SocialStatus.guest:
			if !IsNeedHave(ft1):
				ActivateFoodNeeds(family)
			ActivateNeed(family,"pots")
		SocialStatus.guest:
			if !IsNeedHave(ft2):
				ActivateFoodNeeds(family)
			ActivateNeed(family,"clothes")
			ActivateNeed(family,"tools")
		SocialStatus.guest:
			if !IsNeedHave(ft3):
				ActivateFoodNeeds(family)
			if !IsNeedHave(lx1):
				ActivateLuxNeeds(family)
			ActivateNeed(family,"boots")
		SocialStatus.guest:
			if !IsNeedHave(ft4):
				ActivateFoodNeeds(family)
			if !IsNeedHave(lx2):
				ActivateLuxNeeds(family)
			ActivateNeed(family,"horses")
	
func GetStatusNeeds(family,status):
	var hasHome = GetBuildingNeed(family,"house")
	var waterAcess = GetBuildingAccess(family.visitedWater)
	var furnaceAcess = GetBuildingAccess(family.visitedFurnace)
	var hasMansion = GetBuildingNeed(family,"mansion")
	var hasUrnfield = GetBuildingNeed(family,"urnfield")
	var ft1 = GetFoodAmountBySort(family,0) 
	var ft2 = GetFoodAmountBySort(family,1) 
	var ft3 = GetFoodAmountBySort(family,2) 
	var ft4 = GetFoodAmountBySort(family,3)
	var lx1 = GetLuxAmountBySort(family,0)
	var lx2 = GetLuxAmountBySort(family,1)
	match status:
		SocialStatus.guest:
			return [
				hasHome,
				ft1,
				waterAcess,
				family.needs.pots
			]
		SocialStatus.worker:
			return [
				furnaceAcess,
				ft2,
				family.needs.clothes,
				family.needs.tools
			]
		SocialStatus.citizen:
			return [	#infantry
				hasMansion,
				ft3,
				family.needs.boots,
				lx1
			]
		SocialStatus.noble:
			return [	#cavalry
				hasUrnfield,
				ft4,
				family.needs.horses,
				lx2
			]
		SocialStatus.rider:
			return [NeedStates.NeededDontHave]

func FamilyInfo(family):	
	return [
		GetStatusNeeds(family,0),
		GetStatusNeeds(family,1),
		GetStatusNeeds(family,2),
		GetStatusNeeds(family,3)
	]
