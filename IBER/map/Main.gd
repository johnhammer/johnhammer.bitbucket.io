extends Node2D

const BuildingResource = preload("res://buildings/Building.tscn")
const PlantResource = preload("res://buildings/Plant.tscn")
const ShopResource = preload("res://buildings/Shop.tscn")

var rnd
var plants = []
var buildings
var currentBuilding
var currentFamily
var publicGold = 2000
var PublicGoldCost = 0
var PrivateGoldCost = 0
var PublicBuildings = []
var PublicShops = []
var GlobalBuildDir = 2

func SetDir(dir): GlobalBuildDir = dir

func load_json(name):
	var res = null
	var file = File.new()
	file.open("res://data/"+name+".json", file.READ)
	var text = file.get_as_text()
	var result_json = JSON.parse(text)
	if result_json.error == OK:  # If parse OK
		res = result_json.result
	else:  # If parse has errors
		print("Error: ", result_json.error)
		print("Error Line: ", result_json.error_line)
		print("Error String: ", result_json.error_string)
	file.close()
	return res

func _ready():
	rnd = Random.Create(Random.GlobalSeed)
	
	var menu = load_json("menu")
	var actions = load_json("actions")
	var world = load_json("world")
	buildings = load_json("buildings")
	$Canvas/Control.Init(self,menu)
	Families.Init(self)
	$Canvas/Control.Refresh(publicGold)
	Clock.Init(self)
	Human.Init(self,actions)
	$Nodes.Init()
	$World.Init(world,$Canvas/Control)
	

#### BUILD ####

func IsCurrentBuildingPivoted():
	return currentBuilding.action == "grain field" || currentBuilding.action == "linen field" 

func CanBuildPivoted(size):
	if PublicGoldCost>0:
		return publicGold>=PublicGoldCost*size
	if PrivateGoldCost>0:
		return currentFamily.gold>=PrivateGoldCost*size
	return true

func OnActionPicked(action):	
	currentBuilding = buildings[action]	
	currentBuilding.action = action	
	if "y" in currentBuilding: currentBuilding.y = currentBuilding.y
	else: currentBuilding.y = currentBuilding.size
	$Nodes/Builder.enable(currentBuilding.size,currentBuilding.y)
	
func CreateCurrentBuilding(pos):	
	#create 
	var building
	if "plant" in currentBuilding: 
		building = PlantResource.instance()
		plants.push_back(building)
	elif currentBuilding.action == "shop":
		building = ShopResource.instance()
		PublicShops.push_back(building)
	else:
		building = BuildingResource.instance()
		
	$Nodes.add_child(building)
	building.Create(currentBuilding,pos,GlobalBuildDir,Clock.season)
	
	#cost
	if PublicGoldCost>0:
		publicGold -= PublicGoldCost
		PublicBuildings.push_back(building)
	if PrivateGoldCost>0:
		Families.AddBuilding(currentFamily,building,PrivateGoldCost)
		
	$Canvas/Control.Refresh(publicGold,currentFamily)

func GetShop(item):
	var shops = []
	for shop in PublicShops:
		if(shop.selling==item):
			shops.push_back(shop)
	return Random.Choice(rnd,shops)
	
func GetEmptyShop():	
	var shops = []
	for shop in PublicShops:
		if(shop.sellerFamily == null):
			shops.push_back(shop)
	return Random.Choice(rnd,shops)

#### END BUILD ####

#### FAMILIES ####

func AddFamily(family):
	$Canvas/Control.AddFamily(family.name)

func SelectFamily(id):
	currentFamily = Families.SelectFamily(id)
	$Canvas/Control.Refresh(publicGold,currentFamily)

#### END FAMILIES ####

### EVENTS ###

func ClockEvent(hour):
	Families.ClockEvent(hour)
	
func SeasonEvent(season):
	for plant in plants:
		plant.SeasonEvent(season)
		
### END EVENTS ###
