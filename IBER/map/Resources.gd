extends Node

var resources = {}
var battle = {}

func _ready():
	resources.water = load("res://art/items/resources/resources_01.png")
	resources.urns = load("res://art/items/resources/resources_02.png")
	resources.beer = load("res://art/items/resources/resources_03.png")
	resources.fish = load("res://art/items/resources/resources_04.png")
	resources.meat = load("res://art/items/resources/resources_05.png")
	resources.iron = load("res://art/items/resources/resources_06.png")
	resources.boots = load("res://art/items/resources/resources_07.png")
	resources.clothes = load("res://art/items/resources/resources_08.png")
	resources.leather = load("res://art/items/resources/resources_09.png")
	resources.garum = load("res://art/items/resources/resources_10.png")
	resources.olives = load("res://art/items/resources/resources_11.png")
	resources.figs = load("res://art/items/resources/resources_12.png")
	resources.oil = load("res://art/items/resources/resources_13.png")
	resources.linen = load("res://art/items/resources/resources_14.png")
	resources.milk = load("res://art/items/resources/resources_15.png")
	resources.grain = load("res://art/items/resources/resources_16.png")
	resources.pots = load("res://art/items/resources/resources_17.png")
	resources.tools = load("res://art/items/resources/resources_01.png")
	#resources.cows = load("res://art/items/resources/resources_01.png")
	#resources.horses = load("res://art/items/resources/resources_01.png")
	
	var battlePath = "res://art/items/icons/battle/"
	battle.aim = load(battlePath+"aim.png")
	battle.archer = load(battlePath+"archer.png")
	battle.attack = load(battlePath+"attack.png")
	battle.ballista = load(battlePath+"ballista.png")
	battle.caestrati = load(battlePath+"caestrati.png")
	battle.cavalry = load(battlePath+"cavalry.png")
	battle.ceasefire = load(battlePath+"ceasefire.png")
	battle.charge = load(battlePath+"charge.png")
	battle.elephant = load(battlePath+"elephant.png")
	battle.fireatwill = load(battlePath+"fireatwill.png")
	battle.intimidate = load(battlePath+"intimidate.png")
	battle.jinete = load(battlePath+"jinete.png")
	battle.rally = load(battlePath+"rally.png")
	battle.reload = load(battlePath+"reload.png")
	battle.scavenge = load(battlePath+"scavenge.png")
	battle.scutati = load(battlePath+"scutati.png")
	battle.shieldwall = load(battlePath+"shieldwall.png")
	battle.skirmisher = load(battlePath+"skirmisher.png")
	battle.slinger = load(battlePath+"slinger.png")
	battle.snipe = load(battlePath+"snipe.png")
	battle.swordsman = load(battlePath+"swordsman.png")
	battle.volley = load(battlePath+"volley.png")

func GetResource(resource):
	return resources[resource]
	
func GetBattle(resource):
	return battle[resource]
