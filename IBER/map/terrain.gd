extends Node2D
var preScript = preload("res://map/softnoise.gd")

var map_y = 10
var map_x = 10

func Init():
	PaintTile(0,0)
	PaintTile(1,0)
	PaintTile(0,1)
	PaintTile(1,1)
	
	var parent = get_parent()
	parent.OnActionPicked("tree")
	parent.CreateCurrentBuilding(Vector2(2.0,2.0))
	
	
	
func PaintTile(tx,ty):
	var noise = preScript.SoftNoise.new(42)
	var noise2 = preScript.SoftNoise.new(42+42)
	var scale = 0.04
	var scaleriver = 0.05
	for iy in range(map_y):
		for ix in range(map_x):
			var x = ix+tx*map_x
			var y = iy+ty*map_y
			var noiseraw = noise.openSimplex2D(x*scale,y*scale)
			var noiseriver = noise2.openSimplex2D(x*scaleriver,y*scaleriver)
			
			$Navigation2D/collision.set_cell(x,y,0)
			if(noiseraw>=0.2 && !(noiseriver>=-0.3 && noiseriver<0.3)):
				$Grass.set_cell(x,y,3)#3
				$GrassLevel1.set_cell(x,y,0)
				$GrassLevel2.set_cell(x,y,0)				
			elif(noiseraw>=0.2 && !(noiseriver>=-0.1 && noiseriver<0.1)):
				$Grass.set_cell(x,y,2)#2
				$GrassLevel1.set_cell(x,y,0)
			elif(noiseraw>=-0.2 && noiseraw<0.2 && !(noiseriver>=-0.2 && noiseriver<0.2)):
				$Grass.set_cell(x,y,2)#2
				$GrassLevel1.set_cell(x,y,0)
			else:
				$Grass.set_cell(x,y,1)
			if(noiseriver>=-0.1 && noiseriver<0.1):
				$Grass.set_cell(x,y,3)
				$Navigation2D/collision.set_cell(x,y,-1)
				$GrassLevel1.set_cell(x,y,1)
				$GrassLevel2.set_cell(x,y,-1)
			
	$GrassLevel1.update_bitmask_region(Vector2(tx*map_x,ty*map_y),Vector2(tx*map_x+map_x,ty*map_y+map_y))
	$GrassLevel2.update_bitmask_region(Vector2(tx*map_x,ty*map_y),Vector2(tx*map_x+map_x,ty*map_y+map_y))

