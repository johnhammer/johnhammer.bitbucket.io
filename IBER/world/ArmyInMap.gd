extends KinematicBody2D

var type
var info
var parent
var selected = false
var target = null
var velocity = Vector2()
var speed = 2.0

func Create(_info,_parent):
	info=_info
	parent = _parent
	match info.t:
		0: #enemy
			modulate = Color(1,0,0)
		1: #ally
			modulate = Color(0,1,0)
		2: #me
			modulate = Color(0,0,1)

func _input(event):
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		selected=true
		update()
		parent.SelectArmy(self)

func Move(pos):
	if info.t==2:
		target = pos

func _physics_process(delta):
	if target==null:return
	velocity = position.direction_to(target) * speed
	if position.distance_to(target) > 5:
		velocity = move_and_slide(velocity)

func _draw():
	if selected:
		draw_rect(Rect2(-18,-18,36,36),Color(1,1,1,0.5),false)
	
