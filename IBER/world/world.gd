extends Node2D

const ArmyResource = preload("res://world/ArmyInMap.tscn")

var terrain
var control
var info
var map_sizex
var map_sizey
var selectedArmy = null

func Init(_info,_control):
	info = _info
	control = _control
	terrain = get_node("terrain")
	var wallback = get_node("wallback")
	var wallfront = get_node("wallfront")
	var city = get_node("city")
	
	var noiseImage = Image.new()
	noiseImage.load("res://art/map/small.png")
	noiseImage.lock()
	map_sizex = noiseImage.get_width()
	map_sizey = noiseImage.get_height()
	for y in range(map_sizey):
		for x in range(map_sizex):
			var color = noiseImage.get_pixel(x, y).r
			if(color>0.0 && color<=0.20): terrain.set_cell(x,y,3)
			elif(color>0.20 && color<=0.40): terrain.set_cell(x,y,2)
			elif(color>0.40 && color<=0.60): terrain.set_cell(x,y,1)
			elif(color>0.60 && color<=0.80): terrain.set_cell(x,y,0)
			else: terrain.set_cell(x,y,4)
			
			var cityInPos = GetCityInPos(x,y)
			if cityInPos!=null:
				city.set_cell(x,y,cityInPos.c+8)
				wallback.set_cell(x,y,cityInPos.c)
				wallfront.set_cell(x,y,cityInPos.c)
	
	var army = ArmyResource.instance()
	var data = {
		"n":"Iberian army",
		"t":2
	}
	army.Create(data,self)
	
func GetCityInPos(x,y):
	for city in info:
		if abs(map_sizex*city.x - x)<0.5 && abs(map_sizey*city.y - y)<0.5:
			return city

func _input(event):
	if !visible: return
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var pos = terrain.world_to_map(get_global_mouse_position())
		var cityInPos = GetCityInPos(pos.x,pos.y)
		if(cityInPos!=null):
			selectedArmy = null
			var name = cityInPos.n
			var text = "culture "+String(cityInPos.c)
			control.CityInfo(name,text)

func SelectArmy(army):
	var name = army.n
	var text = "Iberian Army"
	control.CityInfo(name,text)
