Pivot Animator

Version 4.2.6

04 / 06 / 2016

Created By: Peter Bone
Company: Motus Software Ltd
email: support@pivotanimator.net
Pivot website : http://www.pivotanimation.net
Pivot Facebook page : http://www.facebook.com/pivotanimatorofficial


Pivot makes it quick and easy to make simple animations. The animation can be saved as an AVI video, animated gif, sequence of images or a native file so that they can be edited later. You can also build your own stick figures, save them and include them in your animation.


**********************************************************************

Changes in Pivot 4.2.1:

DONE  Copy and paste selected figures using Windows clipboard. Ctrl+C, Ctrl+V.
DONE  Semi-transparent figure builder window showing main canvas underneath. <,> keys to alter transparency.
DONE  Ini file can't save when non admin. Save in user account rather that all users.
DONE  Option for handle outline on or off. Ini file only option for handle colours.
DONE  keyboard shortcuts for Alt drag to lock scale / rotation. Ctrl and Shift keys.
DONE  Work ok canvas but no frames. Click new frame and work lost. Ask if they want to continue.
DONE  Floating point FPS value under slider.
DONE  Show timestamp info for each frame in mouse over text, calculated from frame rate.
DONE  SVG support in export as images, including background and sprite support.
DONE  Double click figure selects it and all figures joined to it recursively.
DONE  Frame preview without handles. Ctrl + Play Button.
DONE  Stop playing/previewing when canvas is clicked.
DONE  Multiple figure Alt drag. Lock individual scale or separation with another shortcut key (X,Z)
DONE  Figure/Background selector. Double click to add and close, single click to add and keep open.
DONE  Rotate segment using arrow keys when cursor is over a handle. End moves by 1 pixel.
DONE  Create folder when exporting as separate images.
DONE  handles no longer become smaller if next to a static segment.
DONE  Select figure by clicking anywhere on it without dragging.
DONE  Operations on figure branches in the builder using Shift. Duplicate, static, thickness, split.
DONE  Mirror duplicated segments in figure builder using Ctrl.
DONE  Store custom colour selector colours in ini file.
PARTIALLY DONE  Speed up save piv file and copy frames.
DONE  Sprites drawn faster if no flip, scale or rotation.
DONE  Crop transparent areas around sprites for speed.
DONE  New language files. Ukranian, Hungarian, Greek, Hebrew, Slovenian.

Bug fixes in Pivot 4.2.1:

FIXED  Onion skins of wrong figure shown after adding new figure type. Wrong ID.
FIXED  Bug. Drag handle and use <> keys to switch frame. If different figure with different # segments in other frame then access violation.
FIXED  Figures with low opacity piled on top of each other can show alpha blending error.


**********************************************************************

Bug fixes in Pivot 4.2.2:

FIXED  Paste button not disabled while playing.
FIXED  Dotted animation area border disappears when previewing frame. 
FIXED  Floating point overflow in TFigure.GetHandleRadius.
FIXED  Figure builder duplicate can cause all line angles set to 90.
FIXED  Play animation using keyboard shortcut. Pressing arrow keys while playing causes figures to appear outside animation.
FIXED  Alt+Z and Alt+X on joined figures. Moves them into unjoined positions until next moved. Skip joined figures.
FIXED  Undo not initialised after deleting a frame.

**********************************************************************

Bug fixes in Pivot 4.2.3

FIXED  Timestamps don't take repeated frames into account. Also, moved from timeline frame hint to frame label hint.
DONE  Keep same figure selected when changing frame if it still exists. Last figure selected only. Not multi-select.
FIXED  Select multiple frames with same background and change background and remove background, then click on frame results in error.
FIXED  Error when loading single colour sprite in CropTransparent. Will now load as opaque.
FIXED  Scaled up sprites not sub-pixel correct when rotated. PIV FILES FROM PREVIOUS VERSIONS MAY HAVE SMALL DIFFERENCES.

**********************************************************************

Bug fixes in Pivot 4.2.4

FIXED  Access violation. Open animation, edit it, then open animation and select save changes.
FIXED  Figure builder message 'Click to add' never shown while adding a segment.
FIXED  Range check error when clicking a frame with no figures.
FIXED  Rotate mouse wheel with cursor over red handle. Rotates segment but can no longer position figures normally. Can now use mouse wheel to rotate segments in 1 pixel increments instead of the arrow keys.
DONE  Icons added to menu items in figure builder.

**********************************************************************

Changes in Pivot 4.2.5

DONE  Keyboard shortcuts for all figure controls in the main window.
DONE  Keyboard shortcuts for frame controls, delete, copy and paste.
DONE  Keyboard shortcuts for all menu items.
DONE  Language files updated so that all shortcuts are fixed.

**********************************************************************
Changes in Pivot 4.2.6

DONE  Improved error handling for corrupted files. Only one error message will show. File loading will abort.
DONE  Support loading multiple sprites at once
DONE  Shortcuts for figure builder menu items
