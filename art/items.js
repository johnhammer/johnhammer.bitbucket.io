ARMOR TYPES
mail
cuirass
gambeson
plated
scaled
brigandine
ARMOR MATERIALS
leather
bronze
iron
steel
fiber
nano
ONE HAND WEAPONS
dagger
machete
stick
sword
axe
mace
TWO HAND WEPAONS
spear
katana
staff
broadsword
poleaxe  
warhammer 
WEAPON MATERIALS
wood
bronze
iron
steel
nano
arcane
SHIELDS
buckler
round shield
long shield
RANGED WEAPONS
bow
sling
shuriken
MAGIC WEAPONS
Gun
Sphere of Mitra
Dark sword


FOOD

MEDIC
medikit		Heals one hero
medikit+	Heals a lot one hero
medikit++	Heals completely one hero
omnikit     Heals all heroes
omnikit+    Heals a lot all heroes
omnikit++   Heals completely all heroes
magikit     Recovers mana for one hero
magikit+    Recovers mana for a lot one hero
magikit++   Recovers mana for completely one hero
nanokit			Heals knockout for one hero
omninanokit 	Heals knockout for all heroes
terakit			Recovers all health and mana for one hero
omniterakit 	Recovers all health and mana for all heroes

BOOKS



__________________________________
SKILLS

Flame	Gift of Sansaar to its descendants among the humans. The rare power of the flame can be wielded by those who can cast the flame flag, and it will allow them to bend it at will, use it for attack and defence, and improve their unarmed and weapon techniques.

Skills
Flame shield	Blocks all enemy attacks for 1 turn or till it breaks.
Flame ray		Magical individual attack. Can be charged to increase its power.

Flame final shield	Final move. Blocks all enemy attacks for 1 turn. 
Flame final ray		Final move. Powerful magical individual attack.

Aura
Flame aura		Improves your unarmed skills, defence and speed.
Flame coat		Improves your weapon effectivity.
Flame sword		Casts a powerful magical weapon.

Flame final punch	Final move. Powerful hit individual attack.
Flame final sword	Final move. Powerful cut individual attack. Breaks all defenses.

Bending
Flame spawn		Magical individual attack. Can be multicasted.
Flame wave		Magical multiple attack.

Flame final wave	Final move. Powerful magical multiple attack.


Arcane	m n

DI
Flame DI
Dark DI
Double DI
DI barrier
Final DI
Final seal

Silver wind



Flux

Comet
Meteor
Black hole
Quasar


Dark

Blood

Health for manna
Manna for health
Flying blood
AUTO (
Blood shield	Blocks incoming attack.
)

Crystal

Crystal air
Crystal ground
Crystal stab
Crystal sword
Crystal core

Mutationq

Increase mutation
Decrease mutation

AUTO (
Mutation: Fleshwall		Improves armor.
Mutation: Multilimb		Improves unarmed attacks.
Mutation: Golem			Improves greatly unarmed attacks and armor but reduces speed.
Mutation: Roots			Improves regeneration.
Mutation: Arrow bones	Extra ranged multiple attack before your move, stab damage.
Mutation: Corrupt		Attacks cause corruption effect.

)


Light

Light

Light sphere
Light barrier
Light storm
Light flash

Aura

Light aura
Light repulsion
Light blessing
Light coat
Light sword
Light phase

Mirror


Weapon

Cut
Stab
Hit
Block
Shot

Air cut
Bullet stab
Wave hit
Hypermoves


Melee

Punch
Kick
Block

Bullet punch
Arrow kick



a	ա
b	բ
c	ծ
d	դ
e	ե
f	փ
g	գ
h	հ
i	ի
j	ձ
k	կ
l	լ
m	մ
n	ն
o	ո
p	պ
q	ք
r	ր
s	ս
t	տ
u	ղ
v	վ
w	ւ
x	շ
y	յ
z	զ

	12.02		9.1		8.12		7.89		8		6.95		8.53		8.3		8.9		7.77		7		7.41
E	12.02	T	9.1	A	8.12	O	7.68	I	7.31	N	6.95	M	2.61	D	4.32	U	2.88	S	6.28	C	2.71	F	2.3
								Q	0.11	K	0.69				H	5.92	L	3.98	R	6.02	B	1.49	W	2.09	Y	2.11
								J	0.1																				G	2.03	V	1.11
																													X	0.17	Z	0.07
																																P	1.82

																																
																																
aaaa























																															