SHADERS.ExplosionFragment =`
varying vec2 vUv;
varying float noise;
/*uniform sampler2D tExplosion;*/
uniform float time;

float random( vec3 scale, float seed ){
  return fract( sin( dot( gl_FragCoord.xyz + seed, scale ) ) * 43758.5453 + seed ) ;
}

void main() {

  float tPos1 = 1.3 * noise + (.01 * random( vec3( 12.9898, 78.233, 151.7182 ), 0.0 ));
  vec4 color = 	vec4(255,0,0,1);
  gl_FragColor = vec4( color.rgb, tPos1/(time*4.0+0.5));
}
`