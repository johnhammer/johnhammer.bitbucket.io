/* CALCULATOR */

var Calculator = {
	Normalize(number1,number2){
		return Math.floor((number1 / number2) * 100)
	},	
	GetShipProps : function(s){
		var piezas 		= s.Weapon + s.Ammo + s.Shield + s.Engine + s.Fuel + s.Habitat + s.Radio + s.Computer;
		var nada		= s.Size - piezas;	
		
		return {		
			FirePower		: this.Normalize(s.Ammo*2 + ((s.Weapon>0) ? 1 : 0)										,16),
			FireSpeed		: this.Normalize(s.Weapon	                                                          	,8), 
			Precision		: this.Normalize(((s.Weapon>0) ? (32+(s.Radio+1) * (s.Habitat+1) - s.Ammo*4) : 0)	   	,96), 
			ElectricAttack	: this.Normalize((s.Habitat+1)*s.Computer*2 + s.Radio*s.Computer 	                    ,40),
			Armor			: this.Normalize(s.Shield*3 + s.Size	                                               	,32), 
			Tripulation	    : this.Normalize(s.Habitat*4	                                    					,32),
			Evasion			: this.Normalize(64 + (s.Engine*3+1) * (s.Radio+1) * (s.Habitat*2+1) - (s.Size)*8	    ,210),
			Camouflage		: this.Normalize(16-s.Engine-s.Size+1	                                                ,16),
			Speed			: this.Normalize(16+s.Engine*3-s.Size-piezas+1	                                       	,25), 
			Perception		: this.Normalize(s.Radio+1	                                                        	,9),
			Autonomy		: this.Normalize(8+s.Fuel-s.Engine+1	                                                ,17), 
			CargoCapacity	: this.Normalize(nada	                                                           		,8)
		}
	},	
	GetFleetProps : function(ships,player=0){
		var Combat		  = 0;
		var Electronics	  = 0;
		var Speed		  = 0;
		var Perception	  = 0;
		var Camouflage	  = 0;
		var Intelligence  = 0;
		var Autonomy	  = 0;
		var CargoCapacity = 0;	
		
		var types = Memory.get('ships',player);
		for(var i=0;i<types.length;i++){
			var amt = ships[i];			
			var parts = types[i].parts;
			Combat += (parts.Weapon+parts.Ammo+parts.Shield) * amt;
			Electronics += (parts.Computer+parts.Radio+parts.Habitat) * amt;
			Speed += (parts.Engine+parts.Nada) * amt;
			Perception += (parts.Radio) * amt;
			Camouflage += (parts.Computer+parts.Radio+parts.Habitat) * amt;
			Intelligence += (parts.Computer+parts.Radio+parts.Habitat) * amt;
			Perception += (parts.Fuel) * amt;
			CargoCapacity += (parts.Nada) * amt;		
		}	
		
		return {		
			Combat			: this.Normalize(Combat		 	,8*64),
			Electronics		: this.Normalize(Electronics	,8*64),
			Speed			: this.Normalize(Speed		 	,8*64),
			Perception		: this.Normalize(Perception	    ,8*64),
			Camouflage		: this.Normalize(Camouflage	    ,8*64),
			Intelligence	: this.Normalize(Intelligence 	,8*64),
			Autonomy		: this.Normalize(Autonomy	 	,8*64),
			CargoCapacity	: this.Normalize(CargoCapacity  ,8*64)
		}
	}
	
	
	
}