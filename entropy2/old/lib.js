//cookies

var TUTORIAL = {
	InitTutorialAiPlayer:function(){
		var player = {
			faction : 1,
			color : 'FFFF00',
			buildings : {
				bunker: [],factory: [],hangar: [],house: [],lab: [],mine: [],plant: [],recycler: [],	
			},
			resources : {
				Population: 0,debris: 0,energy: 0,energymax: 0,matter: 500,population: 0,populationmax: 0
			},	
			fleets : {
				Reichsadler: [10,4],
				Toranaga: [4,8]		
			},
			ships : [
				{
					name: "LENIN",
					parts:{
						Weapon:1,Ammo:2,Shield:0,Engine:2,Fuel:0,Habitat:2,Radio:0,Computer:0,Size:8,Nada:0
					}
				},
				{
					name: "MIR",
					parts:{
						Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:2,Radio:1,Computer:1,Size:4,Nada:0
					}
				}						
			]
		};
		Memory.addPlayer(player);
	},
	
	start:function(){
		Memory.set('faction',0,0);
		Memory.set('tutorial',1);
		Memory.set('color','#FF0000',0);	
		this.InitTutorialAiPlayer();	
		document.location.href = "./world.html";		
	},
	world:function(){
		var tutorial = Memory.get('tutorial');	
		if(tutorial == 1){
			alert(LONGTEXT('0World'));
			alert(LONGTEXT('1World'));
			alert(LONGTEXT('2World'));
		}
	},
	worldObjective:function(buildings){
		var tutorial = Memory.get('tutorial');	
		if(tutorial == 1 && buildings.hangar.length>0){
			document.location.href = "./map.html";			
		}		
	},
	map:function(){
		var tutorial = Memory.get('tutorial');	
		if(tutorial == 1){
			alert(LONGTEXT('0Map'));
			alert(LONGTEXT('1Map'));
			alert(LONGTEXT('2Map'));
			Memory.set('tutorial',2);
		}
	},
	mapObjective:function(cargo){
		var tutorial = Memory.get('tutorial');	
		if(tutorial == 2 && cargo>0){		
			alert(LONGTEXT('0Map2'));
			alert(LONGTEXT('1Map2'));
			alert(LONGTEXT('2Map2'));
		}
	},
	mapObjective2:function(){
		var tutorial = Memory.get('tutorial');	
		if(tutorial == 2){
			alert(LONGTEXT('TutorialEnd'));
			//document.location.href = "./index.html";		
		}
	},
}


function rnd(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

var LIB = {
	Capitalize : function(lower){
		return lower.charAt(0).toUpperCase() + lower.substr(1);
	}
}

var Cookies = {
	set : function(cvalue,cname="entropy", exdays = 1) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + JSON.stringify(cvalue) + ";" + expires + ";path=/";
	},
	get : function(name="entropy") {		
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return JSON.parse(parts.pop().split(";").shift());		
	},
	remove:function(name="entropy") {   
		document.cookie = name+'=; Max-Age=-99999999;';  
	}
}

var Memory = {
	start : function(){
		var cookie = {
			current : null,		
			language : 1,
			faction : 1,
			players : [
				{
					faction : null,
					ships : [],
					fleets : {}					
				}		
			]			
		}
		Cookies.set(cookie);
	},
	set : function(type,value,player){
		var cookie = Cookies.get();		
		
		if(!cookie)return null;			
		if(isNaN(player)){
			cookie[type] = value;
		}
		else{
			cookie.players[player][type] = value;			
		}						
		Cookies.set(cookie);		
	},	
	get : function(type,player){
		var cookie = Cookies.get();			
		
		if(isNaN(player)){
			if(!cookie)return TESTDEFAULTS[type];	
			return cookie[type];
		}
		else{
			if(!cookie)return TESTDEFAULTS.players[player][type];		
			return cookie.players[player][type];	
		}			
	},	
	add : function(type,value,player){
		var cookie = Cookies.get();		
		
		if(!cookie)return null;			
		if(isNaN(player)){
			cookie[type].push(value);
		}
		else{
			cookie.players[player][type].push(value);		
		}		
				
		Cookies.set(cookie);		
	},		
	addProp : function(type,prop,value,player){
		var cookie = Cookies.get();	
		
		if(!cookie)return null;			
		if(isNaN(player)){
			cookie[type][prop]=value;
		}
		else{
			cookie.players[player][type][prop]=value;	
		}
				
		Cookies.set(cookie);		
	},
	addPlayer : function(value){
		var cookie = Cookies.get();		
		
		if(!cookie)return null;			
		
		cookie.players.push(value);			
				
		Cookies.set(cookie);		
	},
}


var TEXT = function(key){
	var language = Memory.get('language');
	
	switch(language){
		case 0: return key;	//english
		case 1: return SPANISH[key];		
	}
}

var LONGTEXT = function(key){
	var language = Memory.get('language');
	
	switch(language){
		case 0: return ENGLISH_LONG[key];
		case 1: return SPANISH_LONG[key];		
	}
}

var TESTDEFAULTS ={
	language : 1,
	tutorial:2,
	current : {
		f1 : 'Toranaga',
		f2 : 'Reichsadler',
		player : 1
	},
	players:[
		{
			faction : 0,
			color : 'FF0000',			
			buildings : {
				bunker: [],factory: [],hangar: [],house: [],lab: [],mine: [],
				plant: [
					{x: 0.4394161187829807, y: 0.4070463103978846, z: 2.9618764263757074},
					{x: 2.019027808974297, y: 0.20949201475700066, z: 2.3538537841887126}
				],
				recycler: [
					{x: 1.057494784310587, y: 2.0042397095248297, z: 2.0392694174768202}
				],	
			},
			resources : {
				Population: 0,debris: 0,energy: -1,energymax: 8,matter: 500,population: 0,populationmax: 0
			},	
			fleets : {
				Reichsadler: [10,4],
				Toranaga: [4,8]		
			},
			ships : [
				{
					name: "TEST SHIP 1",
					parts:{
						Ammo: 0,Computer: 0,Engine: 0,Fuel: 0,Habitat: 0,Nada: 1,Radio: 0,Shield: 0,Size: 2,Weapon: 1
					}
				},
				{
					name: "TEST SHIP 2",
					parts:{
						Ammo: 2,Computer: 0,Engine: 0,Fuel: 0,Habitat: 0,Nada: 0,Radio: 0,Shield: 0,Size: 4,Weapon: 2,
					}
				}						
			]
		},
		{
			faction : 1,
			color : 'FFFF00',
			buildings : {
				bunker: [],factory: [],hangar: [],house: [],lab: [],mine: [],plant: [],recycler: [],	
			},
			resources : {
				Population: 0,debris: 0,energy: 0,energymax: 0,matter: 500,population: 0,populationmax: 0
			},	
			fleets : {
				Reichsadler: [10,4],
				Toranaga: [4,8]		
			},
			ships : [
				{
					name: "LENIN",
					parts:{
						Weapon:1,Ammo:2,Shield:0,Engine:2,Fuel:0,Habitat:2,Radio:0,Computer:0,Size:8,Nada:0
					}
				},
				{
					name: "MIR",
					parts:{
						Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:2,Radio:1,Computer:1,Size:4,Nada:0
					}
				}						
			]
		}
	]
	
	
}

function SetVar(vars,vals){
	document.documentElement.style.setProperty('--'+vars,vals);	
}

var Style = {
SetFactionColor : function(color){
	SetVar('faction_color',color);
},	
	
SetTheme : function(theme){
	switch(theme){
	case 0  : //El Gran Imperio		
		SetVar('font', 							"UnifrakturCook");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#000000");
		SetVar('color_main_text', 				"#8C0702");
		SetVar('color_secondary_back', 			"#99846B");
		SetVar('color_secondary_text', 			"#000000");
		SetVar('color_tertiary_back', 			"#D5B17D");
		SetVar('color_detail_back', 			"#8C0702");
		SetVar('color_detail_text', 			"#FFFFFF");
		break;
	case 1  : //Union Sovietica Universal	
		SetVar('font', 							"buran_ussrregular");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#D52D17");
		SetVar('color_main_text', 				"#400A30");
		SetVar('color_secondary_back', 			"#FEEFA0");
		SetVar('color_secondary_text', 			"#D52D17");
		SetVar('color_tertiary_back', 			"#D5B17D");
		SetVar('color_detail_back', 			"#000000");
		SetVar('color_detail_text', 			"#D52D17");	
		break;
	case 2  : //Confederacion de Jonia		
		SetVar('font', 							"diogenesregular");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#0B0701");
		SetVar('color_main_text', 				"#DD7001");
		SetVar('color_secondary_back', 			"#F3C450");
		SetVar('color_secondary_text', 			"#0E0701");
		SetVar('color_tertiary_back', 			"#722201");
		SetVar('color_detail_back', 			"#485275");
		SetVar('color_detail_text', 			"#E5AC82");	
		break;
	case 3  : //El Nuevo Califato			
		SetVar('font', 							"ds_arabicregular");
		SetVar('fontsize', 						"1.4em");
		SetVar('color_main_back', 				"#CAAD7F");
		SetVar('color_main_text', 				"#772300");
		SetVar('color_secondary_back', 			"#B58542");
		SetVar('color_secondary_text', 			"#472800");
		SetVar('color_tertiary_back', 			"#CD9A6F");
		SetVar('color_detail_back', 			"#A76617");
		SetVar('color_detail_text', 			"#797C49");	
		break;
	case 4  : //Tercera Republica Romana	
		SetVar('font', 							"diogenesregular");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#C4BFBE");
		SetVar('color_main_text', 				"#283942");
		SetVar('color_secondary_back', 			"#E2E1DD");
		SetVar('color_secondary_text', 			"#283942");
		SetVar('color_tertiary_back', 			"#D09D8F");
		SetVar('color_detail_back', 			"#A6B8CC");
		SetVar('color_detail_text', 			"#A88181");	
		break;
	case 5  : //Dinastia Sikh				
		SetVar('font', 							"Courier new");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#38367F");
		SetVar('color_main_text', 				"#F4960C");
		SetVar('color_secondary_back', 			"#253089");
		SetVar('color_secondary_text', 			"#F25D05");
		SetVar('color_tertiary_back', 			"#000000");
		SetVar('color_detail_back', 			"#FFFFFF");
		SetVar('color_detail_text', 			"#000000");		
		break;
	case 6  : //Imperio de la Serpiente		
		SetVar('font', 							"Special Elite");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#680C01");
		SetVar('color_main_text', 				"#558147");
		SetVar('color_secondary_back', 			"#40352F");
		SetVar('color_secondary_text', 			"#aa3C1F");
		SetVar('color_tertiary_back', 			"#A89B67");
		SetVar('color_detail_back', 			"#AF200C");
		SetVar('color_detail_text', 			"#4498AB");	
		break;
	case 7  : //Imperio Austropruso-Espanol	
		SetVar('font', 							"Times New Roman");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#202E39");
		SetVar('color_main_text', 				"#E2C88F");
		SetVar('color_secondary_back', 			"#AFA278");
		SetVar('color_secondary_text', 			"#2B0A09");
		SetVar('color_tertiary_back', 			"#80381E");
		SetVar('color_detail_back', 			"#B59642");
		SetVar('color_detail_text', 			"#4A362F");		
		break;
	case 8  : //Corporacion Universal		
		SetVar('font', 							"Courier new");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#FFFFFF");
		SetVar('color_main_text', 				"var(--faction_color)");
		SetVar('color_secondary_back', 			"#000000");
		SetVar('color_secondary_text', 			"#FFFFFF");
		SetVar('color_tertiary_back', 			"#000000");
		SetVar('color_detail_back', 			"#FFFFFF");
		SetVar('color_detail_text', 			"#000000");	
		break;
	case 9  : //Federacion Anarquista		
		SetVar('font', 							"buran_ussrregular");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#2A373F");
		SetVar('color_main_text', 				"#FFFFFF");
		SetVar('color_secondary_back', 			"#000000");
		SetVar('color_secondary_text', 			"#D52D17");
		SetVar('color_tertiary_back', 			"#000000");
		SetVar('color_detail_back', 			"#D52D17");
		SetVar('color_detail_text', 			"#555281");		
		break;
	case 10 : //EUU							
		SetVar('font', 							"Georgia");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#000000");
		SetVar('color_main_text', 				"#0A007E");
		SetVar('color_secondary_back', 			"#350DED");
		SetVar('color_secondary_text', 			"#000000");
		SetVar('color_tertiary_back', 			"#F232FA");
		SetVar('color_detail_back', 			"#430E89");
		SetVar('color_detail_text', 			"#2B77EF");		
		break;
	case 11 : //Imperio Francobritanico		
		SetVar('font', 							"Times New Roman");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#615D52");
		SetVar('color_main_text', 				"#F2ECE0");
		SetVar('color_secondary_back', 			"#EDEFDF");
		SetVar('color_secondary_text', 			"#00020F");
		SetVar('color_tertiary_back', 			"#96B7B0");
		SetVar('color_detail_back', 			"#C9391A");
		SetVar('color_detail_text', 			"#EDC367");		
		break;
	case 12 : //Alianza de Isandlwana		
		SetVar('font', 							"ds_arabicregular");
		SetVar('fontsize', 						"1.5em");
		SetVar('color_main_back', 				"#C8B482");
		SetVar('color_main_text', 				"#AC3D1C");
		SetVar('color_secondary_back', 			"#E8E4D8");
		SetVar('color_secondary_text', 			"#1D1A1C");
		SetVar('color_tertiary_back', 			"#CE9910");
		SetVar('color_detail_back', 			"#AC3D1C");
		SetVar('color_detail_text', 			"#3C571C");	
		break;
	case 13 : //Khanato celestial			
		SetVar('font', 							"tulisan_tangankuregular");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#0A2538");
		SetVar('color_main_text', 				"#554462");
		SetVar('color_secondary_back', 			"#84663D");
		SetVar('color_secondary_text', 			"#071926");
		SetVar('color_tertiary_back', 			"#B39364");
		SetVar('color_detail_back', 			"#28655E");
		SetVar('color_detail_text', 			"#A35140");	
		break;
	case 14 : //Condados del norte			
		SetVar('font', 							"tulisan_tangankuregular");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#3E652E");
		SetVar('color_main_text', 				"#992211");
		SetVar('color_secondary_back', 			"#B0B1B1");
		SetVar('color_secondary_text', 			"#332211");
		SetVar('color_tertiary_back', 			"#DDDDDD");
		SetVar('color_detail_back', 			"#C6C7D7");
		SetVar('color_detail_text', 			"#1A1C16");		
		break;
	case 15 : //Reino Eterno de Egipto		
		SetVar('font', 							"Special Elite");
		SetVar('fontsize', 						"1em");
		SetVar('color_main_back', 				"#DEB01E");
		SetVar('color_main_text', 				"#000000");
		SetVar('color_secondary_back', 			"#A37B3E");
		SetVar('color_secondary_text', 			"#000000");
		SetVar('color_tertiary_back', 			"#9B6F32");
		SetVar('color_detail_back', 			"#BF8A02");
		SetVar('color_detail_text', 			"#000000");		
		break;
}}
}
