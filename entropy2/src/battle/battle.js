var Battle_Gun = {
	Start(gun,ship){
		gun.Mesh = Draw_Battle.Gun(gun,ship);
		gun.Objective = null;
		gun.LastAttack = 0;
	},
	MultiobjectiveTick(gun,tick,ship){
		if(!gun.Objective || gun.Objective.Dead){
			gun.Objective = 
				Battle_Group.GetShipObjective(ship.Group,ship);
		}
		if(gun.Objective){			
			Battle_Gun.Tick(gun,tick,gun.Objective);			
		}
	},
	Tick(gun,tick,objective){		
		if(tick > gun.Cadence + gun.LastAttack){
			Battle_Gun.Attack(gun,tick,objective);
		}		
	},
	Attack(gun,tick,objective){
		BattleClock.AddEvent(gun.Recoil,()=>Battle_Ship.Damage(objective,gun.Attack));	
		gun.LastAttack = tick;
	}
}

var Battle_Ship = {
	Start(ship,group){		
		ship.Mesh = Draw_Battle.Ship(ship,group);
		
		ship.Targeted = 0;
		ship.ElecTargeted = 0;     
		ship.Dead = false;								
		ship.Objective = null;
		ship.ElecObjective = null;
		ship.LastElectronic = 0;
		ship.Group = group;
		
		for(var i=0; i<ship.Guns.length; i++){
			Battle_Gun.Start(ship.Guns[i],ship);
		}
	},
	Die(ship){
		console.log('ship dies '+ship.Group.Id+' '+ship.Id);
		Draw_Battle.Die(ship);
		ship.Dead = true;	
		Battle.CheckIfOver();
	},
	ElectronicDamage(ship,dmg){
		if(ship.Dead) return;
		console.log('electronic damage ship '+ship.Group.Id+' '+ship.Id+' damage '+dmg);
		ship.ElecHealth -= dmg;
		if(ship.ElecHealth<=0){
			Battle_Ship.Die(ship);
		}		
	},
	Damage(ship,dmg){
		if(ship.Dead) return;
		console.log('normal damage ship '+ship.Group.Id+' '+ship.Id+' damage '+dmg);
		ship.Health -= dmg;
		if(ship.Health<=0){
			Battle_Ship.Die(ship);
		}		
	},
	ElectronicAttack(ship,tick){	
		Battle_Ship.ElectronicDamage(ship.ElecObjective,ship.ElecAttack);
		ship.LastElectronic = tick;
	},
	Tick(ship,tick){
		if(ship.Dead){
			return;
		}
		if(ship.CanElectronic){
			if(!ship.ElecObjective || ship.ElecObjective.Dead){
				ship.ElecObjective = 
					Battle_Group.GetShipElecObjective(ship.Group,ship);
			}
			if(ship.ElecObjective){
				if(tick > ship.CadenceElectronic + ship.LastElectronic){
					Battle_Ship.ElectronicAttack(ship,tick);
				}
			}
		}
		if(ship.CanAttack){
			if(ship.Multiobjective){
				for(var i=0; i<ship.Guns.length; i++){
					Battle_Gun.MultiobjectiveTick(ship.Guns[i],tick,ship);
				}	
			}
			else{
				if(!ship.Objective || ship.Objective.Dead){
					ship.Objective = 
						Battle_Group.GetShipObjective(ship.Group,ship);
				}
				if(ship.Objective){
					for(var i=0; i<ship.Guns.length; i++){
						Battle_Gun.Tick(ship.Guns[i],tick,ship.Objective);
					}	
				}
			}				
		}		
	}
}

var Battle_Group = {	
	Start(group,enemyGroup){
		group.Objectives = enemyGroup.Ships;
		group.Mesh = Draw_Battle.Group(group);
		for(var i=0; i<group.Ships.length; i++){
			Battle_Ship.Start(group.Ships[i],group);
		}
	},
	GetShipObjective(group,ship){
		var validObjectives = group.Objectives
			.filter(x=>!x.Dead)
			.sort(function(a, b){return a.Targeted-b.Targeted});

		if(validObjectives.length){
			var objective = validObjectives[0];
			objective.Targeted++;
			return objective;
		}		
	},
	GetShipElecObjective(group,ship){
		var validObjectives = group.Objectives
			.filter(x=>!x.Dead)
			.sort(function(a, b){return a.Targeted-b.Targeted});
			
		if(validObjectives.length){
			var objective = validObjectives[0];
			objective.Targeted++;
			return objective;
		}		
	},
	Tick(group,tick){
		for(var i=0; i<group.Ships.length; i++){
			Battle_Ship.Tick(group.Ships[i],tick);
		}
	},
	IsDead(group){	
		for(var i=0; i<group.Ships.length; i++){
			if(!group.Ships[i].Dead){
				return false;
			}
		}
		return true;
	}
}

var Battle_Fleet = {
	Start(fleet,enemyFleet){
		for(var i in fleet.Groups){
			Battle_Group.Start(fleet.Groups[i],enemyFleet.Groups[0]);
		}
	},
	Tick(fleet,tick){
		for(var i in fleet.Groups){
			Battle_Group.Tick(fleet.Groups[i],tick);
		}		
	},
	IsDead(fleet){
		for(var i in fleet.Groups){
			if(!Battle_Group.IsDead(fleet.Groups[i])){
				return false;
			}
		}	
		return true;
	}
}

var Battle = {
	Active : true,
	F1 : null,
	F2 : null,	
	Rnd : null,
	Start(fleet1,fleet2){
		Battle.Rnd = Random.Create(); //seed
		Battle.F1 = fleet1;
		Battle.F2 = fleet2;		
		Draw_Battle.Init();
		Battle_Fleet.Start(Battle.F1,Battle.F2);
		Battle_Fleet.Start(Battle.F2,Battle.F1);
		BattleClock.Init();		
		Draw.Render();
	},
	Tick(tick){
		if(tick > 100){ //TIMEOUT
			Battle.Active = false;
			console.log('battle completed - timeout');
		}		
		Battle_Fleet.Tick(Battle.F1,tick);
		Battle_Fleet.Tick(Battle.F2,tick);
	},
	CheckIfOver(){
		var f1dead = Battle_Fleet.IsDead(Battle.F1);
		var f2dead = Battle_Fleet.IsDead(Battle.F2);
		if(f1dead && f2dead){
			console.log('battle completed - draw');
			Battle.Active = false;
		}
		else if(f1dead){
			console.log('battle completed - victory to '+Battle.F2.Name);
			Battle.Active = false;
		}
		else if(f2dead){
			console.log('battle completed - victory to '+Battle.F1.Name);
			Battle.Active = false;
		}		
	}
}

var BattleClock = {
	Events : [],	
	t : 0,	
	Init : function(){
		BattleClock._Tick();
		BattleClock.t = 0;
		BattleClock.Events = [];
	},	
	AddEvent : function(time,callback){
		BattleClock.Events.push({
			t:BattleClock.t+time,
			c:callback,
			d:false
		});
	},
	DisableEvent(e){
		e.d = true;
	},
	_Tick : function(){
		if(Battle.Active){
			Battle.Tick(BattleClock.t);
			setTimeout(function() { 
				BattleClock.t++;
				BattleClock._Check();
				BattleClock._Tick();      
			}, 1000); 
		}          
	},
	_Check : function(){
		var updated = false;
		for(var i=0;i<BattleClock.Events.length;i++){
			if(BattleClock.Events[i].t<=BattleClock.t){
				BattleClock._ProcessEvent(BattleClock.Events[i]);
				updated=true;
			}
		}
		if(updated){ //cleanup
			BattleClock.Events = BattleClock.Events.filter(x=>x.t>BattleClock.t);
		}
	},
	_ProcessEvent : function(e){
		if(!e.d) e.c();
	},
}


