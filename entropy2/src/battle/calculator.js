var Calculator_Battle = {
	/** 
	 * @param {Ship} type 
	*/
	GetShipTicksToShot : function(type){
		return Math.round(ShipCalculator.GetFireSpeed(type.parts)/6);
	},
	/** 
	 * @param {Ship} type 
	 * @param {{Mesh}[]} targets 
	 * @param {Random} rnd 
	*/
	GetShipObjective : function(type,targets,rnd){		
		return Random.Choice(rnd,targets);
	},
	/** 
	 * @param {Ship} type 
	*/
	GetAttack : function(type){
		return ShipCalculator.GetFirePower(type.parts);
	},
	/** 
	 * @param {Ship} type 
	*/
	GetHealth : function(type){
		var props = ShipCalculator.GetArmor(type.parts)
		return props.Armor*30;
	},
}