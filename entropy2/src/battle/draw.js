var Draw_Battle = {	
	Init : function(){
		Draw._Init(battle_canvas,null,Draw_Battle.Animation); //Draw_Ship.RenderShip

		var light = new THREE.AmbientLight( 0x555555 );
        scene.add( light );

        var lights = [];
		lights[ 0 ] = new THREE.PointLight( 0xffffff, 1, 0 );
		lights[ 1 ] = new THREE.PointLight( 0xffffff, 1, 0 );
		lights[ 2 ] = new THREE.PointLight( 0xffffff, 1, 0 );

		lights[ 0 ].position.set( 0, 200, 0 );
		lights[ 1 ].position.set( 100, 200, 100 );
		lights[ 2 ].position.set( - 100, - 200, - 100 );

		scene.add( lights[ 0 ] );
		scene.add( lights[ 1 ] );
		scene.add( lights[ 2 ] );
		
		controls.target = new THREE.Vector3(0,100,0)
	},
	Group : function(group){
		var mesh = new THREE.Mesh();
		scene.add(mesh);
		return mesh;
	},
	Ship : function(ship,group){
		var mesh = Ship_MeshFactory.CreateShipMesh(ship.MeshProps.Parts).mesh;
		mesh.position.set(
			Random.FloatArea(Battle.Rnd,group.MeshProps.Area),
			group.MeshProps.Y,
			Random.FloatArea(Battle.Rnd,group.MeshProps.Area)
		);
		if(!ship.MeshProps.IsMine){			
			mesh.rotation.z = -Math.PI;
		}			
		group.Mesh.add(mesh);
		return mesh;
	},
	Gun : function(gun,ship){
		var mesh = ship.Mesh.ShipObj.Cannons.filter(x=>!x.Used);
		mesh.Used = true;
		return mesh;
	},
	Die : function(ship){
		Draw_Battle.Explode(ship.Mesh.position,.5,2);
		ship.Group.Mesh.remove(ship.Mesh);
	},
	Explosion : function(size,quality){
		var mesh = new THREE.Mesh(
			new THREE.IcosahedronGeometry( .1, quality ),
			MaterialsFactory.Explosion()
		);
		mesh.scale.set(size,size,size);		

		ANIMATED.Explosions.push(mesh);
		return mesh;
	},
	Explode : function(position,size,quality){
		var expl = Draw_Battle.Explosion(size,quality);
		expl.position.set(position.x, position.y, position.z);
		scene.add(expl);
	},
	
	ShotBullet : function(pos,vector){	
        var x = pos.x //+ Math.random()*2-1;
        var y = pos.y //+ Math.random()*2-1;
        var z = pos.z //+ Math.random()*2-1;
        
        var material = MaterialsFactory.ShipMaterial();
        
        var object = new THREE.Mesh(lib.sph(.1).m.geometry,material);
        object.position.set(x,y,z);
        
        scene.add(object);
        
        object.ttl = 100 + Math.random()*50-25;
        object.vector = vector;
        
        ANIMATED.Bullets.push(object);
    },  
	
	//animation
	Animation(){
		//explosions
		for(var i = 0; i < ANIMATED.Explosions.length; i++){			
			if(ANIMATED.Explosions[i]){
				ANIMATED.Explosions[i].material.uniforms['time'].value += .015;
				if(ANIMATED.Explosions[i].material.uniforms['time'].value>.4){
					scene.remove(ANIMATED.Explosions[i]);
					ANIMATED.Explosions[i] = null;
				}
			}			
		}
		//bullets
        for(var i = 0; i < ANIMATED.Bullets.length; i++){
            Draw_Battle.MoveBullet(ANIMATED.Bullets[i],i);
        }
	},  
    MoveBullet : function(bullet,i){
        if(!ANIMATED.Bullets[i]) return;
    
        if(bullet.ttl<0){
            
			Draw_Battle.Explode(bullet.position,0.1,1);
			
            scene.remove(bullet);	
            ANIMATED.Bullets[i] = null		
        }
        bullet.ttl--;

        bullet.position.x += bullet.vector.x * 0.5;
        bullet.position.y += bullet.vector.y * 0.5;
        bullet.position.z += bullet.vector.z * 0.5;

    },
	
	/*
	
	Shot : function(ship){
		Draw_Ship.ShotCannons(ship);
	},
	
	Kill : function(shipMesh){
		Draw_Ship.Explode(shipMesh.position,1,4);
		scene.remove(shipMesh);
	}
	*/
}