var Edit_Battle = {
	/** 
	 * @param {Ship} s1 
	 * @param {Ship} s2
	*/
    Hit : function(s1,s2){
		if(!s2) return;
		var dmg = Calculator_Battle.GetAttack(s1.Type);
		s2.Health -= dmg;
		if(s2.Health <= 0){
			Edit_Battle.Kill(s2);
		}		
	},
	/** 
	 * @param {Ship} ship
	*/
	Kill : function(ship){
		ship.Fleet.Ships = ship.Fleet.Ships.filter(x=>x!=ship);

		for (var i = 0; i < ship.Pointers.length; i++) {
			Battle.SetShipObjective(ship.Pointers[i]);
		}				
		Draw_Battle.Kill(ship.Mesh);
		ship.Objective = null;
		ship.Destroyed = true;
	}
}