var Battle = {
	F1 : null,
	F2 : null,
	Rnd : null,
	/** 
	 * @param {Fleet} f1obj
	 * @param {Fleet} f2obj
	*/
    Start : function(f1obj,f2obj){
        //{Id:fleet.id,Player:fleet.player}
		Battle.Rnd = Random.Create(42);
		Draw_Battle.Init();
		
        Battle.F1 = Battle.CreateFleet(f1obj);
		Battle.F2 = Battle.CreateFleet(f2obj);
		
		Draw_Battle.Fleet(Battle.F1,false);
		Draw_Battle.Fleet(Battle.F2,true);
		
		Clock.AddEvent(1,()=> Battle.Aim());		
    },
	Aim : function(){
		Battle.SetFleetObjectives(Battle.F1,Battle.F2);
		Battle.SetFleetObjectives(Battle.F2,Battle.F1);
	},
	SetFleetObjectives : function(fleet,target){
		for (var i = 0; i < fleet.Ships.length; i++) { 
			fleet.Ships[i].Enemy = target;
			Battle.SetShipObjective(fleet.Ships[i]);
		}
	},
	SetShipObjective : function(ship){
		var targets = ship.Enemy.Ships;
		ship.Objective = Calculator_Battle.GetShipObjective(ship,targets,Battle.Rnd);
		if(ship.Objective){
			ship.Objective.Pointers.push(ship);
			Draw_Ship.AimCannons(ship.Mesh,ship.Objective.Mesh);
		}		
	},
	Shot : function(ship){
		if(ship.Objective && !ship.Destroyed){
			Draw_Battle.Shot(ship);
			Clock.AddEvent(ship.TicksToShot,()=> Battle.Shot(ship));
		}
	},
	ShotHit : function(ship){
		Edit_Battle.Hit(ship,ship.Objective);
		if(ship.Objective && !ship.Destroyed){			
			Clock.AddEvent(ship.TicksToShot,()=> Battle.ShotHit(ship));
		}
	},
	CreateShip : function(type,area,f){
		var ship = {
			X : Random.FloatArea(Battle.Rnd,area),
			Y : 0,
			Z : Random.FloatArea(Battle.Rnd,area),
			Type:type,
			Enemy:null,
			Fleet:f,
			Health : Calculator_Battle.GetHealth(type),
			TicksToShot : Calculator_Battle.GetShipTicksToShot(type),
			Pointers : [],
		};

		return ship;
	},
	CreateFleet : function(f){
		var types = PLAYERS[f.Player].Ships;
        var ships = PLAYERS[f.Player].Fleets[f.Id].ships;

		var area = 50;
		var fleet = {
			Ships : []
		}		
        for (var i = 0; i < ships.length; i++) { 
            var type = types[i];
            for (var j = 0; j < ships[i]; j++) { 
				const ship = Battle.CreateShip(type,area,fleet);
                fleet.Ships.push(ship);		
				Clock.AddEvent(ship.TicksToShot,()=> Battle.Shot(ship));				
				Clock.AddEvent(ship.TicksToShot+2,()=> Battle.ShotHit(ship));				
            }
        }	
		return fleet;
	},
}