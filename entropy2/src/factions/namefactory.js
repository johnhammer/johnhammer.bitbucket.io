/*

	Name maker

*/

var PlaceNameFactory = {
	MakeName : function(factionId,rnd){
		var name = [];
		var faction = PlaceNameFactory.Names[factionId];
		switch(factionId){
			case 0: 
				break;			
			case 1: 
				var fleet = Random.Choice(rnd,FACTIONS[factionId].fleets);
				var place = LIB.LastWord(fleet);
				if(LIB.LastLetter(place)=="g"){
					place+="rad";
				}
				else{
					place+="grad";
				}
				name.push(place);
				break;
			case 2: 
				//Confederacion de Jonia		N	Name of Demonym			Alexander of Macedonnia
				break;	
			case 3: 
				//El Nuevo Califato			   	N	Name Part Pathronym		Mohammed ibn Abdullah
				break;
			case 4:
				//tercera republica romana		N Marcus Aurelius Decimus Meridius    name (name) number (surname)	
				break;
			case 5:
				//dinast
				break;
			case 6:
				break;
			case 7: 
				//Imperio Austro-Espanol     	N	Name of Demonym			Alexander of Macedonnia
				var word = Random.Choice(rnd,FactionNameFactory.SpanishNames.concat(FactionNameFactory.GermanNames));				
				name.push("San" + " " + LIB.Capitalize(word));
				break;
			case 8:
				//Corporacion Universal			N
				name.push('C-'+Random.Range(rnd,10000,99999));
				break;
			case 9:
				//Federacion Anarquista			N Comrade spa/slav/germ/eng son of spa/slav/germ/eng
				break;
			case 10:
				//EUU			               	N Alonso Jose Johns Jr.
				break;	
			case 11: 
				//Imperio Francobritanico		N John (J.) Johns
				break;	
			case 12:
				break;
			case 13:
				//Khanato celestial		N N
				break;
			case 14:
				//Vikingr		N N			
				break;
			case 15:
				//mesopotamia		N N
				break;				
		}
				
		return name.join(' ');
	},	
	Names : [		
		{
			Places:[]		
		}
	]
}


var FactionNameFactory = {
	MakeName : function(factionId,rnd){
		var name = [];
		var faction = FactionNameFactory.Names[factionId];
		switch(factionId){
			case 0: 
				//El Gran Imperio              	N	Name Surname			Adolf Hitler
				name.push(Random.Choice(rnd,faction.Names.concat(FactionNameFactory.GermanNames)));
				name.push(Random.Choice(rnd,faction.Surnames));
				break;
			case 1: 
				//Union Sovietica Universal		N	Name Pathronym			Vladimir Vladimirovich
				var firstName = Random.Choice(rnd,faction.Syllables);
				firstName += Random.Choice(rnd,faction.Terminations);
				
				var secondName = Random.Choice(rnd,faction.Syllables);
				secondName += Random.Choice(rnd,faction.Terminations);
				secondName += Random.Choice(rnd,faction.Suffixs);
				
				name.push(firstName);
				name.push(secondName);
				break;	
			case 2: 
				//Confederacion de Jonia		N	Name of Demonym			Alexander of Macedonnia
				var firstName = Random.Choice(rnd,faction.Names)+' '+TEXT('of');				
				var secondName = Random.Choice(rnd,faction.Places);
				
				name.push(firstName);
				name.push(secondName);
				break;	
			case 3: 
				//El Nuevo Califato			   	N	Name Part Pathronym		Mohammed ibn Abdullah
				//								N	Name al-Adjective		Mohammed al-Farsi
				var firstName = Random.Choice(rnd,faction.Names);
				var secondName;				
				if(Random.Chance(rnd,4)){
					firstName += " al";
					secondName = Random.Choice(rnd,faction.Adjectives);
				}
				else{
					firstName += " "+Random.Choice(rnd,faction.Part);
					secondName = Random.Choice(rnd,faction.Names);
				}	
				
				name.push(firstName);
				name.push(secondName);
				break;
			case 4:
				//tercera republica romana		N Marcus Aurelius Decimus Meridius    name (name) number (surname)
				name.push(Random.Choice(rnd,faction.Names));
				name.push(Random.Choice(rnd,faction.Order));
				name.push(Random.Choice(rnd,faction.Surnames));				
				break;
			case 5:
				var firstName = Random.Choice(rnd,faction.Syllables);				
				if(Random.Chance(rnd,2)){
					firstName += Random.Choice(rnd,faction.Syllables);
				}
				if(Random.Chance(rnd,8)){
					firstName += Random.Choice(rnd,faction.Syllables);
				}				
				name.push(firstName);	
				name.push('singh');	
				break;
			case 6:
				name = FactionNameFactory.MakeTribalName(rnd);
				break;
			case 7: 
				//Imperio Austro-Espanol     	N	Name of Demonym			Alexander of Macedonnia
				name.push(Random.Choice(rnd,FactionNameFactory.SpanishNames.concat(FactionNameFactory.GermanNames)));				
				if(Random.Chance(rnd,4)){
					var secondName = Random.Choice(rnd,FactionNameFactory.SpanishNames.concat(FactionNameFactory.GermanNames));
					if(secondName!=name[0]) name.push(secondName);
				}
				name[name.length-1]+=' '+ TEXT('of');
				name.push(Random.Choice(rnd,faction.Places));
				break;
			case 8:
				//Corporacion Universal			N
				name.push(TEXT('citizen')+' '+Random.Range(rnd,10000,99999));
				break;
			case 9:
				//Federacion Anarquista			N Comrade spa/slav/germ/eng son of spa/slav/germ/eng
				name.push(TEXT('comrade'));
				var firstName = Random.Choice(rnd,FactionNameFactory.EnglishNames
					.concat(FactionNameFactory.SpanishNames)
					.concat(FactionNameFactory.GermanNames)
					.concat(FactionNameFactory.FrenchNames)
				);				
				if(Random.Chance(rnd,5)){
					firstName = Random.Choice(rnd,FactionNameFactory.Names[1].Syllables);
					firstName += Random.Choice(rnd,FactionNameFactory.Names[1].Terminations);
				}
				name.push(firstName);
				break;
			case 10:
				//EUU			               	N Alonso Jose Johns Jr.
				name.push(Random.Choice(rnd,FactionNameFactory.EnglishNames.concat(FactionNameFactory.SpanishNames)));
				if(Random.Chance(rnd,3)){
					var secondName = Random.Choice(rnd,FactionNameFactory.EnglishNames.concat(FactionNameFactory.SpanishNames));
					if(secondName!=name[0])
						name.push(secondName);
				}
				name.push(Random.Choice(rnd,FactionNameFactory.EnglishNames)+Random.Choice(rnd,['s','son']));
				if(Random.Chance(rnd,8)){
					name.push(Random.Choice(rnd,faction.Suffix));
				}
				break;	
			case 11: 
				//Imperio Francobritanico		N John (J.) Johns
				name.push(Random.Choice(rnd,FactionNameFactory.EnglishNames.concat(FactionNameFactory.FrenchNames)));
				if(Random.Chance(rnd,3)){
					name.push(LIB.FirstLetter(Random.Choice(rnd,FactionNameFactory.EnglishNames.concat(FactionNameFactory.FrenchNames)))+'.');
				}
				name.push(Random.Choice(rnd,FactionNameFactory.EnglishNames)+Random.Choice(rnd,['s','son']));
				break;	
			case 12:
				name = FactionNameFactory.MakeTribalName(rnd);
				break;
			case 13:
				//Khanato celestial		N N
				var firstName = Random.Choice(rnd,faction.Syllables);
				firstName += Random.Choice(rnd,faction.Terminations);				
				var secondName = Random.Choice(rnd,faction.Syllables);
				secondName += Random.Choice(rnd,faction.Terminations);
				name.push(firstName);
				name.push(secondName);
				break;
			case 14:
				//Vikingr		N N
				var firstName = Random.Choice(rnd,faction.Syllables);
				firstName += Random.Choice(rnd,faction.Terminations);				
				var secondName = Random.Choice(rnd,faction.Syllables);
				secondName += Random.Choice(rnd,faction.Terminations);
				name.push(firstName);
				name.push(secondName+'son');				
				break;
			case 15:
				//mesopotamia		N N
				name.push(Random.Choice(rnd,faction.Names));	
				name[name.length-1]+=' '+ TEXT('of');
				name.push(Random.Choice(rnd,faction.Places));
				break;
		}
		return name.map(x=>LIB.Capitalize(x)).join(' ');
	},	
	Names : [		
		{
			Names:['hideki','kenji','saito','koichi','takeji','shigetaro','akira'],
			Surnames:['eigen','euler','fahrenheit','heisenberg','kepler','kirchhoff','planck','ribbentrop','minamoto','genji','taira','heishi','tachibana','fujiwara','rommel','koch','schmidt','braun'],		
		},				
		{
			Syllables:['bogd','slobod','bor','vladi','dmitr','ulg','yevg','aleks','serg'],
			Terminations:['an','ir','yan','is','eni','andr','ey',''],
			Suffixs:['evich','ev'],		
		},	
		{
			Names:['thales','anaximander','anaximenes','heraclitus','anaxagoras','archelaus','democritus','alexander','aristarchus','autolycus','euclid','archimedes‎','heron','hippocrates','eratosthenes','empedocles'],
			Places:['smyrna','miletus','myus','priene','ephesus','colophon','lebedus','teos','klazomenai','phocaea','chios','samos','erythrae','magnesia','tralles','halikarnassos','agora','acropolis','anaktoron','megaron','gymnasium','metropolis','ekklesia','theatron','macedonia','peloponnesus','attica','aeolis','caria','thessalia','ionia','epirus'],		
		},	
		{
			Names:['ali','adil','hasan','hussain','muhammad','rashid','abdullah','ahmed','harun'],
			Part:['bin','ibn'],
			Adjectives:['maghrebi','farsi','jazayiri','misri','andilusi','sari','suwri','baghdadi']
		}, 	
		{
			Names:['tiberius','gaius','iulius','claudius','lucius','marcus','publius','titus','servius','spurius','hostus','tullus','hannibal','hamilcar','magus','hasdrubal'],
			Surnames:['agricola','agrippa','aquila','barbatus','caninus','corvus','crassus','lepidus','lupus','marcellus','metellus','regulus','severus','triarius','barca','absalon'],
			Order:['quintus','sextus','octavius','nonus','decimus','maximus','primus','secundus','tertius','quartus']			
		},		
		{
			Syllables:['li','xiu','ying','wei','fang','min','jing','qiang','lei','yang','yong','jun','chao','rong'],
		},
		{},
		{
			Places:['austria','bohemia','prusia','bavaria','holanda','belgica','flandes','renania','aragon','castilla','galicia','leon','granada','cordobas','valencias','brabante','lombardia','flandes','saboya','napoles','arauco','algarve','toledo','navarra','saavedra','fuenclara','colorada','caracena','mortora','montserrat','zamora','seralvo'],		
		},
		{},
		{},
		{
			Suffix:['jr.','sr.']
		},
		{},		
		{},
		{
			Syllables:['batu','ber','bo','tsa','gen','chin','tur','emir','en','er','gan','go','il','naran','otgon','temu'],
			Terminations:['han','ker','k','khan','kmen','gadai','ghis','men','der','baatar','zorig','bayar','gur','jin'],			
		},
		{
			Syllables:['rag','dag','ei','gun','hak','hal','har','half','hal','hel','hjal','ing'],
			Terminations:['thor','finn','nar','rik','vind','mund','var','lendr','bjorn','kon','ald','dan','mar'],			
		},
		//Reino Eterno de Egipto	   	N
		{
			Names:['sargon','gudea','gilgamesh','hammurabi','enkidu','darius','xerxes','artaxerxes','narmer','djoser','neferkare','amenhotep','thutmose','ramesses','hakor','cleopatra'],
			Places:['babylon','ur','akkad','uruk','assur','nimrud','nineveh','eridu','kish','lagash','samarra','shuruppak','sumer','assur','tyre','susa','yamu','mennefer','abydos','khem','raqote','khito','ptkheha','perwadjet','abu','swenett','nubt','behdet','nekheb','nekhen','tasenet','waset'],
		},
	],
	SpanishNames:['alonso','andres','antonio','bartolome','cristobal','diego','francisco','gonzalo','hernando','juan','luis','pedro','rodrigo','sancho','santiago','alvaro'],
	GermanNames:['hans','otto','ernst','rudolf','erwin','klaus','alfred','werner','wolfgang','jurgen','dieter','manfred','gunther','horst','karl','rainer'],
	EnglishNames:['john','william','edward','richard','robert','jack','roger','walter','adam','george','hugh','dean','simon','ralph','peter','stephen'],
	FrenchNames:['jean','francois','he++nri','charles','jacques','louis','pierre','olivier','joseph','antoine','nicolas','guillaume','jules','bernard','frederic','jacob'],
	
	TribalAdjective:['strong','fierce','wild','brave','red','white','black','blue','dark','bright','mighty','glorious','sitting','standing','burning','watching'],
	TribalName:['lion','wolf','bull','eagle','spirit','heart','soul','messenger','cloud','fog','rain','storm','warrior','fire','sea','mountain'],
	
	MakeTribalName(rnd){		
		var adj = TEXT(Random.Choice(rnd,FactionNameFactory.TribalAdjective));
		var name = TEXT(Random.Choice(rnd,FactionNameFactory.TribalName));

		if(LANGUAGE_DETAILS().AdjectiveName){
			return [adj,name];
		}	
		else{
			return [name,adj];
		}
	}
}

