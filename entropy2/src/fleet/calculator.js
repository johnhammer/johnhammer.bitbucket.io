var FleetCalculator = {	

	GetRandomName(faction){
		return LIB.Capitalize(Random.Choice(GLOBAL.rnd,FACTIONS[faction].fleets));
	},

	Phisics : {
		GetDistance(origin,objective){
			var a = new THREE.Vector3(origin.x,origin.y,origin.z);
			var b = new THREE.Vector3(objective.x,objective.y,objective.z);
			return a.distanceTo(b);
		},
		GetTime(distance,speed){
			return Math.round(distance/speed);
		},
		GetConsumption(distance,speed){
			return Math.round(distance*speed);
		},
	},
	
	GetTimeToBuild(fleet){
		return 10;
	},
	GetCost(fleet){
		return 10;
	},
	
	GetFleetRouteDetails(fleet,objective,origin=null){
		var speed = FleetCalculator.GetFleetSpeed(fleet.ships,PLAYERS[fleet.Player].Ships);	
		var origin = origin?origin:GLOBAL.Map[fleet.location];

		var distance = FleetCalculator.Phisics.GetDistance(origin,objective);			
		var time = FleetCalculator.Phisics.GetTime(distance,speed);
		var fuel = FleetCalculator.Phisics.GetConsumption(distance,speed);
			
		return {
			Time:time,
			Fuel:fuel
		}
	},	
	GetFleetCombat : function(ships,types){
		var result = 0;
		for(var i=0;i<types.length;i++){	
			var parts = types[i].parts;
			result += (parts.Weapon+parts.Ammo+parts.Shield) * ships[i];
		}	
		return result;
	},
	GetFleetPassengers : function(ships,types){
		var result = 0;
		for(var i=0;i<types.length;i++){	
			var parts = types[i].parts;
			result += (parts.Habitat) * ships[i];
		}	
		return result;
	},
	GetFleetSpeed : function(ships,types){
		var result = 0;
		for(var i=0;i<types.length;i++){	
			var parts = types[i].parts;
			result += (parts.Engine+parts.Nada) * ships[i];
		}	
		return result+1;
	},
	GetFleetFuel : function(ships,types){
		var result = 0;
		for(var i=0;i<types.length;i++){	
			var parts = types[i].parts;
			result += (parts.Fuel) * ships[i];
		}	
		return result;
	},	
	GetFleetCargo : function(ships,types){
		var result = 0;
		for(var i=0;i<types.length;i++){	
			var parts = types[i].parts;
			result += (parts.Nada) * ships[i];
		}	
		return result;
	},
	GetFleetOrders : function(fleet,objective){
		/*
		MapAction : {           //resource  myworld myfleet enworld enfleet neutworld
        Move        :1,     //defend    join    -       attack  attack  colonize
        Route       :2      //collect   bring   refuel  trade   refuel  -
    }
		*/
		var shiptypes = PLAYERS[fleet.Player].Ships;
		var fleetships = fleet.ships;
		
		var canDefend = FleetCalculator.GetFleetCombat(fleetships,shiptypes)>0;
		var canCarry = FleetCalculator.GetFleetCargo(fleetships,shiptypes)>0;
		var canColonize = FleetCalculator.GetFleetPassengers(fleetships,shiptypes)>0;
		var canAttack = canDefend;//projectiles..
		var canTrade = canCarry&&canColonize;//ships with human and cargo?
		var canRefuel = FleetCalculator.GetFleetFuel(fleetships,shiptypes);

		var move = false;
		var route = false;
		switch(objective.type){
			case ENUM.MapObject.Asteroids:
			case ENUM.MapObject.Gas:
			case ENUM.MapObject.Dust:
				move = canDefend;
				route = canCarry; //collect
				break;
			case ENUM.MapObject.Planet:
				switch(objective.player){
					case -1: //neutral
						move = canColonize;
						break;
					case PLAYER_ID:
						move = true; //join
						route = canCarry;
						break;
					default: //enemy
						move = canAttack;
						route = canTrade;
						break;
				}
				break;
			case ENUM.MapObject.Fleet:
				switch(objective.player){
					case PLAYER_ID:
						route = canRefuel;
						break;
					default: //enemy
						move = canAttack;
						route = canRefuel;
						break;
				}
				break;
		}
		return {
			Move:move,
			Route:route
		};
	},
}