var Fleet_Edit = {
	SaveFleet : function(fleet,player){
		fleet.Building = true;
		Clock.AddEvent(FleetCalculator.GetTimeToBuild(fleet),()=> Fleet_Edit.CompleteFleet(fleet));
		if(fleet.Id){
			PLAYERS[player].Fleets[fleet.Id] = fleet;
		}
		else{
			PLAYERS[player].Fleets.push(fleet);
		}		
	},
	CompleteFleet : function(fleet){
		var data = {
			name : fleet.name
		};
		Frontend.Snackbar(Templating.Fleet.CompleteFleet(data));
		fleet.Building = false;
	}
}