var Fleet_Menu = {
	AddShipToFleet : function(ship){
		CURRENT.Resources -= ShipCalculator.GetCost(PLAYERS[PLAYER_ID].Ships[ship].parts);
		CURRENT.EditingFleet.ships[ship]++;
		Level.Fleets();
	},
	RemoveShipFromFleet : function(ship){		
		CURRENT.Resources += ShipCalculator.GetCost(PLAYERS[PLAYER_ID].Ships[ship].parts);
		CURRENT.EditingFleet.ships[ship]--;
		Level.Fleets();
	},
	SaveFleet : function(){
		CURRENT.Resources = null;
		Orders.SaveFleet([CURRENT.EditingFleet]);
		Level.Map();
	},
	CancelFleet : function(){
		CURRENT.Resources = null;
		Level.Map();
	},
	AddShipType : function(){
		CURRENT.EditingShip = Factory.Ship();
		CURRENT.EditingShip.Temporal = true;
		Level.Ships();
	},
	EditShipType : function(ship){
		CURRENT.EditingShip = LIB.Clone(PLAYERS[PLAYER_ID].Ships[ship]);
		CURRENT.EditingShip.Id = ship;
		Level.Ships();
	}
}

