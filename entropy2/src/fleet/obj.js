var Fleet_Obj = {
    _IsFleetLocal(fleet,obj){
        return fleet.base == obj.id;
    },
    CanBeOrdersObject(fleet,obj){
        return obj.type!=ENUM.MapObject.Planet 
            && !fleet.Building
            && fleet.location !=-1
            && !Fleet_Obj._IsFleetLocal(fleet,obj);
    },
    CanBeLocalObject(fleet,obj){
        return obj.type!=ENUM.MapObject.Planet 
            && !fleet.Building
            && Fleet_Obj._IsFleetLocal(fleet,obj);
    },
    CanBeOrders(fleet,obj){
        return obj.type==ENUM.MapObject.Planet
            && !fleet.Building
            && fleet.location !=-1
            && !Fleet_Obj._IsFleetLocal(fleet,obj);
    },
    CanBeLocal(fleet,obj){
        return obj.type==ENUM.MapObject.Planet 
            && !fleet.Building
            && Fleet_Obj._IsFleetLocal(fleet,obj);
    },
    CanBeConstruction(fleet,obj){
        return obj.type==ENUM.MapObject.Planet 
            && fleet.Building
            && Fleet_Obj._IsFleetLocal(fleet,obj);
    }
}