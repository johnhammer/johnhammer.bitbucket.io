var AI = {
	_GetCivilTargets(){
		var targets = [];
		for(var target in GLOBAL.Map){
			if(GLOBAL.Map[target].type != ENUM.MapObject.Planet){
				targets.push(target);
			}
		}
		return targets;
	},
	_GetMilitaryTargets(player){
		var targets = [];
		for(var target in GLOBAL.Map){
			if(GLOBAL.Map[target].Player!=-1 && GLOBAL.Map[target].Player!=player){ 
				targets.push(target);
			}
		}
		return targets;
	},
	StartAll(){
		for(var i=0;i<PLAYERS.length;i++){
            if(!PLAYERS[i].Info.Human)  {
                AI.Start(i);
            }
        }
	},
	Start(player){	
		PLAYERS[player].AI = {
			CivilTargets:AI._GetCivilTargets(),
			MilitaryTargets:AI._GetMilitaryTargets(player),
			CivilFleets:[],
			MilitaryFleets:[],
			TimeToThink : 10,
			rnd : Random.Create(GLOBAL.Seed+player)
		}
		AI.OnBuildingQueueEmpty(player);
	},
	_IsAi (player){
		return !PLAYERS[player].Info.Human;
	},

	_CreateFleet(player){
		var fleet = {
			Player: player,
			Temporal: true,
			location: PLAYERS[player].Worlds[0].id,
			name: "AI generated fleet",
			ships: [5, 5],
			id:PLAYERS[player].Fleets.length
		}
		Orders.SaveFleet([fleet],player);
		AI.Event_CivilFleetSaved(player,fleet.id);
	},

	//fleet orders
	_SendFleet(player,fleet,objective,order){		
		var origin = PLAYERS[player].Fleets[fleet].location;		
		
		Orders.DeployFleet([fleet,origin,objective,order],player);			
	},	
	_DispatchCivil(player,fleet,target){
		AI._SendFleet(player,fleet,target,ENUM.MapAction.Route);
	},
	_DispatchMilitary(player,fleet,target){
		AI._SendFleet(player,fleet,target,ENUM.MapAction.Route);
	},	
	
	//EXTERNAL EVENTS
	
	//enemy abandons objective
	OnNewCivilTarget(player,target){
		var fleet = AI._GetCivilFleet(player,target);
		if(fleet){
			AI._DispatchCivil(player,fleet,target);
		}
	},
	//enemy occupies objective
	OnNewMilitaryTarget(player,target){
		var fleet = AI._GetMilitaryFleet(player,target);
		if(fleet){
			AI._DispatchMilitary(player,fleet,target);
		}
	},
	
	//INTERNAL EVENTS
	Event_CivilFleetSaved(player,fleet){
		if(AI._IsAi(player)){
			PLAYERS[player].AI.CivilFleets.push(fleet);
			AI.OnNewCivilFleet(player,fleet);
			AI.OnBuildingQueueEmpty(player);
		}
	},

	_GetCivilTarget(player,fleet){
		if(PLAYERS[player].AI.CivilTargets.length){
			return Random.Choice(PLAYERS[player].AI.rnd,
				PLAYERS[player].AI.CivilTargets)
		}
	},

	//fleet created
	OnNewCivilFleet(player,fleet){
		var target = AI._GetCivilTarget(player,fleet);
		if(target){
			AI._DispatchCivil(player,fleet,target);
		}
	},
	//fleet created or returns
	OnNewMilitaryFleet(player,fleet){
		var target = AI._GetMilitaryTarget(player,fleet);
		if(target){
			AI._DispatchMilitary(player,fleet,target);
		}
	},
	//on fleet built
	OnBuildingQueueEmpty(player){
		var resources = PLAYERS[player].Worlds[0].props.matter;		
		if(resources>99){
			//TODO
			PLAYERS[player].Worlds[0].props.matter = 0;

			AI._CreateFleet(player);
		}
		else{
			Clock.AddEvent(PLAYERS[player].AI.TimeToThink,()=> AI.OnBuildingQueueEmpty(player));
		}
	}
	
	
}	