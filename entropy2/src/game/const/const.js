var CONST = {
    TickSpeed : 0.00002,
    Factions:{
        Empire:0,
        Union:1,
        League:2,
        Califate:3,
        Republic:4,
        Dinasty:5,
        Serpent:6,
        Crown:7,
        Corporation:8,
        Confederation:9,
        States:10,
        Colonies:11,
        Aliance:12,
        Khanate:13,
        Earldoms:14,
        Kingdom:15
    }
}

var ENUM = {
    Screens :{
        Room        :0,
        Map         :1,
        Edit        :2
    },
    Gamemode : {
        Blitz       :"0", //Deathmatch + no ship creation (default types)
        Deathmatch  :"1", //no world customization, no technologies        
        Normal      :"2"
    },
    MapObject : {
        Planet 		:1,
        Gas 		:2,
        Asteroids 	:3,
        Dust 		:4,
        Fleet 		:5,
    },
    MapAction : {           //resource  myworld myfleet enworld enfleet neutworld
        None        :0,        
        Move        :1,     //defend    join    -       attack  attack  colonize
        Route       :2,     //collect   bring   refuel  trade   refuel  -
        Return      :3
    },
	PrisonersAction : {
		Enslave 	:1,		//add workers in worlds
		Conscript	:2,	    //add manpower for fleets
		Execute		:3,	    //raise morale
		Integrate	:4	    //raise technology
    },
}