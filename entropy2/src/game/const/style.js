var Style = {
	Var(vars,vals){
		document.documentElement.style.setProperty('--'+vars,vals);	
	},			
	SetFactionColor : function(color){
		Style.Var('faction_color',color);
	},	
	ToggleHelp : function(activate){
		if(activate){
			Style.Var('display-tooltips','block');
		}
		else{
			Style.Var('display-tooltips','none');
		}		
	},
	SetTheme : function(theme){
		custom_style.href = `../styleguide/faction${theme}.css`;
	}
}
