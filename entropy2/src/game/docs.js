/**
* @typedef Mesh
* @type {object}
* @property {object} position
* @property {object} material
* @property {MeshObj} obj
*/

/**
* @typedef MeshObj
* @type {object}
* @property {object} position
* @property {string} id
*/