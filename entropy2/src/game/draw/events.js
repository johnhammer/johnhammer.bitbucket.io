var Events = {
    _Raycast : function(event,callback){
        mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
        mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;
    
        raycaster.setFromCamera( mouse, camera );
    
        var intersects = raycaster.intersectObjects( scene.children, false );
    
        if ( intersects.length > 0 ) {
            callback(intersects[0].object);
        }
    }
}