/* Materials Factory */
var SHADERS = {};
var MaterialsFactory = {
	Materials : {
		Explosion: new THREE.ShaderMaterial({
			transparent:true,
			uniforms: {
				time: {
					type: "f",
					value: 0.0
				}
			},
			vertexShader: SHADERS.ExplosionVertex,
			fragmentShader: SHADERS.ExplosionFragment
		})
	},	
	SetMeshMaterial:function(mesh,material){
		mesh.traverse( function( node ) {			
			node.material = material;
		});
	},
	Explosion : function(){
		//return MaterialsFactory.Materials.Explosion.clone();
		return new THREE.ShaderMaterial({
			transparent:true,
			uniforms: {
				time: {
					type: "f",
					value: 0.0
				}
			},
			vertexShader: SHADERS.ExplosionVertex,
			fragmentShader: SHADERS.ExplosionFragment
		});
	},	
	ShipMaterial : function(color=0xff0000){
		return new THREE.MeshStandardMaterial( { 
			color: color,
			emissive : 0x111111,//0x000000
			roughness : .5,
			metalness:.9,
			roughnessMap : roughnessMaps.bricks,
			envMap : envMaps.reflection,
			flatShading:true
		} );
	},
	MirrorMaterial : function(){
		return new THREE.MeshStandardMaterial( { 
			color: 0xffffff,
			emissive : 0x000000,
			roughness : .2,
			metalness:.5,
			//envMap : envMaps.reflection,
			flatShading:false
		} );
	},
	select : function(){
		return new THREE.MeshLambertMaterial({
			color: 0xFFFFFF, 
			transparent: true, 
			opacity: 0.5
		});
	},
	selected : function(){
		return new THREE.MeshLambertMaterial({
			color: 0xFF0000, 
			transparent: true, 
			opacity: 0.5
		});
	},
	selectable : function(){
		return new THREE.MeshLambertMaterial({
			color: 0x00FF00, 
			transparent: true, 
			opacity: 0.5
		});
	},
	Mask : function(color){
		return new THREE.MeshLambertMaterial({
			color: LIB.StringToColor(color), 
			transparent: true, 
			opacity: 0.5
		});
	},
}


var PATH = "https://johnhammer.bitbucket.io/entropy2/assets/textures/";
var textureLoader = new THREE.TextureLoader();
var cubeTextureLoader = new THREE.CubeTextureLoader();

var envMaps = ( function () {

	var path = PATH;
	var format = '.jpg';
	var urls = [
		path + 'px' + format, path + 'nx' + format,
		path + 'py' + format, path + 'ny' + format,
		path + 'pz' + format, path + 'nz' + format
	];

	var reflectionCube = cubeTextureLoader.load( urls );
	reflectionCube.format = THREE.RGBFormat;

	var refractionCube = cubeTextureLoader.load( urls );
	refractionCube.mapping = THREE.CubeRefractionMapping;
	refractionCube.format = THREE.RGBFormat;

	return {
		none: null,
		reflection: reflectionCube,
		refraction: refractionCube
	};

} )();

var diffuseMaps = ( function () {

	var bricks = textureLoader.load( PATH+'brick_diffuse.jpg' );
	bricks.wrapS = THREE.RepeatWrapping;
	bricks.wrapT = THREE.RepeatWrapping;
	bricks.repeat.set( 9, 1 );

	return {
		none: null,
		bricks: bricks
	};

} )();

var roughnessMaps = ( function () {

	var bricks = textureLoader.load( PATH+'brick_roughness.jpg' );
	bricks.wrapT = THREE.RepeatWrapping;
	bricks.wrapS = THREE.RepeatWrapping;
	bricks.repeat.set( 9, 1 );

	return {
		none: null,
		bricks: bricks
	};

} )();

var matcaps = ( function () {

	return {
		none: null,
		porcelainWhite: textureLoader.load( PATH+'matcap-porcelain-white.jpg' )
	};

} )();

var alphaMaps = ( function () {

	var fibers = textureLoader.load( PATH+'alphaMap.jpg' );
	fibers.wrapT = THREE.RepeatWrapping;
	fibers.wrapS = THREE.RepeatWrapping;
	fibers.repeat.set( 9, 1 );

	return {
		none: null,
		fibers: fibers
	};

} )();

var gradientMaps = ( function () {

	var threeTone = textureLoader.load( PATH+'threeTone.jpg' );
	threeTone.minFilter = THREE.NearestFilter;
	threeTone.magFilter = THREE.NearestFilter;

	var fiveTone = textureLoader.load( PATH+'fiveTone.jpg' );
	fiveTone.minFilter = THREE.NearestFilter;
	fiveTone.magFilter = THREE.NearestFilter;

	return {
		none: null,
		threeTone: threeTone,
		fiveTone: fiveTone
	};

} )();
	
	
	var MAKEMATERIAL = function(){
		var mat = new THREE.MeshStandardMaterial( { 
			color: 0xff0000,
			emissive : 0x000000,
			roughness : .5,
			metalness:.9,
			roughnessMap : roughnessMaps.bricks,
			envMap : envMaps.reflection,
			flatShading:true,
			side:THREE.DoubleSide
		} );
		
		return mat;
	}
	