/* MeshFactory */
var lib = {
	part : function(geo,h){
		if(!h)h=geo.s;		
		return {m:new THREE.Mesh(geo),s:h};
	},
	flip : function(part){
		part.m.rotation.z = Math.PI/2;		
		return part;
	},
	altcyl : function(r2,r1,h) {
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r1,r2,h, 8, 1, false)),s:h};
	},
	cyl : function(r,h){
		return lib.altcyl(r,r,h);
	},
	prism : function(r,p,h){
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r,r,h, p, 1, false)),s:h};
	},
	sph : function(h) {
		return {m:new THREE.Mesh(new THREE.SphereGeometry(h, 8, 8)),s:h};
	},
	tor : function(h1,h2){
		var tor = new THREE.Mesh(new THREE.TorusGeometry(h1,h2,8,16));
		tor.rotation.x=Math.PI/2;
		return {m:tor,s:1};		
	},
	littleWing : function(size){
		var shape = new THREE.Shape();
		shape.moveTo( size,size );
		shape.lineTo( size,-size/2 );
		shape.lineTo( -size/2,-size/2 );
		var mesh = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, {steps: 1,depth: .2,bevelEnabled: false} ));
		return lib.skew({m:mesh,s:1},3);
	},
	skew : function(part,amt){
		part.m.rotation.z = Math.PI / amt;
		return part;
	},
	merge : function(b,a){
		a.updateMatrix();
		b.merge(a.geometry, a.matrix);
	},	
	assemble :function(parts){
		var geo = new THREE.Geometry();	
		var orig = parts[0] ? -parts[0].s/2 : -parts[1].s/2;
		for ( var i = 0; i < parts.length; i++ ) {	
			if(parts[i]){
				parts[i].m.position.y = orig+parts[i].s/2 + (parts[i].shift||0);
				lib.merge(geo,parts[i].m);
				if(!parts[i].skip) orig += parts[i].s;
			}
		}
		geo.s=orig;
		geo.translate(0,-orig/2,0);
		return geo;
	},
	assembleM :function(parts){
		var mesh = new THREE.Mesh();	
		var orig = parts[0] ? -parts[0].s/2 : -parts[1].s/2;
		for ( var i = 0; i < parts.length; i++ ) {	
			if(parts[i]){
				parts[i].m.position.y = orig+parts[i].s/2 + (parts[i].shift||0);
				mesh.add(parts[i].m);
				if(!parts[i].skip) orig += parts[i].s;
			}
		}
		mesh.geometry.translate(0,-orig/2,0);
		return mesh;
	},
	multi: function(object,amount,radius,rotate=false){
		var geo = new THREE.Geometry();
		
		var x0 = 0;
		var y0 = 0;		
		var angle = 0;
		for(var i = 0; i < amount; i++) {
			var x = x0 + radius * Math.cos(2 * Math.PI * i / amount);
			var y = y0 + radius * Math.sin(2 * Math.PI * i / amount);   
			var cyl = object.m.clone();					
			cyl.position.x = x;
			cyl.position.z = y;			
			if(rotate){
				cyl.rotation.y = angle;			
				angle -= 2*Math.PI/amount;	
			}
			lib.merge(geo,cyl);
		}
		
		return {m:new THREE.Mesh(geo),s:object.s};
	},
	multiRot: function(object,amount,radius){
		return lib.multi(object,amount,radius,true)
	},	
	multiM: function(object,amount,radius,meshes,rotate=false){
		var mesh = new THREE.Mesh();		
		var x0 = 0;
		var y0 = 0;		
		var angle = 0;
		for(var i = 0; i < amount; i++) {
			var x = x0 + radius * Math.cos(2 * Math.PI * i / amount);
			var y = y0 + radius * Math.sin(2 * Math.PI * i / amount);   
			var cyl = object.m.clone();					
			cyl.position.x = x;
			cyl.position.z = y;			
			if(rotate){
				cyl.rotation.y = angle;			
				angle -= 2*Math.PI/amount;	
			}
			meshes.push(cyl);
			mesh.add(cyl);
		}		
		mesh.s = object.s;
		return {m:mesh,s:object.s};
	},
	multiRotM: function(object,amount,radius,meshes){
		return lib.multiM(object,amount,radius,meshes,true)
	},
	skip(piece){
		if(piece) piece.skip = true;
		return piece;		
	},
	iif(part,num,lim=1){
		return num<lim ? null : part;
	},
	nif(part,num,lim=1){
		return !num<lim ? null : part;
	},	
	iifA(partCallback,num,lim=1){
		return num<lim ? null : partCallback();
	},
	f(num,factor){
		return 1+num*factor;
	},
	move(piece,amount){
		piece.shift = amount;
		return piece;
	}
}
