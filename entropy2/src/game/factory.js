var Factory = {
	/**
	@typedef Fleet
	@property {string} name
	@property {number[]} ships
	@property {string} location
	@property {string} base
	@property {string} id
	@property {int} order
	@property {number} Player
	*/
	/** @returns {Fleet} */
	Fleet : function(location,player=PLAYER_ID){
		var ships = [];
		for(var i=0;i<PLAYERS[player].Ships.length;i++){
			ships.push(0);
		}
		return { 
			name: FleetCalculator.GetRandomName(PLAYERS[player].Info.Civ),
			ships: ships,
			location:location,
			base:location,
			Player:player,
			id:PLAYERS[player].Fleets.length,
			order:ENUM.MapAction.None
		}
	},
	FleetInMap : function(fleetId,player,origin,destiny,time){
		return {
            x : origin.x,	
            y : origin.y,	
            z : origin.z,
            id : Map_Edit.GetFleetInMapId(player,fleetId),
            objective : destiny,
            origin : origin,
            player: player,
            type: ENUM.MapObject.Fleet,
            speed : time,
			tick:0			
        }
	},
	
	/**
	@typedef Ship
	@property {string} name
	@property {ShipParts} parts

	@typedef ShipParts
	@property {number} Weapon
	@property {number} Ammo
	@property {number} Shield
	@property {number} Engine
	@property {number} Fuel
	@property {number} Habitat
	@property {number} Radio
	@property {number} Computer
	@property {number} Size
	@property {number} Nada
	*/
	/** @returns {Ship} */
	Ship : function(){
		var parts = {
			Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1,Nada:1
		};
		return {
			name: "New ship",
			parts: parts
		}
	}
}