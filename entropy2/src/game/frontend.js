/* FRONTEND */
GLOBAL.Hamburger = false;

var TEXT = function(key){
	var language = GLOBAL.Language;
	
	switch(language){
		case 0: return key;	//english
		case 1: return SPANISH[key];		
	}
	return 'MISSING TEXT';
}   
var LONG = function(key){
	var language = GLOBAL.Language;
	
	switch(language){
		case 0: return ENGLISH_LONG[key];
		case 1: return SPANISH_LONG[key];		
	}
}

var LANGUAGE_DETAILS = function(){
	var language = GLOBAL.Language;
	
	switch(language){
		case 0: return {
			AdjectiveName : true
		};
		case 1: return {
			AdjectiveName : false
		};		
	}
	return 'MISSING LONG TEXT';
}  

var Frontend = {
	Main(content,showHamburger = true){
		mainbody.innerHTML = showHamburger?Templating.Main.Hamburger():'';
		mainbody.innerHTML += content;
		mainbody.innerHTML += Templating.Main.Footer();
	},
	OnHamburgerClick(node){			
		var parent = node.nextElementSibling;
		var first = parent.firstElementChild;
		var second = first.nextElementSibling;
		if(GLOBAL.Hamburger){
			first.style.display = "block";
			second.style.display = "none";
			second.style.position = "absolute";
			GLOBAL.Hamburger=false;
		}
		else{
			first.style.display = "none";
			second.style.display = "flex";
			second.style.position = "relative";
			GLOBAL.Hamburger=true;
		}			
	},
	Snackbar(content) {
		var x = document.getElementById("footer_snackbar");
		x.className = "c-snackbar c-snackbar-show";
		x.innerHTML = content;
		setTimeout(function(){ x.className = x.className.replace("c-snackbar-show", ""); }, 3000);
	},
	Modal : function(content){
        mainbody.innerHTML += `
            <div id="current_modal" class="modal">
                <div class="modal-content">
                    ${content}
                </div>
            </div>            
        `;
        window.onclick = function(event) {
            if (event.target == current_modal) {
                Frontend.CloseModal();
            }
        };
    },
	CloseModal : function(){
		current_modal.outerHTML = '';
		window.onclick = null;
	},
	HideIf(bool){
		return bool?'style="display:none"':'';
    }
}