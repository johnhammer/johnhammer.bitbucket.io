var GLOBAL = {
    Language : 1,
    Seed : 10,
    Gamemode : ENUM.Gamemode.Fast,
    Screen : ENUM.Screens.Room
}
GLOBAL.Seed = Random.Int(Random.Create());
GLOBAL.rnd = Random.Create(GLOBAL.Seed);

/**
* @typedef Current
* @type {object}
* @property {Mesh} SelectedMesh
* @property {object} World
* @property {object} MovingFleets
* @property {number} Tick
* @property {object} EditingShip
* @property {object} EditingFleet
* @property {number} Resources
*/
/** @type {Current} */
var CURRENT = {
    SelectedMesh : null,
    World : null,
    MovingFleets : {},
    Tick : 0,
    EditingShip : null,
    EditingFleet : null,
    Resources : 0
}

var ANIMATED = {
    Ships : [],
    Bullets : [],
	Explosions : []
};

var ISONLINEGAME = true;
var PLAYERS = [];
var CURRENT_PLAYER = 0;
var PLAYER_ID = 0;
var OBJECTS = [];

var scene,renderer,camera,raycaster,mouse,controls;