
var Listener = {
	Listen : function(message){		
		switch(message.t){
			case Protocol.Types.Chat:
				Chat.Show(message.n,message.m);
				break;
			case Protocol.Types.Enter:
				Room.AddHuman();
				Protocol.Welcome();
				break;
			case Protocol.Types.Welcome:
				Room.SetPlayers(message.m);
				break;
			case Protocol.Types.Start:
				Level.Game();
				break;
			case Protocol.Types.Game:
				this.GameListen(message.m);
				break;
			case Protocol.Types.Room:
				this.RoomListen(message.m);
				break;
			default:
				console.log(message);
		}		
	},
	GameListen : function(message){
		var order = JSON.parse(message);
		Orders[order.f](order.a,order.p);
	},
	RoomListen : function(message){
		Room.UpdatePlayers(message);
	}	
}
