var Mapping = {  
    BattleShip(raw,isMine){
		var fireSpeed = ShipCalculator.GetFireSpeed(raw.parts);
		var firePower = ShipCalculator.GetFirePower(raw.parts);
		var health = ShipCalculator.GetArmor(raw.parts);
		var electronics = ShipCalculator.GetElectricAttack(raw.parts);		
		
		var numGuns = fireSpeed;
        var guns = [];
        var gun = {
            Cadence : 1,								
            Attack : firePower,
			Recoil : 1
        };
        for(var j=0; j<numGuns; j++){
            var newGun = LIB.Clone(gun);
            guns.push(newGun);
        }                
        var ship = {
            CanElectronic : electronics>0,					
            CadenceElectronic : electronics,					
            ElecAttack : electronics,
            Health : health*health,
            ElecHealth : electronics*electronics, 
            Multiobjective : fireSpeed>4,	
            CanAttack : fireSpeed>0,	
            Guns : guns,
			MeshProps : {
				Parts : raw.parts,
				IsMine : isMine
			}
        }
        return ship;
    },
    BattleFleet(raw){
		var isMine = raw.fleet.Player==PLAYER_ID;
        var ships = [];
        for(var i=0; i<raw.shipTypes.length; i++){
            if(raw.fleet.ships[i]>0){    
                var ship = Mapping.BattleShip(raw.shipTypes[i],isMine);                
                for(var z=0; z<raw.fleet.ships[i]; z++){
                    var newShip = LIB.Clone(ship);
                    newShip.Id = ships.length;
                    ships.push(newShip);
                }
            }
        }
		var id = raw.fleet.name;
        var result = {
			Name : id,
            Groups:{}
        };		
		result.Groups[0] = {
			Id : id,
			Objectives : [],
			Ships:ships,
			MeshProps : {
				Y : isMine?50:150,
				Area : 50
			}
		};
        return result;
    },
    Ship(raw){
        //ship parts
        var parts = [];
        for(var part in raw.parts){
            if(part!='Nada'&&part!='Size'){
                parts.push({
                    id:part,
                    name:TEXT(part),
                    amount:raw.parts[part],
                    canAdd:raw.parts.Nada > 0,
                    canRemove:raw.parts[part] >= 1               
                })
            }            
        }

        //ship props
        var s = raw.parts;
        var props = [];
        props.push({percent:ShipCalculator.GetFirePowerPercent(s),		amount:ShipCalculator.GetFirePower(s),			id:'FirePower',         name:TEXT('FirePower')		});
        props.push({percent:ShipCalculator.GetFireSpeedPercent(s),		amount:ShipCalculator.GetFireSpeed(s),			id:'FireSpeed',         name:TEXT('FireSpeed')		});
        props.push({percent:ShipCalculator.GetPrecisionPercent(s),		amount:ShipCalculator.GetPrecision(s),			id:'Precision',         name:TEXT('Precision')		});
        props.push({percent:ShipCalculator.GetElectricAttackPercent(s),	amount:ShipCalculator.GetElectricAttack(s),	    id:'ElectricAttack',    name:TEXT('ElectricAttack')	});
        props.push({percent:ShipCalculator.GetArmorPercent(s),			amount:ShipCalculator.GetArmor(s),				id:'Armor',             name:TEXT('Armor')			});
        props.push({percent:ShipCalculator.GetTripulationPercent(s),	amount:ShipCalculator.GetTripulation(s),		id:'Tripulation',       name:TEXT('Tripulation')	});
        props.push({percent:ShipCalculator.GetEvasionPercent(s),		amount:ShipCalculator.GetEvasion(s),			id:'Evasion'	,       name:TEXT('Evasion')		});
        props.push({percent:ShipCalculator.GetCamouflagePercent(s),		amount:ShipCalculator.GetCamouflage(s),		    id:'Camouflage',        name:TEXT('Camouflage')		});
        props.push({percent:ShipCalculator.GetSpeedPercent(s),			amount:ShipCalculator.GetSpeed(s),				id:'Speed',             name:TEXT('Speed')			});
        props.push({percent:ShipCalculator.GetPerceptionPercent(s),		amount:ShipCalculator.GetPerception(s),		    id:'Perception',        name:TEXT('Perception')		});
        props.push({percent:ShipCalculator.GetAutonomyPercent(s),		amount:ShipCalculator.GetAutonomy(s),			id:'Autonomy',          name:TEXT('Autonomy')		});
        props.push({percent:ShipCalculator.GetCargoCapacityPercent(s),	amount:ShipCalculator.GetCargoCapacity(s),		id:'CargoCapacity',     name:TEXT('CargoCapacity')	});

        //ship skills
        var skills = ShipNameFactory.GetShipSkills(s,raw.faction)
            .map(x=>{return{
                name:TEXT(x),
                text:LONG(x),
                id:x
            }});

        //ship type
        var type = ShipNameFactory.GetShipTypeName(s);
        var name = ShipNameFactory.GetShipName(raw.faction,s);

        return {
            parts : parts,
            props : props,
            skills : skills,
            name:name,
            type:type
        }
    },
    Fleet(raw){
        /*
            types : PLAYERS[PLAYER_ID].Ships,
            ships : CURRENT.EditingFleet.ships,
            planet: CURRENT.SelectedMesh.obj
        */
        //get fleet cost
        var resources = CURRENT.SelectedMesh.obj.props.matter;
        var cost = 0;
        var time = 0;
        for(var i=0;i<raw.types.length;i++){	
			cost += ShipCalculator.GetCost(raw.types[i].parts) * raw.ships[i];
			time += ShipCalculator.GetTimeToBuild(raw.types[i].parts) * raw.ships[i];
        }

        //get fleet ships
        var ships = [];
        for(var i=0;i<raw.types.length;i++){	
            var amt = raw.ships[i];
            var scost = cost+ShipCalculator.GetCost(raw.types[i].parts);
			ships.push({
                name:raw.types[i].name,
                id:i,
                amount:amt,
                disablePlus:scost>resources,
                disableMinus:amt<1
            });
        }

        //get fleet
        return {
            name:raw.name,
            cost:cost,
            costmax:resources,
            time:time+'s',
            ships:ships,
            details:{}
        }
    },
    RoomPlayer(raw,i){
        var civname = TEXT(FACTIONS[raw.Info.Civ].name);
        return {
            name:raw.Info.Name,
            color:raw.Info.Color,
            factionName:civname,
            ai:!raw.Info.Human,
            me:i==0
        };
    },
    MapObject(obj){
        var color = obj.Player == -1?null:PLAYERS[obj.Player].Info.Color;

        var result = {
            name:obj.name,
            kingdomColor:color,
            canAddFleet:Map_Obj.CanAddFleet(obj),
            canEdit:Map_Obj.CanEdit(obj)
        };
        if(obj.type==ENUM.MapObject.Planet){
            result.resources={
                matter:obj.props.matter,
                debris:0,
                humans:0
            }
        }
        else{
            result.matter={
                matter:obj.props.matter,
                max:obj.props.maxmatter
            }
        }       
        return result;
    },
    _MapFleet(fleet,obj){
        var ishere = fleet.location!=-1;
        var fleetOrders = FleetCalculator.GetFleetOrders(fleet,obj);
        var routeDetails = FleetCalculator.GetFleetRouteDetails(fleet,obj,GLOBAL.Map[fleet.base]);
        return {
            id:fleet.id,
            name:fleet.name,
            time:routeDetails.Time+'s',
            fuel:routeDetails.Fuel+'T',
            disableArrow1:!fleetOrders.Move,
            disableArrow2:!fleetOrders.Route,
            isHere: ishere
        }
    },
    MapFleets(obj,fleets){
        var result = {};
        if(Map_Obj.CanHaveOrdersObjectFleets(obj)){
            result.ordersObject = 
                fleets
                    .filter(x=>Fleet_Obj.CanBeOrdersObject(x,obj))
                    .map(x=>Mapping._MapFleet(x,obj));
        }
        if(Map_Obj.CanHaveLocalObjectFleets(obj)){
            result.localObject = 
                fleets
                    .filter(x=>Fleet_Obj.CanBeLocalObject(x,obj))
                    .map(x=>Mapping._MapFleet(x,obj));
        }
        if(Map_Obj.CanHaveOrdersFleets(obj)){
            result.orders = 
                fleets
                    .filter(x=>Fleet_Obj.CanBeOrders(x,obj))
                    .map(x=>Mapping._MapFleet(x,obj));
        }
        if(Map_Obj.CanHaveLocalFleets(obj)){
            result.local = 
                fleets
                    .filter(x=>Fleet_Obj.CanBeLocal(x,obj))
                    .map(x=>Mapping._MapFleet(x,obj));
        }
        if(Map_Obj.CanHaveConstructionFleets(obj)){
            result.construction = 
                fleets
                    .filter(x=>Fleet_Obj.CanBeConstruction(x,obj))
                    .map(x=>Mapping._MapFleet(x,obj));
        }
        return result;        
    }
}