var Orders = {
	Emit : function(func,args,player){
		if(ISONLINEGAME && player==PLAYER_ID){
			var msg = JSON.stringify({
				f:func,
				a:args,
				p:player
			});
			Protocol.Game(msg);
		}
	},	
	DeployFleet(args,player=PLAYER_ID){ 		
		Orders.Emit('DeployFleet',args,player); 
		
		var fleet = args[0];
		var origin = args[1];
		var destiny = args[2];
		var order = args[3];

		if(GLOBAL.Screen == ENUM.Screens.Map){
			Draw_Map.DeployFleet(fleet,origin,destiny,player,order);
		}	
		Map_Edit.DeployFleet(fleet,player,origin,destiny,order);			
	},	
	SaveFleet(args,player=PLAYER_ID){
		Orders.Emit('SaveFleet',args,player); 
		
		var fleet = args[0];
		
		Fleet_Edit.SaveFleet(fleet,player);
	},	
	SaveShip(args,player=PLAYER_ID){
		Orders.Emit('SaveShip',args,player); 
		
		var ship = args[0];
		
		Ship_Edit.SaveShip(ship,player);
	}
}