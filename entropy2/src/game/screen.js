var Menu = {
    Main : function(option){
        switch(option){
            case 0:Level.Room(true);break;
            case 1:Level.Room();break;
            case 2:Level.Tutorial();break;
            case 3:Level.Options();break;
            case 4:Level.MapEditor();break;
        }
	}
}

var Level = {
    Tutorial(){
        Frontend.Modal(Templating.Main.Options());
    },
    Options(){
        Frontend.Modal(Templating.Main.Tutorial());
    },
	MapEditor:function(){
		window.location.href = "../editor/index.html"
	},
    Room : function(isSinglePlayer=false){
        var data = {
            isOnline : !isSinglePlayer,
            players : []
        }
        Frontend.Main(Templating.Room.Main(data));
		if(isSinglePlayer){
			Room.Single();
		}		
    },
    Game : function(){                
        Start.Game();        
        Level.Map(); 
		Draw.Render();
        Clock.Init();
        AI.StartAll();
    },
    Fleets : function(){
        GLOBAL.Screen = ENUM.Screens.Edit;
        var data = Mapping.Fleet({
            types : PLAYERS[PLAYER_ID].Ships,
            ships : CURRENT.EditingFleet.ships,
            planet: CURRENT.SelectedMesh.obj,
            name:CURRENT.EditingFleet.name
        });
        Frontend.Main(Templating.Fleet.Main(data));
        fleet_ships.innerHTML = data.ships.map(Templating.Fleet.FleetShip).join('')	
		fleet_details.innerHTML = Templating.Fleet.FleetDetails(data.details);
    },
	Ships : function(){
        var data = Mapping.Ship({
            parts : CURRENT.EditingShip.parts,
            name:CURRENT.EditingShip.name,
            faction:PLAYERS[PLAYER_ID].Info.Civ
        });
        Frontend.Main(Templating.Ship.Main(data));
		Draw_Ship.Ship(CURRENT.EditingShip.parts); 
    },
    Map : function(){
        GLOBAL.Screen = ENUM.Screens.Map;
        Frontend.Main(Templating.Map.Main()); 

		Draw_Map.Map(); 
		
		if(!CURRENT.SelectedMesh){			   
			CURRENT.SelectedMesh = PLAYERS[PLAYER_ID].Worlds[0].mesh;
		}
		
		Map_Events.SelectMapObject(CURRENT.SelectedMesh);
		Draw_Map.MoveCameraToObject(CURRENT.SelectedMesh);		
    },
    Battle : function(f1,f2){
        Frontend.Main(Templating.Battle(f1,f2));    
        Battle.Start(f1,f2);    
    },
}