var Start = {
    Game : function(){  
        var details = MapDetails.GetMapDetails();    

        GLOBAL.Gamemode = details.gamemode;
        var rnd = Random.Create(GLOBAL.Seed);
	
        //Globals
        GLOBAL.Map = Map_Maker.Create(rnd,details);

        //Players
        for(var i=0;i<PLAYERS.length;i++){
			PLAYERS[i].Worlds = Start.InitWorlds(rnd,i);
            PLAYERS[i].Ships = Start.DefaultShips(GLOBAL.Gamemode==ENUM.Gamemode.Blitz,PLAYERS[i].Info.Civ);
            PLAYERS[i].Fleets = Start.DefaultFleets(PLAYERS[i].Worlds[0].id,i);      
        }
    },	
	
	AddGlobalPlayer : function(human){	
        var civ = Random.Range(GLOBAL.rnd,0,15);	
        var name = human?'Player':FactionNameFactory.MakeName(civ,GLOBAL.rnd);	
		PLAYERS.push({
			Info:{
				Name : name,
				Civ : civ,
				Human : human,
				Color : LIB.RandomColor(GLOBAL.rnd) //TODO
			},
		});
	},	
	InitWorlds : function(rnd,player){
        var world = Random.Choice(rnd,
            Object.keys(GLOBAL.Map)
                .filter( x=>GLOBAL.Map[x].type==ENUM.MapObject.Planet 
                    && GLOBAL.Map[x].Player==-1 )
                .map( x=>GLOBAL.Map[x]));
		world.Player = player;
		world.IsBuilding = false;
		return [world];
	},
    DefaultShips : function(isBlitz,faction){
        if(!isBlitz){
            return [];
        }
        var factionShips = FACTIONS[faction].ships.concat(FACTIONS[faction].oship);
        return factionShips.map(x=>{
            var res = {
                name: x.name?x.name:ShipNameFactory.GetShipName(faction,x),
                parts: x
            };
            res.parts.Nada = ShipCalculator.GetCargoCapacity(res.parts);
            return res;
        });        
    },
    DefaultFleets : function(world,player){
        return [];
    },
    
}