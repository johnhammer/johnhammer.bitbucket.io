/* TEMPLATING */
var Templating={};

Templating.Main = {
	Options(){
		return `
<h2>Options</h2>
<div class="l-h l-h-two">
	<div class="l-v-0">		
		<table class="c-table-73">
			<tr>
				<td>Game mode</td>
				<td>
					<select>
						<option>Fast</option>
						<option>Normal</option>
					</select>
				</td>				
			</tr>
		</table>
	</div>
	<div class="l-v-0">
		<table class="c-table-73">
			<tr>
				<td>Game mode</td>
				<td>
					<select>
						<option>Fast</option>
						<option>Normal</option>
					</select>
				</td>				
			</tr>
		</table>
	</div>
</div>
<button onclick="">Guardar</button>
<button onclick="">Cancelar</button>
		`;
	},
	Tutorial(){
		return `Tutorial not yet created`;
	},
	Hamburger(){
		return`
<span class="c-hamburger" onclick="Frontend.OnHamburgerClick(this)">☰</span>
		`;
	},
	Footer(){
		return `
<div id="footer_snackbar" class="c-snackbar"></div>
		`;
	},
	Main(){
		return`
<div class="c-mainmenu">
	<h1>Entropy</h1>		
	<div>
		<button onclick="Menu.Main(0)">${TEXT('Single player')}</button>
		<button onclick="Menu.Main(1)">${TEXT('Multiplayer')}</button>
		<button onclick="Menu.Main(2)">${TEXT('Tutorial')}</button>
		<button onclick="Menu.Main(3)">${TEXT('Options')}</button>
		<button onclick="Menu.Main(4)">${TEXT('Map editor')}</button>
	</div>
</div>
		`;
	}
}

Templating.Room = {
/*
	isOnline,
	player:{
		name,
		color,
		factionName,
		me,
		ai
	}
//////////////
	factions:{
		id,
		name,
		playerColor
	}	
*/
	FactionThumb(data){
		return`
<div onclick="Room.ShowFactionContent(${data.id})">
	<img src="..\\assets\\transparent\\${data.name}.png" 
		style="background-color:${data.playerColor}">
	<h3>${TEXT(data.name)}</h3>
</div>
		`;
	},
	FactionDetail(data){
		return `
<h1>${TEXT(data.name)}</h1>
<img src="..\\assets\\transparent\\${data.name}.png" 
		style="background-color:${data.playerColor}">
<div>
	<p>${LIB.Replace(LONG('History_'+data.name),"#","</p><p>")}</p>
</div>
<button onclick="Room.ChangePlayerFaction(${data.id})">
	${TEXT('Pick this faction')}
</button>
		`;
	},
	Factions(){
		return`
<div class="l-grid">
	<div id="factions"></div>
	<div id="faction_info"></div>
</div>
		`;
	},	
	PlayerRow(data){
		return `
<tr ${data.me?'':'class="unactionable"'}>
	<td>
		<input ${data.me?
			'onchange="Room.ChangePlayerName(this.value)"':
			'disabled=""'} value="${data.name}">
	</td>		
	<td>${data.ai?'💻':''}</td>
	<td>
		<input ${data.me?
			'onchange="Room.ChangePlayerColor(this.value)" id="myColor"':
			'disabled=""'} type="color" value="${data.color}">
	</td>
	<td>
		<select ${data.me?
			'onmousedown="(function(e){ e.preventDefault(); })(event, this)" onclick="Room.OpenFactionModal()"':
			'disabled=""'}>
			<option>${data.factionName}</option>
		</select>
	</td>
</tr>
		`;
	},
	Main(data){
		return `
<div class="l-main l-h l-h-two">
	<div class="l-v-0">
		<h1>${TEXT('New game')}</h1>
		<p id="room_info" style="display:none">
			${TEXT('Copy')}: <input id="room_code">
		</p>
		<table id="players" class="c-table-room"></table>
		<button id="room_create" onclick="Room.Create()" ${Frontend.HideIf(!data.isOnline)}>
			${TEXT('Host')}
		</button>
		<button id="room_join" onclick="Room.Join()" ${Frontend.HideIf(!data.isOnline)}>
			${TEXT('Join')}
		</button>		
		<button ${Frontend.HideIf(data.isOnline)} id="room_add" onclick="Room.AddAI()">
			+ ${TEXT('Add AI')}
		</button>
		<button style="display:none" id="room_allow" onclick="Room.Allow()">
			+ ${TEXT('Add player')}
		</button>
		<button ${Frontend.HideIf(data.isOnline)} style="display: block;" id="room_start" onclick="Room.StartGame()">
			${TEXT('Go!')}
		</button>
	</div>
	<div class="${data.isOnline?'l-v l-v-two-top':'l-v-0'}">
		<div ${Frontend.HideIf(data.isOnline)} id="details">
			<h2>${TEXT('Map details')}</h2>
			<table class="c-table-73 unactionable">
				<tr>
					<td>${TEXT('Game mode')}</td>
					<td>
						<select id="details_gamemode">
							<option value=0>${TEXT('Blitz')}</option>
							<option value=1>${TEXT('Deathmatch')}</option>
							<option value=2>${TEXT('Normal')}</option>
						</select>
					</td>				
				</tr>
				<tr>
					<td>${TEXT('Resources')}</td>
					<td>
						<select id="details_resources">
							<option value=0>${TEXT('Few')}</option>
							<option value=1>${TEXT('Normal')}</option>
							<option value=2>${TEXT('Many')}</option>
						</select>
					</td>				
				</tr>
				<tr>
					<td>${TEXT('Abundance')}</td>
					<td>
						<select id="details_abundance">
							<option value=0>${TEXT('Few')}</option>
							<option value=1>${TEXT('Normal')}</option>
							<option value=2>${TEXT('Many')}</option>
						</select>
					</td>				
				</tr>
				<tr>
					<td>${TEXT('War fog')}</td>
					<td><input id="details_warfog" type="checkbox" checked></td>				
				</tr>
				<tr>
					<td>${TEXT('Explore')}</td>
					<td><input id="details_explore" type="checkbox" checked></td>				
				</tr>
				<tr>
					<td>${TEXT('Map type')}</td>
					<td>
						<select id="details_maptype">
							<option value=0>${TEXT('Cloud')}</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>${TEXT('Dispersion')}</td>
					<td>
						<select id="details_dispersion">
							<option value=0>${TEXT('Low')}</option>
							<option value=1>${TEXT('Medium')}</option>
							<option value=2>${TEXT('High')}</option>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div style="display:none" id="chat">
			<h2>${TEXT('Chat')}</h2>
			<div id="chat_output"></div>
			<input id="chat_input">
			<input id="chat_submit" type="submit" onclick="Chat.Send()">
		</div>
	</div>
</div>`
	}
}

Templating.Map = {
/*
	object :{
		name,
		kingdomColor,
		canAddFleet,
		canEdit,
		resources:{//planet only
			matter,
			debris,
			humans
		},
		matter:{//not planet
			matter,
			max
		}
	},
	fleets : {
		fleet : {
			id,
			name,
			time,
			fuel,
			disableArrow1,
			disableArrow2,			
			isHere,
			type //orders, ordersobject, localobject, local, construction

			canEdit,
			canArrow1,
			canArrow2,
			canArrowBack
			disableArrowBack,	
		},
		orders:[],
		localObject:[],
		local:[],	
		construction:[],
	}
*/
	_MapFleet(data){
		return `
<tr>
	<td ${data.canEdit?
		`onclick="Map_Menu.EditFleet(${data.id})">✎ `
		:'class="unactionable">'}${data.name}</td>		
	${data.time?`<td class="unactionable">${data.time}</td>`:''}
	${data.fuel?`<td class="unactionable">${data.fuel}</td>`:''}
	${data.canArrow1?`<td ${data.disableArrow1?
		'class="disabled"':
		`onclick="Map_Menu.DeployFleet(${data.id},${ENUM.MapAction.Move})"`}>→</td>`:''}
	${data.canArrow2?`<td ${data.disableArrow2?
		'class="disabled"':
		`onclick="Map_Menu.DeployFleet(${data.id},${ENUM.MapAction.Route})"`}>⇄</td>`:''}
	${data.canArrowBack?`<td ${data.disableArrowBack?
		'class="disabled"':
		`onclick="Map_Menu.DeployFleet(${data.id},${ENUM.MapAction.Return})"`}>↶</td>`:''}
</tr>			
		`;
	},
	MapFleet_Orders(data){
		return Templating.Map._MapFleet({
			id:data.id,
			name:data.name,
			time:data.time,
			fuel:data.fuel,
			canArrow1:true
		});
	},
	MapFleet_OrdersObject(data){
		return Templating.Map._MapFleet({
			id:data.id,
			name:data.name,
			time:data.time,
			fuel:data.fuel,
			canArrow1:true,
			canArrow2:true,
			disableArrow1:data.disableArrow1,
			disableArrow2:data.disableArrow2
		});
	},
	MapFleet_LocalObject(data){
		return Templating.Map._MapFleet({
			id:data.id,
			name:data.name,
			time:data.time,
			fuel:data.fuel,
			canArrowBack:true
		});
	},
	MapFleet_Local(data){
		return Templating.Map._MapFleet({
			name:data.name,
			id:data.id,
			canEdit:data.isHere,
			time:data.time,
			fuel:data.fuel,
			canArrowBack:true,
			disableArrowBack:data.isHere
		});
	},
	MapFleet_Construction(data){
		return Templating.Map._MapFleet({
			id:data.id,
			name:data.name,
			time:data.time
		});
	},
	MapFleets(data){
		return `
${data.orders?`
	<h2>${TEXT('Fleet orders')}</h2>
	<table id="fleets_list_orders">
		${data.orders.map(Templating.Map.MapFleet_Orders).join('')}
	</table>
`:''}
${data.ordersObject?`
	<h2>${TEXT('Fleet orders')}</h2>
	<table id="fleets_list_orders_object">
		${data.ordersObject.map(Templating.Map.MapFleet_OrdersObject).join('')}
	</table>
`:''}
${data.localObject?`
	<h2>${TEXT('Local fleets')}</h2>
	<table id="fleets_list_local_object">
	${data.localObject.map(Templating.Map.MapFleet_LocalObject).join('')}
	</table>
`:''}
${data.local?`
	<h2>${TEXT('Local fleets')}</h2>
	<table id="fleets_list_local">
	${data.local.map(Templating.Map.MapFleet_Local).join('')}
	</table>
`:''}
${data.construction?`
	<h2>${TEXT('Fleets in construction')}</h2>
	<table id="fleets_list_construction">
		${data.construction.map(Templating.Map.MapFleet_Construction).join('')}
	</table>
`:''}
		`;
	},
	MapObject(data){
		return `
<div>
	<h1>${data.name}</h1>				
	<div>	
		${data.kingdomColor?
			`<button disabled style="background:${data.kingdomColor};"></button>`:''}				
		<button onclick="Map_Menu.FocusCamera()">⇧</button>	
		${data.canAddFleet?'<button onclick="Map_Menu.AddFleet()">+</button>':''}
		${data.canEdit?'<button onclick="Map_Menu.Edit()">✎</button>	':''}									
	</div>
</div>
${data.resources?`
	<div>
		<h3>${TEXT('Matter')}</h3>
		<h3>${TEXT('Debris')}</h3>
		<h3>${TEXT('Humans')}</h3>
	</div>
	<div>
		<h2>${data.resources.matter}</h2>
		<h2>${data.resources.debris}</h2>
		<h2>${data.resources.humans}</h2>
	</div>
`:''}
${data.matter?`
<div>
	<h3>${TEXT('Matter')}</h3>
	<h2>${data.matter.matter}<span>/${data.matter.max}</span></h2>
</div>
`:''}
		`;
	},
	Main(){
		return `
<div class="l-main l-h l-h-two-left">
	<div class="l-canvas-container">
		<canvas id="map_canvas" class="c-map-canvas"></canvas>
	</div>
	<div class="l-v l-v-two-bot">
		<div id="map_object" class="c-editable-object"></div>
		<div id="map_fleets"></div>
	</div>
</div>
		`;
	}
}

Templating.Fleet = {
	/*
		name,
		cost,
		costmax,
		time,
		ships:{
			name,
			id,
			amount,
			disablePlus,
			disableMinus
		}
	*/
	CompleteFleet(data){
		return `
Fleet built ${data.name}
		`;
	},
	FleetShip(data){
		return `
<tr>
	<td onclick="Fleet_Menu.EditShipType(${data.id})">✎ ${data.name}</td>
	<td id="ship_amount_${data.id}" class="unactionable">${data.amount}</td>
	<td id="ship_add_${data.id}" ${data.disablePlus?
		'class="disabled"':
		`onclick="Fleet_Menu.AddShipToFleet(${data.id})"`}>+</td>
	<td id="ship_remove_${data.id}" 
		${data.disableMinus?'class="disabled"':
		`onclick="Fleet_Menu.RemoveShipFromFleet(${data.id})"`}>-</td>                        
</tr>
		`;
	},
	FleetDetails(data){
		return `
<div>
	<h2>Skills</h2>
</div>
<div>
	<div class="bar_container"></div>
</div>
		`;
	},
	Main(data){
		return`
<div class="l-main l-h l-h-two">
	<div class="l-v l-v-two-bot">
		<div class="c-editable-object">
			<div>
				<h1><input onchange="" value="${data.name}"></h1>			
				<div>	
					<button onclick="Fleet_Menu.SaveFleet()">✓</button>
					<button onclick="Fleet_Menu.CancelFleet()">✗</button>								
				</div>
			</div>
			<div>
				<h3>${TEXT('Cost')}</h3>
				<h2>${data.cost}<span>/${data.costmax}</span></h2>
				<h3>${TEXT('Time to build')}</h3>
				<h2>${data.time}</h2>
			</div>
		</div>
		<div>                
			<table id="fleet_ships"></table>
			<button onclick="Fleet_Menu.AddShipType()">
				+ ${TEXT('Add ship type')}
			</button>
		</div>
	</div>
	<div class="l-v" id="fleet_details"></div>
</div>
		`;
	}
}

Templating.Ship = {
	/*
		parts : {
			id,
			name,
			amount,
			canAdd,
			canRemove
		},
		props : {
			percent,
			amount,
			name
		},
		skills : {
			name
		},
		name,
		type
	*/
	_ShipPart(data){
		return `
<tr>
	<td class="unactionable">${data.name}</td>
	<td id="part_amount_${data.id}" class="unactionable">${data.amount}</td>
	<td id="part_add_${data.id}" ${data.canAdd?		
		`onclick="Ship_Menu.AddPartToShip('${data.id}')"`:
		`class="disabled"`}>+</td>
	<td id="part_remove_${data.id}" ${data.canRemove?		
		`onclick="Ship_Menu.RemovePartFromShip('${data.id}')"`:
		`class="disabled"`}>-</td>                        
</tr>
		`;
	},
	_ShipProp(data){
		return `
<div class="tooltip">
	<div>
		<div style="width:${data.percent}%">${data.amount}</div>
	</div>
	<div>${data.name}</div>
	<span class="tooltiptext">${LONG(data.id)}</span>
</div>
		`;
	},
	_ShipSkill(data){
		return `
${data.name?`
	<li class="tooltip">
		${data.name}
		<span class="tooltiptext">${data.text}</span>
	</li>
`:''}
		`;
	},
	Main(data){
		return`
<div class="l-main l-h l-h-two">
	<div class="l-v l-v-two-bot">
		<div class="c-editable-object">
			<div>
				<h1><input onchange="" value="${data.name}"></h1>			
				<div>	
					<button onclick="Ship_Menu.SaveShip()">✓</button>
					<button onclick="Ship_Menu.CancelShip()">✗</button>	
					<h3>${data.type}</h3>							
				</div>
			</div>
			<div>
				<h2>${TEXT('Ship skills')}</h2>
				<ul id="ship_skills">
					${data.skills.map(Templating.Ship._ShipSkill).join('')}
				</ul>
			</div>			
		</div>
		<div>						
			<div class="c-ship-size-buttons">
				<h2>${TEXT('Size')}</h2>
				<button onclick="Ship_Menu.SetShipSize(1)">1</button>
				<button onclick="Ship_Menu.SetShipSize(2)">2</button>
				<button onclick="Ship_Menu.SetShipSize(4)">4</button>
				<button onclick="Ship_Menu.SetShipSize(8)">8</button>
			</div>									
			<table id="ship_parts">
				${data.parts.map(Templating.Ship._ShipPart).join('')}
			</table>
		</div>
	</div>
	<div class="l-v">        
		<div class="l-canvas-container">    
			<canvas id="ship_canvas"></canvas>
		</div>
		<div class="bar_container" id="ship_props">
			${data.props.map(Templating.Ship._ShipProp).join('')}
		</div>
	</div>
</div>
		`;
	}
}

Templating.Battle = {
	FleetBlock(data){
		return`
<span style="
	background-color:${data.color};
	width: calc(${data.width}% - 6px);
	height: calc(${data.height}% - 6px);
	top: ${data.top}%;
	left: ${data.left}%;
"></span>		
		`;
	},
	_FleetInfoRow(data){
		return `
<tr><td>${data.name}</td><td>${data.amount}/${data.max}</td></tr>
		`;
	},
	FleetInfo(data){
		return `
<h2>${data.name}</h2>
<table>${data.rows.map(Templating.Battle._FleetInfoRow).join('')}</table>		
		`;
	},
	Main(){
		return`
<div class="l-main l-v l-v-canvas">
	<div class="l-canvas-container">    
		<canvas id="battle_canvas"></canvas>
	</div>		
	<div class="c-battle-orders">
		<div id="fleet1_info"></div>
		<div>
			<div id="battle_orders" class="c-battle-panel"></div>
		</div>
		<div id="fleet2_info"></div>
	</div>
</div>
		`;
	}
}
