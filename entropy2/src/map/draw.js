var t = 0, dt = 0.00002;

var Draw_Map = {
    _ColorMeshWithPlayerColor(mesh,player){
        mesh.material = MaterialsFactory.Mask(PLAYERS[player].Info.Color);
    },
    Map : function(){
        Draw._Init(map_canvas,Map_Events.Map,Draw_Map._MoveFleets);

        for(var key in GLOBAL.Map){
            Draw_Map._PaintMapMesh(GLOBAL.Map[key]);
        }

		for(var i=0;i<PLAYERS.length;i++){
			for(var j=0;j<PLAYERS[i].Worlds.length;j++){
                Draw_Map._ColorMeshWithPlayerColor(PLAYERS[i].Worlds[j].mesh,i);
			}			
		}		
		
        var light = new THREE.AmbientLight( 0x555555 );
        scene.add( light );
    },
    SelectMapObject : function(object){
        if(CURRENT.SelectedMesh) {
			if(CURRENT.SelectedMesh.obj.Player>=0){
                Draw_Map._ColorMeshWithPlayerColor(CURRENT.SelectedMesh,CURRENT.SelectedMesh.obj.Player);
		    }
			else{
				CURRENT.SelectedMesh.material = MaterialsFactory.select();
			}
		}			
        object.material = MaterialsFactory.selected();		
    },
	MoveCameraToObject(object){
		controls.target.set(object.obj.x,object.obj.y,object.obj.z);
    },
    _PaintMapMesh : function(obj){
        var mesh;
        switch(obj.type){
            case ENUM.MapObject.Planet: 
                mesh = AstronomyMeshFactory.BuildWorld(); break;
            case ENUM.MapObject.Asteroids:
                mesh = AstronomyMeshFactory.BuildField(); break;
            case ENUM.MapObject.Gas: 
                mesh = AstronomyMeshFactory.BuildGasCloud(); break;
            case ENUM.MapObject.Dust: 
                mesh = AstronomyMeshFactory.BuildDustCloud(); break;
            case ENUM.MapObject.Fleet: 
                mesh = AstronomyMeshFactory.BuildFleet(); 
                Draw_Map._ColorMeshWithPlayerColor(mesh,obj.player);
                break;
        }
        mesh.obj = obj;
		obj.mesh = mesh;
        mesh.position.x = obj.x;
        mesh.position.y = obj.y;
        mesh.position.z = obj.z;
        scene.add(mesh);
        return mesh;
    },
    
    //FLEETS

    _MoveFleets : function(){
        var fleets = CURRENT.MovingFleets;
        for(var i in fleets){
            if(fleets[i].position.distanceTo(new THREE.Vector3(
                    fleets[i].obj.objective.x,
                    fleets[i].obj.objective.y,
                    fleets[i].obj.objective.z))<0.1){
                Map_Events.FleetReachedObjective(i);
            }
            else{
                var curr = fleets[i].position;
                var destiny = fleets[i].obj.objective;	
                var origin = fleets[i].obj.origin;	

                var fps = 60;
                var speed = fleets[i].obj.speed;

                fleets[i].position = curr.lerpVectors(origin,destiny,fleets[i].obj.tick);
                fleets[i].obj.tick += 1 / (speed * fps);
            }		
        }
    },
    RemoveFleet : function(id){	
        if(GLOBAL.Screen!=ENUM.Screens.Map){
            return;
        }
        scene.remove(CURRENT.MovingFleets[id]);
        delete CURRENT.MovingFleets[id];	
    },
    DeployFleet : function(fleetId,originId,destinyId,player){
        if(GLOBAL.Screen!=ENUM.Screens.Map){
            return;
        }
        var origin = GLOBAL.Map[originId];
        var destiny = GLOBAL.Map[destinyId];

        var fleetData = PLAYERS[player].Fleets[fleetId];
        var routeDetails = FleetCalculator.GetFleetRouteDetails(fleetData,destiny,origin);

        var fleet = Factory.FleetInMap(fleetId,player,origin,destiny,routeDetails.Time);
        
        var mesh = Draw_Map._PaintMapMesh(fleet);		
        mesh.lookAt(destiny.x,destiny.y,destiny.z);

        CURRENT.MovingFleets[Map_Edit.GetFleetInMapId(player,fleetId)] = mesh;
    },
    ReturnFleet : function(id){	
        if(GLOBAL.Screen!=ENUM.Screens.Map){
            return;
        }
        var fleet = CURRENT.MovingFleets[id];
        var oldObjective = fleet.obj.objective;
        fleet.obj.objective = fleet.obj.origin;
        fleet.obj.origin = oldObjective;
        fleet.obj.retreat = true;
        fleet.obj.tick=0;        
        fleet.lookAt(fleet.obj.objective.x,fleet.obj.objective.y,fleet.obj.objective.z);	
    }, 
}