var Map_Edit = {	
	RemoveObject(obj){
		scene.remove(obj.mesh);
        delete GLOBAL.Map[obj.id];	
	},
	//FLEETS IN MAP
	GetFleetInMapId(player,fleetId){
		return player+'-'+fleetId;
	},

	DeployFleet : function(fleetId,player,origin,destiny,order){
		var fleet = PLAYERS[player].Fleets[fleetId];
		var routeDetails = FleetCalculator.GetFleetRouteDetails(fleet,GLOBAL.Map[destiny],GLOBAL.Map[origin]);

		Clock.AddEvent(routeDetails.Time,
			()=>Map_Edit.FleetReachedObjective(fleetId,player,origin,destiny,routeDetails));
		fleet.order = order;

		Draw_Map.DeployFleet(fleetId,origin,destiny,player);
		
		fleet.location = -1; //en route		
		Write.MapObject();
	},
	FleetReachedObjective(fleetId,player,origin,destiny,routeDetails){
		var fleet = PLAYERS[player].Fleets[fleetId];
		var objProps = GLOBAL.Map[destiny].props;
		var maxcargo = FleetCalculator.GetFleetCargo(fleet.ships,PLAYERS[player].Ships)

		if(objProps.Fleets.length>0){
			Level.Battle({Id:fleet,Player:player},objProps.Fleets[0]);
		}
		else if(fleet.order==ENUM.MapAction.Route){
			Map_Edit.FleetTake(fleet,GLOBAL.Map[destiny],maxcargo);
			Map_Edit.ReturnFleet(fleetId,player,origin,destiny,fleet.order,routeDetails);        
		}       
		else{
			objProps.Fleets.push({Id:fleetId,Player:player});
			Map_Edit.FleedRebased(fleet,destiny,player);
		}	
		Write.MapObject();
	},
	FleedRebased(fleet,baseId,player){
		Draw_Map.RemoveFleet(Map_Edit.GetFleetInMapId(player,fleet.id));
		fleet.location = baseId;
		fleet.base = baseId;
		fleet.order = ENUM.MapAction.None;
	},
	FleetReturned(fleetId,player,origin,destiny,order){
		var fleet = PLAYERS[player].Fleets[fleetId];
		if(fleet.order==ENUM.MapAction.Route){
			Draw_Map.RemoveFleet(Map_Edit.GetFleetInMapId(player,fleetId));
			Map_Edit.DeployFleet(fleetId,player,origin,destiny,order);
		}
		else if(fleet.order==ENUM.MapAction.Return){
			Map_Edit.FleedRebased(fleet,origin,player);
		}
		var objProps = GLOBAL.Map[origin].props;
		Map_Edit.FleetGive(fleet,objProps);		
	},
	ReturnFleet : function(fleetId,player,origin,destiny,order,routeDetails){
		Clock.AddEvent(routeDetails.Time,
			()=>Map_Edit.FleetReturned(fleetId,player,origin,destiny,order));
		Draw_Map.ReturnFleet(Map_Edit.GetFleetInMapId(player,fleetId));
	},
	FleetTake : function(fleet,objective,maxcargo){
		if(objective.props.matter<maxcargo){
			fleet.matter = objective.props.matter;

			Map_Edit.RemoveObject(objective);
			fleet.order=ENUM.MapAction.Return;
		}
		else{
			fleet.matter = maxcargo;
			objective.props.matter -= maxcargo;
		}		
	},
	FleetGive : function(fleet,objective){
		objective.matter += fleet.matter;
		fleet.matter = 0;		
	}
}