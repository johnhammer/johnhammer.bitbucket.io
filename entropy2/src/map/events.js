var Map_Events = {
    Map : function(event){
        Events._Raycast(event,Map_Events.SelectMapObject);
    },
    SelectMapObject : function(object){
        Draw_Map.SelectMapObject(object);
        CURRENT.SelectedMesh = object;
        Write.MapObject(object.obj);
    },
    FleetReachedObjective : function(i){
        if(GLOBAL.Screen!=ENUM.Screens.Map){
            return;
        }
        //delete CURRENT.MovingFleets[i];
    },
}