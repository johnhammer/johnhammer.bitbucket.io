var Map_Maker = {
/*
gamemode : details_gamemode.value, X
resources : details_resources.value, X
abundance : details_abundance.value,
warfog : details_warfog.value, /
explore : details_explore.value, / 
maptype : details_maptype.value, /
dispersion : details_dispersion.value X
*/

    Create : function(rnd,details){
        var area;
        switch(details.dispersion){
            case ENUM.MapDetails.Few: 
                area=8;
                break;
            case ENUM.MapDetails.Normal: 
                area=16;
                break;
            case ENUM.MapDetails.Many: 
                area=32;
                break;
        }

        var maxResources,maxWorlds;
        switch(details.resources){
            case ENUM.MapDetails.Few: 
                maxResources=1; 
                maxWorlds=1;
                break;
            case ENUM.MapDetails.Normal: 
                maxResources=2; 
                maxWorlds=2;
                break;
            case ENUM.MapDetails.Many: 
                maxResources=4; 
                maxWorlds=4;
                break;
        }

        var resmin,resmax;
        switch(details.abundance){
            case ENUM.MapDetails.Few: 
                resmin=16; 
                resmax=32;
                break;
            case ENUM.MapDetails.Normal: 
                resmin=32; 
                resmax=64;
                break;
            case ENUM.MapDetails.Many: 
                resmin=128; 
                resmax=256;
                break;
        }

        var planets = Random.Range(rnd,PLAYERS.length,PLAYERS.length+maxWorlds);
        var asteroids = Random.Range(rnd,PLAYERS.length,PLAYERS.length+maxResources*2);
        var dust = Random.Range(rnd,PLAYERS.length,PLAYERS.length+maxResources);
        var gas = Random.Range(rnd,PLAYERS.length,PLAYERS.length+maxResources);

        var map = {};
        Map_Maker._GenerateMapObjects(rnd,map,area,planets,ENUM.MapObject.Planet,resmin,resmax);
        Map_Maker._GenerateMapObjects(rnd,map,area,asteroids,ENUM.MapObject.Asteroids,resmin,resmax);
        Map_Maker._GenerateMapObjects(rnd,map,area,dust,ENUM.MapObject.Dust,resmin,resmax);
        Map_Maker._GenerateMapObjects(rnd,map,area,gas,ENUM.MapObject.Gas,resmin,resmax);
        return map;
    },
    _GenerateMapObjectProperties(rnd,type,resmin,resmax){
        var props;
        switch(type){
            case ENUM.MapObject.Planet: 
                props = {
                    matter : 100,
                    humans : 0,
                    debris : 0,
                    Fleets:[]
                };
                break;
            case ENUM.MapObject.Asteroids:
            case ENUM.MapObject.Gas: 
            case ENUM.MapObject.Dust: 
                var maxmatter = Random.Range(rnd,resmin,resmax);
                props = {
                    matter : maxmatter,
                    maxmatter : maxmatter,
                    Fleets:[]
                };
                break;
        }
        return props;
    },
    _GenerateMapObjects: function(rnd,map,area,amount,type,resmin,resmax){
        for(var i=0;i<amount;i++){	            
            var name,id;
            switch(type){
                case ENUM.MapObject.Planet: 
                    name = TEXT('Planet')+' A'; 
                    id = 'P';
                    break;
                case ENUM.MapObject.Asteroids:
                    name = TEXT('Asteroids')+' A'; 
                    id = 'A';
                    break;
                case ENUM.MapObject.Gas: 
                    name = TEXT('Dust cloud')+' A'; 
                    id = 'D';
                    break;
                case ENUM.MapObject.Dust: 
                    name = TEXT('Gas cloud')+' A'; 
                    id = 'G';
                    break;
            }
            var object = {
				id: id+i,
                x : Random.FloatArea(rnd,area),	
                y : Random.FloatArea(rnd,area),	
                z : Random.FloatArea(rnd,area),
                name : name+i,
                type : type,
                Player : -1,
                props : Map_Maker._GenerateMapObjectProperties(rnd,type,resmin,resmax)
            };
            map[id+i] = object;
        }        
    },

}