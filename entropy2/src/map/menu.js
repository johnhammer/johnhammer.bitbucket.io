var Map_Menu = {
	DeployFleet : function(fleet,order){
		var origin = PLAYERS[PLAYER_ID].Fleets[fleet].location;
		var objective = CURRENT.SelectedMesh.obj.id;		
		Orders.DeployFleet([fleet,origin,objective,order]);		
	},	
	AddFleet : function(){
		CURRENT.EditingFleet = Factory.Fleet(CURRENT.SelectedMesh.obj.id);
		CURRENT.EditingFleet.Temporal = true;
		var resources = Map_Menu._GetPlanetResources(CURRENT.SelectedMesh.obj);
		Level.Fleets();
	},
	EditFleet : function(fleet){
		CURRENT.EditingFleet = LIB.Clone(PLAYERS[PLAYER_ID].Fleets[fleet]);
		CURRENT.EditingFleet.Id = fleet;		
		var resources = Map_Menu._GetPlanetResources(CURRENT.SelectedMesh.obj);
		Level.Fleets();
	},
	_GetPlanetResources : function(obj){
		return obj.props.matter;
	},
	FocusCamera(){
		Draw_Map.MoveCameraToObject(CURRENT.SelectedMesh);
	}
}

