var AstronomyMeshFactory = {
	_WorldGeo : function(sz){
		var geo = new THREE.DodecahedronGeometry(sz, 1);
		var size = 1;
		geo.vertices.forEach(function(v){
			v.x += (0-Math.random()*(size/4));
			v.y += (0-Math.random()*(size/4));
			v.z += (0-Math.random()*(size/4));
		});
		for (var i = 0; i < geo.faces.length; i++) {
			face = geo.faces[i];
			face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
			face.faceid = i;
		} 
		geo.computeFaceNormals();
		return geo;		
	},
	BuildMeshSelector:function(sz,playerColor=false){
		var geometry2 = lib.sph(sz).m.geometry;
		var material2 = MaterialsFactory.select();
		
		if(playerColor){
			material2 = MaterialsFactory.Player();
		}
		
		var sphere2 = new THREE.Mesh(geometry2, material2);
		sphere2.isSelectableMesh = true;
		return sphere2;
	},
	BuildWorld:function(){
		var mesh = AstronomyMeshFactory.BuildMeshSelector(1);	

		var material = new THREE.MeshNormalMaterial();
		var geo = AstronomyMeshFactory._WorldGeo(1);
		var SHIPMESH = new THREE.Mesh(geo, material);
		var scale = .5;
		SHIPMESH.scale.set(scale,scale,scale)
		mesh.add(SHIPMESH);
		
		return mesh;
	},
	BuildField:function(){
		var mesh = AstronomyMeshFactory.BuildMeshSelector(1);	

		for(var i=0;i<25;i++){
			var material = new THREE.MeshNormalMaterial();
			var geo = lib.sph(.05).m.geometry;
			var SHIPMESH = new THREE.Mesh(geo, material);
			SHIPMESH.position.x += 0.4-0.8 * Math.random();
			SHIPMESH.position.y += 0.4-0.8 * Math.random();
			SHIPMESH.position.z += 0.4-0.8 * Math.random();
			
			var scale = 1-.5 * Math.random();
			SHIPMESH.scale.set(scale,scale,scale)
			mesh.add(SHIPMESH);
		}
		
		return mesh;
	},
	BuildGasCloud:function(){
		
		//STILL ARTIFACTS
		
		var size = 1.2;
		
		var geometry = new THREE.Geometry(); 
		var particleCount = 5; 
		
		for (i = 0; i < particleCount; i++) {

			var vertex = new THREE.Vector3();
			vertex.x = Math.random() * size - size/2;
			vertex.y = Math.random() * size - size/2;
			vertex.z = Math.random() * size - size/2;

			geometry.vertices.push(vertex);
		}

		var image = new THREE.TextureLoader().load( "https://bitbucket.org/johnhammer/johnhammer.bitbucket.io/raw/49f552ded8d25997b781a9ee99d8f990dff0b903/a/assets/gas.png" )
		
		var material = new THREE.PointsMaterial( {size: 2.5, map: image, blending: THREE.AdditiveBlending, depthTest: false,transparent: true, opacity:.5 } )

		particles = new THREE.Points(geometry, material);
		
		var mesh = AstronomyMeshFactory.BuildMeshSelector(size);	
		//mesh.renderOrder = 999;
		
		mesh.add(particles);

		return mesh;
	},
	BuildDustCloud:function(){
		var mesh = AstronomyMeshFactory.BuildMeshSelector(1);	
		
		var geometry = new THREE.Geometry(); 
		var particleCount = 50; 
		var size = 1;

		for (i = 0; i < particleCount; i++) {

			var vertex = new THREE.Vector3();
			vertex.x = Math.random() * size - size/2;
			vertex.y = Math.random() * size - size/2;
			vertex.z = Math.random() * size - size/2;

			geometry.vertices.push(vertex);
		}

		particles = new THREE.Points(geometry, new THREE.PointsMaterial({
			size: .01
		}));

		particles.rotation.x = Math.random() * 1;
		particles.rotation.y = Math.random() * 1;
		particles.rotation.z = Math.random() * 1;

		mesh.add(particles);
		
		return mesh;
	},
	BuildFleet:function(){
		var mesh = AstronomyMeshFactory.BuildMeshSelector(.5);	

		var material = new THREE.MeshNormalMaterial();
		var geo = lib.altcyl(.2,.5,0).m.geometry;
		var SHIPMESH = new THREE.Mesh(geo, material);
		var scale = .5;
		SHIPMESH.rotation.x = -Math.PI/2;
		SHIPMESH.scale.set(scale,scale,scale)
		mesh.add(SHIPMESH);
		
		return mesh;
	},

	
	
}