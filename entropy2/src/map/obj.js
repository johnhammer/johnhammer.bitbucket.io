var Map_Obj = {
    CanEdit(obj){
        return GLOBAL.Gamemode==ENUM.Gamemode.Normal
            && obj.Player == PLAYER_ID
            && obj.type==ENUM.MapObject.Planet;
    },
    CanAddFleet(obj){
        return obj.type==ENUM.MapObject.Planet
            && obj.Player == PLAYER_ID;
    },
    CanHaveOrdersObjectFleets(obj){
        return !(obj.type==ENUM.MapObject.Planet && obj.Player == PLAYER_ID);
    },
    CanHaveLocalObjectFleets(obj){
        return obj.type!=ENUM.MapObject.Planet
            && (obj.Player == PLAYER_ID || obj.Player == -1);
    },
    CanHaveOrdersFleets(obj){
        return obj.type==ENUM.MapObject.Planet 
            && obj.Player == PLAYER_ID;
    },
    CanHaveLocalFleets(obj){
        return obj.type==ENUM.MapObject.Planet 
            && obj.Player == PLAYER_ID;
    },
    CanHaveConstructionFleets(obj){
        return obj.type==ENUM.MapObject.Planet 
            && obj.Player == PLAYER_ID;
    },

}