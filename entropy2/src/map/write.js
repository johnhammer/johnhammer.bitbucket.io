var Write = {
    MapObject : function(obj=CURRENT.SelectedMesh.obj){	
		if(GLOBAL.Screen!=ENUM.Screens.Map){
            return;
		}
		if(GLOBAL.Map[obj.id]){
			map_object.innerHTML = 
				Templating.Map.MapObject(Mapping.MapObject(GLOBAL.Map[obj.id]));			
			map_fleets.innerHTML = 
				Templating.Map.MapFleets(Mapping.MapFleets(GLOBAL.Map[obj.id],PLAYERS[PLAYER_ID].Fleets));
		}
	}
}