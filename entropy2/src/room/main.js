
var Room = {
	OpenFactionModal : function(){
		Frontend.Modal(Templating.Room.Factions());
		for (var i = 0; i < FACTIONS.length; i++) { 
			var data = {
				id:i,
				name:FACTIONS[i].name,
				playerColor:PLAYERS[PLAYER_ID].Info.Color
			};
			factions.innerHTML += Templating.Room.FactionThumb(data)
		}
	},
	ShowFactionContent : function(faction){
		var data = {
			name:FACTIONS[faction].name,
			playerColor:PLAYERS[PLAYER_ID].Info.Color,
			id:faction
		};
		faction_info.innerHTML = Templating.Room.FactionDetail(data);
	},
	ChangePlayerName : function(name){
		PLAYERS[PLAYER_ID].Info.Name = name;
		Room.UpdatePlayers();
	},
	ChangePlayerColor : function(color){
		PLAYERS[PLAYER_ID].Info.Color = color;
		Room.UpdatePlayers();
	},
	ChangePlayerFaction : function(faction){
		PLAYERS[PLAYER_ID].Info.Civ = faction;
		Frontend.CloseModal();
		Style.SetTheme(faction);
		Room.UpdatePlayers();
	},

	UpdatePlayers : function(players=false){
		if(ISONLINEGAME){
			if(players){
				PLAYERS = players;
			}
			else{
				Protocol.Room(PLAYERS);
			}
		}
		Room.WritePlayers();
	},
	WritePlayers : function(){		
		players.innerHTML = PLAYERS.map((x,i)=>
			Templating.Room.PlayerRow(Mapping.RoomPlayer(x,i))
		).join('');			
	},
	Single : function(){
		ISONLINEGAME = false;
		
		this.AddHuman();
		PLAYER_ID = 0;
	},	
	Create : function(){
		this.AddHuman();	
		PLAYER_ID = 0;

		Online.Init();	
		Online.useServer = false;
		Online.CreateManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_allow);
		LIB.Toggle(room_start);
		LIB.Toggle(chat);
		LIB.Toggle(details);
	},
	Join : function(){
		Online.Init();	
		Online.JoinManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);	
		LIB.Toggle(chat);		
		LIB.Toggle(details);		
	},
	Allow : function(){
		Online.Allow();
	},
	StartGame : function(){	
		GLOBAL.rnd = Random.Create(GLOBAL.Seed);	
		if(ISONLINEGAME){
			Protocol.StartGame();
		}
		Level.Game();
	},
	AddPlayer : function(human){		
		Start.AddGlobalPlayer(human);		
		Room.WritePlayers();		
	},	
	AddHuman : function(){
		this.AddPlayer(true);
	},
	AddAI : function(){		
		this.AddPlayer(false);
		if(ISONLINEGAME){
			Protocol.Welcome();
		}
	},
	GetPlayers : function(){			
		return PLAYERS;
	},
	SetPlayers : function(playersData){		
		if(!PLAYER_ID){
			PLAYER_ID = playersData.length-1;
		}
		PLAYERS = playersData;
		Room.WritePlayers();
	},	
	ShowInfo : function(info){
		room_code.value = LIB.Encrypt(info);
	}
}
