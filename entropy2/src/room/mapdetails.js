ENUM.MapDetails = {
    Few:"0",
    Normal:"1",
    Many:"2"
}
ENUM.MapType = {
    Cloud:"0",
}

var MapDetails = {
    GetMapDetails() {
        return {
            gamemode : details_gamemode.value,
            resources : details_resources.value,
            abundance : details_abundance.value,
            warfog : details_warfog.checked,
            explore : details_explore.checked,
            maptype : details_maptype.value,
            dispersion : details_dispersion.value
        };
    }
}