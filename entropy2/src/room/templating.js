Templating.Players = function(players){
    var result = '';
	for (var i = 0; i < players.length; i++) { 
		result += Templating.Player(i);
	}
    return result;		
}

Templating.Player = function(id){
	var player = PLAYERS[id];
	var isMe = id == PLAYER_ID;
    return `
        <tr class="${isMe?'':'playerrow'}">
            <td>
				<input ${isMe?'':'disabled'} onchange="Room.ChangePlayerName(this.value)" value="${player.Info.Name}"></input>
			</td>		
            <td>${player.Info.Human?'':`<option disabled>IA</option>`}</td>
            <td>
				<input ${isMe?'':'disabled'} onchange="Room.ChangePlayerColor(this.value)" type="color" id="myColor" value="${player.Info.Color}">
			</td>
            <td>
				<select ${isMe?'':'disabled'} onmousedown="(function(e){ e.preventDefault(); })(event, this)" onclick="Room.OpenFactionModal()">
					<option>${Templating.Text(FACTIONS[player.Info.Civ].name)}</option>
				</select>
			</td>
        </tr>
    `;	
}

Templating.ChatMessage = function(nick,message,me){
    return `
        <div ${me?'class="chat_message_me"':''}>${nick}: ${message}</div>
    `;		
}

Templating.Room = function(){
    return `
	<div id="hamburger">M</div>
    <div class="l-main l-three">
        <div class="background-plain">
            <h1>New game</h1>
            <p id="room_info" style="display:none" >
                ${Templating.Text('Copy')}: <input id="room_code"></input>
            </p>
            <table id="players"></table>
            <button id="room_create" onclick="Room.Create()">${Templating.Text('Create')}</button>
            <button id="room_join"   onclick="Room.Join()">${Templating.Text('Join')}</button>
            
            <button style="display:none" id="room_add"   onclick="Room.AddAI()">+ ${Templating.Text('Add AI')}</button>
            <button style="display:none" id="room_allow" onclick="Room.Allow()">+ ${Templating.Text('Add Player')}</button>
            <button style="display:none" id="room_start" onclick="Room.StartGame()">${Templating.Text('Go!')}</button>
        </div>
        <div>
            <div class="background-plain">
                <h2>Map details</h2>
            </div>
            <div class="background-plain">
				<div style="display:none" id="chat">
					<h2>Chat</h2>
					<div id="chat_output"></div>
					<input id="chat_input">
					<input id="chat_submit" type="submit" onclick="Chat.Send()">
				</div>
            </div>
        </div>
    </div>
	
    `;
}
