var ShipCalculator = {
	_GetAmountOfPieces : function(s){
		return s.Weapon + s.Ammo + s.Shield + s.Engine + s.Fuel + s.Habitat + s.Radio + s.Computer;
	},

	GetFirePower : function(s){
		return s.Ammo*2 + ((s.Weapon>0) ? 1 : 0);
	},
	GetFireSpeed : function(s){
		return s.Weapon;
	},
	GetPrecision : function(s){
		return ((s.Weapon>0) ? (32+(s.Radio+1) * (s.Habitat+1) - s.Ammo*4) : 0);
	}, 
	GetElectricAttack : function(s){
		return (s.Habitat+1)*s.Computer*2 + s.Radio*s.Computer;
	},
	GetArmor : function(s){
		return s.Shield*3 + s.Size;
	}, 
	GetTripulation : function(s){
		return s.Habitat*4;
	},
	GetEvasion : function(s){
		return 64 + (s.Engine*3+1) * (s.Radio+1) * (s.Habitat*2+1) - (s.Size)*8;
	},
	GetCamouflage : function(s){
		return 16-s.Engine-s.Size+1;
	},
	GetSpeed : function(s){
		return 16+s.Engine*3-s.Size-ShipCalculator._GetAmountOfPieces(s)+1;
	}, 
	GetPerception : function(s){
		return s.Radio+1;
	},
	GetAutonomy : function(s){
		return 8+s.Fuel-s.Engine+1;
	}, 
	GetCargoCapacity : function(s){	
		return s.Size - ShipCalculator._GetAmountOfPieces(s);
	},

	GetFirePowerPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetFirePower(s),16);
	},		
	GetFireSpeedPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetFireSpeed(s),8);
	},
	GetPrecisionPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetPrecision(s),96);
	},
	GetElectricAttackPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetElectricAttack(s),40);
	},
	GetArmorPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetArmor(s),32);
	},
	GetTripulationPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetTripulation(s),32);
	},
	GetEvasionPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetEvasion(s),210);
	},
	GetCamouflagePercent : function(s){
		return LIB.Normalize(ShipCalculator.GetCamouflage(s),16);
	},
	GetSpeedPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetSpeed(s),25);
	},
	GetPerceptionPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetPerception(s),9);
	},
	GetAutonomyPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetAutonomy(s),17);
	},
	GetCargoCapacityPercent : function(s){
		return LIB.Normalize(ShipCalculator.GetCargoCapacity(s),8);
	},

	GetCost : function(s){	
		return s.Size + s.Size * ShipCalculator._GetAmountOfPieces(s);
	},
	GetTimeToBuild : function(s){	
		return s.Size*2 + s.Size * ShipCalculator._GetAmountOfPieces(s);
	}
}