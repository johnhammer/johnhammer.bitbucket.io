var Draw_Ship = {
	
    Ship : function(parts){
		ANIMATED = {
			Ships : [],
			Bullets : [],
			Explosions : []
		};
		
        Draw._Init(ship_canvas,null,Draw_Ship.RenderShip);

        var ship = Ship_MeshFactory.CreateShipMesh(parts);        
        scene.add( ship.mesh ); 
		
        var light = new THREE.AmbientLight( 0x555555 );
        scene.add( light );

        var lights = [];
		lights[ 0 ] = new THREE.PointLight( 0xffffff, 1, 0 );
		lights[ 1 ] = new THREE.PointLight( 0xffffff, 1, 0 );
		lights[ 2 ] = new THREE.PointLight( 0xffffff, 1, 0 );

		lights[ 0 ].position.set( 0, 200, 0 );
		lights[ 1 ].position.set( 100, 200, 100 );
		lights[ 2 ].position.set( - 100, - 200, - 100 );

		scene.add( lights[ 0 ] );
		scene.add( lights[ 1 ] );
		scene.add( lights[ 2 ] );
    },
    
	RenderShip : function(){
		//explosions
		for(var i = 0; i < ANIMATED.Explosions.length; i++){			
			if(ANIMATED.Explosions[i]){
				ANIMATED.Explosions[i].material.uniforms['time'].value += .009;
				if(ANIMATED.Explosions[i].material.uniforms['time'].value>1.5){
					scene.remove(ANIMATED.Explosions[i]);
					ANIMATED.Explosions[i] = null;
				}
			}			
		}
        //bullets
        for(var i = 0; i < ANIMATED.Bullets.length; i++){
            Draw_Ship.MoveBullet(ANIMATED.Bullets[i],i);
        }
        //ships
        for(var j = 0; j < ANIMATED.Ships.length; j++){    

            //cannons
            for(var i = 0; i < ANIMATED.Ships[j].Cannons.length; i++){ 
                var obj = ANIMATED.Ships[j].Cannons[i];
                if(obj.position.y>0.5){
                    if(!obj.WorldPos){
                        obj.WorldPos = new THREE.Vector3()
                            .setFromMatrixPosition( obj.matrixWorld );
                    }
                    Draw_Ship.ShotBullet(obj.WorldPos,obj.Rot);			
                    obj.incr = 2;
                }
                else if(obj.position.y<0 && obj.incr==2){				
                    obj.incr = 0;
                }                
                if(obj.incr == 1){			
                    obj.position.y+=0.019;
                }
                else if(obj.incr == 2){
                    obj.position.y-=0.08;
                }
            }
        }     
    },
	
	
	ShotCannons : function(ship){
		for (i = 0; i < ship.Mesh.ShipObj.Cannons.length; i++) { 
			ship.Mesh.ShipObj.Cannons[i].incr=1;
		}		
	},
	AimCannons : function(ship,objective){
		var a = new THREE.Vector3();
		a.setFromMatrixPosition( objective.matrixWorld );
		a.sub(ship.position);

		ship.lookAt(a);
		ship.rotateX(Math.PI/2);		

		for (var i = 0; i < ship.ShipObj.Cannons.length; i++) {			
			ship.ShipObj.Cannons[i].Rot = a.normalize();
		}
	}
}