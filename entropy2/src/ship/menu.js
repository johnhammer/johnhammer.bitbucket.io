var Ship_Menu = {
    AddPartToShip : function(part){
		CURRENT.EditingShip.parts[part]++;
		CURRENT.EditingShip.parts.Nada--;
		Level.Ships();
	},
	RemovePartFromShip : function(part){
		CURRENT.EditingShip.parts[part]--;
		CURRENT.EditingShip.parts.Nada++;
		Level.Ships();
	},
	SetShipSize : function(size){
		var nada = size-ShipCalculator._GetAmountOfPieces(CURRENT.EditingShip.parts);
		if(size<CURRENT.EditingShip.parts.Size){
			var isTemporal = CURRENT.EditingShip.Temporal;
			CURRENT.EditingShip = Factory.Ship();
			CURRENT.EditingShip.Temporal = isTemporal;
			Level.Ships();
		}
		else{
			CURRENT.EditingShip.parts.Size=size;
			CURRENT.EditingShip.parts.Nada=nada;
			Level.Ships();
		}		
	},
	SaveShip : function(){
		Orders.SaveShip([CURRENT.EditingShip]);
		CURRENT.EditingFleet.ships.push(0);
		Level.Fleets();
	},
	CancelShip : function(){
		Level.Fleets();
	},
}

