var Ship_MeshFactory = {
    CreateShipMesh : function(ship){
        var ShipObj = {
            Cannons : []
        };       

        var mesh;
		if(ship.Size == 8) mesh = Ship_MeshFactory.CreateShipMesh8(ship,ShipObj);
		if(ship.Size == 4) mesh = Ship_MeshFactory.CreateShipMesh4(ship,ShipObj);
		if(ship.Size == 2) mesh = Ship_MeshFactory.CreateShipMesh2(ship,ShipObj);
        if(ship.Size == 1) mesh = Ship_MeshFactory.CreateShipMesh1(ship,ShipObj);
        
        for(var i = 0; i < ShipObj.Cannons.length; i++){             
			//ShipObj.Cannons[i].position.y = Random.FloatArea(Random.Create(),0.5);
            ShipObj.Cannons[i].Rot = new THREE.Vector3( 0, 1, 0 );
        }
        
        MaterialsFactory.SetMeshMaterial(mesh,MaterialsFactory.ShipMaterial());

        ANIMATED.Ships.push(ShipObj);
        
		ShipObj.mesh = mesh;
		mesh.ShipObj = ShipObj;

        return ShipObj;
	},

    CreateShipMesh1 : function(ship,ShipObj){
		var sphereBody = ship.Shield + ship.Radio + ship.Habitat;
		
		var mesh = lib.assembleM([
			lib.altcyl(.4,.5,.5),
			lib.iif(lib.move(lib.skip(lib.multiRot(lib.littleWing(1),4+ship.Engine,1)),-.8),!ship.Radio),
			
			lib.iif(lib.skip(lib.move(lib.multiRot(lib.skew(lib.altcyl(.1,.2,4),12),4,2.35),-3)),ship.Radio),
			
			lib.altcyl(.8,1*lib.f(ship.Ammo,.5),2-sphereBody),
			
			lib.iif(lib.skip(lib.multi(lib.altcyl(.5,0,3),4,1.1)),ship.Weapon),
			lib.iif(lib.skip(lib.multiRot(lib.flip(lib.cyl(.3,1)),4,0.5)),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.65,2),7,.6)),ship.Fuel),
			
			lib.iif(lib.altcyl(1*lib.f(ship.Ammo,.5),.8,2),ship.Ammo + ship.Engine),			
			lib.iif(lib.sph(2),sphereBody),
			lib.iif(lib.cyl(1*lib.f(ship.Nada,.5),3),ship.Nada),
			
			lib.iif(lib.move(lib.tor(2.2,.5),-1.5),ship.Shield),	

			lib.iif(lib.move(lib.multiRot(lib.skew(lib.cyl(.3,2),8),4,1.5),-2.5),ship.Radio),
						
			lib.iif(lib.altcyl(.8,0,1.5),ship.Engine+ship.Weapon),
			
			lib.iif(lib.move(lib.sph(.8),-.5),ship.Ammo),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,.8),4,.6)),ship.Computer),
			lib.iif(lib.cyl(.3,1),ship.Computer)			
		]);	

		return mesh;
	},
	CreateShipMesh2 : function(parts,ShipObj){
		var ShipObj = {
            Cannons : []
        }; 		
		var r = .3;
        var rm = .2;
        var l = 1;		
		var cannon = lib.part(lib.assemble([			
            lib.altcyl(r*lib.f(parts.Ammo,.05),rm,rm),
            lib.altcyl(r*lib.f(parts.Ammo,.1),rm,rm),
			lib.cyl(rm,1.2*lib.f(parts.Ammo,.05)),
			lib.cyl(.1,1),
			lib.cyl(.12,.5),
		]));		
		var ship = lib.assembleM([		
			lib.altcyl(rm/2,rm,rm),
			lib.cyl(rm,rm),
			
			lib.skip(lib.move(lib.multiRot(lib.cyl(rm/2,.8),2+parts.Weapon,r+rm/2),.5-.8)),
			
			lib.cyl(r,l),
			
			lib.iifA(()=>lib.skip(lib.move(lib.multiRotM(cannon,2+parts.Weapon,r*3,ShipObj.Cannons),-.8)),parts.Weapon),            
            lib.iif(lib.skip(lib.move(lib.multiRot(lib.cyl(r,.8),2+parts.Weapon,r*3),.5-.8)),parts.Weapon),
			
			lib.cyl(l/1.5,rm),
			lib.cyl(r,r),
			lib.altcyl(r,rm,r),
			lib.altcyl(rm,0,l/1.5),
        ]);
		return ship;
	},
    CreateShipMesh4 : function(parts,ShipObj){ 
        var r = .3;
        var rm = .2;
        var l = 1;		
		var cannon1 = lib.part(lib.assemble([			
            lib.altcyl(r*lib.f(parts.Ammo,.05),rm,rm),
            lib.altcyl(r*lib.f(parts.Ammo,.1),rm,rm),
			lib.cyl(rm*lib.f(parts.Ammo,.05),1.2*lib.f(parts.Ammo,.2)),
			lib.cyl(.1*lib.f(parts.Ammo,.05),1.5*lib.f(parts.Ammo,.1)),
			lib.cyl(.12*lib.f(parts.Ammo,.05),.5),
		]));		
		var cannon2 = lib.part(lib.assemble([			
            lib.altcyl(r*lib.f(parts.Ammo,.05),rm,rm),
            lib.altcyl(r*lib.f(parts.Ammo,.1),rm,rm),
			lib.cyl(rm,1.2*lib.f(parts.Ammo,.05)),
			lib.cyl(.1,1),
			lib.cyl(.12,.5),
		]));		
		var ship = lib.assembleM([				
			//Engines
			lib.iif(lib.skip(lib.multiRot(
				lib.altcyl(rm/1.5*lib.f(parts.Engine,.2),r/1.5*lib.f(parts.Engine,.2),rm*lib.f(parts.Engine,.4)),
				2+parts.Engine,r*lib.f(parts.Engine,.5))),
			parts.Engine),
            lib.altcyl(rm*lib.f(parts.Engine,.5),r*lib.f(parts.Engine,.5),rm*lib.f(parts.Engine,.4)),
			lib.altcyl(r*lib.f(parts.Engine,.5),r*lib.f(parts.Engine,.1),l*lib.f(parts.Engine,.2)),
			
			//Fuel
            lib.cyl(rm,rm*lib.f(parts.Fuel,.2)),
            lib.altcyl(rm,r*lib.f(parts.Fuel,.2),l/2),
            lib.skip(lib.multi(lib.cyl(rm,l),5+parts.Fuel*2,rm*2*lib.f(parts.Fuel,.2))),            
            lib.cyl(r*lib.f(parts.Fuel,.2),l+rm),
            lib.cyl(rm,rm*lib.f(parts.Fuel,.2)), 
			
			//Human
            lib.cyl(r*lib.f(parts.Habitat,.2),l*lib.f(parts.Habitat,.3)),  			
			
			//Main weapons
			lib.iifA(()=>lib.skip(lib.move(lib.multiRotM(cannon1,4+parts.Weapon,l*2*lib.f(parts.Ammo,.02),ShipObj.Cannons),-.8)),parts.Weapon),            
            lib.iif(lib.skip(lib.move(lib.multiRot(lib.cyl(r*lib.f(parts.Ammo,.1),.8*lib.f(parts.Ammo,.2)),4+parts.Weapon,l*2*lib.f(parts.Ammo,.02)),.5-.8)),parts.Weapon),
            lib.cyl(l*2,l/3*lib.f(parts.Ammo,.5)),
			
			//Cargo
            lib.cyl(l,l/2),			
            lib.altcyl(l,rm,r),
			
			//Computer
            lib.cyl(rm,rm*lib.f(parts.Computer,.5)),
            lib.cyl(r,rm),
			
			//Radio
            lib.cyl(l*lib.f(parts.Radio,.2),r/2),
			
			//Shields
            lib.altcyl(r,l/3*2,l*lib.f(parts.Shield,.2)),     
            lib.altcyl(l/3*2,(parts.Ammo==4?(0):(l*lib.f(parts.Shield,.2))),l),   
			
			//Extra weapons
			lib.iifA(()=>lib.skip(lib.move(lib.multiRotM(cannon2,2+parts.Weapon,l,ShipObj.Cannons),-.8)),parts.Weapon,3),            
            lib.iif(lib.skip(lib.move(lib.multiRot(lib.cyl(r,.8),2+parts.Weapon,l),.5-.8)),parts.Weapon,3),			
        ]); 
        return ship;
    },
    CreateShipMesh8 : function(ship,ShipObj){
        var mesh = lib.assembleM([			
			lib.altcyl(.8*lib.f(ship.Engine,.1),.5*lib.f(ship.Engine,.1),1),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.iif(lib.multi(lib.cyl(.2,1),ship.Engine+1,.6),ship.Engine),	
			lib.iif(lib.skip(lib.multi(lib.sph(0.75),ship.Fuel*3,1*lib.f(ship.Fuel,.1))),ship.Fuel),
			lib.cyl(0.9*lib.f(ship.Fuel,.1),1.5*lib.f(ship.Nada,.1)),
			
			lib.skip(lib.multi(lib.part(this.Orion(ship),4),5,3)),
			lib.skip(lib.multiRot(lib.flip(lib.cyl(.5,3)),5,1)),			
			
			lib.iif(lib.skip(lib.tor(2.5,.2)),ship.Radio),	
			lib.iif(lib.skip(lib.multi(lib.cyl(.3,1.2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.cyl(1.2*lib.f(ship.Ammo,.1),3),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,1*lib.f(ship.Computer,.1)),4,.6)),ship.Computer),			
			lib.iif(lib.cyl(.3,0.5*lib.f(ship.Computer,.2)),ship.Computer),		
			lib.cyl(1,2*lib.f(ship.Habitat,.2)),
			lib.skip(lib.multi(lib.cyl(.2,1.8),2*(ship.Shield+1),.6)),
			lib.cyl(.5,3),		
			lib.altcyl(.5,0,.2*lib.f(ship.Engine,2))
        ]);
        return mesh;
    },
    Orion : function(ship){
		return lib.assemble([			
			lib.altcyl(.8*lib.f(ship.Engine,.1),.5*lib.f(ship.Engine,.1),1),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.iif(lib.multi(lib.cyl(.2,1),ship.Engine+1,.6),ship.Engine),	
			
			lib.iif(lib.skip(lib.multi(lib.sph(0.75),ship.Fuel*3,1*lib.f(ship.Fuel,.1))),ship.Fuel),
			lib.cyl(0.9*lib.f(ship.Fuel,.1),1.5*lib.f(ship.Nada,.1)),
			
			lib.iif(lib.skip(lib.tor(2.5,.2)),ship.Radio),	
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.3,1.2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.cyl(1.2*lib.f(ship.Ammo,.1),1.2),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,1*lib.f(ship.Computer,.1)),4,.6)),ship.Computer),			
			lib.iif(lib.cyl(.3,0.5*lib.f(ship.Computer,.2)),ship.Computer),		
						
			lib.cyl(1,1*lib.f(ship.Habitat,.2)),
			
			lib.skip(lib.multi(lib.cyl(.2,1.8),2*(ship.Shield+1),.6)),
			
			lib.cyl(.5,2),		
			
			lib.altcyl(.5,0,.2*lib.f(ship.Engine,2)),
		]);		
	},

}