/* Ship Names */

var ShipNameFactory = {
	GetShipName(faction,ship){
		var factionName = ShipNameFactory._GetFactionShipName(faction,ship);
		if(factionName) return factionName;
		
		return ShipNameFactory.GetShipTypeName(ship);	
	},
	GetShipTypeName(ship){
		switch(ship.Size){
			case 1: return ShipNameFactory._GetShipName1(ship);
			case 2: return ShipNameFactory._GetShipName2(ship);
			case 4: return ShipNameFactory._GetShipName4(ship);
			case 8: return ShipNameFactory._GetShipName8(ship);
		}
	},
	_GetFactionShipName(faction,ship){	
		var factionShips = FACTIONS[faction].ships;
		for (var i = 0; i < factionShips.length ; i++) { 
			if(ShipNameFactory._MatchShips(ship,factionShips[i])){
				return factionShips[i].name;
			}
		}
		return null;	
	},
	_MatchShips : function(s1,s2){
		return  s1.Weapon==s2.Weapon &&
				s1.Ammo==s2.Ammo &&
				s1.Shield==s2.Shield &&
				s1.Engine==s2.Engine &&
				s1.Fuel==s2.Fuel &&
				s1.Habitat==s2.Habitat &&
				s1.Radio==s2.Radio &&
				s1.Computer==s2.Computer &&
				s1.Size==s2.Size
	},
	_GetShipName1(count){
		if(count.Weapon==1) 	return TEXT('Fighter drone');
		if(count.Ammo==1) 		return TEXT('Bomb drone');
		if(count.Shield==1) 	return TEXT('Decoy drone');
		if(count.Engine==1) 	return TEXT('Missile');
		if(count.Fuel==1) 		return TEXT('Mother drone');
		if(count.Habitat==1) 	return TEXT('Explorer');
		if(count.Radio==1) 		return TEXT('Probe');
		if(count.Computer==1) 	return TEXT('EMP pulse');
								return TEXT('Cargo drone');
	},
	_GetShipName2(count){		
		if(count.Weapon==1 && count.Ammo==1) 		return TEXT('Fighter bomber');
		if(count.Habitat==1 && count.Ammo==1) 		return TEXT('Kamikaze');
		if(count.Habitat==1 && count.Computer==1) 	return TEXT('Hacker');
		if(count.Habitat==2) 						return TEXT('Small flagship');
		
		var str = [];
		
		if(count.Weapon>=1) 						str.push(TEXT('Fighter'));
		else if(count.Ammo>=1) 						str.push(TEXT('Bomb'));
		else if(count.Computer>=1) 					str.push(TEXT('Interceptor'));
		else if(count.Nada>=1) 						str.push(TEXT('Small cargo ship'));
		else if(count.Habitat>=1) 					str.push(TEXT('Explorer'));
		else if(count.Fuel>=1) 						str.push(TEXT('Small mother ship'));
		else if(count.Radio>=1) 					str.push(TEXT('Telescope'));
		else if(count.Engine>=1) 					str.push(TEXT('Large missile'));
		else if(count.Shield>=1) 					str.push(TEXT('Decoy ship'));
		
		return str.join(" ");
	},
	_GetShipName4(count){
	
		if(count.Habitat==1 && ShipNameFactory._IsProjectile(s)) 		return TEXT('Kamikaze pesado');
	
		var str = [];
		
		if(count.Habitat>=3) 											str.push(TEXT('Insignia'));
		else if(count.Weapon>=1 && count.Ammo>=1 && count.Shield>=1) 	str.push(TEXT('Crucero'));
		else if(count.Weapon>=3) 										str.push(TEXT('Fragata'));
		else if(count.Weapon>=2 && count.Ammo>=1) 						str.push(TEXT('Canonera'));
		else if(count.Weapon>=1 && count.Ammo>=2) 						str.push(TEXT('Destructor'));
		else if(count.Ammo>=3) 											str.push(TEXT('Bomba de antimateria'));
		else if(count.Weapon>=1 && count.Shield>=2) 					str.push(TEXT('Acorazado'));
		else if(count.Engine>=2 && count.Weapon>=1) 					str.push(TEXT('Caza pesado'));
		else if(count.Engine>=2 && count.Ammo>=1) 						str.push(TEXT('Misil pesado'));
		else if(count.Computer>=2 && count.Weapon>=1) 					str.push(TEXT('Corbeta mixta'));
		else if(count.Computer>=3) 										str.push(TEXT('Nave de guerra electronica'));
		else if(count.Computer>=2 && count.Radio>=1)					str.push(TEXT('Nave de guerra electronica'));
		else if(count.Computer>=1 && count.Radio>=2)					str.push(TEXT('Nave de guerra electronica'));
		else if(count.Shield>=3) 										str.push(TEXT('Nave escudo'));
		else if(count.Nada>=3) 											str.push(TEXT('Nave de carga'));
		else if(count.Fuel>=3) 											str.push(TEXT('Nodriza'));
		else if(count.Radio>=3) 										str.push(TEXT('Estacion espacial'));
		else if(count.Radio>=2 && count.Habitat>=1) 					str.push(TEXT('Estacion espacial'));
		else if(count.Weapon>=1 && count.Ammo>=1) 						str.push(TEXT('Nave de guerra'));
		else if(count.Computer>=2) 										str.push(TEXT('Corbeta de senales'));
		else if(count.Weapon>=2) 										str.push(TEXT('Corbeta'));
		else if(count.Radio>=2) 										str.push(TEXT('Nave Radio'));
		else if(count.Nada>=2) 											str.push(TEXT('Nave de carga'));
		else if(count.Habitat>=2) 										str.push(TEXT('Transporte'));
		else if(count.Engine>=3) 										str.push(TEXT('Misil'));	
		else 															str.push(TEXT('Hibrida'));
		
		return str.join(" ");
	},	
	_GetShipName8(count){
		var str = [];	
																		str.push(TEXT('Gran'));
		if(count.Habitat>=3) 											str.push(TEXT('Insignia'));
		else if(count.Weapon>=1 && count.Ammo>=1 && count.Shield>=1) 	str.push(TEXT('Crucero'));
		else if(count.Weapon>=3) 										str.push(TEXT('Fragata'));
		else if(count.Weapon>=2 && count.Ammo>=1) 						str.push(TEXT('Canonera'));
		else if(count.Weapon>=1 && count.Ammo>=2) 						str.push(TEXT('Destructor'));
		else if(count.Ammo>=3) 											str.push(TEXT('Bomba de antimateria'));
		else if(count.Weapon>=1 && count.Shield>=2) 					str.push(TEXT('Acorazado'));
		else if(count.Engine>=2 && count.Weapon>=1) 					str.push(TEXT('Caza pesado'));
		else if(count.Engine>=2 && count.Ammo>=1) 						str.push(TEXT('Misil pesado'));
		else if(count.Computer>=2 && count.Weapon>=1) 					str.push(TEXT('Corbeta mixta'));
		else if(count.Computer>=3) 										str.push(TEXT('Nave de guerra electronica'));
		else if(count.Computer>=2 && count.Radio>=1)					str.push(TEXT('Nave de guerra electronica'));
		else if(count.Computer>=1 && count.Radio>=2)					str.push(TEXT('Nave de guerra electronica'));
		else if(count.Shield>=3) 										str.push(TEXT('Nave escudo'));
		else if(count.Nada>=3) 											str.push(TEXT('Nave de carga'));
		else if(count.Fuel>=3) 											str.push(TEXT('Nodriza'));
		else if(count.Radio>=3) 										str.push(TEXT('Estacion espacial'));
		else if(count.Radio>=2 && count.Habitat>=1) 					str.push(TEXT('Estacion espacial'));
		else if(count.Weapon>=1 && count.Ammo>=1) 						str.push(TEXT('Nave de guerra'));
		else if(count.Computer>=2) 										str.push(TEXT('Corbeta de senales'));
		else if(count.Weapon>=2) 										str.push(TEXT('Corbeta'));
		else if(count.Radio>=2) 										str.push(TEXT('Nave Radio'));
		else if(count.Nada>=2) 											str.push(TEXT('Nave de carga'));
		else if(count.Habitat>=2) 										str.push(TEXT('Transporte'));
		else 															str.push(TEXT('Hibrida'));
		
		return str.join(" ");
	},

	//skills
	GetShipSkills(s,faction){
		var skills = [null,null,null];
	
		if(ShipNameFactory._GetFactionShipName(faction,s)) 							ShipNameFactory._SetSkill(skills,'Glory');
		if(s.Habitat>=2&&s.Radio>=1&&s.Computer>=1)									ShipNameFactory._SetSkill(skills,'Space lab');				
		if(s.Habitat>=3&&s.Fuel>=1)													ShipNameFactory._SetSkill(skills,'Colonizer');				
		if(s.Nada>=4&&s.Fuel>=2)													ShipNameFactory._SetSkill(skills,'Carrier');				
		if(ShipNameFactory._IsProjectile(s))										ShipNameFactory._SetSkill(skills,'Projectile');				
		if(ShipNameFactory._IsProjectile(s)&&s.Size<=4&&s.Habitat>=1&&s.Ammo>=1)	ShipNameFactory._SetSkill(skills,'Kamikaze');				
		if(ShipNameFactory._IsProjectile(s)&&s.Size<=2&&s.Engine>0)					ShipNameFactory._SetSkill(skills,'Missile');				
		if(ShipNameFactory._IsProjectile(s)&&s.Ammo>0)								ShipNameFactory._SetSkill(skills,'Bomb');					
		if(ShipNameFactory._IsProjectile(s)&&s.Ammo>0&&s.Fuel>0)					ShipNameFactory._SetSkill(skills,'Dirty bomb');				
		if(s.Size<=2&&s.Weapon+s.Nada+s.Radio==0&&s.Computer>0)	ShipNameFactory._SetSkill(skills,'EMP');					
		if(s.Size<=2&&s.Radio>=1)								ShipNameFactory._SetSkill(skills,'Espionage');				
		if(s.Ammo>=6)		                                    ShipNameFactory._SetSkill(skills,'Planet buster');			
		if(s.Ammo>=4)											ShipNameFactory._SetSkill(skills,'Armor piercer');			
		if(s.Shield>=4)											ShipNameFactory._SetSkill(skills,'Flying bunker');			
		if(s.Computer>=4)										ShipNameFactory._SetSkill(skills,'Neural center');			
		if(s.Radio>=4)											ShipNameFactory._SetSkill(skills,'Big ear');					
		if(s.Weapon+s.Habitat>1&&s.Weapon==s.Habitat)			ShipNameFactory._SetSkill(skills,'Intelligent fire');			
		if(s.Weapon>=4)											ShipNameFactory._SetSkill(skills,'Rapid fire');				
		if(s.Weapon>=2&&s.Radio>=2)								ShipNameFactory._SetSkill(skills,'Flak AA');					
		if(s.Habitat>=2&&s.Computer>=2)							ShipNameFactory._SetSkill(skills,'Hackathon');				
		if(s.Weapon>=2&&s.Computer>=2)							ShipNameFactory._SetSkill(skills,'Antiradiation gun');		
		if(s.Weapon>=1&&s.Habitat>=1&&s.Ammo>=1)				ShipNameFactory._SetSkill(skills,'Artillery');				
		if(s.Weapon>=1&&s.Radio>=1&&s.Ammo>=1)					ShipNameFactory._SetSkill(skills,'Precision bombardment');	
		if(s.Computer>=1&&s.Habitat>=1)							ShipNameFactory._SetSkill(skills,'Hacker');					
		if(s.Weapon>=1&&s.Ammo>=1)								ShipNameFactory._SetSkill(skills,'Bombardment');			
	
		return skills;
	},
	_SetSkill(skills,skill){
		if(!skills[0]){
			skills[0] = skill;
		}
		else if(!skills[1]){
			skills[1] = skill;
		}
		else if(!skills[2]){
			skills[2] = skill;
		}
	},	
	_IsProjectile(ship){
		return 	ship.Weapon +
				ship.Shield +
				ship.Radio + 
				ship.Nada + 
				ship.Computer == 0;
	},
}





