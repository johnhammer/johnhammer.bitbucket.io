Templating.Technologies = function(){	
	return `
        <div class="l-grid">
            <div>
                ${Templating.TechnologiesList()}
            </div>
            <div id="faction_info">
                ${Templating.TechnologyInfo(PLAYER_ID,PLAYERS[PLAYER_ID].Info.Civ)}
            </div>
        </div>
    `;	
}
