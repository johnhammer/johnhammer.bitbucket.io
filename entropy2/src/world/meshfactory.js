var BuildingMeshFactory = {
	World : function(sz){
		var geo = new THREE.DodecahedronGeometry(sz, 1);
		var size = 1;
		geo.vertices.forEach(function(v){
			v.x += (0-Math.random()*(size/4));
			v.y += (0-Math.random()*(size/4));
			v.z += (0-Math.random()*(size/4));
		});
		for (var i = 0; i < geo.faces.length; i++) {
			face = geo.faces[i];
			face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
			face.faceid = i;
		} 
		geo.computeFaceNormals();
		return geo;		
	},
	Mine : function(amt){	
		return lib.assemble([
			lib.multi(lib.altcyl(.5,0,2),amt,1),
			lib.skip(lib.multiRot(lib.flip(lib.cyl(.2,1)),amt,.5)),
			lib.multi(lib.altcyl(.3,.3,.7),amt,1)
		]);	
	},
	Plant : function(amt){	
		return lib.assemble([
			lib.skip(lib.move(lib.sph(.8),.8)),
			lib.multi(lib.cyl(.3,2),amt,1),
			lib.skip(lib.move(lib.multiRot(lib.flip(lib.cyl(.2,1)),amt,.5),-1))
		]);			
	},
	House : function(amt){	
		return lib.assemble([
			lib.multiRot(lib.prism(.6,amt,2),amt,1),
			lib.skip(lib.move(lib.multiRot(lib.flip(lib.prism(.2,3,1)),amt,.5),-1))
		]);			
	},
	Recycler : function(amt){	
		return lib.assemble([
			lib.prism(2,amt,3)
		]);			
	},
	Factory : function(amt){	
		return lib.assemble([
			lib.cyl(2,4),	
			lib.move(lib.skip(lib.multiRot(lib.skew(lib.cyl(.2,1.5),5),amt,1)),-.1),
			lib.cyl(1,1.5)
		]);			
	},
	Lab : function(amt){	
		return lib.assemble([
			lib.skip(lib.multi(lib.cyl(.2,5),amt,1)),			
			lib.cyl(.6,5),	
			lib.move(lib.skip(lib.multiRot(lib.skew(lib.cyl(.2,1),3),amt,.6)),-.3),
			lib.move(lib.altcyl(.6,0,1),-.1)
		]);			
	},
	Hangar : function(amt){	
		return lib.assemble([
			lib.cyl(2,5),	
			lib.multi(lib.cyl(.3,1),amt,1)
		]);			
	},
	Bunker : function(amt){	
		return lib.assemble([
			lib.sph(2),
			lib.move(lib.skip(lib.multiRot(lib.skew(lib.cyl(.2,1),3),amt,1.8)),-.3)
		]);			
	},
	
}