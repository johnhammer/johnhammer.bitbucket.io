
Frontend.Styleguide = function(){
		var data = {};

		styleguide_main.innerHTML = Templating.Main.Main();

		data = {
			isOnline:false,
			players:[
				{
					name:'Player',
					color:'#FF00FF',
					factionName:'Empire',
					ai:false,
					me:true
				},
				{
					name:'Lancelot',
					color:'#FFFF00',
					factionName:'Aliance',
					ai:true,
					me:false
				},
				{
					name:'Lancelot',
					color:'#FFFF00',
					factionName:'Aliance',
					ai:true,
					me:false
				}
			]
		}
		styleguide_room.innerHTML = Templating.Room.Main(data);
		var elms = document.querySelectorAll("[id='players']"); //todo
		elms[0].innerHTML = data.players.map(Templating.Room.PlayerRow).join('');

		data = {
			isOnline:true,
			players:[
				{
					name:'Player',
					color:'#FF00FF',
					factionName:'Empire',
					ai:false,
					me:true
				},
				{
					name:'Human',
					color:'#FFFF00',
					factionName:'Aliance',
					ai:false,
					me:false
				},
				{
					name:'Lancelot',
					color:'#FFFF00',
					factionName:'Aliance',
					ai:true,
					me:false
				}
			]
		}
		styleguide_room_online.innerHTML = Templating.Room.Main(data);
		var elms = document.querySelectorAll("[id='players']"); //todo
		elms[1].innerHTML = data.players.map(Templating.Room.PlayerRow).join('');

		data = {
			object :{
				name:'Milklagard',
				kingdomColor:'blue',
				canAddFleet:true,
				canEdit:true,
				resources:{//planet only
					matter:128,
					debris:64,
					humans:32
				}
			},
			fleets : {
				orders:[
					{
						name:'Hakenkreuz',
						time:'20s',
						fuel:'10T'	
					},
					{
						name:'Valhalla',
						time:'22s',
						fuel:'9T'	
					}
				],
				local:[
					{
						id:0,
						name:'Zitterbewehgung',
						time:'20s',
						fuel:'10T',
						isHere: true
					},
					{
						id:1,
						name:'Hirohito',
						time:'20s',
						fuel:'10T',
						isHere: false
					}
				],	
				construction:[
					{
						name:'Hakenkreuz II',
						time:'40s'
					}
				],
			}
		}
		styleguide_map_planet.innerHTML = Templating.Map.Main();
		Frontend.SgSelect('map_object',0).innerHTML = 
			Templating.Map.MapObject(data.object);			
		Frontend.SgSelect('map_fleets',0).innerHTML = 
			Templating.Map.MapFleets(data.fleets);
		
		data = {
			object :{
				name:'Cloud 1',
				kingdomColor:'blue',
				matter:{
					matter:120,
					max:128
				}
			},
			fleets : {
				ordersObject:[
					{
						name:'Hakenkreuz',
						time:'20s',
						fuel:'10T',
						disableArrow1:true,
						disableArrow2:false
					},
					{
						name:'Valhalla',
						time:'22s',
						fuel:'9T',
						disableArrow1:false,
						disableArrow2:true
					}
				],
				localObject:[
					{
						name:'Zitterbewehgung',
						time:'20s',
						fuel:'10T'
					},
					{
						name:'Hirohito',
						time:'20s',
						fuel:'10T'
					}
				]
			}
		}
		styleguide_map_resource.innerHTML = Templating.Map.Main();
		Frontend.SgSelect('map_object',1).innerHTML = 
			Templating.Map.MapObject(data.object);			
		Frontend.SgSelect('map_fleets',1).innerHTML = 
			Templating.Map.MapFleets(data.fleets);

		data = {
			name:'Amon Ra',
			cost:'158',
			costmax:'1028',
			time:'10s',
			ships:[
				{
					name:'Fighter drone',
					id:0,
					amount:20,
					disablePlus:true,
					disableMinus:false
				},	
				{
					name:'Cargo ship',
					id:1,
					amount:0,
					disablePlus:false,
					disableMinus:true
				},	
			],
			details:{}
		};		
		styleguide_fleet.innerHTML = Templating.Fleet.Main(data);
		fleet_ships.innerHTML = data.ships.map(Templating.Fleet.FleetShip).join('')	
		fleet_details.innerHTML = Templating.Fleet.FleetDetails(data.details);

		data = {
			parts : [
				{
					id:0,
					name:'Arma',
					amount:5,
					canAdd:true,
					canRemove:false
				},
				{
					id:1,
					name:'Municion',
					amount:3,
					canAdd:false,
					canRemove:true
				},
			],
			props : [
				{
					percent:70,
					amount:45,
					name:'Fire speed'
				},
				{
					percent:50,
					amount:60,
					name:'Fire power'
				},
			],
			skills : [
				{
					name:'Colonize',
					text:'Tighinn air a mhuir tha fear a phòsas mi Tighinn air a mhuir tha fear a phòsas mi'
				},
				{
					name:'Rapid fire',
					text:'short'
				},
			],
			name:'Yamato',
			type:'Frigate'
		};
		styleguide_ship.innerHTML = Templating.Ship.Main(data);
		
		data = {
			fleet1 : {
				name:'Vanguard',
				rows:[
					{
						name:'Fighter drone',
						amount:10,
						max:12
					},
					{
						name:'Decoy drone',
						amount:5,
						max:5
					},
				]
			},
			fleet2 : {
				name:'Zitterbewehgung',
				rows:[
					{
						name:'?',
						amount:'?',
						max:'?'
					}
				]
			},
			blocks : [
				
			],
		};
		styleguide_battle.innerHTML = Templating.Battle.Main();
		fleet1_info.innerHTML = Templating.Battle.FleetInfo(data.fleet1);
		fleet2_info.innerHTML = Templating.Battle.FleetInfo(data.fleet2);		
		battle_orders.innerHTML = data.blocks.ships.map(Templating.Battle.FleetBlock).join('')	
}
Frontend.SgSelect = function(id,num){
	var elms = document.querySelectorAll("[id='"+id+"']");
	return elms[num];
}
Frontend.StyleguideSelectFaction = function(){
	custom_style.href = `faction${styleguide_faction.value}.css`;
	//Style.SetTheme(styleguide_faction.value);
}
