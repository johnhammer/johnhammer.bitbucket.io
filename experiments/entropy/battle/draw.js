var Draw_Battle = {
    PickObjective : function(fleet){
        return ANIMATED.Ships[2];
		return fleet.Ships[0];
    },
    Shot : function(ship,ticksToShot){
        var shipObj = ANIMATED.Ships[ship];
        if(!shipObj.Info.Objective){
            shipObj.Info.Objective = Draw_Battle.PickObjective(shipObj.Info.Enemy);
            Draw_Ship.AimCannons(shipObj,shipObj.Info.Objective.mesh);
        }
        Draw_Ship.ShotCannons(shipObj);
        Clock.AddEvent(ticksToShot,()=>Draw_Battle.Shot(ship,ticksToShot));
    },
    Battle : function(f1,f2){
        Draw._Init(battle_canvas,null,Draw_Ship.RenderShip);

        f1.Ships = [];
        f2.Ships = [];

        var animationId = 0;
        var m1 = Draw_Battle.Fleet(f1,false,f2,animationId);
        var m2 = Draw_Battle.Fleet(f2,true,f1,animationId);

        scene.add(m1);
        scene.add(m2);

        var light = new THREE.AmbientLight( 0xFFFFFF );
        scene.add( light );        
    },
    Fleet : function(f,rotate,objective,animationId){
        var mesh = new THREE.Mesh();

        var types = PLAYERS[f.Player].Ships;
        var ships = PLAYERS[f.Player].Fleets[f.Id].ships;

        var area = 15;

        for (i = 0; i < ships.length; i++) { 
            var type = types[i];

            for (j = 0; j < ships[i]; j++) { 
                var ship = Draw_Ship.CreateShipMesh(type.parts);
            
                ship.mesh.position.x = area-(area*2) * Math.random();	
                ship.mesh.position.y = area-(area*2) * Math.random();	
                ship.mesh.position.z = area-(area*2) * Math.random();
                
                const id = animationId;
                ship.Info = {
                    Type:i,
                    Player:f.Player,
                    Enemy:objective,
                    Id : id
                };

                var ticksToShot = 1;                
                Clock.AddEvent(ticksToShot,()=>Draw_Battle.Shot(id,ticksToShot));
                animationId++;

                mesh.add(ship.mesh);
                f.Ships.push(ship);
            }
        }	

        if(rotate){
            mesh.rotation.z = -Math.PI;
            mesh.position.y = +100;
        }

        return mesh;
    },
}