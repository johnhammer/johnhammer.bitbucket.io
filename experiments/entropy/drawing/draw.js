var Draw = {
	_RenderCallback : null,
    _Init: function(canvas,mouseEvent=null,renderCallback=null){
        var w = canvas.offsetWidth;
        var h = canvas.offsetHeight;
        
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(50, w / h, 0.1, 1000);
        renderer = new THREE.WebGLRenderer({canvas: canvas});
        controls = new THREE.OrbitControls( camera );
        
        renderer.setSize(w, h);
        camera.position.z = 10;
	
        if(mouseEvent){
            canvas.addEventListener( 'mousedown', mouseEvent, false );   
            raycaster = new THREE.Raycaster();
            mouse = new THREE.Vector2();
        }
		else{
			canvas.addEventListener( 'mousedown', function(){}, false );
		}
		Draw._RenderCallback = renderCallback;
    },
    Render : function(){
        var fps = 60;
        setTimeout(function() {     
            if(Draw._RenderCallback) {
                Draw._RenderCallback();
            }    
            requestAnimationFrame(Draw.Render);
            controls.update();
            renderer.render(scene, camera);      
        }, 1000 / fps);               
    }
}