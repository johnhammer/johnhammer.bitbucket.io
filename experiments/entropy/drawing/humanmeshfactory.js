/*
'Empire':'卐 Imperio',		
'Union':'☭ Union ',
	'League':'Liga',
	'Califate':'☪︎ Califato',	
	'Republic':'Ⅲ Republica',
'Dinasty':'☬ Dinastia',
	'Serpent':'🐉 Serpiente',
	'Crown':'✖ Corona',
	'Corporation':'● Corporacion',
	'Confederation':'◥ Confederacion',		
	'States':'◯ Estados',							
	'Colonies':'✚ Colonias',		
	'Aliance':'☦ Alianza',		
	'Khanate':'☾ Khanato',			
	'Earldoms':'◊ Condados',		
	'Kingdom':'☥ Reino',	
*/

var HumanMeshFactory = {
	Empire : function(rnd){
		var material = MaterialsFactory.ShipMaterial();
		var headmaterial = MaterialsFactory.MirrorMaterial();
		var ship = new THREE.Mesh();

		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		var visor = new THREE.CylinderGeometry(1,1,.1,4,1,false,Math.PI/4,Math.PI/2);
		var lathe = new THREE.LatheGeometry([
			new THREE.Vector2(1.3,-.5),
			new THREE.Vector2(1.1,0),
			new THREE.Vector2(1,.5),
			new THREE.Vector2(.8,1),
		],8,Math.PI/4*3,Math.PI/2*3);
		var breather = new THREE.CylinderGeometry(.5,.5,.5,5);
		var maskcyl = new THREE.CylinderGeometry(.5,.5,.5,8);
		var tube = new THREE.CylinderGeometry(.2,.2,.8,8);
		var pad = new THREE.LatheGeometry([			
			new THREE.Vector2(.4,0),
			new THREE.Vector2(.44,.1),
			new THREE.Vector2(.4,.2),
		],8,-Math.PI/8,Math.PI/4);
		var undermask = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,Math.PI/2,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, headmaterial );
		mesh.scale.z = .8;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/16;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( visor, material );
		mesh.position.y = .3;
		mesh.position.x = .2;
		mesh.rotation.z = -Math.PI/32;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( lathe, material );
		mesh.position.y = -.4;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( breather, material );
		mesh.rotation.z = -Math.PI/2;
		mesh.rotation.x = -Math.PI/10;
		mesh.position.y = -.55;
		mesh.position.x = .75;
		
		ship.add( mesh );
		
		mesh = new THREE.Mesh( undermask, material );
		mesh.rotation.z = -Math.PI/6;
		ship.add( mesh );
		
		return ship;
	},
	Union : function(){
		var material = MaterialsFactory.ShipMaterial();
		var headmaterial = MaterialsFactory.MirrorMaterial();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16,0,Math.PI*2,0,Math.PI/1.6);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/1.8);
		var earphone = new THREE.CylinderGeometry(.4,.4,.2,8);
		var disk = new THREE.CylinderGeometry(1,.9,.5,8);
		var lathe = new THREE.LatheGeometry([
			new THREE.Vector2(1,0),
			new THREE.Vector2(.8,.5),
			new THREE.Vector2(1,1),
		]);
		var bigdisk = new THREE.CylinderGeometry(1,1,1,8);			
		
		var mesh = new THREE.Mesh( head, headmaterial );			
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/4.5;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( earphone, material );
		mesh.rotation.x = Math.PI/2;
		mesh.position.z = .9;
		mesh.position.y = -.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( earphone, material );
		mesh.rotation.x = Math.PI/2;
		mesh.position.z = -.9;
		mesh.position.y = -.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( disk, material );
		mesh.rotation.z = -Math.PI/4;
		mesh.position.y = -.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( lathe, material );
		mesh.position.y = -1.5;
		ship.add( mesh );
		
		return ship;
	},
	Dinasty : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16);
		
		var hair = new THREE.CylinderGeometry(.6,.6,1,8);
		var beard = new THREE.CylinderGeometry(.7,.7,1,8);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.y = .4;
		mesh.scale.z = .9;
		mesh.position.x = .23;
		mesh.position.y = .18;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( beard, material );
		mesh.scale.x = .7;
		mesh.position.x = .5;
		mesh.position.y = -.8;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( hair, material );
		mesh.rotation.x = Math.PI/2;			
		mesh.position.y = 1;
		ship.add( mesh );
		
		return ship;
	},		
	Crown : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var horn = new THREE.SphereGeometry(.9,16,16,0,Math.PI*2,0,Math.PI/2);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI,0,Math.PI/2);
		var guard = new THREE.CylinderGeometry(1,1,.1,16,Math.PI);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.y = Math.PI;
		mesh.rotation.z = -Math.PI/6;
		mesh.rotation.x = -Math.PI/16;
		mesh.position.z = 0.1;		
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/6;
		mesh.rotation.x = Math.PI/16;
		mesh.position.z = -0.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( horn, material );
		mesh.scale.z = .3;
		mesh.scale.x = 1.1;
		mesh.scale.y = 1.5;
		mesh.rotation.z = Math.PI/6;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( guard, material );
		mesh.rotation.y = Math.PI;
		mesh.rotation.z = -Math.PI/6;
		mesh.rotation.x = Math.PI/16;
		mesh.position.z = 0.1;	
		ship.add( mesh );
		
		mesh = new THREE.Mesh( guard, material );
		mesh.rotation.z = Math.PI/6;
		mesh.rotation.x = -Math.PI/16;
		mesh.position.z = -0.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.y = Math.PI;
		mesh.rotation.z = -Math.PI+Math.PI/10;
		mesh.rotation.x = Math.PI/16;
		mesh.position.z = 0.1;		
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI-Math.PI/10;
		mesh.rotation.x = -Math.PI/16;
		mesh.position.z = -0.1;
		ship.add( mesh );
		
		return ship;
	},
	League : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Califate : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Republic : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16);
		
		var hair = new THREE.CylinderGeometry(.6,.6,1,8);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.y = .6;
		mesh.scale.z = .9;
		mesh.position.x = .18;
		mesh.position.y = .18;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		mesh.scale.z = .9;
		ship.add( mesh );

						
		mesh = new THREE.Mesh( hair, material );
		mesh.rotation.x = Math.PI/2;			
		mesh.position.y = 1;
		ship.add( mesh );
		
		return ship;
	},
	Serpent : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		var undermask = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,Math.PI/2,Math.PI/2);
		var earphone = new THREE.CylinderGeometry(.4,.4,.4,8);
		var breather = new THREE.CylinderGeometry(.5,.5,.5,6);
		var earring = new THREE.CylinderGeometry(.1,.1,.5,4);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		mesh.scale.x = 1.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( undermask, material );
		mesh.rotation.z = -Math.PI/6;
		mesh.scale.x = 1.1;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( earphone, material );
		mesh.rotation.x = Math.PI/2;
		mesh.position.z = .9;
		mesh.position.y = -.1;
		ship.add( mesh );
		mesh = new THREE.Mesh( earring, material );
		mesh.position.z = 1;
		mesh.position.y = -.7;
		ship.add( mesh );
		mesh = new THREE.Mesh( earring, material );
		mesh.position.z = .9;
		mesh.position.y = -.6;
		mesh.position.x = -.25;
		ship.add( mesh );
		mesh = new THREE.Mesh( earring, material );
		mesh.position.z = .9;
		mesh.position.y = -.6;
		mesh.position.x = .25;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( earphone, material );
		mesh.rotation.x = Math.PI/2;
		mesh.position.z = -.9;
		mesh.position.y = -.1;
		ship.add( mesh );		
		mesh = new THREE.Mesh( earring, material );
		mesh.position.z = -1;
		mesh.position.y = -.7;
		ship.add( mesh );
		mesh = new THREE.Mesh( earring, material );
		mesh.position.z = -.9;
		mesh.position.y = -.6;
		mesh.position.x = -.25;
		ship.add( mesh );
		mesh = new THREE.Mesh( earring, material );
		mesh.position.z = -.9;
		mesh.position.y = -.6;
		mesh.position.x = .25;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( breather, material );
		mesh.rotation.z = -Math.PI/2;
		mesh.position.y = -.55;
		mesh.scale.z = .7;
		mesh.position.x = .75;
		ship.add( mesh );
		
		return ship;
	},
	Corporation : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Confederation : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	States : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Colonies : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Aliance : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(.98,16,16,0,Math.PI,0,Math.PI/2);
		var undermask = new THREE.SphereGeometry(.98,16,16,0,Math.PI,Math.PI/2,Math.PI/2);
		var nex = new THREE.BoxGeometry(.2,.4,.4);
		var backhead = new THREE.CylinderGeometry(.7,.7,.4,8);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.y = Math.PI/2;		
		mesh.scale.z = .6;
		mesh.position.x = .35;
		mesh.position.y = .15;
		ship.add( mesh );		
		
		mesh = new THREE.Mesh( undermask, material );
		mesh.rotation.y = Math.PI/2;
		mesh.scale.z = .6;
		mesh.position.x = .35;		
		mesh.position.y = -.15;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( nex, material );		
		mesh.position.x = .8;	
		ship.add( mesh );
		
		mesh = new THREE.Mesh( backhead, material );	
		mesh.rotation.z = Math.PI/2;		
		mesh.position.x = -.7;	
		ship.add( mesh );
		
		
		return ship;
	},
	Khanate : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Earldom : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	Kingdom : function(){
		var material = MAKEMATERIAL();
		var ship = new THREE.Mesh();
	
		var head = new THREE.SphereGeometry(.9,16,16);
		var helmet = new THREE.SphereGeometry(1,16,16,0,Math.PI*2,0,Math.PI/2);
		
		var mesh = new THREE.Mesh( head, new THREE.MeshNormalMaterial() );
		mesh.scale.z = .9;
		ship.add( mesh );
		
		mesh = new THREE.Mesh( helmet, material );
		mesh.rotation.z = Math.PI/5;
		ship.add( mesh );
		
		return ship;
	},
	
}