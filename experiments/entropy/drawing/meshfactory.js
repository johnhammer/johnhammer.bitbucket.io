/* MeshFactory */

var lib = {
	part : function(geo,h){
		if(!h)h=geo.s;		
		return {m:new THREE.Mesh(geo),s:h};
	},
	flip : function(part){
		part.m.rotation.z = Math.PI/2;		
		return part;
	},
	altcyl : function(r2,r1,h) {
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r1,r2,h, 8, 1, false)),s:h};
	},
	cyl : function(r,h){
		return lib.altcyl(r,r,h);
	},
	prism : function(r,p,h){
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r,r,h, p, 1, false)),s:h};
	},
	sph : function(h) {
		return {m:new THREE.Mesh(new THREE.SphereGeometry(h, 8, 8)),s:h};
	},
	tor : function(h1,h2){
		var tor = new THREE.Mesh(new THREE.TorusGeometry(h1,h2,8,16));
		tor.rotation.x=Math.PI/2;
		return {m:tor,s:1};		
	},
	littleWing : function(size){
		var shape = new THREE.Shape();
		shape.moveTo( size,size );
		shape.lineTo( size,-size/2 );
		shape.lineTo( -size/2,-size/2 );
		var mesh = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, {steps: 1,depth: .2,bevelEnabled: false} ));
		return lib.skew({m:mesh,s:1},3);
	},
	skew : function(part,amt){
		part.m.rotation.z = Math.PI / amt;
		return part;
	},
	merge : function(b,a){
		a.updateMatrix();
		b.merge(a.geometry, a.matrix);
	},	
	assemble :function(parts){
		var geo = new THREE.Geometry();	
		var orig = parts[0] ? -parts[0].s/2 : -parts[1].s/2;
		for ( var i = 0; i < parts.length; i++ ) {	
			if(parts[i]){
				parts[i].m.position.y = orig+parts[i].s/2 + (parts[i].shift||0);
				lib.merge(geo,parts[i].m);
				if(!parts[i].skip) orig += parts[i].s;
			}
		}
		geo.s=orig;
		geo.translate(0,-orig/2,0);
		return geo;
	},
	assembleM :function(parts){
		var mesh = new THREE.Mesh();	
		var orig = parts[0] ? -parts[0].s/2 : -parts[1].s/2;
		for ( var i = 0; i < parts.length; i++ ) {	
			if(parts[i]){
				parts[i].m.position.y = orig+parts[i].s/2 + (parts[i].shift||0);
				mesh.add(parts[i].m);
				if(!parts[i].skip) orig += parts[i].s;
			}
		}
		mesh.geometry.translate(0,-orig/2,0);
		return mesh;
	},
	multi: function(object,amount,radius,rotate=false){
		var geo = new THREE.Geometry();
		
		var x0 = 0;
		var y0 = 0;		
		var angle = 0;
		for(var i = 0; i < amount; i++) {
			var x = x0 + radius * Math.cos(2 * Math.PI * i / amount);
			var y = y0 + radius * Math.sin(2 * Math.PI * i / amount);   
			var cyl = object.m.clone();					
			cyl.position.x = x;
			cyl.position.z = y;			
			if(rotate){
				cyl.rotation.y = angle;			
				angle -= 2*Math.PI/amount;	
			}
			lib.merge(geo,cyl);
		}
		
		return {m:new THREE.Mesh(geo),s:object.s};
	},
	multiRot: function(object,amount,radius){
		return lib.multi(object,amount,radius,true)
	},	
	multiM: function(object,amount,radius,meshes,rotate=false){
		var mesh = new THREE.Mesh();		
		var x0 = 0;
		var y0 = 0;		
		var angle = 0;
		for(var i = 0; i < amount; i++) {
			var x = x0 + radius * Math.cos(2 * Math.PI * i / amount);
			var y = y0 + radius * Math.sin(2 * Math.PI * i / amount);   
			var cyl = object.m.clone();					
			cyl.position.x = x;
			cyl.position.z = y;			
			if(rotate){
				cyl.rotation.y = angle;			
				angle -= 2*Math.PI/amount;	
			}
			meshes.push(cyl);
			mesh.add(cyl);
		}		
		mesh.s = object.s;
		return {m:mesh,s:object.s};
	},
	multiRotM: function(object,amount,radius,meshes){
		return lib.multiM(object,amount,radius,meshes,true)
	},
	skip(piece){
		if(piece) piece.skip = true;
		return piece;		
	},
	iif(part,num,lim=1){
		return num<lim ? null : part;
	},
	nif(part,num,lim=1){
		return !num<lim ? null : part;
	},	
	iifA(partCallback,num,lim=1){
		return num<lim ? null : partCallback();
	},
	f(num,factor){
		return 1+num*factor;
	},
	move(piece,amount){
		piece.shift = amount;
		return piece;
	}
}


var MeshFactory = {
	Explosion : function(){
		
	},
	
	Build : function(ship){		
		if(ship.Size == 8) return this.Size8(ship);
		if(ship.Size == 4) return this.Size4(ship);
		if(ship.Size == 2) return this.Size2(ship);
		if(ship.Size == 1) return this.Size1(ship);
	},
	Size1 : function(ship){
		var sphereBody = ship.Shield + ship.Radio + ship.Habitat;
		
		return lib.assemble([
			lib.altcyl(.5,.4,.5),
			lib.iif(lib.move(lib.skip(lib.multiRot(lib.littleWing(1),4+ship.Engine,1)),-.8),!ship.Radio),
			
			lib.iif(lib.skip(lib.move(lib.multiRot(lib.skew(lib.altcyl(.2,.1,4),12),4,2.35),-3)),ship.Radio),
			
			lib.altcyl(1*lib.f(ship.Ammo,.5),.8,2-sphereBody),
			
			lib.iif(lib.skip(lib.multi(lib.altcyl(0,.5,3),4,1.1)),ship.Weapon),
			lib.iif(lib.skip(lib.multiRot(lib.flip(lib.cyl(.3,1)),4,0.5)),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.65,2),7,.6)),ship.Fuel),
			
			lib.iif(lib.altcyl(.8,1*lib.f(ship.Ammo,.5),2),ship.Ammo + ship.Engine),			
			lib.iif(lib.sph(2),sphereBody),
			lib.iif(lib.cyl(1*lib.f(ship.Nada,.5),3),ship.Nada),
			
			lib.iif(lib.move(lib.tor(2.2,.5),-1.5),ship.Shield),	

			lib.iif(lib.move(lib.multiRot(lib.skew(lib.cyl(.3,2),8),4,1.5),-2.5),ship.Radio),
						
			lib.iif(lib.altcyl(0,.8,1.5),ship.Engine+ship.Weapon),
			
			lib.iif(lib.move(lib.sph(.8),-.5),ship.Ammo),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,.8),4,.6)),ship.Computer),
			lib.iif(lib.cyl(.3,1),ship.Computer)		
			
		]);		
	},
	Size2 : function(ship){
		return lib.assemble([

			lib.nif(lib.altcyl(.5,.8,1),ship.Engine),
			lib.iif(lib.multi(lib.altcyl(.5,.8,1),2+ship.Engine,1),ship.Engine),
			
			lib.cyl(1*lib.f(ship.Engine*2,.3),1),
			lib.cyl(.8,.5),

			lib.cyl(2,3),
		]);		
	},
	Size4 : function(ship){
		return lib.assemble([			
			lib.altcyl(.5*lib.f(ship.Engine,.1),.8*lib.f(ship.Engine,.1),1),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.iif(lib.multi(lib.cyl(.2,1),ship.Engine+1,.6),ship.Engine),	
			
			lib.iif(lib.skip(lib.multi(lib.sph(0.75),ship.Fuel*3,1*lib.f(ship.Fuel,.1))),ship.Fuel),
			lib.cyl(0.9*lib.f(ship.Fuel,.1),1.5*lib.f(ship.Nada,.1)),
			
			lib.iif(lib.skip(lib.tor(2.5,.2)),ship.Radio),	
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.3,1.2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.cyl(1.2*lib.f(ship.Ammo,.1),1.2),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,1*lib.f(ship.Computer,.1)),4,.6)),ship.Computer),			
			lib.iif(lib.cyl(.3,0.5*lib.f(ship.Computer,.2)),ship.Computer),		
						
			lib.cyl(1,1*lib.f(ship.Habitat,.2)),
			
			lib.skip(lib.multi(lib.cyl(.2,1.8),2*(ship.Shield+1),.6)),
			
			lib.cyl(.5,2),		
			
			lib.altcyl(0,.5,.2*lib.f(ship.Engine,2)),
		]);		
	},
	Size8 : function(ship){
		return lib.assemble([			
			lib.altcyl(.5*lib.f(ship.Engine,.1),.8*lib.f(ship.Engine,.1),1),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.iif(lib.multi(lib.cyl(.2,1),ship.Engine+1,.6),ship.Engine),	
			lib.iif(lib.skip(lib.multi(lib.sph(0.75),ship.Fuel*3,1*lib.f(ship.Fuel,.1))),ship.Fuel),
			lib.cyl(0.9*lib.f(ship.Fuel,.1),1.5*lib.f(ship.Nada,.1)),
			
			lib.skip(lib.multi(lib.part(this.Size4(ship),4),5,3)),
			lib.skip(lib.multiRot(lib.flip(lib.cyl(.5,3)),5,1)),			
			
			lib.iif(lib.skip(lib.tor(2.5,.2)),ship.Radio),	
			lib.iif(lib.skip(lib.multi(lib.cyl(.3,1.2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.cyl(1.2*lib.f(ship.Ammo,.1),3),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,1*lib.f(ship.Computer,.1)),4,.6)),ship.Computer),			
			lib.iif(lib.cyl(.3,0.5*lib.f(ship.Computer,.2)),ship.Computer),		
			lib.cyl(1,2*lib.f(ship.Habitat,.2)),
			lib.skip(lib.multi(lib.cyl(.2,1.8),2*(ship.Shield+1),.6)),
			lib.cyl(.5,3),		
			lib.altcyl(0,.5,.2*lib.f(ship.Engine,2))
		]);		
	}
}
