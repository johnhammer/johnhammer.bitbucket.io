/* Ship Names */

function getShipName(ship){
	var factionName = getFactionShip(Memory.get('faction',0),ship);
	if(factionName) return factionName;
	
	return getShipTypeName(ship);	
}

function getShipSkills(s){
	function setSkill(skills,skill){
		if(!skills.s1){
			skills.s1 = {
				title:skill,
				text:LONGTEXT(skill)
			};
		}		else if(!skills.s2){
			skills.s2 = {
				title:skill,
				text:LONGTEXT(skill)
			};
		}
	}
	
	function IsProjectile(ship){
		return 	ship.Weapon +
				ship.Shield +
				ship.Radio + 
				ship.Nada + 
				ship.Computer == 0;
	}

	var skills = {};

	if(getFactionShip(Memory.get('faction',0),s)) 			setSkill(skills,'Glory');
	if(s.Habitat>=2&&s.Radio>=1&&s.Computer>=1)				setSkill(skills,'SpaceLab');				
	if(s.Habitat>=3&&s.Fuel>=1)								setSkill(skills,'Colonizer');				
	if(s.Nada>=4&&s.Fuel>=2)								setSkill(skills,'Carrier');				
	if(IsProjectile(s))										setSkill(skills,'Projectile');				
	if(IsProjectile(s)&&s.Size<=4&&s.Habitat>=1&&s.Ammo>=1)	setSkill(skills,'Kamikaze');				
	if(IsProjectile(s)&&s.Size<=2&&s.Engine>0)				setSkill(skills,'Missile');				
	if(IsProjectile(s)&&s.Ammo>0)							setSkill(skills,'Bomb');					
	if(IsProjectile(s)&&s.Ammo>0&&s.Fuel>0)					setSkill(skills,'DirtyBomb');				
	if(s.Size<=2&&s.Weapon+s.Nada+s.Radio==0&&s.Computer>0)	setSkill(skills,'Emp');					
	if(s.Size<=2&&s.Radio>=1)								setSkill(skills,'Espionage');				
	if(s.Ammo>=6)		                                    setSkill(skills,'PlanetBuster');			
	if(s.Ammo>=4)											setSkill(skills,'ArmorPiercer');			
	if(s.Shield>=4)											setSkill(skills,'FlyingBunker');			
	if(s.Computer>=4)										setSkill(skills,'NeuralCenter');			
	if(s.Radio>=4)											setSkill(skills,'BigEar');					
	if(s.Weapon+s.Habitat>1&&s.Weapon==s.Habitat)			setSkill(skills,'IntelligentFire');			
	if(s.Weapon>=4)											setSkill(skills,'RapidFire');				
	if(s.Weapon>=2&&s.Radio>=2)								setSkill(skills,'FlakAA');					
	if(s.Habitat>=2&&s.Computer>=2)							setSkill(skills,'Hackathon');				
	if(s.Weapon>=2&&s.Computer>=2)							setSkill(skills,'AntiRadiationGun');		
	if(s.Weapon>=1&&s.Habitat>=1&&s.Ammo>=1)				setSkill(skills,'Artillery');				
	if(s.Weapon>=1&&s.Radio>=1&&s.Ammo>=1)					setSkill(skills,'PrecisionBombardment');	
	if(s.Computer>=1&&s.Habitat>=1)							setSkill(skills,'Hacker');					
	if(s.Weapon>=1&&s.Ammo>=1)								setSkill(skills,'Bombardment');			

	return skills;
}

function getShipTypeName(ship){
	switch(ship.Size){
		case 1: return getShipName1(ship);
		case 2: return getShipName2(ship);
		case 4: return getShipName4(ship);
		case 8: return getShipName8(ship);
	}
}

function getFactionShip(faction,ship){	
	var factionShips = CONST.factions[faction].ships;
	for (i = 0; i < factionShips.length ; i++) { 
		if(shipUtils.match(ship,factionShips[i])){
			return factionShips[i].name;
		}
	}
	return null;	
}

function getShipName1(count){
	if(count.Weapon==1) 	return TEXT('Fighter drone');
	if(count.Ammo==1) 		return TEXT('Bomb');
	if(count.Shield==1) 	return TEXT('Decoy drone');
	if(count.Engine==1) 	return TEXT('Missile');
	if(count.Fuel==1) 		return TEXT('Mother drone');
	if(count.Habitat==1) 	return TEXT('Explorer');
	if(count.Radio==1) 		return TEXT('Probe');
	if(count.Computer==1) 	return TEXT('EMP pulse');
							return TEXT('Cargo drone');
}

function getShipName2(count){
	if(count.Weapon==1 && count.Ammo==1) 		return TEXT('Cazabombardero');
	if(count.Habitat==1 && count.Ammo==1) 		return TEXT('Kamikaze');
	if(count.Habitat==1 && count.Computer==1) 	return TEXT('Hacker');
	if(count.Habitat==2) 						return TEXT('Insignia ligera');
	
	var str = [];
	
	if(count.Weapon>=1) 						str.push(TEXT('Caza'));
	else if(count.Ammo>=1) 						str.push(TEXT('Bomba'));
	else if(count.Computer>=1) 					str.push(TEXT('Interceptor'));
	else if(count.Nada>=1) 						str.push(TEXT('Nave de carga pequena'));
	else if(count.Habitat>=1) 					str.push(TEXT('Explorador'));
	else if(count.Fuel>=1) 						str.push(TEXT('Nodriza pequena'));
	else if(count.Radio>=1) 					str.push(TEXT('Telescopio'));
	else if(count.Engine>=1) 					str.push(TEXT('Misil'));
	else if(count.Shield>=1) 					str.push(TEXT('Senuelo'));
	
	return str.join(" ");
}

function getShipName4(count){

	if(count.Habitat==1 && count.Ammo==3) 							return TEXT('Kamikaze pesado');

	var str = [];
	
	if(count.Habitat>=3) 											str.push(TEXT('Insignia'));
	else if(count.Weapon>=1 && count.Ammo>=1 && count.Shield>=1) 	str.push(TEXT('Crucero'));
	else if(count.Weapon>=3) 										str.push(TEXT('Fragata'));
	else if(count.Weapon>=2 && count.Ammo>=1) 						str.push(TEXT('Canonera'));
	else if(count.Weapon>=1 && count.Ammo>=2) 						str.push(TEXT('Destructor'));
	else if(count.Ammo>=3) 											str.push(TEXT('Bomba de antimateria'));
	else if(count.Weapon>=1 && count.Shield>=2) 					str.push(TEXT('Acorazado'));
	else if(count.Engine>=2 && count.Weapon>=1) 					str.push(TEXT('Caza pesado'));
	else if(count.Engine>=2 && count.Ammo>=1) 						str.push(TEXT('Misil pesado'));
	else if(count.Computer>=2 && count.Weapon>=1) 					str.push(TEXT('Corbeta mixta'));
	else if(count.Computer>=3) 										str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=2 && count.Radio>=1)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=1 && count.Radio>=2)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Shield>=3) 										str.push(TEXT('Nave escudo'));
	else if(count.Nada>=3) 											str.push(TEXT('Nave de carga'));
	else if(count.Fuel>=3) 											str.push(TEXT('Nodriza'));
	else if(count.Radio>=3) 										str.push(TEXT('Estacion espacial'));
	else if(count.Radio>=2 && count.Habitat>=1) 					str.push(TEXT('Estacion espacial'));
	else if(count.Weapon>=1 && count.Ammo>=1) 						str.push(TEXT('Nave de guerra'));
	else if(count.Computer>=2) 										str.push(TEXT('Corbeta de senales'));
	else if(count.Weapon>=2) 										str.push(TEXT('Corbeta'));
	else if(count.Radio>=2) 										str.push(TEXT('Nave Radio'));
	else if(count.Nada>=2) 											str.push(TEXT('Nave de carga'));
	else if(count.Habitat>=2) 										str.push(TEXT('Transporte'));
	else if(count.Engine>=3) 										str.push(TEXT('Misil'));	
	else 															str.push(TEXT('Hibrida'));
	
	return str.join(" ");
}

function getShipName8(count){
	var str = [];	
																	str.push(TEXT('Gran'));
	if(count.Habitat>=3) 											str.push(TEXT('Insignia'));
	else if(count.Weapon>=1 && count.Ammo>=1 && count.Shield>=1) 	str.push(TEXT('Crucero'));
	else if(count.Weapon>=3) 										str.push(TEXT('Fragata'));
	else if(count.Weapon>=2 && count.Ammo>=1) 						str.push(TEXT('Canonera'));
	else if(count.Weapon>=1 && count.Ammo>=2) 						str.push(TEXT('Destructor'));
	else if(count.Ammo>=3) 											str.push(TEXT('Bomba de antimateria'));
	else if(count.Weapon>=1 && count.Shield>=2) 					str.push(TEXT('Acorazado'));
	else if(count.Engine>=2 && count.Weapon>=1) 					str.push(TEXT('Caza pesado'));
	else if(count.Engine>=2 && count.Ammo>=1) 						str.push(TEXT('Misil pesado'));
	else if(count.Computer>=2 && count.Weapon>=1) 					str.push(TEXT('Corbeta mixta'));
	else if(count.Computer>=3) 										str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=2 && count.Radio>=1)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=1 && count.Radio>=2)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Shield>=3) 										str.push(TEXT('Nave escudo'));
	else if(count.Nada>=3) 											str.push(TEXT('Nave de carga'));
	else if(count.Fuel>=3) 											str.push(TEXT('Nodriza'));
	else if(count.Radio>=3) 										str.push(TEXT('Estacion espacial'));
	else if(count.Radio>=2 && count.Habitat>=1) 					str.push(TEXT('Estacion espacial'));
	else if(count.Weapon>=1 && count.Ammo>=1) 						str.push(TEXT('Nave de guerra'));
	else if(count.Computer>=2) 										str.push(TEXT('Corbeta de senales'));
	else if(count.Weapon>=2) 										str.push(TEXT('Corbeta'));
	else if(count.Radio>=2) 										str.push(TEXT('Nave Radio'));
	else if(count.Nada>=2) 											str.push(TEXT('Nave de carga'));
	else if(count.Habitat>=2) 										str.push(TEXT('Transporte'));
	else 															str.push(TEXT('Hibrida'));
	
	return str.join(" ");
}


/* Fleet Names */

function getRandomFleetName(){	
	var faction = Memory.get('faction',0);
	return LIB.Capitalize(CONST.factions[faction].fleets[Math.floor(Math.random() * 32)]);
}

/*

	Name maker

*/

var Flavours = {
	German: {
		Name: {
			Min:1,
			Max:2,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:['walt','wern','her','guent','wolf'],
			Terminations:['er','mann','fred','gang'],
			Full:['Franz','Erwin','Otto','Hans','Ludwig','Horst','Adolf','Ludwig'],
		},
		Linker: ['von','van'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['fisch','muell','schroed','zimmer','hof','hart','wern','leh','kohl','walt','berg'],
			Terminations:['stein','stadt','ing','er','mann'],
			Full:['Rommel','Koch','Schmidt','Braun'],
		}
	},
	Japanese: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:2,
			MaxSyllables:5,
			Syllables:['dai','ki','rou','go','wa','hachi','ha','to','hi','bi','shou','shi','taka'],
			Terminations:[],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:4,
			Syllables:['fu','hon','hi','ji','ha','shi','ma','aki','ku','wa','ma'],
			Terminations:['maru','moto','oka','saki','da','shi','kawa','ura'],
			Full:['Tojo'],
		}
	},
	Indian:{
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['akh','braham','pra','ash','bal','win','der','ram','bi','mal','bir'],
			Terminations:['preet','bir','prem','der','ram','pal','kaal','kash'],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:[],
			Terminations:[],
			Full:['Singh'],
		}
	},
	Latin:{
		Name: {
			Min:1,
			Max:3,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['aure','augu','stus','cato','clau','tibe','iuli','magni','regi','anto','marci'],
			Terminations:['nius','dius','stus','lius','ius','us'],
			Full:['Hannibal','Hanno','Hamilcar','Hasdrubal'],
		},
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['aure','augu','sti','cato','clau','tibe','iuli','magni','regi','anto','marci'],
			Terminations:['nius','dius','stus','lius','ius','us'],
			Full:[],
		}
	},
	Spanish:{
		Name: {
			Min:1,
			Max:2,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['Juan','Jose','Miguel','Pedro','Carlos','Antonio','Gabriel','Diego','Andres','Francisco'],
		},
		Linker: ['de'],
		Surname: {
			Min:2,
			Max:2,
			MinSyllables:1,
			MaxSyllables:3,
			Syllables:['mar','tin','gon','zal','per','ra','mir','ji','men','al','var'],
			Terminations:['ez','tara','ar','mano','era'],
			Full:[],
		}
	},
	Slavic: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['bog','slo','bo','vlad','dmi','ul','yev','aleks','serg'],
			Terminations:['dan','imir','tri','yan','ris','geni','andr','ey'],
			Full:['Iosif','Lev','Grigori','Iuri'],
		},
		Patronym: ['ovich','evich'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:3,
			Syllables:['len','stal','bog','dan','ryk','bog','slo','bo','ga','gar','put','yev','aleks'],
			Terminations:['in','ov','ev','nsky'],
			Full:[],
		}
	},
	Chinese: {
		Name: {
			Min:1,
			Max:2,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['li','xiu','ying','wei','fang','min','jing','qiang','lei','yang','yong','jun','chao'],
			Terminations:[],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['li','xiu','ying','wei','fang','min','jing','qiang','lei','yang','yong','jun','chao'],
			Terminations:[],
			Full:[],
		}
	},
	Greek: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['phil','agr','ecyd','le','pe','andr','mnes','her','rod','klid','thal'],
			Terminations:['es','os','as','us','ter'],
			Full:['Alexandros'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:3,
			Syllables:['phil','agr','ecyd','le','pe','andr','mnes','her','rod','klid'],
			Terminations:['akis','as','ides','opoulos','ou','akos','eas','otis'],
			Full:[],
		}
	},
	Islamic: {
		Name: {
			Min:1,
			Max:3,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['abd','amin'],
			Terminations:['ullah','allah','ur Rahman'],
			Full:['Muhammad','Ali','Hussein'],
		},
		Linker: ['ibn','bin','abu'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:['al'],
			Terminations:[],
			Full:['Bakr'],
		}
	},
	Aztec: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:5,
			Syllables:['ya','mat','lal','ihu','zo','no','che','hu','xoch','maz','cue','pa','zt','tz'],
			Terminations:['atl','itl','otl','lin','lal','tli','lli'],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:0,
			Max:0,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Tribal: {
		Name: {
			Min:1,
			Max:2,
			MinSyllables:0,
			MaxSyllables:0,
			Syllables:[],
			Terminations:[],
			Full:['Lobo','Perro','Aguila','Toro','Cazador','Guerrero','Pastor','Lider','Sentado','Sanguinario','Salvaje','Brutal','Recolector','Agricultor','Destructor','Mensajero','Navegante','Explorador','Chaman','Azul','Rojo','Negro','Blanco','Hombre','Pajaro','Cuervo','Leon','Carnicero','Pescador'],
		},
		Linker: ['de','en la','de la','a la'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['Guerra','Paz','Alianza','Discordia','Noche','Montana','Tierra','Mar','Tribu','Familia','Niebla','Lluvia','Pradera','Nube','Luna','Muerte','Vida','Caceria','Masacre','Calma','Senal'],
		}
	},
	English: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:['ald','al','as','bar','brad','wins','cold','craw','crom','day','dud','fair','had','wad'],
			Terminations:['en','win','fred','ger','ton','wick','ter','ford','man','ton','well','ley','wyn'],
			Full:['John','Jack','George','Stephen','Charles','Dean','Edward','Gabriel','Ryan','Robert'],
		},
		Linker: ['J.','G.','D.','R.','E.','S.','C.','A.'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['arch','farm','fish','thatch','turn','pott','park','bridg','field','grov','davi','jack','john','simp','stephen','wat','wil','hender'],
			Terminations:['er','son','s','man','field','wright','e'],
			Full:['Clark','Churchill','Cook','Mason','Wright','Taylor','Sawyer','Smith','King','Bush','Jones','Armstrong'],
		}
	},
	French: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	African: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Mongolian: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Viking: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['ragn','sig','as','bal','tor','ein','eiv','half','ha','ral','hjal','ei','val','thor'],
			Terminations:['urd','fried','geir','mundr','dr','sten','ar','rik','indr','dan','vard'],
			Full:['Olav','Bjorn','Knut','Dagr','Gustav','Yngvar','Jarl','Leif'],
		},
		Patronym: ['son','sson'],
		Linker: [],
		Surname: {
			Min:0,
			Max:0,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Egyptian: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	MiddleEast: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	
}

var Names = [
	[Flavours.German,Flavours.Japanese],									//El Gran Imperio
	[Flavours.Slavic,Flavours.Chinese],										//Union Sovietica Universal			
	[Flavours.Greek],														//Confederacion de Jonia			
	[Flavours.Islamic],														//El Nuevo Califato			
	[Flavours.Latin],														//Tercera Republica Romana
	[Flavours.Indian,Flavours.Chinese],										//Dinastia Sikh			
	[Flavours.Aztec,Flavours.Tribal],										//Imperio de la Serpiente			
	[Flavours.German,Flavours.Spanish],										//Imperio Austro-Espanol
	[Flavours.Japanese,Flavours.English],									//Corporacion Universal			
	[Flavours.Slavic,Flavours.Spanish,Flavours.English,Flavours.German],	//Federacion Anarquista			
	[Flavours.English,Flavours.Spanish],									//EUU			
	[Flavours.English,Flavours.French],										//Imperio Francobritanico			
	[Flavours.African,Flavours.MiddleEast],									//Alianza de Isandlwana			
	[Flavours.Mongolian,Flavours.MiddleEast],								//Khanato celestial			
	[Flavours.Viking,Flavours.Tribal],										//Condados del norte			
	[Flavours.Egyptian,Flavours.MiddleEast]									//Reino Eterno de Egipto	
]

function GetName(object,rnd){
	var name = [];
	
	var numberOfNames = Random.Range(rnd,object.Min,object.Max);	
	for (i = 0; i < numberOfNames; i++) { 
	
		if(object.Syllables.length==0 || object.Full.length>0 && Random.Bool(rnd,0.8)){
			name.push(Random.Choice(rnd,object.Full));
		}
		else{
			var word = '';
			var nameLength = Random.Range(rnd,object.MinSyllables,object.MaxSyllables)
			for (j = 0; j < nameLength; j++) { 
				word += Random.Choice(rnd,object.Syllables);
			}
			if(object.Terminations.length>0){
				word += Random.Choice(rnd,object.Terminations);
			}
			name.push(LIB.Capitalize(word));
		}
	}
	return name;
}

function GetExtra(object,prob,rnd){
	if(object && object.length>0 && Random.Bool(rnd,prob)){
		return [Random.Choice(rnd,object)];
	}
	return []
}

function GetPatronym(flavor,rnd){
	if(flavor.Patronym){
		return [GetName(flavor.Name,rnd)[0]+GetExtra(flavor.Patronym,0,rnd)[0]];
	}
	return []	
}

function MakeName(factionId,rnd){
	return 'PLAYERNAME';
	
	
	var faction = Names[factionId];
	var flavor = faction[Random.Range(rnd,1,faction.length)-1];
	
	var name = GetName(flavor.Name,rnd)
	var patronym = GetPatronym(flavor)
	var linker = GetExtra(flavor.Linker,0.2)
	var surname = GetName(flavor.Surname,rnd)
		
	return name.concat(patronym.concat(linker.concat(surname))).join(" ");
}

function MakeNames(numberOfNames,factionId){
	var names = [];
	for (var i = 0; i < numberOfNames; i++) { 
		names.push(MakeName(factionId));
	}
	return names;
}


var NameFactory = {
	MakeName : function(factionId,rnd){
		var name = [];
		var faction = NameFactory.Names[factionId];
		switch(factionId){
			case 0: 
				//El Gran Imperio              	N	Name Surname			Adolf Hitler
				//							 	N	Name Name Surname		Adolf Gerard Hitler
				name.push(Random.Choice(rnd,faction.Names));
				if(Random.Chance(rnd,4)){
					name.push(Random.Choice(rnd,faction.Names));
				}
				name.push(Random.Choice(rnd,faction.Surnames));
				break;			
		}
		return name.join(' ');
	},	
	Names : [		
		{
			Names:['Hans','Otto','Ernst','Rudolf','Adolf','Klaus','Alfred','Werner','Hideki','Kenji','Saito','Koichi','Takeji','Shigetaro','Akira'],
			Surnames:['Eigen','Euler','Fahrenheit','Heisenberg','Kepler','Kirchhoff','Planck','Ribbentrop','Minamoto','Genji','Taira','Heishi','Tachibana','Fujiwara'],		
		},		
		//Union Sovietica Universal		N	Name Pathronym			Vladimir Vladimirovich
		{
			Names:[],
			Suffixs:['vich','ovich','evich','evich'],		
		},	
		//Confederacion de Jonia		N	Name Demonym			Alexander Macedonou
		{
			Names:[''],
			Places:['Smyrn','Milet','Myus','Prien','Ephes','Coloph','Lebed','Teos','Klazomen','Phocae','Chi','Sam','Erythr','Magnesi','Trall','Halikarnass','Agor','Acropol','Anaktor','Megar','Gymnasi','Metropol','Ekklesi','Theatr','Macedon','Peloponn','Attic','Aeolis','Cari','Thessali','Ioni','Epir'],
			Suffixs:['akis','akos','oulis','atos','eas','otis','ou','allis'],		
		},	
		//El Nuevo Califato			   	N	Name Part Pathronym		Mohammed ibn Abdullah
		//								N	Name al-Demonym			Mohammed al-Farsi
		{
			Names:[],
			Part:['bin','ibn'],		
			Al:'al-',
			Nationalities:['maghrebi','farsi','jazayiri','misri','andilusi','sari','suwri','baghdadi']
		}
		//Tercera Republica Romana     	N
		//Dinastia Sikh			       	N
		//Imperio de la Serpiente		N	
		//Imperio Austro-Espanol     	N
		//Corporacion Universal			N
		//Federacion Anarquista			N
		//EUU			               	N
		//Imperio Francobritanico		N	
		//Alianza de Isandlwana			N
		//Khanato celestial			   	N
		//Condados del norte			N
		//Reino Eterno de Egipto	   	N
	],

}


