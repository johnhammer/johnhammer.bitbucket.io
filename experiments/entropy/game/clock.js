var Clock = {
	Events : [],	
	t : 0,
	
	Init : function(){
		Clock._Tick();
	},
	
	AddEvent : function(time,callback){
		Clock.Events.push({
			t:Clock.t+time,
			c:callback
		});
	},
	
	_Tick : function(){
        setTimeout(function() { 
			Clock.t++;
			Clock._Check();
            Clock._Tick();      
        }, 1000);   
	},
	_Check : function(){
		var updated = false;
		for(var i=0;i<Clock.Events.length;i++){
			if(Clock.Events[i].t<=Clock.t){
				Clock._ProcessEvent(Clock.Events[i]);
				updated=true;
			}
		}
		if(updated){ //cleanup
			Clock.Events = Clock.Events.filter(x=>x.t>Clock.t);
		}
	},
	_ProcessEvent : function(e){
		e.c();
	},
}