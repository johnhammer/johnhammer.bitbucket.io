var Events = {
    _Raycast : function(event,callback){
        mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
        mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;
    
        raycaster.setFromCamera( mouse, camera );
    
        var intersects = raycaster.intersectObjects( scene.children, false );
    
        if ( intersects.length > 0 ) {
            callback(intersects[0].object);
        }
    },
    
    Map : function(event){
        Events._Raycast(event,Events.SelectMapObject);
    },
    SelectMapObject : function(object){
        Draw_Map.SelectMapObject(object);
        CURRENT.SelectedMesh = object;
        Write.MapObject(object.obj);
    },
    FleetReachedObjective : function(i){
        var fleet = CURRENT.MovingFleets[i];
        if(fleet.obj.retreat){
            Edit.FleetGive(fleet.obj,fleet.obj.objective.props);
			Edit.FleetReturns(fleet.obj,fleet.obj.objective);
            
            scene.remove(fleet);
            delete CURRENT.MovingFleets[i];	
			
			Write.Map();
        }
        else{
            var objProps = fleet.obj.objective.props;

            if(objProps.Fleets.length>0){
                Screen.Battle({Id:fleet.obj.id,Player:fleet.obj.player},objProps.Fleets[0]);
            }
            //

            if(fleet.obj.order==ENUM.MapAction.Route){
                Draw_Map.ReturnFleet(i);
                if(objProps.matter){
                    Edit.FleetTake(fleet.obj,fleet.obj.objective.props);
                }
            }       
            else{
                Draw_Map.StandbyFleet(i);
                objProps.Fleets.push({Id:fleet.obj.id,Player:fleet.obj.player});
            }     
        }
		
    },
}