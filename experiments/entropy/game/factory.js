var Factory = {
	Fleet : function(player=PLAYER_ID){
		var ships = [];
		for(var i=0;i<PLAYERS[player].Ships.length;i++){
			ships.push(0);
		}
		return { 
			name: "New fleet",
			ships: ships,
			location:"P0",
			Player:player
		}
	},
	Ship : function(){
		return {
			name: "New ship",
			parts:{
				Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1,Nada:1
			}
		}
	}
}