var Menu = {
    Main : function(option){
        switch(option){
            case 0:Screen.Room(true);break;
            case 1:Screen.Room();break;
            case 2:break;
            case 3:break;
            case 4:break;
        }
	},
	//map
	DeployFleet : function(fleet,order){
		var origin = PLAYERS[PLAYER_ID].Fleets[fleet].location;
		var objective = CURRENT.SelectedMesh.obj.id;		
		Orders.DeployFleet([fleet,origin,objective,order]);		
	},	
	AddFleet : function(){
		CURRENT.EditingFleet = Factory.Fleet();
		CURRENT.EditingFleet.Temporal = true;
		Screen.Fleets();
	},
	EditFleet : function(fleet){
		CURRENT.EditingFleet = LIB.Clone(PLAYERS[PLAYER_ID].Fleets[fleet]);
		CURRENT.EditingFleet.Id = fleet;
		Screen.Fleets();
	},
	//fleets
	AddShipToFleet : function(ship){
		CURRENT.EditingFleet.ships[ship]++;
		Write.UpdateShipAmount(ship,CURRENT.EditingFleet.ships[ship]);
	},
	RemoveShipFromFleet : function(ship){
		CURRENT.EditingFleet.ships[ship]--;
		Write.UpdateShipAmount(ship,CURRENT.EditingFleet.ships[ship]);
	},
	SaveFleet : function(){
		Orders.SaveFleet([CURRENT.EditingFleet]);
		Screen.Map();
	},
	CancelFleet : function(){
		Screen.Map();
	},
	AddShipType : function(){
		CURRENT.EditingShip = Factory.Ship();
		CURRENT.EditingShip.Temporal = true;
		Screen.Ships();
	},
	EditShipType : function(ship){
		CURRENT.EditingShip = LIB.Clone(PLAYERS[PLAYER_ID].Ships[ship]);
		CURRENT.EditingShip.Id = ship;
		Screen.Ships();
	},
	//ship
    AddPartToShip : function(part){
		CURRENT.EditingShip.parts[part]++;
		CURRENT.EditingShip.parts.Nada--;
		Write.UpdateShipPartAmount(part,CURRENT.EditingShip.parts[part]);
		Draw_Ship.UpdateShip();
	},
	RemovePartFromShip : function(part){
		CURRENT.EditingShip.parts[part]--;
		CURRENT.EditingShip.parts.Nada++;
		Write.UpdateShipPartAmount(part,CURRENT.EditingShip.parts[part]);
		Draw_Ship.UpdateShip();
	},
	SetShipSize : function(size){
		if(size<CURRENT.EditingShip.parts.Size){
			var isTemporal = CURRENT.EditingShip.Temporal;
			CURRENT.EditingShip = Factory.Ship();
			CURRENT.EditingShip.Temporal = isTemporal;
			Screen.Ships();
		}
		else{
			CURRENT.EditingShip.parts.Nada+=CURRENT.EditingShip.parts.Size-size;
			CURRENT.EditingShip.parts.Size=size;
			Write.UpdateShipPartAmount();
			Draw_Ship.UpdateShip();
		}		
	},
	SaveShip : function(){
		Orders.SaveShip([CURRENT.EditingShip]);
		Screen.Fleets();
	},
	CancelFleet : function(){
		Screen.Fleets();
	},
}

