var Orders = {
	Emit : function(func,args,player){
		if(ISONLINEGAME && player==PLAYER_ID){
			var msg = JSON.stringify({
				f:func,
				a:args,
				p:player
			});
			Protocol.Game(msg);
		}
	},	
	DeployFleet(args,player=PLAYER_ID){ 		
		Orders.Emit('DeployFleet',args,player); 
		
		var fleet = args[0];
		var origin = args[1];
		var destiny = args[2];
		var order = args[3];

		Draw_Map.DeployFleet(fleet,origin,destiny,player,order);
		Edit.DeployFleet(fleet,player);
		
		Write.Map();
	},	
	SaveFleet(args,player=PLAYER_ID){
		Orders.Emit('SaveFleet',args,player); 
		
		var fleet = args[0];
		
		Edit.SaveFleet(fleet,player);
	}
}