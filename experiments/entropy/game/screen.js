var Screen = {
    Room : function(isSinglePlayer=false){
        Templating.Main(Templating.Room());
		if(isSinglePlayer){
			Room.Single();
		}		
    },
    Game : function(){        
        Start.Game();
        Screen.Map(); 
		Draw.Render();
		Clock.Init();
    },
    Fleets : function(){
        var data = {
            Types : PLAYERS[PLAYER_ID].Ships,
            Ships : CURRENT.EditingFleet.ships
        };
        Templating.Main(Templating.Fleets(data));
    },
	Ships : function(){
        var data = {
            Parts : CURRENT.EditingShip.parts,
			Props : ShipCalculator.GetShipProps(CURRENT.EditingShip.parts)
        };
        Templating.Main(Templating.Ship(data));
		Draw_Ship.Ship(CURRENT.EditingShip.parts); 
    },
    Map : function(){
        Templating.Main(Templating.Map());    
        Write.Map();
        Draw_Map.Map();    
    },
    Battle : function(f1,f2){
        Templating.Main(Templating.Battle(f1,f2));    
        Draw_Battle.Battle(f1,f2);    
        Edit_Battle.Battle(f1,f2);   
    },
}