var Start = {
    Game : function(){      
		var rnd = Random.Create(GLOBAL.Seed);
	
        //Globals 
        GLOBAL.Map = Start.MakeMap(rnd);

        //Players
        for(var i=0;i<PLAYERS.length;i++){
			PLAYERS[i].Worlds = Start.InitWorlds(rnd,i);
            PLAYERS[i].Ships = Start.DefaultShips();
            PLAYERS[i].Fleets = Start.DefaultFleets(PLAYERS[i].Worlds[0].id,i);            
        }

    },	
	
	AddGlobalPlayer : function(human){
		var rnd = Random.Create();
		
		var civ = Random.Range(rnd,0,15);
		
		PLAYERS.push({
			Info:{
				Name : MakeName(civ,rnd),
				Civ : civ,
				Human : human,
				Color : LIB.RandomColor(128)
			},
		});
	},	
	
	InitWorlds : function(rnd,player){
        var world = Random.Choice(rnd,
            Object.keys(GLOBAL.Map)
                .filter( x=>GLOBAL.Map[x].type==ENUM.MapObject.Planet 
                    && GLOBAL.Map[x].Player==-1 )
                .map( x=>GLOBAL.Map[x]));
		world.Player = player;
		return [world];
	},

    DefaultShips : function(){
        return [
            {
                name: "Warship",
                parts:{
                    Weapon:1,Ammo:3,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:4,Nada:0
                }
            },
            {
                name: "Merchant",
                parts:{
                    Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1,Nada:1
                }
            },
            {
                name: "Tanker",
                parts:{
                    Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:1,Habitat:0,Radio:0,Computer:0,Size:1,Nada:0
                }
            },
            {
                name: "Missile",
                parts:{
                    Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1,Nada:0
                }
            }			
        ];
    },
    DefaultFleets : function(world,player){
        return [
            { 
                name: "War",
                ships: [2,0,0,0],
                location:world,
				Player:player
            },
            { 
                name: "Peace",
                ships: [0,10,5,0],
                location:world,
				Player:player
            },
            { 
                name: "War2",
                ships: [1,0,0,0],
                location:world,
				Player:player
            }		
        ];
    },

    MakeMap: function(rnd){ 
        var area = 10;

        var planets = Random.Range(rnd,PLAYERS.length,PLAYERS.length+10);
        var asteroids = Random.Range(rnd,0,20);
        var dust = Random.Range(rnd,0,15);
        var gas = Random.Range(rnd,0,15);

        var map = {};
        Start._GenerateMapObjects(rnd,map,area,planets,ENUM.MapObject.Planet);
        Start._GenerateMapObjects(rnd,map,area,asteroids,ENUM.MapObject.Asteroids);
        Start._GenerateMapObjects(rnd,map,area,dust,ENUM.MapObject.Dust);
        Start._GenerateMapObjects(rnd,map,area,gas,ENUM.MapObject.Gas);
        return map;
    },

    _GenerateMapObjectProperties(rnd,type){
        var props;
        switch(type){
            case ENUM.MapObject.Planet: 
                props = {
                    matter : 0,
                    maxmatter : 0,
                    Fleets:[]
                };
                break;
            case ENUM.MapObject.Asteroids:
            case ENUM.MapObject.Gas: 
            case ENUM.MapObject.Dust: 
                var maxmatter = Random.Range(rnd,1000,10000);
                props = {
                    matter : maxmatter,
                    maxmatter : maxmatter,
                    Fleets:[]
                };
                break;
        }
        return props;
    },
    _GenerateMapObjects: function(rnd,map,area,amount,type){
        for(var i=0;i<amount;i++){	            
            var name,id;
            switch(type){
                case ENUM.MapObject.Planet: 
                    name = Templating.Text('Planet')+' A'; 
                    id = 'P';
                    break;
                case ENUM.MapObject.Asteroids:
                    name = Templating.Text('Asteroids')+' A'; 
                    id = 'A';
                    break;
                case ENUM.MapObject.Gas: 
                    name = Templating.Text('Dust cloud')+' A'; 
                    id = 'D';
                    break;
                case ENUM.MapObject.Dust: 
                    name = Templating.Text('Gas cloud')+' A'; 
                    id = 'G';
                    break;
            }
            var object = {
				id: id+i,
                x : Random.FloatArea(rnd,area),	
                y : Random.FloatArea(rnd,area),	
                z : Random.FloatArea(rnd,area),
                name : name+i,
                type : type,
                Player : -1,
                props : Start._GenerateMapObjectProperties(rnd,type)
            };			
            map[id+i] = object;
        }        
    },
}