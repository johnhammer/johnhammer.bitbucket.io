var Write = {
    Map : function(){
		if(CURRENT.SelectedMesh){
			Write.MapObject(CURRENT.SelectedMesh.obj);
		}
    },
    MapObject : function(obj){	    
		if(obj.player!=undefined){
			//fleet
			var fleet = PLAYERS[obj.player].Fleets[obj.id];
			map_object.innerHTML = Templating.MapFleet(
				fleet.name,
				PLAYERS[obj.player].Info.Name,
				obj.props.matter,
				obj.props.maxcargo,
				obj.player==PLAYER_ID);
		}
		else{
			//object		
			var object = GLOBAL.Map[obj.id];
			map_object.innerHTML = Templating.MapObject(
				object.name,
				object.props.matter,
				object.props.maxmatter);
		}
		Write._MapFleets(obj);
	},
	_MapFleets : function(obj){
		var content = "";
		var fleets = PLAYERS[PLAYER_ID].Fleets;			
		for(var i=0;i<fleets.length;i++){
			if(fleets[i].location!=-1){
				var fleetOrders = FleetCalculator.GetFleetOrders(fleets[i],obj);
				var routeDetails = FleetCalculator.GetFleetRouteDetails(fleets[i],obj);
				content+=Templating.FleetInMap(i,fleets[i].name,fleetOrders,routeDetails);
			}
		} 
		fleets_list.innerHTML = content;
	},
	UpdateShipAmount : function(ship,amount){
		Screen.Fleets();
	},
	UpdateShipPartAmount : function(){
		Screen.Ships(); //update only needed parts
	},
	
	
}