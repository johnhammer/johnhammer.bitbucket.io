var CONST = {
    TickSpeed : 0.00002
}

var ENUM = {
    MapObject : {
        Planet 		:1,
        Gas 		:2,
        Asteroids 	:3,
        Dust 		:4,
        Fleet 		:5,
    },
    MapAction : {           //resource  myworld myfleet enworld enfleet neutworld
        Move        :1,     //defend    join    -       attack  attack  colonize
        Route       :2      //collect   bring   refuel  trade   refuel  -
    },
	PrisonersAction : {
		Enslave 	:1,		//add workers in worlds
		Conscript	:2,	    //add manpower for fleets
		Execute		:3,	    //raise morale
		Integrate	:4	    //raise technology
	},
}