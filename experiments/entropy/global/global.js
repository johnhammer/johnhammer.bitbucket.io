var GLOBAL = {
    Language : 1,
    Seed : 42
}

var CURRENT = {
    SelectedMesh : null,
    World : null,
    MovingFleets : {},
    Tick : 0,
    EditingFleet : null,
}

var ANIMATED = {
    Ships : [],
    Bullets : [],
	Explosions : []
};

var ISONLINEGAME = true;
var PLAYERS = [];
var CURRENT_PLAYER = 0;
var PLAYER_ID = 0;
var OBJECTS = [];

var scene,renderer,camera,raycaster,mouse,controls;