var Style = {
	Var(vars,vals){
		document.documentElement.style.setProperty('--'+vars,vals);	
	},			
	SetFactionColor : function(color){
		Style.Var('faction_color',color);
	},	
	
	SetTheme : function(theme){
		switch(theme){
		case 0  : //El Gran Imperio		
			Style.Var('font', 							"UnifrakturCook");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#000000");
			Style.Var('color_main_text', 				"#8C0702");
			Style.Var('color_secondary_back', 			"#99846B");
			Style.Var('color_secondary_text', 			"#000000");
			Style.Var('color_tertiary_back', 			"#D5B17D");
			Style.Var('color_detail_back', 			"#8C0702");
			Style.Var('color_detail_text', 			"#FFFFFF");
			break;
		case 1  : //Union Sovietica Universal	
			Style.Var('font', 							"buran_ussrregular");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#D52D17");
			Style.Var('color_main_text', 				"#400A30");
			Style.Var('color_secondary_back', 			"#FEEFA0");
			Style.Var('color_secondary_text', 			"#D52D17");
			Style.Var('color_tertiary_back', 			"#D5B17D");
			Style.Var('color_detail_back', 			"#000000");
			Style.Var('color_detail_text', 			"#D52D17");	
			break;
		case 2  : //Confederacion de Jonia		
			Style.Var('font', 							"diogenesregular");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#0B0701");
			Style.Var('color_main_text', 				"#DD7001");
			Style.Var('color_secondary_back', 			"#F3C450");
			Style.Var('color_secondary_text', 			"#0E0701");
			Style.Var('color_tertiary_back', 			"#722201");
			Style.Var('color_detail_back', 			"#485275");
			Style.Var('color_detail_text', 			"#E5AC82");	
			break;
		case 3  : //El Nuevo Califato			
			Style.Var('font', 							"ds_arabicregular");
			Style.Var('fontsize', 						"1.4em");
			Style.Var('color_main_back', 				"#CAAD7F");
			Style.Var('color_main_text', 				"#772300");
			Style.Var('color_secondary_back', 			"#B58542");
			Style.Var('color_secondary_text', 			"#472800");
			Style.Var('color_tertiary_back', 			"#CD9A6F");
			Style.Var('color_detail_back', 			"#A76617");
			Style.Var('color_detail_text', 			"#797C49");	
			break;
		case 4  : //Tercera Republica Romana	
			Style.Var('font', 							"diogenesregular");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#C4BFBE");
			Style.Var('color_main_text', 				"#283942");
			Style.Var('color_secondary_back', 			"#E2E1DD");
			Style.Var('color_secondary_text', 			"#283942");
			Style.Var('color_tertiary_back', 			"#D09D8F");
			Style.Var('color_detail_back', 			"#A6B8CC");
			Style.Var('color_detail_text', 			"#A88181");	
			break;
		case 5  : //Dinastia Sikh				
			Style.Var('font', 							"Courier new");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#38367F");
			Style.Var('color_main_text', 				"#F4960C");
			Style.Var('color_secondary_back', 			"#253089");
			Style.Var('color_secondary_text', 			"#F25D05");
			Style.Var('color_tertiary_back', 			"#000000");
			Style.Var('color_detail_back', 			"#FFFFFF");
			Style.Var('color_detail_text', 			"#000000");		
			break;
		case 6  : //Imperio de la Serpiente		
			Style.Var('font', 							"Special Elite");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#680C01");
			Style.Var('color_main_text', 				"#558147");
			Style.Var('color_secondary_back', 			"#40352F");
			Style.Var('color_secondary_text', 			"#aa3C1F");
			Style.Var('color_tertiary_back', 			"#A89B67");
			Style.Var('color_detail_back', 			"#AF200C");
			Style.Var('color_detail_text', 			"#4498AB");	
			break;
		case 7  : //Imperio Austropruso-Espanol	
			Style.Var('font', 							"Times New Roman");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#202E39");
			Style.Var('color_main_text', 				"#E2C88F");
			Style.Var('color_secondary_back', 			"#AFA278");
			Style.Var('color_secondary_text', 			"#2B0A09");
			Style.Var('color_tertiary_back', 			"#80381E");
			Style.Var('color_detail_back', 			"#B59642");
			Style.Var('color_detail_text', 			"#4A362F");		
			break;
		case 8  : //Corporacion Universal		
			Style.Var('font', 							"Courier new");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#FFFFFF");
			Style.Var('color_main_text', 				"var(--faction_color)");
			Style.Var('color_secondary_back', 			"#000000");
			Style.Var('color_secondary_text', 			"#FFFFFF");
			Style.Var('color_tertiary_back', 			"#000000");
			Style.Var('color_detail_back', 			"#FFFFFF");
			Style.Var('color_detail_text', 			"#000000");	
			break;
		case 9  : //Federacion Anarquista		
			Style.Var('font', 							"buran_ussrregular");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#2A373F");
			Style.Var('color_main_text', 				"#FFFFFF");
			Style.Var('color_secondary_back', 			"#000000");
			Style.Var('color_secondary_text', 			"#D52D17");
			Style.Var('color_tertiary_back', 			"#000000");
			Style.Var('color_detail_back', 			"#D52D17");
			Style.Var('color_detail_text', 			"#555281");		
			break;
		case 10 : //EUU							
			Style.Var('font', 							"Georgia");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#000000");
			Style.Var('color_main_text', 				"#0A007E");
			Style.Var('color_secondary_back', 			"#350DED");
			Style.Var('color_secondary_text', 			"#000000");
			Style.Var('color_tertiary_back', 			"#F232FA");
			Style.Var('color_detail_back', 			"#430E89");
			Style.Var('color_detail_text', 			"#2B77EF");		
			break;
		case 11 : //Imperio Francobritanico		
			Style.Var('font', 							"Times New Roman");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#615D52");
			Style.Var('color_main_text', 				"#F2ECE0");
			Style.Var('color_secondary_back', 			"#EDEFDF");
			Style.Var('color_secondary_text', 			"#00020F");
			Style.Var('color_tertiary_back', 			"#96B7B0");
			Style.Var('color_detail_back', 			"#C9391A");
			Style.Var('color_detail_text', 			"#EDC367");		
			break;
		case 12 : //Alianza de Isandlwana		
			Style.Var('font', 							"ds_arabicregular");
			Style.Var('fontsize', 						"1.5em");
			Style.Var('color_main_back', 				"#C8B482");
			Style.Var('color_main_text', 				"#AC3D1C");
			Style.Var('color_secondary_back', 			"#E8E4D8");
			Style.Var('color_secondary_text', 			"#1D1A1C");
			Style.Var('color_tertiary_back', 			"#CE9910");
			Style.Var('color_detail_back', 			"#AC3D1C");
			Style.Var('color_detail_text', 			"#3C571C");	
			break;
		case 13 : //Khanato celestial			
			Style.Var('font', 							"tulisan_tangankuregular");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#0A2538");
			Style.Var('color_main_text', 				"#554462");
			Style.Var('color_secondary_back', 			"#84663D");
			Style.Var('color_secondary_text', 			"#071926");
			Style.Var('color_tertiary_back', 			"#B39364");
			Style.Var('color_detail_back', 			"#28655E");
			Style.Var('color_detail_text', 			"#A35140");	
			break;
		case 14 : //Condados del norte			
			Style.Var('font', 							"tulisan_tangankuregular");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#3E652E");
			Style.Var('color_main_text', 				"#992211");
			Style.Var('color_secondary_back', 			"#B0B1B1");
			Style.Var('color_secondary_text', 			"#332211");
			Style.Var('color_tertiary_back', 			"#DDDDDD");
			Style.Var('color_detail_back', 			"#C6C7D7");
			Style.Var('color_detail_text', 			"#1A1C16");		
			break;
		case 15 : //Reino Eterno de Egipto		
			Style.Var('font', 							"Special Elite");
			Style.Var('fontsize', 						"1em");
			Style.Var('color_main_back', 				"#DEB01E");
			Style.Var('color_main_text', 				"#000000");
			Style.Var('color_secondary_back', 			"#A37B3E");
			Style.Var('color_secondary_text', 			"#000000");
			Style.Var('color_tertiary_back', 			"#9B6F32");
			Style.Var('color_detail_back', 			"#BF8A02");
			Style.Var('color_detail_text', 			"#000000");		
			break;
		}
	}
}
