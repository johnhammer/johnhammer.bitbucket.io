var LIB = {	
	Capitalize : function(string){		
		return string.charAt(0).toUpperCase() + string.slice(1);
	},
	Toggle : function(node){
		if(node.style.display == "none"){
			node.style.display = "block";
		}
		else{
			node.style.display = "none";
		}
	},
	Crypt : function(me, key){
		key = Number(String(Number(key))) === key ? Number(key) : 13;
		me = me.split('').map(function(c){return c.charCodeAt(0);}).map(function(i){return i ^ key;});
		me = String.fromCharCode.apply(undefined, me);
		return me;
	},
	Encrypt : function(me, key="entropy"){
		return LIB.Crypt(window.btoa(me),key);
	},
	Decrypt : function(me, key="entropy"){
		return window.atob(LIB.Crypt(me,key));
	},
	Clone : function(obj){
		return JSON.parse(JSON.stringify(obj));
	},
	_counter : 0,
	Guid : function() {
	  function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
	      .toString(16)
	      .substring(1);
	  }
	  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	},
	Counter : function() {
		this._counter++;
	  return this._counter;
	},
	StringToColor : function(str) {
		var hash = 0;
		for (var i = 0; i < str.length; i++) {
			hash = str.charCodeAt(i) + ((hash << 5) - hash);
		}
		var colour = '#';
		for (var i = 0; i < 3; i++) {
			var value = (hash >> (i * 8)) & 0xFF;
			colour += ('00' + value.toString(16)).substr(-2);
		}
		return colour;
	},
	RandomColor : function(brightness){
		function randomChannel(brightness){
			var r = 255-brightness;
			var n = 0|((Math.random() * r) + brightness);
			var s = n.toString(16);
			return (s.length==1) ? '0'+s : s;
		}
		return '#' + randomChannel(brightness) + randomChannel(brightness) + randomChannel(brightness);
	},
	Replace : function(str, find, replace){
		return str.replace(new RegExp(find, 'g'), replace);
	}
	
}

var _Random = {
  GetSeed : function(seed,a,b){
    return a*123 + seed*456 + b*789;
  },
  RunN : function(range,callback){
    var result = [];
    for(var i=0;i<range;i++){
      result.push(callback());
    }
    return result;
  }
}

var Random = {
  Create : function(seed) {
    obj = {}
    // LCG using GCC's constants
    obj.m = 0x80000000; // 2**31;
    obj.a = 1103515245;
    obj.c = 12345;

    obj.state = seed ? seed : Math.floor(Math.random() * (obj.m - 1));
    return obj;
  },
  MultiSeed : function(seed,a,b){
    return Random.Create(_Random.GetSeed(seed,a,b));
  },
  MultiSeed2 : function(seed,a,b,c,d){    
    return Random.MultiSeed(seed,_Random.GetSeed(seed,a,c),_Random.GetSeed(seed,b,d));
  },
  Int : function(obj) {
    obj.state = (obj.a * obj.state + obj.c) % obj.m;
    return obj.state;
  },
  Bool : function(obj){
    return Random.Range(obj,0,1);
  },
  Float : function(obj) {
    return Random.Int(obj) / (obj.m - 1);
  },
  Range : function(obj,start,end) {
    var rangeSize = end+1 - start;
    var randomUnder1 = Random.Int(obj) / obj.m;
    return start + Math.floor(randomUnder1 * rangeSize);
  },
  FloatArea : function(obj,area) {
    return area-(area*2) * Random.Float(obj);
  },
  FRange : function(obj,start,end){
    return Random.Float(obj) * (end - start) + start;
  },
  Chance : function(obj,chance){
    var val = Random.Range(obj,1,chance);
    return val == chance;
  },
  Choice : function(obj,array) {
    return array[Random.Range(obj,0, array.length-1)];
  },
  WChoice : function(obj,array) {    
    var val = Random.Range(obj,0,100);
    var sum = 0;
    for(var i=0;i<array.length;i++){
      sum += array[i].w;
      if(val<=sum){
        return array[i].v;
      }
    }
  },
  Multichoice : function(obj,array,min,max){
    return _Random.RunN(Random.Range(obj,min,max),function(){return Random.Choice(obj,array)})
  },
  WMultichoice : function(obj,array,min,max){
    return _Random.RunN(Random.Range(obj,min,max),function(){return Random.WChoice(obj,array)})
  },
}