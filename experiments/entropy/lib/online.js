
var Online = {
	pc1:null,
	pc2:null,
	activedc:null,
	currentRoom:null,
	roomList:null,
	
	useServer:true,
	
	Raw : {
		Receive : function(message){
			if(message.data) Listener.Listen(JSON.parse(message.data));
		},
		Send : function(message){
			if(message) Online.activedc.send(JSON.stringify(message));
		}		
	},
	CreateManual : function(){
		dc1 = Online.pc1.createDataChannel('entropy', {reliable: true})
		Online.activedc = dc1
		dc1.onopen = function(e) { }
		dc1.onmessage = Online.Raw.Receive;
		Online.pc1.createOffer(function(desc) {
			Online.pc1.setLocalDescription(desc, function() {}, function() {})
		}, function() { }, {optional: []})
	},
	JoinManual : function(){
		Online.useServer = false; 
		var room = LIB.Decrypt(prompt("Enter room info", ""));		
		if (room != null) {
			var offerDesc = new RTCSessionDescription(JSON.parse(room))
			Online.pc2.setRemoteDescription(offerDesc)
			Online.pc2.createAnswer(function(answerDesc) {
				Online.pc2.setLocalDescription(answerDesc);
			},
			function () { },{optional: []})
		}		
	},
	Allow : function(){
		var room = LIB.Decrypt(prompt("Enter player info", ""));	
		if (room != null) {
			var answerDesc = new RTCSessionDescription(JSON.parse(room));
			Online.pc1.setRemoteDescription(answerDesc);			
		}
	},	
	Init : function(){
		if (navigator.webkitGetUserMedia) {
			RTCPeerConnection = webkitRTCPeerConnection
		}

		var cfg = {'iceServers': [{'url': "stun:stun.gmx.net"}]},con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
		
		
		//pc 1
		Online.pc1 = new RTCPeerConnection(cfg, con), dc1 = null, tn1 = null, Online.activedc, Online.pc1icedone = false;
		
		Online.pc1.onicecandidate = function (e) {
			if (e.candidate == null) {
				var room = JSON.stringify(Online.pc1.localDescription);
				Room.ShowInfo(room);
				
				//alert(room);				
			}
		}
		
		//pc 2
		Online.pc2 = new RTCPeerConnection(cfg, con), dc2 = null, Online.pc2icedone = false;

		Online.pc2.onicecandidate = function (e) {
			if (e.candidate == null) {
				var desc = JSON.stringify(Online.pc2.localDescription);			
				Room.ShowInfo(desc);				
				//alert(desc);
			}
		}
		
		Online.pc2.ondatachannel = function (e) {
			var datachannel = e.channel || e;
			dc2 = datachannel
			Online.activedc = dc2
			dc2.onopen = function (e) { 
				Protocol.Enter();
			}
			dc2.onmessage = Online.Raw.Receive;
		}	
	}
}
