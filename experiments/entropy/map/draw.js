var t = 0, dt = 0.00002;

var Draw_Map = {
    Map : function(){
        Draw._Init(map_canvas,Events.Map,Draw_Map._MoveFleets);

        for(var i in GLOBAL.Map){
            Draw_Map._PaintMapMesh(GLOBAL.Map[i]);
        }

		//paint player worlds
		for(var i=0;i<PLAYERS.length;i++){
			for(var j=0;j<PLAYERS[i].Worlds.length;j++){
				PLAYERS[i].Worlds[j].mesh.material = MaterialsFactory.Mask(PLAYERS[i].Info.Color);
			}			
		}		
		
        var light = new THREE.AmbientLight( 0x555555 );
        scene.add( light );
    },
    SelectMapObject : function(object){
        if(CURRENT.SelectedMesh) CURRENT.SelectedMesh.material = MaterialsFactory.select(); //former paint
        object.material = MaterialsFactory.selected();		
        controls.target.set(object.obj.x,object.obj.y,object.obj.z);
    },
    _MoveFleets : function(){
        var fleets = CURRENT.MovingFleets;
        for(i in fleets){
            if(fleets[i].position.distanceTo(new THREE.Vector3(
                    fleets[i].obj.objective.x,
                    fleets[i].obj.objective.y,
                    fleets[i].obj.objective.z))<0.5){
                Events.FleetReachedObjective(i);
            }
            else{
                var a = fleets[i].position;
                var b = fleets[i].obj.objective;	
                
                fleets[i].position.x += (b.x - a.x) * CURRENT.Tick;
                fleets[i].position.y += (b.y - a.y) * CURRENT.Tick;
                fleets[i].position.z += (b.z - a.z) * CURRENT.Tick;
            }		
        }
        CURRENT.Tick += CONST.TickSpeed;
    },
    DeployFleet : function(fleet,originId,destinyId,player,order){
        var origin = GLOBAL.Map[originId];
        var destiny = GLOBAL.Map[destinyId];

        var fleet = {
            x : origin.x,	
            y : origin.y,	
            z : origin.z,
            id : fleet,
            objective : destiny,
            origin : origin,
            player: player,
            type: ENUM.MapObject.Fleet,
            props: {
                matter:0,
                maxcargo:FleetCalculator.GetFleetCargo(PLAYERS[player].Fleets[fleet].ships,PLAYERS[player].Ships),
                
            },
            order : order      
        };
        var mesh = Draw_Map._PaintMapMesh(fleet);		
        mesh.lookAt(destiny.x,destiny.y,destiny.z);
                
        CURRENT.MovingFleets[fleet.id] = mesh;
        // write menu return order
    },
    ReturnFleet : function(id){	
        var fleet = CURRENT.MovingFleets[id];
        fleet.obj.objective = fleet.obj.origin;
        fleet.obj.retreat = true;
        fleet.lookAt(fleet.obj.objective.x,fleet.obj.objective.y,fleet.obj.objective.z);	
    },
    StandbyFleet : function(id){	
        var fleet = CURRENT.MovingFleets[id];
        delete CURRENT.MovingFleets[i];	
        fleet.obj.objective = null;
        fleet.lookAt(fleet.obj.origin.x,fleet.obj.origin.y,fleet.obj.origin.z);	
    },     
    _PaintMapMesh : function(obj){
        var mesh;
        switch(obj.type){
            case ENUM.MapObject.Planet: 
                mesh = AstronomyMeshFactory.BuildWorld(obj); break;
            case ENUM.MapObject.Asteroids:
                mesh = AstronomyMeshFactory.BuildField(obj); break;
            case ENUM.MapObject.Gas: 
                mesh = AstronomyMeshFactory.BuildGasCloud(obj); break;
            case ENUM.MapObject.Dust: 
                mesh = AstronomyMeshFactory.BuildDustCloud(obj); break;
            case ENUM.MapObject.Fleet: 
                mesh = AstronomyMeshFactory.BuildFleet(obj); break;
        }
        mesh.obj = obj;
		obj.mesh = mesh;
        mesh.position.x = obj.x;
        mesh.position.y = obj.y;
        mesh.position.z = obj.z;
        scene.add(mesh);
        return mesh;
    },
}