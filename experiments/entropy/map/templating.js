Templating.Map = function(){
    return `
        <div class="l-main l-canvas">
			<canvas id="map_canvas"></canvas>
			<div>
				<div id="map_object"></div>
				<div>
					<h2>Fleets</h2>
					<table id="fleets_list"></table>
					<button onclick="Menu.AddFleet()">Add fleet</button>
				</div>
			</div>
		</div>
    `;	
}

Templating.FleetInMap = function(id,name,orders,routeDetails){
    return `
        <tr>
            <td onclick="Menu.EditFleet(${id})">${name}</td>		
			<td>Time:${routeDetails.Time}s Fuel:${routeDetails.Fuel}T</td>
			<td ${orders.Move?`onclick="Menu.DeployFleet(${id},${ENUM.MapAction.Move})"`:'class="disabled"'}>→</td>
			<td ${orders.Route?`onclick="Menu.DeployFleet(${id},${ENUM.MapAction.Route})"`:'class="disabled"'}>⇄</td>
        </tr> 
    `;	
}

Templating.MapObject = function(name,matter,maxmatter){
	return `
		<h1>${name}</h1>
		<h2>${matter}/${maxmatter}</h2>
	`;
}

Templating.MapFleet = function(name,playername,matter,maxmatter,myfleet){	
	return `
		<h1>Fleet ${name}</h1>
		${myfleet?'':`<h2> of player ${playername}</h2>`}
		<h2>${matter}/${maxmatter}</h2>
	`;
}