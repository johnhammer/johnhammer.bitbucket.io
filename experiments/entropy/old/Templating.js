/* Templating */

var TEMPLATING = {
	Room:function(data){
		var template = `
			<div class="column">	
				<div class="panel ship_main">
					<h1 class="cell cs_tertiary title ship_main_name js_fleet_name">Room</h1>
				</div>
				<div class="panel ship_parts_container js_ship_parts_container">
					<p class="list_element cs_secondary map_fleet_name" onclick="GoToShip()">+Add player</p>
				</div>
			</div>
			<div class="column">
				<div  class=" panel ships_righttop">
					<div id="container">
						<div id="ships_container"></div>
					</div>
				</div>
				<div class="panel ships_rightbottom js_fleet_properties_container">
				</div>
			</div>
		`;
		
		return template;
	}
}