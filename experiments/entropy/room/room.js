
var Room = {
	OpenFactionModal : function(){
		Templating.Modal(Templating.Factions(PLAYER_ID));
	},
	ShowFactionContent : function(faction){
		faction_info.innerHTML = Templating.FactionInfo(PLAYER_ID,faction);
	},
	ChangePlayerName : function(name){
		PLAYERS[PLAYER_ID].Info.Name = name;
		Room.UpdatePlayers();
	},
	ChangePlayerColor : function(color){
		PLAYERS[PLAYER_ID].Info.Color = color;
		Room.UpdatePlayers();
	},
	ChangePlayerFaction : function(faction){
		PLAYERS[PLAYER_ID].Info.Civ = faction;
		Templating.CloseModal();
		Room.UpdatePlayers();
	},

	UpdatePlayers : function(players=false){
		if(ISONLINEGAME){
			if(players){
				PLAYERS = players;
			}
			else{
				Protocol.Room(PLAYERS);
			}
		}
		Room.WritePlayers();
	},


	WritePlayers : function(){
		players.innerHTML = Templating.Players(PLAYERS);
	},
	Single : function(){
		ISONLINEGAME = false;
		
		this.AddHuman();
		PLAYER_ID = 0;
		
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_start);
	},	
	Create : function(){
		this.AddHuman();	
		PLAYER_ID = 0;
	
		Online.Init();	
		Online.useServer = false;
		Online.CreateManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_allow);
		LIB.Toggle(room_start);
		LIB.Toggle(chat);
	},
	Join : function(){
		Online.Init();	
		Online.JoinManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);	
		LIB.Toggle(chat);		
	},
	Allow : function(){
		Online.Allow();
	},
	StartGame : function(){		
		if(ISONLINEGAME){
			Protocol.StartGame();
		}
		Screen.Game();
	},
	AddPlayer : function(human){		
		Start.AddGlobalPlayer(human);		
		Room.WritePlayers();		
	},	
	AddHuman : function(){
		this.AddPlayer(true);
	},
	AddAI : function(){		
		this.AddPlayer(false);
		if(ISONLINEGAME){
			Protocol.Welcome();
		}
	},
	GetPlayers : function(){			
		return PLAYERS;
	},
	SetPlayers : function(playersData){		
		if(!PLAYER_ID){
			PLAYER_ID = playersData.length-1;
		}
		PLAYERS = playersData;
		Room.WritePlayers();
	},	
	ShowInfo : function(info){
		room_code.value = LIB.Encrypt(info);
	}
}
