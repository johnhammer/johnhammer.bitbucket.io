var ShipCalculator = {
	Normalize(number1,number2){
		return Math.floor((number1 / number2) * 100)
	},
	GetShipProps : function(s){
		var piezas 		= s.Weapon + s.Ammo + s.Shield + s.Engine + s.Fuel + s.Habitat + s.Radio + s.Computer;
		var nada		= s.Size - piezas;	
		
		return {		
			FirePower		: this.Normalize(s.Ammo*2 + ((s.Weapon>0) ? 1 : 0)										,16),
			FireSpeed		: this.Normalize(s.Weapon	                                                          	,8), 
			Precision		: this.Normalize(((s.Weapon>0) ? (32+(s.Radio+1) * (s.Habitat+1) - s.Ammo*4) : 0)	   	,96), 
			ElectricAttack	: this.Normalize((s.Habitat+1)*s.Computer*2 + s.Radio*s.Computer 	                    ,40),
			Armor			: this.Normalize(s.Shield*3 + s.Size	                                               	,32), 
			Tripulation	    : this.Normalize(s.Habitat*4	                                    					,32),
			Evasion			: this.Normalize(64 + (s.Engine*3+1) * (s.Radio+1) * (s.Habitat*2+1) - (s.Size)*8	    ,210),
			Camouflage		: this.Normalize(16-s.Engine-s.Size+1	                                                ,16),
			Speed			: this.Normalize(16+s.Engine*3-s.Size-piezas+1	                                       	,25), 
			Perception		: this.Normalize(s.Radio+1	                                                        	,9),
			Autonomy		: this.Normalize(8+s.Fuel-s.Engine+1	                                                ,17), 
			CargoCapacity	: this.Normalize(nada	                                                           		,8)
		}
	}
}