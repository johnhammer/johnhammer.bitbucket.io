var Draw_Ship = {
    UpdateShip : function(){

    },
	Explosion : function(size){
		var mesh = new THREE.Mesh(
			new THREE.IcosahedronGeometry( .1, 4 ),
			MaterialsFactory.Explosion()
		);
		mesh.scale.set(size,size,size);		

		ANIMATED.Explosions.push(mesh);
		return mesh;
	},
    Ship : function(parts){
		ANIMATED = {
			Ships : [],
			Bullets : [],
			Explosions : []
		};
		
        Draw._Init(ship_canvas,null,Draw_Ship.RenderShip);

        var ship = Draw_Ship.CreateShipMesh(parts);        
        scene.add( ship ); 
		
        var light = new THREE.AmbientLight( 0x555555 );
        scene.add( light );

        var lights = [];
		lights[ 0 ] = new THREE.PointLight( 0xffffff, 1, 0 );
		lights[ 1 ] = new THREE.PointLight( 0xffffff, 1, 0 );
		lights[ 2 ] = new THREE.PointLight( 0xffffff, 1, 0 );

		lights[ 0 ].position.set( 0, 200, 0 );
		lights[ 1 ].position.set( 100, 200, 100 );
		lights[ 2 ].position.set( - 100, - 200, - 100 );

		scene.add( lights[ 0 ] );
		scene.add( lights[ 1 ] );
		scene.add( lights[ 2 ] );
    },
    ShotBullet : function(pos,vector){	
        var x = pos.x //+ Math.random()*2-1;
        var y = pos.y //+ Math.random()*2-1;
        var z = pos.z //+ Math.random()*2-1;
        
        var material = MaterialsFactory.ShipMaterial();
        
        var object = new THREE.Mesh(lib.sph(.1).m.geometry,material);
        object.position.set(x,y,z);
        
        scene.add(object);
        
        object.ttl = 100 + Math.random()*50-25;
        object.vector = vector;
        
        ANIMATED.Bullets.push(object);
    },    
    MoveBullet : function(bullet,i){
        if(!ANIMATED.Bullets[i]) return;
    
        if(bullet.ttl<0){
            
			var expl = Draw_Ship.Explosion(0.1);
			expl.position.set(bullet.position.x, bullet.position.y, bullet.position.z);
			scene.add(expl);
			
            scene.remove(bullet);	
            ANIMATED.Bullets[i] = null		
        }
        bullet.ttl--;

        bullet.position.x += bullet.vector.x * 0.5;
        bullet.position.y += bullet.vector.y * 0.5;
        bullet.position.z += bullet.vector.z * 0.5;

    },
	RenderShip : function(){
		//explosions
		for(var i = 0; i < ANIMATED.Explosions.length; i++){			
			if(ANIMATED.Explosions[i]){
				ANIMATED.Explosions[i].material.uniforms['time'].value += .009;
				if(ANIMATED.Explosions[i].material.uniforms['time'].value>1.5){
					scene.remove(ANIMATED.Explosions[i]);
					ANIMATED.Explosions[i] = null;
				}
			}			
		}
        //bullets
        for(var i = 0; i < ANIMATED.Bullets.length; i++){
            Draw_Ship.MoveBullet(ANIMATED.Bullets[i],i);
        }
        //ships
        for(var j = 0; j < ANIMATED.Ships.length; j++){    

            //cannons
            for(var i = 0; i < ANIMATED.Ships[j].Cannons.length; i++){ 
                var obj = ANIMATED.Ships[j].Cannons[i];
                if(obj.position.y>0.5){
                    if(!obj.WorldPos){
                        obj.WorldPos = new THREE.Vector3()
                            .setFromMatrixPosition( obj.matrixWorld );
                    }
                    Draw_Ship.ShotBullet(obj.WorldPos,obj.Rot);			
                    obj.incr = 2;
                }
                else if(obj.position.y<0 && obj.incr==2){				
                    obj.incr = 0;
                }                
                if(obj.incr == 1){			
                    obj.position.y+=0.019;
                }
                else if(obj.incr == 2){
                    obj.position.y-=0.08;
                }
            }
        }     
    },
	CreateShipMesh : function(ship){
		if(ship.Size == 8) return this.CreateShipMesh8(ship);
		if(ship.Size == 4) return this.CreateShipMesh4(ship);
		if(ship.Size == 2) return this.CreateShipMesh2(ship);
		if(ship.Size == 1) return this.CreateShipMesh1(ship);
	},
	CreateShipMesh1 : function(ship){
		var sphereBody = ship.Shield + ship.Radio + ship.Habitat;
		
		var geo = lib.assemble([
			lib.altcyl(.4,.5,.5),
			lib.iif(lib.move(lib.skip(lib.multiRot(lib.littleWing(1),4+ship.Engine,1)),-.8),!ship.Radio),
			
			lib.iif(lib.skip(lib.move(lib.multiRot(lib.skew(lib.altcyl(.1,.2,4),12),4,2.35),-3)),ship.Radio),
			
			lib.altcyl(.8,1*lib.f(ship.Ammo,.5),2-sphereBody),
			
			lib.iif(lib.skip(lib.multi(lib.altcyl(.5,0,3),4,1.1)),ship.Weapon),
			lib.iif(lib.skip(lib.multiRot(lib.flip(lib.cyl(.3,1)),4,0.5)),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.65,2),7,.6)),ship.Fuel),
			
			lib.iif(lib.altcyl(1*lib.f(ship.Ammo,.5),.8,2),ship.Ammo + ship.Engine),			
			lib.iif(lib.sph(2),sphereBody),
			lib.iif(lib.cyl(1*lib.f(ship.Nada,.5),3),ship.Nada),
			
			lib.iif(lib.move(lib.tor(2.2,.5),-1.5),ship.Shield),	

			lib.iif(lib.move(lib.multiRot(lib.skew(lib.cyl(.3,2),8),4,1.5),-2.5),ship.Radio),
						
			lib.iif(lib.altcyl(.8,0,1.5),ship.Engine+ship.Weapon),
			
			lib.iif(lib.move(lib.sph(.8),-.5),ship.Ammo),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,.8),4,.6)),ship.Computer),
			lib.iif(lib.cyl(.3,1),ship.Computer)			
		]);	

		var ShipObj = {};
		ShipObj.mesh = new THREE.Mesh(geo,MaterialsFactory.ShipMaterial());
		return ShipObj;
	},
	CreateShipMesh2 : function(parts){
		var ShipObj = {
            Cannons : []
        }; 
		
		var r = .3;
        var rm = .2;
        var l = 1;
		
		var cannon = lib.part(lib.assemble([			
            lib.altcyl(r*lib.f(parts.Ammo,.05),rm,rm),
            lib.altcyl(r*lib.f(parts.Ammo,.1),rm,rm),
			lib.cyl(rm,1.2*lib.f(parts.Ammo,.05)),
			lib.cyl(.1,1),
			lib.cyl(.12,.5),
		]));
		
		var ship = lib.assembleM([		
			lib.altcyl(rm/2,rm,rm),
			lib.cyl(rm,rm),
			
			lib.skip(lib.move(lib.multiRot(lib.cyl(rm/2,.8),2+parts.Weapon,r+rm/2),.5-.8)),
			
			lib.cyl(r,l),
			
			lib.iifA(()=>lib.skip(lib.move(lib.multiRotM(cannon,2+parts.Weapon,r*3,ShipObj.Cannons),-.8)),parts.Weapon),            
            lib.iif(lib.skip(lib.move(lib.multiRot(lib.cyl(r,.8),2+parts.Weapon,r*3),.5-.8)),parts.Weapon),
			
			lib.cyl(l/1.5,rm),
			lib.cyl(r,r),
			lib.altcyl(r,rm,r),
			lib.altcyl(rm,0,l/1.5),
        ]);
		
		return ship;
	},
    CreateShipMesh4 : function(parts){        
        var ShipObj = {
            Cannons : []
        };        
        var r = .3;
        var rm = .2;
        var l = 1;
		
		var cannon1 = lib.part(lib.assemble([			
            lib.altcyl(r*lib.f(parts.Ammo,.05),rm,rm),
            lib.altcyl(r*lib.f(parts.Ammo,.1),rm,rm),
			lib.cyl(rm*lib.f(parts.Ammo,.05),1.2*lib.f(parts.Ammo,.2)),
			lib.cyl(.1*lib.f(parts.Ammo,.05),1.5*lib.f(parts.Ammo,.1)),
			lib.cyl(.12*lib.f(parts.Ammo,.05),.5),
		]));
		
		var cannon2 = lib.part(lib.assemble([			
            lib.altcyl(r*lib.f(parts.Ammo,.05),rm,rm),
            lib.altcyl(r*lib.f(parts.Ammo,.1),rm,rm),
			lib.cyl(rm,1.2*lib.f(parts.Ammo,.05)),
			lib.cyl(.1,1),
			lib.cyl(.12,.5),
		]));
		
		var ship = lib.assembleM([				
			//Engines
			lib.iif(lib.skip(lib.multiRot(
				lib.altcyl(rm/1.5*lib.f(parts.Engine,.2),r/1.5*lib.f(parts.Engine,.2),rm*lib.f(parts.Engine,.4)),
				2+parts.Engine,r*lib.f(parts.Engine,.5))),
			parts.Engine),
            lib.altcyl(rm*lib.f(parts.Engine,.5),r*lib.f(parts.Engine,.5),rm*lib.f(parts.Engine,.4)),
			lib.altcyl(r*lib.f(parts.Engine,.5),r*lib.f(parts.Engine,.1),l*lib.f(parts.Engine,.2)),
			
			//Fuel
            lib.cyl(rm,rm*lib.f(parts.Fuel,.2)),
            lib.altcyl(rm,r*lib.f(parts.Fuel,.2),l/2),
            lib.skip(lib.multi(lib.cyl(rm,l),5+parts.Fuel*2,rm*2*lib.f(parts.Fuel,.2))),            
            lib.cyl(r*lib.f(parts.Fuel,.2),l+rm),
            lib.cyl(rm,rm*lib.f(parts.Fuel,.2)), 
			
			//Human
            lib.cyl(r*lib.f(parts.Habitat,.2),l*lib.f(parts.Habitat,.3)),  			
			
			//Main weapons
			lib.iifA(()=>lib.skip(lib.move(lib.multiRotM(cannon1,4+parts.Weapon,l*2*lib.f(parts.Ammo,.02),ShipObj.Cannons),-.8)),parts.Weapon),            
            lib.iif(lib.skip(lib.move(lib.multiRot(lib.cyl(r*lib.f(parts.Ammo,.1),.8*lib.f(parts.Ammo,.2)),4+parts.Weapon,l*2*lib.f(parts.Ammo,.02)),.5-.8)),parts.Weapon),
            lib.cyl(l*2,l/3*lib.f(parts.Ammo,.5)),
			
			//Cargo
            lib.cyl(l,l/2),			
            lib.altcyl(l,rm,r),
			
			//Computer
            lib.cyl(rm,rm*lib.f(parts.Computer,.5)),
            lib.cyl(r,rm),
			
			//Radio
            lib.cyl(l*lib.f(parts.Radio,.2),r/2),
			
			//Shields
            lib.altcyl(r,l/3*2,l*lib.f(parts.Shield,.2)),     
            lib.altcyl(l/3*2,(parts.Ammo==4?(0):(l*lib.f(parts.Shield,.2))),l),   
			
			//Extra weapons
			lib.iifA(()=>lib.skip(lib.move(lib.multiRotM(cannon2,2+parts.Weapon,l,ShipObj.Cannons),-.8)),parts.Weapon,3),            
            lib.iif(lib.skip(lib.move(lib.multiRot(lib.cyl(r,.8),2+parts.Weapon,l),.5-.8)),parts.Weapon,3),			
        ]);
        
        MaterialsFactory.SetMeshMaterial(ship,MaterialsFactory.ShipMaterial());
              
        ship.position.x = 5; 
        var shipAngleZ = 0//Math.PI/2;//Math.PI/3;
        //ship.rotation.z = shipAngleZ;

        for(var i = 0; i < ShipObj.Cannons.length; i++){             
			ShipObj.Cannons[i].position.y = Random.FloatArea(Random.Create(),0.5);

            var vector = new THREE.Vector3( 0, 1, 0 );
            var axis = new THREE.Vector3( 0, 0, 1 );
            var angle = shipAngleZ;

            vector.applyAxisAngle( axis, angle );

            ShipObj.Cannons[i].Rot = vector;
        }

		
        ANIMATED.Ships.push(ShipObj);

		var light = new THREE.PointLight();
		ship.add( light );

		ShipObj.mesh = ship;
		return ShipObj;
	},
	ShotCannons : function(ship){
		for (i = 0; i < ship.Cannons.length; i++) { 
			ship.Cannons[i].incr=1;
		}		
	},
	AimCannons : function(ship,objective){

		var a = objective.position;		
		
		ship.mesh.rotation.z=ship.mesh.rotation.x;
		ship.mesh.rotation.x=0;
		ship.mesh.lookAt(a);
		

		for (i = 0; i < ship.Cannons.length; i++) { 
			
			
			//ship.Cannons[i].Rot = ship.mesh.rotation;
		}
	}
}