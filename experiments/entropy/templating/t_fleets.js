Templating.Fleets = function(data){
    return `
		<div class="l-main l-four">
			<div>
				<div>
					<button onclick="Menu.SaveFleet()">✓</button>
					<button onclick="Menu.CancelFleet()">✗</button>
					<input onchange="" value="New fleet">
				</div>
				<div>                
					<table>${Templating.FleetShips(data.Ships,data.Types)}</table>
					<button onclick="Menu.AddShipType()">Add ship type</button>
				</div>
			</div>
			<div>
				<div>
					<h2>Skills</h2>
					<table class="fleettable unactionable"></table>
					<h2>Orders</h2>
					<table class="fleettable"></table>
				</div>
				<div>
					<h2>Cargo Fleet</h2>
					<div class="bar_container"></div>
				</div>
			</div>
		</div>
    `;	
}

Templating.FleetShips = function(ships,shipTypes){
	var result = '';
	for (var i = 0; i < ships.length; i++) { 
		result += `
			<tr>
				<td onclick="Menu.EditShipType(${i})">✎ ${shipTypes[i].name}</td>
				<td id="ship_amount_${i}">${ships[i]}</td>
				<td id="ship_add_${i}" onclick="Menu.AddShipToFleet(${i})">+</td>
				<td id="ship_remove_${i}" ${ships[i]<1?`class="disabled"`:`onclick="Menu.RemoveShipFromFleet(${i})"`}>-</td>                        
			</tr>
		`
	}
    return result;
}