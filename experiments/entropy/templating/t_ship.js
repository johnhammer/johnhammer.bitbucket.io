Templating.Ship = function(data){
    return `		
		<div class="l-main l-four">
        <div class="l-four-ships">
            <div>
                <button onclick="Menu.SaveShip()">✓</button>
                <button onclick="Menu.CancelShip()">✗</button>
                <input onchange="" value="New ship">
				<table>					
                    <tr id="ship_skills"></tr>
				</table>
            </div>
            <div>				
				<button onclick="Menu.SetShipSize(1)">1</button>
				<button onclick="Menu.SetShipSize(2)">2</button>
				<button onclick="Menu.SetShipSize(4)">4</button>
				<button onclick="Menu.SetShipSize(8)">8</button>	
				
				<h2>Partes</h2>						
                <table id="ship_parts">${Templating.ShipParts(data.Parts)}</table>
            </div>
        </div>
        <div>
            <div>
                <canvas id="ship_canvas"></canvas>
            </div>
            <div class="bar_container" id="ship_props">${Templating.ShipProps(data.Props)}</div>
        </div>
    </div>
					
    `;	
}

Templating.ShipParts = function(parts){
	var result = '';
	for(var part in parts){
		if(part!='Nada'&&part!='Size')
			result+=Templating.ShipPart(Templating.Text(part),part,parts[part]);
	}
	return result;
}

Templating.ShipProps = function(parts){
	var result = '';
	for(var part in parts){		
		result+=Templating.ShipProp(parts[part],parts[part],Templating.Text(part));
	}
	return result;
}

Templating.ShipPart = function(name,key,amount){
	return `
		<tr>
			<td>${name}</td>
			<td id="part_amount_${key}">${amount}</td>
			<td id="part_add_${key}" onclick="Menu.AddPartToShip('${key}')">+</td>
			<td id="part_remove_${key}" ${amount<1?`class="disabled"`:`onclick="Menu.RemovePartFromShip('${key}')"`}>-</td>                        
		</tr>
	`;
}

Templating.ShipSkill = function(name){
	return `
		<td onclick="">Ship Type</td>
	`;
}


Templating.ShipProp = function(amount,percent,name){
	return `
		<div>
			<div>
				<div style="width:${percent}%">${amount}</div>
			</div>
			<div>${name}</div>
		</div>
	`;
}
