Templating.Technologies = function(){	
	return `
        <div class="l-grid">
            <div>
                ${Templating.TechnologiesList()}
            </div>
            <div id="faction_info">
                ${Templating.TechnologyInfo(PLAYER_ID,PLAYERS[PLAYER_ID].Info.Civ)}
            </div>
        </div>
    `;	
}

Templating.FactionImage = function(playerId,i){
	var color = PLAYERS[playerId].Info.Color;
	return `
		<img src=".\\assets\\transparent\\${FACTIONS[i].name}.png" style="background-color:${color}"> 
	`;
}

Templating.FactionsList = function(playerId){
	var result = '';
	for (var i = 0; i < FACTIONS.length; i++) { 
		result += `
			<div onclick="Room.ShowFactionContent(${i})">
				${Templating.FactionImage(playerId,i)}
				<h3>${Templating.Text(FACTIONS[i].name)}</h3>
			</div>
		`
	}
    return result;		
}

Templating.FactionInfo = function(playerId,i){
	var history = "<p>"+LIB.Replace(Templating.Long('History_'+FACTIONS[i].name),"#","</p><p>")+"</p>";
	return `
		<h1>${Templating.Text(FACTIONS[i].name)}</h1>
		${Templating.FactionImage(playerId,i)} 
		<div>
			${history}
		</div>
		<button onclick="Room.ChangePlayerFaction(${i})">Pick this faction</button>
	`;
}