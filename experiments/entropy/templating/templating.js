var Templating = {
    Main : function(content){
        mainbody.innerHTML = content;
    },
    Text : function(key){
        var language = GLOBAL.Language;
        
        switch(language){
            case 0: return key;	//english
            case 1: return SPANISH[key];		
        }
    },    
    Long : function(key){
        var language = GLOBAL.Language;
        
        switch(language){
            case 0: return ENGLISH_LONG[key];
            case 1: return SPANISH_LONG[key];		
        }
    },
    Modal : function(content){
        mainbody.innerHTML += `
            <div id="current_modal" class="modal">
                <div class="modal-content">
                    ${content}
                </div>
            </div>            
        `;
        window.onclick = function(event) {
            if (event.target == current_modal) {
                Templating.CloseModal();
            }
        };
    },
	CloseModal : function(){
		current_modal.outerHTML = '';
		window.onclick = null;
	},
	OpenNav : function() {
	  Nav.style.display = "flex";
	},
	CloseNav : function() {
	  Nav.style.width = "none";
	},
}