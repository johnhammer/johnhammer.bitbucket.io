ENGLISH_LONG = {
    //main menu	
    'Single player':'Play alone against an AI controlled opponent.',
    'Multiplayer':'Online',
    'Tutorial':'Learn how to play.',
    'Options':'Opciones',
    'Language':'Idioma',	
        
    //faction history	
    'History_Empire':'Las fuerzas del eje ganan la segunda guerra mundial, gracias a la neutralidad de Estados Unidos y una ataque relampago a la capital de la URSS.#Poco despues de ganar la guerra el eje se hace con los paises restantes, e inicia una rapida expansion por el sistema solar.#Tras varios siglos de investigacion eugenica surge una nueva raza de superhumanos, mas inteligentes y adaptados a la vida en el espacio.',		
    'History_Union':'Tras ganar la segunda guerra mundial, los aliados traicionan a la URSS y continuan el avance hacia el este mas alla de Berlin.#Contando con China como un poderoso aliado y con varias revoluciones alzandose por todo el mundo, los Soviets terminan por gobernar toda la Tierra.#Empezando con la construccion de ciudades flotantes en Venus, la bandera del el hoz y el martillo termina por ondear en toda la galaxia.',
    'History_League':'Los grandes descubrimientos cientificos en Jonia no quedan olvidados en el tiempo, sino que son utilizados para desarrollar una revolucion industrial en la epoca clasica.#Con el apoyo del reino de Macedonia y su enorme superioridad tecnologica y cientifica, las ciudades Jonicas forman una Liga y capturan facilmente todas las civilizaciones antiguas.#El viaje estelar comienza poco despues de conquistar el mundo, usando grandes naves arca multigeneracionales.',
    'History_Califate':'El Nuevo Califato',	
    'History_Republic':'Tercera Republica Romana',
    'History_Dinasty':'Dinastia Sikh',
    'History_Serpent':'Imperio de la Serpiente',
    'History_Crown':'Imperio Austropruso-Espanol',
    'History_Corporation':'Corporacion Universal',
    'History_Confederation':'Federacion Anarquista',		
    'History_States':'EUU',							
    'History_Colonies':'Imperio Francobritanico',		
    'History_Aliance':'Alianza de Isandlwana',		
    'History_Khanate':'Khanato celestial',			
    'History_Earldoms':'Condados del norte',		
    'History_Kingdom':'Reino Eterno de Egipto',	
    
    //world
    'mine' : 'Extracts pure matter from the world.',	
    'plant' : 'Produces energy consuming matter.',	
    'house' : 'Habitat for 32 people, requires matter.',	
    'recycler' : 'Refines debris into pure matter.',
    'factory' : 'Increases ship production speed.',
    'lab' : 'Increases investigation speed.',	
    'hangar' : 'Allows creation of one fleet.',
    'bunker' : 'Adds defense power in case of invasion.',
    
    // ship parts
    'Weapon': 'Weapons add rate of fire to the ship.',
    'Ammo': 'The more space for ammunition you have, the more powerful your weapons will be.',
    'Shield': 'The shields of the ship determine its resistance to enemy attacks.',
    'Engine': 'The more engines you have, the faster your ship will be.',
    'Fuel': 'The more fuel tanks you have, the longer your ship can be operational.',
    'Habitat': 'Habitats add human crew to the ship. Humans are useful for many things, especially they add intelligence to your ship.',
    'Radio': 'The more radios you have, the better you can perceive your surroundings.',
    'Computer': 'Computers can be used to hack enemy ships, and they also offer intelligence to your ship.',
    
    // ship props
    'FirePower': 'Firepower determines how much damage your shots cause.',
    'FireSpeed': 'The rate of fire determines how many times per second you can shoot.',
    'Precision': 'Precision determines what percentage of shots will hit your target.',
    'ElectricAttack': 'Electronic attack capability determines the ability to hack enemy ships.',
    'Armor': 'The defense of the ship determines how much damage it can withstand.',
    'Tripulation': 'The crew indicates the cost in population of the ship.',
    'Evasion': 'Evasion determines what percentage of enemy shots will be dodged by the ship.',
    'Camouflage': 'Camouflage indicates the probability of the ship being detected by the enemy.',
    'Speed': 'Speed ​​indicates how fast the ship can reach its destination.',
    'Perception': 'Perception determines how far the ship can detect objects or fleets.',
    'Autonomy': 'The autonomy determines how long the ship can be operational without refueling.',
    'CargoCapacity': 'Cargo capacity indicates how much material the ship can transport.',
    
    // ship skills
    'Glory':				'Faction units have bonus morale.',
    'SpaceLab':				'can perform scientific mission',	
    'Colonizer':			'can perform colonization mission',	
    'Carrier':				'bonus fleet autonomy',
    'Projectile':			'suicide attack',
    'Kamikaze':				'bonus evasion and damage',
    'Missile':				'bonus speed',
    'Bomb':					'bonus damage',
    'DirtyBomb':			'bonus against enemy humans',
    'Emp':					'emp',
    'Espionage':			'can perform espionage missions',
    'PlanetBuster':			'can destroy planets',	
    'ArmorPiercer':			'bonus against big ships',
    'FlyingBunker':			'bonus fleet defence',	
    'NeuralCenter':			'bonus fleet intelligence',
    'BigEar':				'bonus fleet perception',	
    'IntelligentFire':		'bonus against enemy fuel and computers',	
    'RapidFire':			'bonus against small ships',
    'FlakAA':				'bonus against projectiles',
    'Hackathon':			'bonus fleet electronic attack',
    'AntiRadiationGun':		'bonus against enemy radios',	
    'Artillery':			'bonus against enemy armor',	
    'PrecisionBombardment':	'bonus fleet precision',	
    'Hacker':				'electronic bonus against unmanned ships',
    'Bombardment':			'bonus against surface objectives',
    }