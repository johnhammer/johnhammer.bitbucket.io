/*
'Empire':'卐 Imperio',		
'Union':'☭ Union ',
'League':'Liga',
'Califate':'☪︎ Califato',	
'Republic':'Ⅲ Republica',
'Dinasty':'☬ Dinastia',
'Serpent':'🐉 Serpiente',
'Crown':'✖ Corona',
'Corporation':'● Corporacion',
'Confederation':'◥ Confederacion',		
'States':'◯ Estados',							
'Colonies':'✚ Colonias',		
'Aliance':'☦ Alianza',		
'Khanate':'☾ Khanato',			
'Earldoms':'◊ Condados',		
'Kingdom':'☥ Reino',	
*/

SPANISH = {
    //main menu	
    'Single player':'Un Jugador',
    'Multiplayer':'Online',
    'Create':'Crear',
    'Join':'Unirse',
    'Add AI':'Anadir IA',
    'Add Player':'Anadir Jugador',
    'Go!':'Jugar!',
    'Tutorial':'Tutorial',
    'Options':'Opciones',
    'Language':'Idioma',
    'Copy':'Copia',
    'Not available yet':'Opcion no disponible',
    'Pick a color:':'Elige color:',
            
    //factions
    'Empire':'Imperio',		
    'Union':'Union ',
    'League':'Liga',
    'Califate':'Califato',	
    'Republic':'Republica',
    'Dinasty':'Dinastia',
    'Serpent':'Serpiente',
    'Crown':'Corona',
    'Corporation':'Corporacion',
    'Confederation':'Confederacion',		
    'States':'Estados',							
    'Colonies':'Colonias',		
    'Aliance':'Alianza',		
    'Khanate':'Khanato',			
    'Earldoms':'Condados',		
    'Kingdom':'Reino',		
    
    //world
    'mine' : 'mina',	
    'plant' : 'planta',	
    'house' : 'casa',	
    'recycler' : 'reciclador',
    'factory' : 'fabrica',
    'lab' : 'laboratorio',	
    'hangar' : 'hangar',
    'bunker' : 'bunker',
    
    //map
    'Planet':'Planeta',
    'Dust cloud':'Nube de polvo',
    'Gas cloud':'Nube de gas',
    'Asteroids':'Asteroides',
        
    //ships 1
    'Fighter drone':'Dron cazador',
    'Bomb':'Bomba',
    'Decoy drone':'Dron senuelo',
    'Missile':'Misil',
    'Mother drone':'Dron nodriza',
    'Explorer':'Explorador ligero',
    'Probe':'Sonda',
    'EMP pulse':'Pulso EMP',
    'Cargo drone':'Dron de carga',
        
    //ship skills	
    
    //ship parts
    'Weapon':'Arma',
    'Ammo':'Munición',
    'Shield':'Escudo',
    'Engine':'Motor',
    'Fuel':'Combustible',
    'Habitat':'Habitat',
    'Radio':'Radio',
    'Computer':'Ordenador',
    
    //ship props
    'FirePower':'Potencia de fuego',
    'FireSpeed':'Cadencia de fuego',
    'Precision':'Precisión',
    'ElectricAttack':'Ataque electrónico',
    'Armor':'Defensa',
    'Tripulation':'Tripulación',
    'Evasion':'Evasión',
    'Camouflage':'Camuflaje',
    'Speed':'Velocidad',
    'Perception':'Percepción',
    'Autonomy':'Autonomia',
    'CargoCapacity':'Capacidad de carga',
        
    }