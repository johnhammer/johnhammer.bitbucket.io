SPANISH_LONG = {	
    //main menu	
    'Single player':'Juega solo contra un oponente controlado por la maquina.',
    'Multiplayer':'Online',
    'Tutorial':'Tutorial',
    'Options':'Opciones',
    'Language':'Idioma',	
        
    //tutorial
    '0World':'Esto es la pantalla del mundo. En este planetoide puedes construir cierto numero de edificios y almacenar recursos.',	
    '1World':'Para ver para que sirve un edificio, situa el cursor sobre su nombre. Para construir haz clic en su imagen y emplazalo en el planetoide a la izquierda.',	
    '2World':'OBJETIVO: construye un hangar.',	
    '0Map':'Bien hecho! Esto es la pantalla del mapa. Si seleccionas un objeto podras ver sus detalles.',	
    '1Map':'Para poder recoger materia de un objeto, debes crear una flota con naves de carga. Selecciona Anadir flota, y crea un nuevo tipo de nave.',	
    '2Map':'OBJETIVO: crea una flota con naves de carga y enviala a recoger la materia de la nube de polvo.',	
    '0Map2':'Buen trabajo. Para completar el tutorial debes destruir la flota enemiga.',	
    '1Map2':'Para ello deberas crear un nuevo hangar, y organizar otra flota con naves de guerra. Luego, enviala a atacar a la flota enemiga.',	
    '2Map2':'OBJETIVO: crea una flota de guerra y destruye la flota enemiga.',	
    'TutorialEnd':'Con esto concluye el tutorial. Gracias por jugarlo!',	
    
	//technologies
	'Computers':'',
	
	'AI':'',
	'Cryptography':'Improves ',
	'Human interface':'Improves ship intelligence and ',
	'Quantum software':'Improves technology output and ',	
	
	'Materials':'',
	
	'Nanostructure':'Improves ship defence and bunker defence.',
	'Antimatter':'Improves ship autonomy and ',
	'Reciclage':'Improves recyclage ratio.',
	'Superconductor':'Improves technology output and buildings efficiency.',
	
	'Phisics':'',
	
	'Radioastronomy':'',
	'Energy':'Improves ship speed and ',
	'Firepower':'',
	'Quantum phisics':'Improves technology output and ',	
	
	'Engineering':'',
	
	'Robotics':'Improves factory efficiency and ',
	'Extraction':'Improves ',
	'Domotics':'Improves house efficiency and human cost in ship.',
	'Electronics':'Improves technology output and ',
	
	//factions
	'Empire':'El Gran Imperio',		
    'Union':'Union Sovietica Universal',
    'League':'Confederacion de Jonia',
    'Califate':'El Nuevo Califato',	
    'Republic':'Tercera Republica Romana',
    'Dinasty':'Dinastia Sikh',
    'Serpent':'Imperio de la Serpiente',
    'Crown':'Imperio Austropruso-Espanol',
    'Corporation':'Corporacion Universal',
    'Confederation':'Federacion Anarquista',		
    'States':'EUU',							
    'Colonies':'Imperio Francobritanico',		
    'Aliance':'Alianza de Isandlwana',		
    'Khanate':'Khanato celestial',			
    'Earldoms':'Condados del norte',		
    'Kingdom':'Reino Eterno de Egipto',	
		
    //faction history	
    'History_Empire':'Las fuerzas del eje ganan la segunda guerra mundial, gracias a la neutralidad de Estados Unidos y una ataque relampago a la capital de la URSS.#Poco despues de ganar la guerra el eje se hace con los paises restantes, e inicia una rapida expansion por el sistema solar.#Tras varios siglos de investigacion eugenica surge una nueva raza de superhumanos, mas inteligentes y adaptados a la vida en el espacio.',	
	
    'History_Union':'Tras ganar la segunda guerra mundial, los aliados traicionan a la URSS y continuan el avance hacia el este mas alla de Berlin.#Contando con China como un poderoso aliado y con varias revoluciones alzandose por todo el mundo, los Soviets terminan por gobernar toda la Tierra.#Empezando con la construccion de ciudades flotantes en Venus, la bandera del el hoz y el martillo termina por ondear en toda la galaxia.',
	
    'History_League':'Los grandes descubrimientos cientificos en Jonia no quedan olvidados en el tiempo, sino que son utilizados para desarrollar una revolucion industrial en la epoca clasica.#Con el apoyo del reino de Macedonia y su enorme superioridad tecnologica y cientifica, las ciudades Jonicas forman una Liga y capturan facilmente todas las civilizaciones antiguas.#El viaje estelar comienza poco despues de conquistar el mundo.',
	
    'History_Califate':'Tras las conquistas iniciales islamicas, el shiismo termina por imponerse, erradicando a los sunnitas y expandiendose por todo el antiguo mundo.#Habiendo derrotado a los poderosos imperios Bizantino, Franco, Persa y Chino, el Califato extiende su fe y dominio por el mundo.#Al verse agotados los recursos de la tierra, el Califa ordena la ocnstruccion de una gran ciudad en la Luna, para expandir la gloria de Allah por la galaxia.',	
	
    'History_Republic':'En la segunda guerra punica, el senado decide formar una alianza con la derrotadda Cartago en lugar de arrasarla.#De ello nace la Tercera Republica, un superpoder mediterraneo que pronto domina Europa y Africa, y se expande rapidamente por el resto del mundo.#Una enorme flota de colonizacion civiliza la galaxia desde su base principal en las ciudades flotantes de Jupiter.',
	
    'History_Dinasty':'China resiste la conquista mongol y contraataca creando asi un gran imperio asiatico, llegando hasta India.#Unificando las culturas y religiones del sudeste asiatico bajo el emblema del Sikhismo, la nueva dinastia domina el mundo.#Fundando una gran ciudad-espaciopuerto en la cara oculta de la luna, grandes naves Sikh despegan hacia las estrellas.',
	
    'History_Serpent':'La peste negra arrasa casi al completo Europa, oriente medio y lejano, dejando a Mesoamerica como la mayor civilizacion humana.#Mediante conquistas y alianzas tribales, un imperio sincretico entre los Aztecas, Mayas e Incas termina dominando a la humanidad.#Estableciendo una nueva nacion en Titan, la bandera de la Serpiente pronto ondea en toda la galaxia.',
	
    'History_Crown':'La guerra de secesion conclye prematuramente con la muerte del candidato borbon, dando la corona de Espana a los austriacos.#Mediante pactos matrimoniales, se realiza una union con Prusia. Posteriores uniones y conquistas terminan tomando Europa y colonizando el mundo.#Empezando por minar extensivamente el cinturon de asteroides, las naves de la Corona surcan la galaxia en busca de nuevos mundos.',
	
    'History_Corporation':'El delicado equilibrio de la guerra fria estalla en una guerra nuclear que aniquila gran parte del mundo.#En el nuevo mundo postapocaliptico, empresas y corporaciones toman rapidamente el poder, y terminan por unirse en una unica Corporacion global.#Con la ayuda de una avanzada inteligencia artificial, la orbita terrestre se llena pronto de colonos, que mas adelante se lanzaran hacia las estrellas.',
	
    'History_Confederation':'En lugar de la doctrina comunista, los pensamientos del anarquismo se extienden por el proletariado europeo.#Tras la victoria en varias revoluciones por todo el mundo, se forma una Confederacion Anarquista global.#La exploracion espacial lleva pronto a construir ciudades submarinas en Titan, y se envian multiples misiones colonizadoras a exoplanetas cada vez mas lejanos.',		
	
    'History_States':'Tras el colapso de la Union Sovietica, Estados Unidos inicia planes de alianza y recuperacion economica en la antigua area sovietica.#Por medio de propaganda y habil diplomacia, se logra una globalizacion cultural controlada por Norteamerica.#Tras varios anos de terraformacion, una prospera colonia se establece en Marte, desde la que se abre acceso a toda la galaxia.',		
	
    'History_Colonies':'Napoleon consigue forjar una alianza con los ingleses, creando asi un superpoder colonial Europeo.#Tras eliminar a sus rivales en europa, el resto del mundo es colonizado por las tropas francobritanicas.#Una vez colonizado el mundo, las siguientes colonias se emplazan en Mercurio, Marte, y las lunas de Jupiter, y mas adelante hacia las estrellas.',		
	
    'History_Aliance':'Cuando los europeos llegan a Africa se encuentran ya con una poderosa nacion, resultado de la alianza entre Etiopia y los Zulus.#Aprovechando la desunion de sus adversarios, la alianza se hace con el Oriente medio y Europa, y des de ahi con el resto del mundo.#Por medio de tecnologias de clonacion, la alianza se expande rapidamente por la galaxia.',	
	
    'History_Khanate':'Los avances de Gengis Khan y sus descendientes no se detienen, al ceder su kanato a un unico heredero evitando asi que se fragmente.# Por medio de brutal conquista y colonizacion, el mundo entero queda bajo dominio turco mongol.#Utilizando gigantescas naves arca multigeneracionales, las hordas del Khan se expanden por la galaxia.',			
	
    'History_Earldoms':'La colonia nordica en Norteamerica prospera gracias al comercio con los nativos.#Formando una confederacion de condados independientes, la cultura nordica y norteamericana se funde y expande por todo el mundo.#Un nuevo condado en la luna de Europa sirve de puerto espacial a una enorme red comercial galactica.',		
	
    'History_Kingdom':'Mientras las demas civilizaciones e imperios surgen y caen a su alrededor, los faraones mantienen su eterno reino.#Con una expansion lenta pero constante, a lo largo de miles de anos la entera Tierra esta bajo el mando del reino.#Mediante avanzadas tecnologias criogenicas y geneticas, el Reino se extiende por toda la galaxia.',	
    
    //world
    'mine' : 'Extrae materia pura del mundo.',	
    'plant' : 'Produce energia a partir de materia.',	
    'house' : 'Habitaje para 32 personas.',	
    'recycler' : 'Refina restos para producir materia.',
    'factory' : 'Augmenta velocidad de produccion de naves.',
    'lab' : 'Augmenta velocidad de investigacion.',	
    'hangar' : 'Permite creacion de una flota.',
    'bunker' : 'Anade poder defensivo en caso de invasion.',
    
    //ship parts
    'Weapon':'Las armas añaden cadencia de fuego a la nave.',
    'Ammo':'Cuanto mas espacio para munición tengas, mas potentes serán tus armas.',
    'Shield':'Los escudos de la nave determinan su resistencia a ataques enemigos.',
    'Engine':'Cuantos más motores tengas, más rápida será tu nave.',
    'Fuel':'Cuantos mas depositos de combustible tengas, más tiempo podrá estar tu nave operativa.',
    'Habitat':'Los habitats añaden tripulación humana a la nave. Los humanos son útiles para muchas cosas, especialmente añaden inteligencia a tu nave.',
    'Radio':'Cuantas mas radios tengas, mejor podra percibir su entorno la nave.',
    'Computer':'Los ordenadores pueden usarse para piratear naves enemigas, y también ofrecen inteligencia a tu nave.',
    
    //ship props
    'FirePower':'La potencia de fuego determina cuanto daño causan tus disparos.',
    'FireSpeed':'La cadencia de fuego determina cuantas veces por segundo puedes disparar.',
    'Precision':'La precision determina que porcentaje de disparos acertarán a su blanco.',
    'ElectricAttack':'La capacidad de ataque electrónico determina la capacidad de hackear naves enemigas.',
    'Armor':'La defensa de la nave determina cuanto daño puede resistir.',
    'Tripulation':'La tripulación indica el coste en población de la nave.',
    'Evasion':'La evasión determina que porcentaje de disparos enemigos serán esquivados por la nave.',
    'Camouflage':'El camuflaje indica la probabilidad de la nave de ser detectada por el enemigo.',
    'Speed':'La velocidad indica cuan rápido la nave puede llegar a su destino.',
    'Perception':'La percepción determina a que distancia puede la nave detectar objetos o flotas.',
    'Autonomy':'La autonomia determina cuanto tiempo puede estar la nave operativa sin repostar.',
    'CargoCapacity':'La capacidad de cargo indica que cantidad de materia puede transportar la nave.',
    
    // ship skills
    'Glory':				'Las unidades de facción tienen moral adicional',
    'SpaceLab':				'Puede realizar misión científica',
    'Colonizer':			'Puede realizar misión de colonización',
    'Carrier':				'Bonus de autonomía de la flota',
    'Projectile':			'Ataque suicida',
    'Kamikaze':				'Bonus de evasión y daño',
    'Missile':				'Velocidad extra',
    'Bomb':					'Daño adicional',
    'DirtyBomb':			'Bonus contra humanos enemigos',
    'Emp':					'Emp',
    'Espionage':			'Puede realizar misiones de espionaje',
    'PlanetBuster':			'Puede destruir planetas',
    'ArmorPiercer':			'Bonus contra los grandes barcos',
    'FlyingBunker':			'Defensa de la flota de bonificación',
    'NeuralCenter':			'Inteligencia de la flota de bonificación',
    'BigEar':				'Percepción de la flota adicional',
    'IntelligentFire':		'Bonificación contra combustible y ordenadores enemigos',
    'RapidFire':			'Bonus contra pequeños barcos',
    'FlakAA':				'Bonus contra proyectiles',
    'Hackathon':			'Ataque electrónico de flotas extra',
    'AntiRadiationGun':		'Bonus contra radios enemigas',
    'Artillery':			'Bonus contra armadura enemiga',
    'PrecisionBombardment':	'Precisión adicional de la flota',
    'Hacker':				'Bonificación electrónica contra buques no tripulados',
    'Bombardment':			'Bonus contra objetivos superficiales',
        
}