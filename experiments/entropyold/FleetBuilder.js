/*

	Fleet builder

*/


function GetShipTypeForFleet(ship,index){
	return {
		Name : ship.name,
		FPW : ship.params.FirePower			,
		FSP : ship.params.FireSpeed			,
		PRC : ship.params.Precision			,				   			   
		ELA : ship.params.ElectricAttack 	,
		ARM : ship.params.Armor				,
		ELD : ship.params.ElectricDefence	,   
		EVS : ship.params.Evasion			,
		CMF : ship.params.Camouflage	  	,
		SPD : ship.params.Speed				,
		PCP : ship.params.Perception	  	,
		AUT : ship.params.Autonomy			,
		CMF : ship.params.CargoCapacity		,
		Active : 0,
		Reserve : 0,
		OtherFleet : 0,
		Construction : 0,
	}
}

function GetFleet(){
	
	var ships = [];
	
	for (i = 0; i < GLOBAL.ShipTypes.length; i++) { 
		ships.push(GetShipTypeForFleet(GLOBAL.ShipTypes[i],i))
	}
	
	return {
		ships : ships  
	}
	
}

function SaveFleet(){
	
}