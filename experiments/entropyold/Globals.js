/*

	Globals

*/

var ICONS = {
	Resources : {
		Energy : '⚡',
		Matter : '⚛',
		Waste : '🔩',
		Humans : '👫',
	}, 
}

var GLOBAL = {
	CurrentMenu : 0,
	ShipTypes : [],
	Recursos : {
		POBLACION : 0,
		MATERIA : 0,
		ENERGIA : 0,
		RESTOS : 0
	},
	Edificios : {
		RECICLADOR : 0,
		CASA : 0,
		CENTRAL : 0,
		MINA : 0,
		BUNKER : 0,
		FABRICA : 0,
		LABORATORIO : 0,
		ANTENA : 0
	}
}

var CONST = {
	Factions : [
	{
		Name : 'El Gran Imperio',
		Government : 'Fascismo',
		Culture : 'Japon imperial y Alemania nazi',
		Image : 'nazi',
		Intro : '',
		Politics : '',
		Cities : ['Sig','Hag','Tyr','Rit','Minamoto','Taira','Fujiwara','Tachibana'],
		Names : MakeNames(10,0),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
		
	},
	{
		Name : 'Union Sovietica Universal',
		Government : 'Comunismo',
		Culture : 'URSS y China comunista',
		Image : 'soviet',
		Intro : '',
		Politics : '',
		Cities : ['Moskba','Beijing','Stalingrad','Leningrad'],
		Names : MakeNames(10,1),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:'La conquista del espacio de los comunistas se inicia en medio de la guerra, con la ayuda de cientificos alemanes capturados. Tras colocar los primeros hombres en orbita y luego de la pacificacion de la Tierra, los sovieticos establecen una base permanente en la cara oculta de la Luna, y colonias orbitales en Venus. Dichas colonias pronto colonizan el cielo planetario, estableciendo enormes ciudades flotantes que acaban llenando el infernal paisaje de la atmosfera del planeta.'
		}
	},
	{
		Name : 'Confederacion de Jonia',
		Government : 'Democracia federal',
		Culture : 'Jonia clasica y Macedonia',
		Image : 'ionia',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,2),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:'La civilizacion Jonica consigue grandes proezas en el espacio poco despues de conquistar America. Tras varias misiones no tripuladas a estrellas cercanas, Jonia establece varias bases en las lunas de Jupiter y Saturno, destacando sobre las demas Europa, donde pronto se erige una nueva capital, preparada para lanzar enormes flotas cientificas y colonizadoras dirigidas a otros mundos habitables mas alla de la nube de Oort.'
		}
		
	},
	{
		Name : 'El Nuevo Califato',
		Government : 'Teocracia',
		Culture : 'Persia y Arabia',
		Image : 'islam',
		Intro : '',
		Politics : '',
		Cities : ['Baghdad','Cordoba'],
		Names : MakeNames(10,3),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:'Pese a una gran tradicion astronomica y cientifica, la civilizacion islamica tarda un gran periodo de tiempo en lanzar sus primeras sondas no tripuladas mas alla de la atmosfera terrestre. Pocos detalles se conservan sobre la historia espacial del Califato, pero parece ser que sus primeras misiones fueron empujadas por una imminente crisis de superpoblacion en la Tierra, varios siglos despues de su total conquista.'
		}
		
		
	},
	{
		Name : 'Tercera Republica Romana',
		Government : 'Republica',
		Culture : 'Roma y cartago',
		Image : 'rome',
		Intro : '',
		Politics : '',
		Cities : ['Roma','Cartago'],
		Names : MakeNames(10,4),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Dinastia Sikh',
		Government : 'Democracia centralista',
		Culture : 'Reino Sikh y China imperial',
		Image : 'sikh',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,5),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Imperio de la Serpiente',
		Government : 'Dictadura',
		Culture : 'Azteca e Inca',
		Image : 'serpent',
		Intro : '',
		Politics : '',
		Cities : ['Tenochtitlan'],
		Names : MakeNames(10,6),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
/*
Tito Rommel: los aztecas resistieron
Tito Rommel: los incas se les unieron
Tito Rommel: tras algunos siglos de comercio con europa y guerras controlaron la polvora
Tito Rommel: (parecido a lo q paso con asia)
Tito Rommel: pero ganando america

*/
	},
	{
		Name : 'Imperio Austropruso-Espanol',
		Government : 'Monarquia',
		Culture : 'Imperios de Espana, Austria y Prusia',
		Image : 'spain',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,7),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Corporacion Universal',
		Government : 'Corporativismo',
		Culture : 'Japon postapocaliptico',
		Image : 'corporation',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,8),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Federacion Anarquista',
		Government : 'Anarquia',
		Culture : 'Anarquismo internacional',
		Image : 'anarchy',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,9),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'EUU',
		Government : 'Democracia federal',
		Culture : 'Estados Unidos',
		Image : 'usa',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,10),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:'Tras una victoriosa carrera espacial contra la Union Sovietica, los Estados Unidos situan el primer ser humano sobre la Luna. Poco tiempo despues, tras la derrota definitiva de los comunistas, se lanzan las primeras misiones tripuladas a Marte. Despues de un largo periodo y esfuerzo de terraformacion, Marte se convierte en un mundo habitable y es vastamente populado por colonos de la Tierra, con la mirada puesta a la las estrellas'
		}	
	},	
	{
		Name : 'Imperio Francobritanico',
		Government : 'Monarquia',
		Culture : 'Inglaterra victoriana y Francia monarquica',
		Image : 'france',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,11),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Alianza de Isandlwana',
		Government : 'Confederacion tribal',
		Culture : 'Zulu y Etiope',
		Image : 'zulu',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,12),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Khanato celestial',
		Government : 'Monarquia',
		Culture : 'Turco Mongol',
		Image : 'mongol',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,13),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:'La historia espacial del Khanato es de una arrolladora expansion, parecida a su advance en la Tierra. Tras unos siglos pasada la total dominacion planetaria, el Khan ordena la conquista de la Luna, que junto a una extensiva mineria asteroidal pronto lleva a dominar primero Marte y Venus, para luego expandirse a los mundos exteriores y rapidamente a las estrellas cercanas.'
		}
	},
	{
		Name : 'Condados del norte',
		Government : 'Democracia',
		Culture : 'Vikinga e Iroquesa ',
		Image : 'viking',
		Intro : '',
		Politics : '',
		Cities : ['Fehu','Dagaz','Uruz','Ehwaz','Onondaga','Oneida','Seneca','Cayuga'],
		Names : MakeNames(10,14),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},
	{
		Name : 'Reino Eterno de Egipto',
		Government : 'Monarquia',
		Culture : 'Egipto y Mesopotamia',
		Image : 'egypt',
		Intro : '',
		Politics : '',
		Cities : [''],
		Names : MakeNames(10,15),
		Bonus : [''],
		Story : {
			real:'',
			ucrony:'',
			worldDomination:'',
			spaceHistory:''
		}
	},

	],
	
	MainMenu : {
		menu : [
			'Edificios',
			'Flotas',
			'Naves',
			'Map'
		],
		poblacion : 'Poblacion',
		materia : 'Materia',
		restos : 'Restos',
		energia : 'Energia',
		cerrar : 'Cerrar'
	},
	
	BuildingsMenu : [
		{
			Id : 'BuildCentral',
			Image : 'central',
			Name : 'Central',
			Produce : ICONS.Resources.Energy,
			Consume : ICONS.Resources.Matter,
		},
		{
			Id : 'BuildMina',
			Image : 'mina',
			Name : 'Mina',
			Produce : ICONS.Resources.Matter + ' ' + ICONS.Resources.Waste,
			Consume : ICONS.Resources.Energy,
		},
		{
			Id : 'BuildReciclador',
			Image : 'reciclador',
			Name : 'Reciclador',
			Produce : ICONS.Resources.Matter,
			Consume : ICONS.Resources.Waste,
		},
		{
			Id : 'BuildHabitat',
			Image : 'habitat',
			Name : 'Habitat',
			Produce : ICONS.Resources.Humans + ' ' + ICONS.Resources.Waste,
			Consume : ICONS.Resources.Energy + ' ' + ICONS.Resources.Matter,
		},
		{
			Id : 'BuildBunker',
			Image : 'central',
			Name : 'Bunker',
			Produce : '+ Defensa',
			Consume : ICONS.Resources.Energy + ' ' + ICONS.Resources.Humans,
		},
		{
			Id : 'BuildFabrica',
			Image : 'central',
			Name : 'Fabrica',
			Produce : '+ Produccion',
			Consume : ICONS.Resources.Energy + ' ' + ICONS.Resources.Humans,
		},
		{
			Id : 'BuildLaboratorio',
			Image : 'central',
			Name : 'Laboratorio',
			Produce : '+ Investigacion',
			Consume : ICONS.Resources.Energy + ' ' + ICONS.Resources.Humans,
		},
		{
			Id : 'BuildAntena',
			Image : 'central',
			Name : 'Antena',
			Produce : '+ Exploracion',
			Consume : ICONS.Resources.Energy + ' ' + ICONS.Resources.Humans,
		}
	],
	           
}

