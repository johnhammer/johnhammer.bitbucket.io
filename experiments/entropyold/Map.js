/*
	Map
*/

var CellsX;
var CellsY;
var Map;

var MAP_canvas;
var MAP_ctx;

var Width;
var Height;
var GridSizeX;
var GridSizeY;

function BuildMap(CellsY,CellsZ){
	var map = [];
	for (z=0; z < CellsZ; z += 1) {
		map.push([]);
		for (y=0; y < CellsY; y += 1) {
			map[z].push({});
		}
	}
	return map;
}

function DrawGrid(CellsX,CellsY){
	var i;
	for (i=0; i < Height+1; i += GridSizeX) {
	   MAP_ctx.lineWidth = (1.0+((i%10)==0));
	   MAP_ctx.moveTo(0,i);
	   MAP_ctx.lineTo(Width,i);
	   MAP_ctx.stroke();
	}
	for (i=0; i < Width+1; i += GridSizeY) {
	   MAP_ctx.lineWidth = (1.0+((i%10)==0));
	   MAP_ctx.moveTo(i,0);
	   MAP_ctx.lineTo(i,Height);
	   MAP_ctx.stroke();
	}
	MAP_ctx.strokeStyle = "white";
	MAP_ctx.stroke();
}

function HandleMapClick(e) {
  MAP_ctx.fillStyle = "red";

  MAP_ctx.fillRect(
	Math.floor(e.offsetX / GridSizeX) * GridSizeX + 1, 
	Math.floor(e.offsetY / GridSizeY) * GridSizeY + 1, 
	GridSizeX - 2, 
	GridSizeY - 2);
}

function DrawRect(x,y){
	MAP_ctx.fillStyle = "red";

	MAP_ctx.fillRect(x * GridSizeX + 1, y * GridSizeY + 1, GridSizeX - 2, GridSizeY - 2);
}

function DrawComet(x,y){
	MAP_ctx.strokeStyle = "gray";
	MAP_ctx.fillStyle = "red";
	
	MAP_ctx.beginPath();
	MAP_ctx.arc(x * GridSizeX+GridSizeX/2,y * GridSizeY+GridSizeY/2, (GridSizeX - 2)/4,(GridSizeY - 2)/4,0,2*Math.PI);
	MAP_ctx.fill();
	MAP_ctx.stroke();
}

function DrawAsteroids(x,y){
	MAP_ctx.fillStyle = "gray";
	
	var asteroids = random(20,40)
	for(var i=0; i < asteroids; i += 1){
	
		var xx = random(x * GridSizeX + 2,x * GridSizeX + GridSizeX - 4)
		var yy = random(y * GridSizeY + 2,y * GridSizeY + GridSizeY - 4) 
	
		MAP_ctx.fillRect(xx,yy,1,1); 
	}
}

function DrawColony(x,y){
	MAP_ctx.strokeStyle = "gray";
	MAP_ctx.fillStyle = "red";
	
	MAP_ctx.beginPath();
	MAP_ctx.moveTo(GridSizeX/4 + x * GridSizeX +GridSizeX/4, 	GridSizeY/4 + y * GridSizeY +0);
	MAP_ctx.lineTo(GridSizeX/4 + x * GridSizeX +GridSizeX/2, 	GridSizeY/4 + y * GridSizeY +GridSizeY/4);
	MAP_ctx.lineTo(GridSizeX/4 + x * GridSizeX +GridSizeX/4, 	GridSizeY/4 + y * GridSizeY +GridSizeY/2);
	MAP_ctx.lineTo(GridSizeX/4 + x * GridSizeX +0, 				GridSizeY/4 + y * GridSizeY +GridSizeY/4);
	MAP_ctx.closePath();
	MAP_ctx.fill();
	MAP_ctx.stroke();
}

function DrawFleet(x,y){
	var angle = 0.5;

	MAP_ctx.strokeStyle = "gray";
	MAP_ctx.fillStyle = "red";
	
	MAP_ctx.save();
	MAP_ctx.rotate(angle);
	MAP_ctx.beginPath();
	MAP_ctx.moveTo(GridSizeX/4 + x * GridSizeX +GridSizeX/4, 	GridSizeY/4 + y * GridSizeY +0);
	MAP_ctx.lineTo(GridSizeX/4 + x * GridSizeX +GridSizeX/2, 	GridSizeY/4 + y * GridSizeY +GridSizeY/4);
	MAP_ctx.lineTo(GridSizeX/4 + x * GridSizeX +GridSizeX/4, 	GridSizeY/4 + y * GridSizeY +GridSizeY/2);
	MAP_ctx.closePath();
	
	MAP_ctx.fill();
	MAP_ctx.stroke();
	
	MAP_ctx.restore();
}

function DrawAmount(func,amount,min=1){
	var asteroids = random(min,CellsX*CellsY*amount)
	for(var i=0; i < asteroids; i += 1){
		var x = random(0,CellsX-1);
		var y = random(0,CellsY-1);
		
		if(!Map[y][x].HasObject){
			Map[y][x].HasObject = true;
			func(x,y);
		}	
	}	
}

function InitMap(){

	CellsX = 16;
	CellsY = 16;
	Map = [];

	MAP_canvas = document.getElementById("mapcanvas");
	MAP_ctx = MAP_canvas.getContext("2d");

	Width = MAP_canvas.width;
	Height = MAP_canvas.height;
	GridSizeX = Width/CellsX;
	GridSizeY = Height/CellsY;


	MAP_canvas.addEventListener('click', HandleMapClick);

	DrawGrid(CellsX,CellsY)
	Map = BuildMap(CellsX,CellsY)

	DrawAmount(DrawComet,0.03,2);
	DrawAmount(DrawAsteroids,0.1);
}
