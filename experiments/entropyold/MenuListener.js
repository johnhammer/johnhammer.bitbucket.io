/*
	MenuListener
*/

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

function ListenToMenu(){
	document.getElementById('menu').onclick = function(e) {
		switch(GLOBAL.CurrentMenu){
			case 0: HandleBuildingsMenu(e); return;
			case 1: HandleFleetMenu(e); return;
			case 2: HandleShipTypeMenu(e); return;
			case 3: HandleMapMenu(e); return;
		}
	}
}



function HandleFleetMenu(e){
	
}

function HandleShipTypeMenu(e){
	
}

function HandleMapMenu(e){
	
}

function HandleBuildingsMenu(e){
	if(DetectMenuClick(e, 'BuildCentral')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildCentral')) return;
		model = buildings.central;
		ACCION_ACTUAL = 'BuildCentral';
	} 
	else if(DetectMenuClick(e, 'BuildMina')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildMina')) return;
		model = buildings.mina;
		ACCION_ACTUAL = 'BuildMina';
	} 
	else if(DetectMenuClick(e, 'BuildReciclador')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildReciclador')) return;
		model = buildings.reciclador;
		ACCION_ACTUAL = 'BuildReciclador';
	} 
	else if(DetectMenuClick(e, 'BuildHabitat')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildHabitat')) return;
		model = buildings.casa;
		ACCION_ACTUAL = 'BuildHabitat';
	}
	else if(DetectMenuClick(e, 'BuildBunker')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildBunker')) return;
		model = buildings.bunker;
		ACCION_ACTUAL = 'BuildBunker';
	} 
	else if(DetectMenuClick(e, 'BuildFabrica')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildFabrica')) return;
		model = buildings.fabrica;
		ACCION_ACTUAL = 'BuildFabrica';
	} 	
	else if(DetectMenuClick(e, 'BuildLaboratorio')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildLaboratorio')) return;
		model = buildings.laboratorio;
		ACCION_ACTUAL = 'BuildLaboratorio';
	} 	
	else if(DetectMenuClick(e, 'BuildAntena')) {
		CloseMenu();
		if(!TryToBuildBuilding('BuildAntena')) return;
		model = buildings.antena;
		ACCION_ACTUAL = 'BuildAntena';
	} 	
	else if(DetectMenuClick(e, 'LastMenu')) {
		OpenMenu(1);
	} 	
	else if(DetectMenuClick(e, 'NextMenu')) {
		OpenMenu(3);
	}	
	else {
		CloseMenu();
	}
}

function DetectMenuClick(e,id){ 
	return e.target == document.getElementById(id) || e.target.parentElement == document.getElementById(id);
}

function OpenMenu(type){
	closeNav();
	
	GLOBAL.CurrentMenu = type;

	document.getElementById('menu').style.display = 'table';
	
	var menudata = {};
	var template = "";
	
	if(type == 1){
		template = "menu_buildings";
		menudata = {buildings : CONST.BuildingsMenu}
	}
	
	else if(type == 2){
		template = "menu_fleet";
		menudata = GetFleet()
	}
	
	else if(type == 3){
		template = "ship_builder";
		menudata = {}
	}
	
	else if(type == 4){
		template = "menu_map";
		menudata = {}
	}
		
	document.getElementById('menucontainer').innerHTML = new EJS({url: 'https://johnhammer.bitbucket.io/entropy/templates/'+template+'.ejs'}).render(menudata);
	
	if(type == 3){
		StartShipBuilder();
	}
	
	if(type == 4){
		InitMap();
	}
}

function CloseMenu(){
	GLOBAL.CurrentMenu = 0;
	
	document.getElementById('menu').style.display = 'none';
	document.getElementById('menucontainer').innerHTML = '';	
}
