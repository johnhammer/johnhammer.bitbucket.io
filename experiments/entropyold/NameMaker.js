/*

	Name maker

*/

var Flavours = {
	German: {
		Name: {
			Min:1,
			Max:2,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:['walt','wern','her','guent','wolf'],
			Terminations:['er','mann','fred','gang'],
			Full:['Franz','Erwin','Otto','Hans','Ludwig','Horst','Adolf','Ludwig'],
		},
		Linker: ['von','van'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['fisch','muell','schroed','zimmer','hof','hart','wern','leh','kohl','walt','berg'],
			Terminations:['stein','stadt','ing','er','mann'],
			Full:['Rommel','Koch','Schmidt','Braun'],
		}
	},
	Japanese: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:2,
			MaxSyllables:5,
			Syllables:['dai','ki','rou','go','wa','hachi','ha','to','hi','bi','shou','shi','taka'],
			Terminations:[],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:4,
			Syllables:['fu','hon','hi','ji','ha','shi','ma','aki','ku','wa','ma'],
			Terminations:['maru','moto','oka','saki','da','shi','kawa','ura'],
			Full:['Tojo'],
		}
	},
	Indian:{
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['akh','braham','pra','ash','bal','win','der','ram','bi','mal','bir'],
			Terminations:['preet','bir','prem','der','ram','pal','kaal','kash'],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:[],
			Terminations:[],
			Full:['Singh'],
		}
	},
	Latin:{
		Name: {
			Min:1,
			Max:3,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['aure','augu','stus','cato','clau','tibe','iuli','magni','regi','anto','marci'],
			Terminations:['nius','dius','stus','lius','ius','us'],
			Full:['Hannibal','Hanno','Hamilcar','Hasdrubal'],
		},
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['aure','augu','sti','cato','clau','tibe','iuli','magni','regi','anto','marci'],
			Terminations:['nius','dius','stus','lius','ius','us'],
			Full:[],
		}
	},
	Spanish:{
		Name: {
			Min:1,
			Max:2,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['Juan','Jose','Miguel','Pedro','Carlos','Antonio','Gabriel','Diego','Andres','Francisco'],
		},
		Linker: ['de'],
		Surname: {
			Min:2,
			Max:2,
			MinSyllables:1,
			MaxSyllables:3,
			Syllables:['mar','tin','gon','zal','per','ra','mir','ji','men','al','var'],
			Terminations:['ez','tara','ar','mano','era'],
			Full:[],
		}
	},
	Slavic: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['bog','slo','bo','vlad','dmi','ul','yev','aleks','serg'],
			Terminations:['dan','imir','tri','yan','ris','geni','andr','ey'],
			Full:['Iosif','Lev','Grigori','Iuri'],
		},
		Patronym: ['ovich','evich'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:3,
			Syllables:['len','stal','bog','dan','ryk','bog','slo','bo','ga','gar','put','yev','aleks'],
			Terminations:['in','ov','ev','nsky'],
			Full:[],
		}
	},
	Chinese: {
		Name: {
			Min:1,
			Max:2,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['li','xiu','ying','wei','fang','min','jing','qiang','lei','yang','yong','jun','chao'],
			Terminations:[],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['li','xiu','ying','wei','fang','min','jing','qiang','lei','yang','yong','jun','chao'],
			Terminations:[],
			Full:[],
		}
	},
	Greek: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['phil','agr','ecyd','le','pe','andr','mnes','her','rod','klid','thal'],
			Terminations:['es','os','as','us','ter'],
			Full:['Alexandros'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:3,
			Syllables:['phil','agr','ecyd','le','pe','andr','mnes','her','rod','klid'],
			Terminations:['akis','as','ides','opoulos','ou','akos','eas','otis'],
			Full:[],
		}
	},
	Islamic: {
		Name: {
			Min:1,
			Max:3,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['abd','amin'],
			Terminations:['ullah','allah','ur Rahman'],
			Full:['Muhammad','Ali','Hussein'],
		},
		Linker: ['ibn','bin','abu'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:['al'],
			Terminations:[],
			Full:['Bakr'],
		}
	},
	Aztec: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:5,
			Syllables:['ya','mat','lal','ihu','zo','no','che','hu','xoch','maz','cue','pa','zt','tz'],
			Terminations:['atl','itl','otl','lin','lal','tli','lli'],
			Full:[],
		},
		Linker: [],
		Surname: {
			Min:0,
			Max:0,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Tribal: {
		Name: {
			Min:1,
			Max:2,
			MinSyllables:0,
			MaxSyllables:0,
			Syllables:[],
			Terminations:[],
			Full:['Lobo','Perro','Aguila','Toro','Cazador','Guerrero','Pastor','Lider','Sentado','Sanguinario','Salvaje','Brutal','Recolector','Agricultor','Destructor','Mensajero','Navegante','Explorador','Chaman','Azul','Rojo','Negro','Blanco','Hombre','Pajaro','Cuervo','Leon','Carnicero','Pescador'],
		},
		Linker: ['de','en la','de la','a la'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['Guerra','Paz','Alianza','Discordia','Noche','Montana','Tierra','Mar','Tribu','Familia','Niebla','Lluvia','Pradera','Nube','Luna','Muerte','Vida','Caceria','Masacre','Calma','Senal'],
		}
	},
	English: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:1,
			Syllables:['ald','al','as','bar','brad','wins','cold','craw','crom','day','dud','fair','had','wad'],
			Terminations:['en','win','fred','ger','ton','wick','ter','ford','man','ton','well','ley','wyn'],
			Full:['John','Jack','George','Stephen','Charles','Dean','Edward','Gabriel','Ryan','Robert'],
		},
		Linker: ['J.','G.','D.','R.','E.','S.','C.','A.'],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['arch','farm','fish','thatch','turn','pott','park','bridg','field','grov','davi','jack','john','simp','stephen','wat','wil','hender'],
			Terminations:['er','son','s','man','field','wright','e'],
			Full:['Clark','Churchill','Cook','Mason','Wright','Taylor','Sawyer','Smith','King','Bush','Jones','Armstrong'],
		}
	},
	French: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	African: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Mongolian: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Viking: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:['ragn','sig','as','bal','tor','ein','eiv','half','ha','ral','hjal','ei','val','thor'],
			Terminations:['urd','fried','geir','mundr','dr','sten','ar','rik','indr','dan','vard'],
			Full:['Olav','Bjorn','Knut','Dagr','Gustav','Yngvar','Jarl','Leif'],
		},
		Patronym: ['son','sson'],
		Linker: [],
		Surname: {
			Min:0,
			Max:0,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	Egyptian: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	MiddleEast: {
		Name: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:['John'],
		},
		Linker: [],
		Surname: {
			Min:1,
			Max:1,
			MinSyllables:1,
			MaxSyllables:2,
			Syllables:[],
			Terminations:[],
			Full:[],
		}
	},
	
}

var Names = [
	[Flavours.German,Flavours.Japanese],									//El Gran Imperio
	[Flavours.Slavic,Flavours.Chinese],										//Union Sovietica Universal			
	[Flavours.Greek],														//Confederacion de Jonia			
	[Flavours.Islamic],														//El Nuevo Califato			
	[Flavours.Latin],														//Tercera Republica Romana
	[Flavours.Indian,Flavours.Chinese],										//Dinastia Sikh			
	[Flavours.Aztec,Flavours.Tribal],										//Imperio de la Serpiente			
	[Flavours.German,Flavours.Spanish],										//Imperio Austro-Espanol
	[Flavours.Japanese,Flavours.English],									//Corporacion Universal			
	[Flavours.Slavic,Flavours.Spanish,Flavours.English,Flavours.German],	//Federacion Anarquista			
	[Flavours.English,Flavours.Spanish],									//EUU			
	[Flavours.English,Flavours.French],										//Imperio Francobritanico			
	[Flavours.African,Flavours.MiddleEast],									//Alianza de Isandlwana			
	[Flavours.Mongolian,Flavours.MiddleEast],								//Khanato celestial			
	[Flavours.Viking,Flavours.Tribal],										//Condados del norte			
	[Flavours.Egyptian,Flavours.MiddleEast]									//Reino Eterno de Egipto	
]

function GetName(object){
	var name = [];
	var numberOfNames = random(object.Min,object.Max);	
	for (i = 0; i < numberOfNames; i++) { 
	
		if(object.Syllables.length==0 || object.Full.length>0 && randomBool(0.8)){
			name.push(getRandom(object.Full));
		}
		else{
			var word = '';
			var nameLength = random(object.MinSyllables,object.MaxSyllables)
			for (j = 0; j < nameLength; j++) { 
				word += getRandom(object.Syllables);
			}
			if(object.Terminations.length>0){
				word += getRandom(object.Terminations);
			}
			name.push(Capitalize(word));
		}
	}
	return name;
}

function GetExtra(object,prob){
	if(object && object.length>0 && randomBool(prob)){
		return [getRandom(object)];
	}
	return []
}

function GetPatronym(flavor){
	if(flavor.Patronym){
		return [GetName(flavor.Name)[0]+GetExtra(flavor.Patronym,0)[0]];
	}
	return []	
}

function MakeName(factionId){
	var faction = Names[factionId];
	var flavor = faction[random(1,faction.length)-1];
	
	var name = GetName(flavor.Name)
	var patronym = GetPatronym(flavor)
	var linker = GetExtra(flavor.Linker,0.2)
	var surname = GetName(flavor.Surname)
		
	return name.concat(patronym.concat(linker.concat(surname))).join(" ");
}

function MakeNames(numberOfNames,factionId){
	var names = [];
	for (var i = 0; i < numberOfNames; i++) { 
		names.push(MakeName(factionId));
	}
	return names;
}
