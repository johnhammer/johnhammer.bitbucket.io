/*
	ResourcesCalculator
*/

function SetStartResources(){
	GLOBAL.Recursos.POBLACION = 0;
	GLOBAL.Recursos.MATERIA = 1024;
	GLOBAL.Recursos.ENERGIA = 0;
	GLOBAL.Recursos.RESTOS = 0;
}


function TryToBuildShip(ship){
	var materia = 0;
	var humanos = 0;
	
	switch(edificio){
		case 'BuildCentral':		materia = 0;	humanos = 0;	break;
		case 'BuildMina':			materia = 0;	humanos = 0;	break;
		case 'BuildReciclador':		materia = 0;	humanos = 0;	break;
		case 'BuildHabitat':		materia = 0;	humanos = 0;	break;
		case 'BuildBunker':			materia = 0;	humanos = 0;	break;
		case 'BuildFabrica':		materia = 0;	humanos = 0;	break;
		case 'BuildLaboratorio':	materia = 0;	humanos = 0;	break;
		case 'BuildAntena':			materia = 0;	humanos = 0;	break;
	}
	
	if(GLOBAL.Recursos.MATERIA >= materia && GLOBAL.Recursos.POBLACION >= humanos ){
		GLOBAL.Recursos.MATERIA		-=	materia;
		GLOBAL.Recursos.POBLACION	-=	humanos;
		return true;
	}
	else{
		return false;
	}
}


function TryToBuildBuilding(edificio){
	var materia = 0;
	var humanos = 0;
	
	switch(edificio){
		case 'BuildCentral':		materia = 0;	humanos = 0;	break;
		case 'BuildMina':			materia = 0;	humanos = 0;	break;
		case 'BuildReciclador':		materia = 0;	humanos = 0;	break;
		case 'BuildHabitat':		materia = 0;	humanos = 0;	break;
		case 'BuildBunker':			materia = 0;	humanos = 0;	break;
		case 'BuildFabrica':		materia = 0;	humanos = 0;	break;
		case 'BuildLaboratorio':	materia = 0;	humanos = 0;	break;
		case 'BuildAntena':			materia = 0;	humanos = 0;	break;
	}
	
	if(GLOBAL.Recursos.MATERIA >= materia && GLOBAL.Recursos.POBLACION >= humanos ){
		GLOBAL.Recursos.MATERIA		-=	materia;
		GLOBAL.Recursos.POBLACION	-=	humanos;
		return true;
	}
	else{
		return false;
	}
}

function CalculateResources(){
	var Materia 
		=	GLOBAL.Edificios.MINA			*	(+2) 
		+ 	GLOBAL.Edificios.CENTRAL		*	(-4)
		+ 	GLOBAL.Edificios.CASA			*	(-1) 
		+ 	GLOBAL.Edificios.RECICLADOR		*	(+2)

	var Restos 
		=	GLOBAL.Edificios.CASA			*	(+1) 
		+ 	GLOBAL.Edificios.RECICLADOR		*	(-2)
		
	var Energia
		=	GLOBAL.Edificios.MINA			*	(-1) 
		+ 	GLOBAL.Edificios.CENTRAL		*	(+4)
		+ 	GLOBAL.Edificios.CASA			*	(-1) 
		+ 	GLOBAL.Edificios.RECICLADOR		*	(-1)
		+ 	GLOBAL.Edificios.FABRICA		*	(-1) 
		+ 	GLOBAL.Edificios.BUNKER			*	(-1)
		+ 	GLOBAL.Edificios.LABORATORIO	*	(-1) 
		+ 	GLOBAL.Edificios.ANTENA			*	(-1)
		
	var Poblacion 
		= 	GLOBAL.Edificios.CASA 			* 	(64)
		+ 	GLOBAL.Edificios.FABRICA		*	(-16) 
		+ 	GLOBAL.Edificios.BUNKER			*	(-16)
		+ 	GLOBAL.Edificios.LABORATORIO	*	(-16) 
		+ 	GLOBAL.Edificios.ANTENA			*	(-16)
	
	GLOBAL.Recursos.POBLACION = Poblacion;
	GLOBAL.Recursos.ENERGIA   = Energia;
	
	GLOBAL.Recursos.MATERIA += Materia * 0.01;
	GLOBAL.Recursos.RESTOS  += Restos  * 0.01;
}
