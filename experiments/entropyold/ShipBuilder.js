SHIPICONCONST = {
	weapon : '⚔',
	ammo : 'ⲽ',
	shield : '⛨',
	engine : '⌁',
	fuel : '⛽',
	person : '👨',
	radio : '∿',
	emp : '🔌',
	nothing : '' 
}

SHIP = {
	l:[null,null,null,null,null,null,null,null],
	s:8
}

function saveShip(){
	var ship = {
		name: getShipName(),
		params:GetShipParams()
	}
	
	GLOBAL.ShipTypes.push(ship)
	OpenMenu(2);
}

window.allowDrop = function(ev) {
    ev.preventDefault();
    if (ev.target.getAttribute("draggable") == "true")
        ev.dataTransfer.dropEffect = "none"; 
    else
        ev.dataTransfer.dropEffect = "all"; 
};

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev,a) {
    ev.dataTransfer.setData("text", a);
}

function drop(ev,a) { 
    ev.preventDefault();
	ev.stopPropagation();

    var data = ev.dataTransfer.getData("text");

	SHIP.l[a-1] = data;
	if(data == '0') SHIP.l[a-1] = null;
	
	switch (data){
		case '1': 
			ev.target.innerHTML =SHIPICONCONST.weapon;
			break;
		case '2': 
			ev.target.innerHTML =SHIPICONCONST.ammo;
			break;
		case '3': 
			ev.target.innerHTML =SHIPICONCONST.shield;
			break;
		case '4': 
			ev.target.innerHTML =SHIPICONCONST.engine;
			break;
		case '5': 
			ev.target.innerHTML =SHIPICONCONST.fuel;
			break;
		case '6': 
			ev.target.innerHTML =SHIPICONCONST.person;
			break;
		case '7': 
			ev.target.innerHTML =SHIPICONCONST.radio;
			break;
		case '8': 
			ev.target.innerHTML =SHIPICONCONST.emp;
			break;
		default:
			ev.target.innerHTML = SHIPICONCONST.nothing;
			break;
	}
	SetShipHtml();
}

function setSize(size){
	var hide = [];
	var display = [];
	SHIP.s = parseInt(size);
	switch (size){
		case 1: 
			hide = hide.concat(Array.prototype.slice.call(document.getElementsByName("k2")));
			hide = hide.concat(Array.prototype.slice.call(document.getElementsByName("k4")));
			hide = hide.concat(Array.prototype.slice.call(document.getElementsByName("k8")));	
			break;
		case 2: 
			display = display.concat(Array.prototype.slice.call(document.getElementsByName("k2")));
			hide = hide.concat(Array.prototype.slice.call(document.getElementsByName("k4")));
			hide = hide.concat(Array.prototype.slice.call(document.getElementsByName("k8")));	
			break;
		case 4: 
			display = display.concat(Array.prototype.slice.call(document.getElementsByName("k2")));
			display = display.concat(Array.prototype.slice.call(document.getElementsByName("k4")));
			hide = hide.concat(Array.prototype.slice.call(document.getElementsByName("k8")));	
			break;
		case 8: 
			display = display.concat(Array.prototype.slice.call(document.getElementsByName("k2")));
			display = display.concat(Array.prototype.slice.call(document.getElementsByName("k4")));
			display = display.concat(Array.prototype.slice.call(document.getElementsByName("k8")));
			break;
	}
	for (i = 0; i < display.length; i++) { 
		display[i].style.display = "block";
	}
	for (i = 0; i < hide.length; i++) { 
		hide[i].style.display = "none";
		hide[i].innerHTML = SHIPICONCONST.nothing;
		SHIP.l[7-i] = null;
	}
	SetShipHtml();
}

var NAVE = {
	arma : '1',
	municion : '2',
	escudo : '3',
	motor : '4',
	fuel : '5',
	habitat : '6',
	radio : '7',
	emp : '8'
}

function getCell(i){
	return document.getElementById(''+i);
}

function getRandomShip(){
	SHIP.s = [1,2,4/*,8*/][Math.floor(Math.random() * 3)];
	
	for (i = 0; i < SHIP.s; i++) { 
		SHIP.l[i] = ['1','2','3','4','5','6','7','8',''][Math.floor(Math.random() * 9)];
		switch (SHIP.l[i]){
			case '1': 
				getCell(i).innerHTML =SHIPICONCONST.weapon;
				break;
			case '2': 
				getCell(i).innerHTML =SHIPICONCONST.ammo;
				break;
			case '3': 
				getCell(i).innerHTML =SHIPICONCONST.shield;
				break;
			case '4': 
				getCell(i).innerHTML =SHIPICONCONST.engine;
				break;
			case '5': 
				getCell(i).innerHTML =SHIPICONCONST.fuel;
				break;
			case '6': 
				getCell(i).innerHTML =SHIPICONCONST.person;
				break;
			case '7': 
				getCell(i).innerHTML =SHIPICONCONST.radio;
				break;
			case '8': 
				getCell(i).innerHTML =SHIPICONCONST.emp;
				break;
			default:
				getCell(i).innerHTML = SHIPICONCONST.nothing;
				break;
		}
	}
	setSize(SHIP.s);
}

function Normalize(number1,number2){
	return Math.floor((number1 / number2) * 100)
}

function GetShipParams(){
	var prop = CountElements(SHIP.l,SHIP.s); 
	var piezas = prop.size - prop.nada;
	var params = {}
	
	params.FirePower		= Normalize(prop.municion*2 + ((prop.arma>0) ? 1 : 0)											   ,16);
	params.FireSpeed		= Normalize(prop.arma                                                                              ,8);  
	params.Precision		= Normalize(((prop.arma>0) ? (32+(prop.radio+1) * (prop.habitat+1) - prop.municion*4) : 0)         ,96); 
	params.ElectricAttack	= Normalize((prop.habitat+1)*prop.emp*2 + prop.radio*prop.emp                                      ,40);
	params.Armor			= Normalize(prop.escudo*3 + prop.size                                                              ,32); 
	params.ElectricDefence	= Normalize(prop.habitat*4 + prop.radio*2 + prop.emp*1                                             ,32);
	params.Evasion			= Normalize(64 + (prop.motor*3+1) * (prop.radio+1) * (prop.habitat*2+1) - (prop.size)*8            ,210);
	params.Camouflage		= Normalize(16-prop.motor-prop.size+1                                                              ,16);
	params.Speed			= Normalize(16+prop.motor*3-prop.size-piezas+1                                                     ,25); 
	params.Perception		= Normalize(prop.radio+1                                                                           ,9);
	params.Autonomy			= Normalize(8+prop.fuel-prop.motor+1                                                               ,17); 
	params.CargoCapacity	= Normalize(prop.nada                                                                              ,8);  
	
	return params;
}

var SHIPACTIONS = {
	Move : 'move',
	Attack : 'attack',
	Kamikaze : 'kamikaze',
	Scan : 'scan',
	Cargo : 'cargo',
	Colonize : 'colonize',
	Bombard : 'bombard',
	Emp : 'emp',
	
}

function GetShipActions(){
	var actions = [SHIPACTIONS.Move];
	var prop = CountElements(SHIP.l,SHIP.s); 
	
	if(prop.arma>0) actions.push(SHIPACTIONS.Attack);
	if(prop.municion>0 && prop.municion>=prop.size/2) actions.push(SHIPACTIONS.Kamikaze);
	if(prop.radio>0) actions.push(SHIPACTIONS.Scan);
	if(prop.nada>0) actions.push(SHIPACTIONS.Cargo);
	if(prop.habitat>3 && prop.fuel>1) actions.push(SHIPACTIONS.Colonize);
	if(prop.arma>0 && prop.municion>prop.arma) actions.push(SHIPACTIONS.Bombard);
	if(prop.emp == 1 && prop.size == 1) actions.push(SHIPACTIONS.Emp);
	
	
	return actions;
}

function SetShipHtml(){
	
	var params = GetShipParams();

	document.getElementById('name').innerHTML = getShipName();
	
	document.getElementById('prop1').innerHTML  = params.FirePower		  	;
	document.getElementById('prop2').innerHTML  = params.FireSpeed		  	;
	document.getElementById('prop3').innerHTML  = params.Precision		  	;
	document.getElementById('prop4').innerHTML  = params.ElectricAttack  	;
	document.getElementById('prop5').innerHTML  = params.Armor			  	;
	document.getElementById('prop6').innerHTML  = params.ElectricDefence 	;
	document.getElementById('prop7').innerHTML  = params.Evasion		  	;
	document.getElementById('prop8').innerHTML  = params.Camouflage	  		;
	document.getElementById('prop9').innerHTML  = params.Speed			  	;
	document.getElementById('prop10').innerHTML = params.Perception	  		;
	document.getElementById('prop11').innerHTML = params.Autonomy		  	; 
	document.getElementById('prop12').innerHTML = params.CargoCapacity	  	;
}

function CountElements(arr,sz){
	var count = {
		arma: 0,
		municion: 0,
		escudo: 0,
		motor: 0,
		fuel: 0,
		habitat: 0,
		radio: 0,
		emp: 0,
		nada: 0,
		size: sz
	};

	for ( var i = 0; i < sz; i++ ) {
		switch(arr[i]){
			case NAVE.arma: count.arma += 1; break;
			case NAVE.municion: count.municion += 1; break;
			case NAVE.escudo: count.escudo += 1; break;
			case NAVE.motor: count.motor += 1; break;
			case NAVE.fuel: count.fuel += 1; break;
			case NAVE.habitat: count.habitat += 1; break;
			case NAVE.radio: count.radio += 1; break;
			case NAVE.emp: count.emp += 1; break;
			default: count.nada += 1; break;
		}
    }
	
	return count;
}

function getShipName(){
	switch(SHIP.s){
		case 1: 
			return getShipName1(CountElements(SHIP.l,SHIP.s));
		case 2:
			return getShipName2(CountElements(SHIP.l,SHIP.s));
		case 4: 
			return getShipName4(CountElements(SHIP.l,SHIP.s));
		case 8:
			return getShipName8(CountElements(SHIP.l,SHIP.s));
	}
}

function getShipName1(count){
	if(count.arma==1) return 'Dron cazador';
	if(count.municion==1) return 'Bomba';
	if(count.escudo==1) return 'Dron senuelo';
	if(count.motor==1) return 'Misil';
	if(count.fuel==1) return 'Dron nodriza';
	if(count.habitat==1) return 'Explorador ligero';
	if(count.radio==1) return 'Sonda';
	if(count.emp==1) return 'Pulso EMP';
	return 'Dron de carga';
}

function getShipName2(count){
	if(count.arma==1 && count.municion==1) return 'Cazabombardero';
	if(count.habitat==1 && count.municion==1) return 'Kamikaze';
	if(count.habitat==1 && count.emp==1) return 'Hacker';
	if(count.habitat==2) return 'Insignia ligera';
	
	var str = [];
	
	if(count.arma>=1) str.push('Caza');
	else if(count.municion>=1) str.push('Bomba');
	else if(count.emp>=1) str.push('Interceptor');
	else if(count.nada>=1) str.push('Nave de carga pequena');
	else if(count.habitat>=1) str.push('Explorador');
	else if(count.fuel>=1) str.push('Nodriza pequena');
	else if(count.radio>=1) str.push('Telescopio');
	else if(count.motor>=1) str.push('Misil');
	else if(count.escudo>=1) str.push('Senuelo');
	
	return str.join(" ");
}

function getShipName4(count){

	if(count.habitat==1 && count.municion==3) return 'Kamikaze pesado';

	var str = [];
	
	if(count.habitat>=3) str.push('Insignia');
	else if(count.arma>=1 && count.municion>=1 && count.escudo>=1) str.push('Crucero');
	else if(count.arma>=3) str.push('Fragata');
	else if(count.arma>=2 && count.municion>=1) str.push('Canonera');
	else if(count.arma>=1 && count.municion>=2) str.push('Destructor');
	else if(count.municion>=3) str.push('Bomba de antimateria');
	else if(count.arma>=1 && count.escudo>=2) str.push('Acorazado');
	else if(count.motor>=2 && count.arma>=1) str.push('Caza pesado');
	else if(count.motor>=2 && count.municion>=1) str.push('Misil pesado');
	else if(count.emp>=2 && count.arma>=1) str.push('Corbeta mixta');
	else if(count.emp>=3) str.push('Nave de guerra electronica');
	else if(count.emp>=2 && count.radio>=1) str.push('Nave de guerra electronica');
	else if(count.emp>=1 && count.radio>=2) str.push('Nave de guerra electronica');
	else if(count.escudo>=3) str.push('Nave escudo');
	else if(count.nada>=3) str.push('Nave de carga');
	else if(count.fuel>=3) str.push('Nodriza');
	else if(count.radio>=3) str.push('Estacion espacial');
	else if(count.radio>=2 && count.habitat>=1) str.push('Estacion espacial');
	else if(count.arma>=1 && count.municion>=1) str.push('Nave de guerra');
	else if(count.emp>=2) str.push('Corbeta de senales');
	else if(count.arma>=2) str.push('Corbeta');
	else if(count.radio>=2) str.push('Nave radio');
	else if(count.nada>=2) str.push('Nave de carga');
	else if(count.habitat>=2) str.push('Transporte');
	else if(count.motor>=3) str.push('Misil');
	
	else str.push('Hibrida');
	
	return str.join(" ");
}

function getShipName8(count){
	var str = [];
	
	if(count.habitat>=3) str.push('Insignia');
	else if(count.arma>=1 && count.municion>=1 && count.escudo>=1) str.push('Crucero');
	else if(count.arma>=3) str.push('Fragata');
	else if(count.arma>=2 && count.municion>=1) str.push('Canonera');
	else if(count.arma>=1 && count.municion>=2) str.push('Destructor');
	else if(count.municion>=3) str.push('Bomba de antimateria');
	else if(count.arma>=1 && count.escudo>=2) str.push('Acorazado');
	else if(count.motor>=2 && count.arma>=1) str.push('Caza pesado');
	else if(count.motor>=2 && count.municion>=1) str.push('Misil pesado');
	else if(count.emp>=2 && count.arma>=1) str.push('Corbeta mixta');
	else if(count.emp>=3) str.push('Nave de guerra electronica');
	else if(count.emp>=2 && count.radio>=1) str.push('Nave de guerra electronica');
	else if(count.emp>=1 && count.radio>=2) str.push('Nave de guerra electronica');
	else if(count.escudo>=3) str.push('Nave escudo');
	else if(count.nada>=3) str.push('Nave de carga');
	else if(count.fuel>=3) str.push('Nodriza');
	else if(count.radio>=3) str.push('Estacion espacial');
	else if(count.radio>=2 && count.habitat>=1) str.push('Estacion espacial');
	else if(count.arma>=1 && count.municion>=1) str.push('Nave de guerra');
	else if(count.emp>=2) str.push('Corbeta de senales');
	else if(count.arma>=2) str.push('Corbeta');
	else if(count.radio>=2) str.push('Nave radio');
	else if(count.nada>=2) str.push('Nave de carga');
	else if(count.habitat>=2) str.push('Transporte');
	
	else str.push('Hibrida');
	
	return str.join(" ");
}

function StartShipBuilder(){
	
	document.getElementById('b1').innerHTML = SHIPICONCONST.weapon;
	document.getElementById('b2').innerHTML = SHIPICONCONST.ammo;
	document.getElementById('b3').innerHTML = SHIPICONCONST.shield;
	document.getElementById('b4').innerHTML = SHIPICONCONST.engine;
	document.getElementById('b5').innerHTML = SHIPICONCONST.fuel;
	document.getElementById('b6').innerHTML = SHIPICONCONST.person;
	document.getElementById('b7').innerHTML = SHIPICONCONST.radio;
	document.getElementById('b8').innerHTML = SHIPICONCONST.emp;
	
	SetShipHtml();
}
