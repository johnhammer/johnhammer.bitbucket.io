/*

	World

*/

function InitWorld(){
	
	//Scene
	scene = new THREE.Scene();

	// Camera
	var screenWidth = window.innerWidth;
	var screenHeight = window.innerHeight;
	var viewAngle = 75;
	var nearDistance = 0.1;
	var farDistance = 1000;
	camera = new THREE.PerspectiveCamera(viewAngle, screenWidth / screenHeight, nearDistance, farDistance);
	scene.add(camera);
	camera.position.set(0, 0, 5);
	camera.lookAt(scene.position);

	// Renderer
	renderer = new THREE.WebGLRenderer({antialias: true,alpha: true});
	renderer.setClearColor(0x00000f, 1);
	renderer.setSize(screenWidth, screenHeight);
	container = document.getElementById('container');
	container.appendChild(renderer.domElement);

	// Light
	var light = new THREE.PointLight(0xffffff);
	light.position.set(20, 0, 20);
	scene.add(light);
	var lightAmb = new THREE.AmbientLight(0x777777);
	scene.add(lightAmb);

	// Controls
	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.enableZoom = true;
	
	CreateMainTerrain();
	SetStartResources();
}

// general globals
var container, scene, camera, renderer, controls;

// mouse ray tracing
var targetList = [], INTERSECTED;

// constants
var buildingNameList = [
	"reciclador",
	"casa",
	"central",
	"mina",
	"bunker",
	"fabrica"
];

// game globals
var rockFaces = [];
var model = null;
var buildings = {};

var ACCION_ACTUAL;

var OLDVALUES = {
	POBLACION : 0,
	MATERIA : 0,
	ENERGIA : 0,
	RESTOS : 0
};

function AnadirEdificio(){
	if(ACCION_ACTUAL == 'BuildCentral'){
		GLOBAL.Edificios.CENTRAL += 1;
	}
	else if(ACCION_ACTUAL == 'BuildMina'){		
		GLOBAL.Edificios.MINA += 1;
	}
	else if(ACCION_ACTUAL == 'BuildReciclador'){
		GLOBAL.Edificios.RECICLADOR += 1;
	}
	else if(ACCION_ACTUAL == 'BuildHabitat'){
		GLOBAL.Edificios.CASA += 1;
	}	
	else if(ACCION_ACTUAL == 'BuildBunker'){
		GLOBAL.Edificios.BUNKER += 1;
	}	
	else if(ACCION_ACTUAL == 'BuildFabrica'){
		GLOBAL.Edificios.FABRICA += 1;
	}
	else if(ACCION_ACTUAL == 'BuildLaboratorio'){
		GLOBAL.Edificios.LABORATORIO += 1;
	}	
	else if(ACCION_ACTUAL == 'BuildAntena'){
		GLOBAL.Edificios.ANTENA += 1;
	}		
	model = null;
}

function UpdateWorld(){
	SetAndDisplayResources();	
}


function SetAndDisplayResources(){
	CalculateResources();
	
	var poblacion = Math.floor(GLOBAL.Recursos.POBLACION);
	var energia = Math.floor(GLOBAL.Recursos.ENERGIA);
	var materia = Math.floor(GLOBAL.Recursos.MATERIA);
	var restos = Math.floor(GLOBAL.Recursos.RESTOS);

	if(poblacion != OLDVALUES.POBLACION){
		document.getElementById('poblacion').innerHTML = poblacion;
		OLDVALUES.POBLACION = poblacion;
	}
	if(energia != OLDVALUES.ENERGIA){
		document.getElementById('energia').innerHTML = energia;
		OLDVALUES.ENERGIA = energia;
	}
	if(materia != OLDVALUES.MATERIA){
		document.getElementById('materia').innerHTML = materia;
		OLDVALUES.MATERIA = materia;
	}
	if(restos != OLDVALUES.RESTOS){
		document.getElementById('restos').innerHTML = restos;
		OLDVALUES.RESTOS = restos;
	}
}


function CreateMainTerrain(){
	var faceColorMaterial = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('https://johnhammer.bitbucket.io/entropy/images/02.jpg') } );
		//new THREE.MeshBasicMaterial({color: 0xFF00FF, vertexColors: THREE.VertexColors});
	var size = 2;
	var faceGeometry = new THREE.DodecahedronGeometry(size, 1);

	/*
	faceGeometry.vertices.forEach(function(v){
		v.x += (0-Math.random()*(size/4));
		v.y += (0-Math.random()*(size/4));
		v.z += (0-Math.random()*(size/4));
	})\

	*/	

	for (var i = 0; i < faceGeometry.faces.length; i++) {
		face = faceGeometry.faces[i];
		face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
		face.faceid = i;
		rockFaces.push(face);
	} 

	faceGeometry.computeFaceNormals();
	
	var backgroundMesh = new THREE.Mesh(faceGeometry, faceColorMaterial);

    var geo = new THREE.EdgesGeometry( faceGeometry ); 
    var mat = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 2 } );
    var wireframe = new THREE.LineSegments( geo, mat );
	backgroundMesh.add( wireframe );
	
	scene.add(backgroundMesh);
	targetList.push(backgroundMesh);
}

function ClickOnFace(geometry,face){
	if(model == null) return;

	var object = model.clone();

	var faceColorMaterial = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('https://johnhammer.bitbucket.io/entropy/images/365_preview.jpg') } );

	object.traverse( function ( child ) {
		if ( child instanceof THREE.Mesh ) {
			child.material = faceColorMaterial;
		}
	} );
	cylinder = object ;
	cylinder.scale.set(0.2,0.2,0.2);
	
	var centerPoint=new THREE.Vector3();
	var vertices = geometry.vertices;
	centerPoint.add(vertices[ face.a ]);
	centerPoint.add(vertices[ face.b ]);
	centerPoint.add(vertices[ face.c ]);
	centerPoint.divideScalar(3);

	var faceArea = AreaOfTriangle(vertices,face);
	cylinder.scale.set(0.1*faceArea,0.1*faceArea,0.1*faceArea);

	cylinder.position.copy(centerPoint);  
	var newPos = new THREE.Vector3();
	newPos.addVectors ( centerPoint, face.normal.multiplyScalar( 1 ) ); 
	cylinder.lookAt(newPos);

	cylinder.rotateX(Math.PI / 2);

	scene.add(cylinder);

	AnadirEdificio();
}


function AreaOfTriangle(vertices,face) {
	var v1 = vertices[face.a];
	var v2 = vertices[face.b];
	var v3 = vertices[face.c];
	var triangle = new THREE.Triangle( v1, v2, v3 );
	return triangle.area();
}