/*

	WorldInterface

*/


function onMouseMove(event) {
	event.preventDefault();
	var mouse = {};
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
	var vector = new THREE.Vector3(mouse.x, mouse.y, 1);

	var ray = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
	ray.setFromCamera(mouse, camera);
	var intersects = ray.intersectObjects(targetList);

	if (intersects.length > 0) {
		if (intersects[0].object != INTERSECTED) {
			if (INTERSECTED) {
				INTERSECTED.face.color.setHex(INTERSECTED.currentHex);
			}
			INTERSECTED = intersects[0];
			var geometry = INTERSECTED.object.geometry;
			var face = INTERSECTED.face;
			INTERSECTED.currentHex = face.color.getHex();

			face.color.setHex(0xc0392b);
			geometry.colorsNeedUpdate = true;

			ClickOnFace(geometry,face);		
		}
	}
	else {
		if (INTERSECTED) {
			INTERSECTED.face.color.setHex(INTERSECTED.currentHex);
		}
		INTERSECTED = null;
	}
}