
var Cookies = {
	Set : function(cname, cvalue, exdays = 1) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	},
	Get : function(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}
}

function randomBool(prob)
{
    return Math.random() >= prob;
}

function random(min,max)
{
    return Math.floor(Math.random()*(max-min+1)+min);
}

function getRandom(array){
	return array[Math.floor(Math.random() * array.length)];
}

function Capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}