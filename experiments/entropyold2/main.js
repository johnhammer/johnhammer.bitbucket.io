var container, scene, camera, renderer, controls;
var INTERSECTED;

var targetList = [];
var mouse = { x: 0, y: 0 };


var rockFaces = [];
var meshes = {};

loadAllMeshes(function(){
	init();
	animate();
});

function loadAllMeshes(func){
	jsonLoader.load( "models/reciclador.obj", function ( reciclador ) {
		var modelo = reciclador;
		func();     
	}); 
}


function init() {
	scene = new THREE.Scene();

	var screenWidth = window.innerWidth;
	var screenHeight = window.innerHeight;
	var viewAngle = 75;
	var nearDistance = 0.1;
	var farDistance = 1000;
	camera = new THREE.PerspectiveCamera(viewAngle, screenWidth / screenHeight, nearDistance, farDistance);
	scene.add(camera);
	camera.position.set(0, 0, 5);
	camera.lookAt(scene.position);
	renderer = new THREE.WebGLRenderer({antialias: true,alpha: true});
	renderer.setClearColor(0x00000f, 1);
	renderer.setSize(screenWidth, screenHeight);
	container = document.getElementById('container');
	container.appendChild(renderer.domElement);
  
	var light = new THREE.PointLight(0xffffff);
	light.position.set(20, 0, 20);
	scene.add(light);
	var lightAmb = new THREE.AmbientLight(0x777777);
	scene.add(lightAmb);

	var faceColorMaterial = new THREE.MeshBasicMaterial({
		color: 0xFFFFFF,
		vertexColors: THREE.VertexColors
	});
	/*
		var wireframe_material = new THREE.MeshBasicMaterial( { 
			color: 0xFFFFFF, 
			wireframe: true, 
			wireframe_linewidth: 10 
		});
	*/

	var faceGeometry = new THREE.DodecahedronGeometry(2, 1);

	faceGeometry.vertices.forEach(function(v){
		v.x += (0-Math.random()*(size/4));
		v.y += (0-Math.random()*(size/4));
		v.z += (0-Math.random()*(size/4));
	})
 
	for (var i = 0; i < faceGeometry.faces.length; i++) {
		face = faceGeometry.faces[i];
		face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
		face.faceid = i;
		rockFaces.push(face);
	} 

	faceGeometry.computeFaceNormals();

	var backgroundMesh = new THREE.Mesh(faceGeometry, faceColorMaterial);
	backgroundMesh.acc = THREE.Vector3(0, 0, 0);
	backgroundMesh.vel = THREE.Vector3(0, 0, 0);


	var helper = new THREE.FaceNormalsHelper( backgroundMesh )
	//scene.add(helper);

	scene.add(backgroundMesh);



	targetList.push(backgroundMesh);


	controls = new THREE.OrbitControls( camera, renderer.domElement );
	controls.enableZoom = true;

	controls.addEventListener( 'change', render );
	window.addEventListener( 'resize', onWindowResize, true );
	container.addEventListener('mousedown', onMouseMove, true);
}

function clickAction(geometry,face){
	var faceColorMaterial = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('http://maps.x10host.com/entropy/images/365_preview.jpg') } );
	var jsonLoader = new THREE.OBJLoader();
	//jsonLoader.load( "models/reciclador.obj", function ( event ) {
		var object = modelo;//event;
		object.traverse( function ( child ) {
			if ( child instanceof THREE.Mesh ) {
				child.material = faceColorMaterial;
			}
		} );

		var cylinder = object ;
		cylinder.scale.set(0.1,0.1,0.1);
		
		var centerPoint=new THREE.Vector3();
		var vertices = geometry.vertices;
		centerPoint.add(vertices[ face.a ]);
		centerPoint.add(vertices[ face.b ]);
		centerPoint.add(vertices[ face.c ]);
		centerPoint.divideScalar(3);

		var faceArea = AreaOfTriangle(vertices[0],vertices[1],vertices[2]);
		cylinder.scale.set(0.1*faceArea,0.1*faceArea,0.1*faceArea);

		cylinder.position.copy(centerPoint);  
		var newPos = new THREE.Vector3();
		newPos.addVectors ( centerPoint, face.normal.multiplyScalar( 1 ) ); 
		cylinder.lookAt(newPos);

		cylinder.rotateX(Math.PI / 2);

		scene.add(cylinder);

	//});
}

function onMouseMove(event) {
  event.preventDefault();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  var vectorX = new THREE.Vector2(0, 0);
  var vectorY = new THREE.Vector2(0, 0);
  var vector = new THREE.Vector3(mouse.x, mouse.y, 1);
  var ray = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
  ray.setFromCamera(mouse, camera);
  var intersects = ray.intersectObjects(targetList);
  if (intersects.length > 0) {
    if (intersects[0].object != INTERSECTED) {
      if (INTERSECTED) {
        INTERSECTED.face.color.setHex(INTERSECTED.currentHex);
      }
      INTERSECTED = intersects[0];
      var geometry = INTERSECTED.object.geometry;
      var face = INTERSECTED.face;
      INTERSECTED.currentHex = face.color.getHex();
      face.color.setHex(0xc0392b);

	  clickAction(geometry,face);

      geometry.colorsNeedUpdate = true;
      var vertices = geometry.vertices;
      var v1 = vertices[face.a];
      var v2 = vertices[face.b];
      var v3 = vertices[face.c];
      var position = new THREE.Vector3();
      position.x = (v1.x + v2.x + v3.x) / 3;
      position.y = (v1.y + v2.y + v3.y) / 3;
      position.z = (v1.z + v2.z + v3.z) / 3;      
    }
  } else {
    if (INTERSECTED) {
      INTERSECTED.face.color.setHex(INTERSECTED.currentHex);
    }
    INTERSECTED = null;
  }
}

function AreaOfTriangle( a, b, c ){
	var triangle = new THREE.Triangle( v1, v2, v3 );
	return triangle.area();
}

function animate() {
  requestAnimationFrame(animate);
	controls.update(); 
  render();
}

function render() {
  renderer.autoClear = false;
  renderer.clear();
  renderer.render(scene, camera);
};

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
	render();
}
