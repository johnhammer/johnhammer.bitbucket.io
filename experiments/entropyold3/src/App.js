import React, { Component } from 'react';
import './App.css';
import Scene from './Scene.js';
import Factions from './Factions.js';
import ReactDOM from 'react-dom';
import TopBar from './TopBar.js';
import GameMenu from './GameMenu.js';

class App extends Component {
	render() {
		return (
			<div>
				<TopBar />
				
				<div className="container">
					<div className="panel panel-default">
						<GameMenu />
					</div>
				</div>
				<Scene />
			</div>
		);	
	}
}

export default App;
