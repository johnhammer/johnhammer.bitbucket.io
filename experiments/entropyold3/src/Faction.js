import React from 'react';

let Faction = function statelessFunctionComponentClass(props) {

	return (
		<div className="card col-sm-12 col-xs-12 col-md-4 col-lg-3" >
		<img className="card-img-top img-fluid" src={'./img/' + props.source.Image + '.png'}/>
			<div className="card-block">
				<h4 className="card-title">{props.source.Name}</h4>
				<p className="card-text">{props.source.Culture}</p>
				<a className="btn btn-primary">Choose!</a>
			</div>
		</div>
	);
};

export default Faction;