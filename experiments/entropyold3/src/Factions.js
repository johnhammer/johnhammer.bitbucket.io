import React, { Component } from 'react';
import Faction from './Faction.js';
import data from './english.json';

class Factions extends Component {
  createFaction (faction) {
    return <Faction source={faction} key={faction} />;
  }

  createFactions (factions) {
    return factions.map(this.createFaction);
  }

  render () {
    return (
		<div className="row" style={{overflow:"hidden",width:"100%",heigth:"100%"}}>
			{this.createFactions(data.Factions)}
		</div>
    );
  }
}

export default Factions;
