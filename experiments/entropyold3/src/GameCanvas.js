import React, { Component } from 'react';
import * as THREE from 'three'
import ReactDOM from 'react-dom'

// general globals

var container, scene, camera, renderer, controls;

function InitWorld(container2){
	
	container = container2;
	
	//Scene
	scene = new THREE.Scene();

	// Camera
	var screenWidth = window.innerWidth;
	var screenHeight = window.innerHeight;
	var viewAngle = 75;
	var nearDistance = 0.1;
	var farDistance = 1000;
	camera = new THREE.PerspectiveCamera(viewAngle, screenWidth / screenHeight, nearDistance, farDistance);
	scene.add(camera);
	camera.position.set(0, 0, 5);
	camera.lookAt(scene.position);

	// Renderer
	renderer = new THREE.WebGLRenderer({antialias: true,alpha: true});
	renderer.setClearColor(0x00000f, 1);
	renderer.setSize(screenWidth, screenHeight);
	//container = document.getElementById('container');
	container.appendChild(renderer.domElement);

	// Light
	var light = new THREE.PointLight(0xffffff);
	light.position.set(20, 0, 20);
	scene.add(light);
	var lightAmb = new THREE.AmbientLight(0x777777);
	scene.add(lightAmb);

	// Controls
	//controls = new THREE.OrbitControls( camera, renderer.domElement );
	//controls.enableZoom = true;
	
	//CreateMainTerrain();
	//SetStartResources();
}


class GameCanvas extends Component {
	componentDidMount() {
		var tthis = (ReactDOM.findDOMNode(this));
		InitWorld(tthis);
	}
	render() {
		return (
			<div id="mainContainter"></div>
		);
	}
}

export default GameCanvas;
