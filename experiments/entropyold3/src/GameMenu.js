import React, { Component } from 'react';

class GameMenu extends Component {
	render() {
		return (
			<div className="col-2 col-lg-2">    
			  <nav className="navbar navbar-inverse">         
				<ul className="nav nav-pills nav-stacked text-center" id="menu_1">
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				  <li><a href="#" data-toggle="modal" data-target="#modal_menu">Menu Option</a></li>
				</ul>
			  </nav>
			</div>
		);	
	}
}

export default GameMenu;
