import * as THREE from 'three'

class MeshFactory{
	
	createMine(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.CylinderGeometry(0.5, 0, 2, 8, 8, false);

		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z += 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z -= 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z += 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z -= 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		return group;
	}
	
	createPlant(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.TorusGeometry(1, 0.2, 20, 20, 7);
		cube = new THREE.Mesh(geometry, material);
		cube.rotation.x = Math.PI/2;
		group.add(cube);
		
		geometry = new THREE.SphereGeometry(0.6, 10, 10);
		cube = new THREE.Mesh(geometry, material);
		group.add(cube);
		
		geometry = new THREE.CylinderGeometry(0.2, 0.2, 1, 8, 8, false);

		cube = new THREE.Mesh(geometry, material);
		cube.position.y -= 0.5;		
		cube.position.z += 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y -= 0.5;		
		cube.position.z -= 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y -= 0.5;		
		cube.position.z += 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y -= 0.5;		
		cube.position.z -= 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		return group;
	}
	
	createHouse(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.BoxGeometry(0.75, 2, 0.75);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z += 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z -= 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z += 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z -= 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		
		geometry = new THREE.BoxGeometry(0.75, 0.5, 0.5);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1.5;		
		cube.position.z += 0.7;		
		cube.position.x += 0;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1.5;		
		cube.position.z -= 0.7;		
		cube.position.x += 0;		
		group.add(cube);
		
		geometry = new THREE.BoxGeometry(0.5, 0.5, 0.75);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1.5;		
		cube.position.z += 0;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1.5;		
		cube.position.z -= 0;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		return group;
	}
	
	createRecycler(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.CylinderGeometry(1.75, 1, 1, 8, 1, false);
		cube = new THREE.Mesh(geometry, material);
		group.add(cube);
		
		return group;
	}
	
	createFactory(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.BoxGeometry(1, 1, 1);
		cube = new THREE.Mesh(geometry, material);
		group.add(cube);
		
		return group;
	}
	
	createBunker(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.SphereGeometry(119.62, 14.15, 10.02);
		cube = new THREE.Mesh(geometry, material);
		group.add(cube);
		
		//mesh.rotation.x = 0.97;
		
		return group;
	}
	
	createAntenna(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.BoxGeometry(1, 1, 1);
		cube = new THREE.Mesh(geometry, material);
		group.add(cube);
		
		return group;
	}
	
	createLab(material){
		const group = new THREE.Mesh();
		
		let geometry, cube;
		
		geometry = new THREE.BoxGeometry(1, 1, 1);
		
		cube = new THREE.Mesh(geometry, material);	
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z += 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z -= 0.7;		
		cube.position.x += 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z += 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		cube = new THREE.Mesh(geometry, material);
		cube.position.y += 1;		
		cube.position.z -= 0.7;		
		cube.position.x -= 0.7;		
		group.add(cube);
		
		return group;
	}
	
	createWorld(material){
		var size = 2;
		var faceGeometry = new THREE.DodecahedronGeometry(size, 1);

		for (var i = 0; i < faceGeometry.faces.length; i++) {
			var face = faceGeometry.faces[i];
			face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
			face.faceid = i;
			//rockFaces.push(face);
		} 

		faceGeometry.computeFaceNormals();
		
		var backgroundMesh = new THREE.Mesh(faceGeometry, material);

		var geo = new THREE.EdgesGeometry( faceGeometry ); 
		var mat = new THREE.LineBasicMaterial( { color: 0xffffff, linewidth: 2 } );
		var wireframe = new THREE.LineSegments( geo, mat );
		backgroundMesh.add( wireframe );
		
		return backgroundMesh;
	}
	
}

function ClickOnFace(geometry,face){
	/*
	if(model == null) return;

	var object = model.clone();

	var faceColorMaterial = new THREE.MeshPhongMaterial( { map: THREE.ImageUtils.loadTexture('https://johnhammer.bitbucket.io/entropy/images/365_preview.jpg') } );

	object.traverse( functi*on ( child ) {
		if ( child instanceof THREE.Mesh ) {
			child.material = faceColorMaterial;
		}
	} );
	cylinder = object ;
	cylinder.scale.set(0.2,0.2,0.2);
	
	var centerPoint=new THREE.Vector3();
	var vertices = geometry.vertices;
	centerPoint.add(vertices[ face.a ]);
	centerPoint.add(vertices[ face.b ]);
	centerPoint.add(vertices[ face.c ]);
	centerPoint.divideScalar(3);

	var faceArea = AreaOfTriangle(vertices,face);
	cylinder.scale.set(0.1*faceArea,0.1*faceArea,0.1*faceArea);

	cylinder.position.copy(centerPoint);  
	var newPos = new THREE.Vector3();
	newPos.addVectors ( centerPoint, face.normal.multiplyScalar( 1 ) ); 
	cylinder.lookAt(newPos);

	cylinder.rotateX(Math.PI / 2);

	scene.add(cylinder);

	AnadirEdificio();
}


function onMouseMove(event) {
	event.preventDefault();
	var mouse = {};
	mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
	mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
	var vector = new THREE.Vector3(mouse.x, mouse.y, 1);

	var ray = new THREE.Raycaster(camera.position, vector.sub(camera.position).normalize());
	ray.setFromCamera(mouse, camera);
	var intersects = ray.intersectObjects(targetList);

	if (intersects.length > 0) {
		if (intersects[0].object != INTERSECTED) {
			if (INTERSECTED) {
				INTERSECTED.face.color.setHex(INTERSECTED.currentHex);
			}
			INTERSECTED = intersects[0];
			var geometry = INTERSECTED.object.geometry;
			var face = INTERSECTED.face;
			INTERSECTED.currentHex = face.color.getHex();

			face.color.setHex(0xc0392b);
			geometry.colorsNeedUpdate = true;

			ClickOnFace(geometry,face);		
		}
	}
	else {
		if (INTERSECTED) {
			INTERSECTED.face.color.setHex(INTERSECTED.currentHex);
		}
		INTERSECTED = null;
	}
}



function AreaOfTriangle(vertices,face) {
	var v1 = vertices[face.a];
	var v2 = vertices[face.b];
	var v3 = vertices[face.c];
	var triangle = new THREE.Triangle( v1, v2, v3 );
	return triangle.area();*/
}

export default MeshFactory