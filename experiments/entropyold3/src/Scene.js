import React, { Component } from 'react'
import * as THREE from 'three'
import OrbitControls from './OrbitControls.js'
import MeshFactory from './MeshFactory.js'

class Scene extends Component {
	constructor(props) {
		super(props)

		this.start = this.start.bind(this)
		this.stop = this.stop.bind(this)
		this.animate = this.animate.bind(this)
		
		this.meshFactory = new MeshFactory();
		this.buttonClick = this.buttonClick.bind(this);
	}
		
	createMesh(index){
		const material = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture('./img/electr.png') , transparent: true} );
		
		switch(index){
			case 0:	this.mesh = this.meshFactory.createMine(material); break;
			case 1:	this.mesh = this.meshFactory.createPlant(material); break;
			case 2:	this.mesh = this.meshFactory.createHouse(material); break;
			case 3:	this.mesh = this.meshFactory.createRecycler(material); break;
			case 4:	this.mesh = this.meshFactory.createLab(material); break;
			case 5:	this.mesh = this.meshFactory.createFactory(material); break;
			case 6:	this.mesh = this.meshFactory.createAntenna(material); break;
			case 7:	this.mesh = this.meshFactory.createBunker(material); break;
			case 8:	this.mesh = this.meshFactory.createWorld(material); break;
		}
		return this.mesh;
	}
	
	componentDidMount() {
		const width = this.mount.clientWidth
		const height = this.mount.clientHeight

		this.scene = new THREE.Scene()
		const camera = new THREE.PerspectiveCamera(
			75,
			width / height,
			0.1,
			1000
		)
		const renderer = new THREE.WebGLRenderer({ antialias: true })

		camera.position.z = 4
		renderer.setClearColor('#000000')
		renderer.setSize(width, height)

		var light = new THREE.AmbientLight( 0x404040 );
		this.scene.add( light );
		
		this.camera = camera
		this.renderer = renderer

		this.scene.add(this.createMesh(0));

		this.mount.appendChild(this.renderer.domElement)
		
		camera.aspect = window.innerWidth / window.innerHeight;
		camera.updateProjectionMatrix();
		renderer.setSize( window.innerWidth, window.innerHeight );
		
		this.controls = new OrbitControls( camera );
		
		this.start();
		
	}

	componentWillUnmount() {
		this.stop()
		this.mount.removeChild(this.renderer.domElement)
	}

	start() {
		if (!this.frameId) {
			this.frameId = requestAnimationFrame(this.animate)
		}
	}

	stop() {
		cancelAnimationFrame(this.frameId)
	}

	animate() {
		this.controls.update();
		this.renderScene()
		this.frameId = window.requestAnimationFrame(this.animate)
	}

	renderScene() {
		this.renderer.render(this.scene, this.camera)
	}

	buttonClick(index){
		this.scene.remove(this.mesh);
		this.scene.add(this.createMesh(index));
	}
	
	render() {
		return (
			<div>
				<a href="#" onClick={()=>this.buttonClick(0)}>Mine</a>
				<a href="#" onClick={()=>this.buttonClick(1)}>Plant</a>
				<a href="#" onClick={()=>this.buttonClick(2)}>House</a>
				<a href="#" onClick={()=>this.buttonClick(3)}>Recycler</a>
				<a href="#" onClick={()=>this.buttonClick(4)}>Lab</a>
				<a href="#" onClick={()=>this.buttonClick(5)}>Factory</a>
				<a href="#" onClick={()=>this.buttonClick(6)}>Antenna</a>
				<a href="#" onClick={()=>this.buttonClick(7)}>Bunker</a>
				<a href="#" onClick={()=>this.buttonClick(8)}>World</a>
				<div ref={(mount) => { this.mount = mount }}/>
			</div>
		)
	}
}


export default Scene