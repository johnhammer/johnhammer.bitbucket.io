import React from "react";


 
function toHex(str) {
	var hex = '';
	for(var i=0;i<str.length;i++) {
		hex += ''+str.charCodeAt(i).toString(16);
	}
	return hex;
}

function a2hex(str) {
  var arr = [];
  for (var i = 0, l = str.length; i < l; i ++) {
      var hex = Number(str.charCodeAt(i)).toString(16);
      arr.push(hex.length > 1 && hex || "0" + hex);
  }
  return arr.join('');
}

function hex2a(hexx) {
    var hex = hexx.toString();
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function getMapWord(text,pos,size=80){
	var mapname = text.substring(pos,pos+size);
	return mapname.substring(0, mapname.indexOf("\0"));
}

function getMapHexFrag(text,pos,size){
	var mapname = text.substring(pos,pos+size);	
	var hex = mapname;
	
	return hex
	
	var arr = [];
	
	
	for (var i = 0; i < hex.length; i++) {
		
		
		var size = 160;
		
		if(i%size==0){
			arr.push("\n")
		}
		
		var chars = hex.substring(i,i+1);
		
		arr.push(chars);
	}
	
	return arr.toString();
	
}



function paintMap(hex){
	
	var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
	
	canvas.width = Math.sqrt(hex.length/6)/2;
    canvas.height = Math.sqrt(hex.length/6)*2;
	
    var imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
    var buf = new ArrayBuffer(imageData.data.length);
    var buf8 = new Uint8ClampedArray(buf);
    var data = new Uint32Array(buf);
	

	for (var i = 0; i < hex.length/6; i++) {
		
		var value = getHexColor(hex,i);
				
		data[i] =
			(255   << 24) 	|    	// alpha
			(value/2 << 16) |    	// blue
			(value <<  8) 	|    	// green
			255;            		// red

    }
    imageData.data.set(buf8);
    ctx.putImageData(imageData, 0, 0);
	
	document.body.appendChild(canvas);
}

function paintMap2(hex){
	console.log(hex);
}

function getHexColor(hex,i){
	var chars = hex.substring(i*6,i*6+6);
	if(chars == "fffd00"){
		return 200;
	}
	else{
		return 50;
	}
}


function process(text){

	var hex = buf2hex(text);

	//10240
	/*
	var json = {
		name : getMapWord(text,7766),
		author : getMapWord(text,7830),
		biome : getMapWord(text,7894),
		description : getMapWord(text,10240,496),
		//terrain : getMapHexFrag(text,20542,8114)
		
		
		
	}*/
	
	//paintMap2(json.terrain);
	
	return getMapHexFrag(hex,20480,28672-20480)//JSON.stringify(json);
}

function buf2hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

class TextFileReader extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			text: ""
		};
	}

	componentDidMount() {
		this.readTextFile(this.props.txt);
	}
 
	readTextFile = file => {
		var rawFile = new XMLHttpRequest();

		rawFile.open("GET", file, true);
		rawFile.responseType = "arraybuffer";
		rawFile.onreadystatechange = () => {
			if (rawFile.readyState === 4) {
				if (rawFile.status === 200 || rawFile.status == 0){
					console.log(rawFile)
					var allText = rawFile.response;
					this.setState({
						text: process(allText)
					});
				}
			}
		};
		rawFile.send(null);
	};

	render() {
		return (
			<div>
				{this.state.text.split("\n").map((item, key) => {
					return <span key={key}>{item}<br /></span>;
				})}
			</div>
		);
	}
}

export default TextFileReader;