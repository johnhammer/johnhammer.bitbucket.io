import React from 'react';
var carz = require('../cars.json');
import _ from "underscore";

class Car extends React.Component {
    constructor(props) {
        super(props);
        this.state = {car: {}};
        this.fetchCar= this.fetchCar.bind(this);
    }

    fetchCar(){
        var carId = this.props.params.id;
        var theCar = _.find(carz.cars, function (car) { return car.id == carId; })
        this.setState({car : theCar});
    }

    componentDidMount() {
        this.fetchCar();
    }

    render() {
        return (
            <div className="page-wrapper detail-page">
                    <div className="car-name">
                        {this.state.car.name}
                    </div>
                <div className="content-wrapper">
                    <div className="details white-background">
                        <div className="car-price">
                            {this.state.car.price}
                        </div>
                        <div className="car-location">
                            <i className="font-icon icon-location-alt"></i>
                            {this.state.car.location}
                        </div>
                        <div className="car-description">
                            {this.state.car.description}
                        </div>
                    </div>
                    <div className="car-images white-background">
                        <div className="main-image">
                            <img src={this.state.car.main_picture}/>
                        </div>
                        <div className="thumbnails">
                            <a className="thumbnail thumbnail-prev"></a>
                            <div className="images">
                                <img src={this.state.car.thumbnail}/>
                            </div>
                            <a className="thumbnail thumbnail-nxt"></a>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default Car;
