import React from 'react';
import { Router, Route, IndexRoute, IndexLink, Link} from 'react-router';

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
           <header>
               <Link to="/">
                   Home
               </Link>
           </header>
        );
    }
}

export default Header;
