//forest.js

var container;
var camera, scene, renderer;
var delta;
var time;
var oldTime;

var trees = [];
var objects = [];
var rays = [];

var uniforms;
var uniformsFlower;
var uniformsBushes;
var uniformsParticles;
var uniformsRays;

var cameraTarget = new THREE.Vector3(0,0,-100);
var cameraExtra = new THREE.Vector2(0,0);

var butterflys = [];

var backdropMaterial;
var bgSprite;

var ground;
var bark;
var bark2;
var bark3;

var mouse = new THREE.Vector2(0,0);

var extraTime = {value: 0, noise: 0};
var triggers = [false,false,false];

var touchDevice = ( ('ontouchstart' in document) || (navigator.userAgent.match(/ipad|iphone|android/i) != null) );
var scaleRatio = 1;

startTime = Date.now();
checkLoading();
init();

var loadedItems = 0;

function checkLoading() {

	++loadedItems;

	if (loadedItems >= 1) {
		animate();
		
		var alphaTween = new TWEEN.Tween(bgSprite.material)
			.to({opacity: 0}, 3000)
			.easing(TWEEN.Easing.Cubic.In)
			.onComplete(function () {
				camera.remove( bgSprite );
			});
		alphaTween.start();		
	}
}

function init() {
	container = document.createElement( 'div' );
	document.body.appendChild( container );

	scene = new THREE.Scene();
	scene.fog = new THREE.Fog(0xabaf99, 0, 2000)

	camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 3000 );
	camera.position.z = 0;
	camera.position.y = 40;	
	camera.lookAt(cameraTarget);
	scene.add( camera );

	// ground
	var plane = new THREE.PlaneGeometry(5000,5000);
	plane.computeTangents();

	var material = new THREE.MeshBasicMaterial( { color: 0x0f110d, 
		map: THREE.ImageUtils.loadTexture( "9451-ambientocclusion.jpg", undefined, checkLoading ),
							  } );
	material.map.wrapS = THREE.RepeatWrapping;
	material.map.wrapT = THREE.RepeatWrapping;
	material.map.repeat.x = 20;
	material.map.repeat.y = 20;

	ground = new THREE.Mesh(plane, material);
	ground.rotation.x = -Math.PI*0.5;
	scene.add(ground);

	// tree
	loader = new THREE.JSONLoader();
	loader.load( "https://bitbucket.org/johnhammer/johnhammer.bitbucket.io/raw/a5db86bc756807f65ab0c8145086be7f3497973b/forest/spring/tree.js", treeLoaded );

	var aLight = new THREE.AmbientLight( 0x151c0f );
	scene.add(aLight);

	var pLight = new THREE.PointLight( 0xe3fbdc, 0.9 );
	pLight.position.set(1000,600,0);
	scene.add(pLight);

	// renderer
	renderer = new THREE.WebGLRenderer({antialias: false});
	renderer.setSize( window.innerWidth/scaleRatio, window.innerHeight/scaleRatio );
	renderer.setClearColor(scene.fog.color);
	renderer.sortObjects = false;

	//THREEx.WindowResize(renderer, camera);

	container.appendChild( renderer.domElement );
	
	if (scaleRatio > 1) {
		renderer.domElement.style.webkitTransform = "scale3d("+scaleRatio+", "+scaleRatio+", 1)";
		renderer.domElement.style.webkitTransformOrigin = "0 0 0";
		renderer.domElement.style.transform = "scale3d("+scaleRatio+", "+scaleRatio+", 1)";
		renderer.domElement.style.transformOrigin = "0 0 0";				

		renderer.domElement.style.position = "absolute";
		renderer.domElement.style.top = "0px";
		renderer.domElement.style.left = "0px";
	}
}

function treeLoaded( geometry, mm ) {
	addGrass();
}

function addGrass () {
				
	var planeGeometry = new THREE.PlaneGeometry(800, 8, 20, 2);

	for (var i = 0; i < planeGeometry.vertices.length; i++) {
		planeGeometry.vertices[i].z += Math.sin(planeGeometry.vertices[i].x*0.2)*10;
	}

	planeGeometry.applyMatrix( new THREE.Matrix4().setPosition( new THREE.Vector3( 0, 4, 0 ) ) );
	
	var map = THREE.ImageUtils.loadTexture( "fins2.png", undefined, checkLoading );
	map.wrapS = THREE.RepeatWrapping;

	var shadow = THREE.ImageUtils.loadTexture( "9128-ambientocclusion.jpg", undefined, checkLoading );
	shadow.wrapS = shadow.wrapT = THREE.RepeatWrapping;

	var attributes = {

		customColor: { type: 'c', value: [] },
		time:		 { type: 'f', value: [] },
		uvScale:	 { type: 'v2', value: [] },
		
	};

	uniforms = {

		color:      { type: "c", value: new THREE.Color( 0x53544d ) },
		sunColor:      { type: "c", value: new THREE.Color( 0xe2e784 ) },
		texture:    { type: "t", value: map },
		shadow:    { type: "t", value: shadow },
		globalTime:	{ type: "f", value: 0.0 },
		fogColor : { type: "c", value: scene.fog.color },
		fogNear : { type: "f", value: scene.fog.near },
		fogFar : { type: "f", value: scene.fog.far*0.75 },
		size: { type: "v2", value: new THREE.Vector2( 1200.0, 2400.0 ) },					
	};

	var material = new THREE.ShaderMaterial( {

		uniforms: 		uniforms,
		attributes:     attributes,
		vertexShader:   document.getElementById( 'grass_vertexshader' ).textContent,
		fragmentShader: document.getElementById( 'grass_fragmentshader' ).textContent,

		transparent:	true,
		
	});



	var geometry = new THREE.Geometry();

	for (var i = 0; i < 500; i++) {
		var mesh = new THREE.Mesh(planeGeometry);
		mesh.rotation.y = Math.random()-0.5;
		mesh.position.set(Math.random()*800-400, 0, -1400);
		mesh.scale.y = 1 + Math.random()*1.75;

		THREE.GeometryUtils.merge(geometry, mesh);
	};


	var planeGeometry2 = new THREE.PlaneGeometry(30, 30, 1, 2);
	planeGeometry2.applyMatrix( new THREE.Matrix4().setPosition( new THREE.Vector3( 0, 15, 0 ) ) );

	for (var i = 0; i < 200; i++) {
		var mesh = new THREE.Mesh(planeGeometry2);
		mesh.rotation.y = Math.random()-0.5;
		mesh.position.set(Math.random()*800-400, 0, -1400);
		mesh.scale.y = 1 + Math.random()*0.5;
		
		THREE.GeometryUtils.merge(geometry, mesh);
	};



	var vertices = geometry.vertices;
	var values_time = attributes.time.value;
	var values_uv = attributes.uvScale.value;
	var values_color = attributes.customColor.value;

	var l1 = planeGeometry.vertices.length*500;

	for( var v = 0; v < l1; v+=planeGeometry.vertices.length ) {

		var t = Math.random();

		for (var j = v; j < v+planeGeometry.vertices.length; j++) {
			values_time[j] = t;
			values_uv[j] = new THREE.Vector2(56,1);
			values_color[j] = new THREE.Color(0xffffff);
			values_color[j].setHSL(0.25,0.25-Math.random()*0.25,0.7);
		};

	}


	var l2 = planeGeometry2.vertices.length*200;

	for( var v = l1; v < l1+l2; v+=planeGeometry2.vertices.length ) {

		var t = Math.random();

		for (var j = v; j < v+planeGeometry2.vertices.length; j++) {
			values_time[j] = t;
			values_uv[j] = new THREE.Vector2(1.5,1);
			values_color[j] = new THREE.Color(0x4ea648);
			values_color[j].setHSL(0.25,0.5+Math.random()*0.25,0.5);
		};

	}

	var planes = new THREE.Mesh(geometry, material);
	scene.add(planes);

}

function animate() {

	requestAnimationFrame( animate );

	render();

}


function render() {
/*
	time = Date.now();
	delta = time - oldTime;
	oldTime = time;

	if (isNaN(delta) || delta > 1000 || delta == 0 ) {
		delta = 1000/60;
	}
	
	var optimalDivider = delta/16;
	var smoothing = Math.max(5, (30/optimalDivider) );

	cameraExtra.x += (mouse.x - cameraExtra.x)/smoothing;
	cameraExtra.y += (mouse.y - cameraExtra.y)/smoothing;

	camera.position.x = Math.sin(time*0.0004)*10;
	camera.position.y = 40+Math.cos(time*0.0002)*10;

	cameraTarget.x = Math.cos((time*0.0006)+extraTime.value)*10 + cameraExtra.x*20;
	cameraTarget.y = 65+Math.sin((time*0.0003)+extraTime.value)*10 + cameraExtra.y*20;

	cameraTarget.x += ((Math.random()-0.5)*extraTime.noise)*5;
	cameraTarget.y += ((Math.random()-0.5)*extraTime.noise)*5;

	camera.lookAt(cameraTarget);

	camera.up.x = cameraExtra.x*0.2;

	var speed = delta*0.030;

	if (uniforms) {
		uniforms.globalTime.value += delta * 0.0012;
	}

	if (backdropMaterial) {
		backdropMaterial.map.offset.x -= speed*0.0028;
	}

	ground.material.map.offset.y += delta * 0.000133;

	TWEEN.update();
*/
	
	renderer.render( scene, camera );

}

