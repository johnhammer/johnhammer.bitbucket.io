/*
var WEIGHTS = [
	{v:FUNCTIONS.add,w:2},
	{v:FUNCTIONS.subtract,w:3},
	{v:FUNCTIONS.multiply,w:2},
	{v:FUNCTIONS.divide,w:2},
	{v:FUNCTIONS.equal,w:5},
	{v:FUNCTIONS.greater,w:5},
	{v:FUNCTIONS.and,w:3},
	{v:FUNCTIONS.or,w:3},
	{v:FUNCTIONS.not,w:3},
	{v:FUNCTIONS.if,w:15},
	{v:FUNCTIONS.loop,w:2},
	{v:FUNCTIONS.break,w:15},
	{v:FUNCTIONS.push,w:40},
	{v:FUNCTIONS.push,w:40},
	{v:FUNCTIONS.push,w:40},
	{v:FUNCTIONS.push,w:40},
	{v:FUNCTIONS.push,w:40},
]
*/

var FUNCTIONS = {
	add : 			0,//{t:'numNumToNum'},
	subtract : 		1,//{t:'numNumToNum'},
	multiply : 		2,//{t:'numNumToNum'},
	divide :		3,//{t:'numNumToNum'},
	equal : 		4,//{t:'numNumToBool'},
	greater : 		5,//{t:'numNumToBool'},
	and : 			6,//{t:'boolBoolToBool'},
	or : 			7,//{t:'boolBoolToBool'},
	not : 			8,//{t:'boolToBool'},
	if : 			9,//{t:'boolFlow'},
	loop : 			10,//{t:'boolFlow'},
	break : 		11,//{t:'noop'},
	push : 			12,//{t:'boolFlow'},
	for :			13,
	assign : 		14,
	zero : 			15,
	increment : 	16
}

function getInstruction(code){
	switch(code.f){
		
	case FUNCTIONS.add: 
		return binaryOp("+",code.a,code.b,code.c);
	case FUNCTIONS.subtract: 
		return binaryOp("-",code.a,code.b,code.c);
	case FUNCTIONS.multiply: 
		return binaryOp("*",code.a,code.b,code.c);
	case FUNCTIONS.divide: 
		return binaryOp("/",code.a,code.b,code.c);
		
	case FUNCTIONS.equal: 
		return binaryOp("==",code.a,code.b,code.c);
	case FUNCTIONS.greater: 
		return binaryOp(">",code.a,code.b,code.c);
		
	case FUNCTIONS.and: 
		return binaryOp("&&",code.a,code.b,code.c);
	case FUNCTIONS.or: 
		return binaryOp("||",code.a,code.b,code.c);
		
	case FUNCTIONS.not: 
		return unaryOp("!",code.a,code.b);
		
	case FUNCTIONS.if:
		return {b:1,v:"if("+getVariable(code.a)+"){"};
	case FUNCTIONS.loop:
		return {b:3,v:"for(var i=0;"+getVariable(code.a)+"&&i<"+LOOPSLIMIT+";i++){"};
	case FUNCTIONS.break:
		return {b:2,v:"}"};
	case FUNCTIONS.push:
		return "r.push("+getVariable(code.a)+")";	
		
	case FUNCTIONS.for :
		return {b:3,v:"for(var i=0;i<"+getVariable(code.a)+"&&i<"+LOOPSLIMIT+";i++){"};
	case FUNCTIONS.assign : 		
		return unaryOp("",code.a,code.b);
	case FUNCTIONS.zero : 			
		return unaryOp("",code.a,0);
	case FUNCTIONS.increment : 	
		return unaryOp("",code.a,code.a+"+1");
		
	}
}

function getType(){
	return wChoice(WEIGHTS);
}
	
function getRandomLine (){
	var type = getType(FUNCTIONS);
	return {
		f:type,
		a:getRandomInt(AUXILIARS+PARAMETERS-1,0),
		b:getRandomInt(AUXILIARS+PARAMETERS-1,0),
		c:getRandomInt(AUXILIARS+PARAMETERS-1,0)
	}
}	

function getCode(jcode,json){
	var code = "";
	code += "(function(";
		
	code +=  getVariable(0);
	for(var i=1;i<PARAMETERS;i++) {
		code +=  "," + getVariable(i);
	}		
		
	code += "){var r=[];";
		
	for(var i=PARAMETERS;i<PARAMETERS+AUXILIARS;i++) {
		code += getVariable(i) + " = 0;";
	}	
		
	var extraKeys = 0;
	var nestedLoops = 0;
	for(var i=0;i<jcode.length;i++) {
		var instr = getInstruction(jcode[i]);
		if(instr.b==1){ //if
			code += instr.v;
			extraKeys++;
		}
		else if(instr.b==2){ //break
			if(extraKeys>0){
				code += instr.v;
				extraKeys--;
			}
		}
		else if(instr.b==3){ //loop
			if(nestedLoops<NESTEDLOOPS){
				code += instr.v;
				extraKeys++;
				nestedLoops++;
			}
		}
		else{
			code += instr + ";";
		}
	}
	for(var i=0;i<extraKeys;i++) {
		code += "}";
	}
		
	code += "return r;})";

	return f = eval(code);
}