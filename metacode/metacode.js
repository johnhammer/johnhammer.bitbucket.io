﻿/*
[{"i":[3,2],"o":[5]},{"i":[1,2],"o":[3]}]
[{"i":[3,2],"o":[1]},{"i":[1,2],"o":[-1]}]
[{"i":[3,2],"o":true},{"i":[1,2],"o":false}]
[{"i":[50],"o":[1]},{"i":[1,2],"o":[-1]}]
[{"i":[1,2,8],"o":[11]}]
[
{"i":[10,1,true],"o":[true]}
,{"i":[1,10,true],"o":[false]}
,{"i":[10,1,false],"o":[false]}
]


*/
function start(){
	
}

var stop = true;

var INTERFACE = IMPLgen2;
var LOOPSLIMIT = 1;
var NESTEDLOOPS = 1;
var INSTRUCTIONS = 7; 
var AUXILIARS = 0;
var PARAMETERS = 1;

function run(){
	FITNESS = [];
	
	var tests = Number(iterations.value);
	INSTRUCTIONS = Number(instructions.value);
	AUXILIARS = Number(auxiliars.value);
	LOOPSLIMIT = Number(loopslimit.value);
	NESTEDLOOPS = Number(nestedloops.value);
	
	var s = box.value;
	ans.innerHTML = "..."
	var json = JSON.parse(s);
	
	PARAMETERS = json[0].i.length;
	
	INTERFACE.START(json);
	var f;
	var superflag = false;
	var i;
	var val;
	for(i=0;i<tests;i+=1){	
	
		f = getCode(INTERFACE.RUN(i),json);
		
		var flag = false;
		
		for(var j=0;j<json.length;j+=1){			
			val = f.apply(this,json[j].i);
			if(!compareOutput(val,json[j].o)){
				flag = true;
				break;
			}			
		}
		if(!flag && stop){
			superflag = true;
			break;
		}
		else if(!flag){
			superflag = true;
			console.log('Problem solved in '+i+' iterations, continuing');	
			ans.innerHTML = 'Problem solved in '+i+' iterations, continuing<br>';
		}
	}
	if(superflag){
		fitnessChart()
		console.log('Problem solved in '+i+' iterations');
		console.log(f);
		console.log(val);
		ans.innerHTML = 'Problem solved in '+i+' iterations<br>';
		ans.innerHTML += (f+"")
			.replace(/;/g, ";<br>")
			.replace(/}/g, "}<br>")
			.replace(/{/g, "{<br>\t");
		ans.innerHTML += val;
	}
	else{
		fitnessChart()
		console.log('Unable to solve in '+i+' iterations');
		ans.innerHTML = 'Unable to solve in '+i+' iterations';
	}
}


