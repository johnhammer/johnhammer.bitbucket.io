extends Panel

const ObjectResource = preload("res://room/Object.tscn")

var activeBuild
var list=[]
var matrix=[]

func InitButton(i):
	var node = get_node("Button"+String(i+1))
	node.visible = true
	node.icon = load("res://res/objects/"+list[i]+".png")

func Init(type,room):
	if !type in RoomFactory.RoomsDictionary:
		return
	var roomtype = RoomFactory.RoomsDictionary[type]
	matrix = room.matrix
	$Button1.visible = false
	$Button2.visible = false
	$Button3.visible = false
	$Button4.visible = false
	$Button5.visible = false
	$Button6.visible = false
	$Button7.visible = false
	$Button8.visible = false
	if "build" in roomtype:
		list = roomtype.build
	for i in range(list.size()):
		InitButton(i)
		
	#minigames
	if roomtype.n == "circuit":
		Electronics.cables = []
		Electronics.TableOfTruth(Random.Create(room.seed),room.objects,$List)

func GetBuilding(x,y):
	if x<1 || y<1 || x>matrix.size()-2 || y>matrix[0].size()-2:
		return null
	
	if activeBuild!=null:
		var rnd = Random.Create(Random.GlobalSeed)
		var obj = ObjectFactory.ObjectsDictionary[activeBuild].duplicate()
		obj.x = x
		obj.y = y
		obj.dir = 0
		obj.color = Utils.GetColor(rnd,obj.color)
		obj.id = activeBuild
		activeBuild = null
		if "txt" in obj: #graffiti
			obj.dir = Random.Range(rnd,0,3)
			obj.txt = "EDS"
		
		
		var valid = ObjectFactory.IsObjectValid(x,y,obj.w,obj.h,matrix)
		if valid:
			var item = ObjectResource.instance()
			item.Init(obj,get_node(".."))
			obj.node = item
			return item
	return null

func BuildButton(id):
	var build = list[id-1]
	if build == "cable":
		Electronics.mode = 1
	elif build == "pliers":
		Electronics.mode = 3
	elif build == "on":
		Electronics.StartCircuit()
	elif build == "play":
		Cards.Play()
	elif build == "double":
		Cards.Double()
	elif build == "retire":
		Cards.Retire()
	elif build == "abandon":
		Cards.Abandon()
	else:
		activeBuild = build

func _draw():
	for cable in Electronics.cables:
		var p1 = Vector2(cable.c1.position.x+608,cable.c1.position.y+32)
		var p2 = Vector2(cable.c2.position.x+608,cable.c2.position.y+32)
		var pos = p1 + (p2 - p1).normalized()*32
		var pos2 = p2 - (p2 - p1).normalized()*32
		draw_line(pos,pos2,Color(1,1,1),3.0)
		draw_circle(pos2,10.0,Color(1,1,1))

func _on_Button1_pressed(): BuildButton(1)
func _on_Button2_pressed(): BuildButton(2)
func _on_Button3_pressed(): BuildButton(3)
func _on_Button4_pressed(): BuildButton(4)
func _on_Button5_pressed(): BuildButton(5)
func _on_Button6_pressed(): BuildButton(6)
func _on_Button7_pressed(): BuildButton(7)
func _on_Button8_pressed(): BuildButton(8)
