extends Panel

func HideActions():
	$Button.visible = false
	$Button2.visible = false

func Init(object):
	var actions = []
	if "a" in object:
		actions = object.a
	$Title.text = object.n
	$Description.text = object.d
	HideActions()
	if actions.size()>0:
		if actions[0] == "move":
			var destObj = object.data.door
			if destObj.n=="0":
				$Button.text = "Move out"
			elif destObj.n=="-1":
				$Button.text = "Move out all"
			elif object.n=="speakdoor":
				$Button.text = String(destObj.node)
			else:
				var roomNode = RoomFactory.RoomsDictionary[destObj.n]
				$Button.text = "Move to "+roomNode.n+" "+String(destObj.node)
		else:
			$Button.text = actions[0]
	
		$Button.visible = true
	if actions.size()>1:
		$Button2.text = actions[1]
		$Button2.visible = true
