extends Panel

var inventory = [
	"branch",
	"log",
	"branch",
	"leaves",
	"spray",
	"food_container_empty",
	"food_container_full",
	"toolkit",
	"knife",
	"pocketknife",
]

func _ready():
	for item in inventory:
		$ItemList.add_icon_item(load("res://res/items/"+item+".png"))

