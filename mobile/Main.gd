extends Node2D

var selected

func _ready():
	ObjectFactory.ObjectsDictionary = Utils.load_json("objects")
	RoomFactory.RoomsDictionary = Utils.load_json("rooms")
	TreeFactory.TreesDictionary = Utils.load_json("trees")
	MazeFactory.MazeDictionary = Utils.load_json("mazes")
	
	Start()

func MapChange(type,room):
	$Info.Init(type)
	$Build.Init(type,room)
	$Map.DrawRoom(room,type.n)

func Select(object):
	if "autoenter" in object.obj: 
		if selected!=null: selected.Deselect()
		Doors.Enter(self,object.obj)
	
	if "n" in object.obj:
		if selected!=null: selected.Deselect()
		selected = object
		object.Select()
		$Info.Init(object.obj)

func Action(object,id):
	var action = object.obj.a[id]
	if action=="move":
		selected = null
		Universe.Move(self,object.obj)
	elif action=="enter":
		selected = null
		Universe.Enter(self,object.obj)
	elif action=="speak":
		Universe.Speak(self,object.obj)
		selected = null

func _on_Button_pressed(): Action(selected,0)
func _on_Button2_pressed(): Action(selected,1)

func PickRandomMazeObject(rnd,maze,type):
	var objects = MazeFactory.GetObjectsByType(maze,type)
	return Random.Choice(rnd,objects)

func Start():
	var mazetype = "multiverse"
	var room = RoomFactory.Create(Random.GlobalSeed,mazetype,[],[])
	$Map.DrawRoom(room,mazetype)
	$Build.Init(mazetype,room)
	return
	
	var rnd = Random.Create(Random.GlobalSeed)
	var maze = MazeFactory.Create(Random.GlobalSeed,"multiverse",[],[])	
	var next
	next = PickRandomMazeObject(rnd,maze,"universe")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"cluster")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"galaxygroup")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"spiralgalaxy")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"quadrant")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"stargroup")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"star")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"planet")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"region")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"urbanization")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"neighborhood")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"house")
	maze = Doors.Enter(self,next)
	next = PickRandomMazeObject(rnd,maze,"bedroom_small")
	maze = Doors.Enter(self,next)
	
	

