extends Panel

var current

const ObjectResource = preload("res://room/Object.tscn")

func SecondColor(object):
	var item2 = ObjectResource.instance()
	$Nodes.add_child(item2)
	item2.Init({
		"x":object.x,
		"y":object.y,
		"h":object.h,
		"w":object.w,
		"dir":object.dir,
		"id":object.id+"_2",
		"color":object.color2
	},get_node(".."))

func DrawFloor(type,x,y,w,h):
#	if type==1:
#		for i in range(w+2):
#			for j in range(h+2):
#				if x+i>1 && y+j>1 && x+i<11 && y+j<11:
#					$Floor2.set_cell(x+i-2,y+j-2,1)
#	else:
		for i in range(w):
			for j in range(h):
				$Floor2.set_cell(x+i-1,y+j-1,type)

func DrawRoom(room,type):
	current = room
	
	for x in range(9):
		for y in range(9):
			$Floor.set_cell(x,y,-1)
			$Floor2.set_cell(x,y,-1)
	
	for x in range(room.x):
		for y in range(room.y):
			$Floor.set_cell(x,y,0)
			
	$Floor.modulate = room.color
	
	Utils.ClearObjects($Nodes)
	
	if type == "cards":
		Cards.Init(room,$Nodes)
	elif type == "fight":
		Fight.Init(room,$Nodes)
	elif type == "battle":
		Battle.Init(room,$Nodes)
	
	for object in room.objects:
		if "color2" in object:
			SecondColor(object)
		if "floor" in object:
			DrawFloor(object.floor,object.x,object.y,object.w,object.h)
			$Floor2.modulate = object.floorcolor
		
		var item = ObjectResource.instance()
		$Nodes.add_child(item)
		object.node = item
		item.Init(object,get_node(".."))
	
	$Floor.update_bitmask_region(Vector2(0,0),Vector2(10,10))
	$Floor2.update_bitmask_region(Vector2(0,0),Vector2(10,10))

func _input(event):
	if event is InputEventMouseButton:
		var pos = event.position
		if event.pressed:
			var tile_pos = $Floor.world_to_map(pos)
			var building = get_node("../Build").GetBuilding(tile_pos.x-9,tile_pos.y)
			if building!=null:
				$Nodes.add_child(building)

