extends Node2D

func _on_Map_pressed():
	get_node("../Map").visible = true
	get_node("../Inventory").visible = false


func _on_Inventory_pressed():
	get_node("../Map").visible = false
	get_node("../Inventory").visible = true


func _on_Build_pressed():
	get_node("../Build").visible = true
	get_node("../Info").visible = false


func _on_Info_pressed():
	get_node("../Build").visible = false
	get_node("../Info").visible = true
