extends Node2D

var selected = false
var obj
var parent
var CircuitValue = null

func UpdateText(txt):
	$Label.text = txt

func Init(_obj,_parent):
	obj = _obj
	parent = _parent
	$Object.texture_normal = load("res://res/objects/"+obj.id+".png")
	position.x = obj.x*64
	position.y = obj.y*64
	
	$Object.modulate = obj.color
	if "txt" in obj:
		$Label.text = String(obj.txt)
	
	if obj.dir == Utils.Direction.down:
		$Object.rect_rotation = 0
	if obj.dir == Utils.Direction.left:
		$Object.rect_rotation = 90
		$Object.rect_position.x += (obj.w)*64
	if obj.dir == Utils.Direction.up:
		$Object.rect_rotation = 180
		$Object.rect_position.x += (obj.h)*64
		$Object.rect_position.y += (obj.w)*64
	if obj.dir == Utils.Direction.right:
		$Object.rect_rotation = 270
		$Object.rect_position.y += (obj.h)*64
	
	$Object.rect_scale.x = obj.h
	$Object.rect_scale.y = obj.w

func Select():
	#normal
	$Object.modulate = Color(obj.color.r/2.0,obj.color.g/2.0,obj.color.b/2.0,0.8)
	get_tree().set_input_as_handled()
	
	#electronics
	selected = false
	if Electronics.mode==1:
		Electronics.CableLink1(self)
	elif Electronics.mode==2:
		Electronics.CableLink2(self)
		parent.get_node("Build").update()
	elif Electronics.mode==3:
		Electronics.Pliers(self)
		parent.get_node("Build").update()
	else:
		selected = true
	
func Deselect():
	$Object.modulate = obj.color
	selected = false

func _on_Object_pressed():
	if !selected:
		parent.Select(self)

func CircuitActivation(value):
	CircuitValue = value
	if value==1:
		$Object.modulate=Color(0,0,1)
	elif value==0:
		$Object.modulate=Color(0,1,0)
	else:
		$Object.modulate=Color(1,0,0)
