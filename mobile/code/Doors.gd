extends Node

func Enter(main,obj):
	var lastType ="room"
	var mazetype = "multiverse"
	var maze = RoomFactory.Create(Random.GlobalSeed,mazetype,[],[])
	
	for node in obj.address:
		var mapObj
		if lastType=="maze":
			mapObj = MazeFactory.GetObjectByPos(maze.objects,node.x,node.y)
		else:
			mapObj = RoomFactory.GetObjectById(maze.objects,node.id)
		lastType = node.type
		if lastType=="maze":
			maze = MazeFactory.Create(mapObj.sed,mapObj.id,mapObj.address,mapObj.doors)
		else:
			maze = RoomFactory.Create(mapObj.sed,mapObj.id,mapObj.address,mapObj.doors)
	
	main.get_node("Map").DrawRoom(maze,mazetype)
	return maze
