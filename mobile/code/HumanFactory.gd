extends Node

#creation on tree level

func CreateFamilies(rnd,pmin,pmax):
	var families = []
	var amt = Random.Range(rnd,int(pmin),int(pmax))
	for i in range(amt):
		families.push_back(CreateFamily(rnd))
	return families
	
func CreateFamily(rnd):
	var family = []
	family.push_back(CreateHuman(rnd))
	return family

func CreateHuman(rnd):
	return {
		"seed":Random.Int(rnd)
	}
