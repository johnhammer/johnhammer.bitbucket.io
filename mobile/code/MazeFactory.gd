extends Node

var MazeDictionary

func GetObjectByPos(objects,x,y):
	for object in objects:
		if object.x==x&&object.y==y:
			return object
	return null

func GetObjectsByType(maze,type):
	var result = []
	for object in maze.objects:
		if object.id==type:
			result.push_back(object)
	return result

func GetObjectDoor(rnd,obj,x,y):
	if "doors" in obj:
		for door in obj.doors:
			var lastpos = door.at[door.at.size()-1]
			if lastpos.x==x&&lastpos.y==y:
				return door

func CreateObjectDoor(rnd,objects,xme,yme,xyou,yyou,doordir):
	var obj = GetObjectByPos(objects,xyou,yyou)
	if obj!=null:
		var door = GetObjectDoor(rnd,obj,xme,yme)
		var pos
		if door!=null:
			pos = door.pos
		else:
			pos = Random.Range(rnd,1,9)
		return {
			"at":obj.address,
			"pos":pos,
			"p":doordir
		}
	return null

func GetObjectDoors(rnd,objects,x,y):
	var doors = []
	
	var door = CreateObjectDoor(rnd,objects,x,y,x+1,y,Utils.Direction.right)
	if door!=null:doors.push_back(door)
	door = CreateObjectDoor(rnd,objects,x,y,x-1,y,Utils.Direction.left)
	if door!=null:doors.push_back(door)
	door = CreateObjectDoor(rnd,objects,x,y,x,y+1,Utils.Direction.up)
	if door!=null:doors.push_back(door)
	door = CreateObjectDoor(rnd,objects,x,y,x,y-1,Utils.Direction.down)
	if door!=null:doors.push_back(door)	
	
	return doors

func CheckObjectivesReached(doorobjects):
	if doorobjects.size()==0:return true
	
	for door in doorobjects:
		if !door.connected: return false
		
	return true

func CreateMazeObject(rnd,MazeType,door,address,i,objects):
	var result = false
	var ObjectType = Random.WChoice(rnd,MazeType.nodes)
	if Utils.IsPosValidNoWalls(door.posx,door.posy,1,1):
		var contact = GetObjectByPos(objects,door.posx,door.posy)
		if contact !=null:
			if contact.doorId!=door.doorId:
				door.connected = true
		else:
			objects.push_back(ObjectFactory.Create(rnd,door.posx,door.posy,ObjectType,address,door.doorId,i))
			result = true
		if Random.Bool(rnd):
			if Random.Bool(rnd):door.posx += 1
			else:door.posx -= 1
		else:
			if Random.Bool(rnd):door.posy += 1
			else:door.posy -= 1
	else:
		door.posx = door.srcx
		door.posy = door.srcy
	return result

func CreateMazeObjects(rnd,MazeType,maze_x,maze_y,address,doorobjects):
	var objects = []
	var i=0
	var limit=0
	
	var doorId = 0
	for door in doorobjects:
		door.posx = door.srcx
		door.posy = door.srcy
		door.doorId = doorId
		door.connected = false
		doorId+=1
	
	while i<50 && limit<100:
		for door in doorobjects:
			if CreateMazeObject(rnd,MazeType,door,address,i,objects):
				i+=1
		limit+=1
	
	return objects

func AddDoorLevel(doors,address):
	var level = address[address.size()-1]
	match doors.p:
		Utils.Direction.right:
			doors.at.push_back({"x":1,"y":level.y,"type":level.type})
		Utils.Direction.left:
			doors.at.push_back({"x":9,"y":level.y,"type":level.type})
		Utils.Direction.up:
			doors.at.push_back({"x":level.x,"y":1,"type":level.type})
		Utils.Direction.down:
			doors.at.push_back({"x":level.x,"y":9,"type":level.type})

func CreateMazeDoor(address,door):
	var color = Color(1,0,0)
	var doorlevel = door.at.size()
	var localLevel = address[doorlevel-1]
	var doorLevel = door.at[doorlevel-1]
	var dir
	var x
	var y
	var srcx
	var srcy
	if localLevel.x==doorLevel.x:
		if localLevel.y<doorLevel.y:
			dir = Utils.Direction.down
			x = door.pos
			srcx = door.pos
			y = 10
			srcy = 9
		else:
			dir = Utils.Direction.up
			x = door.pos
			srcx = door.pos
			y = 0
			srcy = 1
	else:
		if localLevel.x<doorLevel.x:
			dir = Utils.Direction.right
			y = door.pos
			srcy = door.pos
			x = 10
			srcx = 9
		else:
			dir = Utils.Direction.left
			y = door.pos
			srcy = door.pos
			x = 0
			srcx = 1
	var obj = {
		"x":x,"y":y,
		"srcx":srcx,"srcy":srcy,
		"enter":"universe",
		"w":1,"h":1,
		"address":door.at,
		"color":color,
		"id":"door",
		"connected":false,
		"autoenter":true,
		"dir":dir
	}
	if address.size()!=doorlevel: 
		obj.color = Color(0,0,1)
		AddDoorLevel(door,address)
		match door.p:
			Utils.Direction.right: obj.y=5
			Utils.Direction.left: obj.y=5
			Utils.Direction.up: obj.x=5
			Utils.Direction.down: obj.x=5
	
	return obj
	
func CleanMatrix(maze_x,maze_y):
	var matrix = []
	for i in range(maze_y+2):
		matrix.push_back([])
		for j in range(maze_x+2):
			matrix[i].push_back(0)
	return matrix

func Create(sed,type,address,doors):
	var MazeType = MazeDictionary[type]
	var rnd =  Random.Create(sed)
	var maze_x = Random.Range(rnd,9,9)
	var maze_y = Random.Range(rnd,9,9)
	var objects
	
	if doors.size()>0: #create doors and connect them with objects
		var doorobjects = []
		for door in doors:
			var newObj = CreateMazeDoor(address,door)
			doorobjects.push_back(newObj)
		var valid = false
		var level = 0
		while !valid && level<100 :
			objects = CreateMazeObjects(rnd,MazeType,maze_x,maze_y,address,doorobjects)
			valid = CheckObjectivesReached(doorobjects)
			level+=1
		objects+=doorobjects

	else: #no doors, start  at midpoint
		var fakeDoorAtMid = {
			"srcx":5,"srcy":5,
			"x":15,"y":15, "w":1,"h":1,"address":[],"color":Color(1,0,0),"id":"door","dir":0
		}
		objects = CreateMazeObjects(rnd,MazeType,maze_x,maze_y,address,[fakeDoorAtMid])
	
	for object in objects: #connect objects
		object.doors = GetObjectDoors(rnd,objects,object.x,object.y)
		
	return {
		"x":maze_x,
		"y":maze_y,
		"objects":objects,
		"color":Utils.GetColor(rnd,MazeType.color)
	}
