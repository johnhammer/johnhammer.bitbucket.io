extends Node

var ObjectsDictionary
const MAX_RECURSION_OBJECT = 50

func IsObjectValid(x,y,w,h,matrix):
	var valid = true
	for i in range(h):
		for j in range(w):
			valid = matrix[y+i][x+j] == 0
			if !valid: return false
		if !valid: return false
	for i in range(h):
		for j in range(w):
			matrix[y+i][x+j] = 1
	return true

func GetObjectRandomPosition(rnd,room_x,room_y,w,h,matrix,b,dir):
	var x
	var y
	if b==0 : #any pos
		x = Random.Range(rnd,1,room_x-w+1)
		y = Random.Range(rnd,1,room_y-h+1)
	elif b==1: #stick to wall
		if dir == Utils.Direction.down:
			x = Random.Range(rnd,1,room_x-w+1)
			y = 1
		if dir == Utils.Direction.left:
			x = room_x-w+1
			y = Random.Range(rnd,1,room_y-h+1)
		if dir == Utils.Direction.up:
			x = Random.Range(rnd,1,room_x-w+1)
			y = room_y-h+1
		if dir == Utils.Direction.right:
			x = 1
			y = Random.Range(rnd,1,room_y-h+1)
	elif b==2 || b==3:  #stick to wall+1
		if dir == Utils.Direction.up:
			x = Random.Range(rnd,1,room_x-w+1)
			y = 0
		if dir == Utils.Direction.right:
			x = room_x-w+2
			y = Random.Range(rnd,1,room_y-h+1)
		if dir == Utils.Direction.down:
			x = Random.Range(rnd,1,room_x-w+1)
			y = room_y-h+2
		if dir == Utils.Direction.left:
			x = 0
			y = Random.Range(rnd,1,room_y-h+1)
	
	var valid = Utils.IsPosValid(x,y,w,h,matrix)
	
	return {
		"x":x,"y":y,"valid":valid
	}

func GetObjDir(rnd,obj):
	#if "n" in obj && obj.n == "speakdoor":return 3
	
	if obj.data!= null && "door" in obj.data:
		if obj.data.door.n!="0":
			var w = obj.data.door.lat-obj.data.door.curlat
			var h = obj.data.door.lon-obj.data.door.curlon
			if abs(w)>abs(h):
				if w>0:
					return Utils.Direction.right
				else:
					return Utils.Direction.left
			else:
				if h>0:
					return Utils.Direction.up
				else:
					return Utils.Direction.down
				
	return Random.Range(rnd,0,3)

func Create(rnd,x,y,ObjectType,oldaddress,doorid,addressId):
	var data = ObjectsDictionary[ObjectType]
	var address = oldaddress.duplicate()
	address.push_back({
		"x":x,"y":y,"id":addressId
	})
	if "type" in data:address[address.size()-1].type = data.type
	var obj = {
		"x":x,"y":y,
		"w":data.w,"h":data.h,
		"address":address,
		"sed":Random.Int(rnd),
		"color":Utils.GetColor(rnd,data.color),
		"id":ObjectType,
		"doorId":doorid
	}
	if "n" in data:
		obj.n = data.n
		obj.d = data.d
	if "a" in data:
		obj.a = data.a
	if "autoenter" in data:
		obj.autoenter = true
	if "rot" in data:
		obj.dir = 3
	else:
		obj.dir = Random.Range(rnd,0,3)
	if "b" in data: 
		obj.b = data.b
	if "txt" in data: 
		obj.txt = Utils.GetText(rnd,data.txt,obj)
	if "color2" in data:
		obj.color2 = Utils.GetColor(rnd,data.color2)
	if "floor" in data:
		obj.floor = data.floor
		obj.floorcolor = Utils.GetColor(rnd,data.floorcolor)
	return obj


func CreateObject(rnd,objectKey,room_x,room_y,matrix,level,address,idinroom):
	#creation
	var obj = Create(rnd,0,0,objectKey,address,0,idinroom)
	obj.idinroom = idinroom
	obj.valid=true
	obj.doors=[]
	
	var w = obj.h
	var h = obj.w
	if obj.dir == Utils.Direction.right || obj.dir == Utils.Direction.left:
		w = obj.w
		h = obj.h
	
	#position & collision check
	var pos = GetObjectRandomPosition(rnd,room_x,room_y,w,h,matrix,obj.b,obj.dir)
	if obj.b==3: obj.dir=0 #recover direction in nondirectional objects
	
	#return
	if !pos.valid && level < MAX_RECURSION_OBJECT:
		return CreateObject(rnd,objectKey,room_x,room_y,matrix,level+1,address,idinroom)
	else:
		obj.x = pos.x
		obj.y = pos.y
		
	return obj

