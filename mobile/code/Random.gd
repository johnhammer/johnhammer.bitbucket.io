extends Node

var GlobalSeed = 420

func Getsed(sed,a,b):
	return a*123 + sed*456 + b*789

func Create (sed):
	var obj = {}
	# LCG using GCC's constants
	obj.m = 0x80000000 # 2**31
	obj.a = 1103515245
	obj.c = 12345

	if(sed!=null):
		obj.state = sed
	else:
		obj.state = floor(randf() * (obj.m - 1))
	return obj
  
func Multised (sed,a,b):
	return Create(Getsed(sed,a,b))

func Multised2(sed,a,b,c,d):    
	return Multised(sed,Getsed(sed,a,c),Getsed(sed,b,d))

func Int(obj) :
	obj.state = (obj.a * obj.state + obj.c) % obj.m
	return obj.state

func Bool(obj):
	return Range(obj,0,1)

func Float(obj) :
	return float(Int(obj)) / (obj.m - 1)

func Range(obj,start,end) :
	var rangeSize = end+1 - start
	var randomUnder1 = Int(obj) / float(obj.m)
	return start + floor(randomUnder1 * rangeSize)

func FloatRange(obj,start,end) :
	var result = Float(obj)
	return result * (end - start) + start

func Chance(obj,chance):
	var val = Range(obj,1,chance)
	return val == chance

func Choice(obj,array) :
	if(array.size()==0):return null
	return array[Range(obj,0, array.size()-1)]

func WChoice(obj,array) :    
	var val = Range(obj,0,100)
	var sum = 0
	for i in array:
		sum += i.w
		if(val<=sum):
			return i.v
			
func Color(rnd):
	return Color(Random.FloatRange(rnd,0.0,1.0),
		Random.FloatRange(rnd,0.0,1.0),
		Random.FloatRange(rnd,0.0,1.0))
