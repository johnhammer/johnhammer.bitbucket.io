extends Node

const MAX_RECURSION_ROOM = 100

var RoomsDictionary

func GetObjectById(objects,id):
	for object in objects:
		if "idinroom" in  object &&  object.idinroom==id:
			return object
	return null

func CreateRoomObjects(rnd,items):
	var objects = []
	for obj in items:
		var amt = Random.Range(rnd,obj.min,obj.max)
		for i in range(amt):
			objects.push_back(obj.n)
	return objects

func Create(sed,type,address,doors):
	return CreateRoom(RoomsDictionary[type],Random.Create(sed),doors,[],address,0)

func CreateRoom(roomtype,rnd,doors,families,address,level):
	var objects = CreateRoomObjects(rnd,roomtype.i)
	
	var room_x = Random.Range(rnd,roomtype.min,roomtype.max)
	var room_y = Random.Range(rnd,roomtype.min,roomtype.max)
	var matrix = []
	for i in range(room_y+2):
		matrix.push_back([])
		for j in range(room_x+2):
			matrix[i].push_back(0)
	
	var newObjects = []
	var valid = true
	for door in doors:
		#if roomtype.n == "speak":doorkey = "speakdoor"
		newObjects.push_back(MazeFactory.CreateMazeDoor(address,door))
#	for family in families:
#		for human in family:
#			var newObj = ObjectFactory.CreateObject(Random.Create(human.seed),"human",room_x,room_y,matrix,0,null)
#			newObj.seed = human.seed
#			valid = newObj.valid
#			if !valid: break
#			newObjects.push_back(newObj)
	if valid:
		for i in range(objects.size()):
			var newObj = ObjectFactory.CreateObject(rnd,objects[i],room_x,room_y,matrix,0,address,i)
			valid = newObj.valid
			if !valid: break
			newObjects.push_back(newObj)
		
	if !valid && level < MAX_RECURSION_ROOM:
		return CreateRoom(roomtype,rnd,doors,families,address,level+1)
	
	return {
		"valid":valid,
		"objects":newObjects,
		"x":room_x,
		"y":room_y,
		"color":Utils.GetColor(rnd,roomtype.color),
		"matrix":matrix,
		"seed":Random.Int(rnd)
	}


