extends Node

const MAX_RECURSION_TREE = 100

var TreesDictionary

func CreateTreeObject(rnd,MazeType,door,address,i,objects):
	var result = false
	var ObjectType = Random.WChoice(rnd,MazeType.nodes)
	if Utils.IsPosValidNoWalls(door.posx,door.posy,1,1):
		var contact = MazeFactory.GetObjectByPos(objects,door.posx,door.posy)
		if contact !=null:
			if contact.doorId!=door.doorId:
				door.connected = true
		else:
			objects.push_back(ObjectFactory.Create(rnd,door.posx,door.posy,ObjectType,address,door.doorId,i))
			result = true
		if Random.Bool(rnd):
			if Random.Bool(rnd):door.posx += 1
			else:door.posx -= 1
		else:
			if Random.Bool(rnd):door.posy += 1
			else:door.posy -= 1
	else:
		door.posx = door.srcx
		door.posy = door.srcy
	return result

func CreateTreeObjects(rnd,TreeType,maze_x,maze_y,address,doorobjects):
	var objects = []
	var i=0
	var limit=0
	
	var doorId = 0
	for door in doorobjects:
		door.posx = door.srcx
		door.posy = door.srcy
		door.doorId = doorId
		door.connected = false
		doorId+=1
	
	while i<50 && limit<100:
		for door in doorobjects:
			if CreateTreeObject(rnd,TreeType,door,address,i,objects):
				i+=1
		limit+=1
	
	return objects

func Create(sed,type,address,doorOut):
	var TreeType = TreesDictionary[type]
	var rnd =  Random.Create(sed)
	var maze_x = Random.Range(rnd,9,9)
	var maze_y = Random.Range(rnd,9,9)
	var objects

	if doorOut: #create doors and connect them with objects
		var newObj = {
			"x":5,"y":10,
			"srcx":0,"srcy":0,
			"enter":"universe",
			"w":1,"h":1,
			"address":address,
			"color":Color(0,0,1),
			"id":"door",
			"connected":false,
			"autoenter":true,
			"dir":Random.Range(rnd,0,3)
		}
		var doorobjects = []
		doorobjects.push_back(newObj)
		var valid = false
		objects = CreateTreeObjects(rnd,TreeType,maze_x,maze_y,address,doorobjects)
		objects += doorobjects

#	else: #no doors, start  at midpoint
#		var fakeDoorAtMid = {
#			"srcx":5,"srcy":5,
#			"x":15,"y":15, "w":1,"h":1,"address":[],"color":Color(1,0,0),"id":"door","dir":0
#		}
#		objects = CreateMazeObjects(rnd,MazeType,maze_x,maze_y,address,[fakeDoorAtMid])

#	for object in objects: #connect objects
#		object.doors = GetObjectDoors(rnd,objects,object.x,object.y)

	return {
		"x":maze_x,
		"y":maze_y,
		"objects":objects,
		"color":Utils.GetColor(rnd,TreeType.color)
	}
#
#
#
#
#
#
#
#
#
#
#
#
#
##func GetTreeNodeById(tree,id):
##	for node in tree:
##		if node.id == id:
##			return node
##
##func CreateTreeObjects(rnd,items,doors,treefamilies):
##	var objects = {}
##	var id = 0
##	#objects
##	for obj in items:
##		var amt = Random.Range(rnd,obj.min,obj.max)
##		for i in range(amt):
##			var item = obj.duplicate()
##			item.id = id
##			item.seed = Random.Int(rnd)
##			item.lon = Random.Int(rnd)
##			item.lat = Random.Int(rnd)
##			item.taken = false
##			objects[id] = item
##
##			if "pmin" in obj:
##				var families = HumanFactory.CreateFamilies(rnd,obj.pmin,obj.pmax)
##				item.families = families
##				treefamilies.push_back(families)
##
##			id+=1
##	#doors
##	for door in doors:
##		var item = {
##			"n":"0",
##			"door":door,
##			"cmin":0,"cmax":0
##		}
##		item.id = id
##		item.lon = Random.Int(rnd)
##		item.lat = Random.Int(rnd)
##		item.taken = false
##		objects[id] = item
##		id+=1
##	return objects
##
##func GetStartNode(treetype,objects):	
##	for i in objects:
##		if objects[i].n==treetype.init:
##			return objects[i]
##
##func GetTreeObject(rnd,objects):
##	var notTakenObjects = []
##	for i in objects:
##		if !objects[i].taken:
##			notTakenObjects.push_back(objects[i])
##	var choice = Random.Choice(rnd,notTakenObjects)
##	if choice != null:
##		choice.taken = true
##		return choice
##
##func LinkTreeNode(rnd,tree,node,objects):
##	var object = objects[node.id]
##	var childrenAmt = Random.Range(rnd,object.cmin,object.cmax)
##	for i in range(childrenAmt):
##		var type = GetTreeObject(rnd,objects)
##		if type!=null:
##			var newNode = {"id":tree.size(),"c":[],"p":[]}
##			node.c.push_back(newNode.id)
##			newNode.p.push_back(node.id)
##			tree.push_back(newNode)
##	for i in node.c:
##		var newNode = GetTreeNodeById(tree,i)
##		LinkTreeNode(rnd,tree,newNode,objects)
##
##func CreateTree(treetype,rnd,doors,level):
##	var families = []
##	var objects = CreateTreeObjects(rnd,treetype.i,doors,families)
##	var start = GetStartNode(treetype,objects)
##	var tree = [
##		{"id":start.id,"c":[],"p":[]}
##	]
##	var object = objects[start.id]
##	object.taken = true
##
##	LinkTreeNode(rnd,tree,tree[0],objects)
##
##	for i in objects:
##		if !objects[i].taken && level<MAX_RECURSION_TREE:
##			return CreateTree(treetype,rnd,doors,level+1)
##
##	var obj = {"t":tree,"o":objects,"f":families}
##
##	return obj
