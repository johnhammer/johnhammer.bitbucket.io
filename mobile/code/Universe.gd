extends Node

var last_tree = null
var last_treetype = null
var last_nodeid = null
var GodPile = []

func GetPrevTree():
	var pile = []
	GodPile.pop_back()
	while GodPile[GodPile.size()-1].type != "room":
		GodPile.pop_back()
	pile.push_front(GodPile.pop_back())
	if GodPile.size()>0: #if not last node
		while GodPile[GodPile.size()-1].type != "room":
			pile.push_front(GodPile.pop_back())
	return pile

func LoadGodPile(pile,main):
	for node in pile:
		var obj = {"n":node.key,"seed":node.seed,"s":node.type}
		if "doors" in node:obj.doors = node.doors
		IEnter(main,obj,false)
	
func PushGodPile(key,sed,type):
	GodPile.push_back({"key":key,"seed":sed,"type":type})

func Start(main,sed):
	LoadRoom(main,"planet",sed,[],[])
	#LoadTree(main,"universe",sed,[],true)
	
func LoadRoom(main,type,sed,doors,families):
	var rnd = Random.Create(sed)
	var roomtype = RoomFactory.RoomsDictionary[type]
	var room = RoomFactory.CreateRoom(roomtype,rnd,doors,families,null,0)
	PushGodPile(type,sed,"room")
	GodPile[GodPile.size()-1].doors = doors
	main.MapChange(roomtype,room)

func LoadTree(main,type,sed,doors,loadnode):
	var rnd = Random.Create(sed)
	var treetype = TreeFactory.TreesDictionary[type]
	var tree = TreeFactory.CreateTree(treetype,rnd,doors,0)
	last_tree = tree
	last_treetype = treetype
	PushGodPile(type,sed,"tree")
	GodPile[GodPile.size()-1].doors = doors
	
	if loadnode:
		last_nodeid = 1
		var node = last_tree.t[last_nodeid]
		LoadTreeNode(main,last_treetype,last_tree,node)
	
func LoadTreeNode(main,treetype,tree,node):
	var treeobj = tree.o[node.id]
	var node_type = treeobj.n
	var node_doors = node.c + node.p
	var newdoors = GetDoors(tree,node,node_doors)
	var node_sed = treeobj.seed
	
	if treetype.type == "room":
		var families = []
		if "families" in treeobj:families = treeobj.families
		LoadRoom(main,node_type,node_sed,newdoors,families)
	elif treetype.type == "conversation":
		LoadRoom(main,"speak",node_sed,newdoors,[])
	elif treetype.type == "tree":
		LoadTree(main,node_type,node_sed,newdoors,true)

func GetDoors(tree,trnode,doors):
	var curnode = tree.t[trnode.id]
	var curtype = tree.o[curnode.id]
	var result = []
	for door in doors:
		var node = TreeFactory.GetTreeNodeById(tree.t,door)
		var type = tree.o[node.id]
		result.push_back({
			"n":type.n,
			"node":node.id,
			"lat":type.lat,
			"lon":type.lon,
			"curlat":curtype.lat,
			"curlon":curtype.lon,
		})
	return result

func Enter(main,obj): 
	Doors.Enter(main,obj)
	#IEnter(main,obj,true)

func IEnter(main,obj,loadnode):
	var key = obj.n
	var sed = obj.seed
	var doors = []
	var families = []
	if "doors" in obj: doors = obj.doors
	if "families" in obj:families = obj.families
	if obj.s == "room":
		LoadRoom(main,key,sed,doors,families)
	elif obj.s == "tree":
		LoadTree(main,key,sed,doors,loadnode)

func Speak(main,obj):
	LoadTree(main,"conversation",obj.seed,[],true)

func Move(main,obj):
	if obj.data.door.n=="0":
		Exit(main,obj)
	elif obj.data.door.n=="-1":
		ExitAll(main,obj)
	else:
		GodPile.pop_back()
		var node = TreeFactory.GetTreeNodeById(last_tree.t,obj.data.door.node)
		LoadTreeNode(main,last_treetype,last_tree,node)

func GetTreeNodeByDoor(objects,doorKey):
	for object in objects:
		if "door" in objects[object] && objects[object].door.node == doorKey:
			return objects[object]

func ExitAll(main,obj):
	var pile = []
	GodPile.pop_back()
	while GodPile[GodPile.size()-1].type != "room":
		GodPile.pop_back()
	pile.push_front(GodPile.pop_back())
	
	while GodPile.size()>1 && GodPile[GodPile.size()-1].type != "room" :
		pile.push_front(GodPile.pop_back())
	
	LoadGodPile(pile,main)
	

func Exit(main,obj):
	GodPile.pop_back() #tree
	GodPile.pop_back() #main item
	var node = TreeFactory.GetTreeNodeById(last_tree.t,obj.data.door.node)
	var node_type = last_tree.o[node.id]
	var nd = GodPile[GodPile.size()-1]
	var hobj = {"n":nd.key,"seed":nd.seed,"s":nd.type}
	if "doors" in nd:hobj.doors = nd.doors
	IEnter(main,hobj,false)
	
	node = TreeFactory.GetTreeNodeById(last_tree.t,node_type.door.node)

	#custom node load
	var node_type2 = last_tree.o[node.id].n
	var node_doors = node.c + node.p
	var newdoors = GetDoors(last_tree,node,node_doors)
	var node_sed = last_tree.o[node.id].seed
	LoadTree(main,node_type2,node_sed,newdoors,false)
	
	var doorNodeId = GetTreeNodeByDoor(last_tree.o,last_nodeid).id
	node = last_tree.t[last_tree.t[doorNodeId].p[0]] #first (only) parent of door node
	
	last_nodeid = node_type.door.node
	
	LoadTreeNode(main,last_treetype,last_tree,node)

