extends Node
const ObjectResource = preload("res://room/Object.tscn")

var Direction = {
	"up":0,
	"right":1,
	"left":2,
	"down":3
}

var DirectionModes = {
	"normal":0,
	"sticktowall":1,
	"door":2,
	"doornondirectional":3
}

func IsPosValid(x,y,w,h,matrix):
	var valid = true
	for i in range(h):
		for j in range(w):
			valid = matrix[y+i][x+j] == 0
			if !valid: return false
		if !valid: return false
	for i in range(h):
		for j in range(w):
			matrix[y+i][x+j] = 1
	return true
	
func IsPosValidNoWalls(x,y,w,h):
	return x>0 && y>0 && x<10 && y<10

func GetText(rnd,key,obj):
	match key:
		"number":
			return Random.Range(rnd,0,9)
		"door":
			return obj.data.door.n
		"letter":
			return char(Random.Range(rnd,56,84))
		"graffiti":
			return char(Random.Range(rnd,56,84))

func GetColor(rnd,key):
	match key:
		"rnd":
			return Random.Color(rnd)
		"wood":
			return Color.from_hsv(0.1, Random.FloatRange(rnd,0.2,0.8), Random.FloatRange(rnd,0.2,0.8))
		"pastel":
			return Color.from_hsv(Random.Float(rnd), 0.5, 0.8)
		"white":
			return Color.from_hsv(Random.Float(rnd), 0.1, 0.9)
		"metal":
			return Color.from_hsv(1, 0, Random.Float(rnd))
		"black":
			return Color.from_hsv(Random.Float(rnd), 0.2, 0.2)
		"glass":
			return Color.from_hsv(1,1,1,0.8)
		"red":
			return Color(1,0,0)
		"deepblack":
			return Color(0,0,0)
		"brick":
			return Color.from_hsv(Random.FloatRange(rnd,0,0.08), Random.FloatRange(rnd,0.4,0.8), Random.FloatRange(rnd,0.4,0.8))
		"grass":
			return Color.from_hsv(Random.FloatRange(rnd,0.15,0.40), Random.FloatRange(rnd,0.6,1.0), Random.FloatRange(rnd,0.2,0.5))
		"forest":
			return Color.from_hsv(Random.FloatRange(rnd,0.15,0.40), 0.5, Random.FloatRange(rnd,0.6,0.7))
		"floor":
			return Color.from_hsv(Random.Float(rnd), 0.2, 0.9)
		"sea":
			return Color.from_hsv(Random.FloatRange(rnd,0.6,0.7), Random.FloatRange(rnd,0.7,0.9), Random.FloatRange(rnd,0.7,0.9))
		"star":
			if Random.Bool(rnd):#red-yellow
				return Color.from_hsv(Random.FloatRange(rnd,0.0,0.13), Random.FloatRange(rnd,0.8,1),1.0)
			else:#blue
				return Color.from_hsv(Random.FloatRange(rnd,0.47,0.62), Random.FloatRange(rnd,0,0.7), 1.0)

func filter(list: Array, matches_criteria: FuncRef) -> Array:
	var filtered: Array = []
	for element in list:
		if matches_criteria.call_func(element):
			filtered.append(element)
	return filtered

func load_json(name):
	var res = null
	var file = File.new()
	file.open("res://data/"+name+".json", file.READ)
	var text = file.get_as_text()
	var result_json = JSON.parse(text)
	if result_json.error == OK:  # If parse OK
		res = result_json.result
	else:  # If parse has errors
		print("Error: ", result_json.error)
		print("Error Line: ", result_json.error_line)
		print("Error String: ", result_json.error_string)
	file.close()
	return res

func ClearObjects(container):
	for n in container.get_children():
		container.remove_child(n)
		n.queue_free()

func CreateObject(objectKey,x,y,dir,color,container,parent):
	var obj = ObjectFactory.ObjectsDictionary[objectKey].duplicate()
	obj.x = x
	obj.y = y
	obj.dir = dir
	obj.id = objectKey
	obj.color = color
	var item = ObjectResource.instance()
	container.add_child(item)
	item.Init(obj,parent)
	obj.node = item
	return item
