extends Node

var container
var selected
var board = []
var rnd

func Init(room,_container):
	rnd = Random.Create(Random.GlobalSeed)
	container = _container
	var size = 8
	board = CreateBoard(size)
	board[2][Random.Range(rnd,0,size-1)] = 3
	board[2][Random.Range(rnd,0,size-1)] = 3
	board[3][Random.Range(rnd,0,size-1)] = 3
	board[3][Random.Range(rnd,0,size-1)] = 3
	board[4][Random.Range(rnd,0,size-1)] = 3
	board[4][Random.Range(rnd,0,size-1)] = 3
	board[5][Random.Range(rnd,0,size-1)] = 3
	board[5][Random.Range(rnd,0,size-1)] = 3
	
	PrintBoard(board)

func CreateBoard(size):
	var board = []
	for x in range(size):
		board.push_back([])
		for y in range(size):
			if x==0: board[x].push_back(2)
			elif x==size-1: board[x].push_back(1)
			else: board[x].push_back(0)
	return board

func PrintBoard(board):
	Utils.ClearObjects(container)
	for x in range(board.size()):
		for y in range(board[x].size()):
			var color = Color(0,0,0)
			if x%2==0 && y%2!=0 || x%2!=0 && y%2==0:
				color = Color(1,1,1)
			Utils.CreateObject("nothing",y+1,x+1,0,color,container,self)
	for x in range(board.size()):
		for y in range(board[x].size()):
			var cell = board[x][y]
			if cell==1:
				Utils.CreateObject("unit",y+1,x+1,1,Color(1,0,0),container,self)
			elif cell==2:
				Utils.CreateObject("unit",y+1,x+1,2,Color(0,1,0),container,self)
			elif cell==3:
				Utils.CreateObject("unit",y+1,x+1,2,Color(0.5,0.5,0.5),container,self)

func MoveIsValid(board,newmove):
	var moves = GetPieceMoves(board,{"x":newmove.sx,"y":newmove.sy})
	for move in moves:
		if move.dx==newmove.dx && move.dy==newmove.dy:
			return true
	return false

func Select(element):
	PrintBoard(board)
	if element.obj.id == "nothing":
		if selected!=null:
			var move = {
				"s":0,
				"dx":element.obj.y-1,
				"dy":element.obj.x-1,
				"sx":selected.y-1,
				"sy":selected.x-1
			}
			if MoveIsValid(board,move):
				RunMove(board,move)
				var aimove = AiPlay(board)
				PrintBoard(board)
				Utils.CreateObject("nothing",aimove.sy+1,aimove.sx+1,0,Color(0,1,0,0.5),container,self)
				selected = null
	elif element.obj.dir == 1:
		selected = element.obj
		#paint moves
		var moves = GetPieceMoves(board,{"x":selected.y-1,"y":selected.x-1})
		for move in moves:
			Utils.CreateObject("nothing",move.dy+1,move.dx+1,0,Color(1,0,0,0.5),container,self)

#AI

func AiPlay(board):
	var move 
	#var result = GetAllMoves(board,2)
	var result = minimax(board, 2, -9999, 9999, 2).m
	if result!=null:
		move = Random.Choice(rnd,result)
		RunMove(board, move)
	return move

func minimax(board, depth, alpha, beta, player):
	if depth == 0:# or board.is_winner() or board.is_board_full():
		return {"m":null, "s":GetBoardScore(board,player)}

	var children = GetAllMoves(board,player)
	if children.size()==0:
		return {"m":null, "s":GetBoardScore(board,player)}
	
	var best_move = [children[0]]
	
	if player==2:
		var max_eval = -9999        
		for child in children:
			var board_copy = GetBoardCopy(board)
			RunMove(board_copy,child)
			var current_eval = minimax(board_copy, depth - 1, alpha, beta, 1).s
			if current_eval > max_eval:
				max_eval = current_eval
				best_move = [child]
			elif current_eval == max_eval:
				best_move.push_back(child)
			alpha = max(alpha, current_eval)
			if beta <= alpha:
				break
		return {"m":best_move, "s":max_eval}
	else:
		var min_eval = 9999
		for child in children:
			var board_copy = GetBoardCopy(board)
			RunMove(board_copy,child)
			var current_eval = minimax(board_copy, depth - 1, alpha, beta, 2).s
			if current_eval < min_eval:
				min_eval = current_eval
				best_move = [child]
			elif current_eval == min_eval:
				best_move.push_back(child)
			beta = min(beta, current_eval)
			if beta <= alpha:
				break
		return {"m":best_move, "s":min_eval}

func GetAllMoves(board,player):
	var moves = []
	for x in range(board.size()):
		for y in range(board[x].size()):
			var cell = board[x][y]
			if cell==player:
				var newmoves = GetPieceMoves(board,{"x":x,"y":y})
				moves += newmoves
	return moves

func GetMove(pos,piece):
	return {"s":0,"dx":pos.x,"dy":pos.y,"sx":piece.x,"sy":piece.y}

func GetPieceMoves(board,piece):
	var pos
	var moves = []
	var sizex = board.size()
	var sizey = board[0].size()
	#8
	pos = piece.duplicate()
	pos.y -= 1
	while pos.y>=0:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.y -= 1
		else:
			break
	#2
	pos = piece.duplicate()
	pos.y += 1
	while pos.y<sizey:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.y += 1
		else:
			break
	#4
	pos = piece.duplicate()
	pos.x -= 1
	while pos.x>=0:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.x -= 1
		else:
			break
	#6
	pos = piece.duplicate()
	pos.x += 1
	while pos.x<sizex:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.x += 1
		else:
			break
	#diagonals
	#9
	pos = piece.duplicate()
	pos.x += 1
	pos.y += 1
	while pos.x<sizex && pos.y<sizex:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.x += 1
			pos.y += 1
		else:
			break
	#7
	pos = piece.duplicate()
	pos.x -= 1
	pos.y += 1
	while pos.x>=0 && pos.y<sizex:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.x -= 1
			pos.y += 1
		else:
			break
	#3
	pos = piece.duplicate()
	pos.x += 1
	pos.y -= 1
	while pos.x<sizex && pos.y>=0:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.x += 1
			pos.y -= 1
		else:
			break
	#1
	pos = piece.duplicate()
	pos.x -= 1
	pos.y -= 1
	while pos.x>=0 && pos.y>=0:
		if board[pos.x][pos.y] == 0:
			moves.push_back(GetMove(pos,piece))
			pos.x -= 1
			pos.y -= 1
		else:
			break
			
	return moves

func GetBoardScore(board,player):
	var score = 0
	for x in range(board.size()):
		for y in range(board[x].size()):
			var cell = board[x][y]
			if cell==player:
				score+=1
			elif cell!=0 && cell!=3:
				score-=1
	return score

func GetBoardCopy(board):
	board = board.duplicate()
	for x in range(board.size()):
		board[x]=board[x].duplicate()
	return board

func GetKills(board,ipos,player):
	var kills = []
	var newkills = []
	var pos
	var sizex = board.size()
	var sizey = board[0].size()
	#8
	pos = ipos.duplicate()
	pos.y -= 1
	newkills=[]
	while pos.y>=0:
		if board[pos.x][pos.y] == 0:
			break
		elif board[pos.x][pos.y] == player || board[pos.x][pos.y] == 3:
			kills+= newkills
			break
		else:
			newkills.push_back(pos.duplicate())
			pos.y -= 1
	#2
	pos = ipos.duplicate()
	pos.y += 1
	newkills=[]
	while pos.y<sizey:
		if board[pos.x][pos.y] == 0:
			break
		elif board[pos.x][pos.y] == player || board[pos.x][pos.y] == 3:
			kills+= newkills
			break
		else:
			newkills.push_back(pos.duplicate())
			pos.y += 1
	#4
	pos = ipos.duplicate()
	pos.x -= 1
	newkills=[]
	while pos.x>=0:
		if board[pos.x][pos.y] == 0:
			break
		elif board[pos.x][pos.y] == player || board[pos.x][pos.y] == 3:
			kills+= newkills
			break
		else:
			newkills.push_back(pos.duplicate())
			pos.x -= 1
	#6
	pos = ipos.duplicate()
	pos.x += 1
	newkills=[]
	while pos.x<sizex:
		if board[pos.x][pos.y] == 0:
			break
		elif board[pos.x][pos.y] == player || board[pos.x][pos.y] == 3:
			kills+= newkills
			break
		else:
			newkills.push_back(pos.duplicate())
			pos.x += 1
	return kills

func RunMove(board,move):
	var player = board[move.sx][move.sy]
	board[move.dx][move.dy] = player
	board[move.sx][move.sy] = 0
	var kills = GetKills(board,{"x":move.dx,"y":move.dy},player)
	for kill in kills:
		board[kill.x][kill.y] = 0

func GetTopMoves(moves):
	var topscore = -9999
	var bestmoves = []
	for move in moves:
		if move.s>topscore:
			topscore = move.s
			bestmoves = []
			bestmoves.push_back(move)
		elif move.s==topscore:
			bestmoves.push_back(move)
	return bestmoves

func GetScoredMoves(board,player):
	var moves = GetAllMoves(board,player)
	for move in moves:
		var newboard = GetBoardCopy(board)
		RunMove(newboard,move)
		move.s = GetBoardScore(newboard,player)
	return moves
	


