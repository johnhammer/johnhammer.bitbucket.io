extends Node
const ObjectResource = preload("res://room/Object.tscn")

var container
var rnd

var players = [
	{"x":3,"y":8,"c":[],"bet":0,"cur":0,"all":10,"me":true,"skip":false},
	{"x":3,"y":2,"c":[],"bet":0,"cur":0,"all":10,"me":false,"skip":false}
]
var GlobalBet = 1

func Init(room,_container):
	container = _container
	rnd = Random.Create(42)
	for player in players:
		DrawPlayer(player)
	GiveAllCard()
	GiveAllBet()

func GetRandomCard(rnd):
	return Random.Range(rnd,1,9)

func GiveAllCard():
	for player in players:
		var card = GetRandomCard(rnd)
		player.c.push_back(card)
		DrawCard(player,card,players[0].c.size())

func GiveAllBet():
	GlobalBet = 1
	for player in players:
		player.skip = false
		player.bet += player.cur
		player.cur = 1
		RepaintPlayer(player)

func PlayAI():
	var skip = true
	for player in players:
		if player.me:continue
		_Play(player)
		if !player.skip: skip=false
	if skip:
		GiveAllCard()
		GiveAllBet()

#actions
func Play():
	_Play(players[0])
	PlayAI()

func Double():
	_Double(players[0])
	PlayAI()

func _Play(player):
	if player.cur == GlobalBet:
		player.skip = true
	else:
		player.cur = GlobalBet
		RepaintPlayer(player)

func _Double(player):
	GlobalBet = GlobalBet*2
	player.cur = GlobalBet
	RepaintPlayer(player)

func Retire():
	pass

func Abandon():
	pass

#drawing
func DrawCard(player,value,cardnum):
	var obj = ObjectFactory.ObjectsDictionary["card"].duplicate()
	obj.x = player.x+cardnum+1
	obj.y = player.y
	obj.dir = 0
	obj.id = "card"
	if player.me: obj.txt = value
	obj.color = Utils.GetColor(Random.Create(Random.GlobalSeed),obj.color)
	var item = ObjectResource.instance()
	container.add_child(item)
	item.Init(obj,self)
	obj.node = item

func GetPlayerText(player):
	return String(player.cur)+"/"+String(player.bet)+"/"+String(player.all)

func RepaintPlayer(player):
	player.node.UpdateText(GetPlayerText(player))

func DrawPlayer(player):
	var obj = ObjectFactory.ObjectsDictionary["player"].duplicate()
	obj.x = player.x
	obj.y = player.y
	obj.dir = 0
	obj.id = "player"
	obj.txt = GetPlayerText(player)
	obj.color = Utils.GetColor(Random.Create(Random.GlobalSeed),obj.color)
	var item = ObjectResource.instance()
	container.add_child(item)
	item.Init(obj,self)
	obj.node = item
	player.node = item

func Select(node):
	pass
