extends Node

var cablelink1
var cablelink2
var cables = []
var tableOfTruth = []
var mode = 0 # 0 inactive, 1 c1, 2 c2, 3 pliers
var state = 1 # 0 not ok, 1 ok, 2 burned

func CableLink1(item):
	mode = 2
	cablelink1=item

func CableLink2(item):
	mode = 0
	cablelink2=item
	AddCable(cablelink1,cablelink2)
	cablelink1 = null
	cablelink2 = null

func AddCable(c1,c2):
	cables.push_back({"c1":c1,"c2":c2})

func Pliers(node):
	var filtered: Array = []
	for element in cables:
		if element.c1 != node:
			filtered.append(element)
	cables = filtered

#tot

func RowOfTruth(rnd,objects):
	var row = []
	for object in objects:
		if object.n=="in"||object.n=="out":
			row.push_back({"o":object,"v":Random.Bool(rnd)})
	return row

func TableOfTruth(rnd,objects,listNode):
	var difficulty = 2
	for i in range(difficulty):
		tableOfTruth.push_back(RowOfTruth(rnd,objects))
	
	#draw
	for rowOfTruth in tableOfTruth:
		for item in rowOfTruth:
			listNode.add_icon_item(load("res://res/objects/"+item.o.n+".png"))
			var idx = listNode.get_item_count()-1
			listNode.set_item_text(idx,String(item.o.txt))
			if item.v:
				listNode.set_item_icon_modulate(idx,Color(0,0,1))
			else:
				listNode.set_item_icon_modulate(idx,Color(0,1,0))

#activation

func Burn(node):
	node.CircuitActivation(2)
	ClearCircuit()
	state = 2

func Not(node,value):
	if node.CircuitValue == null:
		if value == 0: value=1
		else: value=0
		return value
	else:
		Burn(node)
		return 2

func And(node, value):
	if node.CircuitValue == null:
		node.CircuitActivation(value+10)
		return 2
	else:
		var val = node.CircuitValue-10
		if val<0: #more than 2 values, burn
			Burn(node)
			return 2 
		if(value==1 && val==1): value = 1
		else: value = 0
		return value

func Or(node,value):
	if node.CircuitValue == null:
		node.CircuitActivation(value+10)
		return 2
	else:
		var val = node.CircuitValue-10
		if val<0: #more than 2 values, burn
			Burn(node)
			return 2 
		if(value==1 || val==1): value = 1
		else: value = 0
		return value

func Signal(object,value):
	var val = value
	if object.n=="not":
		val = Not(object.node,value)
	elif object.n=="and":
		val = And(object.node,value)
	elif object.n=="or":
		val = Or(object.node,value)
		
	if val!=2:
		object.node.CircuitActivation(val)
		for element in cables:
			if element.c1 == object.node:
				Signal(element.c2.obj,val)

func ClearCircuit():
	state = 1
	for cable in cables:
		cable.c1.CircuitValue = null
		cable.c2.CircuitValue = null

func StartCircuit():
	var state=1
	for rowOfTruth in tableOfTruth:
		state = StartCircuitRow(rowOfTruth)
		if state !=1:break
	if state==0: print("off")
	elif state==1: print("on")
	elif state==2: print("burn")

func StartCircuitRow(rowOfTruth):
	ClearCircuit()
	for item in rowOfTruth:
		if item.o.n=="in":
			Signal(item.o,item.v)
	#validation
	if state!=2:
		for item in rowOfTruth:
			if item.o.n=="out":
				if item.v!=item.o.node.CircuitValue:
					state = 0
					break
	return state
	
