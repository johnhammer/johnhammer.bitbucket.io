extends Node
const ObjectResource = preload("res://room/Object.tscn")

var container

var items = []

func Init(room,_container):
	container = _container

	Utils.CreateObject("fightcolumn",1,2,Utils.Direction.down,Color(1,1,0),container,self)
	Utils.CreateObject("fightcolumn",2,2,Utils.Direction.down,Color(1,0,0),container,self)
	Utils.CreateObject("fightcolumn",3,2,Utils.Direction.down,Color(1,1,0),container,self)
	
	var item = Utils.CreateObject("punch",8,8,Utils.Direction.down,Color(0,0,0),container,self)
	items.push_back(item)
	
func Select(node):
	pass

func _process(delta):
	for item in items:
		item.position.x -= 1
