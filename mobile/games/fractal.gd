extends ColorRect

var sscale =0.371

func _init():
	var rnd = Random.Create(Random.GlobalSeed)
	get_material().set_shader_param("cx",Vector3(Random.Float(rnd),Random.Float(rnd),Random.Float(rnd)))
	get_material().set_shader_param("cy",Vector3(Random.Float(rnd),Random.Float(rnd),Random.Float(rnd)))
	get_material().set_shader_param("cr",Vector3(Random.Float(rnd),Random.Float(rnd),Random.Float(rnd)))

func _input(event):
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_WHEEL_UP :
			sscale += 0.01
		if event.button_index == BUTTON_WHEEL_DOWN:
			sscale -= .01
		get_material().set_shader_param("minRad",sscale)
