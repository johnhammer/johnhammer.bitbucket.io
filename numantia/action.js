class Actions {
    constructor(game){
        this.game = game;
    }
    Attack(item,objective){
        if(item.Attack(objective)){
            objective.Destroy();
            item.Move(objective.x,objective.y);
            this.game.Draw();
        }
        else{
            item.Destroy();
            this.game.Draw();
        }  
    }
    Move(item,x,y){
        item.Move(x,y);
        this.game.Draw();
    }
}