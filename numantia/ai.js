class Ai extends Player {
    Minimax(newGrid, depth, player) {
        const gameState = isGameOver(newGrid);
        //if the game is not over, evalute best move for computer
        if(gameState === false) {
            const values = [];
    
            for(var i = 0; i < 3; i++) {
                for(var j = 0; j < 3; j++) {
                    const gridCopy = _.cloneDeep(newGrid);
                    //if that spot is taken, skip to next loop
                    if(gridCopy[i][j] !== ' ') continue;
                    //if spot is player, evaluate
                    gridCopy[i][j] = player;
                    //need clarification
                    const value = this.Minimax(gridCopy, depth+1, (player == PLAYER_TOKEN) ? COMPUTER_TOKEN : PLAYER_TOKEN);
                    values.push(value);
                }
            }
            //need clarification for computer turn
            if(player === COMPUTER_TOKEN) {
                const max = _.maxBy(value, (v) => {
                    return v.cost;
                });
                if(depth === 0) {
                    return max.cell;
                }
                else {
                    return max.cost;
                }
            }
            //need clarification for user turn
            else {
                const min = _.minBy(value, (v) => {
                    return v.cost;
                });
                if(depth === 0) {
                    return v.cell;
                }
                else {
                    return v.cost;
                }
            }
        }
        //if game state is null return 0
        else if (gameState === null) {
            return 0;
        }
        //if game state is player return negative
        else if(gameState === PLAYER_TOKEN) {
            return depth - 10;
        }
        //if game state is computer return positive
        else if(gameState === COMPUTER_TOKEN) {
            return 10 - depth;
        }
    }
}