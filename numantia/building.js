class Building extends Item{
    constructor(x,y,game,player){
        super(x,y,game,player);
        this.turnsToGenerate = 3;
        this.maxTurnsToGenerate = 3;
    }
    CanAttack(objective){
        return false;
    }
    CanMove(x,y){
        return false;
    }
    Turn(){
        this.turnsToGenerate--;
        if(this.turnsToGenerate<=0){
            this.Generate();
            this.turnsToGenerate=this.maxTurnsToGenerate;
        }
    }
    Generate(){
        
    }
}