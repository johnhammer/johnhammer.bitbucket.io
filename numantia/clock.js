class Clock{
    players
    turn
    me
    Start(players,me){
        this.players = players;
        this.turn = 0;
        this.me = me;
        this.Turn();       
    }
    CanIPlay(){
        return this.me == this.turn;
    }
    IsMe(player){
        return this.me == player;
    }
    Turn(){
        this.players[this.turn].Turn();
        current_player.innerHTML = this.turn;
    }
    Tick(){
        this.turn++;
        if(this.turn>=this.players.length){
            this.turn = 0;
        }
        this.Turn();
    }
    PassTurn(){
        if(this.CanIPlay()){
            this.Tick();
        }
    }
}