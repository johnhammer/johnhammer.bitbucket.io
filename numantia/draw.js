class Draw {
    ctx = null    
	constructor() {
        this.canvas = document.createElement('canvas');
        this.canvas.width = document.body.clientWidth;
        this.canvas.height = document.body.clientHeight;
        document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext("2d");
	}
	Rectangle(x,y,color) {		
		this.ctx.beginPath();
		this.ctx.rect(x*CONST.gridXsize,y*CONST.gridYsize, CONST.gridXsize,CONST.gridYsize);
		this.ctx.fillStyle = color;
		this.ctx.fill();
	}
	RectangleSmall(x,y,color) {
		this.ctx.beginPath();
		this.ctx.rect(x*CONST.gridXsize+CONST.gridXsize/4,y*CONST.gridYsize+CONST.gridYsize/4, CONST.gridXsize/2,CONST.gridYsize/2);
		this.ctx.fillStyle = color;
		this.ctx.fill();
	}
	Mask(x,y,color) {
		this.ctx.beginPath();
		this.ctx.rect(x*CONST.gridXsize,y*CONST.gridYsize, CONST.gridXsize,CONST.gridYsize);
		this.ctx.strokeStyle = color;
		this.ctx.stroke();
	}
	Text(x,y,text) {
		this.ctx.beginPath();
		this.ctx.fillStyle = "black";
		this.ctx.font = "20px Arial";
		this.ctx.fillText(text, x*CONST.gridXsize,y*CONST.gridYsize+CONST.gridYsize);
	}
}