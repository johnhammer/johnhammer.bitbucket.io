class Frontend{
    static Modal(content){
        mainbody.innerHTML += `
            <div id="current_modal" class="modal">
                <div class="modal-content">
                    ${content}
                </div>
            </div>            
        `;
        window.onclick = function(event) {
            if (event.target == current_modal) {
                Frontend.CloseModal();
            }
        };
    }
    static CloseModal(){
        current_modal.outerHTML = '';
        window.onclick = null;
    }
}
