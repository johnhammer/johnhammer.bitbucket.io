var CONST = {
    gridXsize : 64,
    gridYsize : 64
}

var LIB = {
    dist(x1,y1,x2,y2){ 
        if(!x2) x2=0; 
        if(!y2) y2=0;
        return Math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)); 
    },
    remove(array,item){
        return array.filter(x => x !== item);
    }
}

class Game{
    constructor(mapX,mapY){
        this.players = [];        
        this.items = [];
        this.selection = null;

        this.draw = new Draw();
        this.interaction = new Interaction(this,this.draw.canvas);                
        this.actions = new Actions(this);
        this.clock = new Clock();

        this.mapX = mapX;
        this.mapY = mapY;
        this.map = new Map(mapX,mapY,this.draw);        
    }
    RemoveItem(item){
        this.players[item.player].items = LIB.remove(this.players[item.player].items,item);
        this.items = LIB.remove(this.items,item);
    }
    AddItem(item){
        this.items.push(item);
        this.players[item.player].AddItem(item);
    }
    Start(){     
        this.players.push(new Player());
        this.players.push(new Player());

        this.AddItem(new Building(0,1,this,0));
        this.AddItem(new Building(4,1,this,1));
        this.AddItem(new Unit(1,1,this,0,{ moves : 1 }));
        this.AddItem(new Unit(3,1,this,1,{ moves : 1 }));

        this.clock.Start(this.players,0);

        this.Draw();
        this.interaction.Start();
    }
    Draw(){
        this.map.Draw();
        for(var i=0;i<this.items.length;i++){
            this.items[i].Draw();
        }
    }    
    LeftClick(x,y){
        var pos = {x:x,y:y};
        var item = this.items.find(i=>i.IsInPos(pos));
        if(item){
            if(this.clock.CanIPlay() && this.clock.IsMe(item.player)){
                item.Select();
            }
        }
        else{
            this.UnsetSelection();
        }
    }
    RightClick(x,y){
        var pos = {x:x,y:y};
        var item = this.items.find(i=>i.IsInPos(pos));
        if(item){
            if(this.clock.CanIPlay() && this.selection && !this.clock.IsMe(item.player)){
                if(this.selection.CanAttack(item)){
                    this.actions.Attack(this.selection,item);
                }
            }
            else{
                Frontend.Modal("Item clicked");
            }  
        }
        else{
            if(this.clock.CanIPlay() && this.selection){
                if(this.selection.CanMove(x,y)){
                    this.actions.Move(this.selection,x,y);
                }
            }
            else{
                Frontend.Modal("Nothing here");
            }            
        }
    }
    UnsetSelection(){
        if(this.selection){
            this.Draw();
            this.selection.Unselect();
        }
        this.selection = null;
    }
    SetSelection(item){
        this.UnsetSelection();
        this.selection = item;
    }
}

game = new Game(5,3);
game.Start();