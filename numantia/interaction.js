class Interaction {
    constructor (game,canvas){
        this.canvas = canvas;
        this.game = game;		
    }
    Start(){
        this.canvas.addEventListener( 'mousedown', e=>this.OnMouseDown(e), false );
        this.canvas.addEventListener( 'oncontext', e=>this.OnMouseDown(e), false );
    }
	OnMouseDown (event){
        var left, right;
        left = 0;
        right = 2;

		var rect = this.canvas.getBoundingClientRect();
	    var x = Math.floor((event.clientX - rect.left)/CONST.gridXsize);
	    var y = Math.floor((event.clientY - rect.top)/CONST.gridYsize);
        
        if(event.button === left){
            this.game.LeftClick(x,y);
        }
        else if(event.button === right){
            this.game.RightClick(x,y);
        }
    }
    
}