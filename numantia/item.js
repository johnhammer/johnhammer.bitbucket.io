class Item {
    constructor(x,y,game,player){
        this.game = game;
        this.x = x;
        this.y = y;
        this.player = player;
    }
    Draw(){
        this.game.draw.RectangleSmall(this.x,this.y,"#00ffff");
    }
    IsInPos(pos){
        return this.x==pos.x && this.y==pos.y;
    }
    Select(){
        this.game.SetSelection(this);
        this.game.draw.Mask(this.x,this.y,"#00ff00");
    }
    Unselect(){}
    CanAttack(objective){
        return false;
    }
    CanMove(x,y){
        return true;
    }
    Move(nx,ny){}
    Turn(){}
    Destroy(){
        this.game.RemoveItem(this);
    }
    Attack(objective){
        return false;
    }
}