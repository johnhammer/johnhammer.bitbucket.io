class Map {
    constructor(width,height,draw){
        this.width = width;
        this.height = height;
        this.draw = draw;
    }
    Draw(){
        for(var j=0;j<this.width;j++){
            for(var i=0;i<this.height;i++){
                this.draw.Rectangle(j,i,"#0000ff");
            }
        }
    }    
}