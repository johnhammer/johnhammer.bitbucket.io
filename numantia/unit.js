class Unit extends Item{
    constructor(x,y,game,player,params){
        super(x,y,game,player);
        this.maxmoves = params.moves;
        this.moves = params.moves;
    }
    _CanInteract(x,y){
        if(x>=this.game.mapX || x<0 || y>=this.game.mapY || y<0){
            return false;
        }
        var dist = LIB.dist(x,y,this.x,this.y);
        return dist<=this.moves;
    }
    CanAttack(objective){
        return this._CanInteract(objective.x,objective.y);
    }
    CanMove(x,y){
        return this._CanInteract(x,y);
    }
    Move(nx,ny){
        this.x = nx;
        this.y = ny;
        this.moves--;
    }
    Turn(){
        this.moves = this.maxmoves;
    }
    Draw(){
        this.game.draw.RectangleSmall(this.x,this.y,"#A0000f");
        this.game.draw.Text(this.x,this.y,this.moves);
    }
}