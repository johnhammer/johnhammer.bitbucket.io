/* SPANISH */

/*
Instead of learning that evil must be fought, they learned that fighting is evil.﻿
*/

SPANISH = {
//main menu	
'Single player':'Un Jugador',
'Multiplayer':'Online',
'Tutorial':'Tutorial',
'Options':'Opciones',
'Language':'Idioma',
'Not available yet':'Opcion no disponible',
'Pick a color:':'Elige color:',
		
//factions
'Empire':'El Gran Imperio',		
'Union':'Union Sovietica Universal',
'League':'Confederacion de Jonia',
'Califate':'El Nuevo Califato',	
'Republic':'Tercera Republica Romana',
'Dinasty':'Dinastia Sikh',
'Serpent':'Imperio de la Serpiente',
'Crown':'Imperio Austropruso-Espanol',
'Corporation':'Corporacion Universal',
'Confederation':'Federacion Anarquista',		
'States':'EUU',							
'Colonies':'Imperio Francobritanico',		
'Aliance':'Alianza de Isandlwana',		
'Khanate':'Khanato celestial',			
'Earldoms':'Condados del norte',		
'Kingdom':'Reino Eterno de Egipto',		

//world
'mine' : 'mina',	
'plant' : 'planta',	
'house' : 'casa',	
'recycler' : 'reciclador',
'factory' : 'fabrica',
'lab' : 'laboratorio',	
'hangar' : 'hangar',
'bunker' : 'bunker',

//map
'Planet':'Planeta',
'Dust cloud':'Nube de polvo',
'Gas cloud':'Nube de gas',
'Asteroids':'Asteroides',
	
//ships 1
'Fighter drone':'Dron cazador',
'Bomb':'Bomba',
'Decoy drone':'Dron senuelo',
'Missile':'Misil',
'Mother drone':'Dron nodriza',
'Explorer':'Explorador ligero',
'Probe':'Sonda',
'EMP pulse':'Pulso EMP',
'Cargo drone':'Dron de carga',
	
//ship skills	

//ship parts
'Weapon':'Arma',
'Ammo':'Munición',
'Shield':'Escudo',
'Engine':'Motor',
'Fuel':'Combustible',
'Habitat':'Habitat',
'Radio':'Radio',
'Computer':'Ordenador',

//ship props
'FirePower':'Potencia de fuego',
'FireSpeed':'Cadencia de fuego',
'Precision':'Precisión',
'ElectricAttack':'Ataque electrónico',
'Armor':'Defensa',
'Tripulation':'Tripulación',
'Evasion':'Evasión',
'Camouflage':'Camuflaje',
'Speed':'Velocidad',
'Perception':'Percepción',
'Autonomy':'Autonomia',
'CargoCapacity':'Capacidad de carga',
	
}

ENGLISH_LONG = {
//main menu	
'Single player':'Play alone against an AI controlled opponent.',
'Multiplayer':'Online',
'Tutorial':'Learn how to play.',
'Options':'Opciones',
'Language':'Idioma',	
	
//faction history	
'History_Empire':'Las fuerzas del eje ganan la segunda guerra mundial, gracias a la neutralidad de Estados Unidos y una ataque relampago a la capital de la URSS.#Poco despues de ganar la guerra el eje se hace con los paises restantes, e inicia una rapida expansion por el sistema solar.#Tras varios siglos de investigacion eugenica surge una nueva raza de superhumanos, mas inteligentes y adaptados a la vida en el espacio.',		
'History_Union':'Tras ganar la segunda guerra mundial, los aliados traicionan a la URSS y continuan el avance hacia el este mas alla de Berlin.#Contando con China como un poderoso aliado y con varias revoluciones alzandose por todo el mundo, los Soviets terminan por gobernar toda la Tierra.#Empezando con la construccion de ciudades flotantes en Venus, la bandera del el hoz y el martillo termina por ondear en toda la galaxia.',
'History_League':'Los grandes descubrimientos cientificos en Jonia no quedan olvidados en el tiempo, sino que son utilizados para desarrollar una revolucion industrial en la epoca clasica.#Con el apoyo del reino de Macedonia y su enorme superioridad tecnologica y cientifica, las ciudades Jonicas forman una Liga y capturan facilmente todas las civilizaciones antiguas.#El viaje estelar comienza poco despues de conquistar el mundo, usando grandes naves arca multigeneracionales.',
'History_Califate':'El Nuevo Califato',	
'History_Republic':'Tercera Republica Romana',
'History_Dinasty':'Dinastia Sikh',
'History_Serpent':'Imperio de la Serpiente',
'History_Crown':'Imperio Austropruso-Espanol',
'History_Corporation':'Corporacion Universal',
'History_Confederation':'Federacion Anarquista',		
'History_States':'EUU',							
'History_Colonies':'Imperio Francobritanico',		
'History_Aliance':'Alianza de Isandlwana',		
'History_Khanate':'Khanato celestial',			
'History_Earldoms':'Condados del norte',		
'History_Kingdom':'Reino Eterno de Egipto',	

//world
'mine' : 'Extracts pure matter from the world.',	
'plant' : 'Produces energy consuming matter.',	
'house' : 'Habitat for 32 people, requires matter.',	
'recycler' : 'Refines debris into pure matter.',
'factory' : 'Increases ship production speed.',
'lab' : 'Increases investigation speed.',	
'hangar' : 'Allows creation of one fleet.',
'bunker' : 'Adds defense power in case of invasion.',

// ship parts
'Weapon': 'Weapons add rate of fire to the ship.',
'Ammo': 'The more space for ammunition you have, the more powerful your weapons will be.',
'Shield': 'The shields of the ship determine its resistance to enemy attacks.',
'Engine': 'The more engines you have, the faster your ship will be.',
'Fuel': 'The more fuel tanks you have, the longer your ship can be operational.',
'Habitat': 'Habitats add human crew to the ship. Humans are useful for many things, especially they add intelligence to your ship.',
'Radio': 'The more radios you have, the better you can perceive your surroundings.',
'Computer': 'Computers can be used to hack enemy ships, and they also offer intelligence to your ship.',

// ship props
'FirePower': 'Firepower determines how much damage your shots cause.',
'FireSpeed': 'The rate of fire determines how many times per second you can shoot.',
'Precision': 'Precision determines what percentage of shots will hit your target.',
'ElectricAttack': 'Electronic attack capability determines the ability to hack enemy ships.',
'Armor': 'The defense of the ship determines how much damage it can withstand.',
'Tripulation': 'The crew indicates the cost in population of the ship.',
'Evasion': 'Evasion determines what percentage of enemy shots will be dodged by the ship.',
'Camouflage': 'Camouflage indicates the probability of the ship being detected by the enemy.',
'Speed': 'Speed ​​indicates how fast the ship can reach its destination.',
'Perception': 'Perception determines how far the ship can detect objects or fleets.',
'Autonomy': 'The autonomy determines how long the ship can be operational without refueling.',
'CargoCapacity': 'Cargo capacity indicates how much material the ship can transport.',

// ship skills
'Glory':				'Faction units have bonus morale.',
'SpaceLab':				'can perform scientific mission',	
'Colonizer':			'can perform colonization mission',	
'Carrier':				'bonus fleet autonomy',
'Projectile':			'suicide attack',
'Kamikaze':				'bonus evasion and damage',
'Missile':				'bonus speed',
'Bomb':					'bonus damage',
'DirtyBomb':			'bonus against enemy humans',
'Emp':					'emp',
'Espionage':			'can perform espionage missions',
'PlanetBuster':			'can destroy planets',	
'ArmorPiercer':			'bonus against big ships',
'FlyingBunker':			'bonus fleet defence',	
'NeuralCenter':			'bonus fleet intelligence',
'BigEar':				'bonus fleet perception',	
'IntelligentFire':		'bonus against enemy fuel and computers',	
'RapidFire':			'bonus against small ships',
'FlakAA':				'bonus against projectiles',
'Hackathon':			'bonus fleet electronic attack',
'AntiRadiationGun':		'bonus against enemy radios',	
'Artillery':			'bonus against enemy armor',	
'PrecisionBombardment':	'bonus fleet precision',	
'Hacker':				'electronic bonus against unmanned ships',
'Bombardment':			'bonus against surface objectives',
}

SPANISH_LONG = {	
//main menu	
'Single player':'Juega solo contra un oponente controlado por la maquina.',
'Multiplayer':'Online',
'Tutorial':'Tutorial',
'Options':'Opciones',
'Language':'Idioma',	
	
//tutorial
'0World':'Esto es la pantalla del mundo. En este planetoide puedes construir cierto numero de edificios y almacenar recursos.',	
'1World':'Para ver para que sirve un edificio, situa el cursor sobre su nombre. Para construir haz clic en su imagen y emplazalo en el planetoide a la izquierda.',	
'2World':'OBJETIVO: construye un hangar.',	
'0Map':'Bien hecho! Esto es la pantalla del mapa. Si seleccionas un objeto podras ver sus detalles.',	
'1Map':'Para poder recoger materia de un objeto, debes crear una flota con naves de carga. Selecciona Anadir flota, y crea un nuevo tipo de nave.',	
'2Map':'OBJETIVO: crea una flota con naves de carga y enviala a recoger la materia de la nube de polvo.',	
'0Map2':'Buen trabajo. Para completar el tutorial debes destruir la flota enemiga.',	
'1Map2':'Para ello deberas crear un nuevo hangar, y organizar otra flota con naves de guerra. Luego, enviala a atacar a la flota enemiga.',	
'2Map2':'OBJETIVO: crea una flota de guerra y destruye la flota enemiga.',	
'TutorialEnd':'Con esto concluye el tutorial. Gracias por jugarlo!',	
	
//faction history	
'History_Empire':'Las fuerzas del eje ganan la segunda guerra mundial, gracias a la neutralidad de Estados Unidos y una ataque relampago a la capital de la URSS.#Poco despues de ganar la guerra el eje se hace con los paises restantes, e inicia una rapida expansion por el sistema solar.#Tras varios siglos de investigacion eugenica surge una nueva raza de superhumanos, mas inteligentes y adaptados a la vida en el espacio.',		
'History_Union':'Tras ganar la segunda guerra mundial, los aliados traicionan a la URSS y continuan el avance hacia el este mas alla de Berlin.#Contando con China como un poderoso aliado y con varias revoluciones alzandose por todo el mundo, los Soviets terminan por gobernar toda la Tierra.#Empezando con la construccion de ciudades flotantes en Venus, la bandera del el hoz y el martillo termina por ondear en toda la galaxia.',
'History_League':'Los grandes descubrimientos cientificos en Jonia no quedan olvidados en el tiempo, sino que son utilizados para desarrollar una revolucion industrial en la epoca clasica.#Con el apoyo del reino de Macedonia y su enorme superioridad tecnologica y cientifica, las ciudades Jonicas forman una Liga y capturan facilmente todas las civilizaciones antiguas.#El viaje estelar comienza poco despues de conquistar el mundo, usando grandes naves arca multigeneracionales.',
'History_Califate':'El Nuevo Califato',	
'History_Republic':'Tercera Republica Romana',
'History_Dinasty':'Dinastia Sikh',
'History_Serpent':'Imperio de la Serpiente',
'History_Crown':'Imperio Austropruso-Espanol',
'History_Corporation':'Corporacion Universal',
'History_Confederation':'Federacion Anarquista',		
'History_States':'EUU',							
'History_Colonies':'Imperio Francobritanico',		
'History_Aliance':'Alianza de Isandlwana',		
'History_Khanate':'Khanato celestial',			
'History_Earldoms':'Condados del norte',		
'History_Kingdom':'Reino Eterno de Egipto',	

//world
'mine' : 'Extrae materia pura del mundo.',	
'plant' : 'Produce energia a partir de materia.',	
'house' : 'Habitaje para 32 personas.',	
'recycler' : 'Refina restos para producir materia.',
'factory' : 'Augmenta velocidad de produccion de naves.',
'lab' : 'Augmenta velocidad de investigacion.',	
'hangar' : 'Permite creacion de una flota.',
'bunker' : 'Anade poder defensivo en caso de invasion.',

//ship parts
'Weapon':'Las armas añaden cadencia de fuego a la nave.',
'Ammo':'Cuanto mas espacio para munición tengas, mas potentes serán tus armas.',
'Shield':'Los escudos de la nave determinan su resistencia a ataques enemigos.',
'Engine':'Cuantos más motores tengas, más rápida será tu nave.',
'Fuel':'Cuantos mas depositos de combustible tengas, más tiempo podrá estar tu nave operativa.',
'Habitat':'Los habitats añaden tripulación humana a la nave. Los humanos son útiles para muchas cosas, especialmente añaden inteligencia a tu nave.',
'Radio':'Cuantas mas radios tengas, mejor podra percibir su entorno la nave.',
'Computer':'Los ordenadores pueden usarse para piratear naves enemigas, y también ofrecen inteligencia a tu nave.',

//ship props
'FirePower':'La potencia de fuego determina cuanto daño causan tus disparos.',
'FireSpeed':'La cadencia de fuego determina cuantas veces por segundo puedes disparar.',
'Precision':'La precision determina que porcentaje de disparos acertarán a su blanco.',
'ElectricAttack':'La capacidad de ataque electrónico determina la capacidad de hackear naves enemigas.',
'Armor':'La defensa de la nave determina cuanto daño puede resistir.',
'Tripulation':'La tripulación indica el coste en población de la nave.',
'Evasion':'La evasión determina que porcentaje de disparos enemigos serán esquivados por la nave.',
'Camouflage':'El camuflaje indica la probabilidad de la nave de ser detectada por el enemigo.',
'Speed':'La velocidad indica cuan rápido la nave puede llegar a su destino.',
'Perception':'La percepción determina a que distancia puede la nave detectar objetos o flotas.',
'Autonomy':'La autonomia determina cuanto tiempo puede estar la nave operativa sin repostar.',
'CargoCapacity':'La capacidad de cargo indica que cantidad de materia puede transportar la nave.',

// ship skills
'Glory':				'Las unidades de facción tienen moral adicional',
'SpaceLab':				'Puede realizar misión científica',
'Colonizer':			'Puede realizar misión de colonización',
'Carrier':				'Bonus de autonomía de la flota',
'Projectile':			'Ataque suicida',
'Kamikaze':				'Bonus de evasión y daño',
'Missile':				'Velocidad extra',
'Bomb':					'Daño adicional',
'DirtyBomb':			'Bonus contra humanos enemigos',
'Emp':					'Emp',
'Espionage':			'Puede realizar misiones de espionaje',
'PlanetBuster':			'Puede destruir planetas',
'ArmorPiercer':			'Bonus contra los grandes barcos',
'FlyingBunker':			'Defensa de la flota de bonificación',
'NeuralCenter':			'Inteligencia de la flota de bonificación',
'BigEar':				'Percepción de la flota adicional',
'IntelligentFire':		'Bonificación contra combustible y ordenadores enemigos',
'RapidFire':			'Bonus contra pequeños barcos',
'FlakAA':				'Bonus contra proyectiles',
'Hackathon':			'Ataque electrónico de flotas extra',
'AntiRadiationGun':		'Bonus contra radios enemigas',
'Artillery':			'Bonus contra armadura enemiga',
'PrecisionBombardment':	'Precisión adicional de la flota',
'Hacker':				'Bonificación electrónica contra buques no tripulados',
'Bombardment':			'Bonus contra objetivos superficiales',
	
}
