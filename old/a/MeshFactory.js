/* MeshFactory */

var lib = {
	part : function(geo,h){
		return {m:new THREE.Mesh(geo),s:h};
	},
	flip : function(part){
		part.m.rotation.z = Math.PI/2;		
		return part;
	},
	altcyl : function(r1,r2,h) {
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r1,r2,h, 8, 1, false)),s:h};
	},
	cyl : function(r,h){
		return lib.altcyl(r,r,h);
	},
	prism : function(r,p,h){
		return {m:new THREE.Mesh(new THREE.CylinderGeometry(r,r,h, p, 1, false)),s:h};
	},
	sph : function(h) {
		return {m:new THREE.Mesh(new THREE.SphereGeometry(h, 8, 8)),s:h};
	},
	tor : function(h1,h2){
		var tor = new THREE.Mesh(new THREE.TorusGeometry(h1,h2,8,16));
		tor.rotation.x=Math.PI/2;
		return {m:tor,s:1};		
	},
	littleWing : function(size){
		var shape = new THREE.Shape();
		shape.moveTo( size,size );
		shape.lineTo( size,-size/2 );
		shape.lineTo( -size/2,-size/2 );
		var mesh = new THREE.Mesh(new THREE.ExtrudeGeometry(shape, {steps: 1,depth: .2,bevelEnabled: false} ));
		return lib.skew({m:mesh,s:1},3);
	},
	skew : function(part,amt){
		part.m.rotation.z = Math.PI / amt;
		return part;
	},
	merge : function(b,a){
		a.updateMatrix();
		b.merge(a.geometry, a.matrix);
	},	
	assemble :function(parts){
		var geo = new THREE.Geometry();	
		var orig = parts[0] ? -parts[0].s/2 : -parts[1].s/2;
		for ( var i = 0; i < parts.length; i++ ) {	
			if(parts[i]){
				parts[i].m.position.y = orig+parts[i].s/2 + (parts[i].shift||0);
				lib.merge(geo,parts[i].m);
				if(!parts[i].skip) orig += parts[i].s;
			}
		}
		geo.translate(0,-orig/2,0);
		return geo;
	},
	multi: function(object,amount,radius,rotate=false){
		var geo = new THREE.Geometry();
		
		var x0 = 0;
		var y0 = 0;		
		var angle = 0;
		for(var i = 0; i < amount; i++) {
			var x = x0 + radius * Math.cos(2 * Math.PI * i / amount);
			var y = y0 + radius * Math.sin(2 * Math.PI * i / amount);   
			var cyl = object.m.clone();					
			cyl.position.x = x;
			cyl.position.z = y;			
			if(rotate){
				cyl.rotation.y = angle;			
				angle -= 2*Math.PI/amount;	
			}
			lib.merge(geo,cyl);
		}
		
		return {m:new THREE.Mesh(geo),s:object.s};
	},
	multiRot: function(object,amount,radius){
		return lib.multi(object,amount,radius,true)
	},	
	skip(piece){
		piece.skip = true;
		return piece;		
	},
	iif(part,num,lim=1){
		return num<lim ? null : part;
	},
	nif(part,num,lim=1){
		return !num<lim ? null : part;
	},
	f(num,factor){
		return 1+num*factor;
	},
	move(piece,amount){
		piece.shift = amount;
		return piece;
	}
}

var MeshFactory = {
	World : function(sz){
		var geo = new THREE.DodecahedronGeometry(sz, 1);
		var size = 1;
		geo.vertices.forEach(function(v){
			v.x += (0-Math.random()*(size/4));
			v.y += (0-Math.random()*(size/4));
			v.z += (0-Math.random()*(size/4));
		});
		for (var i = 0; i < geo.faces.length; i++) {
			face = geo.faces[i];
			face.color.setRGB(0, 0, 0.8 * Math.random() + 0.2);
			face.faceid = i;
		} 
		geo.computeFaceNormals();
		return geo;		
	},
	Mine : function(amt){	
		return lib.assemble([
			lib.multi(lib.altcyl(.5,0,2),amt,1),
			lib.skip(lib.multiRot(lib.flip(lib.cyl(.2,1)),amt,.5)),
			lib.multi(lib.altcyl(.3,.3,.7),amt,1)
		]);	
	},
	Plant : function(amt){	
		return lib.assemble([
			lib.skip(lib.move(lib.sph(.8),.8)),
			lib.multi(lib.cyl(.3,2),amt,1),
			lib.skip(lib.move(lib.multiRot(lib.flip(lib.cyl(.2,1)),amt,.5),-1))
		]);			
	},
	House : function(amt){	
		return lib.assemble([
			lib.multiRot(lib.prism(.6,amt,2),amt,1),
			lib.skip(lib.move(lib.multiRot(lib.flip(lib.prism(.2,3,1)),amt,.5),-1))
		]);			
	},
	Recycler : function(amt){	
		return lib.assemble([
			lib.prism(2,amt,3)
		]);			
	},
	Factory : function(amt){	
		return lib.assemble([
			lib.cyl(2,4),	
			lib.move(lib.skip(lib.multiRot(lib.skew(lib.cyl(.2,1.5),5),amt,1)),-.1),
			lib.cyl(1,1.5)
		]);			
	},
	Lab : function(amt){	
		return lib.assemble([
			lib.skip(lib.multi(lib.cyl(.2,5),amt,1)),			
			lib.cyl(.6,5),	
			lib.move(lib.skip(lib.multiRot(lib.skew(lib.cyl(.2,1),3),amt,.6)),-.3),
			lib.move(lib.altcyl(.6,0,1),-.1)
		]);			
	},
	Hangar : function(amt){	
		return lib.assemble([
			lib.cyl(2,5),	
			lib.multi(lib.cyl(.3,1),amt,1)
		]);			
	},
	Bunker : function(amt){	
		return lib.assemble([
			lib.sph(2),
			lib.move(lib.skip(lib.multiRot(lib.skew(lib.cyl(.2,1),3),amt,1.8)),-.3)
		]);			
	},
	
	Build : function(ship){		
		if(ship.Size == 8) return this.Size8(ship);
		if(ship.Size == 4) return this.Size4(ship);
		if(ship.Size == 2) return this.Size2(ship);
		if(ship.Size == 1) return this.Size1(ship);
	},
	Size1 : function(ship){
		var sphereBody = ship.Shield + ship.Radio + ship.Habitat;
		
		return lib.assemble([
			lib.altcyl(.5,.4,.5),
			lib.iif(lib.move(lib.skip(lib.multiRot(lib.littleWing(1),4+ship.Engine,1)),-.8),!ship.Radio),
			
			lib.iif(lib.skip(lib.move(lib.multiRot(lib.skew(lib.altcyl(.2,.1,4),12),4,2.35),-3)),ship.Radio),
			
			lib.altcyl(1*lib.f(ship.Ammo,.5),.8,2-sphereBody),
			
			lib.iif(lib.skip(lib.multi(lib.altcyl(0,.5,3),4,1.1)),ship.Weapon),
			lib.iif(lib.skip(lib.multiRot(lib.flip(lib.cyl(.3,1)),4,0.5)),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.65,2),7,.6)),ship.Fuel),
			
			lib.iif(lib.altcyl(.8,1*lib.f(ship.Ammo,.5),2),ship.Ammo + ship.Engine),			
			lib.iif(lib.sph(2),sphereBody),
			lib.iif(lib.cyl(1*lib.f(ship.Nada,.5),3),ship.Nada),
			
			lib.iif(lib.move(lib.tor(2.2,.5),-1.5),ship.Shield),	

			lib.iif(lib.move(lib.multiRot(lib.skew(lib.cyl(.3,2),8),4,1.5),-2.5),ship.Radio),
						
			lib.iif(lib.altcyl(0,.8,1.5),ship.Engine+ship.Weapon),
			
			lib.iif(lib.move(lib.sph(.8),-.5),ship.Ammo),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,.8),4,.6)),ship.Computer),
			lib.iif(lib.cyl(.3,1),ship.Computer)		
			
		]);		
	},
	Size2 : function(ship){
		return lib.assemble([

			lib.nif(lib.altcyl(.5,.8,1),ship.Engine),
			lib.iif(lib.multi(lib.altcyl(.5,.8,1),2+ship.Engine,1),ship.Engine),
			
			lib.cyl(1*lib.f(ship.Engine*2,.3),1),
			lib.cyl(.8,.5),

			lib.cyl(2,3),
		]);		
	},
	Size4 : function(ship){
		return lib.assemble([			
			lib.altcyl(.5*lib.f(ship.Engine,.1),.8*lib.f(ship.Engine,.1),1),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.iif(lib.multi(lib.cyl(.2,1),ship.Engine+1,.6),ship.Engine),	
			
			lib.iif(lib.skip(lib.multi(lib.sph(0.75),ship.Fuel*3,1*lib.f(ship.Fuel,.1))),ship.Fuel),
			lib.cyl(0.9*lib.f(ship.Fuel,.1),1.5*lib.f(ship.Nada,.1)),
			
			lib.iif(lib.skip(lib.tor(2.5,.2)),ship.Radio),	
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.3,1.2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.cyl(1.2*lib.f(ship.Ammo,.1),1.2),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,1*lib.f(ship.Computer,.1)),4,.6)),ship.Computer),			
			lib.iif(lib.cyl(.3,0.5*lib.f(ship.Computer,.2)),ship.Computer),		
						
			lib.cyl(1,1*lib.f(ship.Habitat,.2)),
			
			lib.skip(lib.multi(lib.cyl(.2,1.8),2*(ship.Shield+1),.6)),
			
			lib.cyl(.5,2),		
			
			lib.altcyl(0,.5,.2*lib.f(ship.Engine,2)),
		]);		
	},
	Size8 : function(ship){
		return lib.assemble([			
			lib.altcyl(.5*lib.f(ship.Engine,.1),.8*lib.f(ship.Engine,.1),1),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.cyl(1,.2*lib.f(ship.Engine,.2)),
			lib.iif(lib.multi(lib.cyl(.2,1),ship.Engine+1,.6),ship.Engine),	
			lib.iif(lib.skip(lib.multi(lib.sph(0.75),ship.Fuel*3,1*lib.f(ship.Fuel,.1))),ship.Fuel),
			lib.cyl(0.9*lib.f(ship.Fuel,.1),1.5*lib.f(ship.Nada,.1)),
			
			lib.skip(lib.multi(lib.part(this.Size4(ship),4),5,3)),
			lib.skip(lib.multiRot(lib.flip(lib.cyl(.5,3)),5,1)),			
			
			lib.iif(lib.skip(lib.tor(2.5,.2)),ship.Radio),	
			lib.iif(lib.skip(lib.multi(lib.cyl(.3,1.2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.cyl(1.2*lib.f(ship.Ammo,.1),3),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,2),ship.Weapon*2,1.2*lib.f(ship.Ammo,.1))),ship.Weapon),
			lib.iif(lib.skip(lib.multi(lib.cyl(.2,1*lib.f(ship.Computer,.1)),4,.6)),ship.Computer),			
			lib.iif(lib.cyl(.3,0.5*lib.f(ship.Computer,.2)),ship.Computer),		
			lib.cyl(1,2*lib.f(ship.Habitat,.2)),
			lib.skip(lib.multi(lib.cyl(.2,1.8),2*(ship.Shield+1),.6)),
			lib.cyl(.5,3),		
			lib.altcyl(0,.5,.2*lib.f(ship.Engine,2))
		]);		
	}
}

var Astronomy = {
	BuildMeshSelector:function(sz){
		var geometry2 = lib.sph(sz).m.geometry;
		var material2 = MaterialsFactory.select();
		var sphere2 = new THREE.Mesh(geometry2, material2);
		sphere2.isSelectableMesh = true;
		return sphere2;
	},
	BuildWorld:function(){
		var mesh = Astronomy.BuildMeshSelector(1);	

		var material = new THREE.MeshNormalMaterial();
		var geo = MeshFactory.World(1);
		var SHIPMESH = new THREE.Mesh(geo, material);
		var scale = .5;
		SHIPMESH.scale.set(scale,scale,scale)
		mesh.add(SHIPMESH);
		
		return mesh;
	},
	BuildField:function(){
		var mesh = Astronomy.BuildMeshSelector(1);	

		for(var i=0;i<25;i++){
			var material = new THREE.MeshNormalMaterial();
			var geo = lib.sph(.05).m.geometry;
			var SHIPMESH = new THREE.Mesh(geo, material);
			SHIPMESH.position.x += 0.4-0.8 * Math.random();
			SHIPMESH.position.y += 0.4-0.8 * Math.random();
			SHIPMESH.position.z += 0.4-0.8 * Math.random();
			
			var scale = 1-.5 * Math.random();
			SHIPMESH.scale.set(scale,scale,scale)
			mesh.add(SHIPMESH);
		}
		
		return mesh;
	},
	BuildGasCloud:function(){
		
		//STILL ARTIFACTS
		
		var size = 1.2;
		
		var geometry = new THREE.Geometry(); 
		var particleCount = 5; 
		
		for (i = 0; i < particleCount; i++) {

			var vertex = new THREE.Vector3();
			vertex.x = Math.random() * size - size/2;
			vertex.y = Math.random() * size - size/2;
			vertex.z = Math.random() * size - size/2;

			geometry.vertices.push(vertex);
		}

		var image = new THREE.TextureLoader().load( "https://bitbucket.org/johnhammer/johnhammer.bitbucket.io/raw/49f552ded8d25997b781a9ee99d8f990dff0b903/a/assets/gas.png" )
		
		var material = new THREE.PointsMaterial( {size: 2.5, map: image, blending: THREE.AdditiveBlending, depthTest: false,transparent: true, opacity:.5 } )

		particles = new THREE.PointCloud(geometry, material);
		
		var mesh = Astronomy.BuildMeshSelector(size);	
		//mesh.renderOrder = 999;
		
		mesh.add(particles);

		return mesh;
	},
	BuildDustCloud:function(){
		var mesh = Astronomy.BuildMeshSelector(1);	
		
		var geometry = new THREE.Geometry(); 
		var particleCount = 50; 
		var size = 1;

		for (i = 0; i < particleCount; i++) {

			var vertex = new THREE.Vector3();
			vertex.x = Math.random() * size - size/2;
			vertex.y = Math.random() * size - size/2;
			vertex.z = Math.random() * size - size/2;

			geometry.vertices.push(vertex);
		}

		particles = new THREE.PointCloud(geometry, new THREE.PointCloudMaterial({
			size: .01
		}));

		particles.rotation.x = Math.random() * 1;
		particles.rotation.y = Math.random() * 1;
		particles.rotation.z = Math.random() * 1;

		mesh.add(particles);
		
		return mesh;
	},
	BuildFleet:function(){
		var mesh = Astronomy.BuildMeshSelector(.5);	

		var material = new THREE.MeshNormalMaterial();
		var geo = lib.altcyl(.2,0,.5).m.geometry;
		var SHIPMESH = new THREE.Mesh(geo, material);
		var scale = .5;
		SHIPMESH.rotation.x = -Math.PI/2;
		SHIPMESH.scale.set(scale,scale,scale)
		mesh.add(SHIPMESH);
		
		return mesh;
	},

	
	
}



