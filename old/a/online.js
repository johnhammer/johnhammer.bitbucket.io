/*ONLINE*/
var pc1,pc2,sdpConstraints,activedc;

function create(){
  dc1 = pc1.createDataChannel('entropy', {reliable: true})
  activedc = dc1
  dc1.onopen = function(e) { }
  dc1.onmessage = function(e) {
    if (e.data.size) {
      fileReceiver1.receive(e.data, {})
    } else {
      if (e.data.charCodeAt(0) == 2) {
        return
      }
      var data = JSON.parse(e.data)
      if (data.type === 'file') {
        fileReceiver1.receive(e.data, {})
      } else {
		  alert('[' + new Date() + '] ' + data.message)
      }
    }
  }
  pc1.createOffer(function(desc) {
    pc1.setLocalDescription(desc, function() {}, function() {})
  }, function() { }, sdpConstraints)
}

function allow(){
	var room = prompt("Enter player info", "");
	if (room != null) {
	  var answer = room;
	  var answerDesc = new RTCSessionDescription(JSON.parse(answer))
	  pc1.setRemoteDescription(answerDesc);
	}
}

function join(){
	var room = prompt("Enter room info", "");
	if (room != null) {
		var offer = room;
		var offerDesc = new RTCSessionDescription(JSON.parse(offer))
		pc2.setRemoteDescription(offerDesc)
		pc2.createAnswer(function(answerDesc) {
			pc2.setLocalDescription(answerDesc)
		},
		function () { },
		sdpConstraints)
	}
}

function sendMessage (value) {
  if (value) {
    activedc.send(JSON.stringify({message: value}));
  }
  return false
}


function initOnline(){
	if (navigator.webkitGetUserMedia) {
	  RTCPeerConnection = webkitRTCPeerConnection
	}

	var cfg = {'iceServers': [{'url': "stun:stun.gmx.net"}]},
	con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
	
	var sdpConstraints = {
	  optional: [],
	}
	
	//pc 1
	pc1 = new RTCPeerConnection(cfg, con), dc1 = null, tn1 = null, activedc, pc1icedone = false;
	
	pc1.onicecandidate = function (e) {
	  if (e.candidate == null) {
		var room = JSON.stringify(pc1.localDescription);
		alert(room);
	  }
	}
	
	//pc 2
	pc2 = new RTCPeerConnection(cfg, con), dc2 = null, pc2icedone = false;

	pc2.ondatachannel = function (e) {
	  var datachannel = e.channel || e;
	  dc2 = datachannel
	  activedc = dc2
	  dc2.onopen = function (e) { }
	  dc2.onmessage = function (e) {
		if (e.data.size) {
		  fileReceiver2.receive(e.data, {})
		} else {
		  var data = JSON.parse(e.data)
		  if (data.type === 'file') {
			fileReceiver2.receive(e.data, {})
		  } else {
			alert('[' + new Date() + '] ' + data.message)
		  }
		}
	  }
	}

	pc2.onicecandidate = function (e) {
	  if (e.candidate == null) {
		var desc = JSON.stringify(pc2.localDescription);
		alert(desc);
	  }
	}
}