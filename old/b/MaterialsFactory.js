/* Materials Factory */

var MaterialsFactory = {
	select : function(){
		return new THREE.MeshLambertMaterial({
			color: 0xFFFFFF, 
			transparent: true, 
			opacity: 0.5
		});
	},
	selected : function(){
		return new THREE.MeshLambertMaterial({
			color: 0xFF0000, 
			transparent: true, 
			opacity: 0.5
		});
	},
	selectable : function(){
		return new THREE.MeshLambertMaterial({
			color: 0x00FF00, 
			transparent: true, 
			opacity: 0.5
		});
	},
}