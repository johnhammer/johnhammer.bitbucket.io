/* Ship Names */

function getShipName(ship){
	var factionName = getFactionShip(Memory.get('faction',0),ship);
	if(factionName) return factionName;
	
	return getShipTypeName(ship);	
}

function getShipSkills(s){
	function setSkill(skills,skill){
		if(!skills.s1){
			skills.s1 = {
				title:skill,
				text:LONGTEXT(skill)
			};
		}		else if(!skills.s2){
			skills.s2 = {
				title:skill,
				text:LONGTEXT(skill)
			};
		}
	}
	
	function IsProjectile(ship){
		return 	ship.Weapon +
				ship.Shield +
				ship.Radio + 
				ship.Nada + 
				ship.Computer == 0;
	}

	var skills = {};

	if(getFactionShip(Memory.get('faction',0),s)) 			setSkill(skills,'Glory');
	if(s.Habitat>=2&&s.Radio>=1&&s.Computer>=1)				setSkill(skills,'SpaceLab');				
	if(s.Habitat>=3&&s.Fuel>=1)								setSkill(skills,'Colonizer');				
	if(s.Nada>=4&&s.Fuel>=2)								setSkill(skills,'Carrier');				
	if(IsProjectile(s))										setSkill(skills,'Projectile');				
	if(IsProjectile(s)&&s.Size<=4&&s.Habitat>=1&&s.Ammo>=1)	setSkill(skills,'Kamikaze');				
	if(IsProjectile(s)&&s.Size<=2&&s.Engine>0)				setSkill(skills,'Missile');				
	if(IsProjectile(s)&&s.Ammo>0)							setSkill(skills,'Bomb');					
	if(IsProjectile(s)&&s.Ammo>0&&s.Fuel>0)					setSkill(skills,'DirtyBomb');				
	if(s.Size<=2&&s.Weapon+s.Nada+s.Radio==0&&s.Computer>0)	setSkill(skills,'Emp');					
	if(s.Size<=2&&s.Radio>=1)								setSkill(skills,'Espionage');				
	if(s.Ammo>=6)		                                    setSkill(skills,'PlanetBuster');			
	if(s.Ammo>=4)											setSkill(skills,'ArmorPiercer');			
	if(s.Shield>=4)											setSkill(skills,'FlyingBunker');			
	if(s.Computer>=4)										setSkill(skills,'NeuralCenter');			
	if(s.Radio>=4)											setSkill(skills,'BigEar');					
	if(s.Weapon+s.Habitat>1&&s.Weapon==s.Habitat)			setSkill(skills,'IntelligentFire');			
	if(s.Weapon>=4)											setSkill(skills,'RapidFire');				
	if(s.Weapon>=2&&s.Radio>=2)								setSkill(skills,'FlakAA');					
	if(s.Habitat>=2&&s.Computer>=2)							setSkill(skills,'Hackathon');				
	if(s.Weapon>=2&&s.Computer>=2)							setSkill(skills,'AntiRadiationGun');		
	if(s.Weapon>=1&&s.Habitat>=1&&s.Ammo>=1)				setSkill(skills,'Artillery');				
	if(s.Weapon>=1&&s.Radio>=1&&s.Ammo>=1)					setSkill(skills,'PrecisionBombardment');	
	if(s.Computer>=1&&s.Habitat>=1)							setSkill(skills,'Hacker');					
	if(s.Weapon>=1&&s.Ammo>=1)								setSkill(skills,'Bombardment');			

	return skills;
}

function getShipTypeName(ship){
	switch(ship.Size){
		case 1: return getShipName1(ship);
		case 2: return getShipName2(ship);
		case 4: return getShipName4(ship);
		case 8: return getShipName8(ship);
	}
}

function getFactionShip(faction,ship){	
	var factionShips = CONST.factions[faction].ships;
	for (i = 0; i < factionShips.length ; i++) { 
		if(shipUtils.match(ship,factionShips[i])){
			return factionShips[i].name;
		}
	}
	return null;	
}

function getShipName1(count){
	if(count.Weapon==1) 	return TEXT('Fighter drone');
	if(count.Ammo==1) 		return TEXT('Bomb');
	if(count.Shield==1) 	return TEXT('Decoy drone');
	if(count.Engine==1) 	return TEXT('Missile');
	if(count.Fuel==1) 		return TEXT('Mother drone');
	if(count.Habitat==1) 	return TEXT('Explorer');
	if(count.Radio==1) 		return TEXT('Probe');
	if(count.Computer==1) 	return TEXT('EMP pulse');
							return TEXT('Cargo drone');
}

function getShipName2(count){
	if(count.Weapon==1 && count.Ammo==1) 		return TEXT('Cazabombardero');
	if(count.Habitat==1 && count.Ammo==1) 		return TEXT('Kamikaze');
	if(count.Habitat==1 && count.Computer==1) 	return TEXT('Hacker');
	if(count.Habitat==2) 						return TEXT('Insignia ligera');
	
	var str = [];
	
	if(count.Weapon>=1) 						str.push(TEXT('Caza'));
	else if(count.Ammo>=1) 						str.push(TEXT('Bomba'));
	else if(count.Computer>=1) 					str.push(TEXT('Interceptor'));
	else if(count.Nada>=1) 						str.push(TEXT('Nave de carga pequena'));
	else if(count.Habitat>=1) 					str.push(TEXT('Explorador'));
	else if(count.Fuel>=1) 						str.push(TEXT('Nodriza pequena'));
	else if(count.Radio>=1) 					str.push(TEXT('Telescopio'));
	else if(count.Engine>=1) 					str.push(TEXT('Misil'));
	else if(count.Shield>=1) 					str.push(TEXT('Senuelo'));
	
	return str.join(" ");
}

function getShipName4(count){

	if(count.Habitat==1 && count.Ammo==3) 							return TEXT('Kamikaze pesado');

	var str = [];
	
	if(count.Habitat>=3) 											str.push(TEXT('Insignia'));
	else if(count.Weapon>=1 && count.Ammo>=1 && count.Shield>=1) 	str.push(TEXT('Crucero'));
	else if(count.Weapon>=3) 										str.push(TEXT('Fragata'));
	else if(count.Weapon>=2 && count.Ammo>=1) 						str.push(TEXT('Canonera'));
	else if(count.Weapon>=1 && count.Ammo>=2) 						str.push(TEXT('Destructor'));
	else if(count.Ammo>=3) 											str.push(TEXT('Bomba de antimateria'));
	else if(count.Weapon>=1 && count.Shield>=2) 					str.push(TEXT('Acorazado'));
	else if(count.Engine>=2 && count.Weapon>=1) 					str.push(TEXT('Caza pesado'));
	else if(count.Engine>=2 && count.Ammo>=1) 						str.push(TEXT('Misil pesado'));
	else if(count.Computer>=2 && count.Weapon>=1) 					str.push(TEXT('Corbeta mixta'));
	else if(count.Computer>=3) 										str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=2 && count.Radio>=1)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=1 && count.Radio>=2)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Shield>=3) 										str.push(TEXT('Nave escudo'));
	else if(count.Nada>=3) 											str.push(TEXT('Nave de carga'));
	else if(count.Fuel>=3) 											str.push(TEXT('Nodriza'));
	else if(count.Radio>=3) 										str.push(TEXT('Estacion espacial'));
	else if(count.Radio>=2 && count.Habitat>=1) 					str.push(TEXT('Estacion espacial'));
	else if(count.Weapon>=1 && count.Ammo>=1) 						str.push(TEXT('Nave de guerra'));
	else if(count.Computer>=2) 										str.push(TEXT('Corbeta de senales'));
	else if(count.Weapon>=2) 										str.push(TEXT('Corbeta'));
	else if(count.Radio>=2) 										str.push(TEXT('Nave Radio'));
	else if(count.Nada>=2) 											str.push(TEXT('Nave de carga'));
	else if(count.Habitat>=2) 										str.push(TEXT('Transporte'));
	else if(count.Engine>=3) 										str.push(TEXT('Misil'));	
	else 															str.push(TEXT('Hibrida'));
	
	return str.join(" ");
}

function getShipName8(count){
	var str = [];	
																	str.push(TEXT('Gran'));
	if(count.Habitat>=3) 											str.push(TEXT('Insignia'));
	else if(count.Weapon>=1 && count.Ammo>=1 && count.Shield>=1) 	str.push(TEXT('Crucero'));
	else if(count.Weapon>=3) 										str.push(TEXT('Fragata'));
	else if(count.Weapon>=2 && count.Ammo>=1) 						str.push(TEXT('Canonera'));
	else if(count.Weapon>=1 && count.Ammo>=2) 						str.push(TEXT('Destructor'));
	else if(count.Ammo>=3) 											str.push(TEXT('Bomba de antimateria'));
	else if(count.Weapon>=1 && count.Shield>=2) 					str.push(TEXT('Acorazado'));
	else if(count.Engine>=2 && count.Weapon>=1) 					str.push(TEXT('Caza pesado'));
	else if(count.Engine>=2 && count.Ammo>=1) 						str.push(TEXT('Misil pesado'));
	else if(count.Computer>=2 && count.Weapon>=1) 					str.push(TEXT('Corbeta mixta'));
	else if(count.Computer>=3) 										str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=2 && count.Radio>=1)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Computer>=1 && count.Radio>=2)					str.push(TEXT('Nave de guerra electronica'));
	else if(count.Shield>=3) 										str.push(TEXT('Nave escudo'));
	else if(count.Nada>=3) 											str.push(TEXT('Nave de carga'));
	else if(count.Fuel>=3) 											str.push(TEXT('Nodriza'));
	else if(count.Radio>=3) 										str.push(TEXT('Estacion espacial'));
	else if(count.Radio>=2 && count.Habitat>=1) 					str.push(TEXT('Estacion espacial'));
	else if(count.Weapon>=1 && count.Ammo>=1) 						str.push(TEXT('Nave de guerra'));
	else if(count.Computer>=2) 										str.push(TEXT('Corbeta de senales'));
	else if(count.Weapon>=2) 										str.push(TEXT('Corbeta'));
	else if(count.Radio>=2) 										str.push(TEXT('Nave Radio'));
	else if(count.Nada>=2) 											str.push(TEXT('Nave de carga'));
	else if(count.Habitat>=2) 										str.push(TEXT('Transporte'));
	else 															str.push(TEXT('Hibrida'));
	
	return str.join(" ");
}


/* Fleet Names */

function getRandomFleetName(){	
	var faction = Memory.get('faction',0);
	return LIB.Capitalize(CONST.factions[faction].fleets[Math.floor(Math.random() * 32)]);
}