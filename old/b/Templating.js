/* Templating */

var TEMPLATING = {	
	Main:function(){
		var language = Memory.get('language');
		var text = TEMPLATE[language].main.menu;
		
		return `
			<div class="main_menu">	
				<h1 class="title">Entropy</h1>		
				<div id="colapsibles">				
					${text.map((menuOption, index) => 
						`<button class="cs_tertiary main_button" onclick="Main.Menu(${index},this)">${menuOption}</button>
					`).join('')}				
				</div>
			</div>			
		`;
	},
	MainModalSinglePlayer:function(data){
		return `
			<h1 id="lng_singleplayer" class="title">TUTORIAL</h1>
			<p id="lng_singleplayer_desc" class=""></p>
			<div class="color_picker_container">
				<p id="lng_colorpicker">Pick a color</p>
				<input id="colorpicker" type="color" name="favcolor" value="#ff0000">
			</div>
			<button class="main_button cs_main" onclick="Tutorial.start()">GO!</button>
		`;
	},	
	MainModalMultiplayer:function(data){
		var language = Memory.get('language');
		var text = TEMPLATE[language].main.online;
		
		return `
			<h1 class="title">Multiplayer</h1>
			
			${text.map((menuOption, index) => 
				`<button class="main_button cs_main" onclick="Main.OnlineMenu(${index},this)">${menuOption}</button>
			`).join('')}
		`;
	},	
	MainModalTutorial:function(data){
		return `
			<h1 id="lng_singleplayer" class="title">TUTORIAL</h1>
			<p id="lng_singleplayer_desc" class=""></p>
			<button class="main_button cs_main" onclick="TUTORIAL.start()">GO!</button>
		`;
	},	
	MainModalOptions:function(data){
		return `
			<span class="close">&times;</span>
			<h1>Option not available</h1>
		`;
	},
	MainModalLanguaje:function(data){
		return `
			<button class="cs_tertiary main_button" onclick="Main.ChangeLanguage(0,this)"><img src="http://i64.tinypic.com/fd60km.png"/>English</button>
			<button class="cs_tertiary main_button" onclick="Main.ChangeLanguage(1,this)"><img src="http://i68.tinypic.com/avo5ky.png"/>Español</button>
		`;
	},
	RoomList:function(roomList){
		return `
			${roomList.map((room) => 
				`<button class="cs_tertiary main_button" onclick="Online.JoinRoom(${room.id})">${room.name}</button>
			`).join('')}	
		`;
	},
	Room:function(data){
		return `
			<div class="column">	
				<div class="panel ship_main">
					<h1 class="cell cs_tertiary title ship_main_name js_fleet_name">Room</h1>
				</div>
				<div class="panel ship_parts_container js_ship_parts_container">
					<p class="list_element cs_secondary map_fleet_name" onclick="GoToShip()">+Add player</p>
				</div>
			</div>
			<div class="column">
				<div  class=" panel ships_righttop">					
				</div>
				<div class="panel ships_rightbottom js_fleet_properties_container">
					<div id="chat"></div>				
					<input id="chat_input"></input>
					<button onclick="Room.SendChatMessage()">Send</button>
				</div>
			</div>
		`;
	},
	ChatMessage:function(data){
		return `<p class="chat_message ${data.me?'chat_message_me':''}">${data.nickname}: ${data.message}</p>`		
	},
	Battle:function(){
		return `<div id="battle_canvas" class="canvas battle_canvas"></div>`;
	},

}