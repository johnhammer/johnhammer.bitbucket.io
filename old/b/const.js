/* CONST */

var ShipSkills = {
	Placeholder1:'placeholder1', 
	Emp:'emp', 
	Glory:'faction units have bonus morale', 
	Projectile:'suicide attack', 
	Kamikaze:'bonus evasion and damage', 
	Missile:'bonus speed', 
	Bomb:'bonus damage', 
	Espionage:'can perform espionage missions', 
	PlanetBuster:'can destroy planets', 
	DoomsdayBomb:'can destroy planets', 
	FlyingBunker:'bonus fleet defence', 
	NeuralCenter:'bonus fleet intelligence', 
	BigEar:'bonus fleet perception', 
	RapidFire:'bonus against small ships', 
	FlakAA:'bonus against projectiles', 
	Carrier:'bonus fleet autonomy', 
	SpaceLab:'can perform scientific mission', 
	Colonizer:'can perform colonization mission', 
	Hackathon:'bonus fleet electronic attack', 
	DeepField:'bonus perception', 
	AntiRadiationGun:'bonus against enemy radios', 
	IntelligentFire:'bonus against enemy fuel and computers', 
	Artillery:'bonus against enemy armor', 
	ArmorPiercer:'bonus against big ships', 
	PrecisionBombardment:'bonus fleet precision', 
	DirtyBomb:'bonus against enemy humans', 
	AntimatterWeapon:'expansive blast', 
	Firewall:'bonus fleet electronic defence', 
	HeavyArmor:'defensive bonus against small ships', 
	Hacker:'electronic bonus against unmanned ships', 
	Bombardment:'bonus against surface objectives', 
	Manned:'bonus fleet intelligence'
}


var CONST = {
	factions : [							
		{
			name:'Empire',
			ships: [
				{name:'V3'				,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Zero'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:2},
				{name:'Bismark'			,Weapon:1,Ammo:2,Shield:1,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:4},
				{name:'Yamato'			,Weapon:2,Ammo:1,Shield:4,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:8},
			],
			fleets: [
				'Hakenkreuz','Ritterkreuz','Balkenkreuz','Eisenfaust','Reichsadler','Lanzenschaft','Eiserne kreuz','Amaterasu','Hachiman','Susanoo','Tsukuyomi','Ewigkeit','Ubermensch','Lebensraum','Leibstandarte','Kugelblitz','Uberriese','Weisserzwerg','Schwarzesloch','Roterriese','Nebelfleck','Eigenvektor','Bremsstrahlung','Zitterbewegung','Fujisan','Kusanagi','Yasakani','Tenno heika','Amanogawa','Taiyoukei','Ryuuseigun','Chikyuu'
			]
		},
		{
			name:'Union',
			ships: [
				{name:'Sputnik'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:1,Computer:0,Size:1},
				{name:'Soyuz'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:2,Radio:0,Computer:0,Size:2},
				{name:'Mir'				,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:2,Radio:1,Computer:1,Size:4},
				{name:'Lenin'			,Weapon:1,Ammo:2,Shield:0,Engine:2,Fuel:0,Habitat:2,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'Iosif Stalin','Vladimir Lenin','Fidel Castro','Che Guevara','Mao Zedong','Lei Feng','Josip Tito','Ho Chi Minh','Kim Il Sung','Karl Marx','Friedrich Engels','Valentina Tereshkova','Jiang Ging','Yuri Gagarin','Georgy Zhukov','Erich Weinert','Lin Biao','Zhou Enlai','Hua Guofeng','Liu Shaoqi','Karl Liebknecht','Rosa Luxemburg','Thomas Sankara','Charles Ruthenberg','Palmiro Togliatti','Florian Geyer','Louis Delescluze','Jaroslav Dombrowski','Guo Shoujing','Zhang Heng','Shen Kuo','Zhu Shijie'
			]
		},
		{
			name:'League',
			ships: [
				{name:'Anaximandres'	,Weapon:0,Ammo:0,Shield:1,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Tales'			,Weapon:1,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:2},
				{name:'Apeiron'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:2,Radio:0,Computer:2,Size:4},
				{name:'Cosmos'			,Weapon:1,Ammo:1,Shield:1,Engine:0,Fuel:0,Habitat:1,Radio:2,Computer:2,Size:8}
			],
			fleets: [
				'Thales','Anaximander','Anaximenes','Heraclitus','Anaxagoras','Archelaus','Democritus','Hypatia','Aristarchus','Autolycus','Euclid','Archimedes‎','Heron','Hippocrates','Eratosthenes','Empedocles','Plasma','Pneuma','Psyche','Thesis','Physis','Phos','Phon','Neuron','Neos','Hydor','Dynamis','Chronos','Biblos','Cyclos','Demos','Gramma'
			]
		},
		{
			name:'Califate',
			ships: [
				{name:'Buraq'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:1},
				{name:'Djinn'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:2,Size:2},
				{name:'Ifrit'			,Weapon:2,Ammo:1,Shield:0,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:4},
				{name:'Asadullah'		,Weapon:3,Ammo:2,Shield:2,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'Rahman','Raheem','Malik','Quddus','Salam','Mumin','Muhaymin','Aziz','Jabbaar','Mutakabbir','Khaaliq','Baari','Musawwir','Ghaffaar','Qahhaar','Wahhaab','Razzaaq','Fattaah','Alim','Qaabidh','Baasit','Khaafidh','Raafi','Muizz','Muzil','Sami','Basir','Hakam','Adl','Latif','Khabir','Halim'
			]
		},
		{
			name:'Republic',
			ships: [
				{name:'Lupa'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:1,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Annibal'			,Weapon:1,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:1,Size:2},
				{name:'Gladius'			,Weapon:1,Ammo:1,Shield:0,Engine:1,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:4},
				{name:'Iupiter'			,Weapon:1,Ammo:2,Shield:1,Engine:0,Fuel:0,Habitat:1,Radio:1,Computer:2,Size:8}
			],
			fleets: [
				'Germanica','Sabina','Augusta','Cyrenaica','Gallica','Macedonica','Scythica','Alaudae','Ferrata','Victrix','Claudia','Gemina','Hispana','Fretensis','Equestris','Fulminata','Antiqua','Libyca','Praetorica','Palatina','Iovia','Sagittaria','Martia','Italica','Herculia','Aquilaia','Camelopardia','Draconia','Centauria','Australia','Cygnia','Eridana'
			]
		},
		{
			name:'Dinasty',
			ships: [
				{name:'Singh'			,Weapon:1,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Kaur'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:2,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Nanak'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:1,Habitat:0,Radio:0,Computer:0,Size:4},
				{name:'Nirvana'			,Weapon:1,Ammo:4,Shield:2,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'Kesh','Kanga','Kachera','Kara','Kirpan','Mahadeva','Brahma','Krishna','Vishnu','Ganesha','Trimurti','Rama','Kartikeya','Buda','Dharma','Sangha','Zhuangzi','I-Ching','Daozang','Tao','Xiaojing','Kongzi','Ying-Yang','Han','Wei','Shu','Wu','Jin','Test','Test','Test','Test'
			]
		},
		{
			name:'Serpent',
			ships: [
				{name:'Tecaptl'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Atatl'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:0,Radio:0,Computer:1,Size:2},
				{name:'Macahuitl'		,Weapon:1,Ammo:3,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:4},
				{name:'Inca'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:1,Habitat:1,Radio:2,Computer:3,Size:8}
			],
			fleets: [
				'Ometecuhtli','Quetzalcoatl','Tezcatlipoca','Huitzilopochtli','XipeTotec','Tlaloc','Chalchiuhtlicue','Mixcoatl','Coatlicue','Xochiquetzal','Mictlantecuhtli','Tonatiuh','Inti','Mamaquilla','Pachamama','Viracocha','Supay','Kukulcan','Chaac','AhawKin','Ixbalanque','Yum Cimil','Ixtab','Yum Kaax','Popolvuh','Aztlan','Tawantinsuyu','Quechua','Coyotl','Tecolotl','Tzopilotl','Mayatl'
			]
		},
		{
			name:'Crown',
			ships: [
				{name:'Donau'			,Weapon:0,Ammo:1,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Landsknecht'		,Weapon:2,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Santamaria'		,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:1,Habitat:3,Radio:0,Computer:0,Size:4},
				{name:'Blas de lezo'	,Weapon:3,Ammo:2,Shield:1,Engine:0,Fuel:0,Habitat:2,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'Austria','Bohemia','Prusia','Bavaria','Holanda','Belgica','Flandes','Renania','Aragon','Castilla','Galicia','Leon','Granada','Cordobas','Valencias','Brabante','Lombardia','Flandes','Saboya','Napoles','Arauco','Algarve','Toledo','Navarra','Saavedra','Fuenclara','La Plata','Caracena','Mortora','Garciez','Zamora','Seralvo'
			]
		},
		{
			name:'Corporation',
			ships: [
				{name:'M1'				,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'M2'				,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'M3'				,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:4},
				{name:'M4'				,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'f1','f2','f3','f4','f5','f6','f7','f8','f9','f10','f11','f12','f13','f14','f15','f16','f17','f18','f19','f20','f21','f22','f23','f24','f25','f26','f27','f28','f29','f30','f31','f32'
			]
		},
		{
			name:'Confederation',
			ships: [
				{name:'Kropotkin'		,Weapon:0,Ammo:1,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Bakunin'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:1,Radio:1,Computer:0,Size:2},
				{name:'Tachanka'		,Weapon:4,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:4},
				{name:'Anarchy'			,Weapon:2,Ammo:1,Shield:0,Engine:0,Fuel:1,Habitat:2,Radio:2,Computer:0,Size:8}
			],
			fleets: [
				'Freiheit','Freedom','Libertad','Barcelona','Makhnovia','Libertalia','Ukrayina','Kowloon','Makhno','Catalonia','Sloboda','Eleftheria','Saoirse','Commune','Askartasuna','Batko','Utopia','Revolution','Durruti','Confederacion','Orwell','Cnt-Fai','Arbeiter','Lavorante','Proletaria','Partisana','Libereco','Hope','Lukto','Liberty','Esperanto','Dignity'
			]
		},
		{
			name:'States',
			ships: [
				{name:'Apollo'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:1,Radio:0,Computer:0,Size:1},
				{name:'Voayager'		,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:1,Habitat:0,Radio:1,Computer:0,Size:2},
				{name:'Hubble'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:4,Computer:0,Size:4},
				{name:'Hegemon'			,Weapon:2,Ammo:0,Shield:0,Engine:2,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'USF Protostar','USF Planet','USF Comet','USF Cluster','USF Moon','USF White','USF Neutron','USF Red','USF Star','USF Giant','USF Galaxy','USF Supergiant','USF Spiral','USF Elliptical ','USF Pulsar','USF Quasar','USF Supernova','USF Magnetar','USF Black','USF Hypergiant','USF Blue','USF Red','USF Yellow','USF Brown ','USF Dwarf','USF Carbon','USF Sun','USF System','USF Void','USF Supercluster','USF Remnant','USF Starburst'
			]
		},
		{
			name:'Colonies',
			ships: [
				{name:'Albion'			,Weapon:0,Ammo:0,Shield:1,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Endeavour'		,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:1,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Nelson'			,Weapon:1,Ammo:1,Shield:1,Engine:1,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:4},
				{name:'Napoleon'		,Weapon:3,Ammo:1,Shield:1,Engine:1,Fuel:1,Habitat:1,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'Hydrogen','Helium','Lithium','Beryllium','Boron','Carbon','Nitrogen','Oxygen','Fluorine','Neon','Sodium','Magnesium','Aluminum','Silicon','Phosphorus','Sulfur','Chlorine','Argon','Potassium','Calcium','Scandium','Titanium','Vanadium','Chromium','Manganese','Iron','Cobalt','Nickel','Copper','Zinc','Gallium','Germanium'
			]
		},
		{
			name:'Aliance',
			ships: [
				{name:'Impi'			,Weapon:1,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:1},
				{name:'Assegai'			,Weapon:0,Ammo:1,Shield:0,Engine:0,Fuel:1,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Zulu'			,Weapon:1,Ammo:0,Shield:1,Engine:0,Fuel:0,Habitat:2,Radio:0,Computer:0,Size:4},
				{name:'Haile selassie'	,Weapon:3,Ammo:1,Shield:0,Engine:0,Fuel:0,Habitat:2,Radio:2,Computer:0,Size:8}
			],
			fleets: [
				'Nile','Congo','Niger','Zambezi','Ubangi','Kasai','Orange','Limpopo','Senegal','Kilimanjaro','Kenya','Ngaliema','Rwenzori','Meru','Ras Dashen','Karisimbi','Semien','Virunga','Rwenzori','Sahara','Kalahari','Namib','Nubia','Madagascar','Bioko','Annobon','Socotra','Macaronesia','Guinea','Aden','Tadjoura','Winam'
			]
		},
		{
			name:'Khanate',
			ships: [
				{name:'Garuda'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:1,Size:1},
				{name:'Tengri'			,Weapon:0,Ammo:0,Shield:2,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Turan'			,Weapon:1,Ammo:1,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:2,Computer:0,Size:4},
				{name:'Gengis khan'		,Weapon:2,Ammo:1,Shield:0,Engine:3,Fuel:0,Habitat:1,Radio:1,Computer:0,Size:8}
			],
			fleets: [
				'Temujin','Tolui','Ogedei','Torgene','Guyuk','Oghul','Mongke','Ariq','Batu','Sartaq','Ulaghchi','Berke','Timur','Tuda','Takabuga','Toqta','Orda','Qun','Kochu','Bauyan','Sasibuqa','Ilbasan','Khwaja','Chimtay','Bumin','Muqan','Ilterish','Ozmis','Magyar','Hun','Hetumoger','Khazar'
			]			
		},
		{
			name:'Earldoms',
			ships: [
				{name:'Raven'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:1,Size:1},
				{name:'Iroq'			,Weapon:1,Ammo:0,Shield:1,Engine:0,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Valkirie'		,Weapon:1,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:1,Radio:1,Computer:1,Size:4},
				{name:'Ragnarok'		,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:2,Radio:1,Computer:4,Size:8}
			],
			fleets: [
				'Thor','Odin','Frigg','Balder','Tyr','Loki','Freya','Heimdall','Mohawk','Onondaga','Oneida','Cayuga','Seneca','Tuscarora','Wyandot','Erie','Susquehannock','Cherokee','Iagentci','Tharonhiawakon','Tawiskaron','Heng','Eithinoha','Akonwara','Deohako','Hawenneyu','Nanuq','Igaluk','Sedna','Anguta','Agloolik','Amarok'
			]
		},
		{
			name:'Kingdom',
			ships: [
				{name:'Horus'			,Weapon:0,Ammo:0,Shield:0,Engine:0,Fuel:0,Habitat:0,Radio:1,Computer:0,Size:1},
				{name:'Osiris'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:2},
				{name:'Tanit'			,Weapon:0,Ammo:0,Shield:0,Engine:1,Fuel:2,Habitat:0,Radio:1,Computer:0,Size:4},
				{name:'Amon ra'			,Weapon:2,Ammo:2,Shield:2,Engine:2,Fuel:0,Habitat:0,Radio:0,Computer:0,Size:8}
			],
			fleets: [
				'Cat','Ant','Monkey','Bat','Beetle','Bird','Bee','Camel','Cheetah','Cow','Crab','Crocodile','Zebra','Crane','Rat','Elephant','Duck','Fox','Fly','Moth','Lizard','Vulture','Goat','Grasshopper','Scorpion','Hyena','Ibis','Tiger','Lion','Pheasant','Rhinoceros','Mongoose'
			]
		}
	],	
}

