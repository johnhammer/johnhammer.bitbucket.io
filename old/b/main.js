/*LIBS*/

var Server = {
	url : "http://maps.x10host.com/other/entropyserver",	
	
	CreateRoom : function(roomname,value,callback){
		var request= new XMLHttpRequest()
		request.open("POST", Server.url+"/room.php?name="+roomname, true)
		request.setRequestHeader("Content-type", "application/json")
		request.onreadystatechange = function() {
			if (request.readyState === 4) {
				var response = request.response;
				callback(response);
			}
		}		
		request.send(LIB.Encrypt(value));
	},	
	CreatePetition : function(roomid,value){
		var request= new XMLHttpRequest()
		request.open("POST", Server.url+"/petition.php?room="+roomid, true)
		request.setRequestHeader("Content-type", "application/json")
		request.send(LIB.Encrypt(value));
	},
	GetRoomList : function(callback){
		var request= new XMLHttpRequest()
		request.open("POST", Server.url+"/roomlist.php", true)
		request.setRequestHeader("Content-type", "application/json")
		request.onreadystatechange = function() {
			if (request.readyState === 4) {
				var response = JSON.parse(request.response.replace(new RegExp('\\\\r\\\\n', 'g'), '#R#'));
				callback(response);
			}
		}
		request.send("");		
	},
	GetRoomPetitions : function(roomid,callback){
		var request= new XMLHttpRequest()
		request.open("POST", Server.url+"/petitionlist.php?room="+roomid, true)
		request.setRequestHeader("Content-type", "application/json")
		request.onreadystatechange = function() {
			if (request.readyState === 4) {
				var response = JSON.parse(request.response.replace(new RegExp('\\\\r\\\\n', 'g'), '#R#'));
				callback(response);
			}
		}
		request.send("");
	}
}


/*SCREENS*/

var Main = {
	init : function(){
		Memory.start();
		Style.SetTheme(rnd(0,15));		
		this.render();	
		Online.init();
	},
	render : function(){	
		js_main_container.innerHTML = TEMPLATING.Main();
	},
	GoToFactions : function(){
		Memory.set('color',colorpicker.value,0);
		document.location.href = "./factions.html";
	},	
	ChangeLanguage : function(lang,modal){
		Memory.set('language',lang);			
		this.render();
	},
	Menu : function(id){	
		var content = '';
		switch (id){
			case 0: 
				content = TEMPLATING.MainModalSinglePlayer();
				break;
			case 1: 
				content = TEMPLATING.MainModalMultiplayer();
				break;
			case 2: 
				content = TEMPLATING.MainModalTutorial();
				break;
			case 3: 
				content = TEMPLATING.MainModalOptions();
				break;
			case 4: 
				content = TEMPLATING.MainModalLanguaje();
				break;		
		}
		LIB.OpenModal(content);
	},
	OnlineMenu : function(id){	
		var content = '';
		switch (id){
			case 0: 
				Online.Create();
				Room.init();
				break;
			case 1: 
				Online.Join();
				Room.init();
				break;
			case 2: 
				Online.CreateManual();
				Room.init();
				break;
			case 3: 
				Online.JoinManual();
				Room.init();
				break;
		}
		LIB.OpenModal(content);
	}
}

var Room = {
	init : function(){
		js_main_container.innerHTML = TEMPLATING.Room();		
		setInterval(Online.Allow,5000);
	},
	SendChatMessage : function(){
		Online.SendMessage(chat_input.value);
		chat.innerHTML+=TEMPLATING.ChatMessage({
			nickname:'me',
			message:chat_input.value,
			me:true
		});
		chat_input.value='';	
	},
	ReceiveChatMessage : function(message){
		chat.innerHTML+=TEMPLATING.ChatMessage({
			nickname:'you',
			message:message,
			me:false
		});
	},
}



Main.init();

