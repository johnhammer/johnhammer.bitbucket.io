/*ONLINE*/

var Protocol = {
	
	Welcome : function(){ //host to players, new player entered room
		
	},
	Enter : function(){ //player to host, on enter room
		
	},
	Chat : function(){
		
	}
}


var Online = {
	pc1:null,
	pc2:null,
	sdpConstraints:null,
	activedc:null,
	currentRoom:null,
	roomList:null,
	
	useServer:true,
	
	Create : function(){
		dc1 = Online.pc1.createDataChannel('entropy', {reliable: true})
		Online.activedc = dc1
		dc1.onopen = function(e) { }
		dc1.onmessage = function(e) {
			var data = JSON.parse(e.data)
			Room.ReceiveChatMessage(data.message);
		}
		Online.pc1.createOffer(function(desc) {
			Online.pc1.setLocalDescription(desc, function() {}, function() {})
		}, function() { }, Online.sdpConstraints)
	},	
	Join : function(){	
		function callback(result){
			Online.roomList = result;
			content = TEMPLATING.Online.roomList(Online.roomList);
			LIB.OpenModal(content);		
		}	
		Server.GetOnline.roomList(callback);		
	},	
	CreateManual : function(){
		Online.useServer = false; 
		Online.Create();
	},
	JoinManual : function(){
		Online.useServer = false; 
		var room = prompt("Enter room info", "");		
		if (room != null) {
			var offerDesc = new RTCSessionDescription(JSON.parse(room))
			Online.pc2.setRemoteDescription(offerDesc)
			Online.pc2.createAnswer(function(answerDesc) {
				Online.pc2.setLocalDescription(answerDesc);
				
				
				
			},
			function () { },Online.sdpConstraints)
		}		
	},
	Allow : function(){
		function callback(petitionList){
			//for(var i=0;i<petitionList.length;i++){}		
			var answer = petitionList[0].text;
			if (answer != null) {
				var answerDesc = new RTCSessionDescription(JSON.parse(answer.replace(new RegExp('#R#', 'g'), '\\r\\n')))
				Online.pc1.setRemoteDescription(answerDesc);
				Protocol.Welcome();
			}		
		}	
		if(Online.useServer){
			Server.GetRoomPetitions(Online.currentRoom,callback);
		}
		else{
			var room = prompt("Enter player info", "");	
			if (room != null) {
				var answerDesc = new RTCSessionDescription(JSON.parse(room));
				Online.pc1.setRemoteDescription(answerDesc);
				Protocol.Welcome();
			}	
		}				
	},	
	FindRoomById : function(id){
		return Online.roomList.find(function(element) {
		  return element.id == id;
		}).text.replace(new RegExp('#R#', 'g'), '\\r\\n');
	},
	JoinRoom : function(id){	
		var room = Online.FindRoomById(id);	
		if (room != null) {
			Online.pc2.roomId = id;
			var offerDesc = new RTCSessionDescription(JSON.parse(room))
			Online.pc2.setRemoteDescription(offerDesc)
			Online.pc2.createAnswer(function(answerDesc) {
				Online.pc2.setLocalDescription(answerDesc)
			},
			function () { },
			Online.sdpConstraints)
		}
	},
	SendMessage : function(value) {
		if (value) {
			Online.activedc.send(JSON.stringify({message: value}));
		}
		return false;
	},
	init : function(){
		if (navigator.webkitGetUserMedia) {
			RTCPeerConnection = webkitRTCPeerConnection
		}

		var cfg = {'iceServers': [{'url': "stun:stun.gmx.net"}]},
		con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
		
		var sdpConstraints = {
			optional: [],
		}
		
		//pc 1
		Online.pc1 = new RTCPeerConnection(cfg, con), dc1 = null, tn1 = null, Online.activedc, Online.pc1icedone = false;
		
		Online.pc1.onicecandidate = function (e) {
			if (e.candidate == null) {
				var room = JSON.stringify(Online.pc1.localDescription);		
				var roomname = prompt("Roomname", "room1");		
				if(Online.useServer){
					Server.CreateRoom(roomname,room,function(result){Online.currentRoom=result});
				}			
				else{
					alert(room);
				}
			}
		}
		
		//pc 2
		Online.pc2 = new RTCPeerConnection(cfg, con), dc2 = null, Online.pc2icedone = false;

		Online.pc2.onicecandidate = function (e) {
			if (e.candidate == null) {
				var desc = JSON.stringify(Online.pc2.localDescription);
				if(Online.useServer){
					Server.CreatePetition(Online.pc2.roomId,desc);
				}
				else{
					alert(desc);
				}
			}
		}
		
		Online.pc2.ondatachannel = function (e) {
			var datachannel = e.channel || e;
			dc2 = datachannel
			Online.activedc = dc2
			dc2.onopen = function (e) { }
			dc2.onmessage = function (e) {		
				var data = JSON.parse(e.data)
				Room.ReceiveChatMessage(data.message);		
			}
		}	
	}
}