<?php
	require('lib.php');

    $conn = SqlConnect();

	$str_json = file_get_contents('php://input');
	$room = htmlspecialchars($_GET["room"]);
	
	$sql = "INSERT INTO petitions (room,text) VALUES (".$room.",'".mysqli_real_escape_string($conn,$str_json)."')";

	if ($conn->query($sql) === TRUE) {
		echo "New record created successfully";
	}
	else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}

	$conn->close();
?>