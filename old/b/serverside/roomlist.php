<?php
	require('lib.php');
	
	$conn = SqlConnect();
	
	$sql = "SELECT id,name,text FROM rooms";
	$maps= $conn->query($sql);
	$conn->close();

	$array = "";
	
	if ($maps->num_rows > 0) {
		$array.="[";
		while($row = $maps->fetch_assoc()) {
			$array.="{";
			$array.="\"id\":\"".$row["id"]."\",";
			$array.="\"name\":\"".$row["name"]."\",";
			$array.="\"text\":\"".str_replace("\"","\\\"",$row["text"])."\"";
			$array.="},";
		}
		$array = rtrim($array,",");
		$array.="]";
		echo $array;
	} 
	else {
		echo "{}"; 
	}
?>