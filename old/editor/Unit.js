/*

	Unit

*/

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


var Unit = function(_states,_destination = null, _spritesheet = GLOBAL.images.units){
	var private = {
		x: CONST.CellSizeXHalf,
		y: CONST.CellSizeYHalf,
		w: 20,
		h: 50,
		dir: CONST.DIR.l,
		image : 0,
		states : _states,
		state: _states.walk,
		spritesheet: _spritesheet,
		destination : _destination,
	}
	
	private.image = private.state.first;
	
	/*
		SPRITESHEET GENERATOR
		@id=x:@x,y:@y,w:@w,h:@h,fx:@fx,fy:@fy,fw:@fw,fh:@fh\n
		
		@x,@y,@w,@h,@fx,@fy,@fw,@fh,
		\n@loop	
	*/
	
	return{
		Draw: function(cellx,celly){			
			var image = private.image*8+private.dir*8;
			var x =UnitFactory.Array[image];
			var y =UnitFactory.Array[image+1];
			var w =UnitFactory.Array[image+2];
			var h =UnitFactory.Array[image+3];
			var fx =UnitFactory.Array[image+4];
			var fy =UnitFactory.Array[image+5];
			var fw =UnitFactory.Array[image+6];
			var fh =UnitFactory.Array[image+7];
			
			var startx = cellx + private.x - private.w/2;
			var starty = celly + private.y - private.h;
			
			Drawer.drawImage(startx,starty,private.spritesheet,x,y,w,h);
			
			if(private.state.multi){
				private.image += 4;
				if(private.image >= private.state.last*4){
					private.image = private.state.first;
				}
			}
			else{
				private.image += 1;
				if(private.image >= private.state.last){
					private.image = private.state.first;
				}
			}			
		},
		
		IsCenterOfCell : function(){
			return false; //private.x == CONST.CellSizeXHalf;
		},
		
		CalculateDirection : function(){
			switch (getRandomInt(1,4)){
				case 1: private.dir = CONST.DIR.u; return;
				case 2: private.dir = CONST.DIR.d; return;
				case 3: private.dir = CONST.DIR.l; return;
				case 4: private.dir = CONST.DIR.r; return;				
			}
		},
		
		Tick : function(){
			
			if(this.IsCenterOfCell()){
				this.CalculateDirection();
			}			
			
			var hstep = 2;
			var vstep = 1;
		
			if(private.dir == CONST.DIR.u){
				private.x -= hstep; 
				private.y -= vstep; 
			}
			else if(private.dir == CONST.DIR.d){
				private.x += hstep; 
				private.y += vstep; 
			}
			else if(private.dir == CONST.DIR.l){
				private.x -= hstep; 
				private.y += vstep; 
			}
			else if(private.dir == CONST.DIR.r){
				private.x += hstep; 
				private.y -= vstep; 
			}
		},
		
		OutToCell : function(){
			
			if(private.x<0 || private.x>CONST.CellSizeXHalf){
				if(private.dir == CONST.DIR.u){ 
					private.x= CONST.CellSizeXHalf;
					private.y= CONST.CellSizeYHalf;
					return{u:true};	
				}
				else if(private.dir == CONST.DIR.d){ 
					private.x= CONST.CellSizeXHalf;
					private.y= CONST.CellSizeYHalf;
					return{d:true}; 
				}
				else if(private.dir == CONST.DIR.l){ 
					private.x= CONST.CellSizeXHalf;
					private.y= CONST.CellSizeYHalf;
					return{l:true}; 
				}
				else if(private.dir == CONST.DIR.r){ 
					private.x= CONST.CellSizeXHalf;
					private.y= CONST.CellSizeYHalf;
					return{r:true}; 
				}
			}

		},
		
	}
}