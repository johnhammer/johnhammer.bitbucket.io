var DEBUGBODIES = [];

var LIB = {
	AddAnimation : function(object,name,frames,repeat=true,func=null,speed=10){
		anim = object.animations.add(name,frames,speed,repeat);
		if(func != null){	
			anim.onComplete.add(func, this);
		}
	},
	Clone : function(o){
		return JSON.parse(JSON.stringify(o));
	},
	Overlap : function (spriteA, spriteB) {	
		var boundsA = spriteA.getBounds();
		var boundsB = spriteB.getBounds();
		return Phaser.Rectangle.intersects(boundsA, boundsB);
	},
	KeyEvents : function(key,onDown,onUp=null){
		var d = game.input.keyboard.addKey(key);
		d.onDown.add(onDown, Player);
		if(onUp){
			d.onUp.add(onUp, Player);
		}
	}
}

var GameManager = {
	Start : function(){
		game.physics.startSystem(Phaser.Physics.ARCADE);
		
		EventHandler.Start();
		Player.Start();
		PlayerInterface.Start();
		Enemy.Start();
		EnemyInterface.Start();
		
		PlayerHitMask.Start();
		
		DEBUGBODIES.push(Player.sprite);
		DEBUGBODIES.push(Enemy.sprite);
	},
	Preload : function(){
		var assetsPath = "https://johnhammer.bitbucket.io/fight/assets/";
		game.load.spritesheet('agol', assetsPath+'ezgif-2-eabe19f05d.png', 500, 400);
		game.load.spritesheet('flame', 		assetsPath+'smallflame.png', 80, 36);
	},
	Update : function(){
		
		if (game.input.activePointer.isDown)
		{
			var flame = game.add.sprite(game.input.mousePointer.x,game.input.mousePointer.y, 'flame');
			LIB.AddAnimation(flame,'right', [0, 1, 2, 3, 4, 5, 6, 7,8,9], false, function(){flame.destroy()},8);
			flame.animations.play('right');
		}
		
		if(LIB.Overlap(PlayerHitMask.sprite, Enemy.sprite)){
			Enemy.TakeDamage();
			PlayerHitMask.Destroy();
		}		
	},
	Render : function(){
		for(var i = 0; i<DEBUGBODIES.length;i++){
			game.debug.body(DEBUGBODIES[i]);
		}
	}
}

var EventHandler = {
	Start : function(){
		LIB.KeyEvents(Phaser.Keyboard.SPACEBAR,Player.ActionHit);
		LIB.KeyEvents(Phaser.Keyboard.D,Player.ActionRight,Player.ActionRightOver);
		LIB.KeyEvents(Phaser.Keyboard.A,Player.ActionLeft,Player.ActionLeftOver);
	},
}

var PlayerStates = {
	idle : 0,
	moving : 1
}

var Player = {
	
	sprite : null,
	state : PlayerStates.idle,
	lookingRight : false,
	
	Start : function(){
		this.sprite = game.add.sprite(20, game.world.height - 200, 'agol');
		
		this.sprite.scale.setTo(.5,.5);
		game.physics.arcade.enable(this.sprite);
		this.sprite.anchor.setTo(.5,1);
		this.sprite.body.setSize(80, 240);
		
		LIB.AddAnimation(this.sprite,'idle',[3]);
		LIB.AddAnimation(this.sprite,'walk',[15,16,17]);
		LIB.AddAnimation(this.sprite,'hit',[3,4,5,6],false,function(){this.ActionEndOfHit()}.bind(this));
		
		this.sprite.animations.play('idle');
	},
	
	ActionRight : function(){
		if(this.state == PlayerStates.idle){
			if(!this.lookingRight){
				this.lookingRight = true;
				this.sprite.scale.x *= -1;
			}
			this.state = PlayerStates.moving;	
			this.sprite.animations.play('walk');
			this.sprite.body.velocity.x = 100;
		}
	},
	
	ActionRightOver : function(){
		if(this.state == PlayerStates.moving){
			this.state = PlayerStates.idle;	
			this.sprite.animations.play('idle');
			this.sprite.body.velocity.x = 0;
		}
	},
	
	ActionLeft : function(){
		if(this.state == PlayerStates.idle){
			if(this.lookingRight){
				this.lookingRight = false;
				this.sprite.scale.x *= -1;
			}
			this.state = PlayerStates.moving;	
			this.sprite.animations.play('walk');
			this.sprite.body.velocity.x -= 100;
		}
	},
	
	ActionLeftOver : function(){
		if(this.state == PlayerStates.moving){
			this.state = PlayerStates.idle;	
			this.sprite.animations.play('idle');
			this.sprite.body.velocity.x = 0;
		}
	},
	
	ActionHit : function(){
		if(this.state == PlayerStates.idle || this.state == PlayerStates.moving){
			this.state = PlayerStates.hit;	
			this.sprite.animations.play('hit');
			this.sprite.body.velocity.x = 0;
			PlayerHitMask.Create(
				this.sprite.position.x-100*(this.lookingRight ? -1 : 1),
				this.sprite.position.y-100);
		}
	},
	
	ActionEndOfHit : function(){
		if(this.state == PlayerStates.hit){
			this.state = PlayerStates.idle;	
			this.sprite.animations.play('idle');
			this.sprite.body.velocity.x = 0;
			PlayerHitMask.Destroy();
		}
	},
}

var EnemyStates = {
	idle : 0,
	moving : 1
}

var Enemy = {
	
	sprite : null,
	state : EnemyStates.idle,
	
	Start : function(){
		this.sprite = game.add.sprite(500, game.world.height - 200, 'agol');
		
		this.sprite.scale.setTo(.5,.5);
		game.physics.arcade.enable(this.sprite);
		this.sprite.anchor.setTo(.5,1);
		this.sprite.body.setSize(80, 240);
		
		LIB.AddAnimation(this.sprite,'idle',[3]);
		LIB.AddAnimation(this.sprite,'walk',[15,16,17]);
		
		this.sprite.animations.play('idle');
	},
	
	ActionRight : function(){
		if(this.state == EnemyStates.idle){
			this.state = EnemyStates.moving;	
			this.sprite.animations.play('walk');
			this.sprite.body.velocity.x = 100;
		}
	},
	
	ActionRightOver : function(){
		if(this.state == EnemyStates.moving){
			this.state = EnemyStates.idle;	
			this.sprite.animations.play('idle');
			this.sprite.body.velocity.x = 0;
		}
	},
	
	TakeDamage : function(){
		EnemyInterface.UpdateHealth(-10);
	},
}

var PlayerInterface = {
	healtbar : null,
	maxhealth : 100,
	health : 100,
	
	Start : function(){
		var bmd = game.add.bitmapData(64,8);
		bmd.ctx.beginPath();
		bmd.ctx.rect(0,0,64,8);
		bmd.ctx.fillStyle = '#ffffff';
		bmd.ctx.fill();

		this.healthbar = game.add.sprite(20,20,bmd);
		this.healthbar.anchor.y = 0.5;
	},
	
	UpdateHealth : function(val){
		this.health += val;
		this.healthbar.width = 64*(this.health/this.maxhealth);
	},
}

var EnemyInterface = {
	healtbar : null,
	maxhealth : 100,
	health : 100,
	
	Start : function(){
		var bmd = game.add.bitmapData(64,8);
		bmd.ctx.beginPath();
		bmd.ctx.rect(0,0,64,8);
		bmd.ctx.fillStyle = '#ffffff';
		bmd.ctx.fill();

		this.healthbar = game.add.sprite(500,20,bmd);
		this.healthbar.anchor.y = 0.5;
		this.healthbar.anchor.x = 1;
	},
	
	UpdateHealth : function(val){
		this.health += val;
		this.healthbar.width = 64*(this.health/this.maxhealth);
	},
}

var PlayerHitMask = {
	sprite : null,
	
	Start : function(){
		this.sprite = game.add.sprite(-500,-500);
		game.physics.arcade.enable(this.sprite);
		this.sprite.body.setSize(20, 20);
		DEBUGBODIES.push(this.sprite);
		
		return this;
	},
	
	Create : function(x,y){
		this.sprite.reset(x,y);
	},
	
	Destroy : function(){
		this.sprite.reset(-500,-500);
	}
}


var game = new Phaser.Game(800, 400, Phaser.CANVAS, '', {
	preload	: GameManager.Preload,
	create	: GameManager.Start,
	update	: GameManager.Update,
	render	: GameManager.Render
}, false, false);





/*


var debug = false;

var hotkey;

function render(){
	if(debug){
		game.debug.body(player);
	}
}

function preload() {
	var assetsPath = "https://johnhammer.bitbucket.io/fight/assets/";
	
	
	game.load.spritesheet('agol', 		assetsPath+'ezgif-2-eabe19f05d.png', 500, 400);
	game.load.spritesheet('uryu', 		assetsPath+'ezgif-2-210139d6d8.png', 500, 500);
	game.load.spritesheet('platform', 	assetsPath+'platform.png', 100, 80);
	game.load.spritesheet('flame', 		assetsPath+'flame.png', 200, 200);
	game.load.spritesheet('flama', 		assetsPath+'flama.png', 40, 170);
}



var ObjectFactory = {
	CreatePlayer : function(){
		player = game.add.sprite(30, game.world.height - 400, 'agol');
		game.physics.arcade.enable(player);
		player.body.gravity.y = 370;
		player.body.collideWorldBounds = true;
		
		AnimationHelper.AddAnimation(player,'stand',[3]);
		AnimationHelper.AddAnimation(player,'air',[0]);
		AnimationHelper.AddAnimation(player,'walk',[15,16,17]);
		AnimationHelper.AddAnimation(player,'hit1',[3,4,5,6],false,PlayerHelper.AnimationStopped);
		AnimationHelper.AddAnimation(player,'hit2',[6,7,8,9,10],false,PlayerHelper.AnimationStopped);

		player.goesRight = false; 
		player.scale.setTo(.5,.5);  
		player.anchor.setTo(.5,.5);  
		player.body.setSize(100/2, 225, 0, 75/2);
		
		
		flama = game.add.sprite(30, game.world.height - 400, 'flama');
		flama.scale.setTo(.5,.5);  
		AnimationHelper.AddAnimation(flama,'burn',[0,1,2,3,4,5,6,7,8,9],true,null,5);
		flama.animations.play('burn');
	},
	CreateEnemy : function(){
		enemy = game.add.sprite(300, game.world.height - 400, 'uryu');
		game.physics.arcade.enable(enemy);
		enemy.body.gravity.y = 370;
		enemy.body.collideWorldBounds = true;
		
		AnimationHelper.AddAnimation(enemy,'stand',[3]);
		AnimationHelper.AddAnimation(enemy,'die',[6,7,8,9,10],false,PlayerHelper.AnimationStopped);

		enemy.goesRight = false; 
		enemy.scale.setTo(.5,.5);  
		enemy.anchor.setTo(.5,.5);  
		enemy.body.setSize(100/2, 118, 0, 75/2);
	},
	CreateFlamewave : function(x,y,mirror){
		var flame = game.add.sprite(x,y, 'flame');
		AnimationHelper.AddAnimation(flame,'right', [0, 1, 2, 3, 4, 5, 6, 7], false, function(){PlayerHelper.DestroyObject(flame)},8);
		flame.animations.play('right');
		if(mirror){
			flame.scale.x *= -1;
		}
	}
}

var PlayerHelper = {
	AnimationStopped : function(){
		player.blocker = false;
	},
	DestroyObject : function(obj){
		obj.destroy();
	}
	
}


function mouseLeft(){
	if (game.input.mousePointer.isDown) {		
		ObjectFactory.CreateFlamewave(game.input.mousePointer.x, game.input.mousePointer.y-160,!player.goesRight);
	}
}

function create() {
	var url = new URL(window.location.href);
	debug = url.searchParams.get("debug");		
	
	
	game.input.onDown.add(mouseLeft, this);
	
	
	Phaser.Canvas.setImageRenderingCrisp(game.canvas)
	game.scale.pageAlignHorizontally = true;
	game.scale.pageAlignVertically = true
	game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.physics.startSystem(Phaser.Physics.ARCADE);
	game.stage.backgroundColor = '#5c94fc';

	ObjectFactory.CreatePlayer();
	ObjectFactory.CreateEnemy();

	platform = game.add.sprite(100, game.world.height - 20, 'platform');
	game.physics.arcade.enable(platform);
	platform.body.immovable = true;

	game.camera.follow(player);

	cursors = game.input.keyboard.createCursorKeys();

	hotkey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
}

function update() {	
	game.physics.arcade.collide(player, platform);
	game.physics.arcade.collide(enemy, platform);

	if (player.body.enable && !player.blocker) {
		player.body.velocity.x = 0;
		if (cursors.left.isDown) {
			player.body.velocity.x = -200;

			if(player.goesRight){
				player.scale.x *= -1;
			}

			player.animations.play('walk');
			player.goesRight = false;
		} 
		else if (cursors.right.isDown) {
			player.body.velocity.x = 200;
	  
			if(!player.goesRight){
				player.scale.x *= -1;
			}
	  
			player.animations.play('walk');
			player.goesRight = true;
		} 
		else {
			//player.animations.stop();
			player.animations.play('stand');
		}

		if(player.body.onFloor()){
			if (cursors.up.isDown) {
				player.body.velocity.y = -190;
				player.animations.stop();
			}
			else if (hotkey.isDown) {
				player.blocker = true;
				player.body.velocity.x = 0;
				player.animations.play('hit1');

				var x;
				if(player.goesRight){
					x = player.position.x+25;
				}
				else{
					x = player.position.x-25;
				}
				ObjectFactory.CreateFlamewave(x, player.position.y-100,!player.goesRight);
				
				
			}
		}
    

		if (player.body.velocity.y != 0) {
			player.animations.play('air');
		}
  }
  
  
	if(player.goesRight){
		flama.position.x = player.position.x-75;
	}
	else{
		flama.position.x = player.position.x+60;
	}
	flama.position.y = player.position.y-75;
}

*/