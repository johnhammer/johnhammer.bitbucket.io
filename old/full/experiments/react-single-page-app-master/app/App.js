import React from 'react';
import Header from './Header';

class App extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default App;