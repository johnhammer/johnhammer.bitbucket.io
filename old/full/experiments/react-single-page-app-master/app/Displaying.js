import React from 'react';

class Displaying extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.total == 0){
            var text = 'No results';
        }
        else{
            var text =  'Displaying ' + this.props.from + ' - ' + this.props.to+ ' of ' + this.props.total + ' Results';
        }
        return (
            <div className="display-bar">
                {text}
            </div>
        );

    }
}

export default Displaying;
