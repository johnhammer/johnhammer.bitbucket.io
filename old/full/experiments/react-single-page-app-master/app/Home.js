import React from 'react';
import $ from "jquery";
import _ from "underscore";
import Displaying from "./Displaying";
import Pagination from "./Pagination";
var carz = require('../cars.json');
import { Router, Route, IndexRoute, IndexLink, Link }  from 'react-router';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = { cars: [], page : 1, search: ""};
    }

    fetchCars(){
        debugger
        this.setState({cars : carz.cars, page: this.state.page, search: this.state.search});
    }

    handleSearchChange() {
        this.setState({cars : this.state.cars, page: 1, search: this.refs.search.value});
    }

    componentDidMount() {
        $(window).scroll(function() {
            if (document.body.scrollTop === 0) {
                $('.back-to-top').fadeOut();
            } else {
                $('.back-to-top').fadeIn();
            }
        });
        this.fetchCars();
    }

    goToPage(pageNumber){
        this.setState({cars : this.state.cars, page: pageNumber, search: this.state.search});
        this.scrollToTop();
    }

    scrollToTop(){
        $('html,body').animate({ scrollTop: 0 }, 'slow');
    }

    getSearchResults(){
        if(this.state.search === ""){
            return this.state.cars;
        }

        var search = this.refs.search.value;
        return _.filter(this.state.cars, function(car){
            return car.name.toLowerCase().startsWith(search) || car.name.toUpperCase().startsWith(search);
        });
    }

    renderDisplaying() {
        var from = (this.state.page - 1) * 10 + 1;
        var to = this.isLastPage() ? this.getSearchResults().length : from + 9;
        var total = this.getSearchResults().length;
        return (
            <Displaying from={from} to={to} total={total}/>
        );
    }

    isLastPage(){
        return (this.state.page + 1) > Math.ceil(this.getSearchResults().length /10);
    }

    renderCars(){
        var  realThis = this;
        var offset = (this.state.page - 1) * 10;
        var results = this.getSearchResults();
        var arr = results.slice(offset, offset + 10);
        return _.map(arr, function(car,index) {
            var target = "/car/" + car.id;
            return (
                <Link className="car-clickable" to={target}>
                    <div className="car-wrapper white-background">
                        <div className="car-image">
                            <img src={car.main_picture}/>
                        </div>
                        <div className="car-details">
                            <div className="car-name">{car.name}</div>
                            <div className="car-price">{car.price}</div>
                            <div className="car-location">
                                <i className="font-icon icon-location-alt"></i>
                                {car.location}
                            </div>
                        </div>
                    </div>
                </Link>
            );
        });
    }

    render() {
        return (
            <div>
                <div className="page-wrapper">
                    <input
                        type="text"
                        ref="search"
                        name="search"
                        defaultValue=""
                        placeholder="search"
                        onChange={this.handleSearchChange.bind(this)}
                    />
                    {this.renderDisplaying()}
                    <div>
                        {this.renderCars()}
                    </div>
                    <Pagination goToPage={this.goToPage.bind(this)} currentPage={this.state.page}
                                numberOfPages={Math.ceil(this.getSearchResults().length / 10)} />
                </div>
                <div className="back-to-top" onClick={this.scrollToTop.bind(this)}>
                    <a><i className="font-icon icon-angle-up"></i></a>
                </div>
            </div>

        );
    }
}

export default Home;
