import React from 'react'

export default class Pagination extends  React.Component{
    constructor(props){
        super(props);
    }

    goToPage(pageNumber){
        this.props.goToPage(pageNumber.i);
    }

    handlePrevNext(pageNumber){
        this.props.goToPage(pageNumber);
    }

    renderButtons(){
        const currentPage = this.props.currentPage;
        const numberOfPages = this.props.numberOfPages;
        var buttons = [];

        if(currentPage > 4){
            buttons.push(<span onClick={this.goToPage.bind(this, {i})} key={Math.random()}> 1 </span>);
            buttons.push(<span className="dotdot" key={Math.random()}>....</span>);
        }

        for (var i = Math.max(1,currentPage - 3); i <= currentPage; i++) {
            var classActive="";
            if(i == currentPage){
                classActive="active";
            }
            buttons.push(<span className={classActive} onClick={this.goToPage.bind(this, {i})} key={i}> {i} </span>);
        }
        if(currentPage < numberOfPages - 1){
             buttons.push(<span className="dotdot" key={Math.random()}>....</span>);
         }

        for (var i = numberOfPages - 1 > currentPage ? numberOfPages - 1 : numberOfPages ; i <= numberOfPages && numberOfPages > 0; i++) {
            buttons.push(<span onClick={this.goToPage.bind(this, {i})} key={i}> {i} </span>);
        }

        return buttons;
    }

    renderPrevNextButtons(){
        let prevButtonText = 'Previous button';
        let nextButtonText = 'Next button';
        var prev = this.props.currentPage > 1 ?
            <div className="btn btn-prev" onClick={this.handlePrevNext.bind(this, this.props.currentPage - 1)}>
                <i className="font-icon icon-angle-left"></i>
                {prevButtonText}
                </div> : "";
        var next = this.props.currentPage >=  this.props.numberOfPages ? "" :
            <div className="btn btn-next" onClick={this.handlePrevNext.bind(this, this.props.currentPage + 1)}>
                {nextButtonText}
                <i className="font-icon icon-angle-right"></i>
                </div>;
        return (
            <div>
                {prev}
                {next}
            </div>
        );
    }

    render() {
        if(this.props.numberOfPages > 0){
            return (
                <div className="pagination-wrapper">
                    <div className="pagination-numbers">
                        {this.renderButtons()}
                    </div>
                    <div className="pagination-buttons">
                        {this.renderPrevNextButtons()}
                    </div>
                </div>
            );
        }else{
            return ( <div></div>);
        }
    }
}