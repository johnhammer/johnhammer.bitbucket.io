import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Home from './Home';
import createBrowserHistory from 'history/createBrowserHistory';
import Car from './Car';
import { Router,
    Route,
    IndexRoute,
    browserHistory,
    Link } from 'react-router';



// Filterable CheatSheet Component
ReactDOM.render(
    <Router >
        <Route path="/" component={App}>
            <IndexRoute component={Home}/>
            <Route path="/car/:id" component={Car} />
        </Route>
    </Router>,
    document.getElementById('container')
);

