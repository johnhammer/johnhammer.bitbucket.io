/*

Map & Cell

*/

var Cell = function(_image,_x,_y,_xid,_yid){
	var private = {
		image: _image,
		maskImage : false,
		x: _x,
		y: _y,
		xid: _xid,
		yid: _yid,
		building : null,
		units : [],
	}	

	function GetPoints(size){
		return Math.trunc(size/30);
	}
	
	return {
		Draw: function(){

			if(private.image){		
				var image = private.image;
				image*=5;
				var x =BuildingFactory.Array[image];
				var y =BuildingFactory.Array[image+1];
				var w =BuildingFactory.Array[image+2];
				var h =BuildingFactory.Array[image+3];
				var points = BuildingFactory.Array[image+4]; //GetPoints(w);
				
				var px = private.x;
				var py = private.y;
				
				var extrax = Math.floor((points+2)/2);
				var extray = (points+1)/2;

				var nextx = px - w+(extrax)*CONST.CellSizeX;
				var nexty = py - h+(extray)*CONST.CellSizeY;			
				
				Drawer.drawImage(nextx,nexty,GLOBAL.images.spritesheet,x,y,w,h);			
			} 
			
			for(var i = 0; i < private.units.length; i++){
				var unit = private.units[i];
			
				unit.Draw(private.x,private.y);
				unit.Tick();
				var out = unit.OutToCell();
				if(out){
					var cell = null;
					
					var annex = Map.GetAnnexCells(this);
					
					if(out.u){
						cell = annex.u;
					}                 
					else if(out.d){   
						cell = annex.d;
					}                 
					else if(out.l){   
						cell = annex.l;
					}                 
					else if(out.r){   
						cell = annex.r;
					}
					if(cell){
						cell.AddUnit(unit);
						private.units.splice(i,1);
					}
				}
			}		
			
		},
		DrawMask: function(){
			if(private.maskImage) {
				if(this.CanBuild()){
					var image = BuildingFactory.Images.cell;
					image*=5;
					var x =BuildingFactory.Array[image];
					var y =BuildingFactory.Array[image+1];
					var w =BuildingFactory.Array[image+2];
					var h =BuildingFactory.Array[image+3];
					var points = BuildingFactory.Array[image+4]; //GetPoints(w);
					
					var px = private.x;
					var py = private.y;
					
					var extrax = Math.floor((points+2)/2);
					var extray = (points+1)/2;

					var nextx = px - w+(extrax)*CONST.CellSizeX;
					var nexty = py - h+(extray)*CONST.CellSizeY;	
					
					Drawer.drawMask(nextx,nexty,GLOBAL.images.spritesheet,x,y,w,h);
				}
			}	
		},
		GetPosId : function(){
			return {x:private.xid, y:private.yid};
		},
		SetImage: function(image){
			private.image = image;
		},
		SetBuilding: function(building){
			private.building = building;
		},
		CanBuild: function(){
			return private.building == null;
		},
		StartMask: function(){
			private.maskImage = true;
		},
		StopMask: function(){
			private.maskImage = false;
		},
		AddUnit : function(unit){
			private.units.push(unit);
		}
		
	}
};

var Map = {
	private : {
		matrix: [],
		GetCell : function(x,y){
			if(x>=this.SizeX || x<0 || y>=this.SizeY || y<0) return null;
			
			return this.matrix[y*this.SizeX+x];
		},
		SizeX : 0,
		startx : 0,
		starty : 0,
		PathGrid : null,
	},

	Init: function(sizeX,sizeY,startx,starty){
		var SizeY = sizeX;
		var SizeX = sizeY;
		//this.private.PathGrid = new PF.Grid(sizeX,sizeY); 
		
		this.private.SizeX = sizeX;
		this.private.startx = startx;
		this.private.starty = starty;
		
		for(var y = 0; y < SizeY; y++){
			for(var x = 0; x < SizeX; x++){
				var DrawX = ((x - y) * CONST.CellSizeXHalf) + this.private.startx;
				var DrawY = (x + y) * CONST.CellSizeYHalf + this.private.starty;
				
				var cell = new Cell(BuildingFactory.Images.land,DrawX,DrawY,x,y);
				this.private.matrix.push(cell);
			}
		}
	},
	Draw: function(){
		var size = this.private.SizeX;
		var x, y;
		for(i=0;i<size;i++){
			x = 0;
			y = i;
			for(j=0;j<i+1;j++){
				var cell = this.private.GetCell(x,y);
				cell.Draw();
				x+=1;
				y-=1;
			}
		}
		for(i=size-1;i>0;i--){
			x = size-i;
			y = size-1;
			for(j=i;j>0;j--){
				var cell = this.private.GetCell(x,y);
				cell.Draw();
				x+=1;
				y-=1;
			}
		}
		
		for(var h = 0; h < this.private.matrix.length; h++){
			var cell = this.private.matrix[h];
			cell.DrawMask();
		}	
	},
	GetAnnexCells: function(cell){
		var posId = cell.GetPosId();
		return {
			u : this.private.GetCell(posId.x-1,posId.y),
			d : this.private.GetCell(posId.x+1,posId.y),
			r : this.private.GetCell(posId.x,posId.y-1),
			l : this.private.GetCell(posId.x,posId.y+1),
		}
	},	
	GetCellAtPos: function(ScreenX,ScreenY){
		ScreenX = ScreenX - this.private.startx - CONST.CellSizeXHalf + GLOBAL.OffsetX;
		ScreenY = ScreenY - this.private.starty + GLOBAL.OffsetY;
		
		var TileX = Math.trunc((ScreenY / CONST.CellSizeY) + (ScreenX / CONST.CellSizeX));
		var TileY = Math.trunc((ScreenY / CONST.CellSizeY) - (ScreenX / CONST.CellSizeX));
		
		return this.private.GetCell(TileX,TileY);
	},
	
};

