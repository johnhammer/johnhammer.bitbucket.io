var IMPLgen1 = {
	Population : [],
	Settings : {
		Individuals:50
	},
	Json : null,
	GenerateRandomIndividual(){
		var code = [];
		for(var i=0;i<INSTRUCTIONS;i++){
			code.push(getRandomLine());
		}		
		return {
			c:code,
			f:this.GetFitness(code),
		};
	},
	GetFitness(code){
		var score = 0;
		var f = getCode(code,this.Json);
		for(var j=0;j<this.Json.length;j+=1){			
			var val = f.apply(this,this.Json[j].i);
			for(var i=0;i<this.Json[j].o.length;i++) {
				if(val[i]==this.Json[j].o[i]) score++;
			}
		}
		return score;
	},
	SortPopulation(){
		this.Population.sort((a,b)=>b.f-a.f);
	},
	MutatePopulation(){
		//todo
		this.Population = [];
		for(var i=0;i<this.Settings.Individuals;i++){
			this.Population.push(this.GenerateRandomIndividual());
		}
	},
	
	START : function(json){
		this.Json = json;
		for(var i=0;i<this.Settings.Individuals;i++){
			this.Population.push(this.GenerateRandomIndividual());
		}
	},	
	RUN : function(){
		this.MutatePopulation();
		this.SortPopulation();		
		var result = this.Population[0];		
		return result.c;		
	},
}