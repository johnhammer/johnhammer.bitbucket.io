//lib
function wChoice(array) {    
	var val = getRandomInt(0,100);
	var sum = 0;
	for(var i=0;i<array.length;i++){
		sum += array[i].w;
		if(val<=sum){
			return array[i].v;
		}
	}
}
function Clone(obj){
	return JSON.parse(JSON.stringify(obj));
}
function Chance(chance){
    var val = getRandomInt(chance,1);
    return val == chance;
}
function getRandomInt(max,min=0) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
var randomProperty = function (obj) {
    var keys = Object.keys(obj)
    return obj[keys[ keys.length * Math.random() << 0]];
}
function randomFromArray(myArray){
	return myArray[Math.floor(Math.random() * myArray.length)];
}
function getVariable(num){
	return "variable" + num;
}
function noOp(){
	return "";
}
function zeroOp(a,b){
	return getVariable(a) + "=" + getVariable(b);
}
function unaryOp(operator,a,b){
	return getVariable(a) + "=" + operator + getVariable(b);
}
function binaryOp(operator,a,b,c){
	return getVariable(a) + "=" + getVariable(b) + operator + getVariable(c);
}
function code(f,a = null,b = null,c = null){
	var ret = {
		f:f
	};
	if(a != null) ret.a = a;
	if(b != null) ret.b = b;
	if(c != null) ret.c = c;
	return ret;
}

function compareOutput(val,o){
	for(var i=0;i<o.length;i++) {
		if(val[i]!=o[i]) return false;
	}
	return true;
}
function dotproduct(a,b) {
    var n = 0, lim = Math.min(a.length,b.length);
    for (var i = 0; i < lim; i++) n += a[i] * b[i];
    return n;
 }

function norm2(a) {if(!a.length)return false;var sumsqr = 0; for (var i = 0; i < a.length; i++) sumsqr += a[i]*a[i]; return Math.sqrt(sumsqr);}

function cosinesim(x, y) {
    xnorm = norm2(x);
    if(!xnorm) return 0;
    ynorm = norm2(y);
    if(!ynorm) return 0;
    return dotproduct(x, y) / (xnorm * ynorm);
}