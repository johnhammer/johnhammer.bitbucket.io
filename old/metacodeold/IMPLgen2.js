var IMPLgen2 = {
	Population : [],
	Settings : {
		Individuals:50,
		AddMutation:100,
		RemoveMutation:100,
		SwapMutation:3,
		Reproduction:1
	},
	WEIGHTS : [
		{v:FUNCTIONS.add,w:5},
		{v:FUNCTIONS.subtract,w:5},
		{v:FUNCTIONS.multiply,w:5},
		{v:FUNCTIONS.divide,w:5},
		{v:FUNCTIONS.equal,w:10},
		{v:FUNCTIONS.greater,w:5},
		{v:FUNCTIONS.and,w:5},
		{v:FUNCTIONS.or,w:5},
		{v:FUNCTIONS.not,w:5},
		{v:FUNCTIONS.if,w:10},
		{v:FUNCTIONS.loop,w:10},
		{v:FUNCTIONS.break,w:10},
		{v:FUNCTIONS.push,w:20},
	],
	GetRandomLine (){
		var type = wChoice(this.WEIGHTS);
		return {
			f:type,
			a:getRandomInt(AUXILIARS+PARAMETERS-1,0),
			b:getRandomInt(AUXILIARS+PARAMETERS-1,0),
			c:getRandomInt(AUXILIARS+PARAMETERS-1,0)
		}
	},
	Json : null,
	GenerateRandomIndividual(){
		var code = [];
		for(var i=0;i<INSTRUCTIONS;i++){
			code.push(this.GetRandomLine());
		}		
		return {
			c:code,
			f:this.GetFitness(code),
		};
	},
	GetFitness(code){
		var score = 0;
		
		var exactMatch = 0;
		var aproxMatch = 0;
		var cosine = 0;
				
		var f = getCode(code,this.Json);
		for(var j=0;j<this.Json.length;j+=1){			
			var val = f.apply(this,this.Json[j].i);
			
			cosine+=cosinesim(val,this.Json[j].o);
			
			for(var i=0;i<this.Json[j].o.length;i++) {
				if(val[i]==this.Json[j].o[i]) exactMatch++;
			}
			for(var i=0;i<this.Json[j].o.length;i++) {
				if(val.find(x=>x==this.Json[j].o[i])) aproxMatch++;
			}
		}		
		return exactMatch*10000 + aproxMatch*100 + cosine; //- 1/code.length*0.000001; //extra optimization
	},
	SortPopulation(){
		for(var i=0;i<this.Population.length;i++){
			this.Population[i].f = this.GetFitness(this.Population[i].c);
		}
		this.Population.sort((a,b)=>b.f-a.f);
	},
	MutatePopulation(){
		for(var i=0;i<this.Population.length;i++){
			this.MutateIndividual(this.Population[i]);			
		}
	},
	ReproducePopulation(){
		for(var i=0;i<this.Settings.Individuals;i++){
			if(Chance(this.Settings.Reproduction)){
				this.Population.push(Clone(this.Population[i]));
			}			
		}
	},
	KillPopulation(){
		this.Population = this.Population.slice(0,this.Settings.Individuals);
	},
	GenAlgStep(){		
		this.KillPopulation();			
		this.ReproducePopulation();
		this.MutatePopulation();
		this.SortPopulation();
	},	
	MutateIndividual(individual){
		while(Chance(this.Settings.AddMutation)){
			var index = getRandomInt(individual.c.length-1);
			individual.c.splice(index,0,this.GetRandomLine());
		}
		while(Chance(this.Settings.RemoveMutation)){
			var index = getRandomInt(individual.c.length-1);
			individual.c.splice(index,1)
		}
		while(Chance(this.Settings.SwapMutation)){
			var index = getRandomInt(individual.c.length-1);
			individual.c[index]=this.GetRandomLine();
		}
	},	
	START : function(json){
		this.Json = json;
		this.Population = [];
		for(var i=0;i<this.Settings.Individuals;i++){
			this.Population.push(this.GenerateRandomIndividual());
		}
	},	
	RUN : function(){
		this.GenAlgStep();	
		var result = this.Population[0];	
		console.log(this.Population[0].f);
		return result.c;		
	},
}