
var Forest = {

	Biomes : [
		{w:40,v:{key:'Dense',desc:'A dense forest'}},
		{w:10,v:{key:'Very dense',desc:'A very dense forest'}},
		{w:15,v:{key:'Glade',desc:'A glade between trees'}},
		{w:30,v:{key:'Grassland',desc:'No tree in sight'}},
		{w:5,v:{key:'Barren',desc:'A barren land'}},
	],
	Buildings : [
		{key:'Basic',desc:'Your first hut',items:[
			{key:'Stick',char:'Long',amt:5},
			{key:'Stick',char:'Medium',amt:5},
		]}	
	],
	ItemProps : {
		Length : ['long','short'],
		Thickness : ['thin','thick'],
		Weight : ['heavy','light'],
		Toughness : ['tought','soft'],
		Dryness : ['dry','green']
	},
	Items : {
		Stick:{
			desc:'A wooden stick. Can be used for construction, fire fuel, or as a tool.',
			props:[]
		},
		Stone:{
			desc:'A stone. Can be used for construction or as a tool.'
		},
		Leaves:{
			desc:'Some leaves. Can be used for construction or fire fuel.'
		},
		Grass:{
			desc:'Some grass. Can be used for construction or fire fuel.'
		},
		Trunk:{
			desc:'A piece of tree trunk. Can be used for construction.'
		},
		Flowers:{
			desc:'Some flowers. Can be used for decoration or fire fuel.'
		},
	},
	Objects : [
		{key:'Branches',desc:'A pile of loose branches.'},
		{key:'Bush',desc:'A short dense bush.'},
		{key:'Tree',desc:'A single tree.'},
		{key:'Rocks',desc:'A bunch of rocks in the floor.'},
		{key:'Bramble',desc:'A spined bramble bush.'},
		{key:'Fallen tree',desc:'A big fallen dead tree.'},
		{key:'Scrub',desc:'A scrub of grass and weeds.'},
		{key:'Pond',desc:'A small shallow pond.'},
		{key:'Flowers',desc:'Some white flowers between the grass.'}
	],
	Generate : function(){
		var rnd = Random.Create();

		var biome = Random.WChoice(rnd,Forest.Biomes);
		var amt = Random.Range(rnd,2,5);

		return {
			Biome : biome
		}
	},
	Describe : function(tile){
		return [tile.Biome.desc];
	},
	Start : function() {
		Templating.Title("Forest");

		var tile = Forest.Generate();

		var out = Forest.Describe(tile);
		var inp = [];

		Templating.Out(out);
		Templating.In(inp);
	}
}