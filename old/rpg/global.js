var People = {
	Create : function(name) {
		return {
			name:name,
			relation:50
		}
	}
}

var GLOBAL = {
	Seed : 42,
	Me : {},
	Scene : "Room",
	People : [
		People.Create("Grandpa"),
		People.Create("Grandma")
	],
	Objects : []
}