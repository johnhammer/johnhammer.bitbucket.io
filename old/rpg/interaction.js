var Interaction = {
	Item : function(key,text){
		Templating.Title(text);
		switch(key){
			case "0": 			Scene.Nothing(); break;
			case "1": 			Scene.Sit(); break;
			//room
			case "Upstairs": 	Scene.Start("Stairs"); break;
			case "Window": 	 	Scene.Text("Window"); break;
			case "Bed": 	 	Scene.Text("Bed"); break;
			case "Chair": 		Scene.Items("Chair"); break;
			case "Cupboard": 	Scene.Items("Cupboard"); break;			
			//stairs
			case "Room": 		Scene.Start("Room"); break;
			case "Stairs": 		Scene.Start("Livingroom"); break;
			//livingroom
			case "Grandpa": 	Speech.Start("Grandpa"); break;
			case "Kitchen": 	Scene.Start("Kitchen"); break;
		}
	},
	Action : function(key){

	},
	Back : function(){
		Scene.Start();
	}
}