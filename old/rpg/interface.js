var Interface = {
	Action : function(action,text){
		return {t:text,a:"Interaction.Action('"+action+"')"}
	},
	Item : function(action,text){
		return {t:text,a:"Interaction.Item('"+action+"','"+text+"')"}
	},
	Back : function(text){
		if(!text) text = "Back";
		return {t:text,a:"Interaction.Back()"}
	}
}