var Intro = {
	Start : function() {
		Templating.Title("Unknown place");

		Templating.Out([
			"You look around but don't manage to see anything.",
			"There is something about this place that doesn't feel quite right.",
			"Slowly, as you start to move, see a dim light orb in the horizon, aproaching you."
		]);
		Templating.Input(Templating.Button("Start",	"Intro.Go1()"));		
	},
	Special(text,plus,minus){
		return `<input type="checkbox" name="special" value="boy" checked="checked">${text}<br>`;
	},
	Go1 : function(index){
		Templating.Out([
			"To your suprise the orb starts asking you questions with a deep voice"
		]);
		Templating.Input(`			
			
				<h3>How do you call yourself?</h3>
				<input id="MYNAME" type="text"></input>

				<h3>What do you see if you look at this mirror?</h3>
				<input id="MYGENDER" type="radio" name="gender" value="boy" checked="checked"> Boy<br>
				<input type="radio" name="gender" value="girl"> Girl<br>

				<h3>What makes you different from the others?</h3>
				${Intro.Special("Photosensitive","vision at night", "vision at day")}
				${Intro.Special("Bright eyes","+1 vision at day, -1 at night (can be fixed with glasses)")}		
				${Intro.Special("Hawkeye","+1 vision, -1 reading (can be fixed with glasses)")}	
				${Intro.Special("Short sighted","+1 reading, -1 vision (can be fixed with glasses)")}
				${Intro.Special("Lone wolf","intelligence","charisma")}	
				${Intro.Special("Poster child","charisma","intelligence")}			
				${Intro.Special("Long limbs","speed","endurance & resistance")}		
				${Intro.Special("Rock solid","endurance & resistance","speed")}				

				<h3>What is the world to you?</h3>
				<input id="MYSEED" type="text">

				<h3>Do you want to wake up?</h3>
				<button onclick="Intro.Go2(true)">Yes</button><br>
				<button onclick="Intro.Go2(false)">No (skip tutorial)</button>
			
		`);
	},
	Go2 : function(playTutorial){
		function numberSeed(str){
			var code='';
			for(var i=0;i<str.length;i++){
				code+=str.charCodeAt(i);
			}
			return Number(code);
		}
		
		GLOBAL.Seed = numberSeed(MYSEED.value);
		GLOBAL.Me = {
			Name : MYNAME.value,
			Gender : MYGENDER.checked
		};
		if(playTutorial){
			Scene.Start();
		}
		else{
			Forest.Start();
		}
	}
}