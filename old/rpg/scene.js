var Scene = {
	Start : function(key){
		if(key) GLOBAL.Scene = key;
		else key = GLOBAL.Scene;

		Templating.Title(key);
		var out = [];
		var inp = [];

		switch(key){
			case "Room": 
				out = [
					"You are in your room in the top floor. A bright sunlight lightens the morning of a hot summer day.",
					"You look around the room see this items:",
				];
				inp = [
					Interface.Item("Upstairs",		"A closed wooden door"),
					Interface.Item("Window",	"A window with bars on it"),
					Interface.Item("Bed",		"Your bed, still unmade"),
					Interface.Item("Chair",		"A chair with a pile of clothes on it"),
					Interface.Item("Cupboard",	"Your cupboard embedded into the wall"),
					Interface.Item("0",			"A small table lamp"),
					Interface.Item("0",			"A carpet in the floor"),
				];
			break;

			case "Stairs": 
				out = [
					"You are in the top floor."
				];
				inp = [
					Interface.Item("Room",		"The door of your room"),
					Interface.Item("Stairs",	"Stairs that go down to the living room"),
					Interface.Item("0",			"A door to the bathroom, locked")
				];
			break;

			case "Livingroom": 
				out = [
					"You reach the living room. Three big windows let the sunlight into the house.",
					"A big table with a hanging chandelier occupies most of the room.",
					"In the corner near a fireplace, you see your grandpa reading a book in the sofa.",
					"On the other side, you see a bookcase and a door that leads to the kitchen."
				];
				inp = [
					Interface.Item("Grandpa",	"Speak to your grandpa"),
					Interface.Item("Upstairs",	"Stairs that go up"),
					Interface.Item("0",			"The main table"),
					Interface.Item("1",			"Some chairs"),
					Interface.Item("1",			"The sofa"),
					Interface.Item("0",			"A door to the outside, locked"),				
					Interface.Item("0",			"A black bookcase with many books"),
					Interface.Item("Kitchen",	"A door to the kitchen")
				];
			break;
		}

		Templating.Out(out);
		Templating.In(inp);
	},
	Text : function(key){
		var text = [];
		switch(key){
			case "Window": 
				text= [
					"You look out the window and see a quiet street, bathed in sunlight.",
					"Lonely white houses with walled gardens cover both sides of the street, but you manage to see a forest in the distance.",
					"Looking down, you see the roof of your house, and a tiny bit of garden below."
				]
			break;
			case "Bed": 
				text= [
					"You remember having had a strange dream, but cannot make out the details.",
					"The sun is high in the sky, so you don't feel like going back to bed."
				]
			break;
		}
		Templating.In([Interface.Back()]);
		Templating.Out(text);		
	},
	Items : function(key){
		var items = [];
		switch(key){
			case "Chair": 
				items= [
					Interface.Item("0","A black T-shirt"),
					Interface.Item("0","Short trousers"),
				]
			break;
			case "Cupboard": 
				items= [
					Interface.Item("0","A bunch of clean blankets and sheets"),
				]
			break;
		}
		items.push(Interface.Back());
		Templating.In(items);
		Templating.Out(["You can see this items:"]);
	},
	Nothing : function(){		
		Templating.In([Interface.Back()]);
		Templating.Out(["You can't think of anything to do with this."]);
	},
	Sit : function(){		
		Templating.In([Interface.Back("Get up")]);
		Templating.Out(["You take a seat."]);
	},
}