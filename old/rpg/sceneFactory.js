var Pair = function (x, y) {
	return x << 16 & 0xffff0000 | y & 0x0000ffff;
};

var Depair = function (p) {
	return [p >> 16 & 0xFFFF, p & 0xFFFF]
};

var RoomType = {
	Hall : {
		MinObjects:0,
		MaxObjects:2,
		Objects:[
			'lamp',
			'window'
		],
	},
	Living : {
		MinObjects:2,
		MaxObjects:5,
		Objects:[
			'lamp',
			'window',
			'table',
			'sofa',
			'bookcase',
			'fireplace'			
		],
	},
	Room : {
		MinObjects:4,
		MaxObjects:6,
		Objects:[
			'lamp',
			'window',
			'carpet',
			'table',
			'bed',
			'cupboard',
			'chair'			
		],
	}
}

var SceneFactory = {
	RoomSeed : function(x,y){
		return Random.MultiSeed(GLOBAL.Seed,x,y);
	},	
	GenerateRoomType : function(rnd,accesses){
/*
		var doors = accesses.n+accesses.s+accesses.w+accesses.e;

		if(doors==1){
			return "Room";
		}
		if(doors==2){
			return "Living";
		}
		
		return "Hall";		
*/
		return RoomType[Random.Choice(rnd,Object.keys(RoomType))];
	},
	GenerateObjects : function(rnd,roomType){
		return Random.Multichoice(rnd,roomType.Objects,roomType.MinObjects,roomType.MaxObjects);
	},
	GenerateAccess : function(x1,y1, x2,y2){
		if(x2>x1){
			var aux = x2;
			x2 = x1;
			x1= aux;
		}
		if(y2>y1){
			var aux = y2;
			y2 = y1;
			y1= aux;
		}
		var rnd = Random.MultiSeed2(GLOBAL.Seed,x1,x2,y1,y2);
		return Random.Chance(rnd,2);
	},
	GenerateAccesses : function(x,y){
		return{
			n:SceneFactory.GenerateAccess(x,y, x,y+1),
			s:SceneFactory.GenerateAccess(x,y, x,y-1),
			w:SceneFactory.GenerateAccess(x,y, x-1,y),
			e:SceneFactory.GenerateAccess(x,y, x+1,y),
		};		
	},
	GenerateRoomZero(){

	},
	GenerateRoom : function(x,y){		
		if(x==0&&y==0){
			return SceneFactory.GenerateRoomZero();
		}

		var accesses = SceneFactory.GenerateAccesses(x,y);	
		
		var rnd = SceneFactory.RoomSeed(x,y);
		var roomType = SceneFactory.GenerateRoomType(rnd,accesses);
		var objects = SceneFactory.GenerateObjects(rnd,roomType);
		
		return {
			roomType : roomType,
			objects: objects,
			accesses : accesses
		};
	},
}


var HouseFactory = {
	RoomTypes : {
		Start : {
			MinAccesses:1,
			MaxAccesses:1,
			Accesses:[
				{w:75,v:'Entrance'},
				{w:15,v:'Living'},
				{w:10,v:'Hall'}
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Hall : {
			MinAccesses:3,
			MaxAccesses:6,
			Accesses:[
				{w:45,v:'Room'},
				{w:20,v:'Bathroom'},
				{w:10,v:'Living'},
				{w:10,v:'Garage'},
				{w:5,v:'Kitchen'},
				{w:5,v:'Hall'},
				{w:5,v:'Terrasse'},
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Room : {
			MinAccesses:1,
			MaxAccesses:1,
			Accesses:[
				{w:95,v:'Nothing'},
				{w:5,v:'Terrasse'},
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Bathroom : {
			MinAccesses:0,
			MaxAccesses:0,
			Accesses:[

			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Entrance : {
			MinAccesses:1,
			MaxAccesses:3,
			Accesses:[
				{w:75,v:'Hall'},
				{w:10,v:'Living'},
				{w:10,v:'Garage'},
				{w:5,v:'Kitchen'},			
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Living : {
			MinAccesses:1,
			MaxAccesses:3,
			Accesses:[				
				{w:25,v:'Hall'},
				{w:25,v:'Bathroom'},
				{w:15,v:'Nothing'},
				{w:25,v:'Kitchen'},
				{w:10,v:'Terrasse'},			
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Kitchen : {
			MinAccesses:1,
			MaxAccesses:1,
			Accesses:[				
				{w:95,v:'Nothing'},
				{w:5,v:'Terrasse'},			
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Garage : {
			MinAccesses:1,
			MaxAccesses:1,
			Accesses:[				
				{w:100,v:'Outside'},			
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Terrasse : {
			MinAccesses:1,
			MaxAccesses:1,
			Accesses:[				
				{w:80,v:'Nothing'},
				{w:20,v:'Outside'},			
			],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		},
		Outside : {
			MinAccesses:0,
			MaxAccesses:0,
			Accesses:[],
			MinObjects:0,
			MaxObjects:0,
			Objects:[

			],
		}
	},
	CreateHouse : function(seed){
		var rnd = Random.Create(seed);
		return HouseFactory.CreateNode(rnd,'Start');
	},
	CreateNode : function(rnd,type){
		var types = Random.WMultichoice(rnd,
			HouseFactory.RoomTypes[type].Accesses,
			HouseFactory.RoomTypes[type].MinAccesses,
			HouseFactory.RoomTypes[type].MaxAccesses);

		var accesses = [];
		for(var i=0;i<types.length;i++){
			console.log(types[i]);
			if(types[i]!='Nothing') accesses.push(HouseFactory.CreateNode(rnd,types[i]));
		}

		return {
			type:type,
			accesses:accesses
		};
	},
}