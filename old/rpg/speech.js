var Speech = {
	Start : function(key){
		Templating.Title("Speaking with "+key);

		var out = [];
		var inp = [];

		switch(key){

		case "Grandpa": 
				out = [
					"Your grandpa looks at you over his book, expecting you to say something."
				];
				inp = [
					Interface.Phrase("Grandpa",	"Speak to your grandpa"),
				];
			break;
		}

		Templating.Out(out);
		Templating.In(inp);
	}
}