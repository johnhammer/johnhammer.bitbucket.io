var Templating = {
	Input : function(content){
		input.innerHTML = content;
	},
	Output : function(content){
		output.innerHTML = content;
	},
	Title : function(content){
		title.innerHTML = content;
	},

	Buttons : function(buttons){
		return buttons.map(function (key) {
			return Templating.Button(key.t,key.a);
		}).join("");
	},
	Button : function(name,action){
		return `<button onclick="${action}">${name}</button>`
	},

	Text : function(lines){
		return lines.map(function (key) {
			return `<p>${key}</p>`;
		}).join("");
	},

	In : function(buttons){
		Templating.Input(Templating.Buttons(buttons));
	},
	Out : function(text){
		Templating.Output(Templating.Text(text));
	},
	Extra : function(title,content){
		input.innerHTML = '';
		output.innerHTML = '';
		title.innerHTML = '';
		extra.innerHTML = content;		
	},

	People : function(people){
		return `<h2>Relations</h2>
			<div>`+
				people.map(function (key) {
					return Templating.Person(key);
				}).join("")+
		 	`</div>`;
	},
	Person : function(person){
		return `<div>
				<p>${person.name}</p>
			</div>`
	}


}