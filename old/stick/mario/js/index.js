var game = new Phaser.Game(256, 240, Phaser.CANVAS, '', {
  preload: preload,
  create: create,
  update: update
}, false, false);

var hotkey;

function preload() {
  game.load.spritesheet('tiles', 	'https://johnhammer.bitbucket.io/stick/mario/assets/tiles_dctsfk.png', 16, 16);
  game.load.spritesheet('goomba', 	'https://johnhammer.bitbucket.io/stick/mario/assets/goomba_nmbtds.png', 16, 16);
  game.load.spritesheet('mario', 	'https://johnhammer.bitbucket.io/stick/mario/assets/mario_wjlfy5.png', 42, 51);
  game.load.spritesheet('coin', 	'https://johnhammer.bitbucket.io/stick/mario/assets/coin_iormvy.png', 16, 16);
  game.load.spritesheet('flame', 	'https://johnhammer.bitbucket.io/stick/mario/assets/flame.png', 200, 200);
  game.load.tilemap('level', 		'https://bitbucket.org/johnhammer/johnhammer.bitbucket.io/raw/7b40922681af8c921a3f6ee890ee110d5180e229/stick/mario/assets/3kk2g.json', null, Phaser.Tilemap.TILED_JSON);
}

function create() {
  Phaser.Canvas.setImageRenderingCrisp(game.canvas)
  game.scale.pageAlignHorizontally = true;
  game.scale.pageAlignVertically = true
  game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
  game.physics.startSystem(Phaser.Physics.ARCADE);

  game.stage.backgroundColor = '#5c94fc';

  map = game.add.tilemap('level');
  map.addTilesetImage('tiles', 'tiles');
  map.setCollisionBetween(3, 12, true, 'solid');

  map.createLayer('background');

  layer = map.createLayer('solid');
  layer.resizeWorld();

  coins = game.add.group();
  coins.enableBody = true;
  map.createFromTiles(2, null, 'coin', 'stuff', coins);
  coins.callAll('animations.add', 'animations', 'spin', [0, 0, 1, 2], 3, true);
  coins.callAll('animations.play', 'animations', 'spin');

  goombas = game.add.group();
  goombas.enableBody = true;
  map.createFromTiles(1, null, 'goomba', 'stuff', goombas);
  goombas.callAll('animations.add', 'animations', 'walk', [0, 1], 2, true);
  goombas.callAll('animations.play', 'animations', 'walk');
  goombas.setAll('body.bounce.x', 1);
  goombas.setAll('body.velocity.x', -20);
  goombas.setAll('body.gravity.y', 500);

  player = game.add.sprite(16, game.world.height - 48, 'mario');
  game.physics.arcade.enable(player);
  player.body.gravity.y = 370;
  player.body.collideWorldBounds = true;
  player.animations.add('walkRight', 	[0, 1, 2, 3, 4, 5, 6, 7, 8 ], 10, true);
  player.animations.add('walkLeft', 	[29,28,27,26,24,23,22,21,20], 10, true);
  anim = player.animations.add('hitRight', 	[10,11,12,13,14,15 ], 10, false);
  anim.onComplete.add(animationStopped, this);
  anim = player.animations.add('hitLeft', 	[30,31,32,33,34,39], 10, false);
  anim.onComplete.add(animationStopped, this);
  player.goesRight = true;

  
  
  
  // game.camera.follow(player);

  cursors = game.input.keyboard.createCursorKeys();
  
  hotkey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
  
}

function animationStopped(){
	player.blocker = false;
}

function update() {
  game.physics.arcade.collide(player, layer);
  game.physics.arcade.collide(goombas, layer);
  game.physics.arcade.overlap(player, goombas, goombaOverlap);
  game.physics.arcade.overlap(player, coins, coinOverlap);

  if (player.body.enable && !player.blocker) {
    player.body.velocity.x = 0;
    if (cursors.left.isDown) {
      player.body.velocity.x = -90;
      player.animations.play('walkLeft');
      player.goesRight = false;
    } else if (cursors.right.isDown) {
      player.body.velocity.x = 90;
      player.animations.play('walkRight');
      player.goesRight = true;
    } else {
      player.animations.stop();
      if (player.goesRight) player.frame = 9;
      else player.frame = 25;
    }

	if(player.body.onFloor()){
		if (cursors.up.isDown) {
		  player.body.velocity.y = -190;
		  player.animations.stop();
		}
		else if (hotkey.isDown) {
		  player.blocker = true;
		  if (player.goesRight) player.animations.play('hitRight');
		  else player.animations.play('hitLeft');
		  
		  
		  flame = game.add.sprite(player.position.x, player.position.y+200, 'flame');
		  flame.animations.add('right', 	[0, 1, 2, 3, 4, 5, 6, 7], 8, false);
		  flame.animations.play('right');
		}
	}
    

    if (player.body.velocity.y != 0) {
      if (player.goesRight) player.frame = 8;
      else player.frame = 26;
    }
  }
}

function coinOverlap(player, coin) {
  coin.kill();
}

function goombaOverlap(player, goomba) {
	/*
  if (player.body.touching.down) {
    goomba.animations.stop();
    goomba.frame = 2;
    goomba.body.enable = false;
    player.body.velocity.y = -80;
    game.time.events.add(Phaser.Timer.SECOND, function() {
      goomba.kill();
    });
  } else {
    player.frame = 6;
    player.body.enable = false;
    player.animations.stop();
    game.time.events.add(Phaser.Timer.SECOND * 3, function() {
      game.paused = true;
    });
  }
  */
}