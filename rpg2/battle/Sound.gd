extends AudioStreamPlayer

var Sounds = {}

func LoadSound(key):
	Sounds[key] = load("res://art/sound/"+key+".wav")

func _ready():
	LoadSound("stab1")
	LoadSound("stab2")
	LoadSound("stab3")
	LoadSound("hit1")
	LoadSound("hit2")
	LoadSound("hit3")
	LoadSound("cut1")
	LoadSound("cut2")
	LoadSound("cut3")
	LoadSound("tornstad")
	LoadSound("hogt")
	LoadSound("early_battle")

var IsOn = false

func Toggle(on): 
	if !on: stop()
	IsOn = on

func Play(anim):
	if IsOn && !is_playing():
		stream = Sounds[anim]
		play()

func PlayAttack(anim):
	if  IsOn && !is_playing():
		match anim:
			"cut": stream = Random.Choice([Sounds["stab1"]])
			"stab": stream = Random.Choice([Sounds["cut1"]])
			"hit": stream = Random.Choice([Sounds["hit1"]])
		play()

