extends Node2D

const PlayerResource = preload("res://battle/fighter.tscn")

var friends = []
var feinds = []
var feinds_data = []
var mainPanel

func Clear():
	Global.delete_children($nodes)
	$Controls/Panel.Clear()
	friends = []
	feinds = []
	state = States.PickAction
	state_player = 0
	state_enemy = 0
	state_action_button = null
	state_action = null
	state_objective = null
	state_objective_isfeind = true

func IsPositionValid(x,y,items,num=40):
	var dist
	for friend in items:
		dist = friend.position.distance_to(Vector2(x,y))
		if dist<num: return false #80
	return true

func GetValidFriendPosition():
	var invalid = true
	while invalid:
		var x = Random.Ranges(95,127)
		var y = Random.Ranges(360,512)
		invalid = !IsPositionValid(x,y,friends)
		if !invalid:
			return {
				"x":x,"y":y
			}

func GetValidFeindPosition():
	var invalid = true
	var maxit = 20
	var i = 0
	while invalid && i<maxit:
		var x = Random.Ranges(586,609)
		var y = Random.Ranges(360,512)
		invalid = !IsPositionValid(x,y,feinds)
		if !invalid:
			return {
				"x":x,"y":y
			}
		i+=1
	i = 0
	while invalid && i<maxit:
		var x = Random.Ranges(500,609)
		var y = Random.Ranges(360,512)
		invalid = !IsPositionValid(x,y,feinds,20)
		if !invalid:
			return {
				"x":x,"y":y
			}
		i+=1
	push_error("unable to position feinds")

func start(_feinds_data,img,canflee):
	Clear()
	feinds_data = _feinds_data
	for friend in Global.party:
		var res = PlayerResource.instance()
		var pos = GetValidFriendPosition()
		res.Start(self,pos.x,pos.y,false,friend)
		res.data.dead = false
		$nodes.add_child(res)
		friends.push_back(res)
	
	for feind in feinds_data:
		var res
		if "custom" in feind:
			var resource = load("res://battle/monsters/"+feind.custom+".tscn")
			res = resource.instance()
		else:
			res = PlayerResource.instance()
		var pos = GetValidFeindPosition()
		res.Start(self,pos.x,pos.y,true,feind)
		res.data.dead = false
		$nodes.add_child(res)
		feinds.push_back(res)
	
	mainPanel = $Controls/Panel
	$image.texture = load("res://images/"+img+".jpg")
	mainPanel.init(self,Global.party,feinds_data,canflee)
	
	set_state(States.PickAction)
	get_node("Camera2D").current = true

func Event(event):
	get_node("../story").Event(event)

func Die(fighter):
	fighter.die()
	fighter.data.dead = true

func CheckIfAllFeindsDead():
	for feind in feinds:
		if !feind.data.dead: 
			return false
	return true

func CheckIfAllFriendsDead():
	for feind in friends:
		if !feind.data.dead: return false
	return true

func EndBattle(flee):
	state = States.EndOfBattle
	get_node("..").stop_battle(flee,feinds)
	return

func Effect(position,type):
	var effect = load("res://effects/"+type+".tscn").instance()
	#shader_mat.set_uniform_value("time", 0)
	
	add_child(effect)
	if "rect_position" in effect:
		effect.color = Global.ArrayToColor(Global.colors_dictionary["red_hair"])
		effect.rect_position = Vector2(position.x-32,position.y-64)
	if "position" in effect:
		effect.position = Vector2(position.x+64,position.y)	
	return effect
	
func Projectile(position,type,rotate = true):
	var effect = load("res://effects/projectile.tscn").instance()
	add_child(effect)
	if !rotate: effect.rot_speed = -1
	var sprite = effect.get_node("Path/PathFollow2D/Sprite")
	if type == "ball":
		sprite.get_node("Ball").visible = true
		sprite.get_node("Ball").color = Global.ArrayToColor(Global.colors_dictionary["red_hair"])
	else:
		sprite.texture = load("res://art/human/arms/"+type+".png")
	return effect

func SetState(target,state):
	if !"states" in target.data: target.data.states = {}
	target.data.states[state] = 5;

func UseItem(type,target):
	match type:
		"medikit","omnikit":
			if(target.data.hea>0): 
				target.data.hea = target.data.maxhea
		"wakekit": 
			if(target.data.hea<1): 
				target.data.hea = target.data.maxhea
		"sanakit","curakit": 
			#remove graphic effect
			target.data.states = {}
		"soulkit": 
			target.data.sou = target.data.maxsou
		"megakit","terakit": 
			target.data.hea = target.data.maxhea
			target.data.sou = target.data.maxsou
			#remove graphic effect
			target.data.states = {}

func Action(fighter,targets,action):
	if action == "flee" || action == "smokebomb":
		EndBattle(true)
		return
	if action == "wait":
		OnAnimationEnd(true)
		return
		
	var move = Global.moves_dictionary[action]
	
	get_node("../sound").PlayAttack(move.anim)
	
	if "weapon" in move:
		fighter.show_weapon()
	else:
		fighter.hide_weapon()
	
	fighter.attack(targets,move.anim,move.melee==1,state_objective==-1)
	
	if "cast" in move:
		Effect(fighter.position,"cast")
	
	var cost = Calculator.GetCost(move,fighter.data)
	if(action == "blood"):
		fighter.data.sou += cost
		fighter.data.hea -= cost*1.5
	else:
		fighter.data.sou -= cost
	
	for target in targets:
		
		#effects
		if "item" in move:
			var effect = Effect(Vector2(target.position.x-64,target.position.y),"item")
			effect.texture = load("res://art/inventory/"+move.item+".png")
			UseItem(move.item,target)
		if "projectile" in move:
			var effect = Projectile(Vector2(fighter.position.x-64,fighter.position.y),move.projectile,"rotate" in move)
			var points = [fighter.position,targets[0].position]
			effect.Init(points)
			Items.Remove(move.projectile)
		if "targeteffect" in move:
			Effect(Vector2(target.position.x-64,target.position.y),move.targeteffect)
		if "state" in move:
			target.SetState(move.state)
			SetState(target,action)
		if "weaponstate" in move:
			target.SetWeaponState(move.weaponstate)
			SetState(target,action)
			
		
		if !"damage" in move || move.damage == "e":
			pass
		else: #damage type
			var damage = Calculator.GetDamage(move,fighter.data,target.data,fighter.data.weapon)
			var isCrit = false #Calculator.IsCritical(fighter.data,target.data)
			if isCrit: damage *= 1.5
			
			if(action == "heal"):
				target.data.hea += damage
			elif(action == "blood"):
				pass
			else:
				target.damage(damage,isCrit)
				target.data.hea -= damage
			
			if target.data.hea<=0: #death
				Die(target)
				if CheckIfAllFeindsDead():
					yield(get_tree().create_timer(2.1), "timeout")
					EndBattle(false)
					return
				if CheckIfAllFriendsDead():
					yield(get_tree().create_timer(2.1), "timeout")
					get_node("..").Defeat()
					return

	mainPanel.Refresh(Global.party,feinds_data)
	mainPanel.DisableTargets()

#ai 
func AiGetTarget(enemy):
	return friends[0]

func AiGetAction(enemy):
	var move = null
	if "next_move" in enemy.ai && enemy.ai.next_move!=null:
		move = enemy.ai.next_move
		enemy.ai.next_move = null
	elif "moves_random" in enemy.ai:
		move = Random.Choice(enemy.ai.moves_random)
	elif "moves_combination" in enemy.ai:
		move = enemy.ai.moves_combination[enemy.ai.counter]
		enemy.ai.counter += 1
		if enemy.ai.counter>=enemy.ai.moves_combination.size():
			enemy.ai.counter = 0
	return move

func RunAiEvent(enemy,event):
	if "story" in event:
		get_node("../story").Event(event.story)
	if "ai" in event:
		enemy.ai = enemy[event.ai]
	if "atk" in event:
		enemy.atk = event.atk
	if "next_move" in event:
		enemy.ai.next_move = event.next_move

func AiCheckEvents(enemy):
	if "events" in enemy.ai:
		for event in enemy.ai.events:
			var run = false
			match event.type:
				"hp_lower_than":
					if enemy.hea<event.amt: run = true
				"player_hp_lower_than":
					if friends[0].data.hea<event.amt: run = true
			if run: RunAiEvent(enemy,event)

#state machine

enum States {
	PickAction,
	PickObjective,
	EnemyTurn,
	Animate,
	EndOfBattle
}
var state = States.PickAction
var state_player = 0
var state_enemy = 0
var state_action_button = null
var state_action = null
var state_objective = null
var state_objective_isfeind = true

func OnAnimationEnd(isFeind,isBlock = false):
	if state == States.EndOfBattle:return
	
	if isBlock:
		yield(get_tree().create_timer(0.5), "timeout")
		
	
	if isFeind:
		if(state_enemy>=feinds.size()-1):
			set_state(States.PickAction)
		else:
			state_enemy=state_enemy+1
			set_state(States.EnemyTurn)
	else:
		if(state_player>=friends.size()-1):
			set_state(States.EnemyTurn)
		else:
			state_player=state_player+1
			set_state(States.PickAction)
		

func OnActionPicked(id,action):
	state_action_button = id
	state_action = action
	set_state(States.PickObjective)
	
func OnObjectivePicked(id):
	state_objective = id
	set_state(States.Animate)

func InitPickObjective(move):
	match move.target:
		"1e": #1 enemy
			mainPanel.EnableTargets()
			state_objective_isfeind = true
		"1f": #1 friend
			mainPanel.EnableFriendTargets()
			state_objective_isfeind = false
		"s": #self
			state_objective = state_player
			state_objective_isfeind = false
		"3e": #3 enemies
			state_objective = null
			state=States.Animate
			state_objective_isfeind = true
		"3f": #3 friends
			state_objective = null
			state=States.Animate
			state_objective_isfeind = false
		"b": #block
			state_objective = -1
			state=States.Animate
			state_objective_isfeind = false

func set_state(st):
	state = st
	if state==States.PickAction:
		var fighter = friends[state_player]
		if fighter.data.dead:
			state_player+=1
			if(state_player>friends.size()-1):
				set_state(States.EnemyTurn)
			else:
				set_state(States.PickAction)
			return
		state_enemy = 0
		mainPanel.SetMenu(state_player)
		mainPanel.DisableTargets()
		mainPanel.EnableActions()
		mainPanel.SetPlayerFocus(state_player)
		
	if state==States.PickObjective:
		mainPanel.DisableActions()
		mainPanel.SetActionFocus(state_action_button)
		var move = Global.moves_dictionary[state_action]
		InitPickObjective(move)
		
	if state==States.Animate:
		mainPanel.DisableTargets()
		mainPanel.SetPlayerFocus(state_player)
		var fighter = friends[state_player]
		
		#get targets
		var targets = []
		if state_objective==-1: #block
			targets = []
		elif state_objective==null: #all
			if state_objective_isfeind:
				targets = feinds
			else:
				targets = friends
		else: #one
			mainPanel.SetPlayerFocus(state_objective)
			if state_objective_isfeind:
				targets = [feinds[state_objective]]
			else:
				targets = [friends[state_objective]]
				
		
		Action(fighter,targets,state_action)
		
	if state==States.EnemyTurn:
		mainPanel.DisableTargets()
		state_player = 0
		var fighter = feinds[state_enemy]
		if fighter.data.dead:
			state_enemy+=1
			if(state_enemy>=feinds.size()):
				set_state(States.PickAction)
			else:
				set_state(States.EnemyTurn)
			return
		yield(get_tree().create_timer(0.3), "timeout")
		AiCheckEvents(fighter.data)
		var target = AiGetTarget(fighter.data)
		var action = AiGetAction(fighter.data)
		Action(fighter,[target],action)

