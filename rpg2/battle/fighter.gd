extends "res://object/human.gd"

var parent
var myposition
var data
var isFeind
var oneHand = true
var weaponvisible = false
var animationEndDisabled = false

func SetState(state):
	$chest.get_material().set_shader_param(state, 1)

func SetWeaponState(state):
	$dagger.get_material().set_shader_param(state, 1)
	show_weapon()

func play(anim):
	animationEndDisabled = false
	$legs.play(anim)
	$scaled.play(anim)
	$plated.play(anim)
	$mail.play(anim)
	$gambeson.play(anim)
	$cuirass.play(anim)
	$brigandinedots.play(anim)
	$brigandine.play(anim)
	$chest.play(anim)
	$head.play(anim)
	$hair.play(anim)
	$hairwoman.play(anim)
	$hairmask.play(anim)
	$hairhood.play(anim)
	$hairarima.play(anim)

func playArms(anim):
	var armsanim = anim
	if weaponvisible:
		if anim=="hit" && !oneHand: armsanim = "hit2"
		if anim=="punch" && !oneHand: armsanim = "punch2"
	$arms.play(armsanim)
	
	$dagger.play(armsanim)
	$sable.play(armsanim)
	$club.play(armsanim)
	$axe.play(armsanim)
	$mace.play(armsanim)
	$sword.play(armsanim)
	$spear.play(armsanim)
	$sword.play(armsanim)
	$spear.play(armsanim)
	$staff.play(armsanim)
	$naginata.play(armsanim)
	$halberd.play(armsanim)
	$poleaxe.play(armsanim)
	$warhammer.play(armsanim)
	$gun.play(armsanim)

func playAll(anim):
	play(anim)
	playArms(anim)

func SetupWeapon():
	if data.weapon.size()==0:return
	weaponvisible = true
	match data.weapon[0]:
		"gun":SetupPart($gun,data.weapon)
		
		"dagger":SetupPart($dagger,data.weapon)
		"sable":SetupPart($sable,data.weapon)
		"club":SetupPart($club,data.weapon)
		"axe":SetupPart($axe,data.weapon)
		"mace":SetupPart($mace,data.weapon)
		"sword":SetupPart($sword,data.weapon)
		"spear":
			SetupPart($spear,data.weapon)
			oneHand = false
		"staff":
			SetupPart($staff,data.weapon)
			oneHand = false
		"naginata":
			SetupPart($naginata,data.weapon)
			oneHand = false
		"halberd":
			SetupPart($halberd,data.weapon)
			oneHand = false
		"poleaxe":
			SetupPart($poleaxe,data.weapon)
			oneHand = false
		"warhammer":
			SetupPart($warhammer,data.weapon)
			oneHand = false

func Start(parentnode,x,y,_isFeind,_data):
	data = _data
	isFeind = _isFeind
	Init(data)
	SetupWeapon()
	
	parent = parentnode
	position = Vector2(x,y)
	myposition = Vector2(x,y)
	
	if isFeind:
		scale.x = -1
		$damage.scale.x = -1
	
	playAll("normal")
	$damage.visible = true
	
	SetMaterials()
	
	var _a = $legs.connect("animation_finished", self, "animationend")

func SetMaterials():
	var material = $chest.get_material().duplicate()
	$chest.set_material(material)
	$legs.set_material(material)
	$arms.set_material(material)
	$scaled.set_material(material)
	$plated.set_material(material)
	$mail.set_material(material)
	$gambeson.set_material(material)
	$cuirass.set_material(material)
	$brigandinedots.set_material(material)
	$brigandine.set_material(material)
	$head.set_material(material)
	$hairwoman.set_material(material)
	$hair.set_material(material)
	$hairmask.set_material(material)
	$hairhood.set_material(material)
	$hairarima.set_material(material)
	var weaponmaterial = $dagger.get_material().duplicate()
	$dagger.set_material(weaponmaterial)
	$sable.set_material(weaponmaterial)
	$club.set_material(weaponmaterial)
	$axe.set_material(weaponmaterial)
	$mace.set_material(weaponmaterial)
	$sword.set_material(weaponmaterial)
	$spear.set_material(weaponmaterial)
	$sword.set_material(weaponmaterial)
	$spear.set_material(weaponmaterial)
	$staff.set_material(weaponmaterial)
	$naginata.set_material(weaponmaterial)
	$halberd.set_material(weaponmaterial)
	$poleaxe.set_material(weaponmaterial)
	$warhammer.set_material(weaponmaterial)
	$gun.set_material(weaponmaterial)

func animationend():
	if animationEndDisabled: return
	var anim = $legs.animation
	if $arms.animation == "block":
		parent.OnAnimationEnd(isFeind,true)
		animationEndDisabled=true
		return
	if anim == "die":return
	if anim != "normal":
		playAll("normal")
		position = myposition
		if anim != "damage":
			parent.OnAnimationEnd(isFeind)

func damage(amount,iscrit):
	$damage.show_value(amount,iscrit)
	playAll("damage")

func die():
	playAll("die")

func attack(objectives,anim,melee,isBlock):
	if melee:
		var newX = objectives[0].position.x
		if isFeind: 
			if weaponvisible: newX+=140
			else: newX+=100
		else:
			if weaponvisible: newX-=140
			else: newX-=100
		position = Vector2(newX,objectives[0].position.y)
	elif isBlock:
		if isFeind: 
			position = Vector2(440,420)
		else:
			position = Vector2(140,420)
	if anim=="block":
		play("block")
	else:
		play("punch")
	playArms(anim)

func show_weapon():
	SetupWeapon()
	
func hide_weapon():
	weaponvisible = false
	$dagger.visible = false
	$sable.visible = false
	$club.visible = false
	$axe.visible = false
	$mace.visible = false
	$sword.visible = false
	$spear.visible = false
	$sword.visible = false
	$spear.visible = false
	$staff.visible = false
	$naginata.visible = false
	$halberd.visible = false
	$poleaxe.visible = false
	$warhammer.visible = false
	$gun.visible = false
