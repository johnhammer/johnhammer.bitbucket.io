extends Panel

const BarResource = preload("res://battle/bar.tscn")
const MiniBarResource = preload("res://battle/minibar.tscn")
var parent
var friends = []
var feinds = []
var friends_data = []
var feinds_data = []
var menu_dictionary = null
var menu_current = null
var canflee

var ColorButtonNormal = Color("#995500")
var ColorButtonDisabled = Color("#666666")

func Refresh(datafriends,datafeinds):
	Clear()
	init(parent,datafriends,datafeinds,canflee)

func Clear():
	Global.delete_children($Friends)
	Global.delete_children($Feinds)
	friends = []
	feinds = []
	friends_data = []
	feinds_data = []

func DisableTargets():
	var id = 0
	for friend in friends:
		normal(get_mynode(id))
		id+=1
	for feind in feinds:
		normal(get_mynode(id))
		id+=1

func EnableTargets():
	var id = friends.size()
	for feind in feinds:
		selectable(get_mynode(id))
		id+=1

func EnableFriendTargets():
	var id = 0
	for friend in friends:
		selectable(get_mynode(id))
		id+=1
	
func EnableActions():
	selectable(get_button(1))
	selectable(get_button(2))
	selectable(get_button(3))
	selectable(get_button(4))
	selectable(get_button(5))
	selectable(get_button(6))
	selectable(get_button(7))
	selectable(get_button(8))
	selectable(get_button(9))

func DisableActions():
	normal(get_button(1))
	normal(get_button(2))
	normal(get_button(3))
	normal(get_button(4))
	normal(get_button(5))
	normal(get_button(6))
	normal(get_button(7))
	normal(get_button(8))
	normal(get_button(9))
	
func SetPlayerFocus(num):
	active(get_mynode(num))
	
func SetActionFocus(num):
	active(get_button(num))

func active(node):
	setColor(node,'custom_styles/disabled',ColorButtonNormal)
	
func normal(node):
	setColor(node,'custom_styles/disabled',ColorButtonDisabled)
	node.disabled = true
	
func selectable(node):
	node.disabled = false
	
func setColor(node,statestr,color):
	var style = StyleBoxFlat.new()
	node.set(statestr, style)
	style.set_bg_color(color)
	node.update()	

func init_bar(node,data,id):
	node.get_node("Name").text = data.name
	node.get_node("Health").max_value = data.maxhea
	node.get_node("Health").value = data.hea
	node.get_node("Soul").max_value =  data.maxsou
	node.get_node("Soul").value =  data.sou
	node.connect("pressed", self, "player_button_toggled",[id])

func SetMenu(player):
	var menu = friends_data[player].menu
	menu_dictionary = menu
	load_menu("menu")
	
func ContainsMove(key,moves):
	for move in moves:
		if move == key:
			return true
	return false

func GetMenu(player):
	var moves = Global.party[player].moves.duplicate()
	
	#weapon moves
	if Global.party[player].weapon.size()!=0:
		var weapon = Global.items_dictionary[Global.party[player].weapon[0]]
		moves.push_back("weapon")
		moves += weapon.bonus
	elif ContainsMove("throw",moves):
		moves.push_back("weapon")
		
	#flee
	if canflee: moves.push_back("flee")
	
	#projectiles
	if Items.HasItem("knife"): moves.push_back("knife")
	if Items.HasItem("javelin"): moves.push_back("javelin")
	if Items.HasItem("star"): moves.push_back("star")
	if Items.HasItem("shuriken"): moves.push_back("shuriken")
	if Items.HasItem("bullet"): moves.push_back("bullet")
	if Items.HasItem("spikeball"): moves.push_back("spikeball")
	if Items.HasItem("bomb"): moves.push_back("bomb")
	if Items.HasItem("smokebomb") && canflee: 
		moves.push_back("smokebomb")
		
	
	var menu = {}
	for bloc in Global.menu_dictionary:
		menu[bloc] = []
		for key in Global.menu_dictionary[bloc]:
			var keyname
			if "n" in key:keyname=key.n
			elif "a" in key:keyname=key.a
			if ContainsMove(keyname,moves):
				menu[bloc].push_back(key)
			else:
				menu[bloc].push_back({}) #!!!!!!!!!!
				#menu[bloc].push_back(key)
	return menu

func init(parentNode,datafriends,datafeinds,_canflee):
	canflee = _canflee
	parent = parentNode
	var b = get_children() # node paths dont work
	var id = 0
	friends_data = datafriends
	feinds_data = datafeinds
	for friend in datafriends:
		var res = BarResource.instance()
		b[0].add_child(res)
		init_bar(res,friend,id)
		friend.menu = GetMenu(id)
		friends.push_back(res)
		id+=1
	id = 0
	for feind in datafeinds:
		var res = BarResource.instance()
		if (datafeinds.size()>3):
			res = MiniBarResource.instance()
			b[2].columns = 4
		else:
			b[2].columns = 1
		b[2].add_child(res)
		init_bar(res,feind,id)
		feinds.push_back(res)
		id+=1
	
func load_menu(menu):
	menu_current = menu
	var node = menu_dictionary[menu_current]
	setup_button(1,node[0])
	setup_button(2,node[1])
	setup_button(3,node[2])
	setup_button(4,node[3])
	setup_button(5,node[4])
	setup_button(6,node[5])
	setup_button(7,node[6])
	setup_button(8,node[7])
	setup_button(9,node[8])
	
func setup_button(id,item):
	var button = get_button(id)
	if "n" in item:
		button.text = item.n
	elif "a" in item:
		button.text = item.a
	else:
		button.text = ""

func button_toggled(target):	
	var node = menu_dictionary[menu_current][target-1]
	if "m" in node:
		load_menu(node.m)
	elif "a" in node:
		parent.OnActionPicked(target,node.a)

func player_button_toggled(target):
	parent.OnObjectivePicked(target)

func get_mynode(id):
	if id<friends.size():
		return friends[id]
	else:
		return feinds[id-friends.size()]

func get_button(id):
	var b = get_children() # node paths dont work
	return b[1].get_node("Button"+String(id))

func _on_Button1_pressed():	button_toggled(1)	
func _on_Button2_pressed():	button_toggled(2)
func _on_Button3_pressed():	button_toggled(3)
func _on_Button4_pressed():	button_toggled(4)
func _on_Button5_pressed():	button_toggled(5)
func _on_Button6_pressed():	button_toggled(6)
func _on_Button7_pressed():	button_toggled(7)
func _on_Button8_pressed():	button_toggled(8)
func _on_Button9_pressed():	button_toggled(9)

#actions

#func health(id,amount):
#	get_mynode(id).get_node("Health").value = amount + get_mynode(id).get_node("Health").value
#
#func manna(id,amount):
#	get_mynode(id).get_node("Soul").value = amount + get_mynode(id).get_node("Soul").value
#
