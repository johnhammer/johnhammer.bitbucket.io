extends AnimatedSprite

var parent
var myposition
var data
var isFeind
var oneHand = true

func Start(parentnode,x,y,_isFeind,_data):
	data = _data
	isFeind = _isFeind
	#Init(data)
	#SetupWeapon()
	
	parent = parentnode
	position = Vector2(x,y)
	myposition = Vector2(x,y)
	
	if isFeind:
		scale.x = -1
		$damage.scale.x = -1
	
	play("normal")
	#playAll("normal")
	$damage.visible = true
	var _a = connect("animation_finished", self, "animationend")

func animationend():
	var anim = animation
	if anim == "die":
		return
	if anim != "normal":
		play("normal")
		position = myposition
		if anim != "damage":
			parent.OnAnimationEnd(isFeind)

func damage(amount,iscrit):
	$damage.show_value(amount,iscrit)
	play("damage")

func die():
	play("die")

func attack(objectives,anim,melee,isBlock):
	if melee:
		var newX = objectives[0].position.x-80
		if isFeind: newX+=160
		position = Vector2(newX,objectives[0].position.y)
	play("punch")
	play(anim)

func show_weapon():
	pass
	
func hide_weapon():
	pass
