extends Node

func GetPower(action):
	if !"power" in action: return 0
	match int(action.power):
		6: return 1
		5: return 2
		4: return 3
		3: return 5
		2: return 8
		1: return 13

func GetWeaponBonus(weapon):
	if weapon.size()==0 || weapon[0]=="none":return 1
	var isOneHanded = Global.items_dictionary[weapon[0]].hands == 1
	match weapon[1]: #quality
		"rusty":
			if isOneHanded: return 1
			else: return 1.5
		"bronze":
			if isOneHanded: return 2
			else: return 2.5
		"iron":
			if isOneHanded: return 3
			else: return 5
		"steel":
			if isOneHanded: return 5
			else: return 7.5
		"carbonite":
			if isOneHanded: return 8
			else: return 10
		"legendary":
			if isOneHanded: return 13
			else: return 15

func SoulBonus(spi,sou):
	if sou<spi:return 1
	if spi<sou<2*spi:return 0.75
	if spi<sou<4*spi:return 0.5
	return 0.25

func GetDummyTgt():
	return{
		"sou":0,
		"prc":0,
		"spd":1,
		"armor":"none"
	}

func ArmorBonus(armor,type):
	for bonus in armor:
		if bonus == type:
			return 0.75
	return 1

func HasState(data,state):
	if "states" in data:
		if state in data.states:
			return true
	return false

func ProjectileDmg(me,lvl,plus,tgt,type):
	var dmg = me.atk * ArmorBonus(tgt.armor,type)
	if HasState(me,"forge"): dmg *= 2
	if !plus: return 2 * dmg
	else: return 5 * dmg

func WeaponDmg(me,lvl,weapon,tgt,type):
	var dmg = me.atk * GetWeaponBonus(weapon) * ArmorBonus(tgt.armor,type)
	if HasState(me,"forge"): dmg *= 2
	return dmg
	
func MagicDmg(me,lvl,tgt):
	return me.spi * lvl * SoulBonus(me.spi,tgt.sou)
	
func SpiritDmg(me,lvl,tgt,type):
	return me.spi * lvl * ArmorBonus(tgt.armor,type)

func GetDamage(action,me,tgt,weapon):
	if tgt == null: tgt = GetDummyTgt()
	
	var lvl = 1
	var power = GetPower(action) #action.lvl
	var dmg = 0
	match action.damage:
		"m": #magic only
			dmg = MagicDmg(me,power,tgt)
		"f": #punch
			dmg = me.atk * lvl
		"h": #hit
			dmg = WeaponDmg(me,lvl,weapon,tgt,"hit")
		"s": #stab
			dmg = WeaponDmg(me,lvl,weapon,tgt,"stab")
		"c": #cut
			dmg = WeaponDmg(me,lvl,weapon,tgt,"cut")
		"mh": #hit
			dmg = 0.5*MagicDmg(me,power,tgt) + 0.5 * me.atk * lvl * ArmorBonus(tgt.armor,"hit")
		"ms": #stab
			dmg = 0.5*MagicDmg(me,power,tgt) + 0.5 * me.atk * lvl * ArmorBonus(tgt.armor,"stab")
		"mc": #cut
			dmg = 0.5*MagicDmg(me,power,tgt) + 0.5 * me.atk * lvl * ArmorBonus(tgt.armor,"cut")
		"sh": #hit
			dmg = SpiritDmg(me,power,tgt,"hit")
		"ss": #stab
			dmg = SpiritDmg(me,power,tgt,"stab")
		"sc": #cut
			dmg = SpiritDmg(me,power,tgt,"cut")
		"ph": #hit
			dmg = ProjectileDmg(me,lvl,false,tgt,"hit")
		"ps": #stab
			dmg = ProjectileDmg(me,lvl,false,tgt,"stab")
		"pc": #cut
			dmg = ProjectileDmg(me,lvl,false,tgt,"cut")
		"pph": #hit
			dmg = ProjectileDmg(me,lvl,true,tgt,"hit")
		"pps": #stab
			dmg = ProjectileDmg(me,lvl,true,tgt,"stab")
		"ppc": #cut
			dmg = ProjectileDmg(me,lvl,true,tgt,"cut")
	
	return dmg

func GetCost(action,me):
	if !"power" in action || action.melee==1: return 0
	return GetPower(action.power)*me.spi

func GetTurns(slowest,me,isFirstTurn):
	if me.spd > slowest.spd*4:
		if isFirstTurn: return 2
		else: return 4
	elif me.spd > slowest.spd*3:
		if isFirstTurn: return 2
		else: return 3
	elif me.spd > slowest.spd*2:
		return 2
	else:
		return 1

func GetEffectDuration(action,me,tgt):
	var lvl = 1 #action.lvl
	
	return SoulBonus(me.spi,tgt.sou) * 4

func GetCriticalChance(action,me,tgt):
	return 0

func GetSpeed(action,me):
	return 1

# Tech Tree

func GetMoveShortDescription(action):
	var result = ""
	if action.target == "b":
		result = "Defends all allies"
	else:
		var target = ""
		match action.target:
			"1e": target ="one enemy"
			"1f": target ="one ally"
			"s": target ="yourself"
			"3e": target ="all enemies"
			"3f": target ="all allies"
		match action.damage:
			"m": #magic only
				result = "Attacks "+target+" with spirit damage"
			"f": #punch
				result = "Attacks "+target+" with hit damage"
			"h": #hit
				result = "Hits "+target+" with weapon damage"
			"s": #stab
				result = "Stabs "+target+" with weapon damage"
			"c": #cut
				result = "Cuts "+target+" with weapon damage"
			"mh": #hit
				result = "Attacks "+target+" with spirit hit damage"
			"ms": #stab
				result = "Attacks "+target+" with spirit stab damage"
			"mc": #cut
				result = "Attacks "+target+" with spirit cut damage"
			"sh": #hit
				result = "Hits "+target+" with spirit damage"
			"ss": #stab
				result = "Stabs "+target+" with spirit damage"
			"sc": #cut
				result = "Cuts "+target+" with spirit damage"
			"e":			
				result = "Gives [color=red]"+action.state+"[/color] to "+target
	return result

func GetMoveDescription(action,me,techtree):

	var oldDmg = GetDamage(action,me,null,me.weapon)
	var newme = me.duplicate()
	techtree.learn(action,newme)
	var newDmg = GetDamage(action,newme,null,newme.weapon)
	
	var result = GetMoveShortDescription(action)+"\n\n"

	if oldDmg==newDmg: result+="Damage:"+String(oldDmg)+"\n"
	else: result+="Damage:[color=red]"+String(oldDmg)+"[/color]"+String(newDmg)+"\n"
	
	#result+="Soul cost: 4 5"
	#result+="Speed: [color=red]4[/color] 5"
	#result+="Precision: [color=red]4[/color] 5"
	#result+="Duration: [color=red]4[/color] 5"
	
	return result


			
	
