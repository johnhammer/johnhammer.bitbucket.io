extends Node

var player = null

var colors_dictionary
var items_dictionary
var npcs_dictionary
var enemies_dictionary
var dialog_dictionary
var menu_dictionary
var moves_dictionary
var techtree_dictionary
var shops_dictionary
var chests_dictionary
var encounters_dictionary
var loots_dictionary

func LoadData():
	dialog_dictionary = load_json("dialog")
	menu_dictionary = load_json("menu")
	npcs_dictionary = load_json("npcs")
	items_dictionary = load_json("items")
	colors_dictionary = load_json("colors")
	moves_dictionary = load_json("moves")
	enemies_dictionary = load_json("enemies")
	techtree_dictionary = load_json("techtree")
	shops_dictionary = load_json("shops")
	chests_dictionary = load_json("chests")
	encounters_dictionary = load_json("encounters")
	loots_dictionary = load_json("loots")

enum Direction {
	Right,
	Left,
	Up,
	Down
}

var gold = 0
var xp = 0
var xpToNextLevel = 8
var inventory = []
var party = []
var skills = {}
var ExtraDoor = null #for 2 door scenarios like caves
var EquipedWeapon = null
var EquipedArmor = null
var EquipedShield = null
var EquipedItem = null
var EquipedGunParts = {}

func UpLevel():
	for member in Global.party:
		member.lvl = member.lvl+1
		member.maxhea = member.maxhea+4
		member.hea = member.hea+4
		member.maxsou = member.maxsou+4
		member.sou = member.sou+4
		member.atk = member.atk+2
		member.spi = member.spi+2
		member.prc = member.prc+1
		member.spd = member.spd+1
	xp = xp-xpToNextLevel
	xpToNextLevel = xpToNextLevel*2
	if (xp>xpToNextLevel): UpLevel()

func HasPartyMember(name):
	for member in Global.party:
		if member.name.to_lower()==name.to_lower():return true
	return false

func GiveItemQuality(item,quality):
	var obj = {"item":item,"color":quality,"equiped":false}
	inventory.push_back(obj)
	
func GiveItem(item):
	var obj = {"item":item}
	inventory.push_back(obj)

func GetNpcFighter(key):
	var npc = Global.enemies_dictionary[key].duplicate()
	npc.maxsou = npc.sou
	npc.maxhea = npc.hea
	if "custom" in npc:
		return npc
	
	if "npc" in npc:
		key = npc.npc
	npc.name = Global.npcs_dictionary[key].name
	npc.size = Global.npcs_dictionary[key].size
	npc.hair = Global.npcs_dictionary[key].hair
	npc.head = Global.npcs_dictionary[key].head
	npc.chest = Global.npcs_dictionary[key].chest
	npc.arms = Global.npcs_dictionary[key].arms
	npc.legs = Global.npcs_dictionary[key].legs
	npc.capa = Global.npcs_dictionary[key].capa
	if "brigandine" in Global.npcs_dictionary[key]:
		npc.brigandine = Global.npcs_dictionary[key].brigandine
	return npc

func Wait(s):
	yield(get_tree().create_timer(s), "timeout")

func load_json(name):
	var res = null
	var file = File.new()
	file.open("res://data/"+name+".json", file.READ)
	var text = file.get_as_text()
	var result_json = JSON.parse(text)
	if result_json.error == OK:  # If parse OK
		res = result_json.result
	else:  # If parse has errors
		print("Error: ", result_json.error)
		print("Error Line: ", result_json.error_line)
		print("Error String: ", result_json.error_string)
	file.close()
	return res

func ArrayToColor(color):
	return Color(color[0]/255.0, color[1]/255.0, color[2]/255.0)
	
func ColorToArray(color):
	return [color.r*255,color.g*255,color.b*255]

func GetColor(key):
	if key=="random":
		return [Random.Ranges(0,255),Random.Ranges(0,255),Random.Ranges(0,255)]
	elif key =="pastel":
		return ColorToArray(Color.from_hsv(Random.Float(), 0.2, 0.4))
	return colors_dictionary[key]

static func delete_children(node):
	for n in node.get_children():
		node.remove_child(n)
		n.queue_free()

func save_game():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	var data
	var player = get_tree().get_root().get_node("main/overworld/overworld/Nodes/player")
	
	#inventory
	save_game.store_line(to_json(inventory)) 
	#party
	save_game.store_line(to_json(party)) 
	#worldmap position
	data = {"x":player.position.x,"y":player.position.y}
	save_game.store_line(to_json(data))
	
	save_game.close()

func start_game():
	var main = get_tree().get_root().get_node("main")
	LoadData()
	main.StartGame()

func load_game():
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		return # Error! We don't have a save to load.
	save_game.open("user://savegame.save", File.READ)
	
	var main = get_tree().get_root().get_node("main")

	main.prev_scenario = "worldmap_full"
	main.door(main.prev_scenario)
	
	yield(get_tree().create_timer(0.1), "timeout")
	
	var player = get_tree().get_root().get_node("main/overworld/overworld/Nodes/player")
	var data
	
	#inventory
	data = parse_json(save_game.get_line())
	inventory = data
	#party
	data = parse_json(save_game.get_line())
	party = data
	#worldmap position
	data = parse_json(save_game.get_line())
	player.position.x = data["x"]
	player.position.y = data["y"]
	
	save_game.close()
