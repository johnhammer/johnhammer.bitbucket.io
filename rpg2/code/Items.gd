extends Node

func HasItem(entry):
	for item in Global.inventory:
		if item.item == entry:
			return true
	return false

func Remove(entry):
	var i = 0
	for item in Global.inventory:
		if item.item == entry:
			Global.inventory.remove(i)
			return
		i+=1

func Use(entry):
	var itemdata = Global.items_dictionary[entry.item]
	for member in Global.party:
		if "heals" in itemdata:
			member.hea += itemdata.heals
			if member.hea>member.maxhea:
				member.hea = member.maxhea
		if "mana" in itemdata:
			member.sou += itemdata.mana
			if member.sou>member.maxsou:
				member.sou = member.maxsou

func Unequip(entry):
	var itemdata = Global.items_dictionary[entry.item]
	match itemdata.type:
		"armor":
			Global.EquipedArmor=null
			Global.party[0].chest = ["normal","pale_leather"]
			Global.party[0].armor = []
		"weapon","gun","lightsphere":
			if Global.EquipedWeapon.item == "gun":
				for part in Global.EquipedGunParts:
					Global.EquipedGunParts[part].equiped = false
					Unequip(Global.EquipedGunParts[part])
			Global.EquipedWeapon = null
			Global.party[0].weapon = []
		"shield":
			Global.EquipedShield = null
			Global.party[0].shield = []
		"item":
			Global.EquipedItem = null
			Global.party[0].item = []
		"gun_part":
			Global.EquipedGunParts.erase(entry.item)
			#Global.party[0].gun_parts.erase(entry.item)
		_:return
	entry.equiped = false
	
func Equip(entry):
	var itemdata = Global.items_dictionary[entry.item]
	match itemdata.type:
		"armor":
			if Global.EquipedArmor!=null:
				Global.EquipedArmor.equiped = false
			Global.EquipedArmor = entry
			if entry.item != "brigandine":
				EquipPiece(Global.party[0].chest,entry)
			else:
				Global.party[0].brigandine="black"
				EquipPiece(Global.party[0].chest,entry)
			Global.party[0].armor=itemdata.bonus
		"weapon","gun","lightsphere":
			if Global.EquipedWeapon!=null:
				Global.EquipedWeapon.equiped = false
				if Global.EquipedWeapon.item == "gun":
					Unequip(Global.EquipedWeapon)
			Global.EquipedWeapon = entry
			EquipPiece(Global.party[0].weapon,entry)
		"shield":
			if Global.EquipedShield!=null:
				Global.EquipedShield.equiped = false
			Global.EquipedShield = entry
			EquipPiece(Global.party[0].shield,entry)
		"item":
			if Global.EquipedItem!=null:
				Global.EquipedItem.equiped = false
			Global.EquipedItem = entry
			Global.party[0].item = entry.item
		"gun_part":
			if entry.item in Global.EquipedGunParts:
				Global.EquipedGunParts[entry.item].equiped = false
			Global.EquipedGunParts[entry.item] = entry
			Global.EquipedGunParts[entry.item].equiped = true
		_:return
	entry.equiped = true

####

func GetItemPrice(item):
	var itemdata = Global.items_dictionary[item.item]
	var price = itemdata.price
	if "color" in item:
		match item.color:
			"rusty": price*=1
			"bronze": price*=2
			"iron": price*=4
			"steel": price*=8
			"carbonite": price*=16
			"legendary": price*=32
	return price

func EquipPiece(place,piece):
	if place.size()==0: 
		place.push_back(piece.item)
		place.push_back(piece.color)
	else:
		place[0]=piece.item
		place[1]=piece.color

func GetColoredName(entry):
	var color = entry.color.capitalize()
	if color=="rusty":
		if entry.item=="club" || entry.item=="staff":
			color = "wooden"
		if entry.item=="gambeson" || entry.item=="brigandine":
			color = "leather"
	return color + " " + entry.item

func GetArmorDescription(itemdata,_entry):
	var result = "[table=3]"+ \
			"[cell]Price [/cell]"+ \
			"[cell]"+String(itemdata.price)+"[/cell]"+ \
			"[cell]gold [/cell]"
	for type in itemdata.bonus:
		result += "[cell]Bonus against [/cell]"+ \
			"[cell]"+type+"[/cell]"+ \
			"[cell][/cell]"
	result += "[/table]"
	return result
	
func GetWeaponDescription(itemdata,_entry):
	var result = "[table=3]"+ \
			"[cell]Price [/cell]"+ \
			"[cell]"+String(itemdata.price)+"[/cell]"+ \
			"[cell]gold [/cell]"+ \
			"[cell]Moves [/cell]"+ \
			"[cell]"+String(itemdata.bonus)+"[/cell]"+ \
			"[cell][/cell]"
	result += "[/table]"
	return result
	
func GetProjectileDescription(itemdata,_entry):
	var result = "[table=3]"+ \
			"[cell]Price [/cell]"+ \
			"[cell]"+String(itemdata.price)+"[/cell]"+ \
			"[cell]gold [/cell]"+ \
			"[cell]Moves [/cell]"+ \
			"[cell]"+String(itemdata.bonus)+"[/cell]"+ \
			"[cell][/cell]"
	result += "[/table]"
	return result

func GetFoodDescription(itemdata,_entry):
	var result = "[table=3]"+ \
			"[cell]Price [/cell]"+ \
			"[cell]"+String(itemdata.price)+"[/cell]"+ \
			"[cell]gold [/cell]"
	result += "[/table]"
	return result

func GetBookDescription(itemdata,_entry):
	var result = "[table=3]"+ \
			"[cell]Price [/cell]"+ \
			"[cell]"+String(itemdata.price)+"[/cell]"+ \
			"[cell]gold [/cell]"
	result += "[/table]"
	return result

func SetName(inventory,name):
	inventory.get_node("Text").text = name

func SetDescription(inventory,desc):
	inventory.get_node("Description").bbcode_text = desc

func ShowEquip(inventory,equiped):
	if !equiped:
		inventory.get_node("Equip").visible = true
	else:
		inventory.get_node("Unequip").visible = true
	
func ShowUse(inventory):
	inventory.get_node("Use").visible = true

func LoadItemInfo(inventory,entry):
	var itemdata = Global.items_dictionary[entry.item]
	inventory.get_node("Throw").visible = true
	inventory.get_node("Equip").visible = false
	inventory.get_node("Unequip").visible = false
	inventory.get_node("Use").visible = false
	match itemdata.type:
		"armor":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,GetColoredName(entry))
			SetDescription(inventory,GetArmorDescription(itemdata,entry))
		"weapon":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,GetColoredName(entry))
			SetDescription(inventory,GetWeaponDescription(itemdata,entry))
		"gun_part":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,entry.item)
			SetDescription(inventory,itemdata.desc)
		"gun":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,entry.item)
			SetDescription(inventory,itemdata.desc)
		"lightsphere":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,entry.item)
			SetDescription(inventory,itemdata.desc)
		"shield":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,GetColoredName(entry))
			SetDescription(inventory,itemdata.desc)
		"projectile":
			SetName(inventory,entry.item)
			SetDescription(inventory,GetProjectileDescription(itemdata,entry))
		"food":
			ShowUse(inventory)
			SetName(inventory,entry.item)
			SetDescription(inventory,GetFoodDescription(itemdata,entry))
		"item":
			ShowEquip(inventory,entry.equiped)
			SetName(inventory,entry.item)
			SetDescription(inventory,itemdata.desc)		
		"medicine":
			ShowUse(inventory)
			SetName(inventory,entry.item)
			SetDescription(inventory,itemdata.desc)
		"book":
			ShowUse(inventory)
			SetName(inventory,entry.item)
			SetDescription(inventory,GetBookDescription(itemdata,entry))
		"special":
			SetName(inventory,entry.item)
			SetDescription(inventory,itemdata.desc)
	
