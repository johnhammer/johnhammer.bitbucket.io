extends Node

func Int() :
	return randi()

func Bool():
	return Ranges(0,1)

func Float() :
	return randf()

func Ranges(start,end) :
	var rangeSize = end+1 - start
	var randomUnder1 = Float()
	return start + floor(randomUnder1 * rangeSize)

func FloatRange(start,end) :
	var result = Float()
	return result * (end - start) + start

func Chance(chance):
	var val = Ranges(1,chance)
	return val == chance

func Choice(array) :
	if(array.size()==0):return null
	return array[Ranges(0, array.size()-1)]

func WChoice(array) :    
	var val = Ranges(0,100)
	var sum = 0
	for i in array:
		sum += i.probability
		if(val<=sum):
			return i
			
func Colors():
	return Color(FloatRange(0.0,1.0),
		FloatRange(0.0,1.0),
		FloatRange(0.0,1.0))
