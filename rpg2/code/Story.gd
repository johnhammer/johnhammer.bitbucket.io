extends Node

onready var main = get_node("..")
onready var interface = get_node("../interface")
onready var overworld = get_node("../overworld")

func DoorEvent(name): Event("door_"+name)
func EndOfBattleEvent(name): Event("win_"+name)
func PathEnd(name): Event("path_end_"+name)

func Condition(key):
	var parts = key.split("#")
	if (parts[0]=="not"):
		var cond = ""
		for i in range(1,parts.size()):
			cond += parts[i]+"#"
		return !Condition(cond)
	if (parts[0]=="hasitem"):
		return Items.HasItem(parts[1])
	if (parts[0]=="hasgold"):
		return Global.gold >= int(parts[1])
	if (parts[0]=="hascompanion"):
		return Global.HasPartyMember(parts[1])
	if (parts[0]=="variable"):
		return State[parts[1]]
	
	return CustomCondition(key)

func SpecialEvent(key):
	var parts = key.split("#")
	if (parts[0]=="shop"):
		interface.OpenShop(Global.shops_dictionary[parts[1]])
		interface.get_node("dialog").HideDialogButton()
		return true

func RemovePartyMember(key):
	for member in Global.party:
		if member.name.to_lower()==key:
			Global.party.erase(member)

func AddPartyMember(key):
	Global.party.push_back(Global.GetNpcFighter(key))

##########################

var Events = {
	"inn_room":0
}
var State = {
	"beggar_given_bread":false,
}
	
func CustomCondition(key):
	match key:
		"tutorial_items_equiped":
			return Global.inventory[0].equiped && Global.inventory[1].equiped

	push_error("unknown condition "+key)
	return Global.inventory[999]

func Dialog(key):
	main.enter_dialog(null,key)
	interface.get_node("dialog").ShowDialog()

func Event(key):
	if SpecialEvent(key):return
	
	var children = overworld.get_children()
	var scenario = overworld.get_child(0)
	
	match key:
		"launch_character_creation":
			interface.get_node("dialogbutton").visible = false
			scenario.get_node("creator").visible = true
			scenario.get_node("creator").Refresh()
			scenario.get_node("Nodes/mirror").visible = false
			scenario.get_node("Nodes/widow").Enable()
		"end_character_creation":
			scenario.get_node("Nodes/player").human.Init(Global.party[0])
			scenario.get_node("BaseItems").set_cell(4,3,-1)
			scenario.get_node("BaseItems").set_cell(4,4,-1)
			scenario.get_node("collision").set_cell(4,3,-1)
			scenario.get_node("collision").set_cell(4,4,-1)
		"widow_gift":
			Global.GiveItemQuality("club","rusty")
			Global.GiveItemQuality("gambeson","rusty")
		"start_tutorial":			
			scenario.get_node("Paths/widow").Move()
			Events["tutorial"] = true
		"door_tornstad/widows_house":
			if "tutorial" in Events:
				scenario.get_node("Nodes/widow").Enable()
		"path_end_widow":
			scenario.get_node("Nodes/widow").dialog = "tutorial_1"
		"tutorial_fight":
			main.encounter_key = "widow"
			main.encounter()
		"battle_tutorial_hit":
			Dialog("tutorial_2")
		"beggar_given_bread":
			State.beggar_given_bread = true
			Items.Remove("bread")
		"finbar_fists":
			AddPartyMember("finbar")
			Global.party[0].weapon = []
			Global.party[1].weapon = []
			main.encounter_key = "tornstad_thugs_melee"
			main.encounter()
		"finbar_outside":
			AddPartyMember("finbar")
			main.encounter_key = "tornstad_thugs_knife"
			main.encounter()
		"win_tornstad_thugs_knife","win_tornstad_thugs_melee","finbar_nofight":
			Global.party[0].weapon = ["club","rusty"]
			Global.party[1].weapon = ["spear","rusty"]
			RemovePartyMember("finbar")
			Dialog("finbar_intro")
			interface.CloseLoot()
			Events["tornstad/tornstad_inn"]=1
			scenario.StoryEvent("tornstad/tornstad_inn",1)
			main.encounter_key = null
		"finbar_joins":
			AddPartyMember("finbar")
		"bandit_boss":
			main.encounter_key = "bandit_boss"
			main.encounter()
			interface.get_node("dialog").HideDialogButton()
		"win_bandit_boss":
			main.encounter_key = null
			scenario.get_node("Nodes/boss").queue_free()
			scenario.get_node("Nodes/npc2").queue_free()
			scenario.get_node("Nodes/npc3").queue_free()
			scenario.get_node("Nodes/npc4").queue_free()
			scenario.get_node("Nodes/npc5").queue_free()
		"fight_arima":
			main.encounter_key = "arima_wards"
			main.encounter()
			interface.get_node("dialog").HideDialogButton()
		"taken_by_arima":
			main.door("worldmap_full")
			yield(get_tree().create_timer(0.1), "timeout")
			scenario = overworld.get_child(0)
			scenario.get_node("Nodes/player").position = Vector2(2368,3520)
		"boat_clhadach":
			scenario.get_node("Paths/boat_zerua").Move()
			scenario.get_node("Nodes/boat_zerua").hasmoved = true
			scenario.get_node("Paths/boat_clhadach").Move()
			scenario.get_node("Nodes/boat_clhadach").hasmoved = true
		"path_end_boat_clhadach":
			main.door("zerua/ship")
		"path_end_zerua_boat_path":
			Dialog("arima_arrest_zerua")
		"to_zerua_boat":
			main.door("zerua/prison")
			yield(get_tree().create_timer(4), "timeout")
			Event("prison_door")
		"prison_door":
			scenario.get_node("items2").set_cell(31,15,-1)
			scenario.get_node("items2").set_cell(31,16,-1)
			scenario.get_node("collision").set_cell(31,16,-1)
			scenario.get_node("Nodes/finbar").queue_free()
			Dialog("zerua_intro_escape")
			Global.party.push_back(Global.GetNpcFighter("chara"))
		"prison_ward":
			main.encounter_key = "zerua_ward"
			main.encounter()
			interface.get_node("dialog").HideDialogButton()
		"win_zerua_ward":
			main.encounter_key = null
		"door_zerua/zerua":
			Dialog("zerua_final_escape")
		"door_free/drosia":
			Dialog("enter_drosia")
		"theodoros_pass":
			scenario.get_node("Nodes/theodoros").dialog = "theodoros_back"
			Global.GiveItem("tornstad C pass")
			Events["tornstad/tornstad"]=1 
		"tornstad_ward_c":
			if scenario.get_node("Nodes/ward1").position.x ==1472:
				scenario.get_node("Nodes/ward1").position.x -= 64
				scenario.get_node("Nodes/ward2").position.x += 64
		"kronstad_pay":
			Events["kronstad/kronstad"]=1
			scenario.StoryEvent("kronstad/kronstad",1)
			Global.gold -= 100
		"egil_bandits_fight":
			main.encounter_key = "egil_bandits_fight"
			main.encounter()
		"pirate_boss":
			main.encounter_key = "pirate_boss"
			main.encounter()
			interface.get_node("dialog").HideDialogButton()
		"win_pirate_boss":
			main.encounter_key = null
			scenario.get_node("Nodes/pirate").queue_free()
			scenario.get_node("Nodes/pirate2").queue_free()
			scenario.get_node("Nodes/pirate3").queue_free()
			scenario.get_node("Nodes/pirate4").queue_free()
			scenario.get_node("Nodes/pirate5").queue_free()
			Dialog("pirates_calafort")
			interface.CloseLoot()
		"taken_by_pirates":
			main.door("miestas/pirate_base")
			yield(get_tree().create_timer(4), "timeout")
			Dialog("pirate_base")
			scenario = overworld.get_child(0)
			scenario.get_node("items").set_cell(18,7,-1)
			scenario.get_node("items").set_cell(19,7,-1)
			scenario.get_node("MainDetail").set_cell(18,6,-1)
			scenario.get_node("MainDetail").set_cell(19,6,-1)
			scenario.get_node("collision").set_cell(18,7,-1)
			scenario.get_node("collision").set_cell(19,7,-1)
		"join_vita":
			AddPartyMember("vita")
			scenario.get_node("Nodes/vita").Disable()
		"give_gun":
			Global.GiveItemQuality("gun","carbonite")
		"boat_namas":
			scenario.get_node("Paths/boat_namas").Move()
			scenario.get_node("Nodes/boat_namas").hasmoved = true
		#"path_end_boat_namas":
			#scenario.get_node("Nodes/boat").Disable()
