extends Node2D

export var move_speed = 200
export var rot_speed = 0
export var ttl = -1

func Init(points):
	if points!=null:
		$Path.curve.clear_points()
		for point in points:
			$Path.curve.add_point(point)
	if ttl!=-1:
		yield(get_tree().create_timer(ttl), "timeout")
		queue_free()

func _physics_process(delta):
	if rot_speed>0:
		$Path/PathFollow2D/Sprite.rotation_degrees += rot_speed
	
	if get_node("Path/PathFollow2D").unit_offset>=0.9:
		queue_free()
	else:
		get_node("Path/PathFollow2D").offset += move_speed * delta
