extends Panel

func GetDataText(member):
	return "[table=2]"+\
	"[cell]Level[/cell][cell]0[/cell]"+\
	"[cell]Health[/cell][cell][color=red]"+String(member.hea)+"[/color]/"+String(member.maxhea)+"[/cell]"+\
	"[cell]Soul[/cell][cell][color=red]"+String(member.sou)+"[/color]/"+String(member.maxsou)+"[/cell]"+\
	"[/table][table=4]"+\
	"[cell]Attack[/cell][cell][color=red]"+String(member.atk)+" [/color][/cell]"+\
	"[cell]Spirit[/cell][cell][color=red]"+String(member.spi)+"[/color][/cell]"+\
	"[cell]Precision [/cell][cell][color=red]"+String(member.prc)+"[/color][/cell]"+\
	"[cell]Speed[/cell][cell][color=red]"+String(member.spd)+"[/color][/cell]"+\
	"[/table]"

func LoadItem(id,item):
	if item.size()<1:return
	
	var itemdata = Global.items_dictionary[item[0]]
	if itemdata.type == "book":
		$Items.add_item(item[0],load("res://art/inventory/book.png"))
	else:
		$Items.add_item(item[0],load("res://art/inventory/"+item[0]+".png"))
	if item.size()>1:
		var color = Global.ArrayToColor(Global.GetColor(item[1]))
		$Items.set_item_icon_modulate(id,color)

func Init(member):
	$Name.text = member.name
	$Data.bbcode_text = GetDataText(member)
	$Moves.text = ""
	for move in member.moves: 
		if move!="menu"&&move!="back":
			$Moves.text += move+"\n"
	$Items.clear()
	LoadItem(0,member.weapon)
	LoadItem(1,member.armor)
	$human.Init(Global.npcs_dictionary[member.name.to_lower()])
