extends TextureButton

var conversation = []
var phrase_index = 0
var closeonnext = false

func HideDialog():
	get_node("../dialogbutton").visible = false

func OpenDialog(npc,dialog):
	if dialog=="none":return
	get_node("../dialogbutton").visible = true
	conversation = Global.dialog_dictionary[dialog]
	phrase_index = 0
	closeonnext = false
#	if "next_dialog" in conversation[phrase_index]:
#		npc.dialog = conversation[phrase_index].next_dialog
#	else:
#		npc.dialog = ""
	next_phrase(npc)

func HideDialogButton():
	get_node("../dialogbutton").visible = false

func ShowDialog():
	get_node("../dialogbutton").visible = false
	visible = true
	get_node("../hamburger").visible = false
	get_node("../dropback").visible = true

func GetRichText(text):
	var hair = Global.party[0].hair[0]
	if hair!="woman":
		text = text.replace("HIM", "him")
		text = text.replace("HIS", "his")
		text = text.replace("HE", "he")
	else:
		text = text.replace("HIM", "her")
		text = text.replace("HIS", "her")
		text = text.replace("HE", "she")
	text = text.replace("ME", Global.party[0].name)	
	
	text = text.replace("/*", "[color=#ff0000]")
	text = text.replace("*/", "[/color]")
	return text

func load_speaker(speaker,npc):
	if speaker.name=="me":
		$name.text = ""
		$text.bbcode_text = ""
		$human.visible = false
		$cat.visible = false
	elif speaker.name=="cat":
		$human.visible = false
		$cat.visible = true
		$cat.modulate = Global.ArrayToColor(Global.colors_dictionary[npc.color])
		$name.text = "Linx cat"
	elif speaker.name=="sign":
		$human.visible = false
		$cat.visible = false
		$name.text = "Sign"
	else:
		$human.visible = true
		$cat.visible = false
		if "forcename" in speaker:
			$name.text = speaker.forcename
		else:
			$name.text = Global.npcs_dictionary[speaker.name].name
		$human.Init(Global.npcs_dictionary[speaker.name])
		$human.scale = Vector2(2,2)

func load_phrase(element,npc):
	if "event" in element:
		get_node("../../story").Event(element.event)
	if "close" in element:
		closeonnext = true
	if "name" in element:
		load_speaker(element,npc)
	else:
		get_node("..").CloseDialog()
		return
	if "text" in element:
		if element.name=="me":
			$me.text = element.text
			$me.visible = true
			$arima_text.visible = false
		elif "arima" in element:
			$me.visible = false
			$arima_text.visible = true
			$text.visible = false
			$arima_text.bbcode_text = GetRichText(element.text)
		else:
			$me.visible = false
			$arima_text.visible = false
			$text.visible = true
			$text.bbcode_text = GetRichText(element.text)
		$choice.visible = false
		
	elif "choices" in element:
		$text.visible = false
		$choice.visible = true
		$choice/first.visible = false
		$choice/second.visible = false
		$choice/third.visible = false
		$choice/fourth.visible = false
		
		
		
		
		if element.choices.size()>0 && (!"condition" in element.choices[0] || get_node("../../story").Condition(element.choices[0].condition)):
			$choice/first.visible = true
			$choice/first.text = element.choices[0].text
		if element.choices.size()>1 && (!"condition" in element.choices[1] || get_node("../../story").Condition(element.choices[1].condition)):
			$choice/second.visible = true
			$choice/second.text = element.choices[1].text
		if element.choices.size()>2 && (!"condition" in element.choices[2] || get_node("../../story").Condition(element.choices[2].condition)):
			$choice/third.visible = true
			$choice/third.text = element.choices[2].text
		if element.choices.size()>3 && (!"condition" in element.choices[3] || get_node("../../story").Condition(element.choices[3].condition)):
			$choice/fourth.visible = true
			$choice/fourth.text = element.choices[3].text

func get_next_phrase_index(element,index):
	if !"next" in element:
		return index+1
	var id = 0
	for phrase in conversation:
		if "id" in phrase && phrase.id == element.next:
			return id
		id+=1
	return index+1

func next_phrase(npc):
	if phrase_index < conversation.size() && !closeonnext:
		var element = conversation[phrase_index]
		if "condition" in element:
			if(!get_node("../../story").Condition(element.condition)):
				phrase_index+=1
				element = conversation[phrase_index]
				phrase_index+=1
			else:
				phrase_index = get_next_phrase_index(element,phrase_index)				
		else:
			phrase_index = get_next_phrase_index(element,phrase_index)
		load_phrase(element,npc)
	else:
		get_node("..").CloseDialog()

func _on_dialog_pressed():
	next_phrase(null)

func on_choice(choice):
	phrase_index-=1
	phrase_index = get_next_phrase_index(choice,phrase_index)
	next_phrase(null)

func _on_first_pressed(): 
	on_choice(conversation[phrase_index-1].choices[0])
func _on_second_pressed(): 
	on_choice(conversation[phrase_index-1].choices[1])
func _on_third_pressed(): 
	on_choice(conversation[phrase_index-1].choices[2])
func _on_fourth_pressed(): 
	on_choice(conversation[phrase_index-1].choices[3])
