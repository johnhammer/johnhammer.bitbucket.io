extends CanvasLayer


var current_inventory_item = 0


func _ready():
	$dialogbutton.visible = false
	$dropback.visible = false
	$dialog.visible = false
	$inventory.visible = false
	$techtree.visible = false

##################################################################

#inventory
func LoadInventoryItem(index):
	$inventory/ItemList.select(index)
	current_inventory_item = index
	if Global.inventory.size()>0:
		var entry = Global.inventory[index]
		Items.LoadItemInfo($inventory,entry)

func _on_ItemList_item_selected(index):
	LoadInventoryItem(index)

func _on_Use_pressed():
	Items.Use(Global.inventory[current_inventory_item])
	_on_Throw_pressed()	
	
func _on_Throw_pressed():
	Global.inventory.remove(current_inventory_item)
	current_inventory_item = 0
	ReloadInventory($inventory/ItemList,Global.inventory)
	LoadInventoryItem(current_inventory_item)

func EquipmentReload():
	Global.player.human.Init(Global.party[0])
	
	ReloadInventory($inventory/ItemList,Global.inventory)
	LoadInventoryItem(current_inventory_item)

func _on_Unequip_pressed():
	Items.Unequip(Global.inventory[current_inventory_item])
	EquipmentReload()

func _on_Equip_pressed():
	Items.Equip(Global.inventory[current_inventory_item])
	EquipmentReload()

func ReloadInventory(itemlist,items):
	var id=0
	itemlist.clear()
	for item in items:
		var itemdata = Global.items_dictionary[item.item]
		if itemdata.type == "book":
			itemlist.add_item(item.item,load("res://art/inventory/book.png"))
		else:
			itemlist.add_item(item.item,load("res://art/inventory/"+item.item+".png"))
		if "color" in item:
			var color = Global.ArrayToColor(Global.GetColor(item.color))
			itemlist.set_item_icon_modulate(id,color)
		if "equiped" in item:
			if item.equiped:
				var bkg = Global.ArrayToColor(Global.GetColor("red_hair"))
				itemlist.set_item_custom_bg_color(id,bkg)
		id+=1

func OpenInventory():
	$dropback.visible = true
	$hamburger.visible = false
	$menu.visible = false
	$inventory.visible = true
	
	$inventory/Gold.text = "Gold "+String(Global.gold)
	
	ReloadInventory($inventory/ItemList,Global.inventory)
	if Global.inventory.size()>0:
		LoadInventoryItem(0)

func MoveItem(index,listA,listB,itemA,itemB,remove=true,add=true):
	var item = listA[index]
	if add: listB.push_back(item)
	if remove: listA.remove(index)
	ReloadInventory(itemB,listB)
	ReloadInventory(itemA,listA)


##################################################################

func OpenParty():
	$dropback.visible = true
	$hamburger.visible = false
	$menu.visible = false
	$party.visible = true
	
	$party/PartyMember1.visible=false
	$party/PartyMember2.visible=false
	$party/PartyMember0.Init(Global.party[0])
	if Global.party.size()>1: 
		$party/PartyMember1.Init(Global.party[1])
		$party/PartyMember1.visible=true
	if Global.party.size()>2: 
		$party/PartyMember2.Init(Global.party[2])
		$party/PartyMember2.visible=true

##################################################################

#shop

var shop_items
var shop_sell_items = []
var me_sell_items = []

func GetPrice():
	var gold = 0
	for item in shop_sell_items:
		gold-=Items.GetItemPrice(item)
	for item in me_sell_items:
		gold+=Items.GetItemPrice(item)
	return gold

func RefreshGold():
	var gold = GetPrice()
	$shop/Trade.disabled = gold*-1 > Global.gold
	$shop/goldchange.text = "Gold "+String(gold)

func OpenShop(_shop):
	shop_items = _shop.duplicate()
	$dropback.visible = true
	$hamburger.visible = false
	$menu.visible = false
	$shop.visible = true
	
	$shop/Gold.text = "Gold "+String(Global.gold)
	$shop/goldchange.text = "Gold 0"
	
	ReloadInventory($shop/myinventory,Global.inventory)
	ReloadInventory($shop/shopinventory,shop_items)

func _on_shop_myinventory_item_selected(index):
	MoveItem(index,
		Global.inventory,
		me_sell_items,
		$shop/myinventory,
		$shop/myitems)
	RefreshGold()

func _on_shop_myitems_item_selected(index):
	MoveItem(index,
		me_sell_items,
		Global.inventory,
		$shop/myitems,
		$shop/myinventory)
	RefreshGold()

func _on_shopinventory_item_selected(index):
	MoveItem(index,
		shop_items,
		shop_sell_items,
		$shop/shopinventory,
		$shop/shopitems,false)
	RefreshGold()

func _on_shopitems_item_selected(index):
	MoveItem(index,
		shop_sell_items,
		shop_items,
		$shop/shopitems,
		$shop/shopinventory,true,false)
	RefreshGold()

func _on_shop_Trade_pressed():
	var gold = GetPrice()
	Global.gold += gold
	$shop/Gold.text = "Gold "+String(Global.gold)
	for item in range(shop_sell_items.size()):
		MoveItem(0,
			shop_sell_items,
			Global.inventory,
			$shop/shopitems,
			$shop/myinventory)
	for item in range(me_sell_items.size()):
		MoveItem(0,
			me_sell_items,
			shop_items,
			$shop/myitems,
			$shop/shopinventory,true,false)

func _on_shop_Cancel_pressed():
	for item in range(shop_sell_items.size()):
		_on_shopitems_item_selected(0)
	for item in range(me_sell_items.size()):
		_on_shop_myitems_item_selected(0)

##################################################################

#loot
var loot 

func OpenLoot(_loot,isVictory=false):
	loot = _loot
	$dropback.visible = true
	$hamburger.visible = false
	$menu.visible = false
	$loot.visible = true
	
	if isVictory:
		$loot/VictoryLabel.text = "Victory"
	else:
		$loot/VictoryLabel.text = "Chest"
	
	$loot/xp.bbcode_text = "xp: [color=gray]"+String(Global.xp) \
		+" -> [/color]"+String(Global.xp+loot.xp)
	$loot/gold.bbcode_text = "gold: [color=gray]"+String(Global.gold) \
		+" -> [/color]"+String(Global.gold+loot.gold)
	
	Global.gold += loot.gold
	Global.xp += loot.xp
	if (Global.xp>Global.xpToNextLevel):
		Global.UpLevel()
	
	ReloadInventory($loot/ItemList,Global.inventory)
	ReloadInventory($loot/LootItemList,loot.items)

func _on_LootItemList_item_selected(index):
	MoveItem(index,
		loot.items,
		Global.inventory,
		$loot/LootItemList,
		$loot/ItemList)

func _on_loot_inventory_ItemList_item_selected(index):
	MoveItem(index,
		Global.inventory,
		loot.items,
		$loot/ItemList,
		$loot/LootItemList)

func _on_TakeAll_pressed():
	for item in range(loot.items.size()):
		_on_LootItemList_item_selected(0)

##################################################################


#menu
func CloseDialog():
	$hamburger.visible = true
	$dialog.visible = false
	$dropback.visible = false
	
func CloseOptions():
	$hamburger.visible = true
	$options.visible = false
	$dropback.visible = false
	
func CloseMenu():
	$hamburger.visible = true
	$menu.visible = false
	$dropback.visible = false

func CloseInventory():
	$hamburger.visible = true
	$inventory.visible = false	
	$dropback.visible = false
	
func CloseTechtree():
	$hamburger.visible = true
	$techtree.visible = false
	$dropback.visible = false

func CloseLoot():
	$hamburger.visible = true
	$loot.visible = false
	$dropback.visible = false
	
func CloseParty():
	$hamburger.visible = true
	$party.visible = false
	$dropback.visible = false
	
func CloseShop():
	_on_shop_Cancel_pressed()
	$hamburger.visible = true
	$shop.visible = false
	$dropback.visible = false	

func OpenMenu():
	$hamburger.visible = false
	$menu.visible = true
	$dropback.visible = true

func _on_techtree_pressed():
	$techtree.visible = true
	$dropback.visible = true
	$hamburger.visible = false
	$menu.visible = false
	$techtree.show()

func _on_options_pressed():
	$options.visible = true
	$dropback.visible = true
	$hamburger.visible = false
	$menu.visible = false

func _on_dropback_pressed():
	CloseDialog()
	CloseMenu()
	CloseInventory()
	CloseTechtree()
	CloseLoot()
	CloseOptions()
	CloseShop()
	CloseParty()

func _on_hamburger_pressed():
	OpenMenu()

func _on_inventory_pressed():
	OpenInventory()
	
func _on_party_pressed():
	OpenParty()

func _on_exit_pressed():
	_on_dropback_pressed()
	$intro.visible = true

func _on_dialogbutton_pressed():
	$dialog.ShowDialog()

func _on_up_button_down():#rotated180!
	if Global.player!=null:
		Global.player.Move(Global.Direction.Down)
func _on_right_button_down():
	if Global.player!=null:
		Global.player.Move(Global.Direction.Left) 
func _on_left_button_down():
	if Global.player!=null:
		Global.player.Move(Global.Direction.Right)
func _on_down_button_down():
	if Global.player!=null:
		Global.player.Move(Global.Direction.Up)

func _on_up_button_up():
	if Global.player!=null:
		Global.player.Stop()
func _on_right_button_up():
	if Global.player!=null:
		Global.player.Stop()
func _on_down_button_up():
	if Global.player!=null:
		Global.player.Stop()
func _on_left_button_up():
	if Global.player!=null:
		Global.player.Stop()

func start_battle():
	$mobilearrows.visible = false

func stop_battle(flee,loot):
	$mobilearrows.visible = true
	if !flee:
		OpenLoot(loot,true)

func is_worldmap(isworldmap): $menu/save.disabled = !isworldmap

func _on_save_pressed():
	Global.save_game()
	_on_dropback_pressed()

func _on_load_pressed():
	Global.load_game()
	_on_dropback_pressed()

func _on_main_newgame_pressed():
	Global.start_game()
	$intro.visible = false

func _on_main_loadgame_pressed():
	Global.start_game()
	yield(get_tree().create_timer(0.1), "timeout")
	$intro.visible = false
	_on_load_pressed()

func _on_main_options_pressed():
	_on_options_pressed()


