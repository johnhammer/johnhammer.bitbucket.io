extends Panel


func _on_GhostMode_toggled(on):
	var player = get_tree().get_root().get_node("main/overworld/overworld/Nodes/player")
	if on:
		player.get_node("collision").disabled= true #CHEATS
		player.speed = 500
	else:
		player.get_node("collision").disabled= false #CHEATS

func _on_BigArrows_toggled(on):
	if on:
		get_node("../mobilearrows").rect_scale = Vector2(2,2)
	else:
		get_node("../mobilearrows").rect_scale = Vector2(1,1)

func _on_GiveXpAndGold_toggled(button_pressed):
	Global.gold = 100000
	Global.xp = 100000

func _on_Sound_toggled(on):
	get_tree().get_root().get_node("main/sound").Toggle(on)


func _on_GiveMaxStats_toggled(button_pressed):
	Global.party[0].atk = 5000
	Global.party[0].hea = 5000
	Global.party[0].spi = 5000
