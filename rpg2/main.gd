extends Control

var battle
var overworld
var prev_scenario
var encounter_key = null
var active_battle = false

func StartGame():
	Global.gold = 0
	Global.xp = 0
	Global.inventory = []
	Global.party = []
	Global.skills = {}
	Global.ExtraDoor = null #for 2 door scenarios like caves
	Global.EquipedWeapon = null
	Global.EquipedArmor = null
	Global.EquipedShield = null
	Global.EquipedItem = null
	
	Global.party = [Global.GetNpcFighter("me")]
	$interface/techtree.Init(Global.party[0])
	
	battle = get_node('battle')
	overworld = get_node('overworld')
	yield(get_tree(), "idle_frame")
	
	#prev_scenario = "tornstad/widows_house_in"
	prev_scenario = "worldmap_full"
	start_world(prev_scenario)

#func _ready():
	#Global.LoadData()
	#StartGame()

func start_battle(feinds,img,canflee):
	active_battle = true
	overworld.get_child(0).get_node("Nodes/player").Block()
	overworld.hide()
	battle.show()
	battle.start(feinds,img,canflee)
	$interface.start_battle()

func stop_battle(flee,feinds):
	battle.hide()
	overworld.show()
	overworld.get_child(0).get_node("Nodes/player/Camera2D").current = true
	overworld.get_child(0).get_node("Nodes/player").blocked = false
	
	$interface.stop_battle(flee,GetLoot(feinds))
	$story.EndOfBattleEvent(encounter_key)
	active_battle = false

func Defeat():
	var scenarioResource = load("res://minigames/defeat.tscn")
	var scenario = scenarioResource.instance()
	Global.delete_children(self)
	add_child(scenario)

func start_world(origname):
	#extradoor 
	var parts = origname.split("#")
	var name = parts[0]
	if parts.size()>1:
		Global.ExtraDoor = parts[1]
	else: 
		Global.ExtraDoor = null
	#load world
	$interface.is_worldmap(name=="worldmap_full")
	var scenarioResource = load("res://scenarios/"+name+".tscn")
	var scenario = scenarioResource.instance()
	overworld.add_child(scenario)
	scenario.Init(Global.npcs_dictionary,prev_scenario,name)
	prev_scenario = name
	$story.DoorEvent(name)

#encounters
func set_encounters(name): encounter_key = name
func stop_encounters(): encounter_key = null

func encounter():
	if active_battle:return
	
	if encounter_key!=null:
		var encounter = Random.WChoice(Global.encounters_dictionary[encounter_key])
		var feinds = GetFeindsFromEncounter(encounter)
		start_battle(feinds,encounter.img,!"cannot_flee" in encounter)

func GetFeindsFromEncounter(encounter):
	var feinds = []
	for unit in encounter.units:
		var enemy = Global.GetNpcFighter(unit)
		enemy.id = unit
		enemy.maxhea = enemy.hea
		enemy.maxsou = enemy.sou
		feinds.push_back(enemy)
	return feinds

func GetLootItems(loots):
	var items = []
	for loot in loots:
		if Random.Chance(loot.probability):
			var item = {
				"item":loot.item,
				"equiped":false
			}
			if "color" in loot:
				item.color = loot.color
			if "minquality" in loot:
				item.quality = Random.Ranges(loot.minquality,loot.maxquality)
			items.push_back(item)
	return items

func GetLoot(feinds):
	var xp = 0
	var gold = 0
	var items = []
	for feind in feinds:
		xp += feind.data.xp
		gold += feind.data.gold
		items += GetLootItems(Global.loots_dictionary[feind.data.id])
	return {
		"xp":xp,
		"gold":gold,
		"items":items
	}

#interacions
func door(door):
	overworld.get_child(0).get_node("Nodes/player").removing = true
	var overworldChildren = overworld.get_children()
	for n in overworldChildren:
		overworld.remove_child(n)
		n.queue_free()
	call_deferred("start_world",door)

func enter_dialog(npc,dialog):
	$interface/dialog.OpenDialog(npc,dialog)

func exit_dialog():
	$interface/dialog.HideDialog()

func pathEnd(name):
	$story.PathEnd(name)

#methods
func load_json(name):
	var res = null
	var file = File.new()
	file.open("res://data/"+name+".json", file.READ)
	var text = file.get_as_text()
	var result_json = JSON.parse(text)
	if result_json.error == OK:  # If parse OK
		res = result_json.result
	else:  # If parse has errors
		print("Error: ", result_json.error)
		print("Error Line: ", result_json.error_line)
		print("Error String: ", result_json.error_string)
	file.close()
	return res
