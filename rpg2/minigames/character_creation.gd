extends Control

var isMale = true

func Refresh():	
	if isMale:
		Global.party[0].hair = ["normal","player_hair_color"]
	else:
		Global.party[0].hair = ["woman","player_hair_color"]
	Global.party[0].head = ["normal","player_skin_color"]
	Global.party[0].arms = ["normal","player_skin_color"]
	$Panel/human.Init(Global.party[0])
	$Panel/human.scale = Vector2(2.5,2.5)

func _on_hair_color_changed(color):
	Global.colors_dictionary["player_hair_color"] = Global.ColorToArray(color)
	Refresh()

func _on_sex_item_selected(id):
	if id==0:isMale=true
	else:isMale=false
	Refresh()

func _on_name_text_entered(new_text):
	Global.party[0].name = new_text

func _on_skin_color_changed(color):
	Global.colors_dictionary["player_skin_color"] = Global.ColorToArray(color)
	Refresh()

func _on_soul_color_changed(color):
	Global.colors_dictionary["player_soul_color"] = Global.ColorToArray(color)
	Refresh()

func _on_finish_pressed():	
	Global.party[0].name = $Panel/Control/name.text
	get_node("../../../story").Event("end_character_creation")
	visible=false
