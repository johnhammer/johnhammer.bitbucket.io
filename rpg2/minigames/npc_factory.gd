extends ScrollContainer

const NpcResource = preload("res://object/npc.tscn")

func _ready():
	
	Global.colors_dictionary = Global.load_json("colors")
	Global.npcs_dictionary = Global.load_json("npcs")
	
#	for data in Global.npcs_dictionary:
#		var npc  = NpcResource.instance()
#		$ItemList.add_item(data,npc)
#		npc.human.Init(Global.npcs_dictionary[data])
	
	
	var i = 0
	var j = 1
	for data in Global.npcs_dictionary:
		var npc  = NpcResource.instance()
		$ColorRect.add_child(npc)
		npc.position = Vector2(125*i,64*j)
		npc.human.Init(Global.npcs_dictionary[data])
		var label = Label.new()
		$ColorRect.add_child(label)
		label.set_position(Vector2(128*i,64*j+64))
		label.text = data
		i+=1
		if i%8==0:
			i=0
			j+=2
