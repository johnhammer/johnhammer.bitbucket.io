extends Node2D

var grass = preload("res://art/monsters/grass.png")


const ShooterResource = preload("res://minigames/target.tscn")

var positions = []

func _ready():
	for i in range(10):
		var sprite = Sprite.new()
		sprite.texture = grass
		var pos = Vector2(Random.Ranges(100,900),Random.Ranges(200,500))
		positions.push_back(pos)
		sprite.position = pos
		sprite.z_index = 100
		add_child(sprite)
	CreateTargets()

var ticks = 0
var life = 150

func _process(delta):
	if life<30:
		print(points)
		return
	ticks += 1
	if ticks>life:
		CreateTargets()
		ticks = 0
		life -= 5
		

func CreateTargets():
	for i in range(2):
		var target = ShooterResource.instance()
		target.position = Random.Choice(positions)
		target.life = life
		target.Set(Random.Bool())
		add_child(target)

var points = 0

func Hit(item):
	if item.type: points-=1
	else: points+=1
	item.queue_free()
	
