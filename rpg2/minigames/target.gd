extends Sprite

var lince = preload("res://art/monsters/lince.png")
var duck = preload("res://art/monsters/duck.png")

func _unhandled_input(event):
	if event is InputEventMouseButton and event.pressed and not event.is_echo() and event.button_index == BUTTON_LEFT:
		if get_rect().has_point(get_local_mouse_position()):
			
			get_parent().Hit(self)
			
			get_tree().set_input_as_handled()

var ticks = 0
var life = 2000
var type

func Set(_type):
	type = _type
	if(type):texture = lince
	else:texture = duck

func _process(delta):
	ticks += 1
	if ticks>life:
		queue_free()

	
