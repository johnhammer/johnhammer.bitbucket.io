extends Control

var current_type
var current_name
var me
var treelevel = 1

func learn(action,me):
	pass

func show():
	$menu/exp.text = String(Global.xp)

func CheckDependencies(category,name,me):
	for dependency in Global.techtree_dictionary[category][name].dep:
		var tech = dependency.keys()[0]
		var value = me.techtree[category][tech]
		var requiredValue = dependency[tech]
		if value<requiredValue: 
			return false
	return true

func Refresh(category,nodes,me):
	
	for node in nodes.get_children():
		if node.name == "arrows":continue
		node.disabled = !CheckDependencies(category,node.name,me)

func setup_buttons(category,nodes,me):
	for node in nodes.get_children():
		if node.name == "arrows":continue
		if !node.name in me.techtree[category]:
			me.techtree[category][node.name]=0
		else:
			node.get_child(0).text = me.techtree[category][node.name]
		node.disabled = !CheckDependencies(category,node.name,me)
		node.connect("pressed", self, "load_tech",[category,node.name])

func Init(_me):
	me = _me
	me.techtree = {
		"flame":{"main":0},
		"weapon":{"main":0},
		"crystal":{"main":0},
		"spirit":{"main":0},
		"melee":{"main":0},
		"arcane":{"main":0}
	}	
	setup_buttons("flame",$tree/flame,me)
	setup_buttons("weapon",$tree/weapon,me)
	setup_buttons("crystal",$tree/crystal,me)
	setup_buttons("spirit",$tree/spirit,me)
	setup_buttons("melee",$tree/melee,me)
	setup_buttons("arcane",$tree/arcane,me)

func load_tech(type,name):
	current_type = type
	current_name = name
	var current = Global.techtree_dictionary[type][name]
	$menu/title.text = type.capitalize() + " "+name.capitalize()
	$menu/subtitle.text = ""
	$menu/description.bbcode_text = ""
	var cost = GetTechCost()
	$menu/invest.disabled = cost>Global.xp
	$menu/invest.text = String(cost)+"xp"
	match current.type:
		"tech":
			$menu/subtitle.text = "Unlocks other cells"
		"final":
			$menu/title.text = "Final moves"
			$menu/subtitle.text = "Unlock and bonus "+current.bonus
			$menu/description.bbcode_text = "[table=2]"
			for move in current.moves:
				var m = Global.moves_dictionary[move]
				$menu/description.bbcode_text += "[cell][color=red]"+move.capitalize()+"[/color][/cell]"+ \
				"[cell]"+Calculator.GetMoveShortDescription(m)+"\n[/cell]"
			$menu/description.bbcode_text += "[/table]"
		"bonus":
			$menu/subtitle.text = "Extra "+current.bonus+" point"
		"extra":
			$menu/subtitle.text = "Extra "+current.bonus+" for moves"
			$menu/description.bbcode_text = "[table=1]"
			for move in current.moves:
				$menu/description.bbcode_text += "[cell]"+move.capitalize()+"[/cell]"
			$menu/description.bbcode_text += "[/table]"
		"move":
			var move = Global.moves_dictionary[name]
			$menu/subtitle.text = "Unlock and bonus "+current.bonus
			$menu/description.bbcode_text = Calculator.GetMoveDescription(move,Global.party[0],self)

func _on_invest_pressed():
	Global.xp -= GetTechCost()
	$menu/exp.text = String(Global.xp)
	if current_name=="main":
		me.techtree[current_type][current_name] = treelevel
		$tree/main.get_node(current_type).get_node("number").text = \
			String(treelevel)
		treelevel *= 2
		if current_type!="weapon":
			Global.party[0].moves.push_back(current_type)
		on_menu_item_pressed(current_type)
	else:
		if me.techtree[current_type][current_name]==0:
			Global.party[0].moves.push_back(current_name)
		me.techtree[current_type][current_name] += 1
		$tree.get_node(current_type).get_node(current_name).get_node("number").text = \
			String(me.techtree[current_type][current_name])
		Refresh(current_type,$tree.get_node(current_type),me)

func on_menu_item_pressed(name):
	current_type = name
	current_name = "main"
	var value = me.techtree[current_type][current_name]
	if value>0:
		$tree/main.visible = false
		$tree.get_node(current_type).visible = true
	else:
		$menu/title.text = name
		$menu/subtitle.text = "Unlock tree"
		$menu/description.bbcode_text = ""
		var cost = GetTechCost()
		$menu/invest.disabled = cost>Global.xp
		$menu/invest.text = String(cost)+"xp"

func GetTechCost():
	if current_name == "main":
		return 10*treelevel
	else:
		var multiplier = me.techtree[current_type]["main"]
		return 1*multiplier*pow(2,me.techtree[current_type][current_name]+1)

func _on_weapon_pressed(): on_menu_item_pressed("weapon")
func _on_light_pressed(): on_menu_item_pressed("spirit")
func _on_melee_pressed(): on_menu_item_pressed("melee")
func _on_flame_pressed(): on_menu_item_pressed("flame")
func _on_dark_pressed(): on_menu_item_pressed("crystal")
func _on_arcane_pressed(): on_menu_item_pressed("arcane")

func _on_back_pressed():
	if $tree/main.visible:
		get_tree().get_root().get_node("main/interface").CloseTechtree()
		return
	$tree/main.visible = true
	$tree/weapon.visible = false
	$tree/crystal.visible = false
	$tree/spirit.visible = false
	$tree/flame.visible = false
	$tree/melee.visible = false
	$tree/arcane.visible = false
