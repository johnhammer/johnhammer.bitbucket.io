extends KinematicBody2D

export var doorOut = ""
export (int) var speed = 300
export (Vector2) var cameraLimit = null
export (String) var color = "white"
var hasmoved = false
var blocked = true
var step = 0

var velocity = Vector2()
var dir = Global.Direction.Down

func Block():
	Stop()
	blocked = true

func SetPos(pos):
	position = pos

func _ready():
	$human/sprite.modulate = Global.ArrayToColor(Global.GetColor(color))
	if cameraLimit!=null && cameraLimit.x!=0:
		$Camera2D.limit_top = 0
		$Camera2D.limit_left = 0
		$Camera2D.limit_right = cameraLimit.x*64
		$Camera2D.limit_bottom = cameraLimit.y*64

func _input(event):
	if blocked:return
	if Input.is_action_just_pressed('up'):
		Move(Global.Direction.Up)
	elif Input.is_action_just_pressed('right'):
		Move(Global.Direction.Right)
	elif Input.is_action_just_pressed('left'):
		Move(Global.Direction.Left)
	elif Input.is_action_just_pressed('down'):
		Move(Global.Direction.Down)
	elif Input.is_action_just_released('up'):
		Stop()
	elif Input.is_action_just_released('right'):
		Stop()
	elif Input.is_action_just_released('left'):
		Stop()
	elif Input.is_action_just_released('down'):
		Stop()
	elif Input.is_action_just_released("ui_accept"):
		get_node("collision").disabled= true #CHEATS
		speed = 500
	elif Input.is_action_just_released("ui_cancel"):
		get_node("collision").disabled= false #CHEATS

func Move(d):
	if blocked:return
	dir = d
	velocity = Vector2()
	step+=1
	if dir == Global.Direction.Up:
		velocity.y -= 1
	elif dir == Global.Direction.Right:
		velocity.x += 1
	elif dir == Global.Direction.Left:
		velocity.x -= 1
	elif dir == Global.Direction.Down:
		velocity.y += 1
	velocity = velocity.normalized() * speed

func Stop():
	if blocked:return
	velocity = Vector2()

func _physics_process(_delta):
	if blocked:return
	if velocity.length() == 0:
		match dir:
			Global.Direction.Right:
				$human/sprite.play("r_right")
			Global.Direction.Left:
				$human/sprite.play("r_left")
			Global.Direction.Down:
				$human/sprite.play("r_down")
			Global.Direction.Up:
				$human/sprite.play("r_up")
	else:
		match dir:
			Global.Direction.Right:
				$human/sprite.play("r_right")
			Global.Direction.Left:
				$human/sprite.play("r_left")
			Global.Direction.Down:
				$human/sprite.play("r_down")
			Global.Direction.Up:
				$human/sprite.play("r_up")
		hasmoved = true
	velocity = move_and_slide(velocity)
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.name == "boat_collision":
			Land(collision.position)

func npc_collision():pass
func door_collision():pass

var removing = false

func _on_VisibilityNotifier2D_viewport_exited(_viewport):
	if blocked:return
	if removing:return
	if doorOut=="WRAP":
		if position.x<0: position.x = cameraLimit.x*128
		elif position.x>cameraLimit.x*128: position.x = 0
		elif position.y<0: position.y = cameraLimit.y*128
		elif position.y>cameraLimit.y*128: position.y = 0
	elif doorOut!="":
		var main = get_node("../../../..")
		if main!=null: main.door(doorOut)

func Enable():
	$Camera2D.current = true
	blocked = false
	
func Disable():
	$Camera2D.current = false
	blocked = true

var player = null


func Land(position):
	if !hasmoved:return
	yield(get_tree().create_timer(0.3), "timeout")
	var cell = get_node("../../boat_collision").world_to_map(position)
	match dir:
		Global.Direction.Right:
			if get_node("../../boat_collision").get_cell(cell.x,cell.y) == -1:
				cell.x += 1
		Global.Direction.Left:
			cell.x -= 1
		Global.Direction.Down:
			if get_node("../../boat_collision").get_cell(cell.x,cell.y) == -1:
				cell.y += 1
		Global.Direction.Up:
			cell.y -= 1
	
	if get_node("../../collision").get_cell(cell.x,cell.y) != -1:
		return
	
	var pos = get_node("../../boat_collision").map_to_world(cell)
	pos.x+=32
	pos.y+=32
	player.position = pos
	player.Enable()
	Disable()

func _on_Area2D_body_entered(body):
	if(body.has_method("npc_collision")):
		hasmoved = false
		player = body
		body.Disable()
		Enable()
		get_tree().get_root().get_node("main/story").Event(name)

