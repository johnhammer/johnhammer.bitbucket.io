extends Sprite

export var door = ""
var disabled = false

func _ready():
	if !visible: Disable()
	visible = false

func Deactivate():
	disabled = true

func Enable():
	disabled = false
	$Area2D/CollisionShape2D.disabled = false
	
func Disable():
	disabled = true
	$Area2D/CollisionShape2D.disabled = true


func _on_Area2D_body_entered(body):
	if disabled:
		return
	if(body.has_method("door_collision")):
		get_node("../../../..").door(door)

func _on_Area2D_body_exited(_body):
	disabled = false
