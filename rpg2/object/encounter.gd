extends Node2D

export var encounter = ""

func _on_Area2D_body_entered(body):
	if(body.has_method("door_collision")):
		get_node("../../../..").set_encounters(encounter)

func _on_Area2D_body_exited(body):
	if(body.has_method("door_collision")):
		get_node("../../../..").stop_encounters()
