extends Node2D

func SetupPart(node,data):
	node.visible = true
	var color = Global.GetColor(data[1])
	node.modulate = Color(color[0]/255.0, color[1]/255.0, color[2]/255.0)

func HideAll():
	for child in get_children():
		child.visible = false

func Init(data):
	HideAll()
	scale.x = data.size
	scale.y = data.size
	if data.size<1:scale.x-=0.05
	
	match data.hair[0]:
		"normal":
			SetupPart($hair,data.hair)
		"hood":
			SetupPart($hairhood,data.hair)
		"mask":
			SetupPart($hairmask,data.hair)
		"woman":
			SetupPart($hairwoman,data.hair)
		"evil":
			SetupPart($hairevil,data.hair)
		"arima":
			SetupPart($hairarima,data.hair)
	SetupPart($head,data.head)
	match data.capa[0]:
		"normal":
			SetupPart($capa,data.capa)
	match data.chest[0]:
		"normal":
			SetupPart($chest, data.chest)
		"brigandine": 
			SetupPart($brigandine, [0,data.brigandine])
			SetupPart($brigandinedots, data.chest)
		"cuirass": 
			SetupPart($cuirass, data.chest)
		"gambeson": 
			SetupPart($gambeson, data.chest)
		"mail": 
			SetupPart($mail, data.chest)
		"plated": 
			SetupPart($plated, data.chest)
		"scaled": 
			SetupPart($scaled, data.chest)
	SetupPart($arms,data.arms)
	SetupPart($legs,data.legs)

func play(anim):
	$legs.play(anim)
	$arms.play(anim)
	$scaled.play(anim)
	$plated.play(anim)
	$mail.play(anim)
	$gambeson.play(anim)
	$cuirass.play(anim)
	$brigandinedots.play(anim)
	$brigandine.play(anim)
	$chest.play(anim)
	$capa.play(anim)
	$head.play(anim)
	$hair.play(anim)
	$hairwoman.play(anim)
	$hairarima.play(anim)
	$hairhood.play(anim)
	$hairmask.play(anim)
	
