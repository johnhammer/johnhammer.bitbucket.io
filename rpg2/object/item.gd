extends KinematicBody2D

export (String) var type = "none"
export (String) var dialog = ""
export (String) var loot = ""

func Enable():
	visible = true
	$collision.disabled = false
	$Area2D/CollisionShape2D.disabled = false
	
func Disable():
	visible = false
	$collision.disabled = true
	$Area2D/CollisionShape2D.disabled = true

func _ready():
	if !visible: Disable()

func _on_Area2D_body_entered(body):
	if visible && body.has_method("npc_collision"):
		if loot != "":
			var data = Global.chests_dictionary[loot]
			if !"loot" in data:
				var items = get_tree().get_root().get_node("main").GetLootItems(data.items)
				data.loot = {
					"xp":0,
					"gold":0,
					"items":items
				}
			get_tree().get_root().get_node("main/interface").OpenLoot(data.loot)
		elif dialog != "":
			get_tree().get_root().get_node("main").enter_dialog(self,dialog)

func _on_Area2D_body_exited(body):
	if dialog != "":
		if(body.has_method("npc_collision")):
			get_tree().get_root().get_node("main").exit_dialog()
