extends KinematicBody2D

var velocity = Vector2()
var dir = Global.Direction.Down
export (String) var direction = "down"
export (String) var color = "white"
export (String) var collar = "white"
var speed = 40
var step = 0
var steplimit = 100

func _ready():
	$Cat.modulate = Global.ArrayToColor(Global.GetColor(color))
	if collar == "": collar = color
	$Collar.modulate = Global.ArrayToColor(Global.GetColor(collar))
	match direction:
		"down":dir = Global.Direction.Down
		"up":dir = Global.Direction.Up
		"left":dir = Global.Direction.Left
		"right":dir = Global.Direction.Right
	Move(dir)

func Move(d):
	steplimit = Random.Ranges(100,500)
	dir = d
	velocity = Vector2()
	if dir == Global.Direction.Up:
		velocity.y -= 1
	elif dir == Global.Direction.Right:
		velocity.x += 1
	elif dir == Global.Direction.Left:
		velocity.x -= 1
	elif dir == Global.Direction.Down:
		velocity.y += 1
	velocity = velocity.normalized() * speed

func Swap():
	step = 0
	Move(Random.Choice([
		Global.Direction.Right,
		Global.Direction.Left,
		Global.Direction.Up,
		Global.Direction.Down
	]))
	match dir:
		Global.Direction.Right:
			$Cat.play("right")
			$Collar.play("right")
		Global.Direction.Left:
			$Cat.play("left")
			$Collar.play("left")
		Global.Direction.Down:
			$Cat.play("down")
			$Collar.play("down")
		Global.Direction.Up:
			$Cat.play("up")
			$Collar.play("up")

var lastpos

func _physics_process(_delta):
	if speed == 0 || position==lastpos:
		Swap()
	lastpos = position
	velocity = move_and_slide(velocity)
	step+=1
	if step>steplimit:
		Swap()


func _on_Area2D_body_entered(body):
	if(body.has_method("npc_collision")):
		get_tree().get_root().get_node("main").enter_dialog(self,"cat")
		Swap()

func _on_Area2D_body_exited(body):
	if(body.has_method("npc_collision")):
		get_tree().get_root().get_node("main").exit_dialog()
