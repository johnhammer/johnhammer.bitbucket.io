extends "res://object/item.gd"

var human
var dir = Global.Direction.Down
export (String) var direction = "down"

func _ready():
	match direction:
		"down":dir = Global.Direction.Down
		"up":dir = Global.Direction.Up
		"left":dir = Global.Direction.Left
		"right":dir = Global.Direction.Right
	human = $human
