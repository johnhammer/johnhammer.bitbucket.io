extends Path2D

var npc
export (int) var speed = 200
export (bool) var autostart = false
var followPath = false

func _ready():
	npc = $PathFollow2D/RemoteTransform2D.get_node($PathFollow2D/RemoteTransform2D.remote_path)
	if autostart: Move()

func Move():
	followPath = true

func _physics_process(delta):
	var old_pos = $PathFollow2D/RemoteTransform2D.global_position
	if followPath:
		
		var human = npc.get_node("human")
		var spr = human.get_node_or_null("sprite")
		if spr!=null:
			human = spr
		if $PathFollow2D.unit_offset<1 || $PathFollow2D.loop==true:
			$PathFollow2D.offset += speed * delta
			var direction = ($PathFollow2D/RemoteTransform2D.global_position - old_pos).normalized()
			var cardinal_direction = int(4.0 * (direction.rotated(PI / 4.0).angle() + PI) / TAU)
			match cardinal_direction:
				0:
					npc.dir = Global.Direction.Left
				1:
					npc.dir = Global.Direction.Up
				2:
					npc.dir = Global.Direction.Right
				3:
					npc.dir = Global.Direction.Down
			match npc.dir:
				Global.Direction.Right:
					human.play("r_right")
				Global.Direction.Left:
					human.play("r_left")
				Global.Direction.Down:
					human.play("r_down")
				Global.Direction.Up:
					human.play("r_up")
		else:
			followPath = false
			get_node("../../../..").pathEnd(name)
			npc.dir = Global.Direction.Down
			human.play("s_down")
