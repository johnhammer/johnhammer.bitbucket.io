extends KinematicBody2D

export var doorOut = ""
export (int) var speed = 300
export (Vector2) var cameraLimit = null
export (String) var song = null

var blocked = false
var type = "player"
var human
var step = 0
var encounter_step = 200

var velocity = Vector2()
var dir = Global.Direction.Down

func _ready():
	human = $human
	if cameraLimit!=null && cameraLimit.x!=0:
		$Camera2D.limit_top = 0
		$Camera2D.limit_left = 0
		$Camera2D.limit_right = cameraLimit.x*64
		$Camera2D.limit_bottom = cameraLimit.y*64

func Block():
	Stop()
	blocked = true

func SetPos(pos):
	position = pos

func _input(event):
	if blocked:return
	if Input.is_action_just_pressed('up'):
		Move(Global.Direction.Up)
	elif Input.is_action_just_pressed('right'):
		Move(Global.Direction.Right)
	elif Input.is_action_just_pressed('left'):
		Move(Global.Direction.Left)
	elif Input.is_action_just_pressed('down'):
		Move(Global.Direction.Down)
	elif Input.is_action_just_released('up'):
		Stop()
	elif Input.is_action_just_released('right'):
		Stop()
	elif Input.is_action_just_released('left'):
		Stop()
	elif Input.is_action_just_released('down'):
		Stop()
	elif Input.is_action_just_released("ui_accept"):
		get_node("collision").disabled= true #CHEATS
		speed = 500
	elif Input.is_action_just_released("ui_cancel"):
		get_node("collision").disabled= false #CHEATS

func Move(d):
	if blocked:return
	dir = d
	velocity = Vector2()
	step+=1
	if dir == Global.Direction.Up:
		velocity.y -= 1
	elif dir == Global.Direction.Right:
		velocity.x += 1
	elif dir == Global.Direction.Left:
		velocity.x -= 1
	elif dir == Global.Direction.Down:
		velocity.y += 1
	velocity = velocity.normalized() * speed

func Stop():
	if blocked:return
	velocity = Vector2()

func _physics_process(_delta):
	if blocked:return
	if velocity.length() == 0:
		match dir:
			Global.Direction.Right:
				$human.play("s_right")
			Global.Direction.Left:
				$human.play("s_left")
			Global.Direction.Down:
				$human.play("s_down")
			Global.Direction.Up:
				$human.play("s_up")
	else:
		step+=1
		if step>encounter_step:
			var main = get_node("../../../..")
			if main!=null: main.encounter()
			step = 0
		match dir:
			Global.Direction.Right:
				$human.play("r_right")
			Global.Direction.Left:
				$human.play("r_left")
			Global.Direction.Down:
				$human.play("r_down")
			Global.Direction.Up:
				$human.play("r_up")
	velocity = move_and_slide(velocity)

func npc_collision():pass
func door_collision():pass

var removing = false

func Enable():
	Stop()
	$Camera2D.current = true
	blocked = false
	visible = true
	
func Disable():
	Stop()
	$Camera2D.current = false
	blocked = true
	visible = false

func _on_VisibilityNotifier2D_viewport_exited(_viewport):
	if blocked:return
	if removing:return
	if doorOut=="WRAP":
		if position.x<0: position.x = cameraLimit.x*128
		elif position.x>cameraLimit.x*128: position.x = 0
		elif position.y<0: position.y = cameraLimit.y*128
		elif position.y>cameraLimit.y*128: position.y = 0
	elif doorOut!="":
		var main = get_node("../../../..")
		if main!=null: main.door(doorOut)
