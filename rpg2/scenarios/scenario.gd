extends Node2D

func Init(npcs,prev_scenario,name):
	#story
	var story = get_tree().get_root().get_node("main/story")
	if name in story.Events:
		StoryEvent(name,story.Events[name])
	
	#camera
	if has_node("Camera2D"):
		$Camera2D.current = true
	$collision.visible = false
	
	#npcs and doors
	var children = $Nodes.get_children()
	if get_node_or_null("Doors")!=null:
		children = children + $Doors.get_children()
	for item in children:
		if "human" in item:
			if item.type == "player":
				Global.player = item
				item.human.Init(Global.party[0])
				if item.song != null:
					get_tree().get_root().get_node("main/sound").Play(item.song)
			else:
				item.human.Init(npcs[item.type])
		elif "door" in item:
			if name=="inn_room": item.door = prev_scenario
			var extra = ""
			if Global.ExtraDoor!=null: 
				extra = "#"+Global.ExtraDoor
			if item.door == prev_scenario+extra:
				$Nodes/player.SetPos(item.position)
				item.Deactivate()

const closed_door = 77
const open_door = 30
const collision = 0
const free = -1

func Fill(tileset,xo,yo,xf,yf,value):
	for x in range(xo,xf):
		for y in range(yo,yf):
			tileset.set_cell(x,y,value)

func Disable(npcs): for npc in npcs: npc.Disable() 
func Enable(npcs): for npc in npcs: npc.Enable() 

func StoryEvent(name,id):
	match name:
		"inn_room":
			if !Global.HasPartyMember("finbar"): Disable([$Nodes/finbar])
			if !Global.HasPartyMember("chara"): Disable([$Nodes/chara])
			if !Global.HasPartyMember("egil"): Disable([$Nodes/egil])
			if !Global.HasPartyMember("vita"): Disable([$Nodes/vita])
			if !Global.HasPartyMember("ainhoa"): Disable([$Nodes/ainhoa])
			if !Global.HasPartyMember("tiarnan"): Disable([$Nodes/tiarnan])
		"tornstad/tornstad_inn":
			Disable([$Nodes/finbar,$Nodes/bandit,$Nodes/bandit2,$Nodes/bandit3])
		"tornstad/tornstad":
			Disable([$Nodes/man1,$Nodes/man2,$Nodes/man3,$Nodes/man4])
			Enable([$Nodes/man5,$Nodes/man6,$Nodes/man7,$Nodes/man8,$Nodes/man9,$Nodes/man10,$Nodes/man11])
			$Nodes/tavern.Disable()
			Fill($MainBelowOverlay,14,14,15,15,closed_door)
			Fill($collision,14,15,15,15,collision)
			$Nodes/item.Enable()
			Fill($MainBelowOverlay,11,24,12,25,open_door)
			Fill($collision,11,25,12,25,free)
			$Nodes/blacksmith.Enable()
			Fill($MainBelowOverlay,41,23,42,24,open_door)
			Fill($collision,41,24,42,24,free)
		"kronstad/kronstad":
			Disable([$Nodes/ward])
			Fill($collision,52,130,56,130,free)
			Fill($MainBelow,52,130,56,130,free)
			
