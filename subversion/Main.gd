extends Node2D

var identities = []
var countries = []
var regions = []
var areas = []

var width = 30
var height = 15
var rnd

func _ready():
	rnd = Random.Create(42)
	var maxIdentities = 5
	var minIdentities = 2
	var maxCountriesXIdentity = 4
	var minCountriesXIdentity = 1
	var maxRegionsXCountry = 5
	var minRegionsXCountry = 1
	var maxAreasXRegion = 5
	var minAreasXRegion = 5
	CreateBoard(maxIdentities,minIdentities,maxCountriesXIdentity,minCountriesXIdentity,maxRegionsXCountry,minRegionsXCountry,maxAreasXRegion,minAreasXRegion)
	DrawBoard()

func DrawBoard():
	for x in range(width):
		for y in range(height):
			$Nodes/Base.set_cell(x,y,14)
	
	var map_limits = $Nodes/Base.get_used_rect()
	var map_cellsize = $Nodes/Base.cell_size
	$Nodes/Camera2D.limit_left = map_limits.position.x * map_cellsize.x
	$Nodes/Camera2D.limit_right = map_limits.end.x * map_cellsize.x
	$Nodes/Camera2D.limit_top = map_limits.position.y * map_cellsize.y
	$Nodes/Camera2D.limit_bottom = map_limits.end.y * map_cellsize.y
	
	for area in areas:
		$Nodes/Base.set_cell(area.x,area.y,0)
		$Nodes/Tiles.set_cell(area.x,area.y,GetAreaType(area))
		var region = area.Region
		
		var rect = ColorRect.new()
		rect.color = region.Color
		$Nodes/Rectangles.add_child(rect)
		rect.rect_size = $Nodes/Base.cell_size
		rect.rect_position = $Nodes/Base.map_to_world(Vector2(area.x,area.y))

func CreateBoard(maxIdentities,minIdentities,maxCountriesXIdentity,minCountriesXIdentity,maxRegionsXCountry,minRegionsXCountry,maxAreasXRegion,minAreasXRegion):
	var identitiesAmount = Random.Range(rnd,minIdentities,maxIdentities)
	var identityIndex = 0
	while identityIndex<identitiesAmount:
		identityIndex+=1
		var identity = Factory.Identity(rnd)
		identities.push_back(identity)
		
		var countriesAmount = Random.Range(rnd,minCountriesXIdentity,maxCountriesXIdentity)
		var countryIndex = 0
		while countryIndex<countriesAmount:
			countryIndex+=1
			var country = Factory.Country(rnd,identity)
			identity.Countries.push_back(country)
			countries.push_back(country)
			
			var regionsAmount = Random.Range(rnd,minRegionsXCountry,maxRegionsXCountry)
			var regionIndex = 0
			while regionIndex<regionsAmount:
				regionIndex+=1
				var region = Factory.Region(rnd,country)
				country.Regions.push_back(region)
				regions.push_back(region)
				
				var areasAmount = Random.Range(rnd,minAreasXRegion,maxAreasXRegion)
				var areaIndex = 0
				while areaIndex<areasAmount:
					areaIndex+=1
					var pos = GetPos(rnd,region)
					var area = Factory.Area(rnd,region,pos)
					region.Areas.push_back(area)
					areas.push_back(area)

func GetAreaType(area):
	match area.Type:
		0.0:
			return -1
		1.0:
			return 8 #farm
		2.0:
			return 4 #industry
		3.0:
			return 3 #city

func GetRandomPos(rnd):
	var limit = 0
	while limit < 20:
		limit+=1
		var pos = {
			"x":Random.Range(rnd,0,width),
			"y":Random.Range(rnd,0,height)
		}
		if IsFreePos(pos):
			return pos

func IsFreePos(pos):
	if pos.x>=width || pos.x<0 || pos.y>=height || pos.y<0:
		return false
	for area in areas:
		if area.x==pos.x && area.y==pos.y:
			return false
	return true

func GetNeighbors(x,y):
	return [
		{"x":x-1,"y":y},
		{"x":x,"y":y-1},
		{"x":x+1,"y":y},
		{"x":x,"y":y+1},
	]

func GetPos(rnd,region):
	var tiles = region.Areas
	if tiles.size()==0:
		tiles = region.Country.Regions[0].Areas
		if tiles.size()==0:
			tiles = region.Country.Identity.Countries[0].Regions[0].Areas
			if tiles.size()==0:
				return GetRandomPos(rnd)
	var limit = 0
	while limit < 20:
		limit+=1
		var tile = Random.Choice(rnd,tiles)
		var neighbors = GetNeighbors(tile.x,tile.y)
		var pos = Random.Choice(rnd,neighbors)
		if IsFreePos(pos):
			return pos
	return GetRandomPos(rnd)
