extends Node

enum CompanyType {Food,Industry,Services,Media}
#international
enum Politics_Military {Disarmed,Defensive,Regional,Global}
enum Politics_Globalism {Isolationist,Protectionist,Partner,Globalist}
enum Politics_Immigration {Closed,Identity,Legal,Open}
#economic
enum Politics_Autonomy {Centralized,Autonomic,Federal,Separatist}
enum Politics_Taxation {Haven,Low,High,Hell}
enum Politics_Development {Industrialization,Ruralization,Gentrification,Stagnant}
#social
enum Politics_Family {Natalist,Neutral,Single,Childless}
enum Politics_Identity {Identitarian,Neutral,Colorblind,Xenophile}
enum Politics_Culture {Traditional,Modern,Postmodern,Nihilistic}
#political
enum Politics_Government {Republic,Democracy,Oligarchy,Dictatorship}
enum Politics_Rights {Full,Limited,Private,None}
enum Politics_Justice {Hard,Soft,Corrupt,Political}
