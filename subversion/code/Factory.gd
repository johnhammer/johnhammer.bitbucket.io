extends Node

var Letters = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

var RegionNames = [
'Acarnania','Aeniania','Aetolia','Aperantia','Attica','Boeotia','Dolopia','Doris','Euboea','Locris','Malis','Megaris','Oetaea','Phocis','Peloponnese','Achaea','Arcadia','Argolis','Corinthia','Elis','Laconia','Messenia','Thessalia','Magnesia','Histiaeotis','Pelasgiotis','Perrhaebia','Thessaliotis','Epirus','Athamania','Chaonia','Dassaretia','Molossis','Thesprotia','Parauaea','Tymphaea','Macedonia','Almopia','Bisaltia','Bottiaea','Chalcidice','Crestonia','Edonis','Elimiotis','Emathia','Eordaea','Lynkestis','Mygdonia','Odomantis','Orestis','Pelagonia','Pieria','Sintice','Aeolis','Doris','Ionia','Pontus','Cyprus','Crimea','Cyrenaica'
]

func Company(rnd,type,region,areas):
	var name = region + " "
	if areas.size()>2:
		match type:
			Enums.CompanyType.Food:name+=Random.Choice(rnd,FoodNamesSmall)
			Enums.CompanyType.Industry:name+=Random.Choice(rnd,IndustryNamesSmall)
			Enums.CompanyType.Services:name+=Random.Choice(rnd,ServicesNamesSmall)
			Enums.CompanyType.Media:name+=Random.Choice(rnd,MediaNamesSmall)
	else:
		match type:
			Enums.CompanyType.Food:name+=Random.Choice(rnd,FoodNamesBig)
			Enums.CompanyType.Industry:name+=Random.Choice(rnd,IndustryNamesBig)
			Enums.CompanyType.Services:name+=Random.Choice(rnd,ServicesNamesBig)
			Enums.CompanyType.Media:name+=Random.Choice(rnd,MediaNamesBig)
	return{
		"Name" : name,
		"Type" : type,
		"Income" : Random.Range(rnd,1,10),
		"Value" : Random.Range(rnd,10,200),
		"Areas" : areas
	}

func Values(rnd):
	return [
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3),
		Random.Range(rnd,0,3)
	]

func Party(rnd,region):	
	var name = Random.Choice(rnd,Letters)+Random.Choice(rnd,Letters)+Random.Choice(rnd,Letters)
	return{
		"Name" : name,
		"Values" : Values(rnd)
	}

func Identity(rnd):
	var color = Color(Random.Float(rnd), 0, 0, 0.5)
	return {
		"Countries":[],
		"Color":color,
		"Values" : Values(rnd)
	}

func Country(rnd,identity):
	var color = Color(identity.Color.r, Random.Float(rnd), 0, 0.5)
	return {
		"Regions":[],
		"Identity":identity,
		"Color":color
	}
	
func Region(rnd,country):
	var color = Color(Random.Float(rnd), 
		Random.Float(rnd), 
		Random.Float(rnd), 0.5)
	return {
		"Areas":[],
		"Country":country,
		"Color":color
	}

func Area(rnd,region,pos):
	var area = {
		"x":pos.x,"y":pos.y,
		"Region":region,
		"Type":Random.Range(rnd,0,3)
	}
	return area


var FoodNamesSmall = [
	"grains","fruits","meats","corn","dairy","poultry","oils","cereals","apples","banana","beans","legumes"
]
var FoodNamesBig = [
	"foods","produce","growth","farms"
]
var IndustryNamesSmall = [
	"motors","textile","pharmacy","metals","electronics","electrics","clothing","gadgets","power","energy","precision","devices"
]
var IndustryNamesBig = [
	"utilities","machinery","industries","products"
]
var ServicesNamesSmall = [
	"grains","fruits","meats","corn","dairy","poultry","oils","cereals"
]
var ServicesNamesBig = [
	"foods","produce","growth","farms","aliments","farming","foods","foods"
]
var MediaNamesSmall = [
	"grains","fruits","meats","corn","dairy","poultry","oils","cereals"
]
var MediaNamesBig = [
	"foods","produce","growth","farms","aliments","farming","foods","foods"
]
