extends Button

var food = Color(0.67,0.91,0.63)
var industry = Color(0.46,0.4,0.4)
var services = Color(0.64,0.64,0.8)
var media = Color(0.58,0.49,0.62)

var green = Color(0,0.8,0)
var red = Color(1,0,0)

func InitCompany(type,name,income,value):
	var style = StyleBoxFlat.new()
	match type:
		Enums.CompanyType.Food:style.set_bg_color(food)
		Enums.CompanyType.Industry:style.set_bg_color(industry)
		Enums.CompanyType.Services:style.set_bg_color(services)
		Enums.CompanyType.Media:style.set_bg_color(media)
	set("custom_styles/normal", style)
	$Name.text = name
	$Company/Income.text = String(income)
	$Company.visible = true
	if income>=0:$Company/Income.add_color_override("font_color", green)
	else:$Company/Income.add_color_override("font_color", red)
	$Value.text = String(value)

func GetValueColor(r,g,b):
	return Color(r/3.0,g/3.0,b/3.0)

func InitParty(name,values):
	$Name.text = name
	$Value.text = String(0)
	$Party.visible = true
	var style = StyleBoxFlat.new()
	style.set_bg_color(GetValueColor(values[0],values[1],values[2]))
	$Party/International.set("custom_styles/normal", style)
	style = StyleBoxFlat.new()
	style.set_bg_color(GetValueColor(values[3],values[4],values[5]))
	$Party/Economic.set("custom_styles/normal", style)
	style = StyleBoxFlat.new()
	style.set_bg_color(GetValueColor(values[6],values[7],values[8]))
	$Party/Social.set("custom_styles/normal", style)
	style = StyleBoxFlat.new()
	style.set_bg_color(GetValueColor(values[9],values[10],values[11]))
	$Party/Political.set("custom_styles/normal", style)
	
