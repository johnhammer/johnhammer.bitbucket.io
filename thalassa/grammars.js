var Grammars = {
Train : `
Command = Create/Stop

Create = amt:N _ item:Item {
	var m = amt + ' ' + item.m;
	return {t:'AddUnit',m:m,a:{t:item.t,amt:amt}};
}

Stop = "parar"/"stop"/"terminar"/"acabar"/"aceptar" {
	var m = 'Stop';
	return {t:'Stop',m:m};
}

Item = 
	weapon:Weapon 	{return{t:weapon.a,m:'unidades con '+weapon.m+' anadida'}}
	/unit:Unit		{return{t:unit.a,m:'unidades de '+unit.m+' anadida'}}


/*	######## UNITS ########	*/


Unit = 
	Soldier 	{return{m:text(),a:{dist:0,cut:10,stab:2,hit:2,speed:5}}}
	/Spearman	{return{m:text(),a:{dist:0,cut:2,stab:10,hit:2,speed:5}}}
	/Archer		{return{m:text(),a:{dist:1,cut:0,stab:10,hit:0,speed:5}}}
	/Thrower	{return{m:text(),a:{dist:1,cut:0,stab:0,hit:10,speed:5}}}
	/Horseman	{return{m:text(),a:{dist:0,cut:10,stab:2,hit:2,speed:10}}}
	/General	{return{m:text(),a:{dist:0,cut:0,stab:0,hit:0,speed:5}}}

Soldier = "soldado"
Spearman = "lancero"
Archer = "arquero"
Thrower = "hondero"
Horseman = "jinete"
General	= "general"

/*	######## WEAPONS ########	*/


Weapon = 
	Sword		{return{m:text(),a:{dist:0,cut:10,stab:2,hit:2,speed:5}}}

Sword = "espada"


/*	######## AUX ########	*/


w = [a-z]+ {return text()}
N = [0-9]+ {return Number(text())}
_ = [ \\t\\n\\r]+
`,	

Battle : `
Command = order:Order (_ unit:w)? /*move unit to next rule and remove this?*/
{return order}

Order = 
	Advance (_ Position)?		{return {m:text(),t:'Advance'}}
	/Maneuver 					{return {m:text(),t:'Maneuver'}}
	/Retrocede (_ Position)?    {return {m:text(),t:'Retrocede'}}
	/Maintain (_ Position)?     {return {m:text(),t:'Maintain'}}
	/Volley                     {return {m:text(),t:'Volley'}}
	/FireAtWill                 {return {m:text(),t:'FireAtWill'}}
	/Retreat                    {return {m:text(),t:'Retreat'}}
	/Bribe                      {return {m:text(),t:'Bribe'}}
	/Surrender                  {return {m:text(),t:'Surrender'}}
	/Garrison                   {return {m:text(),t:'Garrison'}}
	/Use                        {return {m:text(),t:'Use'}}
	/Destroy                    {return {m:text(),t:'Destroy'}}

Advance = "avanzar"/"atacar"
Maneuver = "flanquear"/"maniobrar"/"rodear"
Retrocede = "retroceder"
Maintain = "mantener"/"mantener"/"defender"/"aguantar"/"proteger"
Volley = "oleada"/"disparar"/"tirar"
FireAtWill = "fuego libre"/"abrir fuego"
Retreat = "retirar"/"retirarse"/"huir"/"retirada"
Bribe = "sobornar"/"comprar"
Surrender = "rendirse"/"rendir"
Garrison = "apostar"/"guarnecer"/"poner a cubierto"/"ponerse a cubierto"/"protegerse"
Use = "usar"
Destroy	= "destruir"/"demoler"/"romper"

Position = "posicion"
	
w = [a-z]+ {return text()}
_ = [ \\t\\n\\r]+
`,		
	
Regular : `
Command = Colonize/Build/Attack/Train/Show/Explore/Celebrate/Sacrifice/Pray/Trade/Destroy/Repeat/Undo/Pass


/*	######## COMMANDS ########	*/

Pass = ToPass (_ Turn)?{
	return {t:'Pass',m:'Turno pasado'}
}
Build = ToBuild _ building:Building{
	var m = 'Ahora tenemos '+building.m;
	return {t:'Build',a:{building:building.t},m:m};
}
Attack = ToAttack (_ PrepTo)? _ objective:w army:(_ (PrepWith _)? (Army _)? w)?{	
	var armyC = army?army[army.length-1]:null;
	var m = 'Enviando ejercito '+(armyC?armyC+' ':'')+'a '+objective;	
	return {t:'Attack',a:{objective:objective,army:armyC},m:m};
}
Train = ToTrain _ armyType:Army _ army:w{ /*conditional name*/
	var m = 'Crear '+armyType+' '+army
	return {t:'Train',a:{army:army},m:m};
}
Show = ToShow (_ Data)?{
	return {type:'Show'};
}
Colonize = 
	ToColonize1 _ (PrepIn _)? place:w
    {return {type:'colonize',place:place};}
    /ToColonize2 _ Colony _ (PrepIn _)? place:w
    {return {type:'colonize',place:place};}
    
Explore = ToExplore _ (PrepIn _)? place:w{
    return {type:'explore',place:place};
}
Celebrate = ToCelebrate (_ (PrepFor _)? w)?{
    return {type:'celebrate'};
}
Sacrifice = 
  ToSacrifice _ item:Building _ (PrepTo _)? god:God
  {return {type:'sacrifice',item:item,god:god};}
  /ToSacrifice _ (PrepTo _)? god:God _ item:Building 
  {return {type:'sacrifice',item:item,god:god};}

Pray = ToPray _ (PrepTo _)? god:God _ PrepFor _ item:Building {
    return {type:'pray',item:item,god:god};
}
Trade = ToTrade (_ PrepWith)? _ objective:w{
	return {type:'trade',objective:objective};
}
Destroy = ToBuild _ building:Building{
	return {type:'build',building:building};
}
Repeat = ToRepeat (_ LastAction)*{
	return {type:'repeat'};
}
Undo = ToUndo (_ LastAction)*{
	return {type:'undo'};
}


/*	######## BUILDINGS ########	*/


Building = 
House 		{return {m:text(),t:'House'}}
/Farm 		{return {m:text(),t:'Farm'}}
/Mine		{return {m:text(),t:'Mine'}}
/Armory 	{return {m:text(),t:'Armory'}}
/Defence 	{return {m:text(),t:'Defence'}}
/Water 		{return {m:text(),t:'Water'}}
/Road 		{return {m:text(),t:'Road'}}

House = "casa"/"hogar"/"vivienda"/"choza"/"habitaje"
Farm = "campo"/"pesqueria"/"granja"/"cerdo"/"oveja"/"cabra"/"gallina"/"pollo"/"vaca"/"arbol"/"puerto"/"olivo"/"vina"/"zanahoria"/"trigo"/"cebolla"/"higo"/"granada"/"cebada"
Mine = "mina"/"fundicion"
Armory = "armeria"/"herreria"
Defence = "muralla"/"cuartel"/"torre"/"puerta"/"campamento"/"atalaya"/"muro"
Water = "fuente"/"cisterna"/"aqueducto"/"pozo"
Road = "carretera"/"camino"


/*	######## NAMES ########	*/


//army
Army = "ejercito"/"tropa"/"batallon"/"escuadron"/"escuadra"/"centuria"/"decuria"

//direction


//data
Data = "datos"/"ciudad"/"ayuda"/"situacion"

//colony
Colony = "colonia"/"base"/"ciudad"

//last action
LastAction = "accion"/"anterior"

//gods
God = "zeus"

//turn
Turn = "turno"


/*	######## VERBS ########	*/

ToPass = "pasar"/"saltar"/"ignorar"/"siguiente"
ToBuild = "fabricar"/"producir"/"construir"/"levantar"/"hacer"/"plantar"/"sembrar"/"crear"/"criar"/"comprar"/"poner"/"arar"/"mejorar"/"renovar"/"reparar" /"montar"
ToAttack = "atacar"/"conquistar"/"saquear"/"destruir"/"matar"/"invadir"
ToTrain = "formar"/"reclutar"/"reunir"/"crear"/"entrenar"/"reunir"/"armar"/"establecer"/"fundar"
ToShow = "mostrar"/"ensenar"/"explicar"/"ver"/"analizar"/"observar"
ToColonize1 = "colonizar"
ToColonize2 = "asentar"/"establecer"/"crear"/"construir"
ToExplore = "explorar"/"ver"/"viajar"
ToCelebrate = "celebrar"/"festejar"
ToSacrifice = "sacrificar"
ToPray = "orar"/"rogar"/"rezar"/"pedir"/"demandar"
ToTrade = "negociar"/"comerciar"/"vender"/"comprar"/"dar"/"pedir"/"demandar"/"ofrecer"/"regalar"/"exigir"
ToDestroy = "destruir"/"borrar"
ToRepeat = "repetir"/"bis"
ToUndo = "deshacer"/"revertir"


/*	######## AUX ########	*/


PrepWith = "con"
PrepTo = "a"
PrepIn = "en"
PrepFor = "para"/"por"
w = [a-z]+ {return text()}
_ = [ \\t\\n\\r]+

`
}