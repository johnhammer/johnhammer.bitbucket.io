/*ONLINE*/
var CONST = {
	Civilizations : {
		Iberian : 0,
		Celtic : 1,
		Latin : 2,
		Greek : 3,
		Phoenician : 4,
		Egyptian : 5		
	},
	EventTypes : {
		EndOfGame : 0,
		Invasion : 1
	}
}

var ISONLINEGAME = true;
var PLAYERS = [];
var CURRENT_PLAYER = 0;
var CURRENT_YEAR = 0;
var PLAYER_ID = null;
var EVENTS = [];


var Parsing = {
	Parsers : {},
	Init : function(){
		for(Grammar in Grammars){
			this.Parsers[Grammar] = peg.generate(Grammars[Grammar]);
		}		
	},	
	Clean : function(text){		
		text = text.toLowerCase();
		text = text.replace("á","a");
		text = text.replace("é","e");
		text = text.replace("í","i");
		text = text.replace("ó","o");
		text = text.replace("ú","u");
		text = text.replace("ñ","n");
		text = text.replace("¿","");
		text = text.replace("¡","");
		//text = synonymy(text);
		return text;
	},
	Parse : function(raw,parser="Regular"){
		try{
			var result = this.Parsers[parser].parse(this.Clean(raw));
			console.log(result);
			return result;
		}
		catch (e){
			console.warn(e);
			return {m:'Comando desconocido.'};
		}
	}	
}

var Templating = {
	Player : function(id,human){
		return `
			<div class="player js_player">
				<input onchange="Room.ChangePlayerName(${id},this.value)" value="Jugador"></input>
				<select onchange="Room.ChangePlayerFaction(${id},this.value)">
					<option value="0">	Ibero</option>
					<option value="1">	Celta</option>
					<option value="2">	Latin</option>
					<option value="3">	Griego</option>
					<option value="4">	Fenicio</option>
					<option value="5">	Egipcio</option>
				</select>				
				${human?'':'<input id="player1isia" type="checkbox" checked disabled>IA</input>'}				
			</div>
		`;		
	},
	ChatMessage : function(nick,message,me){
		return `
			<div ${me?'class="chat_message_me"':''}>${nick}: ${message}</div>
		`;		
	}
}

var Ai = {
	Play : function(){
		
	}
}

var LIB = {	
	Toggle : function(node){
		if(node.style.display == "none"){
			node.style.display = "block";
		}
		else{
			node.style.display = "none";
		}
	},
	Crypt : function(me, key){
		key = Number(String(Number(key))) === key ? Number(key) : 13;
		me = me.split('').map(function(c){return c.charCodeAt(0);}).map(function(i){return i ^ key;});
		me = String.fromCharCode.apply(undefined, me);
		return me;
	},
	Encrypt : function(me, key="entropy"){
		return LIB.Crypt(window.btoa(me),key);
	},
	Decrypt : function(me, key="entropy"){
		return window.atob(LIB.Crypt(me,key));
	},
	Rnd : function(min,max){
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	PickRnd : function(arr){
		return arr[Math.floor(Math.random() * arr.length)]
	},
}

var Interaction  = {
	Active:false,
	Type : null,
	Args : null,
	Start : function(type,args){
		Interaction.Active = true;
		this.Type = type;
		this.Args = args;
	},
	Interpret : function(raw){
		var ans = Parsing.Parse(raw,this.Type);
		if(ans.t){
			Subaction[ans.t](this.Args,ans.a);	
		}
		return ans.m;
	},
	Send : function(raw){	
		var ans = this.Interpret(raw);
		return ans;
	}
}

var TurnManager = {
	Current : 0,
	EndTurn : function(player){
		Main.Print("Fin del turno del jugador "+CURRENT_PLAYER);
	},	
	StartTurn : function(){
		Main.Print(Main.Info());
	},
	GetPlayerMaxTurns : function(player){
		return PLAYERS[player].Families;
	},
	Next : function(){
		this.Current++;
		EventsHandler.ProcessCurrentEvents();
		if(this.Current>=this.GetPlayerMaxTurns(CURRENT_PLAYER)){
			this.Current=0;			
			this.EndTurn(CURRENT_PLAYER);
			CURRENT_PLAYER++;			
			if(CURRENT_PLAYER>=PLAYERS.length){
				CURRENT_PLAYER=0;				
				this.StartTurn();
			}			
		}
		if(!PLAYERS[CURRENT_PLAYER].Info.Human){
			Ai.Play();
			TurnManager.Next();
		}
	}
}

var Main = {
	Interpret : function(raw){
		var ans = Parsing.Parse(raw);
		if(ans.t){
			Action[ans.t](ans.a);	
		}
		return ans.m;
	},
	Print : function(out){
		output.innerHTML += "<p>"+out+"</p>";	
	},
	Send : function(){	//TODO refactor
		var raw = input.value;	
		if(Interaction.Active){
			if(ISONLINEGAME){
				Protocol.Game(raw);
			}
			var out = Interaction.Send(raw);
			Main.Print(out);		
			input.value = "";
			return;
		}
		if(CURRENT_PLAYER!=PLAYER_ID){
			Main.Print("Espera a tu turno.");	
			return;
		}	
		if(ISONLINEGAME){
			Protocol.Game(raw);
		}		
		var out = this.Interpret(raw);	
		Main.Print(out);		
		input.value = "";	
		if(!Interaction.Active) TurnManager.Next();
	},
	GameMessage : function(raw){
		var out = this.Interpret(raw);	
		Main.Print(out);	
		TurnManager.Next();
	},
	Info : function(){
		return "Tu turno. Hay "+PLAYERS[CURRENT_PLAYER].Families+" familias.";
	},
	StartGame : function(){
		Parsing.Init();
		EVENTS.push({d:100,t:CONST.EventTypes.EndOfGame});
	
		LIB.Toggle(room);
		LIB.Toggle(game);

		Main.Print(Main.Info());
		
		input.addEventListener("keyup", function(event) {
			event.preventDefault();
			if (event.keyCode === 13) {
				submit.click();
			}
		});
	}
}



var Chat = {
	Show : function(name,out,me=false){
		chat_output.innerHTML += Templating.ChatMessage(name,out,me);	
	},
	Send : function(){
		var raw = chat_input.value;			
		Chat.Show(PLAYERS[PLAYER_ID].Info.Name,raw,true);		
		Protocol.Chat(raw);		
		chat_input.value = "";
	}	
}

var Room = {
	Single : function(){
		ISONLINEGAME = false;
		
		this.AddHuman();
		PLAYER_ID = 0;
	
		LIB.Toggle(room_single);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_start);
	},	
	Create : function(){
		this.AddHuman();	
		PLAYER_ID = 0;
	
		Online.Init();	
		Online.useServer = false;
		Online.CreateManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_single);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);
		LIB.Toggle(room_add);
		LIB.Toggle(room_allow);
		LIB.Toggle(room_start);
		LIB.Toggle(chat);
	},
	Join : function(){
		Online.Init();	
		Online.JoinManual();
		LIB.Toggle(room_info);
		LIB.Toggle(room_single);
		LIB.Toggle(room_create);
		LIB.Toggle(room_join);	
		LIB.Toggle(chat);		
	},
	Allow : function(){
		Online.Allow();
	},
	StartGame : function(){		
		if(ISONLINEGAME){
			Protocol.StartGame();
		}
		Main.StartGame();
	},
	AddPlayer : function(human){		
		this.AddGlobalPlayer(human);		
		players.insertAdjacentHTML('beforebegin',Templating.Player(PLAYERS.length-1,human));		
	},	
	AddHuman : function(){
		this.AddPlayer(true);
	},
	AddAI : function(){		
		this.AddPlayer(false);
		if(ISONLINEGAME){
			Protocol.Welcome();
		}
	},
	GetPlayers : function(){
		var playerNodes = document.getElementsByClassName('js_player');
		var playersData = [];		
		for(var i=0;i<playerNodes.length;i++){
			playersData.push('kurwa');			
		}		
		return playersData;
	},
	SetPlayers : function(playersData){
		players.innerHTML = '';
		var i;
		for(i=0;i<playersData.length;i++){
			this.AddHuman();
		}
		if(!PLAYER_ID){
			PLAYER_ID = i-1;
		}
	},	
	AddGlobalPlayer : function(human){
		PLAYERS.push({
			Families:3,
			Info:{
				Name : 'Jugador',
				Civ : 0,
				Human : human
			},
			Buildings : {
				House : 0,
				Food : 0,
				Metal : 0,
				Weapons : 0,
				Defence : 0			
			},		
		});
	},
	ChangePlayerName : function(id,name){
		PLAYERS[id].Info.Name = name;
	},
	ChangePlayerFaction : function(id,civ){
		PLAYERS[id].Info.Civ = Number(civ);
	},
	ShowInfo : function(info){
		room_code.value = LIB.Encrypt(info);
	}
}

var Protocol = {
	Types:{
		Chat:0,
		Enter:1,
		Welcome:2,
		Start:3,
		Game:4,		
	},	
	Enter : function(){ //player to host, on enter room
		var message = {
			t:Protocol.Types.Enter
		}		
		Online.Raw.Send(message);
	},
	Welcome : function(){ //host to players, new player entered room
		var message = {
			t:Protocol.Types.Welcome,
			m:Room.GetPlayers()
		}		
		Online.Raw.Send(message);
	},	
	Chat : function(raw){
		var message = {
			t:Protocol.Types.Chat,
			m:raw,
			n:PLAYERS[PLAYER_ID].Info.Name
		}		
		Online.Raw.Send(message);
	},
	StartGame : function(){
		var message = {
			t:Protocol.Types.Start
		}		
		Online.Raw.Send(message);
	},
	Game : function(raw){
		var message = {
			t:Protocol.Types.Game,
			m:raw		
		}		
		Online.Raw.Send(message);
	},
}

var Listener = {
	Listen : function(message){		
		switch(message.t){
			case Protocol.Types.Chat:
				Chat.Show(message.n,message.m);
				break;
			case Protocol.Types.Enter:
				Room.AddHuman();
				Protocol.Welcome();
				break;
			case Protocol.Types.Welcome:
				Room.SetPlayers(message.m);
				break;
			case Protocol.Types.Start:
				Main.StartGame();
				break;
			case Protocol.Types.Game:
				Main.GameMessage(message.m);
				break;
			default:
				console.log(message);
		}		
	}	
}

var Online = {
	pc1:null,
	pc2:null,
	activedc:null,
	currentRoom:null,
	roomList:null,
	
	useServer:true,
	
	Raw : {
		Receive : function(message){
			if(message.data) Listener.Listen(JSON.parse(message.data));
		},
		Send : function(message){
			if(message) Online.activedc.send(JSON.stringify(message));
		}		
	},
	CreateManual : function(){
		dc1 = Online.pc1.createDataChannel('entropy', {reliable: true})
		Online.activedc = dc1
		dc1.onopen = function(e) { }
		dc1.onmessage = Online.Raw.Receive;
		Online.pc1.createOffer(function(desc) {
			Online.pc1.setLocalDescription(desc, function() {}, function() {})
		}, function() { }, {optional: []})
	},
	JoinManual : function(){
		Online.useServer = false; 
		var room = LIB.Decrypt(prompt("Enter room info", ""));		
		if (room != null) {
			var offerDesc = new RTCSessionDescription(JSON.parse(room))
			Online.pc2.setRemoteDescription(offerDesc)
			Online.pc2.createAnswer(function(answerDesc) {
				Online.pc2.setLocalDescription(answerDesc);
			},
			function () { },{optional: []})
		}		
	},
	Allow : function(){
		var room = LIB.Decrypt(prompt("Enter player info", ""));	
		if (room != null) {
			var answerDesc = new RTCSessionDescription(JSON.parse(room));
			Online.pc1.setRemoteDescription(answerDesc);			
		}
	},	
	Init : function(){
		if (navigator.webkitGetUserMedia) {
			RTCPeerConnection = webkitRTCPeerConnection
		}

		var cfg = {'iceServers': [{'url': "stun:stun.gmx.net"}]},con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
		
		
		//pc 1
		Online.pc1 = new RTCPeerConnection(cfg, con), dc1 = null, tn1 = null, Online.activedc, Online.pc1icedone = false;
		
		Online.pc1.onicecandidate = function (e) {
			if (e.candidate == null) {
				var room = JSON.stringify(Online.pc1.localDescription);
				Room.ShowInfo(room);
				
				//alert(room);				
			}
		}
		
		//pc 2
		Online.pc2 = new RTCPeerConnection(cfg, con), dc2 = null, Online.pc2icedone = false;

		Online.pc2.onicecandidate = function (e) {
			if (e.candidate == null) {
				var desc = JSON.stringify(Online.pc2.localDescription);			
				Room.ShowInfo(desc);				
				//alert(desc);
			}
		}
		
		Online.pc2.ondatachannel = function (e) {
			var datachannel = e.channel || e;
			dc2 = datachannel
			Online.activedc = dc2
			dc2.onopen = function (e) { 
				Protocol.Enter();
			}
			dc2.onmessage = Online.Raw.Receive;
		}	
	}
}

var Map = {
	Regions : [],
	Cities : [],
	
	Init : function(){
		
	},
	GetDistance(place1,place2){
		//coordinates vector dif
	},
	
}
