extends Node2D

func _ready():
	var o = get_node("Navigation2D/man").position
	var d = get_node("Navigation2D/target").position
	
	var array = Array(get_node("Navigation2D").get_simple_path(o, d, false))
	
	for point in array:
		get_node("Navigation2D/TileMap").set_cell(point.x,point.y,2)
