var NameGenerator = {
	MakeSurname : function(){
		var minlength = 2;
		var maxlength = 5;
		
		var vowel = ['a','e','i','o','u'];
		var syllable = ['k','b','t'];
		var consonant = ['s','sh','m','mh','r','rr','n','l'];
		
		var name = '';
		
		var rnd = LIBs.random(1,3);	
		if(rnd==1){
			name += LIBs.getRandom(vowel);
		}
		else{	
			name += LIBs.getRandom(syllable);
			name += LIBs.getRandom(vowel);
		}	
		
		var amt = LIBs.random(minlength,maxlength);	
		for (i = 0; i < amt; i++) { 
			var rnd = LIBs.random(1,4);	
			if(rnd==1){
				name += LIBs.getRandom(vowel);
			}
			else if(rnd==2){
				name += LIBs.getRandom(consonant);
			}
			else{	
				name += LIBs.getRandom(syllable);
				name += LIBs.getRandom(vowel);
			}			
		}
		return name;
	},
	MakeNameMan : function(){
		var minlength = 2;
		var maxlength = 5;
		
		var vowel = ['a','e','i','o','u'];
		var syllable = ['k','b','t'];
		var consonant = ['s','sh','m','mh','r','rr','n','l'];
		
		var name = '';
		
		var rnd = LIBs.random(1,3);	
		if(rnd==1){
			name += LIBs.getRandom(vowel);
		}
		else{	
			name += LIBs.getRandom(syllable);
			name += LIBs.getRandom(vowel);
		}	
		
		var amt = LIBs.random(minlength,maxlength);	
		for (i = 0; i < amt; i++) { 
			var rnd = LIBs.random(1,4);	
			if(rnd==1){
				name += LIBs.getRandom(vowel);
			}
			else if(rnd==2){
				name += LIBs.getRandom(consonant);
			}
			else{	
				name += LIBs.getRandom(syllable);
				name += LIBs.getRandom(vowel);
			}			
		}
		return name;
	},
	MakeNameWoman : function(){
		var minlength = 2;
		var maxlength = 5;
		
		var vowel = ['a','e','i','o','u'];
		var syllable = ['k','b','t'];
		var consonant = ['s','sh','m','mh','r','rr','n','l'];
		
		var name = '';
		
		var rnd = LIBs.random(1,3);	
		if(rnd==1){
			name += LIBs.getRandom(vowel);
		}
		else{	
			name += LIBs.getRandom(syllable);
			name += LIBs.getRandom(vowel);
		}	
		
		var amt = LIBs.random(minlength,maxlength);	
		for (i = 0; i < amt; i++) { 
			var rnd = LIBs.random(1,4);	
			if(rnd==1){
				name += LIBs.getRandom(vowel);
			}
			else if(rnd==2){
				name += LIBs.getRandom(consonant);
			}
			else{	
				name += LIBs.getRandom(syllable);
				name += LIBs.getRandom(vowel);
			}			
		}
		return name;
	}
}

var FamilyFactory = {
	CreatePerson : function(gender,type){
		var name = gender ? NameGenerator.MakeNameMan() : NameGenerator.MakeNameWoman();
		
		var person = {
			name : name,
			gender : gender,
			type : type,
		};
		
		var guid = LIBs.guid();
		
		GLOBAL.people[guid] = person;
		
		return guid; 
	},	
	CreateFamily : function(){
		var family = {
			name : NameGenerator.MakeSurname(),
			
			members : [
				this.CreatePerson(true,ENUM.Person.Adult),
				this.CreatePerson(false,ENUM.Person.Adult)
			],
			
			profession : ENUM.Profession.Farmer,

			buildings:[],
			
			resources:[
				{
					type: ENUM.Resources.Money,
					amount: 10
				}
			],			
		};
		return family;
	}
}

var TimeEventsHandler = {
	HourEvent : function(hour){
		
	},
	DayEvent : function(hour){
		
	},
	MonthEvent : function(hour){
		
	},
	SeasonEvent : function(hour){
		
	},
}

var Calendar = {
	tick : 0,
	hour : 0,
	day : 0,
	month : 0,
	season : 0,	//0 spring, 1 summer, 2 autumn, 3 winter
	
	hourTicks : 60,
	dayHours : 24,
	monthDays : 30,
	seasonMonths : 3,
	
	Update : function(){
		this.tick += 1;
		if(this.tick > this.hourTicks){			
			this.tick = 0;
			this.hour += 1; 
			TimeEventsHandler.HourEvent(this.hour);
		}
		if(this.hour > this.dayHours){			
			this.hour = 0;
			this.day += 1; 
			TimeEventsHandler.DayEvent(this.day);
		}
		if(this.day > this.monthDays){			
			this.day = 0;
			this.month += 1; 
			TimeEventsHandler.MonthEvent(this.month);
		}
		if(this.month > this.seasonMonths){			
			this.month = 0;
			this.season += 1; 
			TimeEventsHandler.SeasonEvent(this.season);
		}
	},
}









