var TreeFactory = {
	CreateTree(type){
		type.seed = LIBs.random(1,1000);
		return CreateTree(type);
	}
}

var ModelsFactory = {
	CreateHorse : function(){
		return AnimationHandler.LoadModel(GLOBAL.jsonModels.horse.val);
	},
	CreateMan : function(){
		loadedObject = GLOBAL.objModels.man.val.clone();
		var mesh;
		loadedObject.traverse( function ( child ) {
			if ( child instanceof THREE.SkinnedMesh ) {
				mesh = child;
			}
		} );

		mesh.rotation.x = Math.PI / 2;
		mesh.scale.set( 0.01,0.01, 0.01 );
		return mesh;
	}
}


var MeshFactory = {
	CreateWall : function(point1,point2,material,size){
		var vector12 = new THREE.Vector3().copy( point2 ).sub( point1 );
		var point3 = new THREE.Vector3().copy( vector12 ).multiplyScalar( 0.5 ).add( point1 );
		var plane = new THREE.BoxGeometry( 1, 4, 1 );
		var wall = new THREE.Mesh( plane, material);
		wall.position.copy( point3 );
		wall.scale.x = vector12.length();
		wall.scale.y = size;
		wall.scale.z = wall.position.z * 2;
		wall.rotation.z = Math.atan2( vector12.y, vector12.x );
		wall.doubleSided = true;
		return wall;
	}
}


var BuildingsFactory = {
	CreateHouse : function(){
		return this.CreateHouse2();
	},	
	CreateTower : function(){
		var mesh = new THREE.Mesh();
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		
		var geometry = new THREE.CylinderGeometry( 4.15, 4.15, 10, 8 );

		var cube = new THREE.Mesh( geometry, material );
		cube.rotation.x = Math.PI / 2;
		cube.position.set(0, 0, 2);	
		mesh.add(cube);
		
		var cube = new THREE.Mesh( geometry, material );	
		cube.rotation.x = Math.PI / 2;		
		cube.scale.set(1.2,0.5,1.2);
		cube.position.set(0, 0, 0);	
		mesh.add(cube);
		
		return mesh;
	},
	CreateHouse1 : function(){
		var mesh = new THREE.Mesh();
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		
		var geometry = new THREE.BoxGeometry(4.15,4.15,1);
		
		 geometry.computeFaceNormals(); geometry.computeVertexNormals();
		
		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(0, 0, -0.5);	
		mesh.add(cube);
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.softwall.val,
		  side: THREE.DoubleSide 
		});
		
		var shape = new THREE.Shape();
			shape.moveTo(-2,-1);
			shape.lineTo(2,-1);
			shape.lineTo(2,2);
			shape.lineTo(-2,2);
		var svgShape = new THREE.Shape();
		var svgGeom = new THREE.ShapeGeometry( shape );
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.x = Math.PI / 2;
		object.position.set(0, -2, 1);
		mesh.add(object);
		
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.x = Math.PI / 2;
		object.position.set(0, 2, 1);
		mesh.add(object);
		
		var shape = new THREE.Shape();
			shape.moveTo(-1,-1);
			shape.lineTo(2,-1);
			shape.lineTo(2,3);
			shape.lineTo(-1,3);
			shape.lineTo(-2,1);
		var svgShape = new THREE.Shape();
		var svgGeom = new THREE.ShapeGeometry( shape );
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.y = Math.PI / 2;
		object.position.set(2, -1, 2);
		mesh.add(object);
		
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.y = Math.PI / 2;
		object.position.set(-2, -1, 2);
		mesh.add(object);
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.roof.val,
		  side: THREE.DoubleSide 
		});
		
		var geometry = new THREE.BoxGeometry(4.15,2.5,0.2);
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(0, 1, 3.6);	
		cube.rotation.x = Math.PI / 0.54;
		mesh.add(cube);
		
		var geometry = new THREE.BoxGeometry(4.15,2.5,0.2);
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(0, -1, 3.6);	
		cube.rotation.x = Math.PI / -0.54;
		mesh.add(cube);
		
		return mesh;
	},
	CreateHouse2 : function(){
		var mesh = new THREE.Mesh();
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		
		var geometry = new THREE.BoxGeometry(4.15,4.15,1);
		
		 geometry.computeFaceNormals(); geometry.computeVertexNormals();
		
		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(0, 0, -0.5);	
		mesh.add(cube);
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.softwall.val,
		  side: THREE.DoubleSide 
		});
		
		var shape = new THREE.Shape();
			shape.moveTo(-2,-1);
			shape.lineTo(2,-1);
			shape.lineTo(2,3);
			shape.lineTo(-2,3);
		var svgShape = new THREE.Shape();
		var svgGeom = new THREE.ShapeGeometry( shape );
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.x = Math.PI / 2;
		object.position.set(0, -2, 1);
		mesh.add(object);
		
		var shape = new THREE.Shape();
			shape.moveTo(-2,-1);
			shape.lineTo(2,-1);
			shape.lineTo(2,2);
			shape.lineTo(-2,2);
		var svgShape = new THREE.Shape();
		var svgGeom = new THREE.ShapeGeometry( shape );
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.x = Math.PI / 2;
		object.position.set(0, 2, 1);
		mesh.add(object);
		
		var shape = new THREE.Shape();
			shape.moveTo(-2,-1);
			shape.lineTo(2,-1);
			shape.lineTo(2,3);
			shape.lineTo(-1,3);
		var svgShape = new THREE.Shape();
		var svgGeom = new THREE.ShapeGeometry( shape );
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.y = Math.PI / 2;
		object.position.set(2, -1, 2);
		mesh.add(object);
		
		var shape = new THREE.Shape();
			shape.moveTo(-2,-1);
			shape.lineTo(2,-1);
			shape.lineTo(2,3);
			shape.lineTo(-1,3);
		var svgShape = new THREE.Shape();
		var svgGeom = new THREE.ShapeGeometry( shape );
		var object = new THREE.Mesh( svgGeom, material ) ;
		object.rotation.y = Math.PI / 2;
		object.position.set(-2, -1, 2);
		mesh.add(object);
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.roof.val,
		  side: THREE.DoubleSide 
		});
		
		var geometry = new THREE.BoxGeometry(4.15,4.2,0.2);
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(0, 0, 3.6);	
		cube.rotation.x = Math.PI / 0.520;
		mesh.add(cube);
		
		return mesh;
	},
	CreateWall : function(point1,point2){
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		var wall = MeshFactory.CreateWall(point1,point2,material,1);
		wall.position.z = 4;
		GLOBAL.world.scene.add( wall );
	},
	CreateSmallWall : function(point1,point2){
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		var wall = MeshFactory.CreateWall(point1,point2,material,0.5);
		wall.position.z = 2;
		GLOBAL.world.scene.add( wall );
	},
	CreateGate : function(){
		var mesh = new THREE.Mesh();
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		
		var geometry = new THREE.BoxGeometry(5,5,8);
		geometry.computeFaceNormals(); geometry.computeVertexNormals();
		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(4, 0, 4);	
		mesh.add(cube);
		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(-4, 0, 4);	
		mesh.add(cube);
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.roof.val
		});
				
		var geometry = new THREE.BoxGeometry(5,1,5);
		geometry.computeFaceNormals(); geometry.computeVertexNormals();
		
		var cube = new THREE.Mesh( geometry, material );
		cube.position.set(0, 0, 3);	
		mesh.add(cube);
		
		return mesh;
	},
	CreateCistern : function(){
		var mesh = new THREE.Mesh();
		
		var material = new THREE.MeshPhongMaterial({
		  map: GLOBAL.textures.wall.val
		});
		
		var geometry = new THREE.CylinderGeometry( 3, 3, 3, 8 );

		var cube = new THREE.Mesh( geometry, material );
		cube.rotation.x = Math.PI / 2;
		cube.scale.x = 0.5;
		cube.position.set(0, 0, 0);	
		mesh.add(cube);
		
		return mesh;
	},
}	