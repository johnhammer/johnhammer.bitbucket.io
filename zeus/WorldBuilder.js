var WorldBuilder = {
	
	BuildWorld : function(scene){
		GLOBAL.world.terrain.light = this.AddLight(GLOBAL.world.scene);
	
		var water = this.AddWater(GLOBAL.world.scene,GLOBAL.world.terrain.light);
		this.AddSkybox(GLOBAL.world.scene,GLOBAL.world.terrain.light,water);
		
		this.AddTerrain(GLOBAL.world.scene);
	},	
	
	
	
	AddTerrain : function(scene){		
		//var heightMap	= TerrainBuilder.CreateRandomHeightMap(128,128);
		var heightMap	= TerrainBuilder.CreateCustomHeightMap(128,128,LIB.pixels);
		var geometry	= TerrainBuilder.CreateMap(heightMap);		

		var material	= new THREE.MeshPhongMaterial({
			map: GLOBAL.textures.dirt.val
		});
		var ground	= new THREE.Mesh( geometry, material );
		scene.add( ground );
		ground.scale.x	= 20*50;
		ground.scale.y	= 20*50;
		ground.scale.z	= 1*500; 
		
		ground.position.z = 680;
		
		GLOBAL.world.terrain.heightMap = heightMap;
		GLOBAL.world.terrain.ground = ground;
	},
	
	AddLight : function(scene){
		var light = new THREE.HemisphereLight( 0xffffbb, 0x080820, 1 );
		scene.add( light );
		return light;
	},
	
	AddWater : function(scene,light){	
		var waterGeometry = new THREE.PlaneBufferGeometry( 1024*1.5, 1024*1.5 );
		var water = new THREE.Water(
			waterGeometry,
			{
				textureWidth: 512,
				textureHeight: 512,
				waterNormals: new THREE.TextureLoader().load( 'https://threejs.org/examples/textures/waternormals.jpg', function ( texture ) {
					texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
				}),
				alpha: 1.0,
				sunDirection: light.position.clone().normalize(),
				sunColor: 0xffffff,
				waterColor: 0x001e0f,
				distortionScale:  3.7,
				fog: GLOBAL.world.scene.fog !== undefined
			}
		);
		water.position.z = 0;
		
		GLOBAL.world.terrain.water = water;
		scene.add( water );
		return water;
	},

	AddSkybox : function(scene,light,water){	
		var sky = new THREE.Sky();
		sky.scale.setScalar( 10000 );
		
		var uniforms = sky.material.uniforms;
		uniforms.turbidity.value = 10;
		uniforms.rayleigh.value = 2;
		uniforms.luminance.value = 1;
		uniforms.mieCoefficient.value = 0.005;
		uniforms.mieDirectionalG.value = 0.8;
		var parameters = {
			distance: 400,
			inclination: 0,
			azimuth: 0.205
		};
		
		GLOBAL.world.terrain.sky = sky;
		GLOBAL.world.terrain.sunParameters = parameters;
		this.UpdateSun(parameters,light,water,sky);
		
		scene.add( sky );
	},
	
	UpdateSun : function(parameters,light,water,sky) {
		var theta = Math.PI * ( parameters.inclination - 0.5 );
		var phi = 2 * Math.PI * ( parameters.azimuth - 0.5 );
		light.position.x = parameters.distance * Math.cos( phi );
		light.position.y = parameters.distance * Math.sin( phi ) * Math.sin( theta );
		light.position.z = parameters.distance * Math.sin( phi ) * Math.cos( theta );
		sky.material.uniforms.sunPosition.value = light.position.copy( light.position );
		water.material.uniforms.sunDirection.value.copy( light.position ).normalize();
	},

	UpdateWorld : function(parameters,light,water,sky){
		water.material.uniforms.time.value += 1.0 / 60.0;
		
		parameters.azimuth+=0.0001*GLOBAL.speed;
		if(parameters.azimuth>1) parameters.azimuth = 0;
		
		this.UpdateSun(parameters,light,water,sky);
	}
}
