var ENUM = {
	Person : {
		Nen 		: "Nen"			,
		Adult 		: "Adult"		,
		Vell 		: "Vell"		,
	},
	Profession : {
		Farmer		: "Farmer"		,
	},
	Resources : {
		Money		: "Money"		,
		Grain		: "Grain"		,
	},	
	Building : {
		Muralla    	: "Muralla"  	,
		Torre      	: "Torre"    	,
		Porta      	: "Porta"    	,
		Cuartel    	: "Cuartel"  	,
		Cisterna   	: "Cisterna" 	,
		Temple     	: "Temple"   	,
		Placa      	: "Placa"    	,
		Forn	   	: "Forn"		,
		Casa	   	: "Casa"		,
		Botiga     	: "Botiga"   	,
		Sitja      	: "Sitja"    	,
		Taller     	: "Taller"   	,
		Estable    	: "Estable"  	,
		Corral     	: "Corral"   	,
		Graner     	: "Graner"   	,
		Magatzem   	: "Magatzem" 	,
	},
	Tree : {
		Pi		    : "Pi"			,
		Alzina	    : "Alzina"		,
		Roure	    : "Roure"    	,
		Avet		: "Avet"		,
		Figuera	    : "Figuera"		,
		Magranera   : "Magranera" 	,
		Vinya	    : "Vinya"	    ,
		Olivera	    : "Olivera"		,
		Blat		: "Blat"		,
		Llegum	    : "Llegum"		,
		Arbust	    : "Arbust"		,
		Herba		: "Herba"		,
	},
	
}

var GLOBAL = {
	textures : {
		wall 		: {url: 'bigwall.jpg'			},
		softwall 	: {url: 'towerwall2.jpg'		},
		roof 		: {url: 'roof.jpg'				},
		dirt 		: {url: 'dryishgrass2.jpg'		}
	},
	jsonModels : {
		horse 		: {url: 'horse.js'		},
		//man 		: {url: 'man.js'		},
		/*
		dog : {
			url: 'dog.js',
			val: undefined
		},
		sheep : {
			url: 'sheep.js',
			val: undefined
		},
		goat : {
			url: 'goat.js',
			val: undefined
		},
		cow : {
			url: 'cow.js',
			val: undefined
		},
		pig : {
			url: 'pig.js',
			val: undefined
		},
		deer : {
			url: 'deer.js',
			val: undefined
		},
		bird : {
			url: 'bird.js',
			val: undefined
		},
		man : {
			url: 'man.js',
			val: undefined
		},
		woman : {
			url: 'woman.js',
			val: undefined
		},
		*/
	},
	objModels : {
		man 		: {url: 'https://bitbucket.org/johnhammer/johnhammer.bitbucket.io/raw/29bd55576d186aad526ecc9b0e543feade614ff6/zeus/assets/marine_anims_core.json'		},
	},
	animations : [],
	UI : {
		menu : null,
		menuElements : {},
	},
	world : {
		lastTime : null,
		terrain : {
			heightMap : null,
			ground : null,
		}
	},
	speed : 1,
	families : {},
	people : {},
};

var LIBs = {
	random : function(min,max){
		return Math.floor(Math.random()*(max-min+1)+min);
	},
	getRandom : function(array){
		return array[Math.floor(Math.random() * array.length)];
	},
	guid() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
	},
}

var HtmlInterface = {
	updateMenu : function() {
		var querySelector = document.querySelector.bind(document);
		var nav = document.querySelector('.vertical_nav');
		var wrapper = document.querySelector('.wrapper');
		var menu = document.getElementById("js-menu");
		var subnavs = menu.querySelectorAll('.menu--item__has_sub_menu');    

		querySelector('.toggle_menu').onclick = function () {
			nav.classList.toggle('vertical_nav__opened');
			wrapper.classList.toggle('toggle-content');
		};

		querySelector('.collapse_menu').onclick = function () {
			nav.classList.toggle('vertical_nav__minify');
			wrapper.classList.toggle('wrapper__minify');
			for (var j = 0; j < subnavs.length; j++) {
				subnavs[j].classList.remove('menu--subitens__opened');
			}
		};

		for (var i = 0; i < subnavs.length; i++) {
			if (subnavs[i].classList.contains('menu--item__has_sub_menu') ) {
				subnavs[i].querySelector('.menu--link').addEventListener('click', function (e) {
					for (var j = 0; j < subnavs.length; j++) {
						if(e.target.offsetParent != subnavs[j])
							subnavs[j].classList.remove('menu--subitens__opened');          
					}
					e.target.offsetParent.classList.toggle('menu--subitens__opened');
				}, false);
			}
		}
	},			
	
	createMenuElement(key,icon,items){
		/*
			users
			monument
			pagelines
			globe
			coins
		*/
		
		var element = this.getMenu(key,icon,items);
		GLOBAL.UI.menu.innerHTML += element;
		GLOBAL.UI.menuElements[key] = document.getElementById(key+"_sub_menu");
	},
	createMenuElementItem(key,name,action){
		document.getElementById(key+"_sub_menu").innerHTML += this.getMenuItem(name,action);
	},
	
	createMenu(){
		function buildActionItem(name,action){
			return {name:name,action:"document.getElementsByClassName('vertical_nav vertical_nav__opened')[0].classList.remove('vertical_nav__opened');event.stopPropagation();ActionsHandler.setBuildAction(ActionsHandler.actions."+action+")"}
		}		
		
		GLOBAL.UI.menu = document.getElementById("js-menu");
				
		var items;
		
		items = [
			buildActionItem("Xai",			"buildHouse"	),
			buildActionItem("Cabra",		"buildHouse"	),
			buildActionItem("Vaca",			"buildHouse"	),
			buildActionItem("Porc",			"buildHouse"	),
			buildActionItem("Cavall",		"createHorse"	),
			buildActionItem("Ase",			"buildHouse"	),
			buildActionItem("Mula",			"buildHouse"	),
			buildActionItem("Gos",			"buildHouse"	),
			buildActionItem("Home",			"createMan"		),
			buildActionItem("Dona",			"buildHouse"	),
			buildActionItem("Guerrer",		"buildHouse"	),
			buildActionItem("Jenet",		"buildHouse"	)
		];
		this.createMenuElement("Families","users",items);
		
		items = [			
			buildActionItem("Muralla",		"buildWall"		),
			buildActionItem("Torre",		"buildTower"	),
			buildActionItem("Porta",		"buildGate"		),
			buildActionItem("Cuartel",		"buildBarracks"	),
			buildActionItem("Cisterna",		"buildCistern"	),
			buildActionItem("Temple",		"buildTemple"	),
			buildActionItem("Placa",		"buildHall"		),
			buildActionItem("Forn",			"buildFurnace"	),
			buildActionItem("Casa",			"buildHouse"	),
			buildActionItem("Botiga",		"buildShop"		),
			buildActionItem("Sitja",		"buildGranary"	),
			buildActionItem("Taller",		"buildWorkshop"	),
			buildActionItem("Estable",		"buildStall"	),
			buildActionItem("Corral",		"buildSmallWall"),
			buildActionItem("Graner",		"buildGranary"	),
			buildActionItem("Magatzem",		"buildWarehosue")
		];
		this.createMenuElement("Buildings","building",items);
		
		items = [
			buildActionItem("Pi",			"plantPi"		),
			buildActionItem("Alzina",		"plantAlzina"	),
			buildActionItem("Roure",		"plantRoure"	),
			buildActionItem("Avet",			"plantAvet"		),
			buildActionItem("Figuera",		"plantFiguera"	),
			buildActionItem("Magranera",	"plantMagranera"),
			buildActionItem("Vinya",		"plantVinya"	),
			buildActionItem("Olivera",		"plantOlivera"	),
			buildActionItem("Blat",			"buildHouse"	),
			buildActionItem("Llegum",		"buildHouse"	),
			buildActionItem("Arbust",		"buildHouse"	),
			buildActionItem("Herba",		"buildHouse"	)
		];
		this.createMenuElement("Agriculture","pagelines",items);
		
		items = [];
		this.createMenuElement("World","globe",items);
		
		this.updateMenu();
	},
	getMenu : function(name,icon,items){
		var html = "";
		html += '<li class="menu--item '+(items.length>0 ? "menu--item__has_sub_menu" : "")+'">';
		html += '	<label class="menu--link" title="ItemaLosowa">';
		html += '		<i class="menu--icon  fa fa-fw fa-'+icon+'"></i>';
		html += '		<span class="menu--label">'+name+'</span>';
		html += '	</label>';
		html += '	<ul id="'+name+'_sub_menu" class="sub_menu">';
		for (var i = 0; i < items.length; i++){
			html += this.getMenuItem(items[i].name,items[i].action);
		}				
		html += '	</ul>';
		html += '</li>';
		return html;
	},
	getMenuItem : function(name,action){
		var html = "";
		html += '<li class="sub_menu--item">';
		html += '	<a href="#" class="sub_menu--link sub_menu--link__active" onclick="'+action+'">';
		html += name;
		html += '	</a>';
		html += '</li>';
		return html;		
	},
}

var ActionsHandler = {
	currentAction : null,
	currentMesh : null,
	currentMeshMask : null,
	point1 : null,
	point2 : null,
	isAnimated : false,
	isAutoAnimated : false,
	
	selectableMeshes : [],
	
	actions : {
		buildHouse 		:0,
		buildTower 		:1,
		buildWall 		:2,
		plantPi		   	:3,
		plantAlzina	   	:4,
		plantRoure	   	:5,
		plantAvet		:6,
		plantFiguera	:7,
		plantMagranera 	:8,
		plantVinya     	:9,
		plantOlivera	:10,	
		createHorse 	:11,
		createMan 		:12,
		buildGate		:13,
		buildBarracks   :14,
		buildCistern	:15,
		buildTemple	    :16,
		buildHall		:17,
		buildFurnace	:18,
		buildShop	    :20,
		buildWorkshop   :21,
		buildStall	    :22,
		buildSmallWall  :23,
		buildGranary	:24,
		buildWarehosue  :25,		
	},
	setBuildAction : function(action){
		this.currentAction = action;
		
		switch(action){
			case this.actions.buildHouse	 :this.setBuildMesh(BuildingsFactory.CreateHouse()				); break;
			case this.actions.buildTower	 :this.setBuildMesh(BuildingsFactory.CreateTower()				); break;
			
			case this.actions.plantPi		 :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.pi			));break;
			case this.actions.plantAlzina	 :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.alzina		));break;
			case this.actions.plantRoure	 :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.roure		));break;
			case this.actions.plantAvet		 :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.avet		));break;
			case this.actions.plantFiguera	 :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.figuera	));break;
			case this.actions.plantMagranera :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.magranera	));break;
			case this.actions.plantVinya     :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.vinya    	));break;
			case this.actions.plantOlivera	 :this.setBuildMesh(TreeFactory.CreateTree(TreeTypes.olivera	));break;
			
			case this.actions.createHorse	 :this.setAnimatedBuildMesh(ModelsFactory.CreateHorse()			); break;
			case this.actions.createMan		 :this.setAutoAnimatedBuildMesh(ModelsFactory.CreateMan()		); break;
			
			case this.actions.buildGate		 :this.setBuildMesh(BuildingsFactory.CreateGate()				); break;
			case this.actions.buildBarracks  :this.setBuildMesh(BuildingsFactory.CreateBarracks()			); break;
			case this.actions.buildCistern	 :this.setBuildMesh(BuildingsFactory.CreateCistern()			); break;
			case this.actions.buildTemple	 :this.setBuildMesh(BuildingsFactory.CreateTemple()				); break;
			case this.actions.buildHall		 :this.setBuildMesh(BuildingsFactory.CreateHall()				); break;
			case this.actions.buildFurnace	 :this.setBuildMesh(BuildingsFactory.CreateFurnace()			); break;
			case this.actions.buildShop	     :this.setBuildMesh(BuildingsFactory.CreateShop()				); break;
			case this.actions.buildWorkshop  :this.setBuildMesh(BuildingsFactory.CreateWorkshop()			); break;
			case this.actions.buildStall	 :this.setBuildMesh(BuildingsFactory.CreateStall()				); break;
			case this.actions.buildGranary	 :this.setBuildMesh(BuildingsFactory.CreateGranary()			); break;
			case this.actions.buildWarehosue :this.setBuildMesh(BuildingsFactory.CreateWarehouse()			); break;
		}
	},
	setAnimatedBuildMesh : function(mesh){
		this.isAnimated = true;
		this.setBuildMesh(mesh);
	},	
	setAutoAnimatedBuildMesh : function(mesh){
		this.isAutoAnimated = true;
		this.setBuildMesh(mesh);
	},
	setBuildMesh : function(mesh){
		GLOBAL.world.scene.remove(this.currentMeshMask);		
		this.currentMesh = mesh;
		this.currentMeshMask = mesh.clone();
				
		this.currentMeshMask.traverse(function ( geo ) { geo.material = new THREE.MeshLambertMaterial( { 
			color: 0x900000 , 
			side: THREE.DoubleSide, 
			transparent :true, opacity  : 0.5 
		} ); } );
		
		GLOBAL.world.scene.add(this.currentMeshMask);	
	},
	BindEvents : function(){
		window.addEventListener('resize', function(e){this.onWindowResize(e)}.bind(this), false );	
		document.addEventListener('mousemove', function(e){this.onMouseMove(e)}.bind(this), false);
		document.addEventListener('click', function(e){this.onMouseClick(e)}.bind(this), false);
	},

	onWindowResize : function(){
		GLOBAL.world.camera.aspect = window.innerWidth / window.innerHeight;
		GLOBAL.world.camera.updateProjectionMatrix();
		GLOBAL.world.renderer.setSize( window.innerWidth, window.innerHeight );
	},

	
	mouse : {x: 0, y: 0},
	
	raycaster : new THREE.Raycaster(),
	
	onMouseMove(event) {
		
		if(this.currentAction == null ||this.currentAction == this.actions.buildWall) return;
		
		event.preventDefault();
		
		this.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
		this.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
		this.raycaster.setFromCamera( this.mouse, GLOBAL.world.camera );
		var intersects = this.raycaster.intersectObjects( [GLOBAL.world.terrain.ground] );

		for ( var i = 0; i < intersects.length; i++ ) {
			this.currentMeshMask.position.copy(intersects[ i ].point);
		}

	},

	onMouseClick(event) {
			
		if(this.currentAction == null) return;
		
		if(this.currentAction == this.actions.buildWall){
			this.buildTwoPointsAction(event, BuildingsFactory.CreateWall);
		}
		else if(this.currentAction == this.actions.buildSmallWall){
			this.buildTwoPointsAction(event, BuildingsFactory.CreateSmallWall);
		}
		else{
			this.buildAction();		
		}
	},
	
	buildTwoPointsAction(event, func){
		var object = this.selectObject(event);
		if(object){
			
			if(!this.point1){
				this.point1 = object.parent.position;
			}
			else if(!this.point2){
				this.point2 = object.parent.position;
				func(this.point1,this.point2);
				this.currentAction = null;
				this.point1 = null;
				this.point2 = null;
			}
		}
	},
	
	buildAction(){
		this.currentMesh.position.copy(this.currentMeshMask.position);
		
		var mesh = this.currentMesh.clone();
		
		GLOBAL.world.scene.add(mesh);

		if(this.isAnimated){
			var mixer = new THREE.AnimationMixer( mesh );

			var clip = THREE.AnimationClip.CreateFromMorphTargetSequence( 'gallop', mesh.geometry.morphTargets, 30 );
			mixer.clipAction( clip ).setDuration( 1 ).play();
			
			GLOBAL.animations.push(mixer);		
			
			this.isAnimated = false;
		}
		
		if(this.isAutoAnimated){
			var mixer = new THREE.AnimationMixer( mesh );

			mixer.clipAction( 'walk' ).play();
			
			GLOBAL.animations.push(mixer);		
			
			this.isAutoAnimated = false;
		}
		
		this.selectableMeshes.push(mesh);
		
		var superbuild = false
		if(!superbuild){
			this.currentAction = null;
			GLOBAL.world.scene.remove(this.currentMeshMask);
		}
	},
	
	selectObject(event){
		event.preventDefault();

		var rect = GLOBAL.world.renderer.domElement.getBoundingClientRect();
		 
		var mouse = {}
		mouse.x = ( ( event.clientX - rect.left ) / ( rect.width - rect.left ) ) * 2 - 1;
		mouse.y = - ( ( event.clientY - rect.top ) / ( rect.bottom - rect.top) ) * 2 + 1;

		var raycaster =  new THREE.Raycaster();         

		raycaster.setFromCamera( mouse, GLOBAL.world.camera );
		var intersects = raycaster.intersectObjects( this.selectableMeshes , true );

		if ( intersects.length > 0 ) {
			return intersects[ 0 ].object;
		}
		return null;
	},
	
}

var AnimationHandler = {
	LoadModel : function(geometry){
		var mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial( {
			vertexColors: THREE.FaceColors,
			morphTargets: true
		} ) );
		mesh.scale.set( 0.01,0.01, 0.01 );
		mesh.rotation.x = Math.PI/2;
		return mesh;
	}
}

var TexturesManager = {
	ConfigureTextures : function(){
		//textures.softwall.val.wrapS = THREE.ClampToEdgeWrapping

		GLOBAL.textures.wall.val.wrapS = THREE.RepeatWrapping;
		GLOBAL.textures.wall.val.wrapT = THREE.RepeatWrapping;
		GLOBAL.textures.wall.val.repeat.x = 3;
		GLOBAL.textures.wall.val.repeat.y = 3;
		
		GLOBAL.textures.softwall.val.wrapS = THREE.RepeatWrapping;
		GLOBAL.textures.softwall.val.wrapT = THREE.RepeatWrapping;
		//textures.softwall.val.repeat.x = 1.1;
		//textures.softwall.val.repeat.y = 1.1;
		
		GLOBAL.textures.roof.val.wrapS = THREE.RepeatWrapping;
		GLOBAL.textures.roof.val.wrapT = THREE.RepeatWrapping;
		//textures.roof.val.repeat.x = 1.1;
		//textures.roof.val.repeat.y = 1.1;
		
		GLOBAL.textures.dirt.val.wrapS = THREE.RepeatWrapping;
		GLOBAL.textures.dirt.val.wrapT = THREE.RepeatWrapping;
		GLOBAL.textures.dirt.val.repeat.x = 20;
		GLOBAL.textures.dirt.val.repeat.y = 20;
	}

}

////////////////////////////////////////////////////

function Preload(){
	
	var promises = [];
	var path = 'https://johnhammer.bitbucket.io/zeus/textures/';
	
	var textureLoader = new THREE.TextureLoader();
	for (var key in GLOBAL.textures) {
		promises.push(new Promise((resolve, reject) => {
			var entry = GLOBAL.textures[key]
			var url = path + entry.url
			textureLoader.load(url,
				texture => {
					entry.val = texture;
					resolve(entry);
				},
				xhr => {
					console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
				},
				xhr => {}
			)	  
		}));
	}
	
	/*
	var modelLoader = new THREE.JSONLoader();
	modelLoader.setCrossOrigin('');
	for (var key in GLOBAL.jsonModels) {
		promises.push(new Promise((resolve, reject) => {
			var entry = GLOBAL.jsonModels[key]
			var url = 'https://johnhammer.bitbucket.io/zeus/assets/' + entry.url
			modelLoader.load(url,
				texture => {
					entry.val = texture;
					resolve(entry);
				},
				xhr => {
					console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
				},
				xhr => {}
			)	  
		}));
	}
	
	var modelLoader = new THREE.ObjectLoader();
	modelLoader.setCrossOrigin('');
	for (var key in GLOBAL.objModels) {
		promises.push(new Promise((resolve, reject) => {
			var entry = GLOBAL.objModels[key]
			var url = entry.url
			modelLoader.load(url,
				texture => {
					entry.val = texture;
					resolve(entry);
				},
				xhr => {
					console.log(url + ' ' + (xhr.loaded / xhr.total * 100) + '% loaded');
				},
				xhr => {}
			)	  
		}));
	}
	*/
	
	Promise.all(promises).then(loadedTextures => {
		Start();
	});
}

function Start(){
	
	TexturesManager.ConfigureTextures();
	
	
	var width  = window.innerWidth//document.getElementById('webgl').offsetWidth;
	var height = window.innerHeight//document.getElementById('webgl').offsetHeight;

	GLOBAL.world.scene = new THREE.Scene();
		
	GLOBAL.world.camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 10000);
	GLOBAL.world.camera.position.set(0, -30, 30);
	GLOBAL.world.camera.up.set( 0, 0, 1 );

	GLOBAL.world.renderer = new THREE.WebGLRenderer();
	GLOBAL.world.renderer.setSize(width, height);

	WorldBuilder.BuildWorld(GLOBAL.world.scene);

	
	
	

	GLOBAL.world.controls = new THREE.OrbitControls(GLOBAL.world.camera); 

	document.getElementById('webgl').appendChild(GLOBAL.world.renderer.domElement);

	GLOBAL.speed = 1;
	
	GLOBAL.world.lastTime = Date.now();
	
	Update();
	
	ActionsHandler.BindEvents();	
}

function Update() {
	GLOBAL.world.controls.update();    
	requestAnimationFrame(Update);
	GLOBAL.world.renderer.render(GLOBAL.world.scene, GLOBAL.world.camera);
	
	var time = Date.now();
	for (var i = 0; i < GLOBAL.animations.length; i++) {
		GLOBAL.animations[i].update( ( time - GLOBAL.world.lastTime ) * 0.001 );
	}
	GLOBAL.world.lastTime = time;
	
	WorldBuilder.UpdateWorld(
		GLOBAL.world.terrain.sunParameters,
		GLOBAL.world.terrain.light,
		GLOBAL.world.terrain.water,
		GLOBAL.world.terrain.sky
	);
	
	
}

function StartGame(){
	HtmlInterface.createMenu();

	Preload();
}



