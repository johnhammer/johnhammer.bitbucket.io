var gameScene;

var CONST = {
	IBERIA : {
		casa1		: {s:2,i:11-1},
		casa2	    : {s:2,i:12-1},
		taller1	    : {s:2,i:16-1},
		taller2	    : {s:2,i:15-1},
		taller3	    : {s:2,i:14-1},
		taller4	    : {s:2,i:13-1},
		tienda1	    : {s:2,i:3-1},
		tienda2	    : {s:2,i:4-1},
		tienda3	    : {s:2,i:10-1},
		tienda4	    : {s:2,i:5-1},
		choza1	    : {s:2,i:41-1},
		corral1	    : {s:1,i:18-1},
		corral2	    : {s:1,i:17-1},
		corral3	    : {s:1,i:20-1},
		corral4	    : {s:1,i:21-1},
		corral5	    : {s:1,i:19-1},
		establo1    : {s:2,i:2-1},
		establo2    : {s:2,i:32-1},
		establo3    : {s:2,i:31-1},
		horno1	    : {s:1,i:9-1},
		horno2	    : {s:1,i:8-1},
		horno3	    : {s:1,i:7-1},
		arbol	    : {s:1,i:30-1},
		frutero	    : {s:1,i:29-1},
		trigo	    : {s:1,i:35-1},
		
		dirt 		: {s:1,i:22-1},
	}

}

var GLOBAL = {
	SPRITES : {
		unit : null,
		iberia : null,
	},
	mapSize : 10,
	map : [],
	colmap : [],
	viewport : {
		width : 800,
		heigth : 640
	},
	tileWidth : 30,
	halfTileWidth : 30/2,
	borderOffset : { 		//new Phaser.Point(300,00);
		x : 300,
		y : 0
	},
	units : {},
	UI : {
		menu : null,
		menuElements : {},
	},
	families : {},
	buildings : {},
	
}

var EVENTS = {
	leftClick : function(){
		var pos = game.input.activePointer.position;
		var isoPt = new Phaser.Point(pos.x-GLOBAL.borderOffset.x,pos.y-GLOBAL.borderOffset.y);
		var tapPos = LIB.isometricToCartesian(isoPt);
		tapPos.x -= GLOBAL.halfTileWidth;
		tapPos.y += GLOBAL.halfTileWidth;
		tapPos = LIB.getTileCoordinates(tapPos,GLOBAL.tileWidth);

		if(tapPos.x>-1&&tapPos.y>-1&&tapPos.x<GLOBAL.map[0].length&&tapPos.y<GLOBAL.map.length){
			ActionsHandler.leftClickAction(tapPos);
		}
	}
}

var LIB = {
	clone : function(obj){
		return obj;
	},
	getTileCoordinates : function(cartPt, tileHeight){
		var tempPt=new Phaser.Point();
		tempPt.x=Math.floor(cartPt.x/tileHeight);
		tempPt.y=Math.floor(cartPt.y/tileHeight);
		return(tempPt);
	},
	cartesianToIsometric : function(cartPt){
		var tempPt=new Phaser.Point();
		tempPt.x=cartPt.x-cartPt.y;
		tempPt.y=(cartPt.x+cartPt.y)/2;
		return (tempPt);
	},
	isometricToCartesian : function(isoPt){
		var tempPt=new Phaser.Point();
		tempPt.x=(2*isoPt.y+isoPt.x)/2;
		tempPt.y=(2*isoPt.y-isoPt.x)/2;
		return (tempPt);
	},
	random(min,max)	{
		return Math.floor(Math.random()*(max-min+1)+min);
	},
	guid() {
	  function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	  }
	  return s4() + s4() + s4() + s4() + s4() + s4() + s4() + s4();
	},

}

var Drawer = {
	drawUnit : function(unit){	
		var realpos = LIB.cartesianToIsometric(unit.heroMapPos);
		
		var heroWidth = GLOBAL.tileWidth;
		var heroHeight = GLOBAL.tileWidth;

		gameScene.renderXY(
			unit.sprite,
			realpos.x+GLOBAL.borderOffset.x+heroWidth, 
			realpos.y+GLOBAL.borderOffset.y-heroHeight, 
			false);
	},

	drawUnits(cell,i,j){		
		for (var unit in GLOBAL.units){
			if(GLOBAL.units[unit] && GLOBAL.units[unit].heroMapTile.x==j&&GLOBAL.units[unit].heroMapTile.y == i){
				this.drawUnit(GLOBAL.units[unit]);
			}
		}			
	},
	
	drawCellHigh : function(cell,i,j){
		if(cell.i){		
			cell.s.frame = CellInterface.getCellHighImage(cell);
			gameScene.renderXY(cell.s,CellInterface.getX(cell),CellInterface.getY(cell),false);		
		}
		this.drawUnits(cell,i,j);
	},
	
	drawCellFloor : function(cell,i,j){
		if(cell.ti){	
			cell.ts.frame = CellInterface.getCellLowImage(cell);
			gameScene.renderXY(cell.ts,CellInterface.getTX(cell),CellInterface.getTY(cell),false);		
		}
	},

	drawLow : function(){
		for (var y = 0; y < GLOBAL.map.length; y++){
			for (var x = 0; x < GLOBAL.map[0].length; x++){
				this.drawCellFloor(GLOBAL.map[x][y],x,y);
			}
		}
	},
	
	drawHigh : function(){
		var size = GLOBAL.map.length;
		var x, y;
		for(i=0;i<size;i++){
			x = 0;
			y = i;
			for(j=0;j<i+1;j++){
				this.drawCellHigh(GLOBAL.map[x][y],x,y);
				x+=1;
				y-=1;
			}
		}
		for(i=size-1;i>0;i--){
			x = size-i;
			y = size-1;
			for(j=i;j>0;j--){
				this.drawCellHigh(GLOBAL.map[x][y],x,y);
				x+=1;
				y-=1;
			}
		}
	},

	render : function(){
		gameScene.clear();
		this.drawLow();
		this.drawHigh();		
	},
}

var CellInterface = {
	createEmptyCell : function(j,i){
		cell = {
			ex : {
				i:i,
				j:j
			},
			b : true
		};
		this.setTerrainImage(cell,CONST.IBERIA.dirt.i,GLOBAL.SPRITES.iberia);
		return cell;
	},
	getX : function(cell){return cell.x;},
	getY : function(cell){return cell.y;},
	getTX : function(cell){return cell.tx;},
	getTY : function(cell){return cell.ty;},
	canBuild : function(cell){
		return cell.b;
	},
	iImage : function(cell,tileType,sprite){
		var floorGraphicHeight=GLOBAL.tileWidth;   
		var floorGraphicWidth=GLOBAL.tileWidth;   
		sprite.frame = tileType;
		var isoPt= new Phaser.Point();
		var cartPt=new Phaser.Point();
		cartPt.x=cell.ex.j*GLOBAL.tileWidth;
		cartPt.y=cell.ex.i*GLOBAL.tileWidth;
		isoPt=LIB.cartesianToIsometric(cartPt);
		var wallGraphicWidth=sprite.width/2-1;           
		var wallGraphicHeight=sprite.height;	
		var wallHeight=wallGraphicHeight-floorGraphicHeight; 
		var wallWidth=wallGraphicWidth-floorGraphicWidth;
		var x = isoPt.x+GLOBAL.borderOffset.x-wallWidth;
		var y = isoPt.y+GLOBAL.borderOffset.y-wallHeight;
	
		return {x:x,y:y};
	},
	setImage : function(cell,tileType,sprite){
		pos = this.iImage(cell,tileType,sprite);
		cell.x = pos.x;
		cell.y = pos.y;	
		cell.i = tileType;
		cell.s = sprite;
	},
	setTerrainImage : function(cell,tileType,sprite){
		pos = this.iImage(cell,tileType,sprite);
		cell.tx = pos.x;
		cell.ty = pos.y;	
		cell.ti = tileType;
		cell.ts = sprite;
	},
	getCellHighImage : function(cell){
		return cell.i;
	},
	getCellLowImage : function(cell){
		return cell.ti;
	},
}

var BuildingsFactory = {
	buildBasic(pos,image,size){
		if(CellInterface.canBuild(GLOBAL.map[pos.y][pos.x])){
			CellInterface.setImage(
				GLOBAL.map[pos.y][pos.x],
				image,
				GLOBAL.SPRITES.iberia);
				
			var cells = CellCalculator.getSquare(size,pos.y,pos.x);	
			for(var i = 0; i < cells.x.length; i++){
				GLOBAL.colmap[cells.x[i]][cells.y[i]] = 1;
			}
			
			var key = LIB.guid();
			
			GLOBAL.buildings[key] = {
				family : GLOBAL.currentFamily,
				type : image,
				pos : pos,
				size : size
			};
			var family = FamilyInterface.addBuilding(GLOBAL.currentFamily,key,image);
			
		}
	}
}

var ActionsHandler = {
	action : null,
	setFamily : function(key){
		FamilyInterface.setFamily(key);
	},
	setBuildAction : function(building){
		this.action = {
			type : "build",
			image : building.i,
			size : building.s
		};
	},
	leftClickAction : function(tapPos){
		if(!this.action) return;
		if(this.action.type == "build"){
			BuildingsFactory.buildBasic(tapPos,this.action.image,this.action.size);
			this.action = null;
		}		
	}
}

var MapInterface = {
	createMap : function(size){
		GLOBAL.map = [];
		GLOBAL.colmap = [];
		for(var y = 0; y < size; y++){
			GLOBAL.map.push([]);
			GLOBAL.colmap.push([]);
			for(var x = 0; x < size; x++){
				GLOBAL.map[y].push(CellInterface.createEmptyCell(x,y));
				GLOBAL.colmap[y].push(0);
			}
		}
	},
	getCollisionMap : function(){
		return GLOBAL.colmap;
	},
}

var CellCalculator = {
	checkSquareLimits(size,x_ini,y_ini){
		return x_ini<GLOBAL.mapSize && 
			y_ini<GLOBAL.mapSize && 
			x_ini-size>=0 && 
			y_ini-size>=0;
	},
	getSquare(size,x_ini,y_ini){
		var xx = [];
		var yy = [];
		for(var y = y_ini; y > y_ini-size; y--){
			for(var x = x_ini; x > x_ini-size; x--){
				xx.push(x);
				yy.push(y);
			}
		}
		return {x:xx,y:yy};
	},
	getSquareAround(size,x_ini,y_ini){
		var xx = [];
		var yy = [];
		
		x = x_ini+1;
		for(var y = y_ini+1; y > y_ini-size-1; y--){
			xx.push(x);
			yy.push(y);
		}
		y = y_ini+1;
		for(var x = x_ini; x > x_ini-size-1; x--){
			xx.push(x);
			yy.push(y);
		}
		x = x_ini-size;
		for(var y = y_ini; y > y_ini-size; y--){
			xx.push(x);
			yy.push(y);
		}
		y = y_ini-size;
		for(var x = x_ini; x > x_ini-size-1; x--){
			xx.push(x);
			yy.push(y);
		}
		return {x:xx,y:yy};
	},
	
}

var UnitInterface = {
	reach: function(unit){
		GLOBAL.units[unit.key] = null;
	},

	findPath : function(unit){
		if(unit.isFindingPath || unit.isWalking) return;
		
		var pos = unit.finalDestination;

		unit.isFindingPath=true;
		
		unit.ai.findPath(unit.heroMapTile.x, unit.heroMapTile.y, pos.x, pos.y, function(newPath){this.moveTile(newPath,pos,unit)}.bind(this));
		unit.ai.calculate();
	},

	moveTile : function(newPath,pos,unit){
		unit.destination=unit.heroMapTile;
		unit.path=newPath;
		unit.isFindingPath=false;
		if (unit.path === null) {
			console.log("No Path was found.");
		}else{
			unit.path.push(pos);
			unit.path.reverse();
			unit.path.pop();        
		}
	},

	aiWalk : function(unit){
		this.aiStep(unit);
		
		if (unit.dY == 0 && unit.dX == 0)
		{
			unit.sprite.animations.stop();
			unit.sprite.animations.currentAnim.frame=0;			
		}else{
			if(unit.sprite.animations.currentAnim.name!=unit.facing){
				unit.sprite.animations.play(unit.facing);
			}
		}
		
		unit.heroMapPos.x +=  unit.heroSpeed * unit.dX;
		unit.heroMapPos.y +=  unit.heroSpeed * unit.dY;
		unit.heroMapTile=LIB.getTileCoordinates(unit.heroMapPos,30);
	},

	aiStep : function(unit){
		if(unit.path.length==0){
			if(unit.heroMapTile.x==unit.destination.x&&unit.heroMapTile.y==unit.destination.y){
				unit.dX=0;
				unit.dY=0;			
				if(unit.isWalking){
					unit.isWalking=false;
					this.reach(unit);
				}	
				return;
			}
		}
		unit.isWalking=true;
		if(unit.heroMapTile.x==unit.destination.x&&unit.heroMapTile.y==unit.destination.y){//reached current unit.destination, set new, change direction
			//wait till we are few steps into the tile before we turn
			unit.stepsTaken++;
			if(unit.stepsTaken<unit.stepsTillTurn){
				return;
			}
			//centralise the hero on the tile    
		   
			unit.stepsTaken=0;
			unit.destination=unit.path.pop();//whats next tile in unit.path
			if(unit.heroMapTile.x<unit.destination.x){
				unit.dX = 1;
			}else if(unit.heroMapTile.x>unit.destination.x){
				unit.dX = -1;
			}else {
				unit.dX=0;
			}
			if(unit.heroMapTile.y<unit.destination.y){
				unit.dY = 1;
			}else if(unit.heroMapTile.y>unit.destination.y){
				unit.dY = -1;
			}else {
				unit.dY=0;
			}
			if(unit.heroMapTile.x==unit.destination.x){//top or bottom
				unit.dX=0;
			}else if(unit.heroMapTile.y==unit.destination.y){//left or right
				unit.dY=0;
			}
			//figure out which direction to face
			if (unit.dX==1)
			{
				if (unit.dY == 0)
				{
					unit.facing = "east";
				}
				else if (unit.dY==1)
				{
					unit.facing = "southeast";
					unit.dX = unit.dY=unit.halfSpeed;
				}
				else
				{
					unit.facing = "northeast";
					unit.dX=unit.halfSpeed;
					unit.dY=-1*unit.halfSpeed;
				}
			}
			else if (unit.dX==-1)
			{
				unit.dX = -1;
				if (unit.dY == 0)
				{
					unit.facing = "west";
				}
				else if (unit.dY==1)
				{
					unit.facing = "southwest";
					unit.dY=unit.halfSpeed;
					unit.dX=-1*unit.halfSpeed;
				}
				else
				{
					unit.facing = "northwest";
					unit.dX = unit.dY=-1*unit.halfSpeed;
				}
			}
			else
			{
				unit.dX = 0;
				if (unit.dY == 0)
				{
					//unit.facing="west";
				}
				else if (unit.dY==1)
				{
					unit.facing = "south";
				}
				else
				{
					unit.facing = "north";
				}
			}
					
			
		}	
	},

	aiStart : function(unit){
		unit.ai = AiFactory.getStrategy(
			AiFactory.basic, 
			MapInterface.getCollisionMap(),
			false, //allow diagonals
			false  //allow corners
		);
		
		this.findPath(unit);
	}

}

var AiFactory = {
	basic : [0], 
	
	getStrategy : function(acceptedTiles,grid,diagonals,corners){
		var easystar = new EasyStar.js();
		easystar.setGrid(grid);
		easystar.setAcceptableTiles(acceptedTiles);
		if(diagonals) easystar.enableDiagonals();
		if(!corners) easystar.disableCornerCutting();	
		return easystar;
	}

}

var UnitFactory = {
	createUnit : function(xo,yo,xd,xy){
		var key = LIB.guid();
		
		var unit = {
			heroMapPos : {x:xo*30,y:yo*30},
			heroMapTile : {x:xo,y:yo},
			finalDestination : {x:xd,y:xy},
			path : [],
			isFindingPath : false,
			isWalking : false,
			destination : null,
			stepsTillTurn : 19,
			stepsTaken : 0,
			heroSpeed : 1.2,
			halfSpeed : 0.8,
			facing : null,
			dX : 0,
			dY : 0,
			key : key
		};		
		unit.destination = unit.heroMapTile;
		unit.sprite = SpriteFactory.createSpriteWithAnimation('units');
		
		GLOBAL.units[key] = unit;
		
		UnitInterface.aiStart(unit);
		
		return unit;
	}
}

var UnitsInterface = {
	update : function(){
		for (var unit in GLOBAL.units){
			if(GLOBAL.units[unit]){
				UnitInterface.aiWalk(GLOBAL.units[unit]);
			}			
		}
	}
}

var SpriteFactory = {
	createSpriteWithAnimation(name){
		var sprite = Engine.addSprite(name);
		
		this.addAnimations(sprite);
		
		return sprite;
	},
	
	addAnimations(sprite){
		var speed = 10;
		Engine.setAnimation(sprite,'north', 	['l (1)','l (9)' ,'l (17)','l (25)','l (33)','l (41)','l (49)','l (57)','l (65)','l (73)','l (81)'],speed);
		Engine.setAnimation(sprite,'northeast', ['l (2)','l (10)','l (18)','l (26)','l (34)','l (42)','l (50)','l (58)','l (66)','l (74)','l (82)'],speed);
		Engine.setAnimation(sprite,'east', 		['l (3)','l (11)','l (19)','l (27)','l (35)','l (43)','l (51)','l (59)','l (67)','l (75)','l (83)'],speed);
		Engine.setAnimation(sprite,'southeast', ['l (4)','l (12)','l (20)','l (28)','l (36)','l (44)','l (52)','l (60)','l (68)','l (76)','l (84)'],speed);
		Engine.setAnimation(sprite,'south', 	['l (5)','l (13)','l (21)','l (29)','l (37)','l (45)','l (53)','l (61)','l (69)','l (77)','l (85)'],speed);
		Engine.setAnimation(sprite,'southwest', ['l (6)','l (14)','l (22)','l (30)','l (38)','l (46)','l (54)','l (62)','l (70)','l (78)','l (86)'],speed);
		Engine.setAnimation(sprite,'west', 		['l (7)','l (15)','l (23)','l (31)','l (39)','l (47)','l (55)','l (63)','l (71)','l (79)','l (87)'],speed);
		Engine.setAnimation(sprite,'northwest', ['l (8)','l (16)','l (24)','l (32)','l (40)','l (48)','l (56)','l (64)','l (72)','l (80)','l (88)'],speed);
	},
}

var Engine = {
	setLoadSettings : function(){
		game.load.crossOrigin='Anonymous';	
	},
	loadSpriteSheet : function(name,png,json){
		game.load.atlasJSONArray(name,png,json);
	},
	setGameSettings : function(){
		game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		game.stage.backgroundColor = '#cccccc';
		gameScene = game.add.renderTexture(game.width,game.height);
		game.add.sprite(0, 0, gameScene);
	},
	setClickEvent : function(func){
		game.input.onDown.add(func);
	},
	makeSprite : function(name){
		return game.make.sprite(-50, 0, name,0);
	},
	addSprite : function(name){
		return game.add.sprite(-50, 0, name, 0);
	},
	setAnimation(sprite,name,frames,speed){
		sprite.animations.add(name,frames,speed,true,true);
	},
	startGame : function(width,height,container,preload,create,update){
		return new Phaser.Game(width, height, Phaser.AUTO, container, { 
			preload: preload, 
			create: create, 
			update:update 
		});
	},
	
}

var HtmlInterface = {
	updateMenu : function() {
		var querySelector = document.querySelector.bind(document);
		var nav = document.querySelector('.vertical_nav');
		var wrapper = document.querySelector('.wrapper');
		var menu = document.getElementById("js-menu");
		var subnavs = menu.querySelectorAll('.menu--item__has_sub_menu');    

		querySelector('.toggle_menu').onclick = function () {
			nav.classList.toggle('vertical_nav__opened');
			wrapper.classList.toggle('toggle-content');
		};

		querySelector('.collapse_menu').onclick = function () {
			nav.classList.toggle('vertical_nav__minify');
			wrapper.classList.toggle('wrapper__minify');
			for (var j = 0; j < subnavs.length; j++) {
				subnavs[j].classList.remove('menu--subitens__opened');
			}
		};

		for (var i = 0; i < subnavs.length; i++) {
			if (subnavs[i].classList.contains('menu--item__has_sub_menu') ) {
				subnavs[i].querySelector('.menu--link').addEventListener('click', function (e) {
					for (var j = 0; j < subnavs.length; j++) {
						if(e.target.offsetParent != subnavs[j])
							subnavs[j].classList.remove('menu--subitens__opened');          
					}
					e.target.offsetParent.classList.toggle('menu--subitens__opened');
				}, false);
			}
		}
	},			
	
	createMenuElement(key,icon,items){
		/*
			users
			monument
			pagelines
			globe
			coins
		*/
		
		var element = this.getMenu(key,icon,items);
		GLOBAL.UI.menu.innerHTML += element;
		GLOBAL.UI.menuElements[key] = document.getElementById(key+"_sub_menu");
	},
	createMenuElementItem(key,name,action){
		document.getElementById(key+"_sub_menu").innerHTML += this.getMenuItem(name,action);
	},
	
	createMenu(){
		
		GLOBAL.UI.menu = document.getElementById("js-menu");
				
		var items;
		
		items = [
			{name:"Casa",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.casa1	)"}
		];
		this.createMenuElement("Families","users",items);
		
		items = [
			{name:"Casa",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.casa1	)"},
			{name:"Taller",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.taller1	)"},
			{name:"Tienda",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.tienda1	)"},
			{name:"Choza",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.choza1	)"},
			{name:"Corral",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.corral	)"},
			{name:"Establo",action:"ActionsHandler.setBuildAction(CONST.IBERIA.establo1	)"},
			{name:"Horno",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.horno1	)"}
		];
		this.createMenuElement("Buildings","building",items);
		
		items = [
			{name:"Arbol",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.arbol	)"},
			{name:"Frutero",action:"ActionsHandler.setBuildAction(CONST.IBERIA.frutero	)"},
			{name:"Trigo",	action:"ActionsHandler.setBuildAction(CONST.IBERIA.trigo	)"}
		];
		this.createMenuElement("Agriculture","pagelines",items);
		
		items = [];
		this.createMenuElement("World","globe",items);
		
		this.updateMenu();
	},
	getMenu : function(name,icon,items){
		var html = "";
		html += '<li class="menu--item '+(items.length>0 ? "menu--item__has_sub_menu" : "")+'">';
		html += '	<label class="menu--link" title="ItemaLosowa">';
		html += '		<i class="menu--icon  fa fa-fw fa-'+icon+'"></i>';
		html += '		<span class="menu--label">'+name+'</span>';
		html += '	</label>';
		html += '	<ul id="'+name+'_sub_menu" class="sub_menu">';
		for (var i = 0; i < items.length; i++){
			html += this.getMenuItem(items[i].name,items[i].action);
		}				
		html += '	</ul>';
		html += '</li>';
		return html;
	},
	getMenuItem : function(name,action){
		var html = "";
		html += '<li class="sub_menu--item">';
		html += '	<a href="#" class="sub_menu--link sub_menu--link__active" onclick="'+action+'">';
		html += name;
		html += '	</a>';
		html += '</li>';
		return html;		
	},
}

var FamilyInterface = {
	addFamily : function(name){
		var key = LIB.guid();		
		HtmlInterface.createMenuElementItem("Families",name,"ActionsHandler.setFamily('"+key+"')");
		
		
		
		GLOBAL.families[key]=FamilyFactory.createFamily();
		
	},
	setFamily : function(key){
		GLOBAL.currentFamily = key;
	},
	getFamily : function(key){
		return GLOBAL.families[key];
	},
	addBuilding : function(family,key,image){
		GLOBAL.families[family].buildings.push(key);
		if(image == CONST.IBERIA.casa1.i){
			GLOBAL.families[family].home = key;
		}
		else if(image == CONST.IBERIA.trigo.i){
			GLOBAL.families[family].work = key;
		}
	}
}

var FamilyFactory = {
	createFamily : function(){
		var family = {
			name:name,
			money:100,
			buildings:[],
			home:null,
			work:null,
			
		};
		return family;
	}
}

var FamilyLifeManager = {
	goToWork : function(){
		for(var family in GLOBAL.families){
			var home = GLOBAL.families[family].home;
			var work = GLOBAL.families[family].work;
			if(home && work){
				var homeBuilding = GLOBAL.buildings[home];
				var workBuilding = GLOBAL.buildings[work];
			
				if(homeBuilding && workBuilding){
					UnitFactory.createUnit(homeBuilding.pos.x,homeBuilding.pos.y,workBuilding.pos.x-1,workBuilding.pos.y);
				}			
			}			
		}
	},
	goHome : function(){
		for(var family in GLOBAL.families){
			var home = GLOBAL.families[family].home;
			var work = GLOBAL.families[family].work;
			if(home && work){
				var homeBuilding = GLOBAL.buildings[home];
				var workBuilding = GLOBAL.buildings[work];
			
				if(homeBuilding && workBuilding){
					UnitFactory.createUnit(workBuilding.pos.x,workBuilding.pos.y,homeBuilding.pos.x+1,homeBuilding.pos.y+1);
				}			
			}			
		}
	},
}

var Calendar = {
	HOUR : 0,
	DAY : 0,
	start : function(){
		this.hourTrigger();
		
		game.time.events.loop(1000, this.hourTrigger, this);
	},
	hourTrigger : function(){
		if(this.HOUR==0){
			FamilyLifeManager.goToWork();
		}
		else if(this.HOUR==12){
			FamilyLifeManager.goHome();
		}		
		
		this.HOUR+=1;
		
		if(this.HOUR>23){
			this.HOUR=0;
			this.DAY+=1;
		}
	},
}

////////////////////////////////

function preload() {	
	Engine.setLoadSettings();

	var path = 'https://bitbucket.org/johnhammer/johnhammer.bitbucket.io/raw/master/zeus/';
	
	Engine.loadSpriteSheet('iberia',path+'spritesheet%20(10).png',path+'sprites%20(9).json');
	Engine.loadSpriteSheet('units',path+'unit.png',path+'unit.json');		
}

function create() {
    GLOBAL.SPRITES.iberia = Engine.makeSprite('iberia');
	
	Engine.setGameSettings();
	
	MapInterface.createMap(GLOBAL.mapSize);
		
	Drawer.render();
	
	Calendar.start();
	
    Engine.setClickEvent(EVENTS.leftClick);
}

function update(){
	UnitsInterface.update();
	Drawer.render();
}

